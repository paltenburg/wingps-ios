//
//  DownloadService.swift
//  WinGPS Marine
//
//  Created by Standaard on 04/01/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class DownloadService :NSObject, URLSessionDownloadDelegate{
    
    static let service = DownloadService()
    
    class DownloadItem {
        var path :String?
        var size :Int64?
        var targetFileURL :URL
        var pid :Int = 0
        var mod :Int = 0
        var fileName :String = ""
        var append :Bool = false
        var finishedHandler :((Int) -> Void)
        var task :URLSessionDownloadTask
        init(targetFileURL :URL, task :URLSessionDownloadTask, finishedHandler :@escaping ((Int) -> Void)){
            self.targetFileURL = targetFileURL
            self.task = task
            self.finishedHandler = finishedHandler
        }
    }
    
    // store downloads by request parameters which are in the httpbody Data-object.
    var downloadsByRequest :[String: DownloadItem] = [:]
    
    //	private let list = [DownloadItem]()
    
    var backgroundSession :URLSession = URLSession()
    
    var downloadManagerController :DownloadManagerController? = nil
    
    var mapViewController :ViewController?
    
    private override init(){
        super.init()
        let configuration = URLSessionConfiguration.background(withIdentifier: "backgroundDownloadSession")
        self.backgroundSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        mapViewController = ViewController.instance
        
        updateDownloadSettings()
    }
    
    func updateDownloadSettings(){
        let settings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
        
        let useMobileData :Bool = settings?.object(forKey: Resources.SETTINGS_USE_MOBILE_DATA) as? Bool ?? true
        if useMobileData != self.backgroundSession.configuration.allowsCellularAccess {
            self.backgroundSession.configuration.allowsCellularAccess = useMobileData
            // need to restart currently active downloads?
        }
    }
    
    func addToQueue (){
        
    }
    
    func doRequest(_ request :URLRequest, pid :Int, mod :Int, fileName :String, targetURL :URL, append :Bool, finishedHandler :@escaping (Int) -> ()){
        print("DownloadService:doRequest request-body:\(request.httpBody?.toString())")
        
        guard let requestParameters = request.httpBody?.toString() else {
            return
        }
        
        // appending at the end of existing files still to be implemented
        
        let task = DownloadService.service.backgroundSession.downloadTask(with: request)
        
        let dlItem = DownloadItem(targetFileURL: targetURL, task: task, finishedHandler: finishedHandler)
        dlItem.pid = pid
        dlItem.mod = mod
        dlItem.fileName = fileName
        
        self.downloadsByRequest[requestParameters] = dlItem
        
        let totalSize = 0
        let headers = (request as? NSURLRequest)?.allHTTPHeaderFields
        print("doRequest: \(targetURL) , \(String(task.currentRequest!.hashValue)) , headers: \(headers) , total size: \(totalSize)")
        
        
        //        let testTask = URLSession.shared.downloadTask(with: request) { (data, response, error) in
        //            let HTTPResponse = response as? HTTPURLResponse
        //            if
        //                error == nil,
        //                let response = response as? HTTPURLResponse,
        //                let contentLength = response.allHeaderFields["Content-Length"] as? String {
        //                dout("contentLength: \(contentLength)")
        //                return
        //            }
        //        }
        //        testTask.resume()
        
        task.resume()
        
        
        // Enable indicator in mapview
        if let mv = mapViewController {
            DispatchQueue.main.async{
                mv.indicatorChartDownloading.isHidden = false
            }
        }
    }
    
    // Update progress
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64, totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
        print("DownloadService:urlSession: didWriteData")
        
        guard let requestParameters = downloadTask.currentRequest?.httpBody?.toString() else { return }
        
        print("download update: \(totalBytesWritten) \(totalBytesExpectedToWrite) for request \(requestParameters)")
        
        //		let divider = ";"
        //		let progressValues :String = "\(totalBytesWritten)\(divider)\(totalBytesExpectedToWrite)\(divider)\(requestParameters)"
        //		downloadsByRequest[requestParams]?.progressHandler?(StChartCheckTask.CHECKRESULT_PROGRESS, progressValues)
        
        if let download = downloadsByRequest[requestParameters] {
            // update dl-manager if it is in view
            // TODO //////////////////// Run from main thread
            (getTopViewController() as? DownloadManagerController)?.setProgress(pid: download.pid, mod: download.mod, fileName: download.fileName, soFar: totalBytesWritten, total: totalBytesExpectedToWrite)
        }
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        //		print("download finished for \(mChartCheckTask.mOutputFile!.path) temp name: \(location.path) size: \(getFileSize(location.path)!)" )
        print("DownloadService:urlSession:didFinishDownloadingTo")
        
        guard let requestParameters = downloadTask.currentRequest?.httpBody?.toString() else { return }
        
        print("download finished for \(downloadsByRequest[requestParameters]?.targetFileURL.path), \(String(downloadTask.currentRequest!.hashValue)), \(requestParameters)" )
        
        // This delegate method can be called too early (right after the initial request), so check if it's really done downloading, or if doRequest() has even finished. If so: Ignore
        guard let download = downloadsByRequest[requestParameters], // BAD EXEC
              let fileSize = getFileSize(location.path) else {
            print("DownloadService:urlSession:didFinishDownloadingTo failed with download: \(downloadsByRequest[requestParameters]), fileSize: \(getFileSize(location.path))")
            return
        }
        
        print("size: \(String(fileSize))")
        
        
        if FileManager.default.fileExists(atPath: download.targetFileURL.path) {
            // appending to existing files still to be implemented
            do {
                try FileManager.default.removeItem(at: download.targetFileURL)
            } catch {
                print("Error removing existing file: \(download.targetFileURL.path)")
            }
        }
        
        do {
            try FileManager.default.copyItem(at: location, to: download.targetFileURL)
            //			try FileManager.default.moveItem(at: location, to: mChartCheckTask.mOutputFile!)
        } catch (let writeError) {
            print("error writing file \(location) : \(writeError)")
        }
        
        downloadsByRequest[requestParameters] = nil
        
        download.finishedHandler(StChartCheckTask.CHECKRESULT_OK)
        
        // Disable indicator in mapview
        if downloadsByRequest.count == 0,
           let mv = mapViewController {
            DispatchQueue.main.async{
                mv.indicatorChartDownloading.isHidden = true
            }
        }
        
        // Handle the "finishing of all downloads" here centrally.
        if downloadsByRequest.count == 0 {
            let alert = UIAlertController(title: "dialog_message_downloads_finished".localized, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                // return to login screen
            }))
            getTopViewController()?.presentFromMain(alert, animated: true)
            DKW2ChartManager.refreshCharts()
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didCompleteWithError error :Error?) {
        print("DownloadService: download didCompleteWithError \(error!.localizedDescription))")
    }
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        print("DownloadService:urlSessionDidFinishEvents" )
        
        //		session.finishTasksAndInvalidate()
        
        DispatchQueue.main.async {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,
               let completionHandler = appDelegate.backgroundSessionCompletionHandler
            {
                appDelegate.backgroundSessionCompletionHandler = nil
                completionHandler()
            }
        }
    }
    
    func cancelDownload(pid :Int, mod :Int) {
        
        // cancel all downloads related to the product with the given codedstring
        for (request, download) in downloadsByRequest {
            if download.pid == pid, download.mod == mod {
                download.task.cancel()
                downloadsByRequest[request] = nil
            }
        }
        
        // Disable indicator in mapview
        if downloadsByRequest.count == 0,
           let mv = mapViewController {
            DispatchQueue.main.async{
                mv.indicatorChartDownloading.isHidden = true
            }
        }
    }
    
    func startChartDownload(pid: Int, mod: Int){
        StatusCheck.runFromDownloadService(callBackHandler: nil, pid: pid, mod: mod)
    }
}
