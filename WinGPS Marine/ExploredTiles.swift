//
//  ExploredTileOverlay.swift
//  MapDemo
//
//  Created by Standaard on 07/07/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

class ExploredTileOverlay: MKTileOverlay {
    
    var base_path: String
    let cache = NSCache<AnyObject, AnyObject>()
    //    var point_buffer: ExploredSegment
    var last_tile_path: MKTileOverlayPath?
    //    var tile_buffer: ExploredTiles
    
//    init(URLTemplate: String?, startingLocation location: CLLocation) {
    override init(urlTemplate URLTemplate: String?) {
            let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: AnyObject = paths[0] as AnyObject
        self.base_path = documentsDirectory.appendingPathComponent("/")
        if (!FileManager.default.fileExists(atPath: base_path)) {
            try! FileManager.default.createDirectory(atPath: base_path, withIntermediateDirectories: false, attributes: nil)
        }
        
//        let new_point = MKMapPointForCoordinate(location.coordinate)
        //        self.point_buffer = ExploredSegment(fromPoint: new_point, inCity: city)
        //        self.tile_buffer = ExploredTiles(startingPoint: ExploredPoint(mapPoint: new_point, r: 50))
        //        self.last_tile_path = Array(tile_buffer.edited_tiles.values).last!.path
        super.init(urlTemplate: URLTemplate)
    }
    
    override func url(forTilePath path: MKTileOverlayPath) -> URL {
        let filled_template = String(format: "%d_%d_%d.png", path.z, path.x, path.y)
        let tile_path = base_path + "/" + filled_template
        //dout(.tiles, "fetching tile " + filled_template)
        if !FileManager.default.fileExists(atPath: tile_path) {
            return URL(fileURLWithPath: "")
        }
        
        return URL(fileURLWithPath: tile_path)
    }
    
    override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
        
        
//        result(nil, nil) // pass through for test

        let url = self.url(forTilePath: path)
        let filled_template = String(format: "%d_%d_%d.png", path.z, path.x, path.y)
//        let tile_path = base_path + "/" + filled_template
        let tile_path = base_path + "/" + "test-tile.png"
        
        dout(.tiles, "loadtile path: " + String(describing: path))
        
        dout(.tiles, "base path: ", base_path)
        dout(.tiles, "tile path: ", tile_path)
        
        
//        if (url != URL(fileURLWithPath: tile_path)) {
//            dout(.tiles, "creating tile at " + String(describing: path))
//            let img_data: Data = UIImagePNGRepresentation(UIImage(named: "small")!)!
//            let filled_template = String(format: "%d_%d_%d.png", path.z, path.x, path.y)
//            let tile_path = base_path + "/" + filled_template
//            //            img_data.writeToFile(tile_path, atomically: true)
//            cache.setObject(img_data as AnyObject, forKey: url as AnyObject)
//            result(img_data, nil)
//            return
//        } else if let cachedData = cache.object(forKey: url as AnyObject) as? NSData {
//            dout(.tiles, "using cache for " + String(describing: path))
//            result(cachedData as Data, nil)
//            return
//        } else {
            dout(.tiles, "loading " + String(describing: path) + " from directory")
            let img_data: NSData = UIImage(contentsOfFile: tile_path)!.pngData()! as NSData
            cache.setObject(img_data, forKey: url as AnyObject)
            result(img_data as Data, nil)
            return
//        }
    }
    
}

class ExploredTileRenderer: MKTileOverlayRenderer {
    
    //    let tile_overlay: ExploredTileOverlay
    //    var zoom_scale: MKZoomScale?
    //    let cache: Cache = Cache()
    //
    //    override init(overlay: MKOverlay) {
    //        self.tile_overlay = overlay as! ExploredTileOverlay
    //        super.init(overlay: overlay)
    //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(saveEditedTiles), name: "com.Coder.Wander.reachedMaxPoints", object: nil)
    //    }
    //
    //    // There's some weird cache-ing thing that requires me to recall it
    //    // whenever I re-draw over the tile, I don't really get it but it works
    //    override func canDrawMapRect(mapRect: MKMapRect, zoomScale: MKZoomScale) -> Bool {
    //        self.setNeedsDisplayInMapRect(mapRect, zoomScale: zoomScale)
    //        return true
    //    }
    //
    //    override func drawMapRect(mapRect: MKMapRect, zoomScale: MKZoomScale, inContext context: CGContext) {
    //        zoom_scale = zoomScale
    //        let tile_path = self.tilePathForMapRect(mapRect, andZoomScale: zoomScale)
    //        let tile_path_string = stringForTilePath(tile_path)
    //        //dout(.tiles, "redrawing tile: " + tile_path_string)
    //        self.tile_overlay.loadTileAtPath(tile_path, result: {
    //            data, error in
    //            if error == nil && data != nil {
    //                if let image = UIImage(data: data!) {
    //                    let draw_rect = self.rectForMapRect(mapRect)
    //                    CGContextDrawImage(context, draw_rect, image.CGImage)
    //                    var path: [(CGMutablePath, CGFloat)]? = nil
    //                    self.tile_overlay.point_buffer.readPointsWithBlockAndWait({ points in
    //                        let total = self.getPathForPoints(points, zoomScale: zoomScale, offset: MKMapPointMake(0.0, 0.0))
    //                        path = total.0
    //                        //dout(.tiles, "number of points: " + String(path!.count))
    //                    })
    //                    if ((path != nil) && (path!.count > 0)) {
    //                        //dout(.tiles, "drawing path")
    //                        for segment in path! {
    //                            CGContextAddPath(context, segment.0)
    //                            CGContextSetBlendMode(context, .Clear)
    //                            CGContextSetLineJoin(context, CGLineJoin.Round)
    //                            CGContextSetLineCap(context, CGLineCap.Round)
    //                            CGContextSetLineWidth(context, segment.1)
    //                            CGContextStrokePath(context)
    //                        }
    //                    }
    //                }
    //            }
    //        })
    //    }
    
    let tile_overlay: ExploredTileOverlay
    var zoom_scale: MKZoomScale?
    //    let cache = NSCache()
    
    //    override init(overlay: MKOverlay) {
    //        self.tile_overlay = overlay as! DKW2TileOverlay
    //        super.init(overlay: overlay)
    //        //        NotificationCenter.defaultCenter().addObserver(self, selector: #selector(saveEditedTiles), name: "com.Coder.Wander.reachedMaxPoints", object: nil)
    //    }
    
    override init(overlay: MKOverlay) {
        self.tile_overlay = overlay as! ExploredTileOverlay
        super.init(overlay: overlay)
        //        NotificationCenter.defaultCenter().addObserver(self, selector: #selector(saveEditedTiles), name: "com.Coder.Wander.reachedMaxPoints", object: nil)
    }
    
        // There's some weird cache-ing thing that requires me to recall it
        // whenever I re-draw over the tile, I don't really get it but it works
        override func canDraw(_ mapRect: MKMapRect, zoomScale: MKZoomScale) -> Bool {
            self.setNeedsDisplay(mapRect, zoomScale: zoomScale)
            return true
        }
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        dout(.tiles, "Explored renderer draw()")
        zoom_scale = zoomScale
        let tile_path = tilePathForMapRect(mapRect: mapRect, andZoomScale: zoomScale)
        let tile_path_string = stringForTilePath(path: tile_path)
        //dout(.tiles, "redrawing tile: " + tile_path_string)
        self.tile_overlay.loadTile(at: tile_path, result: {
            data, error in
//            if error == nil && data != nil {
            if error == nil {
                let draw_rect = self.rect(for: mapRect)

                dout(.tiles, "draw result", mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height)
                dout(.tiles, "draw_rect", draw_rect.origin.x, draw_rect.origin.y, draw_rect.size.width, draw_rect.size.height)
                
                UIGraphicsPushContext(context)
                context.setLineWidth(10.0)
                context.setStrokeColor(UIColor.blue.cgColor)
                context.move(to: CGPoint(x: mapRect.origin.x, y: mapRect.origin.y))
                context.addLine(to: CGPoint(x: mapRect.origin.x + mapRect.size.width, y: mapRect.origin.y + mapRect.size.height))
                context.addLine(to: CGPoint(x: 0, y: 0))
                context.strokePath()
                context.drawPath(using: .fillStroke)

                UIGraphicsPopContext()
//                if let image = UIImage(data: data!) {
//                    let draw_rect = self.rect(for: mapRect)
//                    //                    CGContextDrawImage(context, draw_rect, image.cgImage)
//                    context.draw(image.cgImage!, in: draw_rect)
//                    var path: [(CGMutablePath, CGFloat)]? = nil
//                    //                    self.tile_overlay.point_buffer.readPointsWithBlockAndWait({ points in
//                    //                        let total = self.getPathForPoints(points, zoomScale: zoomScale, offset: MKMapPointMake(0.0, 0.0))
//                    //                        path = total.0
//                    //                        //dout(.tiles, "number of points: " + String(path!.count))
//                    //                    })
//                    if ((path != nil) && (path!.count > 0)) {
//                        //dout(.tiles, "drawing path")
//                        for segment in path! {
//                            context.addPath(segment.0)
//                            //                            CGContextSetBlendMode(context, .Clear)
//                            //                            CGContextSetLineJoin(context, CGLineJoin.Round)
//                            //                            CGContextSetLineCap(context, CGLineCap.Round)
//                            //                            CGContextSetLineWidth(context, segment.1)
//                            //                            CGContextStrokePath(context)
//                        }
//                    }
//                }
            }
        }
        )
    }
    
    
}

//func tilePathForMapRect(mapRect: MKMapRect, andZoomScale zoom: MKZoomScale) -> MKTileOverlayPath {
//    let zoom_level = self.zoomLevelForZoomScale(zoom)
//    let mercatorPoint = self.mercatorTileOriginForMapRect(mapRect)
//    //dout(.tiles, "mercPt: " + String(mercatorPoint))
//
//    let tilex = Int(floor(Double(mercatorPoint.x) * self.worldTileWidthForZoomLevel(zoom_level)))
//    let tiley = Int(floor(Double(mercatorPoint.y) * self.worldTileWidthForZoomLevel(zoom_level)))
//
//    return MKTileOverlayPath(x: tilex, y: tiley, z: zoom_level, contentScaleFactor: UIScreen.mainScreen().scale)
//}
//
//func stringForTilePath(path: MKTileOverlayPath) -> String {
//    return String(format: "%d_%d_%d", path.z, path.x, path.y)
//}
//
//func zoomLevelForZoomScale(zoomScale: MKZoomScale) -> Int {
//    let real_scale = zoomScale / UIScreen.mainScreen().scale
//    var z = Int((log2(Double(real_scale))+20.0))
//    z += (Int(UIScreen.mainScreen().scale) - 1)
//    return z
//}
//
//func worldTileWidthForZoomLevel(zoomLevel: Int) -> Double {
//    return pow(2, Double(zoomLevel))
//}
//
//func mercatorTileOriginForMapRect(mapRect: MKMapRect) -> CGPoint {
//    let map_region: MKCoordinateRegion = MKCoordinateRegionForMapRect(mapRect)
//
//    var x : Double = map_region.center.longitude * (M_PI/180.0)
//    var y : Double = map_region.center.latitude * (M_PI/180.0)
//    y = log10(tan(y) + 1.0/cos(y))
//
//    x = (1.0 + (x/M_PI)) / 2.0
//    y = (1.0 - (y/M_PI)) / 2.0
//
//    return CGPointMake(CGFloat(x), CGFloat(y))
//}

func tilePathForMapRect(mapRect: MKMapRect, andZoomScale zoom: MKZoomScale) -> MKTileOverlayPath {
    let zoom_level = zoomLevelForZoomScale(zoomScale: zoom)
    let mercatorPoint = mercatorTileOriginForMapRect(mapRect: mapRect)
    //dout(.tiles, "mercPt: " + String(mercatorPoint))
    
    let tilex = Int(floor(Double(mercatorPoint.x) * worldTileWidthForZoomLevel(zoomLevel: zoom_level)))
    let tiley = Int(floor(Double(mercatorPoint.y) * worldTileWidthForZoomLevel(zoomLevel: zoom_level)))
    
    return MKTileOverlayPath(x: tilex, y: tiley, z: zoom_level, contentScaleFactor: UIScreen.main.scale)
}

func stringForTilePath(path: MKTileOverlayPath) -> String {
    return String(format: "%d_%d_%d", path.z, path.x, path.y)
}

func zoomLevelForZoomScale(zoomScale: MKZoomScale) -> Int {
    let real_scale = zoomScale / UIScreen.main.scale
    var z = Int((log2(Double(real_scale))+20.0))
    z += (Int(UIScreen.main.scale) - 1)
    return z
}

func worldTileWidthForZoomLevel(zoomLevel: Int) -> Double {
    return pow(2, Double(zoomLevel))
}

func mercatorTileOriginForMapRect(mapRect: MKMapRect) -> CGPoint {
    let map_region: MKCoordinateRegion = MKCoordinateRegion.init(mapRect)
    
    var x : Double = map_region.center.longitude * (.pi/180.0)
    var y : Double = map_region.center.latitude * (.pi/180.0)
    y = log10(tan(y) + 1.0/cos(y))
    
    x = (1.0 + (x / .pi)) / 2.0
    y = (1.0 - (y / .pi)) / 2.0
    
    return CGPoint(x: CGFloat(x), y: CGFloat(y))
}
