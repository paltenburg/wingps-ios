//
//  AccountSettingsController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 23/01/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class AccountSettingsController : UIViewController, UITextFieldDelegate {
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var accountSettingsContainer: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginStackView: UIStackView!
    @IBOutlet weak var loggedinStackView: UIStackView!
    @IBOutlet weak var busyIndicatorView: UIActivityIndicatorView!
    //	@IBOutlet weak var accountSettingsContainerBottom: NSLayoutConstraint!
    @IBOutlet weak var loggedInUserNameLabel: UILabel!
    @IBOutlet weak var accountSettingsContainerBottom2: NSLayoutConstraint!
    @IBOutlet weak var purchaseDateLabel: UILabel!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    
    @IBOutlet weak var backButtonOulet: UIButton!
    @IBOutlet weak var closeButtonOutlet: UIButton!
    
    var bottomHeight : CGFloat = 0.0
    
    var mActDB :StActManager = StActManager.getActManager()
    
    var receiptCheckError :Int = 0
    
    var windowIsLocked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if windowIsLocked {
            backButtonOulet.isHidden = true
            closeButtonOutlet.isHidden = true
        }
        
        //		MainMenuController.instance?.dismiss(animated: false, completion: nil)
        
        // Make the purchase date visible that's from a validated receipt
        if AppStorePurchases.purchaseDateString.count <= 0 {
            AppStorePurchases().makeSureThereIsAReceipt()
            AppStorePurchases.getValidatedReceipt()
        }
        
        purchaseDateLabel.text = AppStorePurchases.purchaseDateString
        
        bottomHeight = accountSettingsContainerBottom2.constant
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
        
        accountSettingsContainer.layer.cornerRadius = 8
        accountSettingsContainer.clipsToBounds = true
        
        loginButton.layer.cornerRadius = 8
        loginButton.clipsToBounds = true
        
        logoutButton.layer.cornerRadius = 8
        logoutButton.clipsToBounds = true
        
        createAccountButton.layer.cornerRadius = 8
        createAccountButton.clipsToBounds = true
        
        usernameTextField.delegate = self
        usernameTextField.tag = 0
        usernameTextField.keyboardType = .emailAddress
        
        //usernameTextField.becomeFirstResponder()
        passwordTextField.delegate = self
        passwordTextField.tag = 1
        
        mActDB.initialize()
        var EMail = mActDB.getUserMail()
        var Pass = mActDB.getUserPass()
        if EMail.count != 0 && Pass.count != 0 {
            // switch to logged in view
            loginStackView.isHidden = true
            loggedinStackView.isHidden = false
            loggedInUserNameLabel.text = EMail
        } else {
            loggedinStackView.isHidden = true
            loginStackView.isHidden = false
        }
        
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: "unwindSegueToSettings", sender: self)
    }
    
    @IBAction func closeButton(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if touch?.view == backgroundView && !windowIsLocked {
            self.dismiss(animated: false, completion: nil) // disabled, so the login screen can't be dismissed as easily
        }
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        usernameTextField.endEditing(true)
        passwordTextField.endEditing(true)
        
        DispatchQueue.main.async{
            self.loginStackView.isHidden = true
            self.busyIndicatorView.startAnimating()
        }
        
        // check login data
        
        var user = usernameTextField.text ?? ""
        var pass = passwordTextField.text ?? ""
        
        //		if StUtils.isDebug() {
        //			if user.count == 0 && pass.count == 0 {
        //				user = "stentecgalaxytabs28@gmail.com"
        //				pass = "st4soft"
        //			}
        //		}
        
        mActDB.setUserData(aEmail: user, aPassword: pass)
        
        
        
        // Run a check that's depending on the type of app
        
        switch Resources.appType {
        case .lite, .plus: // run account and device check
            
            // initialize chartCheckTask and assign handler
            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
            })
            chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: mActDB)
            
        case .dkw1800: // run account and receipt check
            
            // initialize chartCheckTask and assign handler
            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.loginHandleMessagesFromReceiptCheck(msg: msg, data: data)
            })
            chartCheckTask.requestCheckAccountAndReceipt(aActDB: mActDB)
         
        case .friesemeren: // run account and receipt check
            
            // initialize chartCheckTask and assign handler
            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
            })

            let pid = 196664
            let mod = 1
            chartCheckTask.requestFreeChartActAccount(aEMail: user ?? "", aPass: pass ?? "", aPid: pid, aMod: mod, aErrorHandler: {(result :ErrorCode) in
                print("requestFreeChartActAccount", "Result: ",  result)
            })
            
        }
        
    }
    
    func loginHandleMessages(cmd :String, msg :Int, data :String){
        var errorCode = msg
        
        if errorCode == StChartCheckTask.CHECKRESULT_OK {
            
            StUtils.setLoggedInYetAfterInstall()
            
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: {() -> Void in
                    // always show download manager
                    self.showDownloadManagerView()
                })
            }
            
            //			let alert = UIAlertController(title: "error_dialog_title, message: errorMessage, preferredStyle: .alert)
            //			alert.addAction(UIAlertAction(title: "ok, style: .default, handler: {(alert) -> Void in
            //				// return to login screen
            //			}))
            //			self.presentFromMain(alert, animated: true)
            
        } else {
            
            let errorMessage = StatusCheck.serverErrorMessage(cmd: cmd, code: errorCode)
            
            //			// Errorcodes:
            //			switch errorCode {
            //			//		RESULT_DATABASE_ERROR (-4), error while doing something with database
            //			case 4: // RESULT_USER_NOT_FOUND (4), no user found with the given email and password
            //				errorMessage = "statuscheck_connect_unknownuser".localized
            //
            //				//		RESULT_NO_ANONYMOUS_ACTIVATION (2), no anonymous activation allowed
            //				//		RESULT_APPDEVICECHECK_GET (-12), devicecheck get bitstate failed
            //				//		RESULT_APPDEVICECHECK_SET (-13), devicecheck set bitstate failed
            //				//		RESULT_APPWASACTIVE (30), there was still a blacklisted activation for a manually reset activation (1b2 situation)
            //				//		RESULT_APPACTIVEDIFF (34), given activation is not the active one (2b situation)
            //				//		RESULT_APPBLACKLISTED (32), given activation is blacklisted (3b1 situation)
            //				//		RESULT_UNKNOWN_DEVTYPE (33), the given devicetype is not accepted (0 or 1 only)
            //			//		RESULT_ALL_OK (0), everything ok
            //			default:
            //				errorMessage = "statuscheck_acterror_unknownerror".localized + " (code \(errorCode))"
            //			}
            
            mActDB.resetUserData()
            
            DispatchQueue.main.async{
                self.busyIndicatorView.stopAnimating()
                self.loggedinStackView.isHidden = true
                self.loginStackView.isHidden = false
            }
            
            let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                // return to login screen
//                self.loggedinStackView.isHidden = true
//                self.loginStackView.isHidden = false
            }))
            getTopViewController()?.presentFromMain(alert, animated: true)
        }
    }
    
    // For chart specific paid-apps
    func loginHandleMessagesFromReceiptCheck(msg :Int, data :String){
        var errorCode = msg
        
        // Override receiptcheck in case of apple review account:
        let overrideReceiptCheck = mActDB.getUserMail() == "apptest@stentec.com"
        
        if errorCode == StChartCheckTask.CHECKRESULT_OK
            || overrideReceiptCheck {
            
            // next, login with devicecheck, the regular way. After that, open up downloadmanager
            
            // initialize chartCheckTask and assign handler
            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
            })
            chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: mActDB)
            
        } else {
            
            //            result:
            //                0     ok
            //                        new license added, or receipt already spent with same stentec user.
            //                        next: download files with gfd.
            //                3    receipt already spent but with a different stentec user.
            //                -4    database error
            //                11     No license numbers left, or bad licensenumber.
            //               other:     errorcode from Apple receipt check endpoint
            
            // There was an error, but if the chart already exists, there is no need for it anyway.
            
            receiptCheckError = errorCode
            
//            // initialize chartCheckTask and assign handler
//            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
//                self.handleMessagesAfterLoginWithReceiptErrors(msg: msg, data: data)
//            })
//            chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: mActDB)

            // request chart sets in account
            
            // initialize chartCheckTask and assign handler
            let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.handleMessagesAfterAvailableChartsetsRequest(cmd: cmd, msg: msg, data: data)
            })
            chartCheckTask.requestAvailableChartSets(aActDB: mActDB, aUser: mActDB.getUserMail(), aPass: mActDB.getUserPass())

//            var errorMessage = StatusCheck.serverErrorMessage(byCode: errorCode)
//
//            // override error message for this case
//            if errorCode == 3 {
//                // Purchase code already spent for receipt with that timestamp.
//                errorMessage = "statuscheck_error_receiptcheck_3_license_spent".localized
//            }
//
//            logout()
//
//            self.busyIndicatorView.stopAnimating()
//            self.loginStackView.isHidden = false
//
//            let alert = UIAlertController(title: "error_dialog_title".localized, message: errorMessage, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
//                // return to login screen
//                self.loggedinStackView.isHidden = true
//                self.loginStackView.isHidden = false
//            }))
//            getTopViewController()?.presentFromMain(alert, animated: true)
        }
    }

    // Error in receipt check, so the app checked if the receipt was needed in the first place
    func handleMessagesAfterAvailableChartsetsRequest(cmd :String, msg :Int, data :String){
        var errorCode = msg
        
        let overrideReceiptCheck = mActDB.getUserMail() == "apptest@stentec.com"

        if errorCode == StChartCheckTask.CHECKRESULT_OK
            || overrideReceiptCheck {
            
            // Regular login, but still an error with receipt checking
            
            // If account contains charts from app, ignore receipt check warning.
            
            var returnedCharts = [DownloadManagerController.ChartSetItem]()
            
            guard
                let response = try? JSONSerialization.jsonObject(with: data.data(using: .utf8)!) as! [String : Any],
                let sets = response["Sets"] as? [[String : Any]] else {
                    return
            }
            print("AccountsettingsController: JSONresresponse: \(response)")
            for chartSet in sets {
                guard
                    let pid = toOptInt(chartSet["pid"]),
                    let mod = toOptInt(chartSet["mod"])
                    else {
                        continue
                }
                
                var csi = DownloadManagerController.ChartSetItem()
                csi.itemType = 1
                
                csi.productID = Int64(pid)
                csi.moduleBitIndex = mod
                csi.isSub = false
                guard
                    let pName = chartSet["product"] as? String,
                    let mName = chartSet["name"] as? String,
                    let count = toOptInt(chartSet["count"]),
                    let size = optIntToOptInt64(toOptInt(chartSet["size"])),
                    let isCurrentString = chartSet["iscurrent"] as? String,
                    let groupIndex = chartSet["groupindex"] as? String,
                    let orderIndex = chartSet["orderindex"] as? String,
                    let edition = chartSet["edition"] as? String
                    else {
                        continue
                }
                
                csi.pName = pName
                csi.mName = mName
                csi.count = count
                csi.size = size
                //                        case csi.expiry = chartSet["product"] as? String,
                csi.isCurrent = (Int(isCurrentString) ?? 0) == 1
                csi.groupIndex = Int(groupIndex) ?? -1
                csi.orderIndex = Int(orderIndex) ?? -1
                csi.edition = Int(edition) ?? -1
                
                returnedCharts.append(csi)
            }
            
            // For chart apps, filter out only the relevant charts for that app.
            
            if Resources.appType == .dkw1800 {
                
                var filteredCharts = [DownloadManagerController.ChartSetItem]()
                for chartSet in returnedCharts {
                    if ChartApp1800.chartIn1800App(csi: chartSet) {
                        filteredCharts.append(chartSet)
                    }
                }
                
                // nr of filteredcharts should be no less than 8. If so, there is no problem en program is continued.
                if filteredCharts.count >= 8 {
                    
                    // next, login with devicecheck, the regular way. After that, open up downloadmanager
                    let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                        self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
                    })
                    chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: mActDB)
                    
                    return
                    
                } else {
                    // If account doesn't contain charts from this app, continue this function that eventually shows the error
                }
                
            } else if Resources.appType == .friesemeren {
                
                
                
            }
            
        } else {
            // Error getting chart sets
        }
        
        // Error in receipt check wasn't resolved, so display the error
        
        var errorMessage = StatusCheck.serverErrorMessage(cmd: cmd, code: receiptCheckError)
        
//        // override error message for this case
//        if errorCode == 3 {
//            // Purchase code already spent for receipt with that timestamp.
//            errorMessage = "statuscheck_error_receiptcheck_3_license_spent".localized
//        }
        
        logout()
        
        DispatchQueue.main.async{
            self.busyIndicatorView.stopAnimating()
            self.loggedinStackView.isHidden = true
            self.loginStackView.isHidden = false
        }
        
        let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
            // return to login screen
//            self.loggedinStackView.isHidden = true
//            self.loginStackView.isHidden = false
        }))
        getTopViewController()?.presentFromMain(alert, animated: true)
        
    }
    
    @IBAction func logoutButtonAction(_ sender: Any) {
        passwordTextField.text = ""
        
        DispatchQueue.main.async{
            self.loggedinStackView.isHidden = true
            self.busyIndicatorView.startAnimating()
            
        }
        
        // initialize chartCheckTask and assign handler
        let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
            self.logoutHandleMessages(cmd: cmd, msg: msg, data: data)
        })
        // run account and device check
        chartCheckTask.requestDeactivateActiveDevice(aActDB: mActDB)
        
    }
    
    func logoutHandleMessages(cmd: String, msg :Int, data :String){
        var errorCode = msg
        
        let overrideReceiptCheck = mActDB.getUserMail() == "apptest@stentec.com"

        if errorCode == StChartCheckTask.CHECKRESULT_OK
            || overrideReceiptCheck {
            //				RESULT_ALL_OK (0), everything ok. The active activation has been deleted
            
            logout()
            
            //			self.dismiss(animated: true, completion: {() -> Void in })
            
            // // little confirmation dialog:
            //			let alert = UIAlertController(title: "error_dialog_title, message: errorMessage, preferredStyle: .alert)
            //			alert.addAction(UIAlertAction(title: "ok, style: .default, handler: {(alert) -> Void in
            //			}))
            //			self.presentFromMain(alert, animated: true)
            
        } else {
            //			var errorMessage = "statuscheck_acterror_unknownerror".localized + " (code \(errorCode))"
            
            let errorMessage = StatusCheck.serverErrorMessage(cmd: cmd, code: errorCode)
            
            switch errorCode {
                //				errors:
                //				RESULT_DATABASE_ERROR (-4), error while doing something with database
            //				RESULT_USER_NOT_FOUND (4), no user found with the given email and password
            case 4:
                logout()
                //				RESULT_NO_ANONYMOUS_ACTIVATION (2), no anonymous activation allowed
                //				RESULT_APPDEVICECHECK_GET (-12), devicecheck get bitstate failed
                //				RESULT_APPDEVICECHECK_SET (-13), devicecheck set bitstate failed
                //				RESULT_UNKNOWN_DEVTYPE (33), the given devicetype is not accepted (0 or 1 only)
            //				RESULT_NOT_ACTIVATED (6), there is no active activation for the given data
            case 6:
                logout()
            default:
                DispatchQueue.main.async{
                    self.busyIndicatorView.stopAnimating()
                    self.loggedinStackView.isHidden = false
                    self.loginStackView.isHidden = false

                }
                print("LogoutHandleMessages: errorcode \(errorCode)")
            }
            
            
            let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                // return to loggedin screen
            }))
            getTopViewController()?.presentFromMain(alert, animated: true)
        }
    }
    
    //	@IBAction func createNewAccountButtonAction(_ sender: UIButton) {
    //		UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/registers")! as URL)
    //	}
    //
    
    @IBAction func createNewAccountButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        
        performSegue(withIdentifier: "fromAccountToCreateAccountWithSegue", sender: self)
    }
    
    
    func logout(){
        mActDB.resetUserData()
        mActDB.resetProducts()
        
        DownloadManagerController.clearCharts()
        
        DispatchQueue.main.async{
            self.busyIndicatorView.stopAnimating()
            self.loggedinStackView.isHidden = true
            self.loginStackView.isHidden = false
        }
        
        DKW2ChartManager.refreshCharts()
    }
    
    
    @IBAction func forgatPasswordButtonAction(_ sender: Any) {
        UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/lostpassword")! as URL)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
            
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    //	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //		let touch : UITouch? = touches.first
    //		if (touch?.view == backgroundView){
    //			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
    //			self.dismiss(animated: true, completion: nil)
    //		}
    //	}
    
    @objc func keyboardWillShow(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
//            accountSettingsContainerBottom2.constant = keyboardHeight - 50
//        }
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
//        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            accountSettingsContainerBottom2.constant = bottomHeight
//        }
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
    func testBitstate(){
        let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
            self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
            print("testSetBitstate: msg, data: \(String(msg)), \(data)")
            
            let chartCheckTask2 = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
                self.loginHandleMessages(cmd: cmd, msg: msg, data: data)
                print("testGetBitstate: msg, data: \(String(msg)), \(data)")
            })
            chartCheckTask2.requestDeviceCheckGetBitstate()
            
            //			// initialize chartCheckTask and assign handler
            //			let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
            //				self.loginHandleMessages(msg: msg, data: data)
            //			})
            //			// run account and device check
            //			chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: self.mActDB)
        })
        
        chartCheckTask.requestDeviceCheckSetBitstate()
    }
    
    // Bring up DownloadManagerView from code
    func showDownloadManagerView(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let downloadManagerController = storyBoard.instantiateViewController(withIdentifier: "DownloadManagerID") as! DownloadManagerController
        
        downloadManagerController.comingFromLogin = true
        ViewController.instance?.present(downloadManagerController, animated: true, completion: nil)
    }
}
