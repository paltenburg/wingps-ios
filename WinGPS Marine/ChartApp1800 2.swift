//
//  ChartApp1800.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/06/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation

class ChartApp1800 {
    static func chartIn1800App(csi :DownloadManagerController.ChartSetItem) -> Bool {
        
        // pid: 196701 = Editie 2019
        // pid: 196707 = Editie 2020
        if csi.productID == 196707 {
            return true
        }
        return false
    }
}
