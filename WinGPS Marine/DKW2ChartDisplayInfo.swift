//
//  DKW2ChartDisplayInfo.swift
//  MapDemo
//
//  Created by Standaard on 08/11/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation


public class DKW2ChartDisplayInfo {
	
	var calibratedChart :DKW2CalibratedChart
	var chartsHidden :[DKW2ChartDisplayInfo?]    // Charts that are completely hidden by this chart
	var chartsOverlap :[DKW2ChartDisplayInfo?]  // Charts that are overlapping with this chart
	var cornerPoints :[Float]?                   // Corner points of the chart, which can be drawn immediately
	var CPtsInvalidate : Bool?                   // If true, corner points are invalid and need to be calculated again.
	var enabled :Bool               // If true, this chart will be drawn
	var filledArea :Double          // Fraction of the viewer that is covered by this chart (0..1)
	var hasHole :Bool               // True if the chart has a hole
	var hiddenCalculated :Bool      // If true, this chart has been calculated for hidden charts
	var hideBorder :Bool            // If true, the chart border will be hidden
	var overlapCalculated :Bool     // If true, this chart has been calculated for overlapped charts
	private var inViewer :Bool              // If true, this chart has been enabled in the chart manager
	private var moduleInViewer :Bool        // If true, the module (which is the chartset) to which this chart belongs has been enabled
	var transparentColor :UInt32    // If mUseTransparenctColor is true, chart pixels of this color will be transparent.
	var useTransparentColor :Bool       // Use transparency for all charts in this module
	var validated :Bool?        // True, if chart needs to be loaded
	var zoom :Double             // Zoomfactor of the chart
	
	init(_ chart :DKW2CalibratedChart) {
		self.calibratedChart = chart
		self.chartsHidden = []
		self.chartsOverlap = []
		self.cornerPoints = nil
		self.enabled = false
		self.filledArea = 0.0
		self.hasHole = false
		self.hiddenCalculated = false
        // TODO: make setting in settings
//        self.hideBorder = true
        self.hideBorder = false
		self.overlapCalculated = false
		self.inViewer = true
		self.moduleInViewer = true
		self.transparentColor = 0
		self.useTransparentColor = false
		self.zoom = 0.0
	}
	
	//    func addChartHidden(StDKW2ChartDisplayInfo aHiddenChart) {
	//    mChartsHidden.add(aHiddenChart)
	//    }
	//
	//    func addChartOverlap(StDKW2ChartDisplayInfo aChartOverlap) {
	//    mChartsOverlap.add(aChartOverlap)
	//    }
	//
	//    func clearChartsHidden() {
	//    mChartsHidden.clear()
	//    }
	//
	//    func clearChartsOverlap() {
	//    mChartsOverlap.clear()
	//    }
	//
	//    func StDKW2CalibratedChart getChart() {
	//    return mChart
	//    }
	
	    func getChartInViewer() -> Bool {
			return inViewer
	    }
	
	//    func StDKW2ChartDisplayInfo getChartsHidden(int aIndex) {
	//    return mChartsHidden.get(aIndex)
	//    }
	//
	//    func int getChartsHiddenCount() {
	//    return mChartsHidden.size()
	//    }
	//
	//    func float[] getCornerPoints() {
	//    return mCornerPoints
	//    }
	//
	//    func boolean getCPtsInvalidate() {
	//    return mCPtsInvalidate
	//    }
	//
	//    func boolean getEnabled() {
	//    return mEnabled
	//    }
	//
	//    func double getFilledArea() {
	//    return mFilledArea
	//    }
	//
	//    func boolean getHasHole() {
	//    return mHasHole
	//    }
	//
	//    func boolean getHiddenCalculated() {
	//    return mHiddenCalculated
	//    }
	
    func getHideBorder() -> Bool {
        return hideBorder
    }
	
	func getInViewer() -> Bool {
		return moduleInViewer && inViewer
	}
	
	func getModuleInViewer() -> Bool {
		return moduleInViewer
	}
	
	//    func boolean getOverlapCalculated() {
	//    return mOverlapCalculated
	//    }
	//
	//    func StDKW2ChartDisplayInfo getChartOverlap(int aIndex) {
	//    return mChartsOverlap.get(aIndex)
	//    }
	//
	//    func int getChartOverlapCount() {
	//    return mChartsOverlap.size()
	//    }
	//
	//    func boolean getChartOverlaps(StDKW2ChartDisplayInfo aChart) {
	//    return mChartsOverlap.contains(aChart)
	//    }
	
    func getShowBorder() -> Bool {
        return !hideBorder
    }
	
	//    func int getTransparentColor() {
	//    return mTransparentColor
	//    }
	//
	//    func boolean getUseTransparentColor() {
	//    return mUseTransparentColor
	//    }
	//
	func getValidated() -> Bool {
		return validated ?? false
	}
	//
	//    func double getZoom() {
	//    return mZoom
	//    }
	//
	//    func void invalidateCPts() {
	//    mCPtsInvalidate = true
	//    }
	
	func setCornerPoints(_ points :[Float]) {
		cornerPoints = points
		CPtsInvalidate = true
	}
	
	//    func void setEnabled(boolean aEnabled) {
	//    mEnabled = aEnabled
	//    }
	//
	//    func void setHiddenCalculated(boolean aHiddenCalculated) {
	//    mHiddenCalculated = aHiddenCalculated
	//    }
	
	/**
	* Enables the chart in the chart viewer.
	* <p>Note: The module this chart is in should also be set to visible, before the chart will be shown in the viewer.
	*
	* @param aInViewer True, if the chart should be enabled.
	* @see #setModuleInViewer(boolean)
	*/
	func setInViewer(_ aInViewer :Bool) {
		if inViewer != aInViewer {
			inViewer = aInViewer
			DKW2ChartManager.getChartManager().setChartVisibilityChanged(aChanged: true)
		}
	}
	
	/**
	* Enables the module this chart is in in the chart viewer.
	* <p>Note: The chart should also be set to visible, before the chart will be shown in the viewer.
	*
	* @param aModuleInViewer True, if the module this chart is in should be enabled.
	* @see #setInViewer(boolean)
	*/
	func setModuleInViewer(_ aModuleInViewer :Bool) {
		if moduleInViewer != aModuleInViewer {
			moduleInViewer = aModuleInViewer
			DKW2ChartManager.getChartManager().setChartVisibilityChanged(aChanged: true)
		}
	}
	
	//    func void setOverlapCalculated(boolean aOverlapCalculated) {
	//    mOverlapCalculated = aOverlapCalculated
	//    }
	
    func setShowBorder(_ aShowBorder :Bool) {
        hideBorder = !aShowBorder
    }
	
	//    func void setTransparentColor(int aColor) {
	//    mTransparentColor = aColor
	//    }
	//
	//    func void setUseTransparentColor(boolean aUse) {
	//    mUseTransparentColor = aUse
	//    }
	
	func setValidated(aValidated :Bool) {
		validated = aValidated
	}
	
	//    func void setZoom(double Zoom) {
	//    mZoom = Zoom
	//    }
	//
	//    @Override
	//    func String toString() {
	//    return mChart.toString()
	//    }
}

