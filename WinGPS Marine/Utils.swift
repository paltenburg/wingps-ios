//
//  Utils.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import UIKit
import DeviceCheck
import MapKit

import UIKit.UIGestureRecognizerSubclass

// class to put general utility functions in

class StUtils {
	
	static func isDebug() -> Bool {
		if _isDebugAssertConfiguration() {
			return true
		}
		return false
	}
	
	static func getVersionString() -> String {
		return
			"\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "0").\(Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "0")"
	}
	
	static var compileDate :Date {
		let bundleName = Bundle.main.infoDictionary!["CFBundleName"] as? String ?? "Info.plist"
		if let infoPath = Bundle.main.path(forResource: bundleName, ofType: nil),
			let infoAttr = try? FileManager.default.attributesOfItem(atPath: infoPath),
			let infoDate = infoAttr[FileAttributeKey.creationDate] as? Date
		{ return infoDate }
		return Date()
	}
	
    static func formatDistance(_ distInM :Int?) -> String? {
        if let dist = distInM {
            return formatDistance(Double(dist))
        } else {
            return nil
        }
    }
    
	static func formatDistance(_ distInM :Double?) -> String? {
		if let distInM = distInM {
			return formatDistance(distInM: distInM, unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
		} else {
			return nil
		}
	}
	
	static func formatDistance(distInM :Double, unitLarge :String, unitSmall :String) -> String {

		var selectedUnit = unitLarge
		var value = unitConvert(valueDefaultUnit: distInM, targetUnit: unitLarge)
		if value < 1 {
			selectedUnit = unitSmall
			value = unitConvert(valueDefaultUnit: distInM, targetUnit: selectedUnit)
		}
		
		return "\(String(format: "%.1f", value)) \(selectedUnit.splitToStringArray(by: " ")[0])"
	}
	
	static func formatDistanceValueOnly(distInM :Double, unitLarge :String, unitSmall :String) -> String {
		
		var value = unitConvert(valueDefaultUnit: distInM, targetUnit: unitLarge)
		if value < 1 {
			value = unitConvert(valueDefaultUnit: distInM, targetUnit: unitSmall)
		}
		
		return String(format: "%.1f", value)
	}
	
	static func formatSpeed(_ speed :Double?) -> String? {
		guard let speed = speed else { return nil }
		return "\(String(format: "%.1f", (speed * Double(Units.mSpeedConversion)))) \(Units.mSogUnit)"
	}
	
	static func formatTimeInterval(_ duration: Int?) -> String? {
		guard let duration = duration else { return nil }
		
		let interval = TimeInterval(duration)
		let formatter = DateComponentsFormatter()
		formatter.allowedUnits = [.day, .hour, .minute, .second]
		formatter.unitsStyle = .abbreviated
		formatter.maximumUnitCount = 1
		return formatter.string(from: interval)!
	}
	
	static func getUnitForDistance(distInM :Double, unitLarge :String, unitSmall :String) -> String {
		
		var selectedUnit = unitLarge
		if unitConvert(valueDefaultUnit: distInM, targetUnit: unitLarge) < 1 {
			selectedUnit = unitSmall
		}
		return selectedUnit
	}
	
	static func getUnitStringForDistance(distInM :Double, unitLarge :String, unitSmall :String) -> String {
		return getUnitForDistance(distInM: distInM, unitLarge: unitLarge, unitSmall: unitSmall)
			.splitToStringArray(by: " ")[0]
	}
	
	// Convert value from default (si) units
	static func unitConvert(valueDefaultUnit :Double, targetUnit :String) -> Double {
		switch targetUnit {
			
		// Distance units: conversion is from meters
		case Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER:
			return valueDefaultUnit
		case Resources.SETTINGS_UNIT_DISTANCE_SMALL_YARD:
			return valueDefaultUnit / (0.0254 * 12 * 3) // 1 inch = 0.0254 m, 1 yard = 12 * 3 inches
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_KM:
			return valueDefaultUnit / 1000
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_MILE:
			return valueDefaultUnit / (1760 * (0.0254 * 12 * 3)) // 1 (statute) mile = 1609.344 m = 1760 yards
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_NMILE:
			return valueDefaultUnit / 1853 // 1 nautical mile = 1852.0 m

		default:
			return Double.nan
		}
	}

	// Convert value between differentUnits
	static func unitConvert(value :Double, fromUnit :String, targetUnit :String) -> Double {
		var valueM :Double = 0
		
		switch fromUnit {
		case Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER:
			valueM = value
		case Resources.SETTINGS_UNIT_DISTANCE_SMALL_YARD:
			valueM = value * (0.0254 * 12 * 3) // 1 inch = 0.0254 m, 1 yard = 12 * 3 inches
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_KM:
			valueM = value * 1000
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_MILE:
			valueM = value * (1760 * (0.0254 * 12 * 3)) // 1 (statute) mile = 1609.344 m = 1760 yards
		case Resources.SETTINGS_UNIT_DISTANCE_LARGE_NMILE:
			valueM = value * 1853 // 1 nautical mile = 1852.0 m
		default:
			return Double.nan
		}
		
		return unitConvert(valueDefaultUnit: valueM, targetUnit: targetUnit)
	}
	
	
	// Quick way to an item by a value in an array of sorted values
	
//	static func findIbyV(double[] arr, double v) -> Int {
//	// find index where arr[index] <= v and arr[index+1] > v;
//	// increasing values assumed
//	// or return -1 or arr.length - 1
//	int rangeSt = -1;
//	int rangeEnd = arr.length;
//	int rangeLength = rangeEnd - rangeSt;
//	while(rangeLength > 1){
//	int i = rangeSt + rangeLength / 2;
//	if (arr[i] > v)
//	rangeEnd = i;
//	else
//	rangeSt = i;
//	rangeLength = rangeEnd - rangeSt;
//	}
//	return rangeSt;
	
	// specific to finding annotations by latitude
	static func findIbyV(arr :[MKAnnotation], v :Double) -> Int {
		// find index where arr[index] <= v and arr[index+1] > v;
		// increasing values assumed
		// or return -1 or arr.length - 1
		var rangeSt :Int = -1
		var rangeEnd :Int = arr.count
		var rangeLength :Int = rangeEnd - rangeSt
		while rangeLength > 1 {
			var i :Int = rangeSt + rangeLength / 2
			if arr[i].coordinate.latitude > v {
				rangeEnd = i
			} else {
				rangeSt = i
			}
			rangeLength = rangeEnd - rangeSt
		}
		return rangeSt;
	}
	
//	/* Standardises and angle to [-180 to 180] degrees */
//	private static func standardAngle(_ angle :CLLocationDegrees) -> CLLocationDegrees {
//		let truncAngle = angle.truncatingRemainder(dividingBy: 360)
//		return truncAngle < -180 ? -360 - truncAngle : truncAngle > 180 ? 360 - 180 : truncAngle
//	}
//
//	/* confirms that a region contains a location */
//	static func regionContains(region: MKCoordinateRegion, coord: CLLocationCoordinate2D) -> Bool {
//		let deltaLat = abs(standardAngle(region.center.latitude - coord.latitude))
//		let deltalong = abs(standardAngle(region.center.longitude - coord.longitude))
//		return region.span.latitudeDelta >= deltaLat && region.span.longitudeDelta >= deltalong
//	}
	
	/* confirms that a region contains a location */
	static func mapRectGeoContains(rectGeo: CGRect, coord: CLLocationCoordinate2D) -> Bool {
		var pointInRect = rectGeo.contains(CGPoint(x: coord.longitude, y: coord.latitude))
		if pointInRect {
			return true
		} else {
			if rectGeo.maxX > 180 {
				return rectGeo.contains(CGPoint(x: coord.longitude + 360, y: coord.latitude))
			}
		}
		return false
	}
	
	static func getLanguageISO() -> String {
		let locale = Locale.current
		guard let languageCode = locale.languageCode,
			let regionCode = locale.regionCode else {
				return ""
		}
		return languageCode + "_" + regionCode
	}
	
	static func currentTimeMillis() -> Int64 {
		return Int64(Date().timeIntervalSince1970) * 1000
	}
        
    static func systemUptimeMillis() -> Int64 {
        return Int64(Foundation.ProcessInfo.processInfo.systemUptime * 1000.0)
    }
    
//    static func getFirstRunAfterInstall() -> Bool {
//        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
//        if launchedBefore  {
//            print("Function has launched before.")
//            return false
//        } else {
//            print("First launch, setting UserDefault.")
//            UserDefaults.standard.set(true, forKey: "launchedBefore")
//            return true
//        }
//    }
    
    static func notLoggedInYetAfterInstall() -> Bool {
        let loggedInYet = UserDefaults.standard.bool(forKey: "loggedInAtLeastOnce")
        if loggedInYet  {
            print("User has logged in before")
            return false
        } else {
            print("User has not logged in once.")
            return true
        }
    }
    
    static func setLoggedInYetAfterInstall() {
        if UserDefaults.standard.bool(forKey: "loggedInAtLeastOnce") == false {
            UserDefaults.standard.set(true, forKey: "loggedInAtLeastOnce")
        }
    }
    
    
    static func getUrlByFilename(_ filename :String) -> URL {

        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let URL = documentsDirectory.appendingPathComponent(filename)

        return URL
    }
    
    static func TDateTimeToDate(TDateTime :Double) -> Date? {
//        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
//        cal.set(1899, 11, 30, 0, 0); // init delphi version of start of time
//
//        cal.add(Calendar.DATE, (int)TDateTime);  // add in the days
////        cal.add(Calendar.MINUTE, (int)((60 * 24) * (TDateTime - (int)TDateTime))); // add the minutes
//        cal.add(Calendar.SECOND, (int)((60 * 60 * 24) * (TDateTime - (int)TDateTime))); // add the seconds
//
//        return cal.getTime();
//
        
        
//        var currentDate = Date()
//        let calendar = Calendar.current
//
//        // Specify date components
//        var dateComponents = DateComponents()
//        dateComponents.year = calendar.component(.year, from: currentDate)
//        dateComponents.month = bti(string: string, start: 274, upTo: 278)
//        dateComponents.day = bti(string: string, start: 278, upTo: 283)
//        dateComponents.timeZone = TimeZone.current
//        dateComponents.hour = bti(string: string, start: 283, upTo: 288)
//        dateComponents.minute = bti(string: string, start: 288, upTo: 294)
//
//        // Create date from components
//        let newDate = calendar.date(from: dateComponents)

        
        
        var dateComponents = DateComponents()
        dateComponents.timeZone = TimeZone(abbreviation: "GMT")
        // init delphi version of start of time
        dateComponents.year = 1899
        dateComponents.month = 12
        dateComponents.day = 30
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        dateComponents.nanosecond = 0
        
        // Create date from components
        let userCalendar = Calendar.current // user calendar
        let zeroDate = userCalendar.date(from: dateComponents)!
        
        dateComponents = DateComponents()
        dateComponents.day = Int(TDateTime)
        guard let daysDate = userCalendar.date(byAdding: dateComponents, to: zeroDate) else { return nil }
        
        dateComponents = DateComponents()
        dateComponents.second = Int(Double(60 * 60 * 24) * Double(TDateTime - Double(Int(TDateTime))))
        let date = userCalendar.date(byAdding: dateComponents, to: daysDate)
        
//        return date
        
        return date!
        
//        if let date = date {
//            return Int64(date.timeIntervalSince1970 * 1000)
//        } else {
//            return -1
//        }
        
//        lastUpdate = Int64(Date().timeIntervalSince1970 * 1000)

    }
    
    static func radToDegE6(_ aRad :Double) -> Int {
        return Int(aRad * (180.0 / Double.pi) * 1E6)
    }
}


class StUUID {
    var data1 = UInt32()
    var data2 = UInt16()
    var data3 = UInt16()
    var data4 = [UInt8]()
	
	func toString() -> String? {
		var s = ""
		s.append(String(self.data1))
		s.append(String(self.data2))
		s.append(String(self.data3))
		for i in 0 ..< 8 {
			s.append(String(self.data4[i]))
		}
		return s
		
	}
	
	func equals(guid :StUUID) throws -> Bool {
		return (guid.data1 == self.data1) && (guid.data2 == self.data2) && (guid.data3 == self.data3) && (guid.data4 == self.data4)
	}
}

func readGuid(from buffer :NSData, start :Int) throws -> StUUID {
	var uuid = StUUID()
	
	uuid.data1 = try getUInt32FromNSDataBuffer(buffer: buffer, start: start);
	uuid.data2 = try getUShortFromNSDataBuffer(buffer: buffer, start: start+4)
	uuid.data3 = try getUShortFromNSDataBuffer(buffer: buffer, start: start+8)
	
	uuid.data4 = [UInt8]()
	for _ in 0 ..< 8 {
		uuid.data4.append(try getUByteFromNSDataBuffer(buffer: buffer, start: start));
	}
	return uuid
}

// guid size: 16 bytes
func readGuid(from buffer :NSData, at pos :inout Int) throws -> StUUID {
    var uuid = StUUID()
    
    uuid.data1 = try getUInt32FromNSDataBuffer(buffer: buffer, start: pos); pos += 4
    uuid.data2 = try getUShortFromNSDataBuffer(buffer: buffer, at: &pos)
    uuid.data3 = try getUShortFromNSDataBuffer(buffer: buffer, at: &pos)
    
    uuid.data4 = [UInt8]()
    for _ in 0 ..< 8 {
        uuid.data4.append(try getUByteFromNSDataBuffer(buffer: buffer, start: pos)); pos += 1
    }
    return uuid
}


func checksum32(buffer: NSData, count: Int) throws -> UInt32 {
    var Sum: UInt32 = 0
    let ItemCount = count / 4
    
    var Position = 0
    for _ in (0..<ItemCount).reversed() {
        let P: UInt32 = try getUInt32FromNSDataBuffer(buffer: buffer, start: Position)
        Sum ^= P
        Position += 4
    }
    
    var bits: UInt32 = 0
    var outSum: UInt32 = 0
    for i in 0 ..< 4 {
        var B2: UInt8 = UInt8(truncatingIfNeeded: Sum >> bits)
        
        if (i < count % 4){
            let B1: UInt8 = try getUByteFromNSDataBuffer(buffer: buffer, start: Position)
            B2 ^= B1
        }
        
        outSum |= (UInt32(B2) << bits)
        
        bits += 8
        Position += 1
    }
    
    return outSum
}

func getUByteFromNSDataBuffer(buffer: NSData, start: Int) throws -> UInt8 {
    var Data: UInt8 = 0
	try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 1))
    return Data
}

func getUByteFromNSDataBuffer(buffer: NSData, at position :inout Int) throws -> UInt8 {
    let value = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
    position += 1
    return value
}

extension NSData {
    func get(_ pos :inout Int) throws -> UInt8 {
        return try getUByteFromNSDataBuffer(buffer: self, at: &pos)
    }
}

func getShortFromNSDataBuffer(buffer: NSData, start: Int) throws -> Int16 {
    var Data: Int16 = 0
    try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 2))
    return Data
}

func getShortFromNSDataBuffer(buffer: NSData, at position :inout Int) throws -> Int16 {
    let value = try getShortFromNSDataBuffer(buffer: buffer, start: position)
    position += 2
    return value
}

extension NSData {
    func getShort(_ pos :inout Int) throws -> Int16 {
        return try getShortFromNSDataBuffer(buffer: self, at: &pos)
    }
}

func getUShortFromNSDataBuffer(buffer: NSData, start: Int) throws -> UInt16 {
	var Data: UInt16 = 0
	try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 2))
	return Data
}

func getUShortFromNSDataBuffer(buffer: NSData, at position :inout Int) throws -> UInt16 {
	let value = try getUShortFromNSDataBuffer(buffer: buffer, start: position)
    position += 2
    return value
}


func getInt32FromNSDataBuffer(buffer: NSData, start: Int) throws -> Int32 {
    var Data: Int32 = 0
    try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 4))
    return Data
}

func getInt32FromNSDataBuffer(from buffer: NSData, at position :inout Int) throws -> Int32 {
    let value = try getInt32FromNSDataBuffer(buffer: buffer, start: position)
    position += 4
    return value
}

extension NSData {
    func getInt(_ pos :inout Int) throws -> Int32 {
        return try getInt32FromNSDataBuffer(from: self, at: &pos)
    }
}

func getInt64FromNSDataBuffer(from buffer: NSData, start: Int) throws -> Int64 {
    var Data: Int64 = 0
    try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 8))
    return Data
}

func getUInt32FromNSDataBuffer(buffer: NSData, start: Int) throws -> UInt32 {
    var Data: UInt32 = 0
    try SafeData(buffer).getBytes(&Data, range: NSRange(location: start, length: 4))
    return Data
}

func getUInt32FromNSDataBuffer(buffer :NSData, at position :inout Int) throws -> UInt32 {
    let value = try getUInt32FromNSDataBuffer(buffer: buffer, start: position)
    position += 4
    return value
}


extension NSData {
    func getUInt(_ pos :inout Int) throws -> UInt32 {
        return try getUInt32FromNSDataBuffer(buffer: self, at: &pos)
    }
}

func getDoubleFromNSDataBuffer(buffer: NSData, start :Int) throws -> Double {
	var value: Double = 0
	try SafeData(buffer).getBytes(&value, range: NSRange(location: start, length: 8))
	return value
}

func getDoubleFromNSDataBuffer(from buffer :NSData, at position :inout Int) throws -> Double {
    var value :Double = 0
    try SafeData(buffer).getBytes(&value, range: NSRange(location: position, length: 8))
    position += 8
    return value
}

extension NSData {
    func getDouble(_ pos :inout Int) throws -> Double {
        return try getDoubleFromNSDataBuffer(from: self, at: &pos)
    }
}

func getFloatFromNSDataBuffer(buffer :NSData, start: Int) throws -> Float {
	var value :Float = 0
	try SafeData(buffer).getBytes(&value, range: NSRange(location: start, length: 4))
	return value
}

extension NSData {
    func getFloat(_ pos :inout Int) throws -> Float {
        var value = try getFloatFromNSDataBuffer(buffer: self, start: pos)
        pos += 4
        return value
    }
}

func getStringFromNSDataBuffer(buffer :NSData, position :Int, length :Int) throws -> String {
	let str = String(data: buffer.subdata(with: NSRange(location: position, length: length)) as Data, encoding: String.Encoding.isoLatin1)
	return str!
}

func getLStringFromNSDataBuffer(buffer :NSData, position :Int, length :Int) throws -> String {
	let str = String(data: buffer.subdata(with: NSRange(location: position, length: length)) as Data, encoding: String.Encoding.utf16LittleEndian)
	return str!
}

func getDate2000FromNSDataBuffer(buffer :NSData, start :Int) throws -> Date {
	let secondsIn2000 = 31622400
	let seconds = try getInt32FromNSDataBuffer(buffer: buffer, start: start)
	let date = Date(timeIntervalSinceReferenceDate: Double(seconds - Int32(secondsIn2000)))
	return date
}



// MARK: Streaming functions

func readStrA(buffer: NSData, at position: inout Int) throws -> String {
    let len = Int(UInt32(try getUInt32FromNSDataBuffer(buffer: buffer, start: position)))
    position += 4
    let str = String(data: buffer.subdata(with: NSRange(location: position, length: len)) as Data, encoding: String.Encoding.isoLatin1)
    position += len
    // testen met niet-lege strings
    
    return str!
}

/**
 * Reads an UTF16 string saved as an array of 2 byte wchar_t characters from Visual C++.
 *
 * @param aBuffer The byte buffer from which to read the string.
 * @return The java string.
 */

extension NSData {
    func readStrW(_ pos :inout Int) throws -> String {
        do {
            var length :UInt64 = try UInt64(self.getUInt(&pos)) * 2
//            var Src :UInt8 = [UInt8]() //new byte[(int)length];
            //aBuffer.get(Src);
            let str = String(data: self.subdata(with: NSRange(location: pos, length: Int(length))) as Data, encoding: String.Encoding.utf16LittleEndian)
            pos += Int(length)
            return str ?? ""
        } catch {
            return ""
        }
    }
}

func readUIntBuff(fb :NSData, pos :inout Int) throws -> Int64{
    return Int64(try fb.getInt(&pos))
 }


// MARK: File functions

func getAppBundleDKW2Directory() -> URL {
//    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//    let documentsDirectory = paths[0]
//    return documentsDirectory

	return Bundle.main.bundleURL.appendingPathComponent("dkw2files")
}

func getDKW2Directory() -> URL {
	let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

	let dkw2Directory = documentsDirectory.appendingPathComponent("DKW2")
	//
	// make sure it exists
	if !dirExists(dkw2Directory.path) {
		do	{
			try FileManager.default.createDirectory(atPath: dkw2Directory.path, withIntermediateDirectories: true, attributes: nil)
		} catch let error as NSError	{
			NSLog("Unable to create directory \(error.debugDescription)")
			////// handle error
		}
	}

	return dkw2Directory
}


func getFileSize(_ path :String) -> UInt64? {
    
	do {
		let attr = try FileManager.default.attributesOfItem(atPath: path)
		return attr[FileAttributeKey.size] as? UInt64
	} catch {
		return nil
	}
}

func getLastModified(_ path :String) -> Date? {
    
    var time :Date?
    do {
        //return [FileAttributeKey : Any]
        let attr = try FileManager.default.attributesOfItem(atPath: path)
        time = attr[FileAttributeKey.modificationDate] as? Date
        
//        //if you convert to NSDictionary, you can get file size old way as well.
//        let dict = attr as NSDictionary
//        fileSize = dict.fileSize()
    } catch {
//        print("Error: \(error)")
		return nil
    }
    
    return time
}

func dirExists(_ path :String) -> Bool {
	var isDir: ObjCBool = false
	let fileExists = FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
	return fileExists && isDir.boolValue
}

func degToRad(_ deg :Double) -> Double {
	return deg * Math.DEGTORAD
}

func radToDeg(_ rad :Geo2) -> Geo2 {
	let deg = Geo2()
	deg.lat = rad.lat * (180 / .pi)
	deg.lon = rad.lon * (180 / .pi)
	return deg
}


func getSafe(from array :[UInt32], at pos :Int) throws -> UInt32 {
	if pos < 0 || pos >= array.count {
		throw NSError()
	}
	return array[pos]
}

func setSafe(from array :inout [UInt32], at pos :Int, value :UInt32) throws {
	if pos < 0 || pos >= array.count {
		throw NSError()
	}
	array[pos] = value
}


func getStVersionFromNSDataBuffer(buffer: NSData, start position :Int) throws -> StVersion {
	let v = try getUInt32FromNSDataBuffer(buffer: buffer, start: position)
	//print(String(v))
	var version = StVersion(v)
	
	return version
}

func getAvailableSpace(fileUrl :URL?) -> Int64? {
	
	var url :URL
	if let fileUrl = fileUrl, FileManager.default.fileExists(atPath: fileUrl.path) {
		url = fileUrl
	} else {
		url = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!)
	}

	if #available(iOS 11.0, *) {
		do {
			let values = try URL(fileURLWithPath: url.path).resourceValues(forKeys: [.volumeAvailableCapacityForImportantUsageKey])
			if let capacity = values.volumeAvailableCapacityForImportantUsage {

				print("Available capacity for important usage: \(capacity)")
				return capacity
			} else {
				print("Capacity is unavailable")
			}
		} catch {
			print("Error retrieving capacity : \(error.localizedDescription)")
		}
	} else {
		guard
			let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: url.path),
			let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
			else {
				// something failed
				return nil
		}
		return freeSize.int64Value
	}
	return nil
}

func isIpad() -> Bool {
	return UIDevice.current.model == "iPad"
}

func getDeviceToken(handler :@escaping (String)->()) -> Bool {
    if Resources.passDeviceCheck {
        handler(Resources.PASS_DEVICE_CHECK)
        return true
    }
    
	if #available(iOS 11.0, *) {
		let currentDevice = DCDevice.current
		if currentDevice.isSupported {
			currentDevice.generateToken(completionHandler: { (data, error) in
				if let tokenData = data {
					print("getDeviceToken: \(tokenData)")
					
					handler(tokenData.base64EncodedString())

				} else {
					print("getDeviceToken Error: \(error?.localizedDescription ?? "")")
					handler("")
				}
			})
            return true
        } else {
            print("getDeviceToken Error: currentDevice.isSupported == false ")
        }
		return false
		
	} else {
		print("Error: iOS < 11.0 not supported for device check")
	}
	return false
}

public func optIntToOptInt64(_ i : Int?) -> Int64? {
	if let i = i {
		return Int64(i)
	}
	return nil
}

//MARK: JSON helper functions

public func toOptInt(_ value :Any?) -> Int? {
	if let int = value as? Int {
		return int
	}
	if let str = value as? String {
		return Int(str)
	}
	return nil
}

//MARK: Viewcontroller functions

public func getTopViewController() -> UIViewController? {
	var rootController :UIViewController?
	if !Thread.isMainThread {
		DispatchQueue.main.sync{
			rootController = UIApplication.shared.keyWindow?.rootViewController
		}
	} else {
		rootController = UIApplication.shared.keyWindow?.rootViewController
	}
	if var topController = rootController {
		while let presentedViewController = topController.presentedViewController {
			topController = presentedViewController
		}
		return topController
	}
	return nil
	
}

//MARK: Debug functies

public enum DebugTag {
	case def
	case wp
	case tiles
}

class debug {
	static let tagsToShow :[DebugTag] = [
		.wp,
		.def
//		.
//		.tiles
	]
    
    static var doutAppStartTime :Int64 = StUtils.systemUptimeMillis()

}

let disableAllOutput :Bool = false

public func dout(_ tag :DebugTag, _ m: Any...){
	if disableAllOutput { return }
	
	if debug.tagsToShow.contains(tag) {
		var linkedString = ""
//		for item in m {
//			if let str = item as? String { linkedString += str + " " }
//			if let strArray = item as? [String]
//			{
//				for arrayItem in strArray {
//					linkedString += arrayItem + "; "
//				}
//			}
//			if let str = item as? Int { linkedString += String(str) + " " }
//			if let str = item as? Float { linkedString += String(str) + " " }
//			if let str = item as? Double { linkedString += String(str) + " " }
//		}
		extendLinkedString(linkedString: &linkedString, thing: m)
		
		// Indent printout by how deep in call stack
		var csLevel = Thread.callStackSymbols.count
		var indentation :String = ""
		for _ in 0 ..< csLevel {
			indentation += " "
		}
        
        var timestamp = (StUtils.systemUptimeMillis() - debug.doutAppStartTime)
        var timestampString = "\(timestamp / 1000).\((timestamp % 1000) / 10)"
		print(timestampString + indentation + linkedString)
	}
}

func extendLinkedString(linkedString :inout String, thing :Any){
	if let itemArray = thing as? [Any] {
		for item in itemArray {
			extendLinkedString(linkedString: &linkedString, thing: item)
		}
	} else {
		print(thing, terminator: "", to: &linkedString)
	}
}

public func dout(_ items: Any...){
	if disableAllOutput { return }

	dout(.def, items)
}

func degreesToRadians(_ degrees: Double) -> Double { return degrees * .pi / 180.0 }

func radiansToDegrees(_ radians: Float) -> Float { return radians * 180.0 / .pi }

func radiansToDegrees(_ radians: Float?) -> Float? { return radians != nil ? radians! * 180.0 / .pi : nil }

func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / .pi }

func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
	
	let lat1 = degreesToRadians(point1.coordinate.latitude)
	let lon1 = degreesToRadians(point1.coordinate.longitude)
	
	let lat2 = degreesToRadians(point2.coordinate.latitude)
	let lon2 = degreesToRadians(point2.coordinate.longitude)
	
	let dLon = lon2 - lon1
	
	let y = sin(dLon) * cos(lat2)
	let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
	let radiansBearing = atan2(y, x)
	
	return radiansToDegrees(radiansBearing)
}

//class RoutePointTapGestureRecognizer :UILongPressGestureRecognizer {
class RoutePointTapGestureRecognizer :UIGestureRecognizer {

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
		super.touchesBegan(touches, with: event)

		if let editRoute = Routes.service.overlay?.editRoute,
			let mapView = ViewController.instance?.mapView,
			let routeOverlay = ViewController.instance?.routeOverlay {

			for touch in touches {
				var tappedPoint = touch.location(in: ViewController.instance?.mapView)
				let location = mapView.convert(tappedPoint, toCoordinateFrom: mapView)
				if routeOverlay.touchingRoutePoint(point: tappedPoint, inRoute: editRoute) != nil {
					state = .began
				} else {
					state = .failed
				}
				return
			}
		}
		state = .failed
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent) {
		dout("RoutePointTapGestureRecognizer touchesEnded")
		
		super.touchesEnded(touches as! Set<UITouch>, with: event)
		state = .ended
	}
}

//class StoryBoardSegueQuickFading :UIStoryboardSegue{
//	private var selfRetainer: StoryBoardSegueQuickFading? = nil
//
//	override func perform(){
//		destination.transitioningDelegate = self as! UIViewControllerTransitioningDelegate
//		selfRetainer = self
//		destination.modalPresentationStyle = .overCurrentContext
//		source.present(destination, animated: true, completion: nil)
//	}
//}

public extension MKMultiPoint {
    var coordinates: [CLLocationCoordinate2D] {
        var coords = [CLLocationCoordinate2D](repeating: kCLLocationCoordinate2DInvalid,
                                              count: pointCount)

        getCoordinates(&coords, range: NSRange(location: 0, length: pointCount))

        return coords
    }
}
