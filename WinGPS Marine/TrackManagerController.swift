//
//  RouteManagerController.swift
//  WinGPS Marine
//
//  Created by Standaard on 04/10/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

@objc class TrackManagerController :UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var backgroundView: UIView!
    
    @IBOutlet weak var viewContainer: UIView!
    
	@IBOutlet weak var bottomView: UIView!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	var trackTableCells = [TrackTableCell]()

	var selectedIndexPath :IndexPath = IndexPath(row: -1, section: 0)

	@IBAction func closeButton(_ sender: Any) {
		closeDialog()
	}

	fileprivate func closeDialog() {
//		self.dismiss(animated: true, completion: {()-> Void in
			self.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
//		})

	}
	
	override func viewWillAppear(_ animated: Bool) {
		tableView.delegate = self
		tableView.dataSource = self
	}
	
	override func viewDidLoad() {
		viewContainer.layer.cornerRadius = 8
		viewContainer.clipsToBounds = true
        
        // set cell switches
//        cell.trackEnabledSwitchOutlet.setOn(on: enabled, animated: true)

	}
	
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if touch?.view == backgroundView {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func setEnabled(_ cell :TrackTableCell, enabled :Bool){
//        cell.trackEnabledSwitchOutlet.setOn(on: enabled, animated: true)
        
        if let indexPath = tableView.indexPath(for: cell) {
            var track = Tracks.service.tracks[indexPath.row]
            track.setVisible(enabled)
        }
        
    }
    
    
//	@IBAction func newRouteButtonAction(_ sender: UIButton) {
//		ViewController.instance?.createNewRouteInCenter()
//		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
//	}
}

extension TrackManagerController :UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		let nrOfTracks :Int = Tracks.service.tracks.count
		trackTableCells = [TrackTableCell](repeating: TrackTableCell(), count: nrOfTracks)
		return nrOfTracks
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		dout("cellForRowAt indexPath: ", indexPath.section, " ", indexPath.row)

		var tableCell :TrackTableCell
		var row = indexPath.row
		var track = Tracks.service.tracks[row]

//		if let oldTableCell = tableView.dequeueReusableCell(withIdentifier: "routeTableCell-\(indexPath.section)-\(indexPath.row)", for: indexPath) {
		if let oldTableCell = tableView.dequeueReusableCell(withIdentifier: "trackTableCell", for: indexPath) as? TrackTableCell {
			tableCell = oldTableCell
		} else {
			tableCell = TrackTableCell()
		}
		
		tableCell.controller = self
		tableCell.track = track
		
		tableCell.nameLabel.text = track.name
		//		tableCell.routeDateLabel.text = "-date-"
//        tableCell.dateLabel.text = track.locationsBySegment.first?.first?.timestamp.asString(style: .short) ?? ""
        tableCell.dateLabel.text = track.locationsBySegment.first?.first?.timestamp.asString() ?? ""
 tableCell.lengthLabel.text = StUtils.formatDistance(distInM: Double(track.length), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
//        tableCell.durationLabel.text = "0 s"
//        if let firstTimestamp = track.locationsBySegment.first?.first?.timestamp,
//           let lastTimestamp = track.locationsBySegment.last?.last?.timestamp {
//            tableCell.durationLabel.text = StUtils.formatTimeInterval( Int(round((lastTimestamp.timeIntervalSince( firstTimestamp  )))) )
//        }
        tableCell.durationLabel.text = StUtils.formatTimeInterval( Int(round(track.durationSeconds)) )

//		tableCell.routeNameLabelDetailView.text = tableCell.routeNameLabel.text
//		tableCell.dateLabelDetailView.text = tableCell.routeDateLabel.text
//		tableCell.lengthLabelDetailView.text = tableCell.routeLengthLabel.text
        
        tableCell.trackEnabledSwitchOutlet.setOn(track.getVisible(), animated: false)
        
        if selectedIndexPath == indexPath {
            tableCell.expandedView?.isHidden = false
//            tableCell.expandButtonImageView.isHidden = true
//            tableCell.minimizeButtonImageView.isHidden = false
        } else {
            tableCell.expandedView?.isHidden = true
        }
        
        
		trackTableCells[row] = tableCell
		
		return tableCell
	}
	
 
}

@objc extension TrackManagerController :UITableViewDelegate{
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		dout("didDeselectRowAt")
		
		tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
    }

	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		dout("didSelectRowAt selected: \(selectedIndexPath.row == indexPath.row)")

		var previouslySelected = selectedIndexPath
		if selectedIndexPath.row == indexPath.row {
			selectedIndexPath = IndexPath(row: -1, section: 0)
		} else {
			selectedIndexPath = indexPath
		}
		
        
//        trackCell.listItemView?.isHidden = false
//        trackCell.expandedView?.isHidden = true
//
//        expandOrCollapseCell(previouslySelected)
        if previouslySelected.row >= 0 {
            tableView.reloadRows(at: [previouslySelected], with: UITableView.RowAnimation.automatic)
        }
        
		if selectedIndexPath.row >= 0 {
//            expandOrCollapseCell(selectedIndexPath)

			tableView.reloadRows(at: [selectedIndexPath], with: UITableView.RowAnimation.automatic)
		}
        
        
	}
    
//    func expandOrCollapseCell(_ indexPath :IndexPath){
//        if let listItemCell = tableView.cellForRow(at: indexPath) as? TrackTableCell {
//
//            if indexPath == selectedIndexPath {
//                listItemCell.expandedView?.isHidden = false
//                tableView.reloadRows(at: [indexPath], with: .none)
//
////                listItemCell.expandButtonImageView.isHidden = true
////                listItemCell.minimizeButtonImageView.isHidden = false
//            } else {
//                listItemCell.expandedView?.isHidden = true
//                tableView.reloadRows(at: [indexPath], with: .none)
//
////                listItemCell.expandButtonImageView.isHidden = true
////                listItemCell.minimizeButtonImageView.isHidden = false
//            }
//        }
//    }
	
//	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//		let trackCell = trackTableCells[indexPath.row]
//
//		dout("heightForRowAt \(indexPath.section) \(indexPath.row) selected: \(selectedIndexPath.row == indexPath.row) listItemView: \(trackCell.listItemView != nil)")
//
//		if selectedIndexPath.row == indexPath.row {
//            trackCell.listItemView?.isHidden = true
//            trackCell.expandedView?.isHidden = false
//
//			return 155
//		} else {
//            trackCell.listItemView?.isHidden = false
//            trackCell.expandedView?.isHidden = true
//
//			return 50
//		}
//	}
}

// MARK: TrackTableCell

class TrackTableCell :UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var lengthLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    //    @IBOutlet weak var nameLabelDetailView: UILabel!
//    @IBOutlet weak var dateLabelDetailView: UILabel!
//    @IBOutlet weak var lengthLabelDetailView: UILabel!
    
    @IBOutlet weak var editNameButton: UIButton!
    
    @IBOutlet weak var listItemView: UIView!
    @IBOutlet weak var expandedView: UIView!

    @IBOutlet weak var trackEnabledSwitchOutlet: UISwitch!
    
    var controller :TrackManagerController?
    var track :Track?
    
    @IBAction func chartEnabledSwitchAction(_ sender: UISwitch) {
        controller?.setEnabled(self, enabled: sender.isOn)
    }
    
    @IBAction func editNamePressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Edit name".localized, message: nil, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil ))

        alert.addTextField(configurationHandler: { nameField in
            nameField.text = self.nameLabel.text
        })
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            
            guard let newName = alert.textFields?[0].text else { return }
            
            print("edited name: \(newName)")
            
//            self.track?.name = newName
            if let track = self.track {
                track.name = newName
                track.saveToGpxTrack()
            }
//            if self.track?.trackPointAnnotations.count ?? 0 >= 1 {
//                self.track?.trackPointAnnotations[0].title = self.track?.name ?? "" + "\n1"
//                self.track?.trackPointAnnotations[0].updateAnnotationView()
//                self.track?.needsSaving = true
//            }
            
            self.nameLabel.text = newName
//            self.routeNameLabelDetailView.text = newName
            
        }))
        
        getTopViewController()?.presentFromMain(alert, animated: true)
    }
    
    
    @IBAction func findButtonPressedAction(_ sender: UIButton) {
        
        if let firstLoc = track?.locationsBySegment.first?.first?.coordinate {
            ViewController.instance?.moveMapTo(coord: firstLoc, animated: false)
        }
        
        controller?.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
    }
    

    
    @IBAction func deleteTrackButtonPressedAction(_ sender: UIButton) {

        let alert = UIAlertController(title: "dialog_message_confirm_remove_track".localized, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
        alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { action in
            
            if let track = self.track {
                Tracks.service.overlay?.remove(track)
            }
            self.controller?.selectedIndexPath = IndexPath(row: -1, section: 0)
            self.controller?.tableView.reloadData()
            
        }))
        
        controller?.presentFromMain(alert, animated: true)

    }

}
