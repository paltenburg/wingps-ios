//
//  DownloadManagerController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 10/12/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

//class testTableViewController :UITableViewController{
//	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		return 3
//	}
//
//	override func tableView(_ tableView: UITableView,
//									cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//		let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableChartItem", for: indexPath)
//
//		cell.textLabel?.text = String(indexPath.row)
//		//		cell.detailTextLabel?.text = player.game
//		return cell
//	}
//}

// Bring up DownloadManagerView from code
func showDownloadManagerView(caller :StatusCheck?){
	// update UITableView
	
	let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
	
	let downloadManagerController = storyBoard.instantiateViewController(withIdentifier: "DownloadManagerID") as! DownloadManagerController
	
	if caller != nil {
		downloadManagerController.mStatusCheck = caller
	}
	
	ViewController.instance?.present(downloadManagerController, animated: true, completion: nil)
}

//	class AvailableChartSetsController :UITableViewController{
class DownloadManagerController :UIViewController, UITableViewDataSource, UITableViewDelegate{
	
	enum ProductState {
		case NotDownloaded // Product is available to download
		case Incomplete    // Product is activated but not complete
		case Complete      // Product is activated and complete
		case Downloading
		case Busy
	}
	
    class ChartSetItem {
        var itemType :Int?
        var pName :String?
        var mName :String?
        var moduleBitIndex :Int?
        var productID :Int64?
        var count :Int?
        var size :Int64?
        var isSub :Bool?
        //        var expiry :Int64?
        var isCurrent :Bool?
        var groupIndex :Int?
        var orderIndex :Int?
        var edition :Int?
        var state :ProductState?
    }
    
	//	private boolean mDualPane;
	var mStatusCheck :StatusCheck? = nil
	var mChartCheckSt :StChartCheckTask?
	var mChartCheckHandler : ((String, Int, String) -> Void)?
    var mCommand :String = ""
	var mErrorCode :Int? = nil
	var mActDB :StActManager = StActManager.getActManager()
	static var mCharts = [ChartSetItem]()
	//	private Activity mActivity = null;
	//	private Context mContext = null;
	//	private Resources mRes;
	//	private TextView mTextViewDesc;
	//	private TextView mTextViewShop;
	//	private AvailableChartSetsAdapter mAdapter;
	var mUser :String = ""
	var mPass :String = ""
	//	//private boolean mCheckPem;
	//	private ListView mListView;
	//	private boolean mInternet = true;
	//	private boolean mFirstDownload;
	//
	//	@Override
	//	public void onActivityResult(int RequestCode, int ResultCode, Intent data) {
	
	var mSelections = [Bool]()
	
	//	var indicator :UIActivityIndicatorView = UIActivityIndicatorView()
	
    var comingFromLogin :Bool = false
    
	@IBOutlet weak var productsTableView: UITableView!
	
	@IBOutlet var backgroundView: UIView!
	
	@IBOutlet weak var downloadViewContainer: UIView!
	
	@IBOutlet weak var bottomView: UIView!
	
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	@IBOutlet weak var toShopButton: UIButton!
	
	@IBAction func close(_ segue: UIStoryboardSegue) {
        comingFromLogin = false
	}
	
	//	@IBAction func downloadButton(_ sender: UIBarButtonItem) {
	//
	//		if !mSelections.isEmpty {
	//			var selectedIndexes = [Int]()
	//			for (i, e) in mSelections.enumerated() {
	//				if e { selectedIndexes.append(i) }
	//			}
	//			buttonDownloadClick(itemIndexes: selectedIndexes)
	//		}
	//	}
	//
	//	@IBAction func removeButton(_ sender: UIBarButtonItem) {
	//	}
	//
	//	@IBAction func chartSetItemDownloadButton(_ sender: UIButton) {
	//
	//	}
	
	@IBAction func closeButton(_ sender: Any) {
		closeDialog()
	}
	
	@IBAction func goToShopButton(_ sender: Any) {
		UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/en/?option=com_content&view=article&id=1947&Itemid=4")! as URL)
	}
	
	
	func callBackFromStatusCheck(){
		//		if (RequestCode == AppConstants.ACTIVITY_STATUSCHECKACCOUNT) {
		//			if (ResultCode == Activity.RESULT_OK) {
		//				boolean Success = data.getBooleanExtra("Success", false);
		//				if (Success) {
		//					String cdata = data.getStringExtra("Data");
		//					Intent resultIntent = new Intent();
		//					resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERRESULT, ChartManagerActivity.CHARTMANAGERRESULT_RELOADCHARTS);
		//					resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERDATA, cdata);
		//					getActivity().setResult(ChartManagerActivity.RESULT_OK, resultIntent);
		//					getActivity().finish();
		//				}
		//			}
		
		//		mCaller.callBackFromChartManager()
	}
	
	//		} else if (RequestCode == AppConstants.ACTIVITY_USERACCOUNT) {
	//			if (ResultCode == Activity.RESULT_OK) {
	//				String Data[] = data.getStringArrayExtra(UserAccountActivity.UARESULT);
	//				if (Data.length != 2 || Data[0].length() == 0 || Data[1].length() == 0) {
	//				} else {
	//					mTextViewDesc.setText(mRes.getString("availablesets_searching));
	//
	//					mUser = Data[0];
	//					mPass = Data[1];
	//
	//					mActDB = StActManager.getActManager();
	//					mActDB.setUserData(mUser, mPass);
	//
	//					getAvailableChartSets(mUser, mPass);
	//					//                    mCheckPem = true;
	//					//                    mChartCheckSt.checkPemFile();
	//				}
	//			} else {
	//				getActivity().finish();
	//			}
	//		} else if (RequestCode == AppConstants.ACTIVITY_SHOPACTIVITY) {
	//			getActivity().finish();
	//			//        } else if (RequestCode == AppConstants.INTENT_REQUEST_LOLLIPOP_SDCARD_PERMISSION) {
	//			//
	//			//            StDKW2Settings.getSettings(getActivity()).onSDCardPermissionResult(ResultCode, data);
	//		} else if (RequestCode == AppConstants.INTENT_REQUEST_LOLLIPOP_SDCARD_PERMISSION) {
	//			StDKW2Settings.getSettings(getActivity()).onSDCardPermissionResult(ResultCode, data);
	//		}
	//	}
	
	//	override convenience init(style: UITableViewStyle) {
	//		self.init(style: .plain)
	//	}
	
	//	required init?(coder aDecoder: NSCoder) {
	//		super.init(coder: aDecoder)
	//		//			fatalError("init(coder:) has not been implemented")
	//	}
	
	//	func onActivityCreated() {
	//	func manualInit(caller :StatusCheck?, user :String, pass: String) {
	//	override func viewDidLoad() {
	
	override func viewWillAppear(_ animated: Bool) {
		
		productsTableView.delegate = self
		productsTableView.dataSource = self
		
		
		// update chartset state of existing sets. Check if files are complete
		for csi in DownloadManagerController.mCharts {
			csi.state = checkChartSetState(csi: csi)
		}
		DownloadManagerController.sortChartsList()
		
		mActDB = StActManager.getActManager()
		
		if !mActDB.getUserDataAvailable() {
			return
		}
		
		mUser = mActDB.getUserMail()
		mPass = mActDB.getUserPass()
		
		//		// Check if info frame is available
		//		View infoFrame = getActivity().findViewById(R.id.fragments_ChartInfo);
		//		mDualPane = infoFrame != null;// && infoFrame.getVisibility() == View.VISIBLE;
		//
		//		mActivity = getActivity();
		//		if (mContext == null && mActivity != null)
		//		mContext = mActivity.getApplicationContext();
		//
		//		mRes = getActivity().getResources();
		//
		//		mListView = (ListView)getView().findViewById(R.id.lvAvailableSets);
		//		mTextViewShop = (TextView)getView().findViewById(R.id.tvAvailableSetsShopLink);
		//		mTextViewShop.setPaintFlags(mTextViewShop.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		//		mTextViewDesc = (TextView)getView().findViewById(R.id.tvAvailableSetsDesc);
		//
		//		mTextViewShop.setOnClickListener(new View.OnClickListener() {
		//		public void onClick(View v) {
		//		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mRes.getString("action_availablesets_link))));
		//		}
		//		});
		//
		//		mTextViewDesc.setText(mRes.getString("availablesets_searching));
		
		mChartCheckHandler = { (cmd, Msg, Data) -> Void in
            self.chartCheckHandleMessages(cmd: cmd, Msg: Msg, Data: Data)
		}
		//		try {
		mChartCheckSt = StChartCheckTask(aHandler: mChartCheckHandler)
		
		//		} catch (Exception e) {
		//			Log.e("AvailableChartSets", "Exception: " + e.getMessage());
		//		}
		//
		//		//mCheckPem = false;
		//		mFirstDownload = false;
		//
		//		//StDKW2ChartManager.getChartManager().getChartsInChartsDir();
		//
		////		mActDB = StActManager.getActManager()
		//		mCharts = new ArrayList<>();
		//
		//		mListView.setCacheColorHint(Color.TRANSPARENT);
		//		mListView.setOnItemClickListener(new OnItemClickListener() {
		//
		//			@Override
		//			public void onItemClick(AdapterView<?> aParent, View aView, int aPosition, long aId) {
		//				ActivityCompat.invalidateOptionsMenu(getActivity());
		//			}
		//		});
		//
		//		mAdapter = new AvailableChartSetsAdapter(getActivity(), R.layout.chartset_item);
		//		mListView.setAdapter(mAdapter);
		//		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		//
		//		mInternet = StUtils.checkNetworkConnection(getActivity());
		
		// disable table view while it is loading
		productsTableView.isHidden = true
		
		//		indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
		//		loadingIndicator.center = downloadViewContainer.center
		//		self.downloadViewContainer.addSubview(indicator)
		
		if DownloadManagerController.mCharts.count == 0 {
			loadingIndicator.startAnimating()
		} else { // in case of previously loaded chartsetitems:
			loadingIndicator.stopAnimating()
			
			productsTableView.isHidden = false
			productsTableView.reloadData()
		}
		//		loadingIndicator.backgroundColor = UIColor.white
		
		buttonGetAvailableSets()
		
	}
	
	override func viewDidLoad() {
		downloadViewContainer.layer.cornerRadius = 8.0
		downloadViewContainer.clipsToBounds = true
		
		toShopButton.layer.cornerRadius = 8
		toShopButton.clipsToBounds = true
		
	}
	
	override func viewDidAppear(_ animated: Bool) {
		
		DownloadService.service.downloadManagerController = self
		
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		DownloadService.service.downloadManagerController = nil
		
	}
	
	fileprivate func closeDialog() {
        comingFromLogin = false

		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
		//		self.dismiss(animated: false, completion: nil)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if touch?.view == backgroundView {
			closeDialog()
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return DownloadManagerController.mCharts.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell :ProductTableCell = tableView.dequeueReusableCell(withIdentifier: "productTableCell", for: indexPath) as! ProductTableCell
		
		// give each cell a reference to the tableViewController
		cell.controller = self
		
		let chartSet = DownloadManagerController.mCharts[indexPath.row]
		//		if chartSet.itemType == 0 {
		//			cell.textLabel?.text = chartSet.pName
		//		} else {
		//			cell.detailTextLabel?.text = "\t"+(chartSet.mName ?? "")
		//		}
		
		cell.CSI = chartSet
		
		cell.chartSetTitleLabel?.text = chartSet.pName
		let subTitle = chartSet.mName ?? ""
		let edition = chartSet.edition?.description ?? ""
		cell.chartSetSubTitleLabel?.text = "\(subTitle) (\(edition))"
		let size = Int64((chartSet.size ?? 0)/1000000)
		cell.chartSetSizeLabel?.text = (size.description + " MB")
		//		cell.codedString = StChartCheckTask.getCodedProductString(pid: chartSet.productID, mod: chartSet.moduleBitIndex, isSub: chartSet.isSub)
		cell.pid = Int(chartSet.productID!)
		cell.mod = chartSet.moduleBitIndex!
		
		// update button visibility
		cell.updateCellState(state: chartSet.state!)
		//		if chartSet.state == EChartProductState.PRODUCTSTATECOMPLETE {
		//			cell.downloadButtonOutlet.isHidden = true
		//			cell.removeButtonOutlet.isHidden = false
		//		} else if chartSet.state == EChartProductState.PRODUCTSTATEINCOMPLETE {
		//			cell.downloadButtonOutlet.isHidden = true
		//			cell.removeButtonOutlet.isHidden = false
		//		} else {
		//			cell.downloadButtonOutlet.isHidden = false
		//			cell.removeButtonOutlet.isHidden = true
		//		}
		

		return cell
	}
	
	//	func tableView(_ tableView :UITableView, didSelectRowAt indexPath :IndexPath) {
	//		if mSelections.count <= indexPath.row {
	//			mSelections = [Bool](repeating: false, count: mCharts.count)
	//		}
	//
	//		tableView.deselectRow(at: indexPath, animated: true)
	//
	//		// Other row is selected - need to deselect it
	//		//		if let index = mSelectedGameIndex {
	//		//			let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
	//		//			cell?.accessoryType = .none
	//		//		}
	//
	//		//		let selectedChartset = mCharts[indexPath.row]
	//
	//		// update the checkmark for the current row
	//		let cell = tableView.cellForRow(at: indexPath)
	//
	//		if mSelections[indexPath.row] {
	//			cell?.accessoryType = .none
	//			mSelections[indexPath.row] = false
	//		} else {
	//			cell?.accessoryType = .checkmark
	//			mSelections[indexPath.row] = true
	//		}
	//		//		cell?.accessoryType = .none
	//	}
	
	//
	//	@Override
	//	public void onCreate(Bundle savedInstanceState) {
	//		super.onCreate(savedInstanceState);
	//		setHasOptionsMenu(true);
	//	}
	//
	//	@Override
	//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	//		// Inflate the menu; this adds items to the action bar if it is present.
	//		inflater.inflate(R.menu.available_chart_sets, menu);
	//
	//		boolean AccountAvailable = !StActManager.getActManager().getUserDataAvailable();
	//		boolean DownloadAvailable = false, DeleteAvailable = false;
	//		if (mListView != null) {
	//			int Index = mListView.getCheckedItemPosition();
	//			if (Index != ListView.INVALID_POSITION && Index < mListView.getCount()) {
	//				ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
	//				DownloadAvailable = (item.state == EChartProductState.PRODUCTSTATEAVAILABLE || item.state == EChartProductState.PRODUCTSTATEINCOMPLETE);
	//				DeleteAvailable = (item.state != EChartProductState.PRODUCTSTATEAVAILABLE);
	//			}
	//		}
	//
	//		MenuItem m = menu.findItem(R.id.menu_availablesets_account);
	//		//m.setEnabled(AccountAvailable);
	//		m.setVisible(AccountAvailable);
	//		m = menu.findItem(R.id.menu_availablesets_download);
	//		//m.setEnabled(DownloadAvailable);
	//		m.setVisible(DownloadAvailable);
	//		m = menu.findItem(R.id.menu_availablesets_moveToSD);
	//		StDKW2Settings settings = StDKW2Settings.getSettings(getActivity());
	//		if (settings.sdCardAvailable()) {
	//			m.setVisible(true);
	//			if (settings.getUseSDCard())
	//			m.setTitle("settings_title_button_move_to_intern);
	//			else
	//			m.setTitle("settings_title_button_move_to_sd);
	//			m.setEnabled(true);
	//		} else {
	//			m.setVisible(false);
	//		}
	//
	//		m = menu.findItem(R.id.menu_availablesets_delete);
	//		//m.setEnabled(DeleteAvailable);
	//		m.setVisible(DeleteAvailable);
	//
	//		m = menu.findItem(R.id.menu_availablesets_cleanUp);
	//		m.setEnabled(true);
	//	}
	//
	//	@Override
	//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	//		if (mContext == null && container != null && container.getContext() != null)
	//		mContext = container.getContext();
	//		return inflater.inflate(R.layout.fragment_available_chartsets, container, false);
	//	}
	//
	//	@Override
	//	public boolean onOptionsItemSelected(MenuItem item) {
	//		if (item.getItemId() == android.R.id.home) {
	//			// This ID represents the Home or Up button. In the case of this
	//			// activity, the Up button is shown. Use NavUtils to allow users
	//			// to navigate up one level in the application structure. For
	//			// more details, see the Navigation pattern on Android Design:
	//			//
	//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
	//			//
	//			NavUtils.navigateUpFromSameTask(getActivity());
	//			return true;
	//		} else if (item.getItemId() == R.id.menu_availablesets_account) {
	//			openAccount();
	//			return true;
	//		} else if (item.getItemId() == R.id.menu_availablesets_download) {
	//			buttonDownloadClick();
	//			return true;
	//		} else if (item.getItemId() == R.id.menu_availablesets_moveToSD) {
	//			buttonMoveToSDClick(item);
	//			return true;
	//		} else if (item.getItemId() == R.id.menu_availablesets_delete) {
	//			deleteSetClick();
	//			return true;
	//		} else if (item.getItemId() == R.id.menu_availablesets_cleanUp) {
	//			cleanUpClick();
	//			return true;
	//		}
	//		return super.onOptionsItemSelected(item);
	//	}
	//
	//	@Override
	//	public void onPrepareOptionsMenu(Menu menu) {
	//		boolean AccountAvailable = !StActManager.getActManager().getUserDataAvailable();
	//		boolean DownloadAvailable = false, DeleteAvailable = false;
	//		if (mListView != null) {
	//			int Index = mListView.getCheckedItemPosition();
	//			if (Index != ListView.INVALID_POSITION && Index < mListView.getCount()) {
	//				ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
	//				DownloadAvailable = (item.state == EChartProductState.PRODUCTSTATEAVAILABLE || item.state == EChartProductState.PRODUCTSTATEINCOMPLETE);
	//				DeleteAvailable = (item.state != EChartProductState.PRODUCTSTATEAVAILABLE);
	//			}
	//		}
	//
	//		MenuItem m = menu.findItem(R.id.menu_availablesets_account);
	//		//m.setEnabled(AccountAvailable);
	//		m.setVisible(AccountAvailable);
	//		m = menu.findItem(R.id.menu_availablesets_download);
	//		//m.setEnabled(DownloadAvailable);
	//		m.setVisible(DownloadAvailable);
	//		m = menu.findItem(R.id.menu_availablesets_moveToSD);
	//		StDKW2Settings settings = StDKW2Settings.getSettings(getActivity());
	//		if (settings.sdCardAvailable()) {
	//			m.setVisible(true);
	//			if (settings.getUseSDCard())
	//			m.setTitle("settings_title_button_move_to_intern);
	//			else
	//			m.setTitle("settings_title_button_move_to_sd);
	//			m.setEnabled(true);
	//		} else {
	//			m.setVisible(false);
	//		}
	//
	//		m = menu.findItem(R.id.menu_availablesets_delete);
	//		//m.setEnabled(DeleteAvailable);
	//		m.setVisible(DeleteAvailable);
	//
	//		m = menu.findItem(R.id.menu_availablesets_cleanUp);
	//		m.setEnabled(true);
	//	}
	
	func getAvailableChartSets(aEMail :String, aPass :String) {
		//			if (mInternet) { // TODO online check
		mUser = aEMail;
		mPass = aPass;
		
		//				mTextViewDesc.setText(mRes.getString("availablesets_searching));
		//				ActivityCompat.invalidateOptionsMenu(getActivity());

		mChartCheckSt?.requestAvailableChartSets(aActDB: mActDB, aUser: aEMail, aPass: aPass)
		
		//			} else {
		//				mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
		//			}
	}
	
	func buttonGetAvailableSets() {
		getAvailableSets()
	}
	
	func getAvailableSets() {
		//			if (!mInternet) {
		//				mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
		//			} else {
		//				Bundle args = getArguments();
		//				if (args != null) {
		//			let EMail = args.String(forKey: "User")
		//			let Pass = args.String(forKey: "Pass")
		//		mChartCheckSt?.requestAvailableChartSets(aActDB: mActDB, aUser: mUser, aPass: mPass)
		//				} else {
		if mActDB.getUserDataAvailable() {
			mChartCheckSt!.requestAvailableChartSets(aActDB: mActDB, aUser: mActDB.getUserMail(), aPass: mActDB.getUserPass())
		} else {
			//								mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
			print("buttonGetAvailableSets: No account info available")
		}
		//				}
		//			}
		
	}
	
    func chartCheckHandleMessages(cmd :String, Msg :Int, Data :String) {
        
        mCommand = cmd
		mErrorCode = Msg
        
		/*
		if (mCheckPem) {
		if (mErrorCode == StChartCheckTask.CHECKRESULT_OK) {
		getAvailableChartSets(mUser, mPass);
		mCheckPem = false;
		} else {
		showActError();
		}
		return;
		}
		*/
		if mErrorCode == StChartCheckTask.CHECKRESULT_OK {
			
			var newCharts = [ChartSetItem]()
			
			let useJSON = true
			
			if !useJSON { // line-by-line method of parsing
				var paramStr :String = Data
				//			var params :[String] = paramStr.split{$0 == "\n"}.map(String.init) ?? [String]()
				var params :[String] = paramStr.split{$0 == "\n"}.map(String.init)
				
				if params.count == 0 {
					//TODO error msg				Toast.makeText(getActivity(), mRes.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
					return //TODO: finish view
				}
				
				var i :Int = 1
				var chartCount :Int = Int(params[i]) ?? 0
				i += 1
				
				//				var csi :ChartSetItem
				
				// nullpointerexception so check activity for null
				var AppId :Int = 0
				//			FragmentActivity activity = getActivity();
				//			if (activity != null) {
				//				AppId = StActManager.getActManager().checkProductAppID(activity.getPackageName(), StHardwareKey.getHardwareKey(activity), getResources().getInteger(R.integer.appid));
				//			}
				
				while i < chartCount * 12 {
					var pid :Int = Int(params[i + 2]) ?? 0
					var mod :Int = Int(params[i + 3]) ?? 0
					var csi = ChartSetItem()
					csi.itemType = 1
					csi.pName = params[i]
					csi.mName = params[i + 1]
					csi.productID = Int64(params[i + 2])
					csi.moduleBitIndex = Int(params[i + 3])
					csi.count = Int(params[i + 4])
					csi.size = Int64(params[i + 5])
					csi.isSub = params[i + 6] == "1"
					//					csi.expiry = Int64(params[i + 7])
					csi.isCurrent = params[i + 8] == "1"
					csi.groupIndex = Int(params[i + 9])
					csi.orderIndex = Int(params[i + 10])
					csi.edition = Int(params[i + 11])
					
					if mActDB.findProductCode(pid: pid, mod: mod) > -1 {
						csi.state = (mActDB.getProductComplete(aPid: pid, aMod: mod)) ? .Complete : .Incomplete;
					} else {
//                        switch Resources.appType{
//                        case .lite, .standard:
//                            csi.state = .NotDownloaded
//                        case .dkw1800:
////                            csi.state = .Busy
//                            csi.state = .NotDownloaded
//                        }
                        csi.state = .NotDownloaded
					}
					
					i += 12;
					
					//				// In case of the VKNL subscription app, filter out non-subscription licenses
					//				if (mContext != null && AppId == mContext.getResources().getInteger(R.integer.DKW_NEDERLAND_BINNEN) && !csi.isSub)
					//				continue;
					
					
					newCharts.append(csi)
				}
			} else { // JSON parsing
				
				guard
					let response = try? JSONSerialization.jsonObject(with: Data.data(using: .utf8)!) as! [String : Any],
					let sets = response["Sets"] as? [[String : Any]] else {
						return
				}
				
				print("DownloadManagerController: JSONresresponse: \(response)")
				
				for chartSet in sets {
					guard
						let pid = toOptInt(chartSet["pid"]),
						let mod = toOptInt(chartSet["mod"])
						else {
							continue
					}
					
					var csi = ChartSetItem()
					csi.itemType = 1
					
					csi.productID = Int64(pid)
					csi.moduleBitIndex = mod
					csi.isSub = false
					
					print("single chartSet: \(chartSet)")
					
					//					if let size = optIntToOptInt64(chartSet["size"] as? Int) { print(size)}
					//					if let isCurrentInt = chartSet["iscurrent"] as? Int { print("1 \(isCurrentInt)")}
					//					if let isCurrentInt = chartSet["iscurrent"] as? Bool { print("2 \(isCurrentInt)")}
					//					if let isCurrentInt = chartSet["iscurrent"] as? String { print("3 \(isCurrentInt)")}
					//					if let isCurrentInt = chartSet["iscurrent"] as? Double { print("4 \(isCurrentInt)")}
					
					guard
						let pName = chartSet["product"] as? String,
						let mName = chartSet["name"] as? String,
						let count = toOptInt(chartSet["count"]),
						let size = optIntToOptInt64(toOptInt(chartSet["size"])),
						let isCurrentString = chartSet["iscurrent"] as? String,
						let groupIndex = chartSet["groupindex"] as? String,
						let orderIndex = chartSet["orderindex"] as? String,
						let edition = chartSet["edition"] as? String
						else {
							continue
					}
					
					csi.pName = pName
					csi.mName = mName
					csi.count = count
					csi.size = size
					//						case csi.expiry = chartSet["product"] as? String,
					csi.isCurrent = (Int(isCurrentString) ?? 0) == 1
					csi.groupIndex = Int(groupIndex) ?? -1
					csi.orderIndex = Int(orderIndex) ?? -1
					csi.edition = Int(edition) ?? -1
					
					csi.state = checkChartSetState(csi: csi)
					
					//				// In case of the VKNL subscription app, filter out non-subscription licenses
					//				if (mContext != null && AppId == mContext.getResources().getInteger(R.integer.DKW_NEDERLAND_BINNEN) && !csi.isSub)
					//				continue;
					
					// check for new enough charts, except in for the exeptions.
					if csi.edition ?? 0 >= 2018
						|| csi.mName == "Friese Meren"
						|| csi.mName == "Amsterdam"
						|| csi.mName == "De Biesbosch"
					{
						newCharts.append(csi)
					}
				}
			}
			
            // For chart apps, filter out only the relevant charts for that app.
            
            if Resources.appType == .dkw1800 {
                var filteredCharts = [ChartSetItem]()
                for chart in newCharts {
                    if ChartApp1800.chartIn1800App(csi: chart) {
                        filteredCharts.append(chart)
                    }
                }
                newCharts = filteredCharts
            } else if Resources.appType == .friesemeren{
                var filteredCharts = [ChartSetItem]()
                for chart in newCharts {
                    if chartInFrieseMerenApp(csi: chart) {
                        filteredCharts.append(chart)
                    }
                }
                newCharts = filteredCharts
            }
            
            
			//			// sort it
			//			try{
			//				Collections.sort(mCharts, new ChartSetComparator());
			//			} catch (Exception e){
			//				Log.e("popke", "Chart sorting error");
			//			}
			
			//Merge new charts with existing
			
			var chartListChanged :Bool = false
			for newChart in newCharts {
				var found :Bool = false
				for chart in DownloadManagerController.mCharts {
					if newChart.productID == chart.productID,
						newChart.moduleBitIndex == chart.moduleBitIndex,
						newChart.isSub == chart.isSub
					{
						found = true
						break
					}
				}
				if !found {
					DownloadManagerController.mCharts.append(newChart)
					chartListChanged = true
                    
                    
//                    // In case of chart app: Start downloading charts automatically
//                    if Resources.appType == .dkw1800 {
//                        let pid = Int(newChart.productID ?? 0)
//                        let mod = newChart.moduleBitIndex ?? 0
//                        startChartDownloadFromDLManager(pid: pid, mod: mod)
//                    }
				}
			}
			
			if chartListChanged {
				
				//				// Sort by a cascade of conditions:
				//				DownloadManagerController.mCharts = DownloadManagerController.mCharts.sorted(by: {
				//					(($0.state ?? ProductState.NotDownloaded) == ProductState.Complete && ($1.state ?? ProductState.NotDownloaded) != ProductState.Complete) ? true :
				//						(($0.edition ?? 0 > $1.edition ?? 0) ? true :
				//						(($0.productID ?? 0 > $1.productID ?? 0) ? true :
				//							$0.pName ?? "" < $1.pName ?? ""))
				//				})
				
				// Sort by a cascade of conditions:
				
				DownloadManagerController.sortChartsList()
				
			}
			
			newCharts = DownloadManagerController.mCharts
			
			//			// add headers
			//			var pid :Int64 = -1
			//			i = 0
			//			while i < mCharts.count {
			//				var item : ChartSetItem = mCharts[i]
			//				if item.productID != pid {
			//					csi = ChartSetItem()
			//					csi.itemType = 0
			//					csi.pName = item.pName
			//					pid = item.productID ?? 0
			//					csi.productID = pid
			//					mCharts.insert(csi, at: i)
			//					i += 1
			//				}
			//				i += 1
			//			}
			
			//			// check if the newest products are available for the chart apps
			//			if (AppId == 899923 || AppId == 899924 || AppId == 899925) {
			//				boolean found = false;
			//				for (i = 0; i < mCharts.size(); i++) {
			//					ChartSetItem item = mCharts.get(i);
			//					if (item.isCurrent) {
			//						found = true;
			//						break;
			//					}
			//				}
			//				if (!found || mCharts.size() == 0) {
			//					Intent intent = new Intent(activity, IabActivity.class);
			//					startActivityForResult(intent, AppConstants.ACTIVITY_SHOPACTIVITY);
			//					return;
			//				}
			//			}
			
			//			if mCharts.count == 0 {
			//				if mActDB.getActCodeCount() > 0{
			//					mTextViewDesc.setText(mRes.getString("availablesets_noextrasets))
			//				} else {
			//					mTextViewDesc.setText(mRes.getString("availablesets_nosets))
			//				}
			//			} else {
			//				mTextViewDesc.setText(mRes.getString("availablesets_selectset))
			//			}
			
			//			AvailableChartSetsAdapter adapter = (AvailableChartSetsAdapter)mListView.getAdapter()
			//			adapter.notifyDataSetChanged()
			// TODO update list
			
			//        getListView().invalidate();
			
			//			// update UITableView
			//
			//			let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
			//
			//			//			let availableChartSetsController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsControllerID") as! AvailableChartSetsController
			//			let availableChartSetsNavController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsNavControllerID") as! UINavigationController
			//
			//			let availableChartSetsController = availableChartSetsNavController.viewControllers[0] as! AvailableChartSetsController
			//
			//			availableChartSetsController.availableChartSets = self
			//
			////			ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: {() -> Void in self.buttonDownloadClick()})
			//			ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: nil)
			
			
			// available-charts finished loading, so refresh table view
			
			DispatchQueue.main.async{ // to run from main thread
				self.loadingIndicator.stopAnimating()
				//				self.loadingIndicator.hidesWhenStopped = true
				
				// unhide table view and fill it with the new data.
				self.productsTableView.isHidden = false
				if chartListChanged {
					self.productsTableView.reloadData()
                    
                    
                    // For chart apps: After login, ask if all charts should be downloaded
                    if self.comingFromLogin {
                        self.comingFromLogin = false
                        if Resources.appType == .dkw1800 || Resources.appType == .friesemeren {
                        
                            // create dialog
                            dout("Asking for download of chart-app charts")
                        
                            var alert = UIAlertController(title: "dialog_download_charts".localized, message: nil, preferredStyle: .alert)

                            if Resources.appType == .dkw1800 {
//                                let alert = UIAlertController(title: "dialog_download_charts".localized, message: nil, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
                            } else if Resources.appType == .friesemeren {
                                alert = UIAlertController(title: "dialog_download_charts_notification".localized, message: nil, preferredStyle: .alert)
                            }
                            
                            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                                dout("Starting download of all chartsets")
                                
                                for i in 0 ..< DownloadManagerController.mCharts.count {
                                    let chart = DownloadManagerController.mCharts[i]
                                    if let pid = chart.productID,
                                        let mod = chart.moduleBitIndex {
                                        DownloadService.service.startChartDownload(pid: Int(pid), mod: mod)
                                        
                                        DispatchQueue.main.async{
                                            if let cell = self.productsTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? ProductTableCell {
                                                cell.updateCellState(state: .Busy)
                                            }
                                        }
                                    }
                                }
                                
                            }))
                            getTopViewController()?.presentFromMain(alert, animated: true)
                            
                        }
                    }
				}
			}
			
			//			let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
			//			let availableChartSetsController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsControllerID") as! AvailableChartSetsController
			//			availableChartSetsController.mCharts = self.mCharts
			//			ViewController.instance?.present(availableChartSetsController, animated: true, completion: nil)
			
            
            
		} else {
			showActError()
		}
	}
	
	func checkChartSetState(csi :ChartSetItem) -> ProductState {
		if mActDB.findProductCode(pid: Int(csi.productID ?? 0), mod: csi.moduleBitIndex ?? -1) > -1 {
			return mActDB.getProductComplete(aPid: Int(csi.productID ?? 0), aMod: csi.moduleBitIndex ?? -1) ? .Complete : .Incomplete
		} else {
            switch Resources.appType {
            case .lite, .plus:
                return .NotDownloaded
            case .dkw1800, .friesemeren:
//                return .Busy
                return .NotDownloaded
            }
		}
	}
	
	static func sortChartsList(){
		DownloadManagerController.mCharts = DownloadManagerController.mCharts.sorted(by:
			{
				(($0.state ?? ProductState.NotDownloaded) == ProductState.Complete && ($1.state ?? ProductState.NotDownloaded) != ProductState.Complete) ? true :
					//					($0.state == $1.state) &&
					(($0.state ?? ProductState.NotDownloaded) != ProductState.Complete && ($1.state ?? ProductState.NotDownloaded) == ProductState.Complete) ? false :
					(
						($0.edition ?? 0 > $1.edition ?? 0) ? true :
							($0.edition == $1.edition) &&
							(
								$0.pName ?? "" < $1.pName ?? "" ? true :
									($0.pName == $1.pName) &&
									($0.mName ?? "" < $1.mName ?? "")
						)
				)
		})
	}
	
	//	// private boolean checkNetworkConnection() {
	//	//    ConnectivityManager connMgr = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
	//	//    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
	//	//     return (networkInfo != null && networkInfo.isConnected());
	//	// }
	//
	//	private void deleteChartsQuestion() {
	//		Builder cd = new AlertDialog.Builder(getActivity());
	//		cd.setTitle(getResources().getString("availablesets_deletetitle));
	//		cd.setMessage(getResources().getString("availablesets_deletechartsmsg));
	//		cd.setPositiveButton(getResources().getString("button_yes), new DialogInterface.OnClickListener() {
	//
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//				deleteSet(false);
	//			}
	//		});
	//		cd.setNegativeButton(getResources().getString("button_no), new DialogInterface.OnClickListener() {
	//
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//				deleteSet(true);
	//			}
	//		});
	//		if (!getActivity().isFinishing())
	//		cd.show();
	//	}
	
	func deleteSet(index :Int) throws {
		
		//		int Index = mListView.getCheckedItemPosition();
		//		if (Index != ListView.INVALID_POSITION) {
		let item: ChartSetItem = DownloadManagerController.mCharts[index]
		//		mTextViewDesc.setText("Deleting chartset"/*mRes.getString("availablesets_noaccount)*/);
		
		//		if (aDeleteCharts) {
		//		StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
		//		StDocumentFile path = DKWSettings.getDKW2ChartsDir();
		
		let filecount = mActDB.getFileNameCount(aPid: Int(item.productID!), aMod: item.moduleBitIndex!)
		for i in 0 ..< filecount {
			let filename = mActDB.getFileName(aPid: Int(item.productID!), aMod: item.moduleBitIndex!, aIndex: i)
			
			var filePath :URL = getDKW2Directory().appendingPathComponent(filename)
			
			//			StDocumentFile Chart = new StDocumentFile(path, filename)
			if mActDB.getFilenameCount(aFilename: filename) < 2 {
				if FileManager.default.fileExists(atPath: filePath.path) {
					//				Chart.delete();
					//				do {
					try FileManager.default.removeItem(at: filePath)
					//				} catch {
					//					print("Error removing file: \(filePath)")
					//				}
				}
				
				// delete tmp file also
				var tmpFile :URL = getDKW2Directory().appendingPathComponent(filename + ".tmp")
				if FileManager.default.fileExists(atPath: tmpFile.path) {
					//					do {
					try FileManager.default.removeItem(at: tmpFile)
					//					} catch {
					//						print("Error removing existing tmp-file: \(tmpFile.path)")
					//					}
				}
			}
		}
		
		mActDB.deleteProductCode(aPid: Int(item.productID!), aMod: item.moduleBitIndex!)
		
		//		Intent resultIntent = new Intent();
		//		resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERRESULT, ChartManagerActivity.CHARTMANAGERRESULT_RELOADCHARTS);
		//		getActivity().setResult(ChartManagerActivity.RESULT_OK, resultIntent);
		//		getActivity().finish();
		////		DKW2ChartManager.getChartManagerRefresh(mChartDB, mUIHandler, Resources.pid, Resources.mod, pidshow, modshow, true)
		_ = DKW2ChartManager.getChartManagerRefresh(aPid: Resources.pid, aMod: Resources.module, aPidShow: 0, aModShow: 0, aSaveVisibility: true)
		
		//		}
	}
	
	//	private void deleteSetClick() {
	//		Builder cd = new AlertDialog.Builder(getActivity());
	//		cd.setTitle(getResources().getString("availablesets_deletetitle));
	//		cd.setMessage(getResources().getString("availablesets_deletemsg));
	//		cd.setPositiveButton(getResources().getString("button_yes), new DialogInterface.OnClickListener() {
	//
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//				deleteChartsQuestion();
	//			}
	//		});
	//		cd.setNegativeButton(getResources().getString("button_no), new DialogInterface.OnClickListener() {
	//
	//			@Override
	//			public void onClick(DialogInterface dialog, int which) {
	//			}
	//		});
	//		if (!getActivity().isFinishing())
	//		cd.show();
	//	}
	//
	//	private void cleanUpClick() {
	//		if (!mInternet) {
	//			mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
	//		} else {
	//			Bundle args = getArguments();
	//			if (args != null) {
	//				String EMail = args.getString("User");
	//				String Pass = args.getString("Pass");
	//				mChartCheckSt.requestAvailableChartSets(mActDB, EMail, Pass);
	//			} else {
	//				if (mActDB.getUserDataAvailable()) {
	//					mChartCheckSt.requestAvailableChartSets(mActDB, mActDB.getUserMail(), mActDB.getUserPass());
	//				} else {
	//					mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
	//				}
	//			}
	//		}
	//	}
	//
	//	private void openAccount() {
	//		Intent intent = new Intent(getActivity(), UserAccountActivity.class);
	//		intent.putExtra(UserAccountActivity.UACANCELABLE, true);
	//		startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
	//	}
	//
	func showActError() {
		//			Builder alertBuilderConnectError = new AlertDialog.Builder(mActivity);
		//			if (!this.isAdded() || mContext == null) return; // check if still attached for exception below
		//			Resources res = mContext.getResources();
		//			if (mErrorCode == StChartCheckTask.CHECKRESULT_OK)
		//			alertBuilderConnectError.setTitle(res.getString("statuscheck_connect_title_ok));
		//			else
		//			alertBuilderConnectError.setTitle(res.getString("statuscheck_connect_title_fail));

		//		var errText = ""
//		switch (mErrorCode) {
//			//			case StChartCheckTask.CHECKRESULT_OK:
//			//				errText = res.getString("statuscheck_connect_ok); break;
//			//			case -7: // Unknown product
//			//				errText = res.getString("statuscheck_acterror_unknownproduct); break;
//			//			case 6: // Not activated
//			//				errText = res.getString("statuscheck_acterror_notactivated); break;
//			//			case 10: // No results
//			//				errText = res.getString("statuscheck_acterror_noresults); break;
//			//			case 12: // Chart not found
//			//				errText = res.getString("statuscheck_acterror_chartnotfound); break;
//			//			case 13: // Activation blocked
//			//				errText = res.getString("statuscheck_acterror_actblocked); break;
//			//			case  4: // User not found
//			//				errText = res.getString("statuscheck_connect_unknownuser);
//			//				mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
//			//				break;
//			//			case -4: // Database error
//			//			case -5: // License (de)code error
//			//			case -6: // Hardwarekey error
//			//			case -8: // Invalid license
//			//			case 11: // No licensecode available
//			//				errText = res.getString("statuscheck_acterror_servererror); break;
//			//			case 14: // Connection exists
//			//				errText = res.getString("statuscheck_connect_exists); break;
//			//			case 15: // Connection too soon
//			//				errText = res.getString("statuscheck_connect_too_soon); break;
//			//			case 97: // Timeout
//			//				//errText = res.getString("statuscheck_noconnection);
//			//				mTextViewDesc.setText(mRes.getString("statuscheck_noconnection2));
//		//				return;
//		case StChartCheckTask.CHECKRESULT_NOTCONNECTEDTOINTERNET:
//			errText = "statuscheck_acterror_notconnected".localized
//		default:
//			errText = "\("statuscheck_acterror_unknownerror".localized) (error: \(mErrorCode))"
//		}
		
        let errText = StatusCheck.serverErrorMessage(cmd: mCommand, code: mErrorCode)
		
		//			alertBuilderConnectError.setMessage(errText);
		//			alertBuilderConnectError.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		//
		//				@Override
		//				public void onClick(DialogInterface dialog, int which) {
		
		//				}
		//			});
		//			if (!getActivity().isFinishing())
		//			alertBuilderConnectError.show();
		
//        let alert = UIAlertController(title: "error_dialog_title".localized, message: errText, preferredStyle: .alert)
        let alert = UIAlertController(title: errText, message: nil, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { (alertAction) in
			if self.mErrorCode == 4 {
				self.mActDB.resetUserData()
				
				StatusCheck.showLoginDialog(continueWith: {(result :DialogResult, email :String, pw :String) -> () in
					//									self.handleLoginData(aResult: result, aUser: email, aPass: pw)
					if result == .ResultOk {
						self.getAvailableChartSets(aEMail: email, aPass: pw)
					} else {
						self.dismiss(animated: true, completion: nil)
					}
					
				})
				
			} else {
				if self.mErrorCode != StChartCheckTask.CHECKRESULT_OK {
					self.dismiss(animated: true, completion: nil)
				}
			}
		}))
		self.presentFromMain(alert, animated: true)
	}
	
	
	//	private class AvailableChartSetsAdapter extends ArrayAdapter<ChartSetItem> {
	//
	//		public AvailableChartSetsAdapter(Context context, int TextViewResourceId) {
	//			super(context, TextViewResourceId, mCharts);
	//		}
	//
	//		@Override
	//		public View getView(int Position, View ConvertView, ViewGroup Parent) {
	//
	//			ViewContainer vc = null;
	//
	//			if (ConvertView == null) {
	//				LayoutInflater vi = (LayoutInflater)(getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
	//				ConvertView = vi.inflate(R.layout.availableset_item, null);
	//
	//				vc = new ViewContainer();
	//				vc.ivStatus = (ImageView)ConvertView.findViewById(R.id.ivAvailableSetStatus);
	//				vc.tvInfo = (TextView)ConvertView.findViewById(R.id.tvAvailableSetInfo);
	//				vc.tvName = (TextView)ConvertView.findViewById(R.id.tvAvailableSetName);
	//				LinearLayout ll = (LinearLayout)ConvertView.findViewById(R.id.availableSetInfoBox);
	//				ll.setClickable(false);
	//
	//				ConvertView.setTag(vc);
	//			} else {
	//				vc = (ViewContainer)ConvertView.getTag();
	//			}
	//
	//			// Android versions prior to honeycomb don't know the activated state in our
	//			// list_selector, this code sets the activated drawable manually.
	//			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
	//				if (((ListView)Parent).getCheckedItemPosition() == Position)
	//				ConvertView.setBackgroundResource(R.drawable.gradient_bg);
	//				else
	//				ConvertView.setBackgroundResource(0);
	//			}
	//
	//			ChartSetItem csi = mCharts.get(Position);
	//
	//			if (csi.itemType == 0) {
	//				vc.tvName.setText(csi.pName);
	//				vc.tvInfo.setVisibility(View.GONE);
	//				vc.ivStatus.setVisibility(View.GONE);
	//				ConvertView.setPadding(0, 0, 0, 0);
	//			} else {
	//				vc.tvName.setText(csi.mName);
	//				vc.tvInfo.setVisibility(View.VISIBLE);
	//				if (csi.state == EChartProductState.PRODUCTSTATECOMPLETE || csi.state == EChartProductState.PRODUCTSTATEINCOMPLETE) {
	//					vc.ivStatus.setVisibility(View.VISIBLE);
	//					if (csi.state == EChartProductState.PRODUCTSTATECOMPLETE) {
	//						vc.ivStatus.setImageResource(R.drawable.check);
	//						vc.ivStatus.setContentDescription("Chartset usable");
	//					} else {
	//						vc.ivStatus.setImageResource(R.drawable.check_incomplete);
	//						vc.ivStatus.setContentDescription("Chartset incomplete and not usable");
	//					}
	//				} else
	//				vc.ivStatus.setVisibility(View.GONE);
	//
	//				vc.tvInfo.setText(csi.count + " " + mRes.getString("files) + " (" + StUtils.bytesToFileSize(csi.size) + ")");
	//				//          ConvertView.setBackgroundResource(R.drawable.list_selector2);
	//				ConvertView.setPadding(43, 0, 0, 0);
	//			}
	//
	//			vc.tvName.setClickable(false);
	//			vc.tvInfo.setClickable(false);
	//			vc.ivStatus.setClickable(false);
	//
	//			return ConvertView;
	//		}
	//
	//		@Override
	//		public boolean isEnabled(int aPosition) {
	//			ChartSetItem csi = mCharts.get(aPosition);
	//			return csi.itemType == 1;
	//		}
	//	}
	//
	//	private class ChartSetComparator implements Comparator<ChartSetItem> {
	//		@Override
	//		public int compare(ChartSetItem item1, ChartSetItem item2) {
	//
	//			if (item1 == item2)
	//			return 0;
	//
	//			if (item1.state == EChartProductState.PRODUCTSTATECOMPLETE && item2.state != EChartProductState.PRODUCTSTATECOMPLETE)
	//			return -1;
	//			if (item1.state != EChartProductState.PRODUCTSTATECOMPLETE && item2.state == EChartProductState.PRODUCTSTATECOMPLETE)
	//			return 1;
	//
	//			// check product id
	//			//            if (item1.productID > item2.productID)
	//			//                return 1;
	//			//            else if (item1.productID < item2.productID)
	//			//                return -1;
	//			//            // check product id
	//			if (item1.productID > item2.productID)
	//			return -1;
	//			else if (item1.productID < item2.productID)
	//			return 1;
	//			else {
	//				// check module bit
	//				if (item1.moduleBitIndex > item2.moduleBitIndex)
	//				return 1;
	//				else if (item1.moduleBitIndex < item2.moduleBitIndex)
	//				return -1;
	//				else {
	//					return 0;
	//				}
	//			}
	//
	//		}
	//	}

	
	//	private class ViewContainer {
	//		ImageView ivStatus;
	//		TextView tvName;
	//		TextView tvInfo;
	//	}
	//
	//	private class ChartCheckHandlerCallback implements Handler.Callback {
	//
	//		@Override
	//		public boolean handleMessage(Message Msg) {
	//			chartCheckHandleMessages(Msg);
	//			return true;
	//		}
	//	}
	
	
	//	super.init(nibName: nil, bundle: nil)
	
	func downloadByCell(_ cell: ProductTableCell) {
		if let indexPath = productsTableView.indexPath(for: cell) {
			
			DispatchQueue.main.async{
				cell.downloadProgress.progress = 0
				cell.currentlyDownloading = nil
				cell.updateCellState(state: .Downloading)
				
				//				self.reload(indexPath.row)
			}
			
			//			let track = searchResults[indexPath.row]
			//			downloadService.startDownload(track)
			
			//			let itemIndex = indexPath.row
			
			//			var CheckedItem :String = ""
			//		int Index = mListView.getCheckedItemPosition();
			//		if (Index != ListView.INVALID_POSITION) {
			//			ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
			
			// TODO for now: only download the first chart set
			//			let item :ChartSetItem = mCharts[itemIndex]
			//
			//			cell.codedString = StChartCheckTask.getCodedProductString(pid: item.productID, mod: item.moduleBitIndex, isSub: item.isSub)
			//
			//		}
			
			//		if (CheckedItem.length() > 0) {
			//			if (mFirstDownload) {
			//				// This is used when no charts are present (activated) in the app
			//				Intent resultIntent = new Intent();
			//				resultIntent.putExtra(AvailableChartSetsActivity.CSRESULT, CheckedItem);
			//
			//				getActivity().setResult(Activity.RESULT_OK, resultIntent);
			//				getActivity().finish();
			//			} else {
			//				Intent intent = new Intent(getActivity(), StatusCheckActivityAccountNew.class);
			//				intent.putExtra(AvailableChartSetsActivity.CSRESULT, CheckedItem);
			//				startActivityForResult(intent, AppConstants.ACTIVITY_STATUSCHECKACCOUNT);
			
			//((ErrorCode, Bool, String) -> ())
			
			DownloadService.service.startChartDownload(pid: cell.pid, mod: cell.mod)
//            startChartDownloadFromDLManager(pid: cell.pid, mod: cell.mod)
			//			StatusCheck.runFromChartManager(aViewController: self, callBackHandler: nil, pid: cell.pid, mod: cell.mod)
			
			//			}
			//		}
		}
	}
	
	func pauseByCell(_ cell: ProductTableCell){
	}
	
	func resumeByCell(_ cell: ProductTableCell){
	}
	
	func cancelByCell(_ cell: ProductTableCell){
		
		//		let index = productsTableView.indexPath(for: cell)!.row
		//		let item :ChartSetItem = mCharts[index]
		//		var codedString = DownloadManagerController.codedProductString(pid: item.productID, mod: item.moduleBitIndex, isSub: item.isSub)
		//
		////		StatusCheck.runFromChartManager(aViewController: self, callBackHandler: nil, checkedItem: codedString)
		//
		//		mOutputFile = path.appendingPathComponent(aFilename + ".tmp")
		
	}
	
	func remove(cell: ProductTableCell){
		if let indexPath = productsTableView.indexPath(for: cell) {
			let oldState = cell.CSI?.state
			do {
				DispatchQueue.main.async{
					cell.updateCellState(state: .Busy)
				}
				
				try deleteSet(index: indexPath.row)
				
				// if succesful
				DispatchQueue.main.async{
					cell.updateCellState(state: .NotDownloaded)
				}
			} catch {
				print("Error deleting chartset")
				DispatchQueue.main.async{
					if let oldState = oldState { cell.updateCellState(state: oldState) }
				}
			}
		}
	}
	
	//	// Update table cell
	//	func reload(_ row: Int) {
	//		DispatchQueue.main.async{
	//			self.productsTableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .none)
	//		}
	//	}
	
	func setProgress(pid :Int, mod :Int, fileName :String, soFar :Int64, total :Int64){
		
		for i in 0 ..< DownloadManagerController.mCharts.count {
			let chart = DownloadManagerController.mCharts[i]
			if Int(chart.productID!) == pid, chart.moduleBitIndex == mod {
				
				DispatchQueue.main.async{
					if let cell = self.productsTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? ProductTableCell{
						
						cell.updateCellState(state: .Downloading)
                        if total > 0 {
						cell.downloadProgress.progress = Float(soFar) / Float(total)
						cell.downloadProgressLabel.text = fileName
                        } else {
//                            cell.downloadProgress.progress = Float(soFar) / Float(total)
                            cell.downloadProgressLabel.text = "\(fileName) (\(soFar) B)"
                        }
						
						if #available(iOS 10.0, *) {
							cell.downloadProgressLabel.adjustsFontForContentSizeCategory = true
						} 
						cell.downloadProgressLabel.adjustsFontSizeToFitWidth = true
						cell.downloadProgressLabel.textAlignment = .right
						
					}
				}
			}
		}
	}
	
	//	func finishDownload(ufr :StatusCheck.UpdateFileRecord){
	func finishDownload(pid: Int, mod :Int){
		for i in 0 ..< DownloadManagerController.mCharts.count {
			let chart = DownloadManagerController.mCharts[i]
			if chart.productID == Int64(pid), chart.moduleBitIndex == mod {
				
				DispatchQueue.main.async{
					if let cell = self.productsTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? ProductTableCell {
						cell.currentlyDownloading = nil
						cell.downloadProgressLabel.text = ""
						cell.updateCellState(state: .Complete)
						//					self.reload(i)
					}
				}
			}
		}
	}
	
	static func clearCharts(){
		DownloadManagerController.mCharts = [ChartSetItem]()
	}
    
//    func startChartDownloadFromDLManager(pid :Int, mod: Int){
//
//        DownloadService.service.startChartDownload(pid: pid, mod: mod)
//
//        // Disable button right away, so find it first:
//        for i in 0 ..< DownloadManagerController.mCharts.count {
//            let chart = DownloadManagerController.mCharts[i]
//            if chart.productID == Int64(pid), chart.moduleBitIndex == mod {
//                DispatchQueue.main.async{
//                    if let cell = self.productsTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? ProductTableCell {
//                        cell.updateCellState(state: .Busy)
//                    }
//                }
//            }
//        }
//    }
    
    func chartInFrieseMerenApp(csi :DownloadManagerController.ChartSetItem) -> Bool {
        
        // pid: 196664 = Friese meren
        if csi.productID == 196664 && csi.moduleBitIndex == 1 {
            return true
        }

        return false
    }
}


