//
//  LPWWPlanner.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 30/07/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class LPWWPlanner {

//    private static final double INFINITE_WEIGHT = Float.MAX_VALUE;
//
//    private static final int LCRPASS          = 0; // The constraint can be passed (possibly with a delay).
//    //private static final int LCRBARRIER       = 1; // The constraint causes a barrier.
//    //private static final int LCRTIMEDEPENDENT = 2; // The constraint can be passed (possibly with a delay) or causes a barrier depending on traversal time.
//
//    private boolean                mDirectionPositive; // True if the current section is traversed from head to tail.
//    private StWWPlannerPoint       mDstPlannerPoint;   // Planner point at the destination.
//    private StLPLeg                mLeg;               // Pointer to current leg.
//    private final StLPPlanner      mLegPlanner;        // Pointer to leg planner.
//    private final StLPRoutePoint   mRoutePointNext;    // Pointer to next route point.
//    private final StLPRoutePoint   mRoutePointPrev;    // Pointer to previous route point.
//    private LPSavepoint            mSavepoint;         // Savepoint to commit or roll back the creation of points along a section.
//    private ArrayList<StLPSection> mSectionBin;        // Bin containing all sections created during planning.
//    private StWWPlannerPoint       mSrcPlannerPoint;   // Planner point at the source.
//    private final StWWNetwork      mWWNetwork;         // Waterway network.
//
//    private StLPWWPlanner(MutableBoolean aFound, StLPPlannerState aPlannerState, StLPLeg aLeg, final StLPPlanner aLegPlanner, final StWWNetwork aWWNetwork) {
//        mDirectionPositive = true;
//        mDstPlannerPoint = null;
//        mLeg = aLeg;
//        mLegPlanner = aLegPlanner;
//        mRoutePointNext = aLeg.getRoutePointNext();
//        mRoutePointPrev = aLeg.getRoutePointPrev();
//        mSavepoint = null;
//        mSectionBin = new ArrayList<>();
//        mSrcPlannerPoint = null;
//        mWWNetwork = aWWNetwork;
//
//        //Assert(mLeg->GetSectionCount() == 0, L"The leg should be cleared before calling this function");
//
//        aFound.setValue(findOptimalPath(aPlannerState));
//    }
//
//    private double calcWeight(StLPState aState, StLPBoatState aBoatState, StLPSection aSection, final LPWWSectionPos aStart, final LPWWSectionPos aEnd) {
//
//        final StWWSection Section = aSection.getSection();
//        //Assert(aStart.Pos >= 0.0 && aStart.Pos <= Section->GetLen(), L"aPosStart out of bounds");
//        //Assert(aEnd.Pos >= 0.0 && aEnd.Pos <= Section->GetLen(), L"aPosEnd out of bounds");
//
//        // Test if the section is part of the included water bodies.
//        int Flag = StWWDefs.WWWaterbodyIDToMask(Section.getWaterBodyID());
//        if ((mLegPlanner.getWaterBodyMask() & Flag) == 0)
//            return INFINITE_WEIGHT;
//
//        // Create the first point.
//        StLPPoint FirstPoint = createPoint(aState, aBoatState, aSection, aStart.Pos, -1);
//
//        // Loop through all section parts and process them, advancing CurDateTime and aPosStart. Process forward and backward
//        // traversal through the section in separate loops.
//        MutableInteger MStart = new MutableInteger(0);
//        MutableInteger MEnd = new MutableInteger(0);
//        getSectionPartRange(MStart, MEnd, Section, aStart.Pos, aEnd.Pos);
//        int StartIndex = MStart.getValue();
//        int EndIndex = MEnd.getValue();
//
//        LPWWSectionPos Start = aStart;
//        if (aEnd.Pos >= aStart.Pos) {
//            // Process all parts from start to end in forward direction through the section.
//            mDirectionPositive = true;
//            for (int i = StartIndex; i <= EndIndex; i++) {
//                final StWWSectionPart Part = Section.getSectionPart(i);
//                LPWWSectionPos End;
//                if (Part.getPosEnd() < aEnd.Pos)
//                    End = new LPWWSectionPos(Part.getPosEnd(), Section.getPlannerPoint(Part.getPlannerPointIndexEnd()), Part.getPlannerPointIndexEnd());
//                else
//                    End = aEnd;
//                if (!executeSectionPart(aState, aBoatState, aSection, Part, Start, End))
//                    return INFINITE_WEIGHT;
//            }
//        } else {
//            // Process all parts from start to end in backward direction through the section.
//            mDirectionPositive = false;
//            for (int i = StartIndex; i >= EndIndex; i--) {
//                final StWWSectionPart Part = Section.getSectionPart(i);
//                LPWWSectionPos End;
//                if (Part.getPosStart() > aEnd.Pos)
//                    End = new LPWWSectionPos(Part.getPosStart(), Section.getPlannerPoint(Part.getPlannerPointIndexStart()), Part.getPlannerPointIndexStart());
//                else
//                    End = aEnd;
//                if (!executeSectionPart(aState, aBoatState, aSection, Part, Start, End))
//                    return INFINITE_WEIGHT;
//            }
//        }
//
//        // Return the weight, depending on the planner mode.
//        //switch (mLegPlanner->mSettings.Mode) {
//        //    case lpmTime:     return aState.GetPlannerState().Time;
//        //    case lpmDistance: return aState.GetPlannerState().DistSOP;
//        //    case lpmFuel:     return aState.GetPlannerState().FuelSOP;
//        //    case lpmMoney:    return aState.GetPlannerState().MoneySOP;
//        //    default: throw CException(L"Unknown case");
//        //}
//        return aState.getPlannerState().DistSOP;
//    }
//
//    private int comparePassedPassages(final StLPPassedPassage aP1, final StLPPassedPassage aP2) {
//        // Sort by time, clearance.
//        if (aP1.DateTime < aP2.DateTime)
//            return -1;
//        else if (aP1.DateTime > aP2.DateTime)
//            return 1;
//        else if (aP1.Clearance > aP2.Clearance)
//            return -1;
//        else if (aP1.Clearance < aP2.Clearance)
//            return 1;
//        else
//            return 0;
//    }
//
//    private StLPPoint createPoint(final StLPState aState, final StLPBoatState aBoatState, StLPSection aSection, double aSectionPos, int aResult) {
//
//        StLPPoint Point = new StLPPoint(aBoatState, aState.getCOG(), aState.getVMax(), aState.getEnvState(),
//                aState.getPlannerState(), aState.getPosGeo(), aState.getPosGlb(), aSection, aSectionPos, aResult);
//
//        // Add the point to the section.
//        aSection.addPoint(Point);
//
//        return Point;
//    }
//
//    private StLPSection createSection(final StWWSection aSection) {
//        StLPSection Section = new StLPSection(aSection);
//        mSectionBin.add(Section);
//        return Section;
//    }
//
//    private int executeBarrier(StLPPassedPassage aPassedPassage, long aArrivalDateTime, final StWWBarrier aBarrier) {
//
//        // If the barrier is normally closed (e.g. weirs), treat the barrier as a barrier.
//        if (!aBarrier.getNormallyOpened())
//            return StLPPoint.SECTIONRESULT_SCLOSED;// false;
//
//        // Loop through all passages of the structure.
//        int PassageFound = -1;//false;
//        int Count = aBarrier.getDescPassageCount();
//        for (int i = 0; i < Count; i++) {
//            // Get a passage of the barrier.
//            final StWWBarrier.StWWBarrierPassage Passage = aBarrier.getDescPassage(i);
//
//            // Test if the passage's width is sufficient for the vessel. If not, continue finding another passage.
//            if (mLegPlanner.mBoatDef.getWidth() > Passage.getWidth()) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SWIDTH);
//                continue;
//            }
//
//            // Test if the passage's threshold depth is sufficient for the vessel. If not, continue finding another passage.
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), Passage.getThresholdDepth())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//
//            // Test if the passage's height is sufficient for the vessel. If not, continue finding another passage.
//            MutableFloat Clearance = new MutableFloat();
//            if (!StWWUtils.passHeightTest(Clearance, Passage.getHeightOpened(), mLegPlanner.mBoatDef.getHeight(), mLegPlanner.mSettings.WaterLevelNAP)) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SHEIGHT);
//                continue;
//            }
//
//            // The passage can be used.
//            StLPPassedPassage PassedPassage = new StLPPassedPassage();
//            initStructure(PassedPassage, aArrivalDateTime, aBarrier, i, Clearance.getValue());
//
//            // Test if this is the best passage to use.
//            if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                PassageFound = StLPPoint.SECTIONRESULT_OK;
//                aPassedPassage.init(PassedPassage);
//            }
//        }
//        return PassageFound;
//    }
//
//    private int executeBridge(StLPPassedPassage aPassedPassage, long aArrivalDateTime, final StWWBridge aBridge) {
//
//        // Loop through all passages of the bridge.
//        int PassageFound = -1;// false;
//        boolean OperationSearched = false;
//        StWWOperation Operation = new StWWOperation();
//        MutableDouble OperationDelay = new MutableDouble();
//        boolean OperationFound = false;
//        int Count = aBridge.getDescPassageCount();
//        for (int i = 0; i < Count; i++) {
//            // Get a passage of the bridge.
//            StWWBridge.StWWBridgePassage Passage = aBridge.getDescPassage(i);
//
//            // Test if the passage's width is sufficient for the vessel. If not, continue finding another passage.
//            if (mLegPlanner.mBoatDef.getWidth() > Passage.getWidth()) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SWIDTH);
//                continue;
//            }
//
//            // Test if the passage's threshold depth is sufficient for the vessel. If not, continue finding another passage.
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), Passage.getThresholdDepth())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//
//            // Test if the passage's closed height is sufficient for the vessel.
//            MutableFloat Clearance = new MutableFloat();
//            if (StWWUtils.passHeightTest(Clearance, Passage.getHeightClosed(), mLegPlanner.mBoatDef.getHeight(), mLegPlanner.mSettings.WaterLevelNAP)) {
//                // The passage can be used when closed.
//                StLPPassedPassage PassedPassage = new StLPPassedPassage();
//                initStructure(PassedPassage, aArrivalDateTime, aBridge, i, Clearance.getValue());
//
//                // Test if this is the best passage to use.
//                if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                    PassageFound = StLPPoint.SECTIONRESULT_OK;
//                    aPassedPassage.init(PassedPassage);
//                }
//            }
//
//            // Test if the passage's opened height is sufficient for the vessel.
//            if (StWWUtils.passHeightTest(Clearance, Passage.getHeightOpened(), mLegPlanner.mBoatDef.getHeight(), mLegPlanner.mSettings.WaterLevelNAP)) {
//                // The passage can be used when opened.
//                if (aBridge.getNormallyOpened()) {
//                    // The bridge is normally opened; it does not need to be operated to pass.
//                    StLPPassedPassage PassedPassage = new StLPPassedPassage();
//                    initStructure(PassedPassage, aArrivalDateTime, aBridge, i, Clearance.getValue());
//                    // Test if this is the best passage to use.
//                    if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                        PassageFound = StLPPoint.SECTIONRESULT_OK;
//                        aPassedPassage.init(PassedPassage);
//                    }
//                } else {
//                    // Find the first operation period that can be used. Test for value of PeriodIndex to ensure this call is done
//                    // only once for bridges with multiple passages that can be opened.
//                    if (!OperationSearched) {
//                        OperationFound = aBridge.findNearestOperation(Operation, OperationDelay, aArrivalDateTime,
//                                mLegPlanner.mSettings.DelayMax, mLegPlanner.mBoatDef.getCommercial());
//                        OperationSearched = true;
//                    }
//
//                    if (OperationFound) {
//                        // An operation period was found.
//                        StLPPassedPassage PassedPassage = new StLPPassedPassage();
//                        initOperatedStructure(PassedPassage, aArrivalDateTime, aBridge, i, Clearance.getValue(), Operation, OperationDelay.getValue(),
//                                mLegPlanner.mSettings.DelayBridges);
//                        // Test if this is the best passage to use.
//                        if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                            PassageFound = StLPPoint.SECTIONRESULT_OK;
//                            aPassedPassage.init(PassedPassage);
//                        }
//                    } else {
//                        PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SOPERATION);
//                    }
//                }
//            } else {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SHEIGHT);
//            }
//        }
//        return PassageFound;
//    }
//
//    private int executeFixedStructure(StLPPassedPassage aPassedPassage, long aArrivalDateTime, final StWWFixedStructure aFixedStructure) {
//
//        // Loop through all passages of the fixed structure.
//        int PassageFound = -1;//false;
//        int Count = aFixedStructure.getDescPassageCount();
//        for (int i = 0; i < Count; i++) {
//            // Get a passage of the structure.
//            StWWFixedStructure.StWWFixedStructurePassage Passage = aFixedStructure.getDescPassage(i);
//
//            // Test if the passage's width is sufficient for the vessel. If not, continue finding another passage.
//            if (mLegPlanner.mBoatDef.getWidth() > Passage.getWidth()) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SWIDTH);
//                continue;
//            }
//
//            // Test if the passage's threshold depth is sufficient for the vessel. If not, continue finding another passage.
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), Passage.getThresholdDepth())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//
//            // Test if the passage's height is sufficient for the vessel. If not, continue finding another passage.
//            MutableFloat Clearance = new MutableFloat();
//            if (!StWWUtils.passHeightTest(Clearance, Passage.getHeight(), mLegPlanner.mBoatDef.getHeight(), mLegPlanner.mSettings.WaterLevelNAP)) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SHEIGHT);
//                continue;
//            }
//
//            // The passage can be used.
//            StLPPassedPassage PassedPassage = new StLPPassedPassage();
//            initStructure(PassedPassage, aArrivalDateTime, aFixedStructure, i, Clearance.getValue());
//
//            // Test if this is the best passage to use.
//            if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                PassageFound = StLPPoint.SECTIONRESULT_OK;
//                aPassedPassage.init(PassedPassage);
//            }
//        }
//        return PassageFound;
//    }
//
//    private int executeConstraint(MutableLong aEntryDateTime, final StWWConstraint aConstraint) {
//        switch (aConstraint.evaluatePeriod(aEntryDateTime.getValue())) {
//            case StWWConstraint.WPRNOPERIOD:
//                // No period defined. The constraint is valid always. Test it.
//                return passConstraint(aConstraint);
///*
//                if (passConstraint(aConstraint))
//                    return LCRPASS;
//                else
//                    return LCRBARRIER;
//            case StWWConstraint.WPRBEFORE:
//                // The period is not is effect yet. Depending on the time traveled through the section part, it may become
//                // effective. Set the entry time to the arrival time, trying to pass the constraint without delay.
//                if (passConstraint(aConstraint))
//                    return LCRPASS;
//                else
//                    return LCRTIMEDEPENDENT;
//            case StWWConstraint.WPRDURING:
//                // The period is in effect. If the vessel is affected by the constraint, delay it until the end of the
//                // constraint's period.
//                if (!passConstraint(aConstraint))
//                    aEntryDateTime.setValue(aConstraint.getPeriodEnd());
//                return LCRPASS;*/
//            default:
//                // The period is no longer in effect. Ignore the constraint.
//                return LCRPASS;
//        }
//    }
//
//    private int executeLock(StLPPassedPassage aPassedPassage, long aArrivalDateTime, final StWWLock aLock) {
//
//        // Loop through all passages of the lock.
//        int PassageFound = -1;//false;
//        boolean OperationSearched = false;
//        StWWOperation Operation = new StWWOperation();
//        MutableDouble OperationDelay = new MutableDouble();
//        boolean OperationFound = false;
//        int Count = aLock.getDescPassageCount();
//        for (int i = 0; i < Count; i++) {
//            // Get a passage of the lock.
//            final StWWLock.StWWLockChamber LockChamber = aLock.getDescPassage(i);
//
//            // Test if the lock chamber's width is sufficient for the vessel. If not, continue finding another passage.
//            float WidthMax = Math.min(LockChamber.getChamberWidth(), LockChamber.getMinGateWidth());
//            if (mLegPlanner.mBoatDef.getWidth() > WidthMax) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SWIDTH);
//                continue;
//            }
//
//            // Test if the lock chamber's length is sufficient for the vessel. If not, continue finding another passage.
//            float LockingLen = LockChamber.getLockingLength();
//            if (LockChamber.getHasTidalDoors()) {
//                // Because no tidal information is available currently, use the shortest of two lengths.
//                LockingLen = Math.min(LockingLen, LockChamber.getLockingLengthLowTide());
//            }
//            if (mLegPlanner.mBoatDef.getLength() > LockingLen) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SLENGTH);
//                continue;
//            }
//
//            // Test if the lock chamber's thresholds are sufficient for the vessel. If not, continue finding another passage.
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), LockChamber.getDepthInside())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), LockChamber.getDepthOutside())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//            if (!StWWUtils.passDepthTest(mLegPlanner.mBoatDef.getDraught(), LockChamber.getDepthMiddle())) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SDRAUGHT);
//                continue;
//            }
//
//            // Test if the lock chamber's height is sufficient for the vessel. If not, continue finding another passage.
//            MutableFloat Clearance = new MutableFloat();
//            if (!StWWUtils.passHeightTest(Clearance, LockChamber.getHeight(), mLegPlanner.mBoatDef.getHeight(), mLegPlanner.mSettings.WaterLevelNAP)) {
//                PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SHEIGHT);
//                continue;
//            }
//
//            // The passage can be used.
//            StLPPassedPassage PassedPassage = new StLPPassedPassage();
//            if (aLock.getNormallyOpened()) {
//                // The lock is normally opened (no locking, doors opened); it does not need to be operated to pass.
//                initStructure(PassedPassage, aArrivalDateTime, aLock, i, Clearance.getValue());
//            } else {
//                // Find the first operation period that can be used. Test for value of PeriodIndex to ensure this call is done
//                // only once for bridges with multiple passages that can be opened.
//                if (!OperationSearched) {
//                    OperationFound = aLock.findNearestOperation(Operation, OperationDelay, aArrivalDateTime,
//                            mLegPlanner.mSettings.DelayMax, mLegPlanner.mBoatDef.getCommercial());
//                    OperationSearched = true;
//                }
//                if (!OperationFound) {
//                    PassageFound = setStructResult(PassageFound, StLPPoint.SECTIONRESULT_SOPERATION);
//                    continue;
//                }
//
//                // An operation period was found.
//                initOperatedStructure(PassedPassage, aArrivalDateTime, aLock, i, Clearance.getValue(), Operation, OperationDelay.getValue(),
//                        mLegPlanner.mSettings.DelayLocks);
//            }
//
//            // Test if this is the best passage to use.
//            if (PassageFound != StLPPoint.SECTIONRESULT_OK || comparePassedPassages(aPassedPassage, PassedPassage) > 0) {
//                PassageFound = StLPPoint.SECTIONRESULT_OK;
//                aPassedPassage.init(PassedPassage);
//            }
//        }
//        return PassageFound;
//    }
//
//    private boolean executeSectionPart(StLPState aState, StLPBoatState aBoatState, StLPSection aSection, final StWWSectionPart aPart,
//                                       LPWWSectionPos aStart, final LPWWSectionPos aEnd) {
//
//        // If the section part has no length, it should contain one structure constraint. Otherwise it is a part with zero or
//        // more linear constraints.
//        if (aPart.getPosStart() == aPart.getPosEnd()) {
//            //Assert(aStart.Pos == aEnd.Pos, L"The section part has no length. aStart.Pos should be equal to aEnd.Pos");
//            //Assert(aPart.GetConstraintCount() == 0, L"Section parts with no length should contain zero constraints");
//            //Assert(aPart.GetStructure(), L"Section parts with no length should contain a structure");
//
//            // Execute the structure constraint.
//            StLPPassedPassage PassedPassage = new StLPPassedPassage();
//            int Result = executeStructure(aState, PassedPassage, aPart.getStructure());
//            if (Result != StLPPoint.SECTIONRESULT_OK) {
//                //return false;
//            }
//
//            // If the last point contains a structure already, add a point first.
//            StLPPoint Point = aSection.mPoints.get(aSection.mPoints.size() - 1);
//            if (Point.mStructure != null) {
//                // Create a new point.
//                Point = createPoint(aState, aBoatState, aSection, Point.mSectionPos, -1);
//            }
//
//            Point.setStructure(aPart.getStructure()/*PassedPassage.Structure*/, PassedPassage.DateTime, PassedPassage.DateTime, PassedPassage.DescPassageIndex, Result
//                    /*PassedPassage.MustUnstepMast, PassedPassage.Operation, PassedPassage.Toll*/);
//
//            return true;
//        } else {
//
//            // A list of time-dependent linear constraints.
////            CList<const CWWConstraint> TimeDepConstraints;
//
//            // Process all constraints, find the entry time and collect the time-dependent constraints.
//            MutableLong EntryDateTime = new MutableLong(aState.getPlannerState().Time);
//            int Result = 0;
//            int Count = aPart.getConstraintCount();
//            for (int i = 0; i < Count; i++) {
//                StWWConstraint Constraint = aPart.getConstraint(i);
//                Result = executeConstraint(EntryDateTime, Constraint);
///*
//                switch (Result) {
//                    case lcrBarrier:
//                        return false;
//                    case lcrTimeDependent:
//                        // Add to list for processing after calculating the traversal time.
//                        TimeDepConstraints.Add(Constraint);
//                        break;
//                }*/
//            }
//
//            // Create a savepoint that can be used to roll back or commit.
////            LPSavepoint Savepoint = new LPSavePoint(this, aSection);
//
//            // Traverse the section part. This calculates the exit time.
//            StLPState State = new StLPState(aState, EntryDateTime.getValue());
//            StLPBoatState BoatState = new StLPBoatState(aBoatState);
//            if (!traversePart(State, BoatState, aSection, aStart, aEnd, Result))
//                return false;
//
///*
//            // Process all time-dependent constraints by moving the entry time forward until all time-dependent constraints can
//            // be passed.
//            bool AllPassed = false;
//            while (!AllPassed) {
//                AllPassed = true;
//                for (int i = TimeDepConstraints.GetCount() - 1; i >= 0; i--) {
//                    const CWWConstraint *Constraint = TimeDepConstraints[i];
//                    bool Passed = Constraint->ExecuteTimeDep(EntryDateTime, State.GetPlannerState().Time);
//                    if (!Passed) {
//                        // The constraint could not be passed with the current entry time. Update the entry time and traverse the
//                        // section part again to update the exit time. Because this generates new points along the section, the
//                        // section should be rolled back to the state before calling TraversePart for the first time.
//                        State.Init(aState, EntryDateTime);
//                        BoatState = aBoatState;
//                        Savepoint.RollBack();
//                        if (!TraversePart(State, BoatState, aSection, aPart, aStart, aEnd))
//                            return false;
//
//                        // Set AllPassed to false. This causes the loop to start from the beginning to test all time-dependent
//                        // constraints.
//                        AllPassed = false;
//                        break;
//                    }
//                }
//            }
//
//            Savepoint.Commit();
//*/
//            // Update the start position and set the results.
//            aStart.init(aEnd);
//            aState.init(State);
//            aBoatState.init(BoatState);
//
//            return true;
//        }
//    }
//
//    private int executeStructure(StLPState aState, StLPPassedPassage aPassedPassage, final StWWStructure aStructure) {
//
//        if (aStructure == null) {
//            int a = 0;
//            return StLPPoint.SECTIONRESULT_SCLOSED;
//        }
//
//        StWWLocation.WWLocationClass SClass = aStructure.getLocationClass();
//
//        // If the structure is a bridge, process the bridge.
//        if (SClass == StWWLocation.WWLocationClass.BRIDGE) {
//            final StWWBridge Bridge = (StWWBridge)aStructure;
//            int Result = executeBridge(aPassedPassage, aState.getPlannerState().Time, Bridge);
//            if (Result != StLPPoint.SECTIONRESULT_OK)
//                return Result;
//        // If the structure is a lock, execute the lock.
//        } else if (SClass == StWWLocation.WWLocationClass.LOCK) {
//            final StWWLock Lock = (StWWLock)aStructure;
//            int Result = executeLock(aPassedPassage, aState.getPlannerState().Time, Lock);
//            if (Result != StLPPoint.SECTIONRESULT_OK)
//                return Result;
//        // If the structure is a barrier, execute the barrier.
//        } else if (SClass == StWWLocation.WWLocationClass.BARRIER) {
//            final StWWBarrier Barrier = (StWWBarrier)aStructure;
//            int Result = executeBarrier(aPassedPassage, aState.getPlannerState().Time, Barrier);
//            if (Result != StLPPoint.SECTIONRESULT_OK)
//                return Result;
//        } else {
//        // If the structure is a fixed structure, process the fixed structure.
//            final StWWFixedStructure FixedStructure = (StWWFixedStructure)aStructure;
//            int Result = executeFixedStructure(aPassedPassage, aState.getPlannerState().Time, FixedStructure);
//            if (Result != StLPPoint.SECTIONRESULT_OK)
//                return Result;
//        }
///*
//        // Calculate the time the structure is left.
//        aState.SetPlannerState(aState.GetPlannerState().DistSOP,
//                aState.GetPlannerState().FuelSOP + aPassedPassage.Fuel,
//                aState.GetPlannerState().MoneySOP + aPassedPassage.Money,
//                aPassedPassage.DateTimeExit);
//*/
//        return StLPPoint.SECTIONRESULT_OK;
//    }

    private func findOptimalPath(aPlannerState :LPPlannerState) -> Bool {

//        final StWWSection SrcSection StWWSection = mRoutePointPrev.getSection();
//        double SrcPos = mRoutePointPrev.getSectionPos();
//        final StWWSection DstSection = mRoutePointNext.getSection();
//        double DstPos = mRoutePointNext.getSectionPos();
//        //Assert(SrcPos >= 0.0 && SrcPos <= SrcSection->GetLen(), L"aSrcPos out of bounds");
//        //Assert(DstPos >= 0.0 && DstPos <= DstSection->GetLen(), L"aDstPos out of bounds");
//
//        // Allocate planner nodes for every node and one additional node for the destination.
//        int NodeCount = mWWNetwork.getNodeCount();
//        LPWWNode Nodes[] = new LPWWNode[NodeCount + 1];
//
//        // Set the node pointer of every planner node.
//        for (int i = 0; i < NodeCount; i++) {
//            final StWWNode Node = mWWNetwork.getNode(i);
//            Nodes[Node.getDataIndex()] = new LPWWNode();
//            Nodes[Node.getDataIndex()].Node = Node;
//        }
//
//        // Set the destination node. It's node pointer is NULL because it does not correspond to a waterway node.
//        Nodes[Nodes.length - 1] = new LPWWNode();
//        LPWWNode DstNode = Nodes[Nodes.length - 1];
//        DstNode.Node = null;
//
//        // Set the weight of each planner node to infinity and the OptimalPath pointers to NULL and create a linked list of
//        // all nodes in the netwerk sorted by weight (no sorting is needed because all nodes have the same weight initially).
//        NodeCount = Nodes.length;
//        LPWWNode First = Nodes[0];//Nodes.GetItems();
//        LPWWNode PlannerNode = First;
//        for (int i = 0; i < NodeCount; i++) {
//            PlannerNode.Next = (i < NodeCount - 1) ? Nodes[i + 1] : null;//PlannerNode + 1;
//            PlannerNode.Prev = (i > 0) ? Nodes[i - 1] : null;
//            PlannerNode.OptimalPathNodePrev = null;
//            PlannerNode.OptimalPathSection = null;
//            PlannerNode.Weight = INFINITE_WEIGHT;
//            PlannerNode = PlannerNode.Next;
//        }
//
//        // Reset the first node's Prev pointer and the last node's Next pointer to mark the beginning and end of the linked list.
//        First.Prev = null;
//        Nodes[NodeCount - 1].Next = null;
//
//        // Allocate a lookup table to lookup CLPWWSections from CWWSections.
//        StLPSection SectionLookup[] = new StLPSection[mWWNetwork.getSectionCount()];
//        //SectionLookup.FillZero();
//
//        // Create a planner point at the source position and at the destination position.
//        int SrcPlannerPointIndex = 0, DstPlannerPointIndex = 0;
//        MutableInteger PointIndex = new MutableInteger(SrcPlannerPointIndex);
//        mSrcPlannerPoint = SrcSection.createPlannerPoint(PointIndex, SrcPos/*, mLegPlanner->mNLTidesDB, -1*/);
//        SrcPlannerPointIndex = PointIndex.getValue();
//
//        PointIndex.setValue(DstPlannerPointIndex);
//        mDstPlannerPoint = DstSection.createPlannerPoint(PointIndex, DstPos/*, mLegPlanner->mNLTidesDB, 1*/);
//        DstPlannerPointIndex = PointIndex.getValue();
//
//        // Create a vertex at the source.
//        StLPVertex VertexSrc = new StLPVertex(mRoutePointPrev, mRoutePointNext, mSrcPlannerPoint);
//
//        // Create a section that goes from the source to the source section's head node.
//        StLPSection SrcSectionHead = createSection(SrcSection);
//
//        // Calculate the environment state at the source traveling in the direction of the section's head node.
//        StLPState State = new StLPState(mSrcPlannerPoint.getCOGPrev(), mSrcPlannerPoint.getVMaxPrev(), mSrcPlannerPoint.getWWCurrentPrev(),
//                mSrcPlannerPoint.getPosGlb(), mSrcPlannerPoint.getPosGeo(), aPlannerState);
//        StLPBoatState BoatState = new StLPBoatState();
//        int Result = mLegPlanner.calcEnvAndBoatState(State, BoatState, VertexSrc, false);
//        if (Result == StLPRoutePoint.LPRESULT_OK) {
//            // Calculate the weight of the head node that can be reached from the source.
//            PlannerNode = Nodes[SrcSection.getHeadNode().getDataIndex()];
//            LPWWSectionPos Start = new LPWWSectionPos(SrcPos, mSrcPlannerPoint, SrcPlannerPointIndex);
//            LPWWSectionPos End = new LPWWSectionPos(0.0, SrcSection.getPlannerPoint(0), 0);
//            double Weight = calcWeight(State, BoatState, SrcSectionHead, Start, End);
//            if (Weight < PlannerNode.Weight) {
//                if (linkedList_ReduceWeight(First, PlannerNode, Weight))
//                    First = PlannerNode;
//                PlannerNode.OptimalPathNodePrev = null;
//                PlannerNode.OptimalPathSection = SrcSectionHead;
//                PlannerNode.EnvState = State.getEnvState();
//                PlannerNode.PlannerState.init(State.getPlannerState());
//            }
//        }
//
//        // Create a planner section that goes from the source to the source section's tail node.
//        StLPSection SrcSectionTail = createSection(SrcSection);
//
//        // Calculate the state at the source traveling in the direction of the section's tail node.
//        State.init(mSrcPlannerPoint.getCOGNext(), mSrcPlannerPoint.getVMaxNext(), mSrcPlannerPoint.getWWCurrentNext(), mSrcPlannerPoint.getPosGlb(),
//                mSrcPlannerPoint.getPosGeo(), aPlannerState);
//        Result = mLegPlanner.calcEnvAndBoatState(State, BoatState, VertexSrc, false);
//        if (Result == StLPRoutePoint.LPRESULT_OK) {
//            // Calculate the weight of the tail node that can be reached from the source.
//            PlannerNode = Nodes[SrcSection.getTailNode().getDataIndex()];
//            LPWWSectionPos Start = new LPWWSectionPos(SrcPos, mSrcPlannerPoint, SrcPlannerPointIndex + 1);
//            LPWWSectionPos End = new LPWWSectionPos(SrcSection.getLen(), SrcSection.getPlannerPoint(SrcSection.getPlannerPointCount() - 1), SrcSection.getPlannerPointCount() - 1);
//            double Weight = calcWeight(State, BoatState, SrcSectionTail, Start, End);
//            if (Weight < PlannerNode.Weight) {
//                if (linkedList_ReduceWeight(First, PlannerNode, Weight))
//                    First = PlannerNode;
//                PlannerNode.OptimalPathNodePrev = null;
//                PlannerNode.OptimalPathSection = SrcSectionTail;
//                PlannerNode.EnvState = State.getEnvState();
//                PlannerNode.PlannerState.init(State.getPlannerState());
//            }
//        }
//
//        // Check if the source section and destination section are equal. In this case the destination can be reached from the
//        // source immediately. Note that this is not necessarily the shortest path. It might be shorter to go via the source
//        // section's head or tail node. This is solved automatically once the correct weights are calculated for all three
//        // nodes.
//        StLPSection Dummy = new StLPSection(null);
//
//        if (SrcSection == DstSection) {
//            // Create a planner section that goes from the source to the destination.
//            StLPSection SrcSectionDst = createSection(SrcSection);
//
//            // Calculate the state at the source traveling in the direction of the destination.
//            float COG, VMax, WWCurrent;
//            if (SrcPos < DstPos) {
//                COG = mSrcPlannerPoint.getCOGNext();
//                VMax = mSrcPlannerPoint.getVMaxNext();
//                WWCurrent = mSrcPlannerPoint.getWWCurrentNext();
//            } else {
//                COG = mSrcPlannerPoint.getCOGPrev();
//                VMax = mSrcPlannerPoint.getVMaxPrev();
//                WWCurrent = mSrcPlannerPoint.getWWCurrentPrev();
//            }
//            State.init(COG, VMax, WWCurrent, mSrcPlannerPoint.getPosGlb(), mSrcPlannerPoint.getPosGeo(), aPlannerState);
//            Result = mLegPlanner.calcEnvAndBoatState(State, BoatState, VertexSrc, false);
//            if (Result == StLPRoutePoint.LPRESULT_OK) {
//
//                // Calculate the weight of the destination node that can be reached from the source.
//                PlannerNode = DstNode;
//                LPWWSectionPos Start, End;
//                if (SrcPos <= DstPos) {
//                    Start = new LPWWSectionPos(SrcPos, mSrcPlannerPoint, SrcPlannerPointIndex + 1);
//                    End = new LPWWSectionPos(DstPos, mDstPlannerPoint, DstPlannerPointIndex + 1);
//                } else {
//                    Start = new LPWWSectionPos(SrcPos, mSrcPlannerPoint, SrcPlannerPointIndex);
//                    End = new LPWWSectionPos(DstPos, mDstPlannerPoint, DstPlannerPointIndex);
//                }
//                double Weight = calcWeight(State, BoatState, SrcSectionDst, Start, End);
//                if (Weight < PlannerNode.Weight) {
//                    if (linkedList_ReduceWeight(First, PlannerNode, Weight))
//                        First = PlannerNode;
//                    PlannerNode.OptimalPathNodePrev = null;
//                    PlannerNode.OptimalPathSection = SrcSectionDst;
//                    PlannerNode.EnvState = State.getEnvState();
//                    PlannerNode.PlannerState.init(State.getPlannerState());
//                }
//            }
//        } else {
//            // The source section and destination section are not equal. Fill the lookup table entry with a non-null value to
//            // mark the source section as calculated.
//            SectionLookup[SrcSection.getDataIndex()] = Dummy; //(CLPSection*)0xffffffff;
//        }
//
//        // Start finding the shortest path.
//        while (true) {
//            // Get the node with the smallest weight relative to the source. This is the first node in the sorted linked list.
//            // Remove this node from the list.
//            LPWWNode PlannerNodeU = linkedList_RemoveFirst(First);
//            First = First.Next;
//
//            // If the node with the smallest weight has infinite weight, all other nodes also have infinite weight. This means
//            // that the destination is inaccessible from source.
//            if (PlannerNodeU.Weight == INFINITE_WEIGHT)
//                return false;
//
//            // If u equals mDstNode, the shortest path from source to destination has been found.
//            if (PlannerNodeU == DstNode) {
//                traceOptimalPath(DstNode);
//                // Update aState. It's value should be equal to the state of the last point.
//                aPlannerState.init(mLeg.getLastPoint().mPlannerState);
//                return true;
//            }
//
//            // Loop through all neighbours v of u.
//            final StWWNode NodeU = PlannerNodeU.Node;
//            for (int i = NodeU.getSectionCount() - 1; i >= 0; i--) {
//                final StWWSection Section = NodeU.getSection(i);
//                int LookupIndex = Section.getDataIndex();
//                StLPSection PlannerSection = SectionLookup[LookupIndex];
//
//                // If the section has a planner section already, it was calculated before and should be ignored. This prevents
//                // sections being calculated in both directions.
//                if (PlannerSection == null) {
//                    LPWWSectionPos Start, End;
//                    float COGStart, VMax, WWCurrent;
//                    LPWWNode PlannerNodeV;
//                    if (Section != DstSection) {
//                        // The section is not the destination section. The weight should be calculated from u to v through the entire
//                        // section.
//                        final StWWNode vNode;
//                        if (NodeU == Section.getHeadNode()) {
//                            Start = new LPWWSectionPos(0.0, Section.getPlannerPoint(0), 1);
//                            End = new LPWWSectionPos(Section.getLen(), Section.getPlannerPoint(Section.getPlannerPointCount() - 1), Section.getPlannerPointCount() - 1);
//                            COGStart = Start.PlannerPoint.getCOGNext();
//                            VMax = Start.PlannerPoint.getVMaxNext();
//                            WWCurrent = Start.PlannerPoint.getWWCurrentNext();
//                            vNode = Section.getTailNode();
//                        } else {
//                            Start = new LPWWSectionPos(Section.getLen(), Section.getPlannerPoint(Section.getPlannerPointCount() - 1), Section.getPlannerPointCount() - 2);
//                            End = new LPWWSectionPos(0.0, Section.getPlannerPoint(0), 0);
//                            COGStart = Start.PlannerPoint.getCOGPrev();
//                            VMax = Start.PlannerPoint.getVMaxPrev();
//                            WWCurrent = Start.PlannerPoint.getWWCurrentPrev();
//                            vNode = Section.getHeadNode();
//                        }
//                        PlannerNodeV = Nodes[vNode.getDataIndex()];
//
//                        // Create a planner section that goes from PosStart to PosEnd along Section.
//                        PlannerSection = createSection(Section);
//                        SectionLookup[LookupIndex] = PlannerSection;
//                    } else {
//                        // The section is the destination section. The weight should be calculated from u to the destination position.
//                        if (NodeU == Section.getHeadNode()) {
//                            Start = new LPWWSectionPos(0.0, Section.getPlannerPoint(0), 1);
//                            End = new LPWWSectionPos(DstPos, mDstPlannerPoint, DstPlannerPointIndex + 1);
//                            COGStart = Start.PlannerPoint.getCOGNext();
//                            VMax = Start.PlannerPoint.getVMaxNext();
//                            WWCurrent = Start.PlannerPoint.getWWCurrentNext();
//                        } else {
//                            Start = new LPWWSectionPos(Section.getLen(), Section.getPlannerPoint(Section.getPlannerPointCount() - 1), Section.getPlannerPointCount() - 2);
//                            End = new LPWWSectionPos(DstPos, mDstPlannerPoint, DstPlannerPointIndex);
//                            COGStart = Start.PlannerPoint.getCOGPrev();
//                            VMax = Start.PlannerPoint.getVMaxPrev();
//                            WWCurrent = Start.PlannerPoint.getWWCurrentPrev();
//                        }
//                        PlannerNodeV = DstNode;
//
//                        // Create a planner section that goes from PosStart to PosEnd along Section.
//                        PlannerSection = createSection(Section);
//                    }
//
//                    // Generate a vertex at node u.
//                    StLPVertex Vertex = new StLPVertex(mRoutePointPrev, mRoutePointNext, Start.PlannerPoint);
//
//                    // Initialize the state at node u and calculate the boat state at the node u traveling in the direction of node v.
//                    State.init(COGStart, VMax, WWCurrent, Start.PlannerPoint.getPosGlb(), Start.PlannerPoint.getPosGeo(), PlannerNodeU.PlannerState, PlannerNodeU.EnvState);
//                    Result = mLegPlanner.calcBoatState(BoatState, State, Vertex, false);
//                    if (Result == StLPRoutePoint.LPRESULT_OK) {
//
//                        // Calculate the weight of the section from u to v.
//                        double Weight = calcWeight(State, BoatState, PlannerSection, Start, End);
//                        if (Weight < PlannerNodeV.Weight) {
//                            if (linkedList_ReduceWeight(First, PlannerNodeV, Weight))
//                                First = PlannerNodeV;
//
//                            PlannerNodeV.OptimalPathNodePrev = PlannerNodeU;
//                            PlannerNodeV.OptimalPathSection = PlannerSection;
//                            PlannerNodeV.EnvState = State.getEnvState();
//                            PlannerNodeV.PlannerState.init(State.getPlannerState());
//                        }
//                    }
//                }
//            }
//        }
        return false
    }

//    private void getSectionPartRange(MutableInteger aStartIndex, MutableInteger aEndIndex, final StWWSection aSection, double aPosStart, double aPosEnd) {
//
//        int StartIndex = aStartIndex.getValue();
//        int EndIndex = aEndIndex.getValue();
//
//        if (aPosStart <= aPosEnd) {
//            // If the start position is at the beginning of the section, start at the first section, otherwise, find the first part.
//            StartIndex = 0;
//            if (aPosStart > 0.0) {
//                while (StartIndex < aSection.getSectionPartCount() && aSection.getSectionPart(StartIndex).getPosEnd() <= aPosStart)
//                    StartIndex++;
//            }
//
//            // If the end position is at the end of the section, stop at the last section, otherwise, find the last part.
//            EndIndex = aSection.getSectionPartCount() - 1;
//            if (aPosEnd < aSection.getLen()) {
//                while (EndIndex >= 0 && aSection.getSectionPart(EndIndex).getPosStart() >= aPosEnd)
//                    EndIndex--;
//            }
//        } else {
//            // If the start position is at the end of the section, stop at the last section, otherwise, find the last part.
//            StartIndex = aSection.getSectionPartCount() - 1;
//            if (aPosStart < aSection.getLen()) {
//                while (aSection.getSectionPart(StartIndex).getPosStart() >= aPosStart)
//                StartIndex--;
//            }
//
//            // If the end position is at the beginning of the section, start at the first section, otherwise, find the first part.
//            EndIndex = 0;
//            if (aPosEnd > 0.0) {
//                while (aSection.getSectionPart(EndIndex).getPosEnd() <= aPosEnd)
//                EndIndex++;
//            }
//        }
//
//        aStartIndex.setValue(StartIndex);
//        aEndIndex.setValue(EndIndex);
//    }
//
//    private void initOperatedStructure(StLPPassedPassage aPassedPassage, long aDateTimeArrival, final StWWStructure aStructure,
//                                       int aDescPassageIndex, float aClearance, final StWWOperation aOperation, double aOperationDelay,
//                                       int aPassDelay) {
//
//        // Apply delay until structure is operated.
//        long DateTimeEntry = (int)(aDateTimeArrival + aOperationDelay);
//
//        // Apply delay to pass structure.
////        TDateTime DateTimeExit = DateTimeEntry + aPassDelay / (double)SECSPERDAY;
//
//        aPassedPassage.init(DateTimeEntry, aStructure, aDescPassageIndex, aClearance, aOperation);
//    }
//
//    private void initStructure(StLPPassedPassage aPassedPassage, long aDateTimeArrival, final StWWStructure aStructure,
//                               int aDescPassageIndex, float aClearance) {
//
//        long DateTimeEntry = aDateTimeArrival;
//
//        StWWOperation Operation = new StWWOperation();
//        aPassedPassage.init(DateTimeEntry, aStructure, aDescPassageIndex, aClearance, Operation);
//    }
//
//    private boolean linkedList_ReduceWeight(LPWWNode aFirst, LPWWNode aNode, double aWeight) {
//
//        // Adjust the weight.
//        aNode.Weight = aWeight;
//
//        // If the node is at the correct position in the list, no further processing is required.
//        if (aNode == aFirst || aWeight >= aNode.Prev.Weight)
//            return false;
//
//        // Remove the node from the linked list.
//        if (aNode.Next != null)
//            aNode.Next.Prev = aNode.Prev;
//        aNode.Prev.Next = aNode.Next;
//
//        // Insert the node in the linked list at the correct position. The search is started at the top of the list, simply
//        // traversing the next-pointers. This is very efficient because a node who's weight is reduced, is connected to the
//        // node that was previously at the top of the list. Because connected nodes are usually near each other, this node
//        // ends up somewhere near the top of the list.
//        if (aWeight < aFirst.Weight) {
//            // Add the node at the beginning of the linked list.
//            aNode.Prev = null;
//            aNode.Next = aFirst;
//            aFirst.Prev = aNode;
//            //aFirst = aNode;
//            return true;
//        } else {
//            // Check if the node should be inserted in the linked list.
//            LPWWNode Node = aFirst;
//            while (Node.Next != null) {
//                Node = Node.Next;
//                if (aWeight < Node.Weight) {
//                    aNode.Prev = Node.Prev;
//                    Node.Prev.Next = aNode;
//                    aNode.Next = Node;
//                    Node.Prev = aNode;
//                    return false;
//                }
//            }
//            // This code-point should never be reached. If it were reached, it means that the node should be added at the end of
//            // the list. This cannot happen because weights are always reduced, so nodes can only move up in the list. If the
//            // node was already at the end of the list, it was not removed anyway.
//        }
//        return false;
//    }

    private func linkedList_RemoveFirst(aFirst :LPWWNode) -> LPWWNode {
        var Node = aFirst
//        if (aFirst.Next != nil) {
//            aFirst.Next.Prev = nil
//        }
        //aFirst = aFirst.Next;
        return Node
    }

//    private int passConstraint(final StWWConstraint aConstraint) {
//        switch (aConstraint.getConstraintClass()) {
//            case CURRENT: return StLPPoint.SECTIONRESULT_OK;
//            case DIRECTION: return passConstraintDirection((StWWConstraintDirection)aConstraint) ? StLPPoint.SECTIONRESULT_OK : StLPPoint.SECTIONRESULT_DIRECTION;
//            case PERMIT: return StLPPoint.SECTIONRESULT_OK;
//            case SHIPPINGCLASS: return passConstraintShippingClass((StWWConstraintShippingClass)aConstraint) ? StLPPoint.SECTIONRESULT_OK : StLPPoint.SECTIONRESULT_SHIPPING;
//            case VESSELSIZE: return passConstraintVesselSize((StWWConstraintVesselSize)aConstraint) ? StLPPoint.SECTIONRESULT_OK : StLPPoint.SECTIONRESULT_VESSELSIZE;
//            case VHFREPORT: return StLPPoint.SECTIONRESULT_OK;
//            case VHFSECTOR: return StLPPoint.SECTIONRESULT_OK;
//            default: return StLPPoint.SECTIONRESULT_OK;
//        }
//    }
//
//    private boolean passConstraintDirection(final StWWConstraintDirection aConstraint) {
//        if (aConstraint.getPositive() == mDirectionPositive)
//            return true;
//        else
//            return false;
//    }
//
//    private boolean passConstraintShippingClass(final StWWConstraintShippingClass aConstraint) {
//        byte Flag = mLegPlanner.mBoatDef.getCommercial() ? StWWDefs.WSF_COMMERCIAL : StWWDefs.WSF_RECREATION;
//        if ((aConstraint.getShippingFlags() & Flag) == Flag)
//            return true;
//        else
//            return false;
//    }
//
//    private boolean passConstraintVesselSize(final StWWConstraintVesselSize aConstraint) {
//        final StLPBoatDef BoatDef = mLegPlanner.mBoatDef;
//        if (BoatDef.getLength() > aConstraint.getLengthMax())
//            return false;
//
//        if (BoatDef.getWidth() > aConstraint.getWidthMax())
//            return false;
//
//        if (BoatDef.getHeight() > aConstraint.getHeightMax())
//            return false;
//
//        if (BoatDef.getDraught() > aConstraint.getDepthMax())
//            return false;
//
//        return true;
//    }
//
//    private int setStructResult(int aStructResult, int aNewResult) {
//        return (aStructResult != StLPPoint.SECTIONRESULT_OK) ? aNewResult : aStructResult;
//    }
//
//    private void traceOptimalPath(final LPWWNode aDstNode) {
//        // Get the planning.
//        StLPPlanning Planning = mLegPlanner.mPlanning;
//
//        // Trace the optimal path back from the destination to the source along all sections that are part of the optimal path.
//        LPWWNode Node = aDstNode;
//        while (Node != null) {
//            // Add the section to the path. Add to the end of the list. This results in reverse order, but is much faster than
//            // adding to the start of the list.
//            StLPSection Section = Node.OptimalPathSection;
//            mLeg.addSection(Section);
//            Section.mInOptimalPath = true;
//
//            // Get previous node in path.
//            Node = Node.OptimalPathNodePrev;
//        }
//
//        // Reverse the order of the sections in the path.
//        mLeg.reverseSectionOrder();
//
//        mLeg.setFirstPoint(mLeg.getSection(0).getPoint(0));
//        final StLPSection LastSection = mLeg.getSection(mLeg.getSectionCount() - 1);
//        mLeg.setLastPoint(LastSection.getPoint(LastSection.getPointCount() - 1));
//    }
//
//    private boolean traversePart(StLPState aState, StLPBoatState aBoatState, StLPSection aSection/*, final StWWSectionPart aPart*/,
//                                 final LPWWSectionPos aStart, final LPWWSectionPos aEnd, int aResult) {
//
//        StWWSection Section = aSection.mSection;
//
//        boolean FirstPoint = true;
//        StWWPlannerPoint P1 = aStart.PlannerPoint;
//        if (aStart.Pos < aEnd.Pos) {
//            for (int i = aStart.PlannerPointIndex; i <= aEnd.PlannerPointIndex; i++) {
//                // Get the next planner point.
//                final StWWPlannerPoint P0 = P1;
//                P1 = i == aEnd.PlannerPointIndex ? aEnd.PlannerPoint : Section.getPlannerPoint(i);
//
//                // Create a vertex at P1.
//                StLPVertex Vertex = new StLPVertex(mRoutePointPrev, mRoutePointNext, P1);
//
//                // Total length to travel
//                double PartLen = P1.getDistance() - P0.getDistance();
//
//                float WWCurrent = mDirectionPositive ? P1.getWWCurrentNext() : -P1.getWWCurrentNext();
//                if (mLegPlanner.calcLegPart(aState, aBoatState, Vertex, P1.getCOGNext(), P1.getVMaxNext(), WWCurrent, PartLen/*mLeg -> GetTravelMode()*/, false) != StLPRoutePoint.LPRESULT_OK)
//                    return false;
//
//                /*StLPPoint Point = */createPoint(aState, aBoatState, aSection, P1.getDistance(), /*FirstPoint ? */aResult/* : -1*/);
//                FirstPoint = false;
//            }
//        } else {
//            for (int i = aStart.PlannerPointIndex; i >= aEnd.PlannerPointIndex; i--) {
//                // Get the next planner point.
//                final StWWPlannerPoint P0 = P1;
//                P1 = i == aEnd.PlannerPointIndex ? aEnd.PlannerPoint : Section.getPlannerPoint(i);
//
//                // Create a vertex at P1.
//                StLPVertex Vertex = new StLPVertex(mRoutePointPrev, mRoutePointNext, P1);
//
//                double PartLen = P0.getDistance() - P1.getDistance();
//
//                float WWCurrent = mDirectionPositive ? P1.getWWCurrentPrev() : -P1.getWWCurrentPrev();
//                if (mLegPlanner.calcLegPart(aState, aBoatState, Vertex, P1.getCOGPrev(), P1.getVMaxPrev(), WWCurrent, PartLen/*mLeg -> GetTravelMode()*/, false) != StLPRoutePoint.LPRESULT_OK)
//                    return false;
//
//                /*StLPPoint Point = */createPoint(aState, aBoatState, aSection, P1.getDistance(), /*FirstPoint ? */aResult/* : -1*/);
//                FirstPoint = false;
//            }
//        }
//        return true;
//    }
//
//    static boolean LPWWPlannerFindOptimalPath(StLPPlannerState aPlannerState, StLPLeg aLeg, final StLPPlanner aLegPlanner, final StWWNetwork aWWNetwork)
//    {
//        MutableBoolean Found = new MutableBoolean(false);
//        StLPWWPlanner WWPlanner = new StLPWWPlanner(Found, aPlannerState, aLeg, aLegPlanner, aWWNetwork);
//        return Found.getValue();
//    }
//
///*    private class LPPassedPassage {
//        public Date DateTimeEntry;        // Entry date and time.
//        public Date DateTimeExit;         // Exit date and time.
//        //double              Fuel;                 // Amount of fuel used to pass the structure [m�].
//        //double              Money;                // Amount of money used to pass the structure (including fuel cost) [curreny unit].
//        public final StWWStructure Structure;            // Pointer to the waterway structure.
//        public int DescPassageIndex;     // Index of described passage of structure.
//        public float Clearance;            // Clearance of structure (doorvaarthoogte) [m].
//        //SWWOperation        Operation;            // Information about the operation.
//        //float               Toll;                 // Toll to pass structure [currency unit].
//        //bool                MustUnstepMast;       // True if mast must be unstepped to pass this structure.
//
//        public LPPassedPassage() {
//            DateTimeEntry = null;
//            DateTimeExit = null;
//            Structure = null;
//            DescPassageIndex = 0;
//            Clearance = 0;
//        }
//
//        public LPPassedPassage(Date aDateTimeEntry, Date aDateTimeExit, final StWWStructure aStructure, int aDescPassageIndex, float aClearance*//*, const SWWOperation &aOperation,
//               float aToll, bool aMustUnstepMast*//*) {
//            DateTimeEntry = aDateTimeEntry;
//            DateTimeExit = aDateTimeExit;
//            //Fuel = aFuel;
//            //Money = aMoney;
//            Structure = aStructure;
//            DescPassageIndex = aDescPassageIndex;
//            Clearance = aClearance;
//            //Operation = aOperation;
//            //Toll = aToll;
//            //MustUnstepMast = aMustUnstepMast;
//        }
//    }*/

    private class LPWWNode {
//        var Next :LPWWNode                 // Next in linked list of nodes.
//        var Prev :LPWWNode                 // Previous in linked list of nodes.
//        var Node :WWNode                 // Pointer to the waterway node.
//        LPWWNode OptimalPathNodePrev;  // Previous node in optimal path from source.
//        StLPSection OptimalPathSection;   // Section that connects Node with OptimalPathNodePrev.
//        double Weight;               // Weight to reach this node from the source node. Depending on the
//        // planner mode, weight can be time, distance or fuel consumption.
//        StLPEnvState     EnvState;             // Environment state for this node.
//        StLPPlannerState PlannerState = new StLPPlannerState();         // Planner state for this node.
    }

//    private class LPWWSectionPos {
//        public double Pos;               // Position along section.
//        StWWPlannerPoint PlannerPoint;      // Pointer to planner point at this point.
//        int PlannerPointIndex; // Index of planner point that is following this position in the direction of the planning.
//
//        //public LPWWSectionPos() {}
//        LPWWSectionPos(double aPos, final StWWPlannerPoint aPlannerPoint, int aPlannerPointIndex) {
//            Pos = aPos;
//            PlannerPoint = aPlannerPoint;
//            PlannerPointIndex = aPlannerPointIndex;
//        }
//
//        public void init(double aPos, final StWWPlannerPoint aPlannerPoint, int aPlannerPointIndex) {
//            Pos = aPos;
//            PlannerPoint = aPlannerPoint;
//            PlannerPointIndex = aPlannerPointIndex;
//        }
//
//        public void init(LPWWSectionPos aWWSectionPos) {
//            Pos = aWWSectionPos.Pos;
//            PlannerPoint = aWWSectionPos.PlannerPoint;
//            PlannerPointIndex = aWWSectionPos.PlannerPointIndex;
//        }
//    }
//
//    // Savepoint to commit or roll back the creation of points along a section.
//    private class LPSavepoint {
//        private StLPWWPlanner mPlanner;                   // Pointer to planner.
//        private boolean mRollBackLastHasStructure;  // True if the last point at creation of the savepoint has a structure.
//        private int mRollBackPointCount;        // Number of points when the savepoint was created.
//        private StLPSection mSection;                   // Pointer to section.
//
//        public LPSavepoint(StLPWWPlanner aPlanner, StLPSection aSection) {
//            mPlanner = aPlanner;
//            mRollBackLastHasStructure = mRollBackPointCount > 0 && aSection.mPoints.get(mRollBackPointCount - 1).mStructure != null;
//            mRollBackPointCount = aSection.mPoints.size();
//            mSection = aSection;
//        }
//
////        public void commit() {
////        }
//
//        public void rollBack() {
//            // Remove the points from the section that were created after creation of the savepoint.
//            mSection.mPoints.subList(mRollBackPointCount, mSection.mPoints.size()).clear();
//
////            if (mRollBackPointCount > 0 && !mRollBackLastHasStructure) {
////                StLPPoint Point = mSection.mPoints.get(mRollBackPointCount - 1);
////                if (Point.mStructure)
////                    DeleteAndNULL(Point->mStructure);
////            }
//
//        }
//    }
}
