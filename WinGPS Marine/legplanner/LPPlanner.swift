//
//  LPPlanner.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 30/07/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class LPPlanner{ // } extends AsyncTask<Void, Void, Boolean> {

//    private static final float MIN_SOG_REACHABLE = 0.01f;  // Minimum SOG. A routepoint is considered unreachable when the SOG is smaller that this value
//
//    StLPBoatDef mBoatDef;           // Boat definition.
//    private StLPBoatState mCurBoatState;    // Current boat state (only set for an active route).
//    private StLPEnvState mCurEnvState;      // Current environment state (only set for an active route).
//    private boolean mImposeSpeedLimit;      // True if speed limit should be imposed.
//    private boolean mIsActiveRoute; // True if the currently planned route is the active route.
//    private Handler mMainHandler;         // This is a handler to notify when databases have been loaded
//    StLPPlanning mPlanning;
//    StLPSettings mSettings;         // Planner settings that apply to all legs.
//    private boolean mUseWWCurrent;  // If true, use currents from the waterway network.
//    float mVMGFiltered;             // Filtered VMG (only set for an active route).
//    private StWWNetwork mWWNetwork;
//
//    private final StAppRoute mRoute;
//    private final StAppRoutepoint mRoutePointPrev;
//    private final StAppRoutepoint mRoutePointActive;
//    private final StWptCurState mCurState;
//
//
//    public StLPPlanner(final StWWNetwork aWWNetwork, Handler aMainHandler, final StLPBoatDef aBoatDef, final StAppRoute aRoute) {
//        mBoatDef = aBoatDef;
//        mCurBoatState = new StLPBoatState();
//        mImposeSpeedLimit = true;
//        mIsActiveRoute = false;
//        mMainHandler = aMainHandler;
//        mPlanning = null;
//        mRoute = aRoute;
//        mSettings = null;
//        mUseWWCurrent = false;
//        mVMGFiltered = 0.0f;
//        mWWNetwork = aWWNetwork;
//        mRoutePointPrev = null;
//        mRoutePointActive = null;
//        mCurState = null;
//    }
//
//    public StLPPlanner(final StWWNetwork aWWNetwork, Handler aMainHandler, final StLPBoatDef aBoatDef, final StAppRoute aRoute, final StAppRoutepoint aRoutePointPrev,
//        final StAppRoutepoint aRoutePointActive, final StWptCurState aCurState) {
//
//        mBoatDef = aBoatDef;
//        mCurBoatState = new StLPBoatState();
//        mImposeSpeedLimit = true;
//        mIsActiveRoute = false;
//        mMainHandler = aMainHandler;
//        mPlanning = null;
//        mRoute = aRoute;
//        mSettings = null;
//        mUseWWCurrent = false;
//        mVMGFiltered = 0.0f;
//        mWWNetwork = aWWNetwork;
//        mRoutePointPrev = aRoutePointPrev;
//        mRoutePointActive = aRoutePointActive;
//        mCurState = aCurState;
//    }
//
//    int calcBoatState(StLPBoatState aBoatState, final StLPState aState, final StLPVertex aVertex/*, ELPTravelMode aTravelMode*/, boolean aUseCurState) {
//
////        Assert(aState.GetPlannerState().Time > 0.0, L"Time is undefined");
//
//        // aUseCurrentState is true for the active open leg with VMGFiltered > MIN_VMG. In this case, use the current boat
//        // state. Determine the travel result and the fuel flow based on the travel mode.
//        if (aUseCurState)
//            aBoatState.init(mCurBoatState);
///*
//            // Determine the travel result and the fuel flow based on the travel mode.
//            if (aTravelMode == ltmMotoring) {
//                aBoatState.TravelResult = ltrMotoring;
//                aBoatState.FuelFlow = mBoatDef->GetFuelCurve().CalcFuelFlow(aBoatState.STW);
//            } else {
//                aBoatState.TravelResult = ltrSailing;
//                aBoatState.FuelFlow = 0.0f;
//            }
//            return lprOk;
//        }
//
//        // Calculate HDG, COG, SOG and STW.
//*/
//        boolean BoatStateValid = calcBoatStateInternal(aBoatState, aState);
///*
//        switch (aTravelMode) {
//            case ltmMotoring:
//                // Calculate the boat state when motoring.
//               BoatStateValid = CalcBoatStateInternal(aBoatState, aState, true, aPolar, aMoored);
//                break;
//            case ltmSailing: {
//                // Calculate the boat state when sailing.
//                BoatStateValid = CalcBoatStateInternal(aBoatState, aState, false, aPolar, aMoored);
//                // If the SOG is too low, recalculate the boat state when motoring, except if the boat is a racing sailing boat.
//                float SOG = BoatStateValid ? aBoatState.SOG : 0.0f;
//                if (!aMoored && mBoatDef->GetBoatType() != lbtSailingboatRacing && SOG < mBoatDef->GetMotorLowSOG())
//                    BoatStateValid = CalcBoatStateInternal(aBoatState, aState, true, aPolar, aMoored);
//                break;
//            }
//            case ltmSailingNoTacking: {
//                // Calculate the boat state when sailing.
//                BoatStateValid = CalcBoatStateInternal(aBoatState, aState, false, aPolar, aMoored);
//                // If the SOG is too low or if the result is tacking, recalculate the boat state when motoring, except if the boat
//                // is a racing sailing boat.
//                float SOG = BoatStateValid ? aBoatState.SOG : 0.0f;
//                if (!aMoored && mBoatDef->GetBoatType() != lbtSailingboatRacing &&
//                        (SOG < mBoatDef->GetMotorLowSOG() || aBoatState.TravelResult == ltrTacking))
//                BoatStateValid = CalcBoatStateInternal(aBoatState, aState, true, aPolar, aMoored);
//                break;
//            }
//        }
//*/
//        return (BoatStateValid) ? StLPRoutePoint.LPRESULT_OK : StLPRoutePoint.LPRESULT_WINDANDCURRENT;
//    }
//
//    private boolean calcBoatStateInternal(StLPBoatState aBoatState, final StLPState aState) {
//        // Determine SOG, STW, HDG, travel mode and fuel flow.
//        MutableFloat SOG = new MutableFloat();
//        MutableFloat STW = new MutableFloat();
//        MutableFloat HDG = new MutableFloat();
//        if (!calcVectorsMotoring(SOG, STW, HDG, aState.getCOG(), aState.getVMax(), aState.getWWCurrent(), mSettings)) {
//            aBoatState.init(aState.getCOG(), aState.getCOG(), 0.0f, 0.0f);
//            return false;
//        }
//
//        aBoatState.init(aState.getCOG(), HDG.getValue(), SOG.getValue(), STW.getValue());
//        return true;
//    }
//
//    int calcEnvAndBoatState(StLPState aState, StLPBoatState aBoatState, final StLPVertex aVertex,/* ELPTravelMode aTravelMode,*/ boolean aUseCurState) {
///*
//        // Calculate state of the environment.
//        bool DepthAvailable;
//        CalcEnvState(aState, aVertex, aUseCurState, DepthAvailable);
//*/
//        // Calculate boat state.
////        int lpResult = calcBoatState(aBoatState, aState, aVertex, aUseCurState);
//        return calcBoatState(aBoatState, aState, aVertex, aUseCurState);
///*
//        if (lpResult != lprOk)
//            return lpResult;
//
//        // If depth is lower than 1/3 of the ships length, linearly lower speed until 1/3 of the speed at depth 0
//        if (GetWptRouteDefaults().DepthDepSpeed && aState.GetEnvState().DepthAvailable) {
//            float MinFactor = THIRDf;
//            float MaxDepth = mBoatDef->GetLength() / 3;
//            float Factor = (aState.GetEnvState().DepthTotal / MaxDepth) * (1 - MinFactor) + MinFactor;
//            Factor = Limit(Factor, MinFactor, 1.0f);
//            //SOG = aBoatState.SOG;
//            aBoatState.SOG = aBoatState.SOG * Factor;
//        }
//
//        if (GetWptRouteDefaults().CalcDryingOut && ((DepthAvailable && mSettings.SrcTideHeight == lsthGRIB && aVertex.UseNAPDepth) || (mSettings.SrcTideHeight == lsthNLTides && aVertex.NLTidesRefStation))) {
//            // Check whether we have dryed out and if so calculate when we can sail again.
//            float MinDepthTolerance = mBoatDef->GetMinDepthTolerance();
//            float DepthUKC = aState.GetEnvState().DepthUKC + MinDepthTolerance;
//            if (DepthUKC <= MinDepthTolerance) {
//                if (aOptimization) {
//                    // While optimizing, reduce SOG when UKC is smaller than the minimal depth tolerance
//                    float SOG = aBoatState.SOG * Max(0.0f, DepthUKC / MinDepthTolerance);
//                    int i = 0;
//                    while (i < 86400 && SOG == 0) {
//                        aState.SetPlannerState(aState.GetPlannerState().DistSOP, aState.GetPlannerState().FuelSOP,
//                                aState.GetPlannerState().MoneySOP, aState.GetPlannerState().Time + (300 / (double)SECSPERDAY));
//                        CalcEnvState(aState, aVertex, aUseCurState, DepthAvailable);
//                        lpResult = CalcBoatState(aBoatState, aState, aVertex, aPolar, aTravelMode, aMoored, aUseCurState);
//                        if (lpResult != lprOk)
//                            return lpResult;
//
//                        DepthUKC = aState.GetEnvState().DepthUKC + MinDepthTolerance;
//                        SOG = aBoatState.SOG * Max(0.0f, DepthUKC / MinDepthTolerance);
//                        i += 300;
//                    }
//                    if (SOG == 0) {
//                        aBoatState.Init(aState.GetCOG(), false, 0.0f, aState.GetCOG(), 0.0f, 0.0f, ltrMoored);
//                        return lprDepth;
//                    } else {
//                        aBoatState.SOG = SOG;
//                        aBoatState.DryedOut = true;
//                    }
//                } else {
//                    // Run until UKC > MinDepthTolerance for a maximum of 24 hours
//                    int i = 0;
//                    while (i < 86400 && DepthUKC <= MinDepthTolerance) {
//                        aState.SetPlannerState(aState.GetPlannerState().DistSOP, aState.GetPlannerState().FuelSOP,
//                                aState.GetPlannerState().MoneySOP, aState.GetPlannerState().Time + (300 / (double)SECSPERDAY));
//                        CalcEnvState(aState, aVertex, aUseCurState, DepthAvailable);
//                        lpResult = CalcBoatState(aBoatState, aState, aVertex, aPolar, aTravelMode, aMoored, aUseCurState);
//                        if (lpResult != lprOk)
//                            return lpResult;
//
//                        DepthUKC = aState.GetEnvState().DepthUKC + MinDepthTolerance;
//                        i += 300;
//                    }
//                    if (DepthUKC <= MinDepthTolerance) {
//                        aBoatState.Init(aState.GetCOG(), false, 0.0f, aState.GetCOG(), 0.0f, 0.0f, ltrMoored);
//                        return lprDepth;
//                    } else {
//                        aBoatState.DryedOut = true;
//                    }
//                }
//            }
//        } else if (aState.GetEnvState().DepthUKC < 0.0f) {
//            aBoatState.Init(aState.GetCOG(), false, 0.0f, aState.GetCOG(), 0.0f, 0.0f, ltrMoored);
//            return lprDepth;
//        }
//*/
////        return StLPRoutePoint.LPRESULT_OK;
//    }
//
//    int calcLegPart(StLPState aState, StLPBoatState aBoatState, final StLPVertex aVertexEnd, float aCOGEnd, float aVMaxEnd, float aWWCurrentEnd, double aLen
//                            /*ELPTravelMode aTravelMode, bool aOptimization*/, boolean aUseCurState) {
//
//        int Result = StLPRoutePoint.LPRESULT_OK;
//
//        // Calculate Time.
//        double V = aUseCurState ? mVMGFiltered : aBoatState.SOG;
//        double t = aLen / V;
//        long Time = aState.getPlannerState().Time + (int)(t * 1000);// / (double) SECSPERDAY;
//
//        // Calculate DistSOP.
//        double DistSOP = aState.getPlannerState().DistSOP + aLen;
//
//        // Calculate FuelSOP and MoneySOP.
///*
//        double FuelSOP = aState.getPlannerState().FuelSOP;
//        double MoneySOP = aState.GetPlannerState().MoneySOP;
//        if (aBoatState.TravelResult == ltrMotoring) {
//            double Fuel = t * aBoatState.FuelFlow;
//            FuelSOP += Fuel;
//            MoneySOP += Fuel * mSettings.FuelPrice;
//        }
//*/
//        // Initialize the state with the new values.
//        StLPPlannerState PlannerState = new StLPPlannerState(DistSOP/*, FuelSOP, MoneySOP*/, Time);
//        //float WWCurrent = (mSettings.UseWWCurrent) ? aWWCurrentEnd : 0.0f;
//        aState.init(aCOGEnd, aVMaxEnd, 0.0f, aVertexEnd.PosGlb, aVertexEnd.PosGeo, PlannerState);
//
//        // Calculate the state of the environment and the boat.
//        return calcEnvAndBoatState(aState, aBoatState, aVertexEnd/*aTravelMode, aOptimization*/, aUseCurState);
//    }
//
//    private float calcSTWMax(float aSOGMax, final Float2 aVAbsDir/*, const float2 &aCurrent*/) {
//        // Calculate VAbsMax.
//        Float2 VAbsMax = new Float2(aVAbsDir);
//        VAbsMax.multiply(aSOGMax);
//
//        // Calculate STWMax.
//        return StMath.len(VAbsMax);
//    }
//
//    private boolean calcVectorsMotoring(MutableFloat aSOG, MutableFloat aSTW, MutableFloat aHDG, float aCOG, float aSOGMax, float aWWCurrent, final StLPSettings aSettings) {
//
//        // The STW is equal to the user-defined motoring speed plus the current from the ww network.
//        //float STWFuel = aSettings.MotoringSpeed;
//        aSTW.setValue(aSettings.MotoringSpeed + aWWCurrent);
//
//        // Calculate normalized direction vector of COG.
//        Float2 VAbsDir = new Float2((float)Math.cos(aCOG), (float)Math.sin(aCOG));
//
//        // Calculate the maximum STW to impose the speed limit.
//        float STWMax = aSettings.ImposeSpeedLimit && aSOGMax < StWWDefs.WW_INF ? calcSTWMax(aSOGMax, VAbsDir/*, aCurrent*/) : StWWDefs.WW_INF;
//
///*
//        if (aSettings.ReduceFuelOnTailWind || aSettings.ReduceSpeedOnHeadWind) {
//            // Calculate WoW.
//            float2 WoW = aWind - aCurrent;
//
//            // Calculate component of WoW in direction of VAbs.
//            float VWind = Dot(WoW, VAbsDir);
//
//            if (VWind < -REDUCE_WIND_VMIN) {
//                // Headwind.
//                float Factor = (-VWind - REDUCE_WIND_VMIN) / (REDUCE_WIND_V0 - REDUCE_WIND_VMIN);
//                if (aSettings.ReduceSpeedOnHeadWind) {
//                    // Reduce STW due to head wind.
//                    //float STWFuel = aSTW;
//                    aSTW = Max(0.25f * aSTW, aSTW - Factor * aBoatDef.GetReductionSpeed());
//                    if (aSTW > STWMax) {
//                        STWFuel = STWFuel * STWMax / aSTW;
//                        aSTW = STWMax;
//                    }
//                    aFuelFlow = aFuelCurve.CalcFuelFlow(STWFuel);
//                } else {
//                    // Increase fuel flow due to head wind.
//                    if (aSTW > STWMax)
//                        aSTW = STWMax;
//                    aFuelFlow = aFuelCurve.CalcFuelFlow(STWFuel);
//                    aFuelFlow += Factor * aBoatDef.GetReductionFuel();
//                }
//            } else if (VWind > REDUCE_WIND_VMIN) {
//                // Tailwind.
//                float Factor = (VWind - REDUCE_WIND_VMIN) / (REDUCE_WIND_V0 - REDUCE_WIND_VMIN);
//                if (aSettings.ReduceFuelOnTailWind) {
//                    // Reduce fuel flow due to tail wind.
//                    if (aSTW > STWMax)
//                        aSTW = STWMax;
//                    aFuelFlow = aFuelCurve.CalcFuelFlow(STWFuel);
//                    aFuelFlow = Max(0.25f * aFuelFlow, aFuelFlow - Factor * aBoatDef.GetReductionFuel());
//                } else {
//                    // Increase STW due to tail wind.
////        float STWFuel = aSTW;
//                    aSTW += Factor * aBoatDef.GetReductionSpeed();
//                    if (aSTW > STWMax) {
//                        STWFuel = STWFuel * STWMax / aSTW;
//                        aSTW = STWMax;
//                    }
//                    aFuelFlow = aFuelCurve.CalcFuelFlow(STWFuel);
//                }
//            } else {
//                // Simply impose speed limit and calculate fuel flow.
//                if (aSTW > STWMax)
//                    aSTW = STWMax;
//                aFuelFlow = aFuelCurve.CalcFuelFlow(STWFuel);
//            }
//        } else {*/
//            // Simply impose speed limit and calculate fuel flow.
//        if (aSTW.getValue() > STWMax)
//            aSTW.setValue(STWMax);
//
//        // Calculate current perpendicular to COG.
//        Float2 Current = new Float2(0, 0);
//        float LenCP = Math.abs(StMath.dot(Current, Float2.makeFloat2(VAbsDir.y, -VAbsDir.x)));
//
//        // If the STW is not larger than the perpendicular current, the STW is too low in comparison to the current to
//        // maintain the course indicated by aCOG.
//        if (aSTW.getValue() < LenCP)
//            return false;
//
//        // Calculate current in direction of COG.
//        float LenC = StMath.dot(Current, VAbsDir);
//
//        // Calculate SOG.
//        aSOG.setValue(LenC + (float)Math.sqrt(aSTW.getValue() * aSTW.getValue() - LenCP * LenCP));
//        if (aSOG.getValue() < MIN_SOG_REACHABLE)
//            return false;
//
//        // Calculate STW vector.
//        Float2 VAbs = new Float2(VAbsDir);
//        VAbs.multiply(aSOG.getValue());
//        Float2 STW = new Float2(VAbs);
//
//        // Assume a drift angle of 0, so the HDG is the angle of the STW.
//        aHDG.setValue((aSTW.getValue() >= 0.01) ? (float)Math.atan2(STW.y, STW.x) : aCOG);
//        return true;
//    }
//
//    @Override
//    protected Boolean doInBackground(Void... params) {
//        mRoute.setIsBeingPlanned(true);
//        Log.i("LPPlanner", "Starting planning of route: " + mRoute.getName());
//
//        // Get the planning from the route.
////        StLPPlanning Planning = mRoute.getPlanning();
//
//        // Clear all old planning information.
//        long Time = mRoutePointActive != null ? mCurState.DateTime : mRoute.getStartTime();
//  //      Planning.reset(Time);
//
//        mPlanning = new StLPPlanning(Time);
//
//        // Initialize settings of leg planner, altering conflicting settings.
//        mSettings = new StLPSettings(mImposeSpeedLimit, mRoute.getMotoringSpeed(), mUseWWCurrent, mRoute.getWaterBodyMask());
//
//        // In case of an active route, create a temporary route point at the boat location and an active leg from the boat
//        // position to the active point.
//        StLPRoutePoint Prev = null;
//        if (mRoutePointActive != null) {
//            // The first leg starts at the current position. Create a route point at that location. If the current state
//            // contains a waterway section, snap the position to the network.
//            Geo2 PosGeo = mCurState.PosGeo;
//            if (mCurState.WWSection != null) {
//                Double3 PosGlb = new Double3();
//                mCurState.WWSection.calcPosGlb(PosGlb, mCurState.WWSectionPos);
//                StGeodeticCalibration.globalToGeo2(PosGlb, PosGeo);
//            }
//
//            Prev = mPlanning.createRoutePoint("ActiveLegStart", -1/*, ManualCurrent, ManualDepthLAT, ManualDepthNAP, ManualWind, NULL, 0.0f, 1.0f, false*/,
//                    PosGeo, mCurState.WWSection, mCurState.WWSectionPos);
//
//        }
//
//        // Reset or create new planning information of each route point.
//        boolean CreatePoints = mRoutePointActive == null;
//        StLPRoutePoint RPs[] = new StLPRoutePoint[mRoute.getRoutePointCount()];
//        for (int i = 0; i < mRoute.getRoutePointCount(); i++) {
//            // Get route point and reset it's planning.
//            StAppRoutepoint Point = mRoute.getRoutePoint(i);
//            RPs[i] = null;
//            //Point.setPlannedRoutePoint(null);
//
//            // Only create route points and legs in the planner for legs after aFirstPoint.
//            if (Point == mRoutePointActive)
//                CreatePoints = true;
//            if (CreatePoints) {
//                // Construct a name for the planner point.
//                String Name = Point.getName(true);
//                int PointIndex = Point.getIndexInRoute();
//                // Check if the section is part of the selected waterbodies.
//                StWWSection Section = Point.getSection();
//                double SectionPos = Point.getSectionPos();
//
//                // cannot plan legs that are not attached to the ww network
//                boolean FoundSection = false;
//                if (Section != null && ((mRoute.getWaterBodyMask() & StWWDefs.WWWaterbodyIDToMask(Section.getWaterBodyID())) == 0)) {
//                    Section = null;
//                    SectionPos = 0.0;
//                    FoundSection = false;
//                }
//                if (FoundSection) {
//                }
//
//                // Create a planner route point.
//                StLPRoutePoint RoutePoint = mPlanning.createRoutePoint(Name, PointIndex, Point.getPosition(), Section, SectionPos);
//                RPs[i] = RoutePoint;
//                //Point.setPlannedRoutePoint(RoutePoint);
//
//                // Create a leg if it is not the first point.
//                if (Prev != null) {
//                    //ELPTravelMode TravelMode = Point->GetTravelMode();
//                    final StLPLeg Leg = mPlanning.createLeg(Prev, RoutePoint/*TravelMode*/);
//                }
//                Prev = RoutePoint;
//            }
//        }
//
//        // Routes with zero points should not be planned.
//        if (mPlanning.getRoutePointCount() == 0)
//            return true;
//
//        // Get the current boat and environemnt state if the route is active.
//        StLPBoatState CurBoatState = null;
//        StLPEnvState CurEnvState = null;
//        float VMGFiltered = 0;
//        if (mCurState != null) {
//            CurBoatState = mCurState.BoatState;
//            //CurBoatStatePtr = &CurBoatState;
//            CurEnvState = mCurState.EnvState;
//            //CurEnvStatePtr = &CurEnvState;
//            VMGFiltered = mCurState.VMGFiltered;
//            //VMGFilteredPtr = &VMGFiltered;
//        }
//
//        // Plan all legs.
//        try { // can throw IndexOutOfBoundsException in StWWSection.createPlannerPoint (StWWSection.java:111)
//            planLegs(mSettings, CurBoatState, CurEnvState, VMGFiltered);
//        } catch (IndexOutOfBoundsException e){
//
//        }
//
//        for (int i = 0; i < RPs.length; i++)
//            if (mRoute.getRoutePoint(i) != null)
//                mRoute.getRoutePoint(i).setPlannedRoutePoint(RPs[i]);
//        mRoute.setPlanning(mPlanning);
//        return true;
//    }
//
//    public int getWaterBodyMask() {
//        return mSettings.WaterBodyMask;
//    }
//
//    @Override
//    protected void onPostExecute(Boolean Result) {
//        mRoute.setIsBeingPlanned(false);
//        Log.i("LPPlanner", "Planning of route: " + mRoute.getName() + " complete.");
//        if (mMainHandler != null) {
//            Message msg = Message.obtain();
//            msg.what = AppConstants.ROUTE_PLANNED;
//            msg.obj = mRoute;
//            mMainHandler.sendMessage(msg);
//        }
//    }
//
//    @Override
//    protected void onCancelled(Boolean Result) { // this function is called from API level 11 onwards
//        mRoute.setIsBeingPlanned(false);
//    }
//
//    @Override
//    protected void onCancelled() { // this function is called from API level 3
//        mRoute.setIsBeingPlanned(false);
//    }
//
//    private void planLeg(StLPPlannerState aPlannerState, StLPLeg aLeg, boolean aIsActiveLeg) {
//
//        // Get route points at start and end of leg.
//        StLPRoutePoint RoutePointPrev = aLeg.getRoutePointPrev();
//        StLPRoutePoint RoutePointNext = aLeg.getRoutePointNext();
//
//        // Normally, the result of a route point is the result of the previous leg. If there is no previous leg, the result of
//        // the route point is set to lprOk.
//        if (RoutePointPrev.getLegPrev() == null)
//            RoutePointPrev.setResult(StLPRoutePoint.LPRESULT_OK);
//
//        // If the previous route point is not reachable, the next is also not reachable.
//        if (RoutePointPrev.getResult() > StLPRoutePoint.LPRESULT_PREVPOINT) {
//            RoutePointNext.setResult(StLPRoutePoint.LPRESULT_PREVPOINT);
//            return;
//        }
//
//        // Calculate the leg depending on if it is an open leg or a waterway leg.
//        int Result = (aLeg.getIsOpen()) ? planLegOpen(aPlannerState, aLeg, aIsActiveLeg) : planLegWW(aPlannerState, aLeg, aIsActiveLeg);
//
//        // Add the points of this leg to the planning.
//        mPlanning.addPoints(aLeg);
//
//        // Generate the section groups and indications of this leg.
//        //aLeg.GenerateSectionGroupsAndIndications();
//
//        // The result of the next route point is the result of the leg.
//        RoutePointNext.setResult(Result);
//
//        // If this is the first leg, the previous route point has no point of arrival. Let it point to the first point of this leg.
//        if (RoutePointPrev.getLegPrev() == null) {
//            RoutePointPrev.setPointArrival(aLeg.getFirstPoint());
//        }
//
//        // Set the departure point to the first point of the leg.
//        RoutePointPrev.setPointDeparture(aLeg.getFirstPoint());
//
//        // Set the arrival point to the last point of the leg, but only if the result is ok. If the result is not ok,
//        // calculation of the leg stopped somewhere between the route points.
//        if (Result == StLPRoutePoint.LPRESULT_OK)
//            RoutePointNext.setPointArrival(aLeg.getLastPoint());
//    }
//
//    private void planLegs(final StLPSettings aSettings, final StLPBoatState aCurBoatState, final StLPEnvState aCurEnvState, float aVMGFiltered) {
//
//        //mPlanning = aPlanning;
////        mBoatDef = aBoatDef;
//        mSettings = aSettings;
//        mIsActiveRoute = aCurBoatState != null;
//        if (mIsActiveRoute) {
//            mCurBoatState = aCurBoatState;
//            mCurEnvState = aCurEnvState;
//            mVMGFiltered = aVMGFiltered;
//        }
//
//        // Initialize planner state.
//        StLPPlannerState PlannerState = new StLPPlannerState(0.0, mPlanning.mTimeStart.getTime());
//
//        // Plan each leg.
//        boolean IsActiveLeg = mIsActiveRoute;
//        int LegCount = mPlanning.getLegCount();
//        for (int i = 0; i < LegCount; i++) {
//            planLeg(PlannerState, mPlanning.getLeg(i), IsActiveLeg);
//            IsActiveLeg = false;
//        }
//
//        // Reset pointers.
////        mPlanning = null;
////        mBoatDef = null;
//    }
//
//    private int planLegOpen(StLPPlannerState aPlannerState, StLPLeg aLeg, boolean aIsActiveLeg) {
//        return StLPOLPlanner.LPOLPlannerFindOptimalPath(aPlannerState, aLeg, aIsActiveLeg, this);
//    }
//
//    private int planLegWW(StLPPlannerState aPlannerState, StLPLeg aLeg, boolean aIsActiveLeg) {
//        return StLPWWPlanner.LPWWPlannerFindOptimalPath(aPlannerState, aLeg, this, mWWNetwork) ? StLPRoutePoint.LPRESULT_OK : StLPRoutePoint.LPRESULT_PATH;
//    }
//
///*
//    public void planRoute(final StLPBoatDef aBoatDef, final StAppRoute aRoute) {
//        planRouteInternal(aBoatDef, aRoute, null, null, null);
//    }
//
//    public void planRouteActive(final StLPBoatDef aBoatDef, final StAppRoute aRoute, final StAppRoutepoint aRoutePointPrev,
//                                final StAppRoutepoint aRoutePointActive, final StWptCurState aCurState) {
//        planRouteInternal(aBoatDef, aRoute, aRoutePointPrev, aRoutePointActive, aCurState);
//    }
//
//*/
//    private void planRouteInternal(final StLPBoatDef aBoatDef, final StAppRoute aRoute, final StAppRoutepoint aRoutePointPrev, final StAppRoutepoint aRoutePointActive, final StWptCurState aCurState) {
//
//        //if (aRoute.getIsBeingPlanned())
//            // stop planning
//
///*
//        // Get the planning from the route.
//        StLPPlanning Planning = aRoute.getPlanning();
//
//        // Clear all old planning information.
//        long Time = aRoutePointActive != null ? aCurState.DateTime : aRoute.getStartTime();
//        Planning.reset(Time);
//*/
//        // Initialize settings of leg planner, altering conflicting settings.
///*
//        int MaxDelay;
//        switch (aRoute.GetPlannerMode()) {
//            case lpmDistance : MaxDelay = 3 * SECSPERDAY; break;
//            case lpmFuel     : MaxDelay = 3 * SECSPERDAY; break;
//            case lpmMoney    : MaxDelay = 3 * SECSPERDAY; break;
//            default: MaxDelay = 0x7fffffff; break;
//        }
//        float MotoringSpeed;
//        switch (aRoute.GetMotoringSpeed()) {
//            case lmsLow  : MotoringSpeed = aBoatDef->GetVLow(); break;
//            case lmsHigh : MotoringSpeed = aBoatDef->GetVHigh(); break;
//            default      : MotoringSpeed = aBoatDef->GetVCruise();
//        }
//        ELPSourceCurrent SrcCurrent = aRoute.GetSrcCurrent();
//        CGRIBParam *ParamCurrent = aRoute.GetParamCurrent();
//        switch (SrcCurrent) {
//            case lscGRIB   : if (!mGRIBManager) {
//                SrcCurrent = lscNone;
//                ParamCurrent = NULL;
//            } else if (ParamCurrent && !mGRIBManager->HasParam(ParamCurrent)) {
//                ParamCurrent = NULL;
//            } else if (ParamCurrent && !ParamCurrent->GetActive()) {
//                ParamCurrent = NULL;
//            }
//                break;
//            case lscNLTides: if (!mNLTidesDB)
//                SrcCurrent = lscNone; break;
//        }
//        ELPSourceTideHeight SrcTideHeight = aRoute.GetSrcTideHeight();
//        if (SrcTideHeight == lsthNLTides && !mTideFileManager)
//            SrcTideHeight = lsthNone;
//        if (SrcTideHeight == lsthGRIB && !mGRIBManager) {
//            SrcTideHeight = lsthNone;
//        } else if (SrcTideHeight == lsthGRIB && !mGRIBManager->GetParamByID(gpiWaterDepth)) {
//            SrcTideHeight = lsthNone;
//        }
//        ELPSourceWind SrcWind = aRoute.GetSrcWind();
//        CGRIBParam *ParamWind = aRoute.GetParamWind();
//        if (SrcWind == lswGRIB && !mGRIBManager) {
//            SrcWind = lswNone;
//            ParamWind = NULL;
//        } else if (ParamWind && !mGRIBManager->HasParam(ParamWind)) {
//            ParamWind = NULL;
//        } else if (ParamWind && !ParamWind->GetActive()) {
//            ParamWind = NULL;
//        }
//
//        SLPSettings Settings(mAvgToll, mDelayBridges, mDelayLocks, MaxDelay, mFuelPrice, mImposeSpeedLimit,
//            aRoute.GetMastMode(), aRoute.GetPlannerMode(), MotoringSpeed, mOpenLegOptimAccuracy, mOpenLegOptimPointCount,
//            mOpenLegOptimSecondPass, EIGHTHf, mOpenLegOptimVMGReduction1,
//            mOpenLegOptimVMGReduction2, mReduceFuel, mReduceSpeed, SrcCurrent, ParamCurrent, SrcTideHeight,
//            SrcWind, ParamWind, mSailingTimes, mUseSailingTimes, mUseWWCurrent, aRoute.GetWaterBodyMask(),
//            mWaterLevelNAP);
//*/
//
///*
//        mSettings = new StLPSettings(mImposeSpeedLimit, aRoute.getMotoringSpeed(), mUseWWCurrent, aRoute.getWaterBodyMask());
//
//        // In case of an active route, create a temporary route point at the boat location and an active leg from the boat
//        // position to the active point.
//        StLPRoutePoint Prev = null;
//        if (aRoutePointActive != null) {
//*/
///*
//           // Calculate the position of the previous point (P0) and the active route point (P1) in the horizontal space at the
//           // current position.
//           double3 P0Glb, P0Hor, P1Glb, P1Hor;
//           GeoToGlobal(P0Glb, aRoutePointPrev->GetPos(), gGeodeticEllipsoidWGS84);
//           Mul(P0Hor, P0Glb, aCurState->RefHor->Hor.MatrixGlbToHor);
//           GeoToGlobal(P1Glb, aRoutePointActive->GetPos(), gGeodeticEllipsoidWGS84);
//           Mul(P1Hor, P1Glb, aCurState->RefHor->Hor.MatrixGlbToHor);
//
//           // DistToLineSegment return a fractional value. The fraction is 0 when the first point is the closest point, and
//           // it returns 1 if the last point is the closest. A value between these values is returned when the closest point
//           // is somewhere on the line segment.
//           double Factor;
//           DistToLineSegment(P0Hor.xy, P1Hor.xy, ZeroDouble2, Factor);
//
//           // Interpolate manual current, depth and wind.
//           float ManualDepthLAT, ManualDepthNAP;
//           float2 ManualCurrent, ManualWind;
//           Lerp(ManualCurrent, aRoutePointPrev->GetManualCurrent(), aRoutePointActive->GetManualCurrent(), (float)Factor);
//           Lerp(ManualDepthLAT, aRoutePointPrev->GetManualDepthLAT(), aRoutePointActive->GetManualDepthLAT(), (float)Factor);
//           Lerp(ManualDepthNAP, aRoutePointPrev->GetManualDepthNAP(), aRoutePointActive->GetManualDepthNAP(), (float)Factor);
//           Lerp(ManualWind, aRoutePointPrev->GetManualWind(), aRoutePointActive->GetManualWind(), (float)Factor);
//*/
//           // The first leg starts at the current position. Create a route point at that location. If the current state
//           // contains a waterway section, snap the position to the network.
///*
//            Geo2 PosGeo = aCurState.PosGeo;
//            if (aCurState.WWSection != null) {
//                Double3 PosGlb = new Double3();
//                aCurState.WWSection.calcPosGlb(PosGlb, aCurState.WWSectionPos);
//                StGeodeticCalibration.globalToGeo2(PosGlb, PosGeo);
//            }
//
//            Prev = Planning.createRoutePoint("ActiveLegStart", -1,
//                                            PosGeo, aCurState.WWSection, aCurState.WWSectionPos);
//
//        }
//
//        // Reset or create new planning information of each route point.
//        boolean CreatePoints = aRoutePointActive == null;
//        for (int i = 0; i < aRoute.getRoutePointCount(); i++) {
//            // Get route point and reset it's planning.
//            StAppRoutepoint Point = aRoute.getRoutePoint(i);
//            Point.setPlannedRoutePoint(null);
//
//            // Only create route points and legs in the planner for legs after aFirstPoint.
//            if (Point == aRoutePointActive)
//                CreatePoints = true;
//            if (CreatePoints) {
//                // Construct a name for the planner point.
//                String Name = Point.getName(true);
//                int PointIndex = Point.getIndexInRoute();
//                // Check if the section is part of the selected waterbodies.
//                StWWSection Section = Point.getSection();
//                double SectionPos = Point.getSectionPos();
//
//                // cannot plan legs that are not attached to the ww network
////                if (Section == null)
////                    return;
//
//                boolean FoundSection = false;
////      if (!Section && aRoute.GetCount() == 2) {
////        const CWWNetwork *Network = aRoute.GetWaypointManager()->GetWWNetwork();
////        if (Network) {
////          double DistToSection;
////          Network->FindNearestSectionPos(Section, SectionPos, DistToSection, Point->GetPos().LatLong);
////          FoundSection = true;
////        }
////      }
//                if (Section != null && ((aRoute.getWaterBodyMask() & StWWDefs.WWWaterbodyIDToMask(Section.getWaterBodyID())) == 0)) {
//                    Section = null;
//                    SectionPos = 0.0;
//                    FoundSection = false;
//                }
//                if (FoundSection) {
//                }
//
//                // Create a planner route point.
//                StLPRoutePoint RoutePoint = Planning.createRoutePoint(Name, PointIndex, Point.getPosition(), Section, SectionPos);
//                Point.setPlannedRoutePoint(RoutePoint);
//
//                // Create a leg if it is not the first point.
//                if (Prev != null) {
//                    //ELPTravelMode TravelMode = Point->GetTravelMode();
//                    final StLPLeg Leg = Planning.createLeg(Prev, RoutePoint);
//                }
//                Prev = RoutePoint;
//            }
//        }
//
//        // Routes with zero points should not be planned.
//        if (Planning.getRoutePointCount() == 0)
//            return;
//
//        // Get the current boat and environemnt state if the route is active.
//        StLPBoatState CurBoatState = null;
//        StLPEnvState CurEnvState = null;
//        float VMGFiltered = 0;
//        if (aCurState != null) {
//            CurBoatState = aCurState.BoatState;
//            //CurBoatStatePtr = &CurBoatState;
//            CurEnvState = aCurState.EnvState;
//            //CurEnvStatePtr = &CurEnvState;
//            VMGFiltered = aCurState.VMGFiltered;
//            //VMGFilteredPtr = &VMGFiltered;
//        }
//
//        // Plan all legs.
//        planLegs(Planning, aBoatDef, mSettings, CurBoatState, CurEnvState, VMGFiltered);
//        return true;
//*/
//    }
//
//    public void setWWNetwork(StWWNetwork aNetwork) {
//        mWWNetwork = aNetwork;
//    }
///*
//    private class RoutePlannerTask extends AsyncTask<Void, Void, Boolean> {
//
//        final StLPBoatDef mBoatDef;
//        final StAppRoute mRoute;
//        final StAppRoutepoint mRoutePointPrev;
//        final StAppRoutepoint mRoutePointActive;
//        final StWptCurState mCurState;
//
//        public RoutePlannerTask(final StLPBoatDef aBoatDef, final StAppRoute aRoute, final StAppRoutepoint aRoutePointPrev, final StAppRoutepoint aRoutePointActive, final StWptCurState aCurState) {
//            mBoatDef = aBoatDef;
//            mRoute = aRoute;
//            mRoutePointPrev = aRoutePointPrev;
//            mRoutePointActive = aRoutePointActive;
//            mCurState = aCurState;
//        }
//    }
//*/
}
