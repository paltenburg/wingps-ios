//
//  classes.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 30/07/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class LPPlannerState {

    var DistSOP :Double = 0  // Distance from start of planning to this point [m].
    //public double          FuelSOP;    // Used fuel volume from start of planning to this point [m≥].
    //public double          MoneySOP;   // Used money from start of planning to this point [currency unit].
    var Time  :Int64 = 0     // UTC date and time of arrival at this point [ms].

//    init() {
//        DistSOP = 0;
//        Time = 0;
//    }
//
//    StLPPlannerState(double aDistSOP/*, double aFuelSOP, double aMoneySOP*/, long aTime) {
//        DistSOP = aDistSOP;
//        Time = aTime;
//    }
//
//    StLPPlannerState(StLPPlannerState aPlannerState) {
//        DistSOP = aPlannerState.DistSOP;
//        Time = aPlannerState.Time;
//    }
//
//    void init(double aDistSOP, long aTime) {
//        DistSOP = aDistSOP;
//        Time = aTime;
//    }
//
//    void init(double aDistSOP/*, double aFuelSOP, double aMoneySOP*/, Date aTime) {
//        DistSOP = aDistSOP;
//        Time = aTime.getTime();
//    }
//
//    void init(StLPPlannerState aPlannerState) {
//        DistSOP = aPlannerState.DistSOP;
//        Time = aPlannerState.Time;
//    }
}

public class StLPNode {

//    boolean     mInOptimalPath;             // Boolean used internally during planning.
//    Geo2        mPosGeo;                    // Position in geographic space.
//    Double3     mPosGlb;                    // Position in global space.
//    StLPSection mSectionNext;               // Pointer to next section (optional).
//    StLPSection mSectionPrev;               // Pointer to previous section (optional).

//    StLPNode() {
//        mInOptimalPath = false;
//        mPosGeo = new Geo2();
//        mPosGlb = new Double3();
//        mSectionNext = null;
//        mSectionPrev = null;
//    }
//
//    StLPNode(final StLPNode aNode) {
//        mInOptimalPath = false;
//        mPosGeo = new Geo2(aNode.mPosGeo);
//        mPosGlb = new Double3(aNode.mPosGlb);
//        mSectionNext = null;
//        mSectionPrev = null;
//    }
//
//    StLPNode(final StLPRoutePoint aRoutePoint) {
//        mInOptimalPath = false;
//        mPosGeo = new Geo2(aRoutePoint.mPosGeo);
//        mPosGlb = new Double3(aRoutePoint.mPosGlb);
//        mSectionNext = null;
//        mSectionPrev = null;
//    }
//
//    StLPNode(final Geo2 aPosGeo, Double3 aPosGlb) {
//        mInOptimalPath = true;
//        mPosGeo = new Geo2(aPosGeo);
//        mPosGlb = new Double3(aPosGlb);
//        mSectionNext = null;
//        mSectionPrev = null;
//    }
//
//    public void move(final Geo2 aPosGeo) {
//        mPosGeo.set(aPosGeo);
//        StGeodeticCalibration.geoToGlobal(mPosGeo, mPosGlb);
//
//        GeoPoint Pos = null;
//        GeoPoint PosOther = null;
//        if (mSectionPrev != null) {
//            Pos = StUtils.geo2ToGeoPoint(aPosGeo, null);
//            PosOther = StUtils.geo2ToGeoPoint(mSectionPrev.mNodePrev.mPosGeo, null);
//            mSectionPrev.mOpenSectionLen = Pos.distanceTo(PosOther);
//
////            CRhumbLine RhumbLine(mSectionPrev->mNodePrev->mPosGeo, aPosGeo, gGeodeticEllipsoidWGS84);
////            mSectionPrev->mOpenSectionLen = RhumbLine.GetDist();
//        }
//        if (mSectionNext != null) {
//            if (Pos != null) {
//                StUtils.geo2ToGeoPoint(mSectionNext.mNodeNext.mPosGeo, PosOther);
//            } else {
//                Pos = StUtils.geo2ToGeoPoint(aPosGeo, null);
//                PosOther = StUtils.geo2ToGeoPoint(mSectionNext.mNodeNext.mPosGeo, null);
//            }
//            mSectionNext.mOpenSectionLen = Pos.distanceTo(PosOther);
//
////            CRhumbLine RhumbLine(aPosGeo, mSectionNext->mNodeNext->mPosGeo, gGeodeticEllipsoidWGS84);
////            mSectionNext->mOpenSectionLen = RhumbLine.GetDist();
//        }
//
//    }


}
