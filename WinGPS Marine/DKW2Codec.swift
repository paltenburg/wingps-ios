//
//  DKW2Codec.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class StDKW2Codec {
	
	static let BLOCKCOUNTX  = 16 // Number of blocks in one tile in x-direction.
	static let BLOCKCOUNTY  = 16 // Number of blocks in one tile in y-direction.
	static let BLOCKSIZEX   = 16 // Size of a block in pixels in x-direction.
	static let BLOCKSIZEY   = 16 // Size of a block in pixels in y-direction.
	static let COMP_RAW     = 255
	static let COMP_NOPAL   = 254
	static let COMP_PAL     = 253
	static let COMP_RLE1    = 0
	static let PACKET_ABS   = 0
	static let TILEBPP      = 4
	//    let SCANLINESIZE = StDKW2Utils_DKW2TILESIZEX * TILEBPP;
	static let SCANLINESIZE = StDKW2Utils_DKW2TILESIZEX * 4;
	
	//    let BLOCKSIZE = BLOCKSIZEX * BLOCKSIZEY
	static let BLOCKSIZE = 16 * 16
	
	// Unused!
	static func tileDecompressBlock(width :Int, height :Int, pitch :Int, bitCountTable :[UInt8], buffers :inout StDKW2ByteBuffer!, tileDataPos :inout Int, includeAlpha :Bool, fullyTransparent :inout Bool, fullyOpaque :inout Bool, hasTransparentColor :inout Bool, transparentColor :UInt32) throws -> NSMutableData {
		
		let dst = buffers.DKW2DstData
		var dstPos = 0
		
		let src = buffers.tileData
		
		var pal = buffers.pal
		
		var bitBuffer = BitBuffer()
		
		let masterPalSize = Int(try getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos))
		
		if (masterPalSize == 0){
			// No Master Palette
			for y in 0 ..< BLOCKCOUNTY {
				for x in 0 ..< BLOCKCOUNTX {
					var pixelCount = 0
					
					let blockOffset = StDKW2Utils_DKW2TILESIZEX * (y << 4) * 4 + x * TILEBPP * BLOCKSIZEX
					
					let colorCount = Int(try getUByteFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos))
					
					if colorCount == COMP_RLE1 {
						// RLE1
						////// Unchecked
						let color = try read3(buffer: src as NSData, at: &tileDataPos) | 0xff //// check color values in android
						while pixelCount < BLOCKSIZE {
							
							//                            (dst as! NSMutableData).replaceBytes(
							//                                in: NSRange(location:
							//                                    blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP
							//                                    , length: 4),
							//                                withBytes: &color, length: 4)
							
							putInt(CFSwapInt32(color), in: dst,
							       at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							
							//                            dst.replaceSubrange(1 ..< 3, with: src)
							pixelCount += 1
						}
					} else if colorCount <= COMP_PAL {
						////// Unchecked
						// Use palette
						for i in 0 ..< colorCount {
							pal[i] = try read3(buffer: src as NSData, at: &tileDataPos) | UInt32(0xff)
						}
						
						let indexSize = bitCountTable[colorCount - 1]
						while pixelCount < BLOCKSIZE {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
									let color = pal[Int(colorIndex)]
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							} else {
								// RLE Packet
								let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
								let color = pal[Int(colorIndex)]
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
						
					} else if colorCount == COMP_NOPAL {
						////// Unchecked
						// No Palette
						while (pixelCount < BLOCKSIZE) {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24) << 8 | 0xff
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							} else {
								// RLE Packet
								let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24) << 8 | 0xff
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
							
						}
						
					} else {
						////// Unchecked
						// Raw
						while pixelCount < BLOCKSIZE {
							let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24)
							
							putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							pixelCount += 1
						}
					}
				}
			}
		} else {
			// Master Palette
			var masterPal = [UInt32](repeating: 0, count: masterPalSize)
			var i = 0
			while i < masterPalSize {
				try setSafe(from: &masterPal, at: i, value: (try read3(buffer: src as NSData, at: &tileDataPos) | 0xff))
				
//				dout(.tiles, "masterPal ", i," ", Int32(bitPattern: masterPal[i]))
				i += 1
			}
			
			let indexSizeMP = UInt32(bitCountTable[masterPalSize])
			bitBuffer.buffer = 0
			bitBuffer.bitsLeft = 0
			
			var y = 0
			
//			dout(.tiles, "Block counts: ", BLOCKSIZEY," ", BLOCKCOUNTX)
			
			while y < BLOCKCOUNTY {
//				dout(.tiles, "y: ", y)
				
				var x = 0
				while x < BLOCKCOUNTX {
//					dout(.tiles, "x: ", x)
					
					var pixelCount = 0
					let blockOffset = StDKW2Utils_DKW2TILESIZEX * (y << 4) * 4 + x * TILEBPP * BLOCKSIZEX
					
					let colorCount = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8))
					if colorCount == COMP_RLE1 {
						//                        dout(.tiles, "COMP_RLE1")
						// RLE1
						let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSizeMP))
						let color = try getSafe(from: masterPal, at: Int(colorIndex))
						if (color & 0xff) != 0xff {
							dout(.tiles, "Non transparent pixel in MASTER/RLE: ", color)
						}
						
						while pixelCount < BLOCKSIZE {
							putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + pixelCount % BLOCKSIZEX * TILEBPP)
							pixelCount += 1
						}
						
					} else if colorCount <= COMP_PAL {
						//                        dout(.tiles, "COMP_PAL")
						
						// Use palette
						for i in 0 ..< colorCount {
							pal[i] = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSizeMP))))
						}
						
						let indexSize = bitCountTable[colorCount - 1]
						while pixelCount < BLOCKSIZE {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
									let color = pal[Int(colorIndex)]
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
									
								}
							} else {
								// RLE Packet
								let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
								let color = pal[Int(colorIndex)]
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
					} else if colorCount == COMP_NOPAL {
//						dout(.tiles, "COMP_NOPAL")
						// No Palette
						while pixelCount < BLOCKSIZE {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							var b = 0
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									b = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP))
									let color = try getSafe(from: masterPal, at: b)
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
									
								}
							} else {
								// RLE Packet
								var color = UInt32(0)
								if (y == 15) && (x == 15) {
									b = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP))
									color = try getSafe(from: masterPal, at: b)
								} else {
									color = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP)))
								}
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
					} else {
						// Raw
//						dout(.tiles, "Raw")
						while pixelCount < BLOCKSIZE {
							let color = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP)))
							putInt(CFSwapInt32(color), in: dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							pixelCount += 1
						}
					}
					x += 1
				}
				y += 1
			}
		}
		
		//        let sizeRGB = tileDataPos
		
		if includeAlpha {
			fullyTransparent = true
			fullyOpaque = true
			var pixelCount = 0
			var dstIndex = 0
			while pixelCount < StDKW2Utils_DKW2TILESIZEX * StDKW2Utils_DKW2TILESIZEY {
				var alphaCount = UInt32(try getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos))
				if alphaCount >= 0x8000 {
					alphaCount = (alphaCount & 0x7fff) - 1
					for _ in 0 ... alphaCount {
						let pixel = CFSwapInt32(try getUInt32FromNSDataBuffer(buffer: dst as NSData, start: dstIndex))
						putInt(CFSwapInt32(pixel | 0xff), in: dst, at: dstIndex)
						dstIndex += 4
						pixelCount += 1
					}
					fullyTransparent = false
				} else {
					for _ in 0 ..< alphaCount {
						putInt(0, in: dst, at: dstIndex)
						dstIndex += 4
						pixelCount += 1
					}
					fullyOpaque = false
				}
			}
		} else {
			fullyOpaque = true
			fullyTransparent = false
		}
		
		// Check for the pixels that should be made transparent
		if transparentColor != 0 {
			var dstIndex = 0
			hasTransparentColor = false
			fullyTransparent = true
			for _ in (0 ... StDKW2Utils_DKW2TILESIZEX * StDKW2Utils_DKW2TILESIZEY - 1).reversed() {
				let pixel = try getUInt32FromNSDataBuffer(buffer: dst as NSData, at: &dstIndex)
				if pixel == transparentColor {
					putInt(0, in: dst, at: dstIndex)
					fullyOpaque = false
					hasTransparentColor = true
				} else {
					fullyTransparent = false
				}
				dstIndex += 4
			}
		}
		
		if width != StDKW2Utils_DKW2TILESIZEX || height != StDKW2Utils_DKW2TILESIZEY || pitch != (width * 4) {
			var dstS = buffers.DKW2DstSData
			var dstSPos = 0
			
			var srcIndex = 0
			//
			//            var scanLine = [UInt8]()
			//            var scanLine = NSData()
			let scanLineSize = width * 4
			var scanline = [UInt8](repeating: 0, count: scanLineSize)
			
			
			//            dst.getBytes(&scanline, range: NSRange(location: srcIndex, length: scanLineSize))
			
			
			//            for y in (0 ... height - 1).reversed() {
			for _ in (0 ... height - 1).reversed() {
				//                for _ in 0 ..< scanLineSize {
				//                    //                    scanLine[i] = getUByteFromNSDataBuffer(buffer: dst as NSData, at: &dstPos)
				//                    ////                    dstS[dstPos] = getUByteFromNSDataBuffer(buffer: dst as NSData, at: &dstPos) as UInt8
				//                    //dstSPos += 1
				//                }
				
				try SafeData(dst).getBytes(&scanline, range: NSRange(location: srcIndex, length: scanLineSize))
				
				let rangeDstS = NSRange(location: dstSPos, length: scanLineSize)
				dstS.replaceBytes(in: rangeDstS,
				                  withBytes: &scanline, length: scanLineSize)
				dstSPos += scanLineSize
				
				srcIndex += StDKW2Utils_DKW2TILESIZEX * 4
				
				dstPos = srcIndex
			}
			return dstS
		}
		
		return dst
	}
	
	static func tileDecompressBlock(into pixels :UnsafeMutablePointer<UInt32>, width :Int, height :Int, pitch :Int, bitCountTable :[UInt8], buffers :inout StDKW2ByteBuffer!, tileDataPos :inout Int, includeAlpha :Bool, fullyTransparent :inout Bool, fullyOpaque :inout Bool, hasTransparentColor :inout Bool, transparentColor :UInt32, tileOffset :UInt64) throws -> Bool {
		
		dout(.tiles, "tileDecompressBlock: Tileoffset 1", tileOffset)
		var dst :UnsafeMutablePointer<UInt32>
		let transferToSmallerTile :Bool = width != StDKW2Utils_DKW2TILESIZEX || height != StDKW2Utils_DKW2TILESIZEY || pitch != (width * 4)
		
		var dstSize :Int = 0
	
		if !transferToSmallerTile {
			dst = pixels
		} else {
			dstSize = 256 * 256
			dst = UnsafeMutablePointer<UInt32>.allocate(capacity: dstSize)
			defer {
				//dst.deallocate(capacity: dstSize)
			}
		}
		
//		var dstPos = 0
		
		let src = buffers.tileData
		
		var pal = buffers.pal
		
		var bitBuffer = BitBuffer()
		
		let masterPalSize = try getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos)
//		dout(.tiles, "masterPalSize: ", masterPalSize)

		//		// DEBUG-code om Eerste 20 bytes printen <-Floris
//		for j in 0 ..< 1{
//			var valuesString = ""
//			for i in 0 ..< 20 {
//				//			valuesString  += "\(String(tileDataPos)) \(String(Int(getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos)))) "
//				var testByte :UInt8 = 0
//				(src as NSData).getBytes(&testByte, range: NSRange(location: i, length: 1))
//				valuesString  += "\(i) \(testByte) "
//			}
//			dout(.tiles, "tileDecompressBlock 1st values:", valuesString)
//		}
		dout(.tiles, "tileDecompressBlock: Tileoffset 2", tileOffset)

		
		if (masterPalSize == 0){
			// No Master Palette
			for y in 0 ..< BLOCKCOUNTY {
				for x in 0 ..< BLOCKCOUNTX {
					var pixelCount = 0
					bitBuffer.bitsLeft = 0
					bitBuffer.buffer = 0
					
					let blockOffset = StDKW2Utils_DKW2TILESIZEX * (y << 4) * 4 + x * TILEBPP * BLOCKSIZEX
					
					let colorCount = try getUByteFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos)
					if colorCount == COMP_RLE1 {
						// RLE1
						let color = try read3(buffer: src as NSData, at: &tileDataPos) | 0xff
						while pixelCount < BLOCKSIZE {
							
							putInt(CFSwapInt32(color), in: &dst,
							       at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							
							pixelCount += 1
						}
					} else if colorCount <= COMP_PAL {
						// Use palette
						for i in 0 ..< colorCount {
							pal[Int(i)] = try read3(buffer: src as NSData, at: &tileDataPos) | UInt32(0xff)
						}
						
						let indexSize = bitCountTable[Int(colorCount) - 1]
						while pixelCount < BLOCKSIZE {

							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff // Exc
									let color = pal[Int(colorIndex)]
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							} else {
								// RLE Packet
								let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
								let color = pal[Int(colorIndex)]
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
						
					} else if colorCount == COMP_NOPAL {
						// No Palette
						while (pixelCount < BLOCKSIZE) {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24) << 8 | 0xff
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							} else {
								// RLE Packet
								let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24) << 8 | 0xff
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
						
					} else {
						// Raw
						while pixelCount < BLOCKSIZE {
							let color = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 24)
							putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							pixelCount += 1
						}
					}
				}
			}
		} else {
			// Master Palette
			
			var masterPal = [UInt32](repeating: 0, count: Int(masterPalSize))
			var i = 0
			while i < masterPalSize {
				try setSafe(from: &masterPal, at: i, value: (try read3(buffer: src as NSData, at: &tileDataPos) | 0xff))
				i += 1
			}
			
			let indexSizeMP = UInt32(bitCountTable[Int(masterPalSize)])
			bitBuffer.buffer = 0
			bitBuffer.bitsLeft = 0
			
			var y = 0
			
			while y < BLOCKCOUNTY {
			
				var x = 0
				while x < BLOCKCOUNTX {
					
					var pixelCount = 0
					let blockOffset = StDKW2Utils_DKW2TILESIZEX * (y << 4) * 4 + x * TILEBPP * BLOCKSIZEX
					
					let colorCount = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8))
					if colorCount == COMP_RLE1 {
						
						// RLE1
						let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSizeMP))
						let color = try getSafe(from: masterPal, at: Int(colorIndex))
						if (color & 0xff) != 0xff {
							dout(.tiles, "Non transparent pixel in MASTER/RLE: ", color)
						}
						
						while pixelCount < BLOCKSIZE {
							//                            if x == 15 {
							//                                dout(.tiles, " ",pixelCount," ",blockOffset + (pixelCount >> 4) * SCANLINESIZE + pixelCount % BLOCKSIZEX * TILEBPP)
							//                            }
							
							
							putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + pixelCount % BLOCKSIZEX * TILEBPP) // 1
							pixelCount += 1
						}
						
					} else if colorCount <= COMP_PAL {
					
						// Use palette
						for i in 0 ..< colorCount {
							pal[i] = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSizeMP))))
						}
						
						let indexSize = bitCountTable[colorCount - 1]
						while pixelCount < BLOCKSIZE {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
									let color = pal[Int(colorIndex)]
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
									
								}
							} else {
								// RLE Packet
								let colorIndex = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: UInt32(indexSize)) & 0xff
								let color = pal[Int(colorIndex)]
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
								}
							}
						}
					} else if colorCount == COMP_NOPAL {
						// No Palette
						while pixelCount < BLOCKSIZE {
							let packetType = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 1) & 0xff
							let packetSize = try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: 8)
							var b = 0
							if packetType == UInt32(PACKET_ABS) {
								// ABS Packet
								for _ in 0 ... packetSize {
									b = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP))
									let color = try getSafe(from: masterPal, at: b)
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
									pixelCount += 1
									
								}
							} else {
								// RLE Packet
								var color = UInt32(0)
								if (y == 15) && (x == 15) {
									b = Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP))
									color = try getSafe(from: masterPal, at: b)
								} else {
									color = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP)))
								}
								for _ in 0 ... packetSize {
									putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP) //1
									pixelCount += 1
								}
							}
						}
					} else {
						// Raw
						while pixelCount < BLOCKSIZE {
							let color = try getSafe(from: masterPal, at: Int(try readBits(src: src, bb: &bitBuffer, at: &tileDataPos, bitCount: indexSizeMP)))
							putInt(CFSwapInt32(color), in: &dst, at: blockOffset + (pixelCount >> 4) * SCANLINESIZE + (pixelCount % BLOCKSIZEX) * TILEBPP)
							pixelCount += 1
						}
					}
					x += 1
				}
				y += 1
			}
		}
		
		dout(.tiles, "tileDecompressBlock: Tileoffset 3", tileOffset)
        
        if includeAlpha {
            fullyTransparent = true
			fullyOpaque = true
			var pixelCount :Int = 0
			var dstIndex :Int = 0
			while pixelCount < StDKW2Utils_DKW2TILESIZEX * StDKW2Utils_DKW2TILESIZEY {
				var alphaCount = UInt32(try getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos))
				if alphaCount >= 0x8000 {
					alphaCount = (alphaCount & 0x7fff) - 1
					for _ in 0 ... alphaCount {
//						let testIndex = dstIndex / 4
//						let testVal = dst[dstIndex / 4] // debug
						let pixel = CFSwapInt32(dst[dstIndex / 4])
						dst[dstIndex/4] = CFSwapInt32(pixel | 0xff) // dbg
						putInt(CFSwapInt32(pixel | 0xff), in: &dst, at: dstIndex) // 2
						dstIndex += 4
						pixelCount += 1
					}
					fullyTransparent = false
				} else {
					for _ in 0 ..< alphaCount {
//						let testIndex = dstIndex / 4 // debug
						dst[dstIndex/4] = 0 // dbg
						putInt(0, in: &dst, at: dstIndex) //2
						dstIndex += 4
						pixelCount += 1
					}
					fullyOpaque = false
				}
			}
		} else {
			fullyOpaque = true
			fullyTransparent = false
		}
		
		dout(.tiles, "tileDecompressBlock: Tileoffset 4", tileOffset)

		// Check for the pixels that should be made transparent
		if transparentColor != 0 {
			var dstIndex = 0
			hasTransparentColor = false
			fullyTransparent = true
			for _ in (0 ... StDKW2Utils_DKW2TILESIZEX * StDKW2Utils_DKW2TILESIZEY - 1).reversed() {
				let pixel = dst[dstIndex / 4]
				if pixel == transparentColor {
					putInt(0, in: &dst, at: dstIndex)
					fullyOpaque = false
					hasTransparentColor = true
				} else {
					fullyTransparent = false
				}
				dstIndex += 4
			}
		}
		
		dout(.tiles, "tileDecompressBlock: Tileoffset 5", tileOffset)

		//        if width != StDKW2Utils_DKW2TILESIZEX || height != StDKW2Utils_DKW2TILESIZEY || pitch != (width * 4) {
		if transferToSmallerTile {
			//            var dstS = buffers.DKW2DstSData
			var dstIndex = 0
			var srcIndex = 0
			//
			//            var scanLine = [UInt8]()
			//            var scanLine = NSData()
			
			//            let scanLineSize = width * 4
			let scanLineSize = width
			
			for _ in (0 ... height - 1).reversed() {
				pixels.advanced(by: dstIndex).assign(from: dst.advanced(by: srcIndex), count: scanLineSize)
				
				srcIndex += StDKW2Utils_DKW2TILESIZEX
				dstIndex += scanLineSize
			}
			
			return true
		}
		return true
	}
	
	static func putInt(_ what :UInt32, in dst :NSMutableData, at pos :Int){
		var whatVar = what
		
		//        let dstMutable = dst as! NSMutableData
		
		//        var P: UInt8 = 0
		
		//        dst.replaceBytes(in: NSRange(location: 0, length: 1), withBytes: &P, length: 1)
		
		
		//        dst.replaceBytes(in: NSRange(location: pos, length: 1),
		//                                withBytes: &whatVar, length: 1) 
		
		dst.replaceBytes(in: NSRange(location: pos, length: 4),
		                 withBytes: &whatVar, length: 4)
		
	}
		
	static func putInt(_ what :UInt32, in pixelArray :inout UnsafeMutablePointer<UInt32>, at pos :Int){
		
		pixelArray[pos/4] = what
	
	}
	
	static func read3(buffer: NSData, at position :inout Int) throws -> UInt32 {
		
		if position > (buffer.length - 3) {	throw NSError() }
		
		var byte1 :UInt8 = 0
		var byte2 :UInt8 = 0
		var byte3 :UInt8 = 0
		buffer.getBytes(&byte1, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte2, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte3, range: NSRange(location: position, length: 1)); position += 1
		
		return UInt32(byte1) << 8 | UInt32(byte2) << 16 | UInt32(byte3) << 24
		
	}
	
	static func read4(buffer: NSData, at position :inout Int) throws -> UInt32 {
		
		if position > (buffer.length - 4) {	throw NSError() }

		var byte1 :UInt8 = 0
		var byte2 :UInt8 = 0
		var byte3 :UInt8 = 0
		var byte4 :UInt8 = 0
		buffer.getBytes(&byte1, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte2, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte3, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte4, range: NSRange(location: position, length: 1)); position += 1
		
		return UInt32(byte1) | UInt32(byte2) << 8 | UInt32(byte3) << 16 | UInt32(byte4) << 24
	}
	
	static func readBits (src :Data, bb :inout BitBuffer, at tileDataPos :inout Int, bitCount :UInt32) throws -> UInt32 {
		var res = (bb.buffer >> (31 - bb.bitsLeft)) >> UInt32(1)
		if bitCount > bb.bitsLeft {
			bb.buffer = try read4(buffer: src as NSData, at: &tileDataPos)
			res = res | (bb.buffer << bb.bitsLeft)
		}
		//        dout(.tiles, "try readBits: ", bitCount," ",bb.bitsLeft," ",
		//              Int32(bitCount) - Int32(bb.bitsLeft)," ",
		//              (32 - Int32(Int32(bitCount) - Int32(bb.bitsLeft))) % 32," ",
		//              UInt32((32 - Int32(Int32(bitCount) - Int32(bb.bitsLeft))) % 32)
		//              )
		//        //,(32 - UInt32(bitCount - bb.bitsLeft)) )
		bb.bitsLeft = UInt32((32 - Int32(Int32(bitCount) - Int32(bb.bitsLeft))) % 32)
		return res & (0xffffffff as UInt32 >> (32 - bitCount))
	}
	
	class BitBuffer {
		var bitsLeft :UInt32 = 0
		var buffer :UInt32 = 0
	}
}
