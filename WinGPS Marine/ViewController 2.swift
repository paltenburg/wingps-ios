//
//  ViewController.swift
//  MapDemo
//
//  Created by Standaard on 05/05/17.
//  Copyright © 2017 Stentec. All rights reserved.
//

import UIKit
import MapKit
import Foundation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate, StMapViewListener
{

	
	@IBOutlet weak	var mapView: StMapView!
	
	static var instance :ViewController?
	
    
//	var locationManager = CLLocationManager()
	lazy var locationManager: CLLocationManager = {
		let manager = CLLocationManager()
		// Set up your manager properties here
		manager.delegate = self
		return manager
	}()
	
	var stDB :[StDB] = [StDB]()
	var wpAnnotations :[StDBWaypointAnnotation] = [StDBWaypointAnnotation]()
	
	var mapRotation :Double = 0
	var zoomLevel :Double = 0
	var courseUp :Bool = false
	
	@IBOutlet weak var mainToolbar: UIToolbar!
	@IBOutlet weak var centreer: UIBarButtonItem!
	@IBOutlet weak var mainMenu: UIBarButtonItem!
	@IBOutlet weak var debugInfo: UILabel!
	@IBOutlet weak var InstrumentInfoLabel: UILabel!
	//	@IBOutlet weak var touchInterceptingView: UIView!
	
	var headingImageView: UIImageView?
	var CourseLineImageView: UIImageView?

	
	var userHeading: CLLocationDirection?
	var userCourse: CLLocationDirection = 0
	var userSpeed: CLLocationSpeed = 0
	
//	override func viewDidLoad() {
//	super.viewDidLoad()
//	print("viewDidLoad()")
	override func viewDidAppear(_ animated: Bool) {

		super.viewDidAppear(true)

		ViewController.instance = self
		
		var DBFiles = [URL]()
		do{ // Get the directory contents urls (including subfolders urls)
			let directoryContents = try FileManager.default.contentsOfDirectory(at: getDKW2Directory(), includingPropertiesForKeys: nil, options: [])
			
			// if you want to filter the directory contents you can do like this:
			DBFiles = directoryContents.filter{ $0.pathExtension == "db" }
			print("DB urls:",DBFiles)
			//			let fileNames = files.map{ $0.deletingPathExtension().lastPathComponent }
			//			print("list:", fileNames)
		} catch {
			print(error.localizedDescription)
		}
		//mapView = StMapView(frame: self.view.bounds)
		mapView.listener = self
//		locationManager.delegate = self
		
		// load database files
		for stDBFile in DBFiles {
			stDB.append(StDB(fileName: stDBFile))
			
		}
		
		mapView.mapType = MKMapType.standard
		
		if let files = try? FileManager.default.contentsOfDirectory(at: getDKW2Directory(), includingPropertiesForKeys: nil, options: []){
			for file in files {
				print(file)
			}
		}
		
		if let filepath = Bundle.main.path(forResource: "test", ofType: "txt") {
			print("bundletest:", filepath)
		}
		//		VaarkaartNL2017_Noord_8mpp.dkw2
		
		
		// setup chart manager
		var chartManager = DKW2ChartManager.getChartManagerRefresh()
		
		
		let location = CLLocationCoordinate2D(latitude: 52.3388, longitude: 5.1302)
		
		//		let span = MKCoordinateSpanMake(0.3, 0.3)
		let span = MKCoordinateSpanMake(1, 1)
		let region : MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
		mapView.setRegion(region, animated: true)
		
		//let annotation = MKPointAnnotation()
		//annotation.coordinate = location
		//annotation.title = "Custom pin"
		//annotation.subtitle = "Abbega"
		
		//mapView.addAnnotation(annotation)
		
		for stdb in stDB {
			for waypoint in stdb.dbWaypoints {
				
				let wpLocation = CLLocationCoordinate2D(latitude: waypoint.getLatitude(), longitude: waypoint.getLongitude())
				
				let wpAnnotation = StDBWaypointAnnotation()
				wpAnnotation.coordinate = wpLocation
				wpAnnotation.title = waypoint.name
				//print("Waypoint :",waypoint.name)
				wpAnnotation.subtitle = waypoint.subName
				
				wpAnnotation.waypoint = waypoint
				
				wpAnnotations.append(wpAnnotation)
				
				//if(mapView.annotationVisibleRect.contains(wpAnnotation.accessibilityActivationPoint)){
				//	mapView.addAnnotation(wpAnnotation)
				//}
				if mapView.annotations.count > 5000 {
					break
				}
				
			}
		}
		
		
		let template = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
		//        let template = "https://tileservice.charts.noaa.gov/tiles/50000_1/{z}/{x}/{y}.png"
		//        let template = "https://tile.openstreetmap.org/0/0/0.png"
		//        let customOverlay = DKW2TileOverlay(urlTemplate: template)
		
		//		// a background overlay is not faster
		//		let dKW2BackgroundOverlay = DKW2BackgroundOverlay()
		//		dKW2BackgroundOverlay.canReplaceMapContent = true
		//		mapView.add(dKW2BackgroundOverlay, level: .aboveLabels)
		
		
		
		let dKW2Overlay = DKW2TileOverlay()
		//        let customOverlay = ExploredTileOverlay(urlTemplate: template)
		
		//        let customOverlay = MKTileOverlay(urlTemplate: template)
		dKW2Overlay.canReplaceMapContent = true
		//        customOverlay.isGeometryFlipped = true
		//        customOverlay.tileSize = CGSize(width: 80, height:80)
		
		mapView.add(dKW2Overlay, level: .aboveLabels)
		
		mapView.delegate = self
		
		//        print("starting test")
		//        testSingleTile()
		
		mapView.showsCompass = true
		mapView.showsScale = true
		
		checkLocationAuthorizationStatus()
		
		let userLocation = mapView.userLocation
		print("viewcontroller userLocation:", userLocation.coordinate)
		mapView.showsUserLocation = true
		let userRegion :MKCoordinateRegion = MKCoordinateRegion(center: userLocation.coordinate, span: span)
		//		mapView.setRegion(userRegion, animated: true)
		mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
		
		locationManager.startUpdatingHeading()
		locationManager.headingOrientation = CLDeviceOrientation.faceUp
		locationManager.startUpdatingLocation()
		
		//		let doubleTapGR = UITapGestureRecognizer(target: self, action: "doubleTapAction")
		//		doubleTapGR.numberOfTapsRequired = 2
		////		doubleTapGR.cancelsTouchesInView = false
		////		mapView.addGestureRecognizer(doubleTapGR)
		//		touchInterceptingView.addGestureRecognizer(doubleTapGR)
		
		let doubleTapGRMV = UITapGestureRecognizer(target: self, action: "doubleTapAction")
		doubleTapGRMV.numberOfTapsRequired = 2
		doubleTapGRMV.numberOfTouchesRequired = 1
		//		doubleTapGR.cancelsTouchesInView = false
		//		touchInterceptingView.addGestureRecognizer(doubleTapGRMV)
		mapView.addGestureRecognizer(doubleTapGRMV)
		
		let tapGR = UITapGestureRecognizer(target: self, action: "singleTapAction")
		tapGR.require(toFail: doubleTapGRMV)
		tapGR.numberOfTapsRequired = 1
		mapView.addGestureRecognizer(tapGR)
		
		showLoginDialog()
	}
	
	func doubleTapAction() {
		print("doubleTapAction()")
	}
	
	func singleTapAction() {
		//		mapView.setUserTrackingMode( MKUserTrackingMode.follow, animated: true)
		print("singleTapAction()")
	}
	
	func checkLocationAuthorizationStatus() {
		if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
			mapView.showsUserLocation = true
			//			centerMapOnLocation(locationManager.location!, map: mapView, radius: regionRadius)
		} else {
			locationManager.requestAlwaysAuthorization() //requestWhenInUseAuthorization()
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		print("didReceiveMemoryWarning() TODO: Flush Cache")
		// Dispose of any resources that can be recreated.
	}
	
	func mapView(_ ProfileMapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
		
		//if annotation.isMember(of: MKUserLocation.self) {
		//	return nil
		//}
		
		if let customAnnotation = annotation as? StDBWaypointAnnotation {
			let reuseId = customAnnotation.waypoint?.GUID?.toString()!
			var wpView :MKAnnotationView
			//var iconImageView :UIImageView?
			//var label :UILabel?
			if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: reuseId!){
				dequededView.annotation = customAnnotation
				wpView = dequededView
				//iconImageView = UIImageView(image: wpView.image)
				//if(wpView.subviews.count > 0){
				//	label = wpView.subviews[0] as! UILabel
				//}
			} else {
				wpView = MKAnnotationView(annotation: customAnnotation, reuseIdentifier: reuseId!)
				wpView.canShowCallout = true
				wpView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
				let iconImage :UIImage? = customAnnotation.waypoint?.type?.iconImage!
				//iconImageView = UIImageView(image: iconImage)
				wpView.image = iconImage
				
				if(customAnnotation.waypoint?.type?.name == "City") {
					let label = UILabel()
					let titleText = customAnnotation.title!
					label.text = titleText
					label.textColor = UIColor.black
					label.backgroundColor = UIColor.clear
					//label.frame = CGRect.init(x: 0, y: wpView.bounds.height, width: 400, height: 20)
					wpView.addSubview(label)
					//wpView.autoresizesSubviews = false
					//wpView.translatesAutoresizingMaskIntoConstraints = false
				}
			}
			
			var rotationCorrection :Float = 0
			if(customAnnotation.waypoint!.type!.fixedToChart){
				rotationCorrection = Float(mapRotation) * Float.pi/180
			}
			
			let scale = CGFloat(customAnnotation.waypoint.drawScale)
			//(wpView.subviews[0] as! UILabel).text = "Zoom: " + String(z) + "  Scale: " + String(Float(scale))
			let size = CGSize(width: wpView.image!.size.width * scale, height: wpView.image!.size.height * scale)
			wpView.frame.size = size
			
			wpView.transform = CGAffineTransform(rotationAngle: CGFloat(customAnnotation.waypoint.angle - rotationCorrection))
			//wpView.transform = CGAffineTransform(rotationAngle: CGFloat(customAnnotation.waypoint.angle - Float(mapRotation)*Float.pi/180))
			
			if (wpView.subviews.count > 0) {
				
				(wpView.subviews[0] as! UILabel).frame = CGRect.init(x: 0, y: wpView.bounds.height, width: 400, height: 20)
			}		/*if (wpView.frame.size.width > 5) || (wpView.frame.size.height > 5) {
			wpView.isHidden = false
			wpView.isEnabled = true
			} else {
			wpView.isHidden = true
			wpView.isEnabled = false
			}*/
			return wpView
			
		} else if annotation is MKUserLocation {
			
			let reuseId = "user location"
			var locationMarker :MKAnnotationView
			//var iconImageView :UIImageView?
			//var label :UILabel?
			if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
				//it shoudl be always the same dequededView.annotation = customAnnotation
				locationMarker = dequededView
				//iconImageView = UIImageView(image: wpView.image)
				//if(wpView.subviews.count > 0){
				//	label = wpView.subviews[0] as! UILabel
				//}
			} else {
				locationMarker = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
				locationMarker.canShowCallout = true
				locationMarker.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
				//				let iconImage :UIImage? = annotation.waypoint?.type?.iconImage!
				//iconImageView = UIImageView(image: iconImage)


				locationMarker.image = nil
			}
			
			
//						locationMarker = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
//						locationMarker.pinTintColor = UIColor.purple
			return locationMarker
		}
		return nil
	}
	
	func mapView(_ mapView: StMapView, rotationDidChange rotation: Double) {
		// process new map rotation
		self.zoomLevel = mapView.getZoom()
		self.mapRotation = rotation
		//print("ROTATING :",rotation, "Zoom: ", zoomLevel)
		
		updateLocationRotation()
		
	}

	
	func mapView(_ mapView: MKMapView, regionDidChangeAnimated: Bool) {
		//		var visibleAnnotations = Array(mapView.annotations(in: mapView.visibleMapRect)) as! [MKAnnotation]
		//mapView.removeAnnotations(visibleAnnotations)
		//mapView.addAnnotations(visibleAnnotations)
		
		//		for (annotation) in visibleAnnotations {
		//			if annotation is StDBWaypointAnnotation{
		//				let annotationCopy = StDBWaypointAnnotation(annotation as! StDBWaypointAnnotation)
		//				mapView.addAnnotation(annotationCopy)
		//				mapView.removeAnnotation(annotation)
		//			}
		//		}
		//				mapView.removeAnnotations(mapView.annotations)
		
		let z = Float(self.mapView.getZoom())
		
		var scale :CGFloat = 1
		
		
		//		var visibleAnnotations = Array(mapView.annotations(in: mapView.visibleMapRect)) as! [StDBWaypointAnnotation]
		var wpsToRemove :[StDBWaypointAnnotation] = [StDBWaypointAnnotation]()

		
		// add waypoints to mapview
		for i in 0 ..< wpAnnotations.count {
			let annotation = wpAnnotations[i]
			
			if (mapView.annotationVisibleRect.contains(mapView.convert(annotation.coordinate, toPointTo: mapView))){
				//		var visibleAnnotations = Array(mapView.annotations(in: mapView.visibleMapRect))
				//		for annotation in visibleAnnotations {
				//			if let stDBWaypointAnnotation = annotation as? StDBWaypointAnnotation{
				if(z > 0) {
					scale = CGFloat(annotation.waypoint.getScale(zoomLevel: z))
				}
				if(CGFloat(annotation.waypoint!.type!.width) * scale > 5 || CGFloat(annotation.waypoint!.type!.height) * scale > 5) {
					
					//					if mapView.annotations.contains(where: { $0 === stDBWaypointAnnotation as MKAnnotation } ){
					//
					//					}
					//					mapView.addAnnotation(stDBWaypointAnnotation)
					
					var annotationCopy = StDBWaypointAnnotation(annotation)
					mapView.addAnnotation(annotationCopy)
					wpAnnotations[i] = annotationCopy
					
					wpsToRemove.append(annotation)
				}
			}
		}
		for annotation in wpsToRemove {
			mapView.removeAnnotation(annotation) // nog steeds knipper, misschien nog een slag later?
		}
		
		//		wpAnnotations = wpAnnotationsTemp.map{ $0 }
		
//		debugInfo.text = "Waypoints: \(mapView.annotations.count)\nMaptiles: \(mapView.overlays.count)\nLocation.speed: \(userSpeed)"
		debugInfo.text = "Location.speed: \(userSpeed)"
	}
	
	func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
		if views.last?.annotation is MKUserLocation {
			addHeadingView(toAnnotationView: views.last!)
		}
	}
	
	func addHeadingView(toAnnotationView annotationView: MKAnnotationView) {
		
		if CourseLineImageView == nil {
			let image :UIImage = getCourseLineImage()
			CourseLineImageView = UIImageView(image: image)
			CourseLineImageView!.frame = CGRect(x: (annotationView.frame.size.width - image.size.width)/2, y: (annotationView.frame.size.height - image.size.height)/2, width: image.size.width, height: image.size.height)
			annotationView.addSubview(CourseLineImageView!)
			CourseLineImageView!.isHidden = true
		}
		
		if headingImageView == nil {
			let image :UIImage = getLocationMarkerImage()
			headingImageView = UIImageView(image: image)
			headingImageView!.frame = CGRect(x: (annotationView.frame.size.width - image.size.width)/2, y: (annotationView.frame.size.height - image.size.height)/2, width: image.size.width, height: image.size.height)
			annotationView.addSubview(headingImageView!)
			headingImageView!.isHidden = true
		}
	}
	
	func getCourseLineImage() -> UIImage {
		//		let color = UIColor.blue
		let height :CGFloat = 300.0;
		let width :CGFloat = 5.0;
		let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		
		//		color.setFill()
		
		let context = UIGraphicsGetCurrentContext()!
		context.translateBy(x: width/2, y: height/2)
		context.setLineWidth(0.4)
//		context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
		context.setStrokeColor(UIColor.black.cgColor)
		context.move(to: CGPoint(x: 0, y: 0))
		context.addLine(to: CGPoint(x: 0, y: -height/2))
		context.strokePath()
		
//		context.translateBy(x: 0, y: -10)
		
		//		color.setFill()
		//UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		//		guard let cgImage = image?.cgImage else { return nil }
		return UIImage(cgImage: (image?.cgImage)!)
	}
	
	func getLocationMarkerImage() -> UIImage {
//		let color = UIColor.blue
		let height :CGFloat = 20.0;
		let width :CGFloat = 20.0;
		let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		
//		color.setFill()

		let context = UIGraphicsGetCurrentContext()!
		context.translateBy(x: width/2, y: 0)
		context.setLineWidth(2.0)
		context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
		context.move(to: CGPoint(x: 0, y: 0))
		context.addLine(to: CGPoint(x: 8, y: 20))
		context.addLine(to: CGPoint(x: 0, y: 16))
		context.addLine(to: CGPoint(x: -8, y: 20))
		context.addLine(to: CGPoint(x: 0, y: 0))
		context.fillPath()
		
		context.translateBy(x: 0, y: -10)

//		color.setFill()
		//UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
//		guard let cgImage = image?.cgImage else { return nil }
		return UIImage(cgImage: (image?.cgImage)!)
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
		
		if newHeading.headingAccuracy >= 0 {
			let heading = newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading
			userHeading = heading
//			updateLocationRotation()
		}
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

		let location :CLLocation = locations.last!

		let course = location.course
		if course >= 0 {
			userCourse = course
			
			if self.courseUp {
				mapView.camera.heading = userCourse
				mapView.setCamera(mapView.camera, animated: true)
			}
		}
		let speed = location.speed
		if speed >= 0 {
			userSpeed = speed
		}
		
		updateLocationRotation()
		
		updateInstrumentInfo()
	}
	
	func updateLocationRotation() {
//		if let heading = userHeading,
//			let headingImageView = headingImageView {
//		if let course = userCourse,
		if let headingImageView = headingImageView {
			
			headingImageView.isHidden = false
			let rotation = CGFloat( (userCourse - self.mapRotation) / 180 * Double.pi)
			headingImageView.transform = CGAffineTransform(rotationAngle: rotation)
		}
		if let courseLineImageView = CourseLineImageView {
		
			courseLineImageView.isHidden = false
			let rotation = CGFloat( (userCourse - self.mapRotation) / 180 * Double.pi)
			courseLineImageView.transform = CGAffineTransform(rotationAngle: rotation)
		}
	}
	
	func updateInstrumentInfo() {
	
		InstrumentInfoLabel.text = "COG: " + String(format: "%.0f", userCourse) + "° SOG: " + String(format: "%.1f", (userSpeed * 3.6)) + "km/u"
		
	}
	
	// MARK: MKMapViewDelegate
	
	//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
	//        guard let tileOverlay = overlay as? MKTileOverlay else {
	//            return MKOverlayRenderer()
	//        }
	//
	//        return MKTileOverlayRenderer(tileOverlay: tileOverlay)
	//    }
	
	//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer!
	//    {
	//
	//        if overlay is MKTileOverlay
	//        {
	//            let renderer = MKTileOverlayRenderer(overlay:overlay)
	//
	//            renderer.alpha = 0.8
	//
	//            return renderer
	//        }
	//        return MKOverlayRenderer()
	//    }
	
	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
		// This is the final step. This code can be copied and pasted into your project
		// without thinking on it so much. It simply instantiates a MKTileOverlayRenderer
		// for displaying the tile overlay.
		if let tileOverlay = overlay as? DKW2TileOverlay {
			print("mapview() tileOverlay = DKW2TileOverlay")
			//            let renderer = MKTileOverlayRenderer(overlay: tileOverlay)
			let renderer = DKW2TileOverlayRenderer(overlay: tileOverlay)
			//Set the renderer alpha to be overlay alpha
			//            renderer.alpha = (overlay as MyTileOverlay).alpha
			renderer.alpha = 1
			//            print("alpha: ", renderer.alpha)
			
			return renderer
		} else if let tileOverlay = overlay as? DKW2BackgroundOverlay {
			print("mapview() tileOverlay = DKW2BackgroundOverlay")
			let renderer = DKW2BackgroundOverlayRenderer(overlay: tileOverlay)
			//Set the renderer alpha to be overlay alpha
			//				renderer.alpha = 0.7
			//            print("alpha: ", renderer.alpha)
			
			return renderer
		} else {
			print("mapview() tileOverlay is no DKW2 overlay")
			return MKTileOverlayRenderer(overlay: overlay)
		}
	}
	
	//func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
	
	//}
	
	//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
	//        // This is the final step. This code can be copied and pasted into your project
	//        // without thinking on it so much. It simply instantiates a MKTileOverlayRenderer
	//        // for displaying the tile overlay.
	//        if let tileOverlay = overlay as? ExploredTileOverlay {
	//            print("mapview() tileOverlay = ExploredTileOverlay")
	////                        let renderer = MKTileOverlayRenderer(overlay: tileOverlay)
	//            let renderer = ExploredTileRenderer(overlay: tileOverlay)
	//            //Set the renderer alpha to be overlay alpha
	//            //            renderer.alpha = (overlay as MyTileOverlay).alpha
	//            renderer.alpha = 0.7
	//            print("alpha: ", renderer.alpha)
	//
	//            return renderer
	//        } else {
	//            print("mapview() tileOverlay is not ExploredTileOverlay")
	//            return MKTileOverlayRenderer(overlay: overlay)
	//        }
	//    }
	
	
	@IBAction func toolbarCenterButton(_ sender: Any) {
		print("Centreer knop")
		
//		//		let span = MKCoordinateSpanMake(0.3, 0.3)
//		//		let region : MKCoordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: span)
//		//		mapView.setRegion(region, animated: true)
//		if(courseUp) {
//			courseUp = false
//		} else {
//			courseUp = true
//		}
//		mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
//		//		mainToolbar.barTintColor = UIColor.darkGray
		
//		let chartCheckTask = StChartCheckTask()
		
//		showLoginDialog()
		
//		let sessionIdentifier :String = "ChartDownload testingUrl"
//		var urlSession: URLSession = {
//			let config = URLSessionConfiguration.background(withIdentifier: sessionIdentifier)
//			config.isDiscretionary = true
//			config.sessionSendsLaunchEvents = true
//			return URLSession(configuration: config, delegate: self, delegateQueue: nil)
//		}()
//
//		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/bu353/GpsInfo.exe")!
//
//		let backgroundTask = urlSession.downloadTask(with: testingUrl) // OR URLRequest instance
//		//			backgroundTask.earliestBeginDate = Date().addingTimeInterval(60 * 60)
//		//			backgroundTask.countOfBytesClientExpectsToSend = 200
//		//			backgroundTask.countOfBytesClientExpectsToReceive = 500 * 1024
//		backgroundTask.resume()

//		testDownload()
//		testDownloadBackground()
	}
	
//	func testDownload(){
//		let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
//		
//		//		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/bu353/GpsInfo.exe")!
//		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/wingps/wingps5litesetup.exe")!
//		
//		let task = urlSession.downloadTask(with: testingUrl)
//		
//		task.resume()
//	}
	

	
	@IBAction func toolbarMenuButton(_ sender: Any) {
		print("Menu knop")
		
		//		mainToolbar.barTintColor = UIColor.clear
		//		mainToolbar.isTranslucent = true
		
		let alertController = UIAlertController(title: "Main menu", message: "", preferredStyle: .alert)
		let chartAction = UIAlertAction(title: NSLocalizedString("Chart Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
			//Add your code
		})
		let waypointAction = UIAlertAction(title: NSLocalizedString("Waypoint Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
			//Your code
		})
		let routeAction = UIAlertAction(title: NSLocalizedString("Route Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
			//Your code
		})
		let trackAction = UIAlertAction(title: NSLocalizedString("Track Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
			print("cancel action")
		})
		let cancelAction = UIAlertAction(title: NSLocalizedString("Track Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
			print("cancel action")
		})
		alertController.addAction(chartAction)
		alertController.addAction(waypointAction)
		alertController.addAction(routeAction)
		alertController.addAction(trackAction)
		alertController.preferredAction = chartAction
		
		
		//		self.present(alertController, animated: true, completion: nil)
		self.present(alertController, animated: true) {
			alertController.view.superview?.isUserInteractionEnabled = true
			alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
		}
	}
	
	func alertControllerBackgroundTapped()
	{
		self.dismiss(animated: true, completion: nil)
	}
	
	func showLoginDialog(){
		
		let alert = UIAlertController(title: "Login", message: nil, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
		
		alert.addTextField(configurationHandler: { nameField in
			nameField.placeholder = "E-mail adress"
		})
		
		alert.addTextField(configurationHandler: { passwordField in
			passwordField.placeholder = "Stentec password"
			passwordField.isSecureTextEntry = true
		})

		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
			
			guard let email = alert.textFields?[0].text else { return }
			
			print("e-name: \(email)")
			
			guard let pw = alert.textFields?[1].text else { return }

			print("password: \(pw)")
			
			self.startChartCheck(user: email, pass: pw)
			
		}))
		
		self.present(alert, animated: true)
	}

	func startChartCheck(user :String, pass: String){
		
		let statusCheck = StatusCheck()
		
//		// for testing
//		let userPopke = "paltenburg@gmail.com"
//		let pwPopke = "1oposse1"
//
//		statusCheck.mActDB.setUserData(aEmail: userPopke, aPassword: pwPopke)
//		//mChartCheckState = EChartCheckState.StCheckPem;
//		statusCheck.checkCharts(true)
		
		
	}
	
}

