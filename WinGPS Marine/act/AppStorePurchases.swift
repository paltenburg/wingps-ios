//
//  appStorePurchases.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 27/05/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import StoreKit

class AppStorePurchases :NSObject, SKRequestDelegate{
    
    //    public func verifyReceipt() {
    //
    ////        let receiptRefreshRequest = SKReceiptRefreshRequest(receiptProperties: nil)
    ////        receiptRefreshRequest.delegate = self
    ////        receiptRefreshRequest.start()
    //
    //        // Get the receipt if it's available
    //        let appStoreReceiptURL = Bundle.main.appStoreReceiptURL
    //
    //        if let appStoreReceiptURL = appStoreReceiptURL,
    //            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
    //
    //            do {
    //                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
    //                print(receiptData)
    //
    //                let receiptString = receiptData.base64EncodedString(options: [])
    //
    //                dout("verifyReceipt: \(receiptString)")
    //                // Read receiptData
    //            }
    //            catch { print("Couldn't read receipt data with error: " + error.localizedDescription) }
    //        }
    //    }
    
    func requestNewReceipt(){
        let receiptRefreshRequest = SKReceiptRefreshRequest(receiptProperties: nil)
        receiptRefreshRequest.delegate = self
        receiptRefreshRequest.start()
    }
    
    // SKRequestDelegate
    func requestDidFinish(_ request :SKRequest) {
        dout("receiptRefreshRequest did finish")
    }
    
    //SKRequestDelegate
    func request(_ request :SKRequest, didFailWithError: Error){
        dout("receiptRefreshRequest did fail with error: \(didFailWithError.localizedDescription)")
        
    }
    
    func makeSureThereIsAReceipt(){
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           !FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            requestNewReceipt()
        }
    }
    
    static func getReceipt() -> String {
        
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                print(receiptData)
                
                let receiptString = receiptData.base64EncodedString(options: [])
                
                dout("verifyReceipt: \(receiptString)")
                
                return receiptString
            }
            catch {
                print("Couldn't read receipt data with error: " + error.localizedDescription)
                
            }
        }
        
        return ""
    }
    
    static var purchaseDateString = ""
    
    static func getValidatedReceipt() {
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
           FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {
            
            NSLog("^A receipt found. Validating it...")
            
            do {
                let receiptData = try Data(contentsOf: appStoreReceiptURL, options: .alwaysMapped)
                let receiptString = receiptData.base64EncodedString(options: [])
                let dict = ["receipt-data" : receiptString, "password" : "your_shared_secret"] as [String : Any]
                
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                    
                    if let storeURL = Foundation.URL(string:"https://buy.itunes.apple.com/verifyReceipt"),
                       let sandboxURL = Foundation.URL(string: "https://sandbox.itunes.apple.com/verifyReceipt") {
                        var request = URLRequest(url: storeURL)
                        request.httpMethod = "POST"
                        request.httpBody = jsonData
                        let session = URLSession(configuration: URLSessionConfiguration.default)
                        NSLog("^Connecting to production...")
                        let task = session.dataTask(with: request) { data, response, error in
                            
                            // BEGIN of closure #1 - verification with Production
                            if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                               error == nil, httpResponse.statusCode == 200 {
                                NSLog("^Received 200, verifying data...")
                                do {
                                    if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                       let status = jsonResponse["status"] as? Int64 {
                                        switch status {
                                        case 0: // receipt verified in Production
                                            NSLog("^Verification with Production succesful, updating expiration date...")
                                            //                                                self.updateExpirationDate(jsonResponse: jsonResponse) // Leaves isPremiumInAmbiquousState=true if fails
                                            
                                            
                                            if let receipt = jsonResponse["receipt"] as? Dictionary<String, AnyObject> {
                                                purchaseDateString = "Download date: \(receipt["original_purchase_date"] as? String ?? "date not valid") \(receipt["original_purchase_date_ms"] as? String ?? "ms not valid")"
                                                dout("\(purchaseDateString)")
                                            }
                                            
                                        case 21007: // Means that our receipt is from sandbox environment, need to validate it there instead
                                            NSLog("^need to repeat evrything with Sandbox")
                                            var request = URLRequest(url: sandboxURL)
                                            request.httpMethod = "POST"
                                            request.httpBody = jsonData
                                            let session = URLSession(configuration: URLSessionConfiguration.default)
                                            NSLog("^Connecting to Sandbox...")
                                            let task = session.dataTask(with: request) { data, response, error in
                                                
                                                // BEGIN of closure #2 - verification with Sandbox
                                                if let receivedData = data, let httpResponse = response as? HTTPURLResponse,
                                                   error == nil, httpResponse.statusCode == 200 {
                                                    NSLog("^Received 200, verifying data...")
                                                    do {
                                                        if let jsonResponse = try JSONSerialization.jsonObject(with: receivedData, options: JSONSerialization.ReadingOptions.mutableContainers) as? Dictionary<String, AnyObject>,
                                                           let status = jsonResponse["status"] as? Int64 {
                                                            //                                                                switch status {
                                                            //                                                                    case 0: // receipt verified in Sandbox
                                                            NSLog("^Verification succesfull, updating expiration date...")
                                                            //                                                                for key in jsonResponse {
                                                            //                                                                    dout("jsonkey:\(key)")
                                                            //                                                                }
                                                            
                                                            if let receipt = jsonResponse["receipt"] as? Dictionary<String, AnyObject>
                                                            //                                                                    ,let ms = receipt["original_purchase_date_ms"] as? Int64
                                                            {
                                                                //                                                                    purchaseDateString = "download date: \(receipt["original_purchase_date_ms"] as? String ?? "ms not valid") \(receipt["original_purchase_date"] as? String ?? "date not valid")"
                                                                
                                                                purchaseDateString = "download date: \(receipt["original_purchase_date"] as? String ?? "date not valid") \(receipt["original_purchase_date_ms"] as? String ?? "ms not valid")"
                                                                dout("purchase date: \(purchaseDateString)")
                                                                
                                                            }
                                                            //                                                                        self.updateExpirationDate(jsonResponse: jsonResponse) // Leaves isPremiumInAmbiquousState=true if fails
                                                            //                                                                    default: self.showAlertWithErrorCode(errorCode: status)
                                                            //                                                                }
                                                        } else {
                                                            NSLog("Failed to cast serialized JSON to Dictionary<String, AnyObject>")
                                                        }
                                                    }
                                                    catch {
                                                        NSLog("Couldn't serialize JSON with error: " + error.localizedDescription)
                                                        
                                                    }
                                                }
                                                //                                                    else {
                                                //                                                        self.handleNetworkError(data: data, response: response, error: error)
                                                //
                                                //                                                }
                                            }
                                            // END of closure #2 = verification with Sandbox
                                            
                                            task.resume()
                                        default: NSLog("self.showAlertWithErrorCode(errorCode: status)")
                                        }
                                    } else { NSLog("Failed to cast serialized JSON to Dictionary<String, AnyObject>") }
                                }
                                catch { NSLog("Couldn't serialize JSON with error: " + error.localizedDescription) }
                            } else { NSLog("self.handleNetworkError(data: data, response: response, error: error)") }
                        }
                        // END of closure #1 - verification with Production
                        
                        task.resume()
                    } else { NSLog("Couldn't convert string into URL. Check for special characters.") }
                }
                catch { NSLog("Couldn't create JSON with error: " + error.localizedDescription) }
            }
            catch { NSLog("Couldn't read receipt data with error: " + error.localizedDescription) }
        } else {
            NSLog("No receipt found even though there is an indication something has been purchased before")
            NSLog("^No receipt found. Need to refresh receipt.")
            //            self.refreshReceipt()
        }
    }
    
    
}
