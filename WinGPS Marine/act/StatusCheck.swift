//
//  StatusCheck.swift
//  WinGPS Marine
//
//  Created by Standaard on 27/08/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class StatusCheck {
    //implements SDCardPermissionCallbackActivity
    //implements ActivityCompat.OnRequestPermissionsResultCallback
    //
    //	private static final int REQUEST_WRITE_STORAGE = 112;
    
    enum EChartCheckState {
        case GPlay          // Check with the Play Store
        //StCheckPem,     // Check with Stentec server
        //		case StCheck        // Check app with Stentec server
        case StCheckTides   // Check for tide files
        case StCheckNetwork // Check the route network
        //		case StRequestSingleChartset // Requesting files of a single chartset
        case StCheckCharts  // Check with Stentec server
        case StGetFile      // Get a file
        
        case StCheckFrieseMeren // 1st check for Friese meren app
    }
    
    enum EFileState {
        case FILENEW        // File not found on system
        case FILEINCOMPLETE // File found, but incomplete
        case FILEUPDATE      // File has an updateMapview
    }
    
    static func serverErrorMessage(cmd :String, code aErrorCode :Int?) -> String {
        var errText :String = ""
        switch aErrorCode {
        
        // internal errors:
        case StChartCheckTask.CHECKRESULT_NOTCONNECTEDTOINTERNET:
            errText = "statuscheck_acterror_notconnected".localized
        case StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR:
            errText = "statuscheck_error_devicetoken".localized
            
        // stentec server response errors:
        case  -4: // error while doing something with database
            errText = "statuscheck_error_database".localized
        case   4: // User not found
            errText = "statuscheck_connect_unknownuser".localized
        case   2: // no anonymous activation allowed
            errText = "statuscheck_error_no_anonymous_activation".localized
        case -12: // devicecheck get bitstate failed
            errText = "statuscheck_error_appdevicecheck_get".localized
        case -13: // devicecheck set bitstate failed
            errText = "statuscheck_error_appdevicecheck_set".localized
        case  30: // there was still a blacklisted activation for a manually reset activation (1b2 situation)
            errText = "statuscheck_error_appwasactive".localized
        case  34: // given activation is not the active one (2b situation: app-installation (hwkey) key registered at stentec, bitstate zero)
            errText = "statuscheck_error_appact4ivediff".localized
        case  32: // given activation is blacklisted (3b1 situation)
            errText = "statuscheck_error_appblacklisted".localized
        case  33: // the given devicetype is not accepted (0 or 1 only)
            errText = "statuscheck_error_unknown_devtype".localized
        case  31: // 3a: installation-key activated at stentec, but different from current installation, and bitstate not set to one. - installed on a different iphone or ipad than this one.
            errText = "statuscheck_error_alreadyinstalled".localized
        case  35: // 2b situation: app-installation key activated at stentec (2), but “Bit state not found” (b))
            errText = "statuscheck_error_bitstatenotfound".localized
        case  6: // no activation for the given data
            errText = "statuscheck_error_activationnotfound".localized
            
        // "Create New Account" error messages
        case  3:
            switch cmd {
            case StChartCheckTask.COMMAND_REGISTER_ACCOUNT:
                errText = "statuscheck_userexists".localized // "Create New Account": User exists already
            case StChartCheckTask.COMMAND_CHECK_ACCOUNT_AND_RECEIPT, StChartCheckTask.COMMAND_GETSETS_ACCOUNT:
                errText = "statuscheck_error_receiptcheck_3_license_spent".localized // Receipt check: Purchase already spent.
            default:
                errText = "statuscheck_acterror_unknownerror".localized + " (error code: \(aErrorCode != nil ? String(aErrorCode!) : "nil"))"
            }
            
        //		case -4: // Database error
        //		case 94: // No email address given
        //			errText = res.getString(R.string.statuscheck_noemail); break;
        //		case 95: // No valid email address given
        //			errText = res.getString(R.string.statuscheck_novalidemail); break;
        //		case 96: // Email addresses not the same
        //			errText = res.getString(R.string.statuscheck_emailsdiffer); break;
        //		case 97: // No password given
        //			errText = res.getString(R.string.statuscheck_nopwd); break;
        //		case 98: // Password not long enough
        //			errText = res.getString(R.string.statuscheck_pwdshort); break;
        //		case 99: // Passwords not the same
        //			errText = res.getString(R.string.statuscheck_pwdsdiffer); break;
        //
        default:
            errText = "statuscheck_acterror_unknownerror".localized + " (error code: \(aErrorCode != nil ? String(aErrorCode!) : "nil"))"
        }
        return errText
    }
    
    //	let mViewController :UIViewController
    
    var mAborted = false
    var mActDB :StActManager
    //	private Button mButtonCancel;
    //	private Context mContext;
    var mChartCheckHandler :((String, Int, String) -> Void)?
    //	private StChartCheckGoogle mChartCheckG;
    var mChartCheckTsk :StChartCheckTask?
    var mChartCheckState :EChartCheckState?
    //	//private StDocumentFile mDKW2Path;
    //	//    private boolean mNetworkCheckAllowed;
    var mErrorCode :Int = 0
    var mHWKey :StHardwareKey
    //	private ProgressBar mProgressCharts;
    //	private ProgressBar mProgressChartDl;
    //	private Resources mResources;
    //	private TextView mTextViewCharts;
    //	private TextView mTextViewChartDl;
    var mUpdateFiles = [UpdateFileRecord]()
    var mUpdateIndex = 0
    var mTotalChartCount :Int = 0
    //	var mGoogleId :String? = nil
    var mUser :String?
    var mPass :String?
    
    var mChartManMode :Bool = false  // true, if the activity is called from the chart manager
    var mCheckedItem :String? = nil
    var mPid :Int = 0
    var mMod :Int = 0
    
    var instance :StatusCheck? = nil
    
    var mCallBackHandler :((ErrorCode, Bool, String) -> ())?
    
    //	{aResult: Error.RESULT_OK, aSucces: aSuccess, aData: mCheckedItem}
    
    //	private String mChartManModeData;
    //	private boolean developersMode = false;
    //	//private boolean developersMode = true;
    //
    //	private Runnable mContinueAfterSDCardPermission = null;
    //	private boolean mSDCardPermission;
    //
    //	StDKW2Settings mSettings;
    //
    //	private BroadcastReceiver mDlManComplete;
    //	private BroadcastReceiver mDlManClicked;
    //	private DownloadManager mDlMan;
    //
    //
    //	@Override
    //	protected void onActivityResult(int RequestCode, int ResultCode, Intent data) {
    
    func handleLoginData(aResult :DialogResult, aUser :String, aPass :String){
        //		if (RequestCode == AppConstants.ACTIVITY_USERACCOUNT) {
        //			finishActivity(AppConstants.ACTIVITY_USERACCOUNT);
        if  aResult == .ResultOk {
            //				String Data[] = data.getStringArrayExtra(UserAccountActivity.UARESULT);
            //				if (Data.length != 2 || Data[0].length() == 0 || Data[1].length() == 0) {
            //					Toast.makeText(this, mResources.getString("statuscheck_userpassempty), Toast.LENGTH_LONG).show();
            //					Intent intent = new Intent(this, UserAccountActivity.class);
            //					startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
            //				} else {
            
            mUser = aUser
            mPass = aPass
            mActDB.setUserData(aEmail: mUser!, aPassword: mPass!)
            //mChartCheckState = EChartCheckState.StCheckPem;
            checkLoginThenCharts(aAccountCheck: true)
            //		}
        } else {
            // no user data entered, check for tides
            mActDB.resetProducts()
            checkLoginThenCharts(aAccountCheck: false)
            //				//          statusCheckComplete(true);
        }
    }
    
    //		} else if (RequestCode == AppConstants.ACTIVITY_AVAILABLECHARTSETS) {
    //			finishActivity(AppConstants.ACTIVITY_AVAILABLECHARTSETS);
    //			if (ResultCode == RESULT_OK) {
    //				String Data = data.getStringExtra(AvailableChartSetsActivity.CSRESULT);
    //				if ((Data != null && Data.length() == 0) || Data == null) {
    //					finish();
    //				} else {
    //					mChartCheckState = EChartCheckState.StCheckCharts;
    //					mChartCheckSt.requestChartActAccount(mUser, mPass, Data);
    //				}
    //			} else {
    //				statusCheckComplete(true);
    //			}
    
    //	func callbackFromAvailableChartSets(aEMail :String, aPass :String, aChartList :String){
    //		mChartCheckState = EChartCheckState.StCheckCharts
    //		mChartCheckSt?.requestChartActAccount(aEMail: aEMail, aPass: aPass, aChartList :aChartList)
    //	}
    
    //		} else if (RequestCode == AppConstants.ACTIVITY_SHOPACTIVITY) {
    //			finishActivity(AppConstants.ACTIVITY_SHOPACTIVITY);
    //			if (ResultCode == RESULT_OK) {
    //				if (data.getIntExtra(IabActivity.IABRESULT, 0) == 1) {
    //					// do something for users that just ordered an upgrade
    //					Intent intent = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
    //					startActivity(intent);
    //					finish();
    //				}
    //			} else {
    //				statusCheckComplete(true);
    //			}
    //		} else if (RequestCode == AppConstants.INTENT_REQUEST_LOLLIPOP_SDCARD_PERMISSION) {
    //			StDKW2Settings.getSettings(this).onSDCardPermissionResult(ResultCode, data);
    //		}
    //	}
    //
    //	@Override
    //	public void onBackPressed(){
    //		Builder cd = new AlertDialog.Builder(this);
    //		cd.setTitle(mResources.getString("statuscheck_stopupdate_title));
    //		cd.setMessage(mResources.getString("statuscheck_stopupdate_msg));
    //		cd.setPositiveButton(mResources.getString("button_yes), new DialogInterface.OnClickListener() {
    //
    //			@Override
    //			public void onClick(DialogInterface dialog, int which) {
    //				cancel();
    //			}
    //		});
    //		cd.setNegativeButton(mResources.getString("button_no), new DialogInterface.OnClickListener() {
    //
    //			@Override
    //			public void onClick(DialogInterface dialog, int which) {
    //			}
    //		});
    //		if (!isFinishing())
    //		cd.show();
    //	}
    
    static func runFromDownloadService(callBackHandler : ((ErrorCode, Bool, String) -> ())?, pid :Int, mod :Int){
        _ = StatusCheck(callBackHandler: callBackHandler, pid: pid, mod: mod)
    }
    
    convenience init(){
        self.init(callBackHandler: nil, pid: 0, mod: 0)
    }
    
    //	@Override
    //	protected void onCreate(final Bundle savedInstanceState) {
    init(callBackHandler : ((ErrorCode, Bool, String) -> ())?, pid :Int, mod :Int){
        //		super.onCreate(savedInstanceState);
        
        //		mViewController = aViewController
        mCallBackHandler = callBackHandler
        
        mAborted = false;
        //		mContext = getApplicationContext();
        //		mResources = getResources();
        //		mChartManMode = false;
        
        mHWKey = StHardwareKey()
        mActDB = StActManager.getActManager()
        mActDB.initialize()
        
        //		mUpdateFiles = new ArrayList<>();
        //		mUpdateIndex = 0;
        //		mTotalChartCount = 0;
        //
        //		//        mNetworkCheckAllowed = false;
        //
        //		mDlMan = (DownloadManager)mContext.getSystemService(DOWNLOAD_SERVICE);
        
        //		// Check if we're coming from the chart manager
        //		Intent createIntent = getIntent();
        //		final String Data = createIntent.getStringExtra(AvailableChartSetsActivity.CSRESULT);
        //		if (Data != null && Data.length() > 0) {
        //			mChartManMode = true;
        //		}
        
        if pid != 0 && mod != 0 {
            mChartManMode = true
            mPid = pid
            mMod = mod
        }
        
        // Setup the checks
        //		if setupStatusCheck1() {
        //			setupStatusCheck2()
        //		}
        
        // content of setupStatusCheck1()
        
        mChartCheckHandler = { (cmd :String, Msg :Int, Data :String) -> Void in
            self.chartCheckHandleMessages(cmd: cmd, Msg: Msg, Data: Data)
        }
        
        mChartCheckTsk = StChartCheckTask(aHandler: mChartCheckHandler)
        
        if mChartManMode {
            let EMail :String = mActDB.getUserMail()
            let Pass :String = mActDB.getUserPass()
            //					mChartManModeData = Data;
            //			mChartCheckState = EChartCheckState.StRequestSingleChartset
            mChartCheckState = EChartCheckState.StCheckCharts
            
            mChartCheckTsk!.requestFilesForChartSet(aEMail: EMail, aPass: Pass, aPid: mPid, aMod: mMod, aErrorHandler: {(result :ErrorCode) in
                print("StCheck", "Result: ",  result)
            })
            
            return
        }
        
        checkLoginThenCharts(aAccountCheck: true)
    }
    
    
    // unused
    func setupStatusCheck1() -> Bool {
        
        //		mSettings = StDKW2Settings.getSettings(this);
        //
        //		//mDKW2Path = settings.getDKW2ChartsDir();
        //
        //		StDocumentFile UserPath = new StDocumentFile(mSettings.getDKW2ChartsDir(), "User");
        //		UserPath.mkdirs();
        //
        //		// check activations and updates of installed charts
        //		if (!StUtils.checkNetworkConnection(this)) {
        //			// no internet connection, check if all charts have been activated.
        //			// if there are no charts available or activated, tell this to the user.
        //
        //			// check if the app has successfully gone through activation check
        //			boolean s = true;
        //			if (!developersMode)
        //			if (mActDB.checkProductAct(getPackageName(), mHWKey.getDeviceKey()).equals("")) {
        //				s = false;
        //			}
        //			Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_nointernet), Toast.LENGTH_LONG).show();
        //			statusCheckComplete(s);
        //			return false;
        //		} else {
        //			setContentView(R.layout.activity_status_check);
        //
        //			ImageView iv = (ImageView)findViewById(R.id.imageView1);
        //			TextView tv = (TextView)findViewById(R.id.tvApp_name);
        //			int AppId = mActDB.checkProductAppID(getPackageName(), StHardwareKey.getHardwareKey(this), getResources().getInteger(R.integer.appid));
        //			if (AppId == 69633 || AppId == 102403) {
        //				//          iv.setImageResource(R.drawable.img_splash_wingps_marine_lite);
        //				tv.setText("WinGPS Marine+");
        //			} else if (AppId == 69632 || AppId == 102402) {
        //				tv.setText("WinGPS Marine");
        //				//          iv.setImageResource(R.drawable.img_splashm);
        //			}
        //
        //			mProgressCharts = (ProgressBar)findViewById(R.id.progressBar2);
        //			mProgressCharts.setVisibility(View.GONE);
        //			mProgressChartDl = (ProgressBar)findViewById(R.id.progressBar1);
        //			mProgressChartDl.setVisibility(View.GONE);
        //			mTextViewCharts = (TextView)findViewById(R.id.tvCheckFiles);
        //			mTextViewChartDl = (TextView)findViewById(R.id.tvCheckUpdates);
        //			mTextViewChartDl.setVisibility(View.GONE);
        //			mButtonCancel = (Button)findViewById(R.id.buttonChDlCancel);
        //			mButtonCancel.setOnClickListener(new View.OnClickListener() {
        //				@Override
        //				public void onClick(View v) {
        //					cancel();
        //				}
        //			});
        //			mButtonCancel.setVisibility(View.GONE);
        //
        
        mChartCheckHandler = { (cmd :String, Msg :Int, Data :String) -> Void in
            self.chartCheckHandleMessages(cmd: cmd, Msg: Msg, Data: Data)
        }
        
        //			mChartCheckState = EChartCheckState.StCheck;
        //			//            mChartCheckState = EChartCheckState.StCheckPem;
        //
        //			mChartCheckG = null;
        //			mChartCheckSt = null;
        //			try {
        //			mChartCheckSt = StChartCheckTask(this, mChartCheckHandler);
        
        mChartCheckTsk = StChartCheckTask(aHandler: mChartCheckHandler)
        
        //			} catch (Base64DecoderException e) {
        //			Log.d("StatusCheck", "Base64DecoderException: " + e.getMessage());
        //			} catch (IOException e) {
        //			Log.d("StatusCheck", "IOException: " + e.getMessage());
        //			}
        //			if (mChartCheckSt == null) {
        //				Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_unknownerror), Toast.LENGTH_LONG).show();
        //				statusCheckComplete(false);
        //				return false;
        //			}
        //		}
        //
        //		StUtils.lockRotation(this);
        //
        return true;
    }
    
    // unused
    func setupStatusCheck2(){
        
        // If we're in chart manager mode, this means a specific chart is reqeuested for activation and download
        if mChartManMode {
            let EMail :String = mActDB.getUserMail()
            let Pass :String = mActDB.getUserPass()
            //					mChartManModeData = Data;
            //			mChartCheckState = EChartCheckState.StRequestSingleChartset
            mChartCheckState = EChartCheckState.StCheckCharts
            
            mChartCheckTsk!.requestFilesForChartSet(aEMail: EMail, aPass: Pass, aPid: mPid, aMod: mMod, aErrorHandler: {(result :ErrorCode) in
                print("StCheck", "Result: ",  result)
            })
            
            return
        }
        
        //
        // TODO:
        //		// If savedInstanceState == null, this is the first time StatusCheck is called, so check with the Play Store
        //		// If it is not null, we are coming back from useraccount or availablechartsets, onActivityResult will start the check.
        //		if (savedInstanceState == null) {
        //			if (!developersMode){
        
        //		mChartCheckState = EChartCheckState.GPlay
        //		mChartCheckG = StChartCheckGoogle(mContext, mHWKey, mChartCheckHandler)
        
        //			} else {
        // replace this with a google play check
        
        checkLoginThenCharts(aAccountCheck: true)
        
        //			}
        //		}
    }
    
    
    //	/*
    //	@Override
    //	protected void onResume() {
    //	super.onResume();
    //
    //	IntentFilter clickedFilter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);
    //	mDlManClicked = new BroadcastReceiver() {
    //	@Override
    //	public void onReceive(Context context, Intent intent) {
    //	long[] dlIds = intent.getLongArrayExtra(DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS);
    //	for (long dlId : dlIds) {
    //	if (dlId == mChartCheckSt.getCurrentDlID()) {
    //	Toast.makeText(getApplicationContext(), "Current download clicked...", Toast.LENGTH_LONG).show();
    //	}
    //	}
    //	}
    //	};
    //	registerReceiver(mDlManClicked, clickedFilter);
    //
    //	IntentFilter completeFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
    //	mDlManComplete = new BroadcastReceiver() {
    //	@Override
    //	public void onReceive(Context context, Intent intent) {
    //	long dlId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
    //	if (dlId == mChartCheckSt.getCurrentDlID()) {
    //	DownloadManager.Query query = new DownloadManager.Query();
    //	query.setFilterById(dlId);
    //	Cursor c = mDlMan.query(query);
    //	c.moveToFirst();
    //	int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
    //	int dlStatus = c.getInt(columnIndex);
    //
    //	if (dlStatus == DownloadManager.STATUS_SUCCESSFUL) {
    //	Toast.makeText(getApplicationContext(), "File downloaded...", Toast.LENGTH_LONG).show();
    //
    //	UpdateFileRecord ufr = mUpdateFiles.get(mUpdateIndex);
    //	StDocumentFile tmpFile;
    //	StDocumentFile path;
    //	if (ufr.Pid == 899000) {
    //	if (ufr.Mod == 1)
    //	path = new StDocumentFile(new File(Environment.getExternalStorageDirectory() + "/stentec/tides/"));
    //	else
    //	path = new StDocumentFile(new File(StActManager.getActManager().getWWFilePath(mContext, false)));
    //	} else
    //	path = mSettings.getDKW2ChartsDir();
    //	tmpFile = new StDocumentFile(path, ufr.FileName + ".tmp");
    //
    //	StDocumentFile newFile = new StDocumentFile(path, ufr.FileName);
    //	if (tmpFile.exists() && tmpFile.length() == ufr.FileSize) {
    //	if (!tmpFile.renameWithinSameDirectory(newFile)) {
    //	Log.e("popke", "Error! renameWithinSameDirectory() failed for: " + tmpFile.toString() + " to " + newFile.toString()+". Deleting tmpfile");
    //	tmpFile.delete();
    //	}
    //	}
    //
    //	// Reload network if we just downloaded it
    //	if (ufr.Pid == 899000 && ufr.Mod == 2) {
    //	String WWFilename = StActManager.getActManager().getWWFilePath(mContext, true);//String WWFilename = Environment.getExternalStorageDirectory() + "/stentec/WaterwayNetwork.stwwn";
    //	StWWNetwork.reloadNetwork(WWFilename, null);
    //	}
    //
    //	// next file
    //	mUpdateIndex++;
    //	if (!mAborted && mUpdateIndex < mUpdateFiles.size())
    //	chartCheck();
    //	else {
    //	if (mAborted)
    //	mTextViewCharts.setText(mResources.getString("statuscheck_aborted));
    //	else
    //	mTextViewCharts.setText(mResources.getString("statuscheck_alluptodate));
    //	mTextViewChartDl.setText(mResources.getString("statuscheck_loadingcharts));
    //	statusCheckComplete(true);
    //	}
    //	} else if (dlStatus == DownloadManager.STATUS_FAILED) {
    //	columnIndex = c.getColumnIndex(DownloadManager.COLUMN_REASON);
    //	int dlReason = c.getInt(columnIndex);
    //	if (dlReason == 1) {
    //
    //	}
    //
    //	}
    //	}
    //	}
    //	};
    //	registerReceiver(mDlManComplete, completeFilter);
    //	}
    //
    //	@Override
    //	protected void onPause() {
    //	super.onPause();
    //	unregisterReceiver(mDlManClicked);
    //	unregisterReceiver(mDlManComplete);
    //	}
    //	*/
    //	private void cancel() {
    //		mChartCheckSt.cancel();
    //		mAborted = true;
    //		statusCheckComplete(mChartCheckState != EChartCheckState.StCheck && mChartCheckState != EChartCheckState.GPlay);
    //	}
    
    func checkLoginThenCharts(aAccountCheck :Bool) {
        
        if aAccountCheck {
            
            var EMail = mActDB.getUserMail()
            var Pass = mActDB.getUserPass()
            
            if EMail.count == 0 || Pass.count == 0 {
                
                //				Intent intent = new Intent(this, UserAccountActivity.class);
                //				intent.putExtra(UserAccountActivity.UACANCELABLE, true);
                //				startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
                
                //				StatusCheck.showLoginDialog(callingViewController: mViewController, continueWith: {(result :DialogResult, email :String, pw :String) -> () in
                //					self.handleLoginData(aResult: result, aUser: email, aPass: pw)}
                //				)
                
                StatusCheck.showLoginController(handler: nil)
                
                return
                
            } else {
                mUser = EMail;
                mPass = Pass;
            }
        }
        
        if Resources.appType == .friesemeren {
            mChartCheckState = EChartCheckState.StCheckFrieseMeren

        } else {
            
            //		mChartCheckState = EChartCheckState.StCheck
            mChartCheckState = EChartCheckState.StCheckCharts
        }
        
        chartCheck();
        //        mChartCheckState = EChartCheckState.StCheckPem;
        //        mChartCheckSt.checkPemFile();
    }
    
    func chartCheck() {
        
        var Result = ErrorCode.CHECKRESULT_OK
        
        //		if mChartCheckState == EChartCheckState.StCheck {
        //			print("StCheck: mChartCheckState == StCheck")
        //
        //			//			if mGoogleId == nil {
        //			//				if mActDB.getUserID().count > 0 {
        //			//					print("StCheck: Getting UserID")
        //			//					mGoogleId = mActDB.getUserID()
        //			//				} else {
        //			//					//					Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_unknownproduct), Toast.LENGTH_LONG).show();
        //			//					// TODO showError
        //			//					print("ERROR: statuscheck_acterror_unknownproduct")
        //			//					statusCheckComplete(false) // comment to avoid playstore check
        //			//					return // comment to avoid playstore check
        //			//				}
        //			//			}
        //
        //			//			Result = mChartCheckSt!.requestBestActivation(aUid: mGoogleId ?? "", aActDB: mActDB)
        //
        //			doAppleDeviceCheck()
        //
        //			Result = mChartCheckTsk!.requestBestActivation(aActDB: mActDB)
        //
        //		} else
        if (mChartCheckState == EChartCheckState.StCheckTides) {
            //			Log.d("StCheck", "mChartCheckState == StCheckTides");
            //			File tideDir = new File(Environment.getExternalStorageDirectory() + "/stentec/tides/");
            //			tideDir.mkdirs();
            //			Result = mChartCheckSt.requestTideFiles();
        } else if mChartCheckState == EChartCheckState.StCheckNetwork {
            dout("mChartCheckState == StCheckNetwork")
            
            Result = mChartCheckTsk!.checkWWNetwork()
            
        } else if (mChartCheckState == EChartCheckState.StCheckCharts) {
            print("StCheck", "mChartCheckState == StCheckCharts")
            if mActDB.getProductCodeCount() > 0 {
                // activations available on the device, so check them.
                //				Result = mChartCheckTsk!.requestChartCheckAccount(aActDB: mActDB)
                mChartCheckTsk!.requestCheckAccountGetChartsetFiles(aActDB: mActDB)
            } else {
                // no activations available, check if userdata has been entered.
                if mActDB.getUserDataAvailable() {
                    
                    // load chart selection
                    //					Intent intent = new Intent(this, AvailableChartSetsActivity.class);
                    //
                    //					intent.putExtra("User", mActDB.getUserMail());
                    //					intent.putExtra("Pass", mActDB.getUserPass());
                    //					startActivityForResult(intent, AppConstants.ACTIVITY_AVAILABLECHARTSETS);
                    
                    //					var chartsView = AvailableChartSets(caller: self, user: mActDB.getUserMail(), pass: mActDB.getUserPass())
                    
                    //					showAvailableChartSetsScreen(caller: self, user: mActDB.getUserMail(), pass: mActDB.getUserPass())
                    showDownloadManagerView(caller: self)
                    //AvailableChartSets()
                    
                    Result = ErrorCode.CHECKRESULT_OK
                } else {
                    //					// no user data available check for tides updates.
                    //					if (mUpdateFiles.size() > 0) {
                    //						// if updates found, ask to download them.
                    //						askForSDUsageThenAskDownload(mTotalChartCount);
                    //						Result = ErrorCode.CHECKRESULT_OK;
                    //					} else {
                    //						// no tide updates continue to main
                    //						statusCheckComplete(true);
                    //					}
                }
            }
            //    } else if (mChartCheckState == EChartCheckState.StGetFileInfo) {
            //       Result = mChartCheckSt.requestFileInfo(mUser, mActCode);
        } else if (mChartCheckState == EChartCheckState.StGetFile) {
            print("StCheck", "mChartCheckState == StGetFile")
            
            var ufr = mUpdateFiles[mUpdateIndex]
            var size :Int64 = ufr.FileSize!
            if ufr.FileState == EFileState.FILEINCOMPLETE {
                size -= ufr.IncompleteSize!
            }
            
            let fileUrl :URL = getDKW2Directory().appendingPathComponent(ufr.FileName!)
            
            //	check for available space
            guard let spaceAvail = getAvailableSpace(fileUrl: fileUrl) else { return }
            
            while size >= spaceAvail {
                mUpdateIndex += 1
                if mAborted || mUpdateIndex >= mUpdateFiles.count {
                    //					if (mAborted) {
                    //						mTextViewCharts.setText(mResources.getString("statuscheck_aborted))
                    //					} else {
                    //						mTextViewCharts.setText(mResources.getString("statuscheck_alluptodate))
                    //					}
                    //					mTextViewChartDl.setText(mResources.getString("statuscheck_loadingcharts))
                    statusCheckComplete(true)
                    return
                }
                ufr = mUpdateFiles[mUpdateIndex]
                size = ufr.FileSize!
                if (ufr.FileState == EFileState.FILEINCOMPLETE) {
                    size -= ufr.IncompleteSize!
                }
            }
            
            //			mProgressChartDl.setProgress(0)
            //			mProgressCharts.setProgress(Math.round((float)mUpdateIndex / (float)mUpdateFiles.size() * 100));
            //			String text = mResources.getString("statuscheck_update) + ": " + String.valueOf(mUpdateIndex + 1) + " / " + String.valueOf(mUpdateFiles.size());
            var toGo :Int64 = 0
            for i in mUpdateIndex ..< mUpdateFiles.count {
                toGo += mUpdateFiles[i].FileSize!
            }
            //			text += " (" + StUtils.bytesToFileSize(togo) + ")"
            //			mTextViewCharts.setText(text);
            //			mTextViewChartDl.setVisibility(View.VISIBLE);
            //			mTextViewChartDl.setText(mResources.getString("statuscheck_updateprogress) + ":");
            //			mButtonCancel.setVisibility(View.VISIBLE);
            
            // check for an existing temporary file to append to
            //			StDocumentFile tmpFile = new StDocumentFile(mSettings.getDKW2ChartsDir(), ufr.FileName + ".tmp")
            //			var tmpFile :URL = URL(fileURLWithPath: getDKW2Directory().path + ufr.FileName! + ".tmp")
            var tmpFile :URL = getDKW2Directory().appendingPathComponent(ufr.FileName! + ".tmp")
            
            var fStart :Int64 = 0
            if FileManager.default.fileExists(atPath: tmpFile.path) {
                if getFileSize(tmpFile.path)! != UInt(ufr.FileSize!) {
                    fStart = Int64(getFileSize(tmpFile.path)!)
                }
            }
            
            //ufr.DownloadManagerId = mChartCheckSt.requestFile(ufr.Pid, ufr.Mod, ufr.FileName);
            //return;
            print("chartCheck: sending download request. size: \(size), org. filesize: \(ufr.FileSize), incompl. size: \(ufr.IncompleteSize)")
            
            var Result :ErrorCode
            if ufr.Pid == 899000 && ufr.Mod == 2 {
                Result = mChartCheckTsk!.requestFile(aPid: ufr.Pid!, aMod: ufr.Mod!, aFilename: ufr.FileName!, aStart: fStart)
            } else {
                Result = mChartCheckTsk!.requestFile(aActDB: mActDB, aPid: ufr.Pid!, aMod: ufr.Mod!, aFilename: ufr.FileName!, aStart: fStart)
            }
            
        } else if (mChartCheckState == EChartCheckState.StCheckFrieseMeren) {
            
//            let actCode :String = mActDB.getActCode(aPid: mPid, aMod: mMod)
            
//            pid the productid of the product to get files for (for Friese Meren this is 196664)
//            mod the module of the product to get files for (for Friese Meren this is 1)

            mPid = 196664
            mMod = 1
            
            mChartCheckTsk!.requestFreeChartActAccount(aEMail: mUser ?? "", aPass: mPass ?? "", aPid: mPid, aMod: mMod, aErrorHandler: {(result :ErrorCode) in
                print("StCheck", "Result: ",  result)
            })

        }
        
        print("StCheck", "Result: ",  Result)
        
        if Result != ErrorCode.CHECKRESULT_OK {
            //			Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_unknownerror), Toast.LENGTH_LONG).show();
            // TODO: error message
            print("statuscheck_acterror_unknownerror")
            statusCheckComplete(false)
        }
    }
    
    func chartCheckHandleMessages(cmd :String, Msg :Int, Data :String) {
        var msgCodeInt :Int
        var WaitForInput = false
        if (mChartCheckState == EChartCheckState.GPlay) {
            //			// do something with the error code
            //			ErrorCode = mChartCheckG.getErrorCode();
            //			// 1001 = all ok, 3257 = could not connect to play server
            //			if (ErrorCode == 1001) {
            //				//                if (ErrorCode == 1001 || ErrorCode == 3257) {
            //				mUser = Msg.getData().getString(StChartCheckGoogle.CHECKRESULT_GEBRUIKER);
            //				if (mActDB.getUserID().length() == 0 && mUser != null)
            //				mActDB.setUserID(mUser);
            //
            //				//mChartCheckState = EChartCheckState.StCheckPem;
            //				if (!mAborted)
            //				checkCharts(true);
            //
            //				//                mChartCheckState = EChartCheckState.StCheckPem;
            //				//            if (!mActDB.checkProduct(getPackageName(), mHWKey.getDeviceKey()))
            //				//               mActDB.addProduct(getPackageName(), mHWKey.getDeviceKey());
            //				//            if (!mAborted)
            //				//               checkCharts(true);
            //			} else {
            //				showActError(ErrorCode);
            //}
            ///*
            //} else if (mChartCheckState == EChartCheckState.StCheckPem) {
            //ErrorCode = Msg.what;
            //if (ErrorCode == StChartCheckTask.CHECKRESULT_OK) {
            //mChartCheckState = EChartCheckState.StCheck;
            //if (!mAborted)
            //chartCheck();
            //} else {
            //showActError(ErrorCode);
            //}
            //
            ///*
            //if (ErrorCode == StChartCheckTask.CHECKRESULT_OK) {
            //if (getResources().getInteger(R.integer.appid) == 69633) {
            //mChartCheckState = EChartCheckState.StCheckTides;
            //} else {
            //mChartCheckState = EChartCheckState.StCheckCharts;
            //}
            //if (!mAborted)
            //chartCheck();
            //else
            //statusCheckComplete(true);
            //} else {
            //showActError(ErrorCode);
            //}*/
            //		} else if (mChartCheckState == EChartCheckState.StCheck) {
            //			msgCodeInt = Msg
            //			if msgCodeInt == StChartCheckTask.CHECKRESULT_OK {
            //
            //				//            if (!mActDB.checkProduct(getPackageName(), mHWKey.getDeviceKey()))
            //				//            mActDB.addProduct(getPackageName(), mHWKey.getDeviceKey());
            //				//         if (!mAborted)
            //				//            checkCharts(true);
            //
            //				let paramStr = Data
            //				//				String[] params = (paramStr != null) ? paramStr.split("\n") : new String[0];
            //				let params = paramStr.split{$0 == "\n"}.map(String.init)
            //				if params.count == 0 {
            //					//					Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
            //					// TODO Error
            //					statusCheckComplete(false)
            //					return
            //				}
            //
            //				var AppID :Int = Resources.appid
            //
            //				//				try {
            //				AppID = Int(params[0]) ?? AppID
            //				//				} catch (NumberFormatException e) {
            //				//				}
            //				let ActCode :String = params.count >= 2 ? params[1] : ""
            //
            //				//TODO if !mActDB.checkProductAct(getPackageName(), mHWKey.getDeviceKey()).equals(ActCode + "_" + AppID))
            //				//TODO mActDB.addProductAct(getPackageName(), mHWKey.getDeviceKey(), ActCode, AppID);
            //
            //				// handle inapp products
            //				if params.count > 2 { //TODO for now, just check charts
            //					//					let InAppCount :Int = Int(params[2])!
            //					//					let InAppIndex :Int = 3
            //					//					for j in 0 ..< InAppCount {
            //					//						var CP :StActManager.ChartProduct = StActManager.ChartProduct()
            //					//						CP.Pid = Int(params[InAppIndex]) ?? 0
            //					//						CP.Mod = 0
            //					//						CP.ActCode = params[InAppIndex + 1]
            //					//						var Index :Int = mActDB.findProductCode(CP.Pid, CP.Mod)
            //					//						if (Index == -1) {
            //					//							CP.Enabled = true;
            //					//							mActDB.addActCode(CP);
            //					//						}
            //					//						InAppIndex += 2;
            //					//					}
            //				}
            //
            //				//				if (AppID == 69633 || AppID == 102403) {
            //				//					mChartCheckState = EChartCheckState.StCheckTides;
            //				//				} else {
            //				//					//                    mChartCheckState = mNetworkCheckAllowed ? EChartCheckState.StCheckNetwork : EChartCheckState.StCheckCharts;
            //				mChartCheckState = EChartCheckState.StCheckCharts;
            //				//				}
            //
            //				//				//// Bring up AvailableChartSets
            //				//				var chartsView = AvailableChartSets(caller: self, user: mActDB.getUserMail(), pass: mActDB.getUserPass())
            //
            //
            //				if (!mAborted) {
            //					chartCheck()
            //				} else {
            //					statusCheckComplete(true)
            //				}
            //			} else {
            //				showActError(aErrorCode: msgCodeInt)
            //			}
            
            
            //} else if (mChartCheckState == EChartCheckState.StCheckTides) {
            //ErrorCode = Msg.what;
            //if (ErrorCode == StChartCheckTask.CHECKRESULT_OK) {
            //String paramStr = Msg.getData().getString(StChartCheckTask.CHECKRESULT_DATA);
            //String[] params = (paramStr != null) ? paramStr.split("\n") : new String[0];
            //if (params.length == 0) {
            //Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
            //mChartCheckState = EChartCheckState.StCheckCharts;
            //chartCheck();
            //return;
            //}
            //
            //int tideFileCount = Integer.valueOf(params[0]);
            //mTotalChartCount += tideFileCount;
            //int PIndex = 1;
            //for (int j = 0; j < tideFileCount; j++) {
            //String fileName = params[PIndex];
            //File foundFile = new File(Environment.getExternalStorageDirectory() + "/stentec/tides/", fileName);
            ////               boolean canRead = foundFile.canRead();
            //boolean Found = foundFile.exists();
            //long LModified = foundFile.lastModified();
            //long SModified = Long.valueOf(params[PIndex + 2]) * 1000;
            //if (!Found || (Found && LModified < SModified)) {
            //UpdateFileRecord ufr = new UpdateFileRecord();
            //ufr.ActCode = "";
            //ufr.Pid = 899000;
            //ufr.Mod = 1;
            //ufr.FileName = fileName;
            //ufr.FileSize = Long.valueOf(params[PIndex + 1]);
            //
            //// check if an incomplete file is available
            //ufr.IncompleteSize = 0;
            //ufr.FileState = EFileState.FILEUPDATE;
            //File tmpFile = new File(Environment.getExternalStorageDirectory() + "/stentec/tides/", ufr.FileName + ".tmp");
            //if (tmpFile.exists()) {
            //// check if the temp file is not bigger than expected if so delete it and
            //// flag this file as a new download
            //if (tmpFile.length() > ufr.FileSize) {
            //tmpFile.delete();
            //ufr.FileState = EFileState.FILENEW;
            //} else {
            //ufr.FileState = EFileState.FILEINCOMPLETE;
            //ufr.IncompleteSize = tmpFile.length();
            //}
            //} else if (!Found) {
            //ufr.FileState = EFileState.FILENEW;
            //}
            //
            //mUpdateFiles.add(ufr);
            //}
            //PIndex += 3;
            //}
            //}
            ////            mChartCheckState = mNetworkCheckAllowed ? EChartCheckState.StCheckNetwork : EChartCheckState.StCheckCharts;
            //mChartCheckState = EChartCheckState.StCheckCharts;
            //chartCheck();
            //
        } else if (mChartCheckState == EChartCheckState.StCheckCharts) {
            msgCodeInt = Msg
            if msgCodeInt == StChartCheckTask.CHECKRESULT_OK {
                
                let useJSON = true
                
                if useJSON { // parse json
                    if let response = try? JSONSerialization.jsonObject(with: Data.data(using: .utf8)!) as! [String : Any]{
                        print(response)
                        
                        var products: [[String : Any]] = response["Products"] as! [[String : Any]]
                        
                        var actManagerFiles = [String]()
                        
                        for product in products {
                            print("\(product)")
                            
                            actManagerFiles.removeAll()
                            
                            guard
                                let ares = toOptInt(product["ares"]),
                                let pid = Int(product["pid"] as? String ?? ""),
                                let mod = Int(product["mod"] as? String ?? "") else { continue }
                            
                            var Index = mActDB.findProductCode(pid: pid, mod: mod)
                            if Index == -1 {
                                mActDB.addProductCode(pid: pid, mod: mod, enabled: true, subType: 0)
                            }
                            
                            if ares == StChartCheckTask.CHECKRESULT_OK {
                                guard let charts = product["Charts"] as? [[String : Any]] else { continue }
                                
                                for chart in charts {
                                    guard
                                        let fileName = chart["filename"] as? String,
                                        let fileSize = optIntToOptInt64(toOptInt(chart["filesize"])),
                                        let filedate = toOptInt(chart["filedate"]) else { continue }
                                    actManagerFiles.append(fileName)
                                    
                                    let foundFile :URL? = getDKW2Directory().appendingPathComponent(fileName)
                                    let ChartFound :Bool = FileManager.default.fileExists(atPath: foundFile!.path)
                                    let LModified = getLastModified(foundFile!.path) ?? Date(timeIntervalSince1970: 0)
                                    let SModified = Date(timeIntervalSince1970: TimeInterval(filedate))
                                    if (!ChartFound || (ChartFound && LModified < SModified)) {
                                        
                                        var ufr = UpdateFileRecord()
                                        ufr.Pid = pid
                                        ufr.Mod = mod
                                        ufr.FileName = fileName
                                        ufr.FileSize = fileSize
                                        
                                        // check if an incomplete file is available
                                        ufr.IncompleteSize = 0
                                        ufr.FileState = EFileState.FILEUPDATE
                                        let tmpFile = getDKW2Directory().appendingPathComponent(ufr.FileName! + ".tmp")
                                        if FileManager.default.fileExists(atPath: tmpFile.path) {
                                            // check if the temp file is not bigger than expected if so delete it and
                                            // flag this file as a new download
                                            if getFileSize(tmpFile.path) ?? 0 > ufr.FileSize ?? 0 {
                                                do {
                                                    try FileManager.default.removeItem(at: tmpFile)
                                                } catch {
                                                }
                                                ufr.FileState = EFileState.FILENEW
                                            } else {
                                                ufr.FileState = EFileState.FILEINCOMPLETE
                                                ufr.IncompleteSize = Int64(getFileSize(tmpFile.path)!)
                                            }
                                        } else if (!ChartFound) {
                                            ufr.FileState = EFileState.FILENEW
                                        }
                                        mUpdateFiles.append(ufr)
                                    }
                                }
                                
                                mActDB.updateFiles(aPid: pid, aMod: mod, aFiles: actManagerFiles)
                            } else { // ares != StChartCheckTask.CHECKRESULT_OK
                                mActDB.updateFiles(aPid: pid, aMod: mod, aFiles: actManagerFiles)
                                let Index = mActDB.findProductCode(pid: pid, mod: mod)
                                if Index >= -1 {
                                    mActDB.deleteProductCode(aPid: pid, aMod: mod)
                                }
                                WaitForInput = true
                                if (pid == 102402 || pid == 102403) {
                                    showExpired() // trail period expired
                                } else {
                                    showActError(aErrorCode: ares)
                                }
                            }
                        }
                    }
                    
                } else { // response in (old) line-by-line format
                    
                    //
                    //					var paramStr :String = Data
                    //					//				var params :[String] = paramStr.split{$0 == "\n"}.map(String.init) ?? [String]()
                    //					var params = (paramStr.split(separator: "\n", omittingEmptySubsequences: false)).map{ String($0) }
                    //
                    //					if params.count == 0 {
                    //						//	TODO				Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
                    //						statusCheckComplete(false);
                    //					}
                    //
                    //					print("params 0, 1: ", params[0], params[1])
                    //					var ActCount = Int(params[0])
                    //					var PIndex = 1
                    //					var pid, mod, ares, chartCount :Int
                    //					var ActCode, PStr, ModStr :String
                    //					var CP = StActManager.ChartProduct()
                    //					var actManagerFiles = [String]()
                    //
                    //					for i in 0 ..< ActCount! {
                    //						actManagerFiles.removeAll()
                    //
                    //						pid = Int(params[PIndex])!
                    //						mod = Int(params[PIndex + 1])!
                    //						PStr = params[PIndex + 2]
                    //						ModStr = params[PIndex + 3]
                    //						ares = Int(params[PIndex + 4])!
                    //						if ares == StChartCheckTask.CHECKRESULT_OK {
                    //							ActCode = params[PIndex + 5]
                    //
                    //							CP.Pid = pid
                    //							CP.Mod = mod
                    //							//						CP.ActCode = ActCode
                    //
                    //							var Index = mActDB.findProductCode(aPid: pid, aMod: mod)
                    //							if Index == -1 {
                    //								CP.Enabled = true
                    //								mActDB.addActCode(aCP: CP)
                    //							}
                    //
                    //							// Get chart files for this product
                    //							chartCount = Int(params[PIndex + 6]) ?? 0
                    //							mTotalChartCount += chartCount
                    //							PIndex += 7
                    //
                    //							for j in 0 ..< chartCount {
                    //								var fileName = params[PIndex]
                    //								actManagerFiles.append(fileName)
                    //								let foundFile :URL? = getDKW2Directory().appendingPathComponent(fileName)
                    //
                    //								let ChartFound :Bool = FileManager.default.fileExists(atPath: foundFile!.path)
                    //								let LModified = getLastModified(foundFile!.path) ?? Date(timeIntervalSince1970: 0)
                    //								//							let SModified = Int64(params[PIndex + 2])! * 1000
                    //								let SModified = Date(timeIntervalSince1970: TimeInterval(params[PIndex + 2])!)
                    //								if (!ChartFound || (ChartFound && LModified < SModified)) {
                    //									var ufr = UpdateFileRecord()
                    //									//								ufr.ActCode = CP.ActCode
                    //									ufr.Pid = CP.Pid
                    //									ufr.Mod = CP.Mod
                    //									ufr.FileName = fileName
                    //									ufr.FileSize = Int64(params[PIndex + 1])
                    //
                    //									// check if an incomplete file is available
                    //									ufr.IncompleteSize = 0
                    //									ufr.FileState = EFileState.FILEUPDATE
                    //									//								StDocumentFile tmpFile = new StDocumentFile(mSettings.getDKW2ChartsDir(), ufr.FileName + ".tmp")
                    //									//								let tmpFile = URL(fileURLWithPath: Resources.DKW2ChartsDir + (ufr.FileName! + ".tmp"))
                    //									let tmpFile = getDKW2Directory().appendingPathComponent(ufr.FileName! + ".tmp")
                    //
                    //
                    //									if FileManager.default.fileExists(atPath: tmpFile.path) {
                    //										// check if the temp file is not bigger than expected if so delete it and
                    //										// flag this file as a new download
                    //										if getFileSize(tmpFile.path) ?? 0 > ufr.FileSize ?? 0 {
                    //											do {
                    //												try FileManager.default.removeItem(at: tmpFile)
                    //											} catch {
                    //
                    //											}
                    //											ufr.FileState = EFileState.FILENEW
                    //										} else {
                    //											ufr.FileState = EFileState.FILEINCOMPLETE
                    //											ufr.IncompleteSize = Int64(getFileSize(tmpFile.path)!)
                    //										}
                    //									} else if (!ChartFound) {
                    //										ufr.FileState = EFileState.FILENEW
                    //									}
                    //
                    //									mUpdateFiles.append(ufr)
                    //
                    //								}
                    //								PIndex += 3
                    //							}
                    //
                    //							mActDB.updateFiles(aPid: pid, aMod: mod, aFiles: actManagerFiles)
                    //						} else { // ares != StChartCheckTask.CHECKRESULT_OK
                    //							mActDB.updateFiles(aPid: pid, aMod: mod, aFiles: actManagerFiles)
                    //							let Index = mActDB.findProductCode(aPid: pid, aMod: mod)
                    //							if Index >= -1 {
                    //								mActDB.deleteActCode(aPid: pid, aMod: mod)
                    //							}
                    //							WaitForInput = true
                    //							if (pid == 102402 || pid == 102403) {
                    //								showExpired() // trail period expired
                    //							} else {
                    //								showActError(aErrorCode: ares)
                    //							}
                    //						}
                    //					}
                } // /old way
                
                if (getNetworkCheckAllowed()) {
                    mChartCheckState = EChartCheckState.StCheckNetwork
                    chartCheck()
                    
                } else {
                    
                    //                    if mUpdateFiles.count > 0 {
                    //                        //					if mChartManMode { // <- this condition prevents updating on startup, so remove it eventually
                    //                        askForDownload(mTotalChartCount)
                    //                        //					}
                    //                    } else {
                    //                        DKW2ChartManager.refreshCharts()
                    //                        // No downloads, so reset tablecell and update DownloadManager screen
                    //                        if mChartManMode,
                    //                           let downloadManagerController = getTopViewController() as? DownloadManagerController{
                    //                            downloadManagerController.finishDownload(pid: mPid, mod: mMod)
                    //                        }
                    //                        if !WaitForInput {
                    //                            statusCheckComplete(true)
                    //                        }
                    //                    }
                    wrapUpChartCheckOrNetworkCheck(waitForInput: WaitForInput)
                    
                }
            } else {
                
                // update DownloadManager screen
                if mChartManMode,
                   let downloadManagerController = getTopViewController() as? DownloadManagerController {
                    downloadManagerController.finishDownload(pid: mPid, mod: mMod)
                }
                
                //				String toastText;
                //				boolean pass = false;
                if (msgCodeInt == StChartCheckTask.CHECKRESULT_INTERNALERROR) {
                    var errorText = "statuscheck_acterror_connect".localized
                    var pass = false
                    if (mActDB.getProductCodeCount() == 0) {
                        errorText += " " + "statuscheck_acterror_internet".localized
                    } else {
                        errorText += " " + "statuscheck_noupdatesapplied".localized
                        pass = true
                    }
                    //                    let alert = UIAlertController(title: "error_dialog_title".localized, message: errorText, preferredStyle: .alert)
                    let alert = UIAlertController(title: errorText, message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: .default))
                    getTopViewController()?.presentFromMain(alert, animated: true)
                    
                    statusCheckComplete(pass)
                    
                } else {
                    
                    showActError(aErrorCode: msgCodeInt)
                }
            }
            
        } else if (mChartCheckState == EChartCheckState.StCheckNetwork) {
            
            //                ErrorCode = Msg.what;
            //                if (ErrorCode == StChartCheckTask.CHECKRESULT_OK) {
            //                    String paramStr = Msg.getData().getString(StChartCheckTask.CHECKRESULT_DATA);
            //                    String[] params = (paramStr != null) ? paramStr.split("\n") : new String[0];
            //                    if (params.length == 0) {
            //                        Toast.makeText(getApplicationContext(), mResources.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
            //                        //                    mChartCheckState = EChartCheckState.StCheckCharts;
            //                        //                    chartCheck();
            //                        //                    return;
            //                    } else {
            //
            //                        int Count = Integer.valueOf(params[0]); // should be 1
            //                        if (Count == 1) {
            //                            mTotalChartCount += Count;
            //                            String fileName = params[1];
            //                            File foundFile = new File(StActManager.getActManager().getWWFilePath(mContext, false), fileName);
            //                            //               boolean canRead = foundFile.canRead();
            //                            boolean Found = foundFile.exists();
            //                            long LModified = foundFile.lastModified();
            //                            long SModified = Long.valueOf(params[2]) * 1000;
            //                            if (!Found || (Found && LModified < SModified)) {
            
            //                                UpdateFileRecord ufr = new UpdateFileRecord();
            //                                ufr.ActCode = "";
            //                                ufr.Pid = 899000;
            //                                ufr.Mod = 2;
            //                                ufr.FileName = fileName;
            //                                ufr.FileSize = Long.valueOf(params[3]);
            //
            //                                // check if an incomplete file is available
            //                                ufr.IncompleteSize = 0;
            //                                ufr.FileState = EFileState.FILEUPDATE;
            //                                File tmpFile = new File(StActManager.getActManager().getWWFilePath(mContext, false), ufr.FileName + ".tmp");
            //                                if (tmpFile.exists()) {
            //                                    // check if the temp file is not bigger than expected if so delete it and
            //                                    // flag this file as a new download
            //                                    if (tmpFile.length() > ufr.FileSize) {
            //                                        tmpFile.delete();
            //                                        ufr.FileState = EFileState.FILENEW;
            //                                    } else {
            //                                        ufr.FileState = EFileState.FILEINCOMPLETE;
            //                                        ufr.IncompleteSize = tmpFile.length();
            //                                    }
            //                                } else if (!Found) {
            //                                    ufr.FileState = EFileState.FILENEW;
            //                                }
            //
            //                                mUpdateFiles.add(ufr);
            //
            //                                if (StWWNetwork.getNetwork(this).isLoading())
            //                                StWWNetwork.getNetwork(this).cancelLoading();
            //                            }
            //                        }
            //                    }
            //                }
            
            msgCodeInt = Msg
            if msgCodeInt == StChartCheckTask.CHECKRESULT_OK {
                // Assuming JSON
                
                if let jsonResponse = try? JSONSerialization.jsonObject(with: Data.data(using: .utf8)!) as! [String : Any]{
                    print(jsonResponse)
                    
                    var files: [[String : Any]] = jsonResponse["Files"] as! [[String : Any]]
                    if files.count == 1,
                       let file = files.first {
                        mTotalChartCount += files.count
                        if
                            let fileName = file["file"] as? String,
                            let fileSize = optIntToOptInt64(toOptInt(file["filesize"])),
                            let fileDate = toOptInt(file["filedate"]) {
                            
                            let foundFile :URL = StUtils.getUrlByFilename(fileName)
                            let Found :Bool = FileManager.default.fileExists(atPath: foundFile.path)
                            let LModified = getLastModified(foundFile.path) ?? Date(timeIntervalSince1970: 0)
                            let SModified = Date(timeIntervalSince1970: TimeInterval(fileDate))
                            if !Found || (Found && LModified < SModified) {
                                
                                var ufr = UpdateFileRecord()
                                ufr.Pid = 899000
                                ufr.Mod = 2
                                ufr.FileName = fileName
                                ufr.FileSize = fileSize
                                
                                // check if an incomplete file is available
                                ufr.IncompleteSize = 0
                                ufr.FileState = EFileState.FILEUPDATE
                                let tmpFile :URL = StUtils.getUrlByFilename(fileName + ".tmp")
                                if FileManager.default.fileExists(atPath: tmpFile.path) {
                                    // check if the temp file is not bigger than expected if so delete it and
                                    // flag this file as a new download
                                    if getFileSize(tmpFile.path) ?? 0 > ufr.FileSize ?? 0 {
                                        do {
                                            try FileManager.default.removeItem(at: tmpFile)
                                        } catch {
                                        }
                                        ufr.FileState = EFileState.FILENEW
                                    } else {
                                        ufr.FileState = EFileState.FILEINCOMPLETE
                                        ufr.IncompleteSize = Int64(getFileSize(tmpFile.path)!)
                                    }
                                } else if !Found {
                                    ufr.FileState = EFileState.FILENEW
                                }
                                mUpdateFiles.append(ufr)
                                
                                if WWNetwork.network?.mLoading ?? false {
                                    WWNetwork.network?.cancelLoading()
                                }
                            }
                        }
                    }
                }
            }
            
            //			if (mUpdateFiles.size() > 0) {
            //			askForSDUsageThenAskDownload(mTotalChartCount);
            //			} else {
            //			if (!WaitForInput)
            //			statusCheckComplete(true);
            //			}
            wrapUpChartCheckOrNetworkCheck(waitForInput: WaitForInput)
            
        } else if mChartCheckState == EChartCheckState.StGetFile {
            msgCodeInt = Msg
            
            print("Index out of bounds check, chartCheckHandleMessages: mUpdateIndex: \(mUpdateIndex) mUpdateFiles.count: \(mUpdateFiles.count)")
            
            // The following shouldn't happen, but it has
            if mUpdateIndex >= mUpdateFiles.count {
                print("Error StatusCheck:chartCheckHandleMessages: mUpdateIndex >= mUpdateFiles.count. mUpdateIndex: \(mUpdateIndex) mUpdateFiles.count: \(mUpdateFiles.count)")
            }
            
            var ufr :UpdateFileRecord = mUpdateFiles[mUpdateIndex]
            
            //			 received message about update progress
            if msgCodeInt == StChartCheckTask.CHECKRESULT_PROGRESS { // progress updateMapview
                //				int oldprog = mProgressChartDl.getProgress();
                //				Bundle dlData = Msg.getData();
                //				int progress = 0;
                //				String dlText = mResources.getString("statuscheck_updateprogress) + ":";
                //				if (dlData != null) {
                //					long total = dlData.getLong("total");
                //					progress = Math.round((float)(ufr.IncompleteSize + total) / (float)ufr.FileSize * 100);
                //					if (progress > oldprog)
                //					dlText += " " + StUtils.bytesToFileSize(ufr.IncompleteSize + total) + " / " + StUtils.bytesToFileSize(ufr.FileSize);
                //				} else {
                //					dlText += " " + StUtils.bytesToFileSize(ufr.FileSize);
                //				}
                //
                //				if (progress > oldprog) {
                //					mProgressChartDl.setProgress(progress);
                //					mTextViewChartDl.setText(dlText);
                //				}
                
                // moved directly to downloadservice
                //				if let downloadManagerController = mViewController as? DownloadManagerController {
                //
                //					let progressValues = Data.splitToStringArray(by: ";")
                //					//					let product = progressValues[0]
                //					//					let filename = progressValues[1]
                //					let soFar = Int(progressValues[0])!
                //					let total = Int(progressValues[1])!
                //					let requestParams = progressValues[2]
                //					//					availableChartsetsController.setProgress(product: product, filename: filename, soFar: soFar, total: total)
                //					downloadManagerController.setProgress(ufr: mUpdateFiles[mUpdateIndex], soFar: soFar, total: total, downloadRequestParameters: requestParams, fileName: ufr.FileName)
                //				}
            } else { // file complete or error
                var tmpFile :URL
                var path :URL = getDKW2Directory()
                if ufr.Pid == 899000 {
                    //                     for tides files and wwnetwork
                    if ufr.Mod == 1 {
//                        path = new StDocumentFile(new File(Environment.getExternalStorageDirectory() + "/stentec/tides/"));
                    } else {
//                        path = new StDocumentFile(new File(StActManager.getActManager().getWWFilePath(mContext, false)));
                        path = StUtils.getUrlByFilename("") //default path used for WWNetwork
                    }
                }
                
                tmpFile = path.appendingPathComponent(ufr.FileName! + ".tmp")
                
                if msgCodeInt == StChartCheckTask.CHECKRESULT_OK {
                    // file downloaded, check and rename
                    
                    var newFile :URL = path.appendingPathComponent(ufr.FileName!)
                    
                    if FileManager.default.fileExists(atPath: tmpFile.path) && getFileSize(tmpFile.path)! == ufr.FileSize! {
                        //					if !tmpFile.renameWithinSameDirectory(newFile) {
                        //						Log.e("popke", "Error! renameWithinSameDirectory() failed for: " + tmpFile.toString() + " to " + newFile.toString()+". Deleting tmpfile");
                        //						tmpFile.delete();
                        //					}
                        if FileManager.default.fileExists(atPath: newFile.path) {
                            do {
                                try FileManager.default.removeItem(at: newFile)
                            } catch {
                                print("Error removing existing file: \(newFile.path)")
                            }
                        }
                        
                        do {
                            try FileManager.default.moveItem(at: tmpFile, to: newFile)
                            
                            print("Temporary file renamed. \(newFile.path)")
                        } catch {
                            print("Renaming failed, deleting temporary file \(tmpFile.path) ")
                            do {
                                try FileManager.default.removeItem(at: tmpFile)
                            } catch let error as NSError {
                                print("Deleting failed: \(error)")
                            }
                        }
                    }
                    
                    // Reload network if we just downloaded it
                    if (ufr.Pid == 899000 && ufr.Mod == 2) {
                        let WWFilepath = StActManager.getActManager().getWWFilePath()
                        WWNetwork.reloadNetwork(aFilename: WWFilepath, aMainHandler: nil)
                    }
                    
                    
                    // update DownloadManager screen, but only if all files have finished
                    if let downloadManagerController = getTopViewController() as? DownloadManagerController,
                       mUpdateIndex == mUpdateFiles.count - 1
                    {
                        let ufr = mUpdateFiles[mUpdateIndex]
                        downloadManagerController.finishDownload(pid: ufr.Pid ?? 0, mod: ufr.Mod ?? 0)
                    }
                    
                } else {
                    //               if (tmpFile.exists())
                    //                     tmpFile.delete();
                }
                
                // next file
                mUpdateIndex += 1
                
                if !mAborted && mUpdateIndex < mUpdateFiles.count {
                    chartCheck()
                    
                } else {
                    
                    // update DownloadManager screen
                    if let downloadManagerController = getTopViewController() as? DownloadManagerController,
                       mUpdateIndex < mUpdateFiles.count {
                        let ufr = mUpdateFiles[mUpdateIndex]
                        downloadManagerController.finishDownload(pid: ufr.Pid ?? 0, mod: ufr.Mod ?? 0)
                    }
                    
                    if mAborted {
                        //					mTextViewCharts.setText(mResources.getString("statuscheck_aborted));
                        print("Downloading aborted")
                    } else {
                        //					mTextViewCharts.setText(mResources.getString("statuscheck_alluptodate))
                        print("Downloading up to date")
                    }
                    //				mTextViewChartDl.setText(mResources.getString("statuscheck_loadingcharts))
                    statusCheckComplete(true)
                }
            }
        }
    }
    
    func wrapUpChartCheckOrNetworkCheck(waitForInput :Bool) {
        if mUpdateFiles.count > 0 {
            
            //                    if mChartManMode { // <- this condition prevents updating on startup, so remove it eventually
            askForDownload(mTotalChartCount)
            //                    }
            
        } else {
            
            DKW2ChartManager.refreshCharts()
            
            // No downloads, so reset tablecell and update DownloadManager screen
            if mChartManMode,
               let downloadManagerController = getTopViewController() as? DownloadManagerController{
                downloadManagerController.finishDownload(pid: mPid, mod: mMod)
            }
            
            if !waitForInput {
                statusCheckComplete(true)
            }
        }
    }
    
    
    func getNetworkCheckAllowed() -> Bool {
        // check if waterway network checking and downloading is allowed and wanted
        
        // check global setting first
        if Resources.useWWNetwork == false {
            return false
        }
        
        var allowedWaterbodyMask :Int = 0
        if mActDB.getProductCodeCount() > 0 {
            
            var CP = StActManager.ChartProduct()
            for i in 0 ..< mActDB.getProductCodeCount() {
                mActDB.getActCode(aIndex: i, aCP: CP)
                allowedWaterbodyMask = allowedWaterbodyMask | WWDefs.chartToWaterbody(CP.Pid, CP.Mod)
            }
            
            //SharedPreferences Prefs = getSharedPreferences("Stentec.DKW1800.Settings", MODE_PRIVATE);
            //            boolean LoadNetwork = StSettingsConstants.settings().getLoadNetwork();// Prefs.getBoolean(StSettingsConstants.LOAD_WWNETWORK, true);
            //            boolean LoadNetwork = StSettingsConstants.settings().getLoadNetwork()// Prefs.getBoolean(StSettingsConstants.LOAD_WWNETWORK, true);
            
            if (allowedWaterbodyMask > 0 && ViewController.instance?.mLoadNetwork ?? false) {
                //                mNetworkCheckAllowed = true;
                return true;
            }
        }
        return false;
    }
    
    func startUpdate() {
        if mUpdateFiles.count > 0 {
            //			mProgressCharts.setVisibility(View.VISIBLE)
            //			mProgressChartDl.setVisibility(View.VISIBLE)
            mChartCheckState = EChartCheckState.StGetFile
            chartCheck()
        }
    }
    
    func statusCheckComplete(_ aSuccess :Bool) {
        //			StUtils.unlockRotation(this);
        if mChartManMode {
            //			Intent resultIntent = new Intent();
            //			resultIntent.putExtra("Success", aSuccess);
            //			resultIntent.putExtra("Data", mChartManModeData);
            //			setResult(RESULT_OK, resultIntent);
            
            //			mCallBackHandler!(ErrorCode.RESULT_OK, aSuccess, mCheckedItem!)
            // onReturningToMapViewFromChartManager in viewController called by segue
            
        } else {
            if (aSuccess) {
                //					Intent intent = new Intent(StatusCheckActivityAccountNew.this, MainActivity.class);
                //					startActivity(intent);
                
                // Instead of starting "MainActivity" just refresh chartview.
                
                DKW2ChartManager.refreshCharts()
                
            } else {
                
                // Statuscheck on startup failed
                // Depending on the errorcode, handle it.
                
            }
        }
        //			finish();
    }
    
    func askForDownload(_ aTotalChartCount :Int){
        //Builder alertBuilderDownload = new AlertDialog.Builder(this);
        //alertBuilderDownload.setTitle(mResources.getString("statuscheck_download_title));
        //
        var downloadOnly = true
        var totalBytes :Int64 = 0
        for ufr in mUpdateFiles {
            totalBytes += ufr.FileSize!
            //			if (ufr.FileState == EFileState.FILEINCOMPLETE) {
            //				totalBytes -= ufr.IncompleteSize!
            //			}
            if (ufr.FileState == EFileState.FILEUPDATE) {
                downloadOnly = false
            }
        }
        
        //String msg = "";
        //boolean SpaceLow = false;
        //long spaceAvail = StUtils.getAvailableSpaceDKW2Location(this);
        
        //		String tDl = StUtils.bytesToFileSize(TotalBytes);
        
        var tDl :String = ""
        if totalBytes > 0 { tDl = "(\(totalBytes / 1000000) MB)" }
        
        //if (TotalBytes >= spaceAvail) {
        //msg = String.format(mResources.getString("statuscheck_download_warning), tDl, StUtils.bytesToFileSize(spaceAvail));
        //SpaceLow = true;
        //} else {
        //// new installation
        //if (DownloadOnly) {
        //if (mUpdateFiles.size() == aTotalChartCount)
        //msg = String.format(mResources.getString("statuscheck_download_new), tDl);
        //else
        //msg = String.format(mResources.getString("statuscheck_download_missing), String.valueOf(mUpdateFiles.size()), tDl);
        //} else {
        //msg = String.format(mResources.getString("statuscheck_download_updates), String.valueOf(mUpdateFiles.size()), tDl);
        //}
        //}
        //
        //alertBuilderDownload.setMessage(msg);
        ////    alertBuilderDownload.setMessage(mResources.getString("statuscheck_download_msg));
        //if (SpaceLow) {
        //alertBuilderDownload.setPositiveButton(mResources.getString("button_ok), new DialogInterface.OnClickListener() {
        //
        //@Override
        //public void onClick(DialogInterface dialog, int which) {
        //statusCheckComplete(true);
        //}
        //});
        //} else {
        //alertBuilderDownload.setPositiveButton(mResources.getString("button_yes), new DialogInterface.OnClickListener() {
        //
        //@Override
        //public void onClick(DialogInterface dialog, int which) {
        //		startUpdate();
        ////                    StDKW2Settings settings = StDKW2Settings.getSettings(StatusCheckActivityAccountNew.this);
        ////                    if (settings.sdCardAvailable()) {
        ////                        //First ask for sd card permission, then specifiy what to do next
        ////                        settings.checkAskedForSDCard(
        ////                                new Runnable() {
        ////                                    @Override
        ////                                    public void run() {
        ////startUpdate()
        //	}
        ////                                });
        ////                    } else {
        ////                        startUpdate();
        ////                    }
        //}
        //});
        //alertBuilderDownload.setNegativeButton(mResources.getString("button_no), new DialogInterface.OnClickListener() {
        //
        //@Override
        //public void onClick(DialogInterface dialog, int which) {
        //statusCheckComplete(true);
        //}
        //});
        //}
        //alertBuilderDownload.setOnCancelListener(new DialogInterface.OnCancelListener() {
        //
        //@Override
        //public void onCancel(DialogInterface dialog) {
        //dialog.dismiss();
        //statusCheckComplete(true);
        //}
        //});
        //if (!isFinishing())
        //alertBuilderDownload.show();
        
        if !mChartManMode {
            
            let alert = UIAlertController(title: "statuscheck_download_updates_title".localized+" "+tDl, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel))
            alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                self.startUpdate()
            }))
            
            dout("Ran topviewcontroller, now presenting alert:")
            
            getTopViewController()?.presentFromMain(alert, animated: true)
            
            
        } else {
            startUpdate()
        }
    }
    
    func showActError(aErrorCode :Int) {
        //	Builder alertBuilderActError = new AlertDialog.Builder(this);
        //	Resources res = getResources();
        //	alertBuilderActError.setTitle(res.getString("statuscheck_acterror_title));
        
        let errText = StatusCheck.serverErrorMessage(cmd: "", code: aErrorCode)
        
        print("showActError: \(errText)")
        
        //	alertBuilderActError.setMessage(errText);
        //        let alert = UIAlertController(title: "error_dialog_title".localized, message: errText, preferredStyle: .alert)
        let alert = UIAlertController(title: errText, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { action in
            if self.mErrorCode == 4 {
                
                self.mActDB.resetUserData()
                DKW2ChartManager.refreshCharts()
                //		Intent intent = new Intent(mContext, UserAccountActivity.class);
                //		intent.putExtra(UserAccountActivity.UACANCELABLE, true);
                //		startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
                StatusCheck.showLoginDialog(continueWith: {(result: DialogResult, email: String, pw: String) -> () in
                                                self.handleLoginData(aResult: result, aUser: email, aPass: pw)})
                
            } else {
                var s :Bool = self.mChartCheckState != EChartCheckState.StCheckCharts
                self.statusCheckComplete(s)
            }
        }))
        //	}
        //	});
        //	if (!isFinishing())
        //	alertBuilderActError.show();
        getTopViewController()?.presentFromMain(alert, animated: true)
    }
    
    func showExpired() { // show a message telling the user that the trial version has expired
        //		Builder alertBuilderExpired = new AlertDialog.Builder(this);
        //		Resources res = getResources();
        //		alertBuilderExpired.setTitle(res.getString("statuscheck_acterror_applicexpiredtitle));
        //		String errText = res.getString("statuscheck_acterror_applicexpired);
        //		alertBuilderExpired.setMessage(errText);
        //		alertBuilderExpired.setPositiveButton("Lite", new DialogInterface.OnClickListener() {
        //
        //			@Override
        //			public void onClick(DialogInterface dialog, int which) {
        //				statusCheckComplete(true);
        //			}
        //
        //		});
        //		alertBuilderExpired.setNegativeButton(res.getString("statuscheck_purchase), new DialogInterface.OnClickListener() {
        //			@Override
        //			public void onClick(DialogInterface dialog, int which) {
        //				IabClick();
        //			}
        //		});
        //		if (!isFinishing())
        //		alertBuilderExpired.show();
    }
    
    //	private void IabClick() {
    //	Intent intent = new Intent(this, IabActivity.class);
    //	startActivityForResult(intent, AppConstants.ACTIVITY_SHOPACTIVITY);
    //	}
    //
    //	private boolean checkActAndTimed() {
    //	//    if
    //	return true;
    //	}
    //
    //	private class ChartCheckHandlerCallback implements Handler.Callback {
    //	@Override
    //	public boolean handleMessage(Message Msg) {
    //	chartCheckHandleMessages(Msg);
    //	return true;
    //	}
    //	}
    
    //	let chartCheckHandlerCallback = { (_ SCObject :StatusCheck, _ Msg :ErrorCode, _ Data :String) -> Bool in
    //		SCObject.chartCheckHandleMessages(Msg: Msg, Data: Data)
    //		return true
    //	}
    
    class UpdateFileRecord {
        //		var ActCode :String?
        var Pid :Int?
        var Mod :Int?
        var FileName :String?
        var FileSize :Int64?
        var FileState :EFileState?
        var IncompleteSize :Int64?
        var DownloadManagerId :Int64?
    }
    
    //
    //    @Override
    //    public void setDoAfterQuestionRunnable(Runnable r) {
    //        mContinueAfterSDCardPermission = r;
    //    }
    
    static func showLoginDialog(continueWith: @escaping (DialogResult, String, String) -> Void){
        
        let alert = UIAlertController(title: "Login", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { action in
            continueWith(.ResultCancelled, "", "")
        }))
        
        alert.addTextField(configurationHandler: { nameField in
            nameField.placeholder = "E-mail adress"
        })
        
        alert.addTextField(configurationHandler: { passwordField in
            passwordField.placeholder = "Stentec password"
            passwordField.isSecureTextEntry = true
        })
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            guard var email = alert.textFields?[0].text else { return }
            
            print("e-name: \(email)")
            
            guard var pw = alert.textFields?[1].text else { return }
            
            print("password: \(pw)")
            
            // bring up available chart sets
            //			self.handleLoginData(aUser: email, aPass: pw)
            
            if StUtils.isDebug() {
                if email.count == 0 && pw.count == 0 {
                    email = "stentecgalaxytabs@gmail.com"
                    pw = "st4soft"
                    //				email = "paltenburg@gmail.com"
                    //				pw = "1oposse1"
                }
            }
            
            //			self.handleLoginData(aUser: email, aPass: pw)
            continueWith(.ResultOk, email, pw)
        }))
        
        getTopViewController()?.presentFromMain(alert, animated: true)
    }
    
    static func showLoginController(handler: (() -> Void)?){
        showLoginController(handler: handler, withoutEscapeButtons: false)
    }
    
    static func showLoginController(handler: (() -> Void)?, withoutEscapeButtons :Bool){
        
        //		let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        //		let accountSettingsController = storyBoard.instantiateViewController(withIdentifier: "AccountSettingsSceneID") as! AccountSettingsController
        //		callingViewController.present(accountSettingsController, animated: true, completion: handler)
        
        if let vc = (getTopViewController() as? ViewController) {
            vc.accountSettingsWindowIsLocked = withoutEscapeButtons
            vc.performSegue(withIdentifier: "accountSegue", sender: nil)
            

        }
        
        
    }
    
    //	func doAppleDeviceCheck() {
    //		if #available(iOS 11.0, *) {
    //			let device = DCDevice.current
    //
    //			// check if the device supports the DeviceCheck API
    //			guard device.isSupported else {
    //				let alert = UIAlertController(title: "Unsupported device", message: "Please try in a real device instead of simulator", preferredStyle: .alert)
    //				let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    //				alert.addAction(okAction)
    //
    //				self.mViewController.presentFromMain(alert, animated: true, completion: nil)
    //				return
    //			}
    //
    //			// generate token for DeviceCheck
    //			device.generateToken(completionHandler: { data, error in
    //				guard error == nil,
    //					let data = data else {
    //						let alert = UIAlertController(title: "Unable to generate token", message: "Please sign the app using a valid Apple Developer Account", preferredStyle: .alert)
    //						let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    //						alert.addAction(okAction)
    //
    //						self.mViewController.presentFromMain(alert, animated: true, completion: nil)
    //						return
    //				}
    //
    //				// send the base64 encoded device token string to your server
    //				let token = data.base64EncodedString()
    //
    ////				print("token for deviceCheck: \(token)")
    //				// use URLSession or something to send the token string to your server API
    //			})
    //		} else {
    //			// Fallback on earlier versions
    //		}
    //	}
}
