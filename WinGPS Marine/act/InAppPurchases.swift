//
//  InAppPurchases.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 21/04/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import StoreKit

class InAppPurchases :NSObject{
    
    enum InAppPurchasesError: Error {
        case noProductIDsFound
        case noProductsFound
        case paymentWasCancelled
        case productRequestFailed
    }
    
    static let service  = InAppPurchases()
    
    var request :SKProductsRequest!
    var products :[SKProduct]?
    
    private override init(){
        super.init()
        SKPaymentQueue.default().add(self)
    }
    

//    func getProducts(withHandler productsReceiveHandler: @escaping (_ result: Result<[SKProduct], InAppPurchasesError>) -> Void) {
      func getProducts() {
    
    }
    
    public func isPurchased(productID :String) -> Bool{
        return false
    }
    
    public func buyProduct(_ product: SKProduct) {
      print("Buying \(product.productIdentifier)...")
      let payment = SKPayment(product: product)
      SKPaymentQueue.default().add(payment)
    }
    
    func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    public func checkProducts() {

        let request = SKProductsRequest(productIdentifiers: [Resources.iapTestingConsumableID])
        request.delegate = self
        request.start()
    }
}

// MARK: - SKPaymentTransactionObserver
 
extension InAppPurchases: SKPaymentTransactionObserver {
 

    
  public func paymentQueue(_ queue: SKPaymentQueue,
                           updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState {
      case .purchased:
        dout("paymentQueue: purchased")
//        complete(transaction: transaction)
        break
      case .failed:
        dout("paymentQueue: failed")
//        fail(transaction: transaction)
        break
      case .restored:
        dout("paymentQueue: restored")
//        restore(transaction: transaction)
        break
      case .deferred:
        break
      case .purchasing:
        break
      }
    }
  }
 
//  private func complete(transaction: SKPaymentTransaction) {
//    print("complete...")
//    deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
//    SKPaymentQueue.default().finishTransaction(transaction)
//  }
//
//  private func restore(transaction: SKPaymentTransaction) {
//    guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
//
//    print("restore... \(productIdentifier)")
//    deliverPurchaseNotificationFor(identifier: productIdentifier)
//    SKPaymentQueue.default().finishTransaction(transaction)
//  }
//
//  private func fail(transaction: SKPaymentTransaction) {
//    print("fail...")
//    if let transactionError = transaction.error as NSError?,
//      let localizedDescription = transaction.error?.localizedDescription,
//        transactionError.code != SKError.paymentCancelled.rawValue {
//        print("Transaction Error: \(localizedDescription)")
//      }
//
//    SKPaymentQueue.default().finishTransaction(transaction)
//  }
//
//  private func deliverPurchaseNotificationFor(identifier: String?) {
//    guard let identifier = identifier else { return }
//
//    purchasedProductIdentifiers.insert(identifier)
//    UserDefaults.standard.set(true, forKey: identifier)
//    NotificationCenter.default.post(name: .IAPHelperPurchaseNotification, object: identifier)
//  }
}

extension InAppPurchases :SKProductsRequestDelegate{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        // Get the available products contained in the response.
        let products = response.products
        self.products = products
        
        // Check if there are any products available.
        dout("product count: \(products.count)")

        for invalidId in response.invalidProductIdentifiers{
             dout("invalid product id's: \(invalidId)")
         }
        
        if products.count > 0 {
            for product in products{
                dout("product: \(product.productIdentifier)")
                
//                // buy it:
//
//                for product in (InAppPurchases.service.products)! {
//                    InAppPurchases.service.buyProduct(product)
//                }
            }
            // Call the following handler passing the received products.
//            onReceiveProductsHandler?(.success(products))
        } else {
            // No products were found.
//            onReceiveProductsHandler?(.failure(.noProductsFound))
        }
    }

    func getPriceFormatted(for product: SKProduct) -> String? {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = product.priceLocale
        return formatter.string(from: product.price)
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        
    }
}
