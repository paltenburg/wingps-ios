//
//  StChartCheckTask.swift
//  WinGPS Marine
//
//  Created by Standaard on 22/08/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit


class StChartCheckTask : NSObject, URLSessionDelegate {
	
    static let testing :Bool = false
    
	static let APPENDING_DISABLED :Bool = true // implement later
	
	//	static func getCodedProductString(pid :Int64?, mod :Int?, isSub :Bool?) -> String{
	//		var CPI = String(pid ?? 0) + "_" + String(mod ?? 0)
	//		if isSub ?? false {
	//			CPI += "_1"
	//		}
	//		return CPI
	//	}
	
	static let CHECKRESULT_DATA :String = "CheckData"
	
	static let CHECKRESULT_INTERNALERROR :Int = -1
	static let CHECKRESULT_OK :Int = 0
//	static let CHECKRESULT_TIMEOUT :Int = 97
//	static let CHECKRESULT_CANCELED :Int = 98
	static let CHECKRESULT_PROGRESS :Int = 99
	static let CHECKRESULT_NOTCONNECTEDTOINTERNET :Int = -2
	static let CHECKRESULT_DEVICETOKEN_ERROR :Int = -3
	//
	//	// private static final String COMMAND_FILELIST_LICENSE = "fll";
	static let COMMAND_BEST_ACT = "bac2"
	//	private static final String COMMAND_ACT_APP = "caa";
	//	static let COMMAND_CHARTCHECK_ACCOUNT = "cca"
	static let COMMAND_CHECK_ACCOUNT_GET_CHART_FILES = "afa"
	static let COMMAND_DEACTIVATE_ACTIVE_ACTIVATION = "daa"
	static let COMMAND_CHARTACT_ACCOUNT = "asa"
    static let COMMAND_CHARTFREE_ACCOUNT = "gaf"
	static let COMMAND_FILE = "gfd"
    static let COMMAND_CHECK_ACCOUNT_AND_RECEIPT = "glr"
    
	//	private static final String COMMAND_FILELIST_ACCOUNT = "fla";
	//	private static final String COMMAND_FILELIST_APP = "fap";
	//	private static final String COMMAND_CONNECT_ACCOUNT = "csa";
	//	static let COMMAND_GETSETS_ACCOUNT = "gsa2"
	static let COMMAND_GETSETS_ACCOUNT = "gsa"
	//	private static final String COMMAND_ACT_LICENSE = "ap4";
	static let COMMAND_REGISTER_ACCOUNT = "re3"
	//	private static final String COMMAND_TIDEFILES = "gtf";
	static let COMMAND_FILEINFO = "gwd"
	static let COMMAND_FILEWW = "gwi"
    
	//
	//	//private static final String PEMFILE = "stentec.pem";
	//
	//	// private static final int CONNECTTIMEOUT = 6000;
	//	// private static final int READTIMEOUT = 15000;
	//	// make them a little longer. It's worse to get an error message, than to wait a little more.
	//	private static final int CONNECTTIMEOUT = 12000;
	//	private static final int READTIMEOUT = 30000;
	
	let mAppID :Int
	let PID :Int = 0
	let MODULE :Int = 0
	
	//	var mClient :HttpsURLConnection? = nil
	var mRequest :URLRequest? = nil
	var mCommand :String = ""
	//	private Context mContext;
	var mHandler :((String, Int, String) -> Void)?
	var mStatusCheck :StatusCheck? = nil
	//	var mOutputFile :FileHandle? = nil
	var mOutputFile :URL? = nil
	//	// private File mPemFile;
	var mPostData :String? = nil
	//	//private SSLSocketFactory mSocketFactory;
	//	// private String mServerUrl = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzMnRlc3QucGhw"; // https://www.stentec.com/stlib/getcharts2test.php

	let mServerUrlAndroid :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzMi5waHA="; // https://www.stentec.com/stlib/getcharts2.php

	let mServerUrlActAndroid :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvYWN0aXZhdGlvbi5waHA="; // https://www.stentec.com/stlib/activation.php

	// Testing url, to test ios specific file downloading script
	//	let mServerTestingUrl :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzMnRlc3QucGhw"; // https://www.stentec.com/stlib/getcharts2test.php
	//	let mServerTestingUrl :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzMi5waHA=";
	
	// url to script for Apple devices
	let mServerUrlIos :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzYS5waHA="; // https://www.stentec.com/stlib/getchartsa.php
	
    let mServerUrlIosReceiptCheck = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvZ2V0Y2hhcnRzYWNoZWNrLnBocA==" // getchartsatest.phpgetchartsacheck.php

    let mServerUrlIosTest :String = "aHR0cHM6Ly93d3cuc3RlbnRlYy5jb20vc3RsaWIvdGVzdC9nZXRjaGFydHNhLnBocA==" // https://www.stentec.com/stlib/test/getchartsa.php
        
	var mTask :CheckTask? = nil
	var mUrl :URL?
	var mUrlAndroid :URL?
	
	//	private Uri mUri;
	var mChecksum :String
	var mCodedProductString :String?
	
	//	private long mCurrentDlID = 0;
	var mSubsOnly = false
	//	//private DownloadManager mDownloadManager;
	
	convenience init(aHandler: ((String, Int, String) -> Void)?) {
		self.init(aHandler: aHandler, aUseAct: false)
	}
	
	//	public StChartCheckTask(Context aContext, Handler aUIHandler) throws IOException, Base64DecoderException {
	//	init(handler aUIHandler: @escaping (_ Msg :Int, _ Data :String) -> Bool) {
	//	init(_ aStatusCheck :StatusCheck) {
	init(aHandler: ((String, Int, String) -> Void)?, aUseAct :Bool) {
		//
		mHandler = aHandler
		//		mStatusCheck = aStatusCheck
		//	mContext = aContext;
		//	//mSocketFactory = null;
		
		mAppID = R.integer.appid
		//	PID = aContext.getResources().getInteger(R.integer.pid);
		//	MODULE = aContext.getResources().getInteger(R.integer.module);
		//
		//	if(mAppID == aContext.getResources().getInteger(R.integer.DKW_NEDERLAND_BINNEN))
		//	mSubsOnly = true;
		
		//		String UrlStr = new String(Base64.decode(mServerUrl));
		//		mUrl = new URL(UrlStr);
		//		mUri = Uri.parse(UrlStr);
		//		//mPemFile = new File(aContext.getFilesDir(), PEMFILE);

        if !StChartCheckTask.testing {
            if let data = aUseAct ? Data(base64Encoded: mServerUrlActAndroid) : Data(base64Encoded: mServerUrlIos) {
                if let urlStr = String(data: data, encoding: .utf8) {
                    print("UrlStr Activation Android: ", urlStr)
                    
                    mUrl = URL(string: urlStr)
                    
                    //                    let testurl = "https://www.stentec.com/stlib/getchartsatest.php"
                    //                    mUrl = URL(string: testurl)
                    
                    print("Url used for http call: ", urlStr)
                    
                }
            }
        } else {
            
            if let data = Data(base64Encoded: mServerUrlIosTest) {
                if let UrlStr = String(data: data, encoding: .utf8) {
                    mUrl = URL(string: UrlStr)!
                }
            }
            
////            mUrl = URL(string: mServerUrlIosTestUncoded) // returns nil, so use:
//            mUrl = Foundation.URL(string: mServerUrlIosTestUncoded.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
//
//            mUrl = Foundation.URL(string: mServerUrlIosTestUncoded)

        }
        
		//	String fileName = mContext.getPackageCodePath();
		//	mChecksum = String.valueOf(StUtils.getAppChecksum(fileName));
		mChecksum = ""
	}
	
	//	override init(){
	//
	//		mAppID = 0
	//
	//		if let data = Data(base64Encoded: mServerUrlAct) {
	//			let UrlStr = String(data: data, encoding: .utf8)
	//			if let UrlStr = UrlStr {
	//				print("UrlStr: ", UrlStr)
	//				mUrl = URL(string: UrlStr)
	//			}
	//		}
	//		mChecksum = ""
	//	}

	//
	//	public void cancel() {
	//	if (mTask != null)
	//	mTask.cancel(true);
	//	}
	//	/*
	//	public int checkPemFile()  {
	//	URL url;
	//	HttpURLConnection client;
	//	try {
	//	url = new URL("http://213.206.86.120/pem/stentec.pem");
	//	} catch (MalformedURLException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	try {
	//	client = (HttpURLConnection)url.openConnection();
	//	} catch (IOException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//
	//	client.setUseCaches(false);
	//	try {
	//	new CheckPemTask(client).execute();
	//	} catch (Exception e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//
	//	return CHECKRESULT_OK;
	//	}
	//	*/
    
    func checkWWNetwork() -> ErrorCode {
        var request = getRequest()
        if request == nil {
            return .CHECKRESULT_INTERNALERROR
        }
        
        setCommand(aCommand: StChartCheckTask.COMMAND_FILEINFO)
        
        let pName :String = Resources.packageName
        let pVersion :String = String(Resources.appVersionString)
        
        addVar("ft", "1")
        addVar("pn", pName)
        addVar("pv", pVersion)
        addVar("cf", "1")
        
        sendCommand(false)
        
        return ErrorCode.CHECKRESULT_OK
    }
	
	//	public long getCurrentDlID() {
	//	return mCurrentDlID;
	//	}
	//
	//	public String getCommand() {
	//	return mCommand;
	//	}
	//
	//	public int requestActLicense(StActManager aActDB, String aLicense) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_ACT_LICENSE);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//
	//	addVar("em", aActDB.getUserMail());
	//	addVar("pw", aActDB.getUserPass());
	//	addVar("hk", HWKey.getDeviceKey());
	//	addVar("lc", aLicense);
	//
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	
	//	// Used with getCharts2.php
	//	func requestAvailableChartSets(aActDB :StActManager, aUser :String, aPass :String) -> ErrorCode {
	//		print("requestAvailableChartSets")
	//
	//		//		try {
	//		//		mClient = getConnection();
	//
	//		mRequest = getRequest()
	//		if mRequest == nil {
	//			return .CHECKRESULT_INTERNALERROR
	//		}
	//
	//		//		} catch (IOException e1) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		} catch (GeneralSecurityException e) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		}
	//		//		if (mClient == null)
	//		//		return CHECKRESULT_INTERNALERROR;
	//
	//		setCommand(aCommand: StChartCheckTask.COMMAND_GETSETS_ACCOUNT)
	//
	//		//		var nPname = mContext.getPackageName();
	//		var pName = Resources.packageName
	//		var pVersion :String = ""
	//		//		try {
	//		//		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//		//		} catch (NameNotFoundException e) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		}
	//		pVersion = String(Resources.appVersionNumber)
	//
	//
	//		//    String EMail;
	//		//     String Pass;
	//
	//		//     if (aFirstPass) {
	//		//       EMail = aEmail;
	//		//       Pass = aPass;
	//		//    } else {
	//		//        EMail = aActDB.getUserMail();
	//		//        Pass = aActDB.getUserPass();
	//		//    }
	//		if mSubsOnly {
	//			addVar("so", "1")
	//		}
	//		addVar("em", aUser)
	//		addVar("pw", aPass)
	//		addVar("fpw", "1")
	//		addVar("an", pName)
	//		addVar("av", pVersion)
	//		addVar("acs", mChecksum)
	//		addVar("gid", aActDB.getUserID()) // Google ID
	//		//addVar("ic", "1");
	//
	//		//		let bodyData = mRequest!.httpBody!
	//
	//		//		let bodyDataString = String(data: bodyData, encoding: String.Encoding.utf8)!
	//
	//		//		print("httpBody:" , bodyDataString)
	//
	//		sendCommand(false);
	//
	//		return ErrorCode.CHECKRESULT_OK
	//	}
	
	
	func requestAvailableChartSets(aActDB :StActManager, aUser :String, aPass :String) -> ErrorCode {
		print("requestAvailableChartSets")

		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}

		setCommand(aCommand: StChartCheckTask.COMMAND_GETSETS_ACCOUNT) // gsa

		var pName = Resources.packageName
//        var pVersion = String(Resources.appVersionNumber)
        var pVersion = Resources.appVersionString

		//		if mSubsOnly {
		//			addVar("so", "1")
		//		}
		addVar("em", aUser)
		addVar("pw", aPass)
		addVar("av", pVersion) // optional, at the moment
		addVar("an", pName) // optional, at the moment
		addVar("acs", mChecksum) // optional, at the moment
		addVar("ic", "1") // ic > 0 returns sets broken up by their parts, 0 = returns complete sets (1800 complete only)
		//		addVar("fpw", "1")
		//		addVar("gid", aActDB.getUserID()) // Google ID
		addVar("json", "1")

		sendCommand(false)

		return ErrorCode.CHECKRESULT_OK
	}
	
	
	
	//	// Used with getCharts2.php
	//	/**
	//	* Request best activation (used for Lite, Marine and Plus to get best possible activation)
	//	*
	//	* @param aUid The Google ID of the user
	//	* @param aActDB Pointer to activation database
	//	*/
	//	//	func requestBestActivation(aUid :String, aActDB :StActManager) -> Int {
	////	func requestBestActivation(aUid :String, aActDB :StActManager) -> ErrorCode {
	//	func requestBestActivation(aActDB :StActManager) -> ErrorCode {
	//		print("requestBestActivation")
	//
	//		//		try {
	//		//		mClient = getConnection();
	//
	//		mRequest = getRequest()
	//		if mRequest == nil {
	//			return .CHECKRESULT_INTERNALERROR
	//		}
	//		//		} catch (IOException e1) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		} catch (GeneralSecurityException e) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		}
	//		//		if (mClient == null)
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//
	//
	//		setCommand(aCommand: StChartCheckTask.COMMAND_BEST_ACT)
	//
	//		var EMail :String = aActDB.getUserMail()
	//		var Pass :String = aActDB.getUserPass()
	//
	//		var HWKey = StHardwareKey()
	//		var pHWKey = HWKey.getDeviceKey()
	//
	////		var pName = mContext.getPackageName()
	//		var pName = Resources.packageName
	//
	//		var pVersion :String = ""
	////		try {
	////		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//		pVersion = String(Resources.appVersionNumber)
	//		//		} catch (NameNotFoundException e) {
	////		return CHECKRESULT_INTERNALERROR;
	////		}
	//
	//		var pAct = aActDB.checkProductAct(aPackageName: pName, aHWKey: pHWKey)
	//
	//		//act_pid
	//		var pActCode :String = ""
	//		var pAppID :String = String(mAppID)
	//
	//		if !pAct.isEmpty {
	//			var values :[String] = pAct.splitToStringArray(by: "_")
	//			if values.count == 2 {
	//				pActCode = values[0]
	//				pAppID = values[1]
	//			}
	//		}
	//
	//		addVar("em", EMail)
	//		addVar("pw", Pass)
	////		addVar("uid", aUid)
	//		addVar("uid", "") // used in android for the google-user-id, for playstore purchases
	//		addVar("an", pName)
	//		addVar("av", pVersion)
	//		addVar("acs", mChecksum)
	//		addVar("hw", pHWKey)
	//		addVar("pid", pAppID)
	//		if (pActCode.count > 0) {
	//			addVar("act", pActCode)
	//		}
	//
	//		//		let userEmail = "stentecgalaxytabs@gmail.com"
	//		//		let userPassword = "st4soft"
	//
	//		//		let bodyData = "email=\(userEmail)&password=\(userPassword)"
	////		let bodyData = "c=bac2&em=paltenburg%40gmail.com&pw=1oposse1&uid=ANlOHQP%2BiVxIL4j1jlOOfi%2BFudVAPVfTmg%3D%3D&an=com.stentec.wingps_marine_lite&av=130&acs=4229718151&hw=LPNP-23GT&pid=899922"
	////		let bodyData = "c=bac2&em=paltenburg%40gmail.com&pw=1oposse1&uid=&an=com.stentec.wingps_marine_lite&av=130&acs=&hw=LPNP-23GT&pid=899922"
	////		                c=bac2&em=paltenburg%40gmail.com&pw=1oposse1&uid=&an=com.stentec.wingps_marine_lite&av=11.4.1&acs=&hw=ZCQ7-TA3H&pid=899922
	//
	//		//request file:
	////		let bodyData = "c=gfd&em=paltenburg%40gmail.com&pw=1oposse1&an=com.stentec.wingps_marine_lite&av=130&acs=&hw=ZCQ7-TA3H&pid=196664&mod=1&act=4AGK-TDZB&fn=charts.collection&fs=0"
	//
	//		//		mRequest!.httpBody = bodyData.data(using: String.Encoding.utf8)
	//		//		request.httpBody = bodyData.data(using: String.Encoding.utf8)
	////		mPostData = bodyData
	//
	//		//		print("request.httpBody:" , String(data: request.httpBody!, encoding: String.Encoding.utf8))
	////		print("mRequest.httpBody:", mPostData)
	//
	//		sendCommand(false)
	//
	//		return .CHECKRESULT_OK
	//	}
	
	//	/**
	//	* Request chart check (used by chart apps, to check the chart activation)
	//	*
	//	* @param aUid The Google ID of the user
	//	* @param aActCode Activation code of the chart to check
	//	*/
	//	public int requestChartCheck(String aUid, String aActCode) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_ACT_APP);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//
	//	addVar("uid", aUid);
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("hw", pHWKey);
	//	addVar("pid", String.valueOf(PID));
	//	addVar("mod", String.valueOf(MODULE));
	//	if (aActCode.length() > 0)
	//	addVar("act", aActCode);
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	
	// Used with getCharts2.php
	/**
	* Request to activate a chart set.
	*
	* @param aEMail Email address of the user for which to activate the set.
	* @param aPass Password of the user for which to activate the set.
	* @param aChartList A string containing the pid's and mod's (and subtype for subscriptions) of the charts separated by a semicolon. The pid's, mod's and subtypes are separated by an underscore.
	* @return An errorcode indicating the result of this function.
	*/
//	func _requestChartActAccount(aEMail :String, aPass :String, aChartList :String) -> ErrorCode {
//		print("requestChartActAccount")
//
//		//		try {
//		//		mClient = getConnection();
//
//		mRequest = getRequest()
//		if mRequest == nil {
//			return .CHECKRESULT_INTERNALERROR
//		}
//
//		//		} catch (IOException e1) {
//		//		return CHECKRESULT_INTERNALERROR;
//		//		} catch (GeneralSecurityException e) {
//		//		return CHECKRESULT_INTERNALERROR;
//		//		}
//		//		if (mClient == null)
//		//		return CHECKRESULT_INTERNALERROR;
//
//		setCommand(aCommand: StChartCheckTask.COMMAND_CHARTACT_ACCOUNT)
//
//		var HWKey = StHardwareKey()
//		var pHWKey = HWKey.getDeviceKey()
//
//		//		String pName = mContext.getPackageName();
//		var pName = Resources.packageName
//
//		var pVersion :String = ""
//		//		try {
//		//		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
//		pVersion = String(Resources.appVersionNumber)
//
//		//		} catch (NameNotFoundException e) {
//		//		return CHECKRESULT_INTERNALERROR;
//		//		}
//
//		//		String list[] = aChartList.split(";");
//		//		var list :[String] = aChartList.splitToStringArray(by: ";", omittingEmptySubsequences: false).map{ String($0) } ?? [String]()
//		var list :[String] = aChartList.splitToStringArray(by: ";")
//
//		addVar("em", aEMail)
//		addVar("pw", aPass)
//		addVar("fpw", "1")
//		addVar("an", pName)
//		addVar("av", pVersion)
//		addVar("acs", mChecksum)
//		addVar("hw", pHWKey ?? "")
//		addVar("ed", "2")
//		addVar("prc", String(list.count))
//
//		for i in 0 ..< list.count {
//			var pr = list[i].split{$0 == "_"}.map(String.init)
//			addVar("pid" + String(i), String(pr[0]))
//			addVar("mod" + String(i), String(pr[1]))
//			if (pr.count == 3) {
//				addVar("sty" + String(i), String(pr[2]))
//			}
//			addVar("act" + String(i), "")
//		}
//
//		sendCommand(false);
//
//		return .CHECKRESULT_OK
//	}
	
	
	func requestFilesForChartSet(aEMail :String, aPass :String, aPid :Int, aMod :Int, aErrorHandler :@escaping (ErrorCode)->()) {
		if !getDeviceToken(handler: {(token : String) in
			aErrorHandler(
				self.requestFilesForChartSet(aEMail: aEMail, aPass: aPass, aPid: aPid, aMod: aMod, aDeviceToken: token)
			)
		}) {
			print("requestCheckAccountGetChartsetFiles Error with deviceToken")
			aErrorHandler(.CHECKRESULT_DEVICETOKEN_ERROR)
		}
	}
	
	/**
	* Request to get the filelist for a chart set.
	*/
	private func requestFilesForChartSet(aEMail :String, aPass :String, aPid :Int, aMod :Int, aDeviceToken : String) -> ErrorCode {
		
		if aDeviceToken == "" {
			return .CHECKRESULT_DEVICETOKEN_ERROR
		}
		
		//		try {
		//		mClient = getConnection();
		//		} catch (IOException e1) {
		//		return CHECKRESULT_INTERNALERROR;
		//		} catch (GeneralSecurityException e) {
		//		return CHECKRESULT_INTERNALERROR;
		//		}
		//		if (mClient == null)
		//		return CHECKRESULT_INTERNALERROR;
		
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: StChartCheckTask.COMMAND_CHECK_ACCOUNT_GET_CHART_FILES) // afa
		
		let HWKey :StHardwareKey = StHardwareKey()
		let pHWKey :String = HWKey.getDeviceKey()
		
        dout("UIDevice.current.identifierForVendor!.uuidString : \(UIDevice.current.identifierForVendor!.uuidString)")
        
		let pName :String = Resources.packageName
        addVar("an", pName)

//		var pVersion :String = ""
//		pVersion = String(Resources.appVersionNumber)
        var pVersion = Resources.appVersionString

		
		addVar("em", aEMail)
		addVar("pw", aPass)
		addVar("hw", pHWKey)
        if aDeviceToken == Resources.PASS_DEVICE_CHECK {
            addVar("ht", "967")
            addVar("dt", "")
        } else { // default:
            addVar("ht", isIpad() ? "1" : "0")
            addVar("dt", aDeviceToken)
        }
		addVar("json", "1")
		addVar("prc", String(1))
		
		if Resources.alwaysUseDeveloperDeviceCheck {
			addVar("isdev", "1")
		} else if Resources.alwaysUseProductionDeviceCheck {
			addVar("isdev", "0")
		} else {
			addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
		}
		
		addVar("pid0", String(aPid))
		addVar("mod0", String(aMod))
		
		sendCommand(false)
		
		return .CHECKRESULT_OK
	}
	
    func requestFreeChartActAccount(aEMail :String, aPass :String, aPid :Int, aMod :Int, aErrorHandler :@escaping (ErrorCode)->()) {
        if !getDeviceToken(handler: {(token : String) in
            aErrorHandler(
                self.requestFreeChartActAccount(aEMail: aEMail, aPass: aPass, aPid: aPid, aMod: aMod, aDeviceToken: token)
            )
        }) {
            print("requestFreeChartActAccount Error with deviceToken")
            aErrorHandler(.CHECKRESULT_DEVICETOKEN_ERROR)
        }
    }
    
    private func requestFreeChartActAccount(aEMail :String, aPass :String, aPid :Int, aMod :Int, aDeviceToken : String) -> ErrorCode {
        
        //		try {
        //		mClient = getConnection();
        //		} catch (IOException e1) {
        //		return CHECKRESULT_INTERNALERROR;
        //		} catch (GeneralSecurityException e) {
        //		return CHECKRESULT_INTERNALERROR;
        //		}
        //		if (mClient == null)
        //		return CHECKRESULT_INTERNALERROR;
        
        mRequest = getRequest()
        if mRequest == nil {
            return .CHECKRESULT_INTERNALERROR
        }
        
        setCommand(aCommand: StChartCheckTask.COMMAND_CHARTFREE_ACCOUNT)
        
        let HWKey :StHardwareKey = StHardwareKey()
        let pHWKey :String = HWKey.getDeviceKey()
        let pName :String = Resources.packageName
        var pVersion = Resources.appVersionString
        
        addVar("em", aEMail)
        addVar("pw", aPass)
//        addVar("fpw", "1")
        addVar("hw", pHWKey)
        
        if aDeviceToken == Resources.PASS_DEVICE_CHECK {
            addVar("ht", "967")
            addVar("dt", "")
        } else { // default:
            addVar("ht", isIpad() ? "1" : "0")
            addVar("dt", aDeviceToken)
        }
 
        addVar("pid", String(aPid))
        addVar("mod", String(aMod))

        addVar("an", pName)

        if Resources.alwaysUseDeveloperDeviceCheck {
            addVar("isdev", "1")
        } else if Resources.alwaysUseProductionDeviceCheck {
            addVar("isdev", "0")
        } else {
            addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
        }
        
        sendCommand(false)
        
        return .CHECKRESULT_OK
    }
	
	//	func _requestChartCheckAccount(aActDB : StActManager) -> ErrorCode {
	//
	////		try {
	////		mClient = getConnection();
	////		} catch (IOException e1) {
	////		return CHECKRESULT_INTERNALERROR;
	////		} catch (GeneralSecurityException e) {
	////		return CHECKRESULT_INTERNALERROR;
	////		}
	////		if (mClient == null)
	////		return CHECKRESULT_INTERNALERROR;
	//
	//		mRequest = getRequest()
	//		if mRequest == nil {
	//			return .CHECKRESULT_INTERNALERROR
	//		}
	//
	//		setCommand(aCommand: StChartCheckTask.COMMAND_CHARTCHECK_ACCOUNT)
	//
	//		let ActCount = aActDB.getActCodeCount()
	//		if ActCount == 0 {
	//			return ErrorCode.CHECKRESULT_OK
	//		}
	//
	//		let EMail = aActDB.getUserMail()
	//		let Pass = aActDB.getUserPass()
	//
	//		let HWKey :StHardwareKey = StHardwareKey()
	//		let pHWKey :String = HWKey.getDeviceKey()
	//		let pName :String = Resources.packageName
	//		var pVersion :String = ""
	////		try {
	////		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	////		} catch (NameNotFoundException e) {
	////		return CHECKRESULT_INTERNALERROR;
	////		}
	//		pVersion = String(Resources.appVersionNumber)
	//
	//		var EnabledCount :Int = 0
	//		let CP :StActManager.ChartProduct = StActManager.ChartProduct()
	//		for i in 0 ..< ActCount {
	//			aActDB.getActCode(aIndex: i, aCP :CP)
	//			//       if (CP.SubType == 0) {
	//			//            if (CP.Enabled) {
	//			EnabledCount += 1
	//			addVar("pid" + String(i), String(CP.Pid))
	//			addVar("mod" + String(i), String(CP.Mod))
	//			addVar("act" + String(i), CP.ActCode!)
	//			//            }
	//			//            }
	//		}
	//		if EnabledCount == 0 {
	//			return .CHECKRESULT_OK
	//		}
	//
	//		addVar("em", EMail)
	//		addVar("pw", Pass)
	//		addVar("fpw", "1")
	//		addVar("an", pName)
	//		addVar("av", pVersion)
	//		addVar("acs", mChecksum)
	//		addVar("hw", pHWKey)
	//		addVar("ed", "2")
	//		addVar("prc", String(EnabledCount))
	//
	//		sendCommand(false)
	//
	//		return .CHECKRESULT_OK
	//	}
	
	func requestCheckAccountGetChartsetFiles(aActDB : StActManager) {
		if !getDeviceToken(handler: {(token : String) in
			var resultCode :ErrorCode = self.requestCheckAccountGetChartsetFiles(aActDB: aActDB, aDeviceToken: token)
			
			if resultCode != ErrorCode.CHECKRESULT_OK {
				// Error in creating the request
				if resultCode == ErrorCode.CHECKRESULT_DEVICETOKEN_ERROR {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
				} else {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
				}
			}
		})
		{
			// Devicetoken Error
			print("requestCheckAccountGetChartsetFiles: Error with deviceToken")
			mHandler!("", StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
		}
	}
	
	private func requestCheckAccountGetChartsetFiles(aActDB : StActManager, aDeviceToken : String) -> ErrorCode {
		
		if aDeviceToken == "" {
			return .CHECKRESULT_DEVICETOKEN_ERROR
		}
		
		//		try {
		//		mClient = getConnection();
		//		} catch (IOException e1) {
		//		return CHECKRESULT_INTERNALERROR;
		//		} catch (GeneralSecurityException e) {
		//		return CHECKRESULT_INTERNALERROR;
		//		}
		//		if (mClient == null)
		//		return CHECKRESULT_INTERNALERROR;
		
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: StChartCheckTask.COMMAND_CHECK_ACCOUNT_GET_CHART_FILES) // afa
		
		let ActCount = aActDB.getProductCodeCount()
		// With no products, it should still check the device and account
		//		if ActCount == 0 {
		//			return ErrorCode.CHECKRESULT_OK
		//		}
		
		let EMail = aActDB.getUserMail()
		let Pass = aActDB.getUserPass()
		
		let HWKey :StHardwareKey = StHardwareKey()
		let pHWKey :String = HWKey.getDeviceKey()
		
        dout("UIDevice.current.identifierForVendor!.uuidString : \(UIDevice.current.identifierForVendor!.uuidString)")
        
		var pVersion :String = ""
		//		try {
		//		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
		//		} catch (NameNotFoundException e) {
		//		return CHECKRESULT_INTERNALERROR;
		//		}
		pVersion = String(Resources.appVersionString)
		
		var EnabledCount :Int = 0
		let CP :StActManager.ChartProduct = StActManager.ChartProduct()
		for i in 0 ..< ActCount {
			aActDB.getActCode(aIndex: i, aCP :CP)
			//       if (CP.SubType == 0) {
			if (CP.Enabled) {
				EnabledCount += 1
				addVar("pid" + String(i), String(CP.Pid))
				addVar("mod" + String(i), String(CP.Mod))
			}
			//            }
		}
		//		if EnabledCount == 0 {
		//			return .CHECKRESULT_OK
		//		}
		
		addVar("em", EMail)
		addVar("pw", Pass)
		addVar("hw", pHWKey)
        if aDeviceToken == Resources.PASS_DEVICE_CHECK {
            addVar("ht", "967")
            addVar("dt", "")
        } else { // default:
            addVar("ht", isIpad() ? "1" : "0")
            addVar("dt", aDeviceToken)
        }
		addVar("json", "1")
		addVar("prc", String(EnabledCount))
        
        let pName :String = Resources.packageName
        addVar("an", pName)

        //// Add optional parameter that indicates the app id
        
		if Resources.alwaysUseDeveloperDeviceCheck {
			addVar("isdev", "1")
		} else if Resources.alwaysUseProductionDeviceCheck {
			addVar("isdev", "0")
		} else {
			addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
		}
		
		sendCommand(false)
		
		return .CHECKRESULT_OK
	}
	
	
//	func _requestCheckDeviceActivationOnly(aActDB : StActManager, aErrorHandler :@escaping (ErrorCode)->()) {
//		if !getDeviceToken(handler: {(token : String) in
//			aErrorHandler(
//				self._requestCheckDeviceActivationOnly(aActDB: aActDB, aDeviceToken: token)
//			)
//		}) {
//			print("requestCheckAccountGetChartsetFiles Error with deviceToken")
//			aErrorHandler(.CHECKRESULT_INTERNALERROR)
//		}
//	}
//	
//	private func _requestCheckDeviceActivationOnly(aActDB : StActManager, aDeviceToken : String) -> ErrorCode {
//		
//		if aDeviceToken == "" {
//  	return .CHECKRESULT_DEVICETOKEN_ERROR
//		}
//		
//		//		try {
//		//		mClient = getConnection();
//		//		} catch (IOException e1) {
//		//		return CHECKRESULT_INTERNALERROR;
//		//		} catch (GeneralSecurityException e) {
//		//		return CHECKRESULT_INTERNALERROR;
//		//		}
//		//		if (mClient == null)
//		//		return CHECKRESULT_INTERNALERROR;
//		
//		mRequest = getRequest()
//		if mRequest == nil {
//			return .CHECKRESULT_INTERNALERROR
//		}
//		
//		setCommand(aCommand: StChartCheckTask.COMMAND_CHECK_ACCOUNT_GET_CHART_FILES)
//		
//		
//		let EMail = aActDB.getUserMail()
//		let Pass = aActDB.getUserPass()
//		
//		let HWKey :StHardwareKey = StHardwareKey()
//		let pHWKey :String = HWKey.getDeviceKey()
//		
//		addVar("em", EMail)
//		addVar("pw", Pass)
//		addVar("hw", pHWKey)
//		addVar("ht", isIpad() ? "1" : "0")
//		addVar("dt", aDeviceToken)
//		addVar("json", "1")
//		addVar("prc", "0")
//		addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
//
//		sendCommand(false)
//		
//		return .CHECKRESULT_OK
//	}

	func requestDeactivateActiveDevice(aActDB : StActManager) {
		if getDeviceToken(handler: {(token : String) in
//			if token == "" {
//				self.mHandler!(StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
//			}
			var resultCode = self.requestDeactivateActiveDevice(aActDB: aActDB, aDeviceToken: token)
			// Error preparing request
			var numericalErrorCode :Int = StChartCheckTask.CHECKRESULT_OK
			switch resultCode {
			case ErrorCode.CHECKRESULT_OK:
				numericalErrorCode = StChartCheckTask.CHECKRESULT_OK
			case ErrorCode.CHECKRESULT_DEVICETOKEN_ERROR:
				numericalErrorCode = StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR
			default:
				numericalErrorCode = StChartCheckTask.CHECKRESULT_INTERNALERROR
			}
			self.mHandler!("", numericalErrorCode, "")

		}) {
		} else {
			print("requestDeactivateActiveDevice: Error with deviceToken")
			self.mHandler!("", StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
		}
	}
	
	private func requestDeactivateActiveDevice(aActDB : StActManager, aDeviceToken : String) -> ErrorCode {
		
		if aDeviceToken == "" {
			return .CHECKRESULT_DEVICETOKEN_ERROR
		}
		
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: StChartCheckTask.COMMAND_DEACTIVATE_ACTIVE_ACTIVATION)
		
		let EMail = aActDB.getUserMail()
		let Pass = aActDB.getUserPass()
		
		let HWKey :StHardwareKey = StHardwareKey()
		let pHWKey :String = HWKey.getDeviceKey()
		
		addVar("em", EMail)
		addVar("pw", Pass)
		addVar("hw", pHWKey)
        if aDeviceToken == Resources.PASS_DEVICE_CHECK {
            addVar("ht", "967")
            addVar("dt", "")
        } else { // default:
            addVar("ht", isIpad() ? "1" : "0")
            addVar("dt", aDeviceToken)
        }
		addVar("json", "1")
        
        let pName :String = Resources.packageName
        addVar("an", pName)
        
		if Resources.alwaysUseDeveloperDeviceCheck {
			addVar("isdev", "1")
		} else if Resources.alwaysUseProductionDeviceCheck {
			addVar("isdev", "0")
		} else {
			addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
		}
		
		sendCommand(false)
		
		return .CHECKRESULT_OK
	}
	
	func requestDeviceCheckGetBitstate() {
		if getDeviceToken(handler: {(token : String) in
			var resultCode = self.requestDeviceCheckGetBitstate(aDeviceToken: token)
			// Error preparing request
			
//			if resultCode != ErrorCode.CHECKRESULT_OK {
//				self.mHandler!(StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
//			}
			if resultCode != ErrorCode.CHECKRESULT_OK {
				// Error in creating the request
				if resultCode == ErrorCode.CHECKRESULT_DEVICETOKEN_ERROR {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
				} else {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
				}
			}
		}) {
		} else {
			print("requestDeactivateActiveDevice: Error with deviceToken")
			self.mHandler!("", StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
		}
	}
	
	private func requestDeviceCheckGetBitstate(aDeviceToken : String) -> ErrorCode {
		
		if aDeviceToken == "" {
			return .CHECKRESULT_DEVICETOKEN_ERROR
		}
		
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: "gbs")
		
		addVar("dt", aDeviceToken)
		addVar("json", "1")
		addVar("isdev", "1")
		
		sendCommand(false)
		
		return .CHECKRESULT_OK
	}
	
	func requestDeviceCheckSetBitstate() {
		if getDeviceToken(handler: {(token : String) in
			var resultCode = self.requestDeviceCheckSetBitstate(aDeviceToken: token)
			// Error preparing request
//			if resultCode != ErrorCode.CHECKRESULT_OK {
//				self.mHandler!(StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
//			}
			if resultCode != ErrorCode.CHECKRESULT_OK {
				// Error in creating the request
				if resultCode == ErrorCode.CHECKRESULT_DEVICETOKEN_ERROR {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
				} else {
					self.mHandler!("", StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
				}
			}
		}) {
		} else {
			print("requestDeactivateActiveDevice: Error with deviceToken")
			self.mHandler!("", StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
		}
	}
	
	private func requestDeviceCheckSetBitstate(aDeviceToken : String) -> ErrorCode {
		
		if aDeviceToken == "" {
			return .CHECKRESULT_DEVICETOKEN_ERROR
		}
		
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: "sbs")

		addVar("dt", aDeviceToken)
		addVar("json", "1")
		addVar("isdev", "1")
		addVar("bs", "01")

		
		sendCommand(false)
		
		return .CHECKRESULT_OK
	}
	
	func requestCreateAccount(email :String, pwd :String, fn :String, ln :String, str_nr :String, pc :String, city :String, cc :String, phone :String, newsletter :Bool) -> ErrorCode {
	
		mRequest = getRequest()
		if mRequest == nil {
			return .CHECKRESULT_INTERNALERROR
		}
	
		setCommand(aCommand: StChartCheckTask.COMMAND_REGISTER_ACCOUNT)
	
		addVar("em", email);
		addVar("pw", pwd);
		addVar("fn", fn);
		addVar("ln", ln);
		addVar("st", str_nr);
		addVar("sn", "");
		addVar("pc", pc);
		addVar("ci", city);
		
		// get country code from ios locale settings
		var ccToSend = Locale.current.regionCode ?? ""
		addVar("co", ccToSend);
		
		addVar("pn", phone);
		addVar("nl", newsletter ? "1" : "0");
		addVar("sw", "1");
	
		sendCommand(false);
	
		return ErrorCode.CHECKRESULT_OK;
	}
 
	
	//	/**
	//	* Older version of requestFile used for chart apps.
	//	*
	//	* @param aUid
	//	* @param aActCode
	//	* @param aFilename
	//	* @param aStart
	//	* @return Result
	//	*/
	//	public int requestFile(String aUid, String aActCode, String aFilename, long aStart) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_FILE);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//
	//	addVar("uid", aUid);
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("hw", pHWKey);
	//	addVar("pid", String.valueOf(PID));
	//	addVar("mod", String.valueOf(MODULE));
	//	addVar("act", aActCode);
	//	addVar("fn", aFilename);
	//	addVar("fs", String.valueOf(aStart));
	//
	//	StDKW2Settings settings = StDKW2Settings.getSettings();
	//	StDocumentFile path = settings.getDKW2ChartsDir();
	//	mOutputFile = new StDocumentFile(path, aFilename + ".tmp");
	//
	//	/*
	//	try {
	//	if(mDownloadManager==null) mDownloadManager = (DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE);
	//	Request request = new Request(Uri.parse(new String(Base64.decode(mServerUrl))));
	//
	//	request.addRequestHeader("uid", aUid);
	//	request.addRequestHeader("an", pName);
	//	request.addRequestHeader("av", pVersion);
	//	request.addRequestHeader("acs", mChecksum);
	//	request.addRequestHeader("hw", pHWKey);
	//	request.addRequestHeader("pid", String.valueOf(PID));
	//	request.addRequestHeader("mod", String.valueOf(MODULE));
	//	request.addRequestHeader("act", aActCode);
	//	request.addRequestHeader("fn", aFilename);
	//	request.addRequestHeader("fs", String.valueOf(aStart));
	//
	//	request.setDestinationUri(Uri.parse(mOutputFile.toString()));
	//	long enqueue = mDownloadManager.enqueue(request);
	//	}catch (Exception e){
	//	Log.e("StChartCheckTask", "Exception: " + e.getMessage());
	//	}
	//	*/
	//	sendCommand(aStart > 0);
	//
	//	return CHECKRESULT_OK;
	//	}
	//
	//	// public long requestFile(int aPid, int aMod, String aFilename){
	//	//
	//	//    StDKW2Settings settings = StDKW2Settings.getSettings();
	//	//    File path;
	//	//    if (aPid == 899000 && aMod == 1)
	//	//       path = new File(Environment.getExternalStorageDirectory() + "/stentec/tides/");
	//	//    else
	//	//       path = settings.getDKW2ChartsDir();
	//	//    File OutputFile = new File(path, aFilename + ".tmp");
	//	//    long dmID = 0;
	//	//    try {
	//	//       if(mDownloadManager==null) mDownloadManager = (DownloadManager) mContext.getSystemService(DOWNLOAD_SERVICE);
	//	//       String serverUrl = "http://www.stentec.com/charts/dkw2/" + aPid + "/" + aMod + "/" + aFilename.replace(" ", "%20");
	//	//       Log.d("StChartCheckTask", "ServerURL: " + serverUrl);
	//	//       Request request = new Request(Uri.parse(serverUrl));
	//	//
	//	//       request.addRequestHeader("Authorization", "Basic NjQwNjIxMTcyNzpZOFpOMjJqag==");
	//	//
	//	//       String pathString = "file://" + OutputFile.toString();
	//	//       Log.d("Uri", "pathString: " + pathString);
	//	//       Uri uri = Uri.parse(pathString);
	//	//       Log.d("Uri", "Uri: " + uri.toString());
	//	//       request.setDestinationUri(uri);
	//	//
	//	//       Log.d("Uri", "request destinationUri set.");
	//	//       dmID = mDownloadManager.enqueue(request);
	//	//    } catch (Exception e){
	//	//
	//	//       Log.e("StChartCheckTask", "Exception: " + e.getMessage());
	//	//    }
	//	//
	//	//    //sendCommand(aStart > 0);
	//	//
	//	//    return dmID;
	//	// }
	
	//	// Used with getCharts2.php
	//	/**
	//	* RequestFile function used in newer apps to download charts
	//	*
	//	* @param aActDB
	//	* @param aPid
	//	* @param aMod
	//	* @param aActCode
	//	* @param aFilename
	//	* @param aStart
	//	* @return
	//	*/
	//	func _requestFile(aActDB :StActManager, aPid :Int, aMod :Int, aFilename :String, aStart :Int64) -> ErrorCode {
	//		print("requestFile")
	//
	//		var start = aStart
	//		if StChartCheckTask.APPENDING_DISABLED {
	//			start = 0
	//		}
	//
	//		//		try {
	//		//		mClient = getConnection();
	//
	////		mRequest = getRequest()
	//		mRequest = getRequestTestScript()
	//
	//		if mRequest == nil {
	//			return .CHECKRESULT_INTERNALERROR
	//		}
	//
	//		//	} catch (IOException e1) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		} catch (GeneralSecurityException e) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		}
	//		//		if (mClient == null)
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//
	//		setCommand(aCommand: StChartCheckTask.COMMAND_FILE)
	//
	//		var HWKey = StHardwareKey()
	//		var pHWKey = HWKey.getDeviceKey()
	//
	//		var pName = Resources.packageName
	//		var pVersion :String = ""
	//
	//		//		try {
	//		//		pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//		pVersion = String(Resources.appVersionNumber)
	//		//		} catch (NameNotFoundException e) {
	//		//		return CHECKRESULT_INTERNALERROR;
	//		//		}
	//
	//		let EMail = aActDB.getUserMail()
	//		let Pass = aActDB.getUserPass()
	//
	//		addVar("em", EMail)
	//		addVar("pw", Pass)
	//		addVar("an", pName)
	//		addVar("av", pVersion)
	//		addVar("acs", mChecksum)
	//		addVar("hw", pHWKey ?? "")
	//		addVar("pid", String(aPid))
	//		addVar("mod", String(aMod))
	//
	//		addVar("fn", aFilename)
	//		addVar("fs", String(start))
	//
	//		// voor ios specifiek
	//		addVar("ios", "dummyvalue")
	//
	//
	//		//		//		StDKW2Settings settings = StDKW2Settings.getSettings()
	//		var path :URL
	//		//		var path :String
	//		//		if (aPid == 899000) {
	//		//			if (aMod == 1) {
	//		//				path = new StDocumentFile(new File(Environment.getExternalStorageDirectory() + "/stentec/tides/"));
	//		//			} else {
	//		//				path = new StDocumentFile(new File(aActDB.getWWFilePath(mContext, false)));
	//		//			}
	//		//
	//		//		} else {
	//
	//		path = getDKW2Directory()
	//
	//		//		}
	//
	////		mCodedProductString = StChartCheckTask.getCodedProductString(pid: Int64(aPid), mod: aMod, isSub: false)
	//		mOutputFile = path.appendingPathComponent(aFilename + ".tmp")
	//
	//		//		do {
	//		//			try mOutputFile = FileHandle(forWritingTo: URL(fileURLWithPath: path + aFilename + ".tmp"))
	//		//		} catch {
	//		//		}
	//
	//		// request file
	////		let bodyData = "c=gfd&em=paltenburg%40gmail.com&pw=1oposse1&an=com.stentec.wingps_marine_lite&av=130&acs=&hw=ZCQ7-TA3H&pid=196664&mod=1&act=4AGK-TDZB&fn=charts.collection&fs=0"
	//
	//		// request best activation
	////		let bodyData = "c=bac2&em=paltenburg%40gmail.com&pw=1oposse1&uid=&an=com.stentec.wingps_marine_lite&av=130&acs=&hw=LPNP-23GT&pid=899922"
	//
	////		mPostData = bodyData
	//
	//		sendCommand(start > 0)
	//
	//		return .CHECKRESULT_OK
	//
	//		/*
	//
	//		String UrlStr;
	//		try {
	//		UrlStr = new String(Base64.decode(mServerUrl));
	//		} catch (Base64DecoderException e) {
	//		return CHECKRESULT_INTERNALERROR;
	//		}
	//		Uri FUri = Uri.parse(UrlStr)
	//		.buildUpon()
	//		.appendQueryParameter("c", "gfd2")
	//		.appendQueryParameter("em", EMail)
	//		.appendQueryParameter("pw", Pass)
	//		.appendQueryParameter("an", pName)
	//		.appendQueryParameter("av", pVersion)
	//		.appendQueryParameter("acs", mChecksum)
	//		.appendQueryParameter("hw", pHWKey)
	//		.appendQueryParameter("pid", String.valueOf(aPid))
	//		.appendQueryParameter("mod", String.valueOf(aMod))
	//		.appendQueryParameter("act", aActCode)
	//		.appendQueryParameter("fn", aFilename)
	//		.build();
	//
	//		//DlReq.addRequestHeader("c", "gfd2");
	//		//DlReq.addRequestHeader("em", EMail);
	//		//DlReq.addRequestHeader("pw", Pass);
	//		//DlReq.addRequestHeader("an", pName);
	//		//DlReq.addRequestHeader("av", pVersion);
	//		//DlReq.addRequestHeader("acs", mChecksum);
	//		//DlReq.addRequestHeader("hw", pHWKey);
	//		//DlReq.addRequestHeader("pid", String.valueOf(aPid));
	//		//DlReq.addRequestHeader("mod", String.valueOf(aMod));
	//		//DlReq.addRequestHeader("act", aActCode);
	//		//DlReq.addRequestHeader("fn", aFilename);
	//		//DlReq.addRequestHeader("fs", String.valueOf(aStart));
	//
	//		DownloadManager DlMan = (DownloadManager)mContext.getSystemService(DOWNLOAD_SERVICE);
	//
	//		//        String serverUrl = "http://www.stentec.com/charts/dkw2/" + aPid + "/" + aMod + "/" + aFilename.replace(" ", "%20");
	//		String serverUrl = "";
	//		try {
	//		serverUrl = "http://www.stentec.com/charts/dkw2/" + aPid + "/" + aMod + "/" + URLEncoder.encode(aFilename, "UTF-8");
	//		} catch (UnsupportedEncodingException e) {
	//		// shouldn't happen. UTF8 is always supported.
	//		}
	//		//       Log.d("StChartCheckTask", "ServerURL: " + serverUrl);
	//		//
	//
	//		//       Request request = new Request(Uri.parse(serverUrl));
	//
	//		//    DownloadManager.Request DlReq = new DownloadManager.Request(FUri);
	//		DownloadManager.Request DlReq = new DownloadManager.Request(Uri.parse(serverUrl));
	//		DlReq.addRequestHeader("Authorization", "Basic NjQwNjIxMTcyNzpZOFpOMjJqag==");
	//
	//		DlReq.setDescription("Downloading chart: " + aFilename).setTitle("Chart download");
	//		//    DlReq.setDestinationInExternalFilesDir(mContext, null, OutputFile.toString());
	//
	//		DlReq.setDestinationUri(Uri.parse(mOutputFile.file.toURI().toString())/*mOutputFile.getUri()*/);
	//
	//		DlReq.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
	//		DlReq.setVisibleInDownloadsUi(false);
	//
	//		mCurrentDlID = DlMan.enqueue(DlReq)*/
	//	}
	
	// Used with getchartsa.php
	func requestFile(aActDB :StActManager, aPid :Int, aMod :Int, aFilename :String, aStart :Int64) -> ErrorCode {
		print("requestFile for files that need activation check")
		
		var start = aStart
		if StChartCheckTask.APPENDING_DISABLED {
			start = 0
		}
		
//        var request = getRequestTestScript()
        var request = getRequest()
		if request == nil {
			return .CHECKRESULT_INTERNALERROR
		}
		
		setCommand(aCommand: StChartCheckTask.COMMAND_FILE)
		
		//		var HWKey = StHardwareKey()
		//		var pHWKey = HWKey.getDeviceKey()
		
		//		var pName = Resources.packageName
		//		var pVersion :String = ""
		
		//		pVersion = String(Resources.appVersionNumber)
		
		let EMail = aActDB.getUserMail()
		let Pass = aActDB.getUserPass()
		
		addVar("em", EMail)
		addVar("pw", Pass)
		addVar("pid", String(aPid))
		addVar("mod", String(aMod))
		
		addVar("fn", aFilename)
		addVar("fs", String(start))
		
		var path = getDKW2Directory()
		
		//		mCodedProductString = StChartCheckTask.getCodedProductString(pid: Int64(aPid), mod: aMod, isSub: false)
		
		mOutputFile = path.appendingPathComponent(aFilename + ".tmp")
		
		//		sendCommand(start > 0)
		
		//		guard var request = mRequest,
		//			let postData = mPostData else { return .CHECKRESULT_INTERNALERROR }
		//
		
		request?.httpBody = mPostData?.data(using: String.Encoding.utf8)
		
		//		CheckTask(self, aAppend: start > 0).execute()
		
		// Pass request on to download service class
		DownloadService.service.doRequest(request!,
													 pid: aPid,
													 mod: aMod,
													 fileName: aFilename,
													 targetURL: mOutputFile!,
													 append: start > 0,
													 finishedHandler: {(result) in
                                                        self.mHandler!(self.mCommand, result, "")})
		
		return .CHECKRESULT_OK
	}
	
		// Used with getChartsa.php
		/**
		* RequestFile used for files that do not need activation checks. (Waterway network).
		* @param aPid
		* @param aMod
		* @param aFilename
		* @param aStart
		* @return
		*/
		func requestFile(aPid :Int, aMod :Int, aFilename :String, aStart :Int64) -> ErrorCode {
            print("requestFile for files that need no activation")

            var start = aStart
            if StChartCheckTask.APPENDING_DISABLED {
                start = 0
            }
            
            var request = getRequest()
            if request == nil {
                return .CHECKRESULT_INTERNALERROR
            }
            
            setCommand(aCommand: StChartCheckTask.COMMAND_FILEWW)
            
            let pName :String = Resources.packageName
//            String pVersion = "";
//            try {
//            pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
//            } catch (NameNotFoundException e) {
//            return CHECKRESULT_INTERNALERROR;
//            }
            let pVersion :String = String(Resources.appVersionString)

            addVar("ft", "1")
            addVar("pn", pName)
            addVar("pv", pVersion)
            addVar("fn", aFilename)
            addVar("fs", String(aStart))
            
            if !(aPid == 899000 && aMod == 2) {
                return .CHECKRESULT_INTERNALERROR
            }
            mOutputFile = StUtils.getUrlByFilename(aFilename + ".tmp")

//            sendCommand(aStart > 0);
            
            request?.httpBody = mPostData?.data(using: String.Encoding.utf8)
            
            // Pass request on to download service class
            DownloadService.service.doRequest(request!,
                                                         pid: aPid,
                                                         mod: aMod,
                                                         fileName: aFilename,
                                                         targetURL: mOutputFile!,
                                                         append: start > 0,
                                                         finishedHandler: {(result) in
                                                            self.mHandler!(self.mCommand, result, "")})
			return .CHECKRESULT_OK
		}
	
	//	public int requestFileInfo(String aUid, String aActCode) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_FILELIST_APP);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	addVar("uid", aUid);
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("hw", pHWKey);
	//	addVar("pid", String.valueOf(PID));
	//	addVar("mod", String.valueOf(MODULE));
	//	addVar("act", aActCode);
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	//
	//	public int requestFileInfoAccount(String aEmail, String aPass) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_FILELIST_ACCOUNT);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("hw", pHWKey);
	//	addVar("em", aEmail);
	//	addVar("pw", aPass);
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	//
	//	public int requestConnectToAccount(String aUid, String aEmail, String aPass, String aActCode) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_CONNECT_ACCOUNT);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	addVar("uid", aUid);
	//	addVar("hw", pHWKey);
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("pid", String.valueOf(PID));
	//	addVar("mod", String.valueOf(MODULE));
	//	addVar("em", aEmail);
	//	addVar("pw", aPass);
	//	addVar("act", aActCode);
	//
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	
	//	// request program check (used by program apps, to check their activation)
	//	public int requestAppCheck(StActManager aActDB, String aUid, String aActCode) {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_ACT_APP);
	//
	//	StHardwareKey HWKey = new StHardwareKey(mContext);
	//	String pHWKey = HWKey.getDeviceKey();
	//	String pName = mContext.getPackageName();
	//	String pVersion = "";
	//	try {
	//	pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
	//	} catch (NameNotFoundException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (aActDB.getUserDataAvailable()) {
	//	addVar("em", aActDB.getUserMail());
	//	addVar("pw", aActDB.getUserPass());
	//	}
	//	addVar("uid", aUid);
	//	addVar("an", pName);
	//	addVar("av", pVersion);
	//	addVar("acs", mChecksum);
	//	addVar("hw", pHWKey);
	//	addVar("pid", String.valueOf(mAppID));
	//	addVar("mod", "");
	//	if (aActCode.length() > 0)
	//	addVar("act", aActCode);
	//	sendCommand(false);
	//
	//	return CHECKRESULT_OK;
	//	}
	//
	//	public int requestTideFiles() {
	//	try {
	//	mClient = getConnection();
	//	} catch (IOException e1) {
	//	return CHECKRESULT_INTERNALERROR;
	//	} catch (GeneralSecurityException e) {
	//	return CHECKRESULT_INTERNALERROR;
	//	}
	//	if (mClient == null)
	//	return CHECKRESULT_INTERNALERROR;
	//
	//	setCommand(COMMAND_TIDEFILES);
	//
	//	sendCommand(false);
	//	return CHECKRESULT_OK;
	//	}
	
    //
    
    func requestCheckAccountAndReceipt(aActDB : StActManager) -> ErrorCode{
//        if !getDeviceToken(handler: {(token : String) in
//            var resultCode :ErrorCode = self.requestCheckAccountAndReceipt(aActDB: aActDB, aDeviceToken: token)
//
//            if resultCode != ErrorCode.CHECKRESULT_OK {
//                // Error in creating the request
//                if resultCode == ErrorCode.CHECKRESULT_DEVICETOKEN_ERROR {
//                    self.mHandler!(StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
//                } else {
//                    self.mHandler!(StChartCheckTask.CHECKRESULT_INTERNALERROR, "")
//                }
//            }
//        })
//        {
//            // Devicetoken Error
//            print("requestCheckAccountGetChartsetFiles: Error with deviceToken")
//            mHandler!(StChartCheckTask.CHECKRESULT_DEVICETOKEN_ERROR, "")
//        }
//    }
//
//    private func requestCheckAccountAndReceipt(aActDB : StActManager, aDeviceToken : String) -> ErrorCode {
//
//        if aDeviceToken == "" {
//            return .CHECKRESULT_DEVICETOKEN_ERROR
//        }
        
        //        try {
        //        mClient = getConnection();
        //        } catch (IOException e1) {
        //        return CHECKRESULT_INTERNALERROR;
        //        } catch (GeneralSecurityException e) {
        //        return CHECKRESULT_INTERNALERROR;
        //        }
        //        if (mClient == null)
        //        return CHECKRESULT_INTERNALERROR;
        
        mRequest = getRequestReceiptCheck()
        
        
        if mRequest == nil {
            return .CHECKRESULT_INTERNALERROR
        }
        
        setCommand(aCommand: StChartCheckTask.COMMAND_CHECK_ACCOUNT_AND_RECEIPT)
        
        let ActCount = aActDB.getProductCodeCount()
        // With no products, it should still check the device and account
        //        if ActCount == 0 {
        //            return ErrorCode.CHECKRESULT_OK
        //        }
        
        let EMail = aActDB.getUserMail()
        let Pass = aActDB.getUserPass()
        
        let HWKey :StHardwareKey = StHardwareKey()
        let pHWKey :String = HWKey.getDeviceKey()
        
        let pName :String = Resources.packageName
//        var pVersion :String = ""
        //        try {
        //        pVersion = String.valueOf(mContext.getPackageManager().getPackageInfo(pName, 0).versionCode);
        //        } catch (NameNotFoundException e) {
        //        return CHECKRESULT_INTERNALERROR;
        //        }
//        pVersion = String(Resources.appVersionNumber)
        var pVersion = Resources.appVersionString

        
        var EnabledCount :Int = 0
        let CP :StActManager.ChartProduct = StActManager.ChartProduct()
        for i in 0 ..< ActCount {
            aActDB.getActCode(aIndex: i, aCP :CP)
            //       if (CP.SubType == 0) {
            if (CP.Enabled) {
                EnabledCount += 1
                addVar("pid" + String(i), String(CP.Pid))
                addVar("mod" + String(i), String(CP.Mod))
            }
            //            }
        }
        //        if EnabledCount == 0 {
        //            return .CHECKRESULT_OK
        //        }
        
        addVar("em", EMail)
        addVar("pw", Pass)
        
        var receipt = AppStorePurchases.getReceipt()
        // TODO: handle error if receipt == ""
        
        addVar("rd", receipt)

//        addVar("hw", pHWKey)
//        addVar("ht", isIpad() ? "1" : "0")
//        addVar("dt", aDeviceToken)
//        addVar("json", "1")
//        addVar("prc", String(EnabledCount))

//        if Resources.alwaysUseDeveloperDeviceCheck {
//            addVar("isdev", "1")
//        } else if Resources.alwaysUseProductionDeviceCheck {
//            addVar("isdev", "0")
//        } else {
//            addVar("isdev", String(StUtils.isDebug() ? 1 : 0))
//        }
        
        
        sendCommand(false)
        
        return .CHECKRESULT_OK
    }
    
	func sendCommand(_ aAppend :Bool) {
		//		try {
		
		//		let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
		
        guard mRequest != nil, mPostData != nil else { return }

		mRequest!.httpBody = mPostData!.data(using: String.Encoding.utf8)
		
		mTask = CheckTask(self, aAppend: aAppend)
		
		guard let task = mTask
			//			,	let mPostData = mPostData
			else { return }
		
		//		mTask.execute(mPostData)
		task.execute()
		
		//		Log.d("POSTDATA","POSTDATA: " + mPostData);
		//		} catch (Exception e) {
		//		Log.e("StChartCheckTask", "Exception: " + e.getMessage());
		//		}
	}
	
	//public void setServerUrl(String aUrl) {
	//mServerUrl = aUrl;
	//}
	
	func addVar(_ aVarName :String, _ aValue :String)  {
		
		if mPostData == nil { return }
		
		if mPostData!.count > 0 {
			mPostData! += "&"
		}
		
		//		try {
		mPostData! += aVarName + "="
		if aValue != nil && !aValue.isEmpty {
			//			mPostData += URLEncoder.encode(aValue, "UTF-8")
			mPostData! += aValue.addingPercentEncodingIncludingExtraCharacters()!
		}
		//		} catch (UnsupportedEncodingException e) {
		//		}
	}
	
	//	@Nullable
	//	func getConnection() -> HttpsURLConnection
	//		//	throws IOException, KeyStoreException, KeyManagementException, CertificateException, NoSuchAlgorithmException
	//	{
	//		//     if (mSocketFactory == null)
	//		//           mSocketFactory = getSocketFactory(mPemFile);
	//		//         if (mSocketFactory == null)
	//		//            return null;
	//
	//		HttpsURLConnection client = (HttpsURLConnection)mUrl.openConnection();
	//		//        client.setSSLSocketFactory(mSocketFactory);
	//		client.setRequestMethod("POST");
	//		client.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	//		client.setDoOutput(true);
	//		client.setUseCaches(false);
	//		return client;
	//	}
	
	func getRequest() -> URLRequest? {
		guard let url = mUrl else { return nil }
		var request = URLRequest(url: url)
		request.httpMethod = "POST"
		return request
	}
	
    func getRequestReceiptCheck() -> URLRequest? {
        if let data = Data(base64Encoded: mServerUrlIosReceiptCheck),
            let urlStr = String(data: data, encoding: .utf8) {
            var request = URLRequest(url: URL(string: urlStr)!)
            request.httpMethod = "POST"
            return request
        }
        return nil
    }
    
//	func getRequestTestScript() -> URLRequest? {
//		if let data = Data(base64Encoded: mServerUrlIos) {
//			if let UrlStr = String(data: data, encoding: .utf8) {
//				var request = URLRequest(url: URL(string: UrlStr)!)
//				request.httpMethod = "POST"
//				return request
//			}
//		}
//		return nil
//	}
//

	
	func urlSession(_ session: URLSession, didReceiveChallenge challenge: URLAuthenticationChallenge, completionHandler: (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
		if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
			if let trust = challenge.protectionSpace.serverTrust {
				completionHandler(.useCredential, URLCredential(trust: trust))
			}
		}
	}
	
	//private SSLSocketFactory getSocketFactory(Certificate aCa) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
	//// Create a KeyStore containing the trusted CAs
	//String keyStoreType = KeyStore.getDefaultType();
	//KeyStore keyStore = KeyStore.getInstance(keyStoreType);
	//keyStore.load(null, null);
	//keyStore.setCertificateEntry("ca", aCa);
	//
	//// Create a TrustManager that trusts the CAs in the KeyStore
	//String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
	//TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
	//tmf.init(keyStore);
	//
	//// Create an SSLContext that uses the TrustManager
	//SSLContext context = SSLContext.getInstance("TLS");
	//
	//context.init(null, tmf.getTrustManagers(), null);
	//return context.getSocketFactory();
	//}
	//
	//private SSLSocketFactory getSocketFactory(File aFilename) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
	//// Load CA from file
	//CertificateFactory cf = CertificateFactory.getInstance("X.509");
	//FileInputStream caInput;
	//try {
	//caInput = new FileInputStream(aFilename);
	//} catch (FileNotFoundException e) {
	//return null;
	//}
	//
	//Certificate ca;
	//try {
	//ca = cf.generateCertificate(caInput);
	//} finally {
	//caInput.close();
	//}
	//
	//return getSocketFactory(ca);
	//}
	//
	//private SSLSocketFactory getSocketFactory(int aCertificateId) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
	//
	//// Load CA from raw resources
	//CertificateFactory cf = CertificateFactory.getInstance("X.509");
	//InputStream caInput = mContext.getResources().openRawResource(aCertificateId);
	////     caInput.read();
	//Certificate ca;
	//try {
	//ca = cf.generateCertificate(caInput);
	//} finally {
	//caInput.close();
	//}
	//return getSocketFactory(ca);
	//}
	
	func setCommand(aCommand :String) {
		mCommand = aCommand
		mPostData = ""
		//		mOutputFile = nil
		addVar("c", aCommand)
	}
	
}


class CheckTask :NSObject, URLSessionDelegate, URLSessionDataDelegate{
	
	var mAppend :Bool = false
	//	var mUrlSession :URLSession? = nil
	var mChartCheckTask :StChartCheckTask
    
	//	init(_ aOutputFile :FileHandle?, aAppend :Bool, aRequest :URLRequest?, aUrlSession :URLSession?, aStatusCheck :StatusCheck?, aHandler :((Int, String) -> Void)?, aStChartCheckTask :StChartCheckTask) {
	init(_ aStChartCheckTask :StChartCheckTask, aAppend :Bool) {
		//		mAppend = aAppend
		//		mOutputFile = aOutputFile
		//		mRequest = aRequest
		//		mUrlSession = aUrlSession
		//		mStatusCheck = aStatusCheck
		//		mHandler = aHandler
		mChartCheckTask = aStChartCheckTask
		mAppend = aAppend
		//		mUrlSession = aUrlSession
	}
	
	//			func execute(_ postData :String) {
	func execute() {
		
		//		mClient.setConnectTimeout(CONNECTTIMEOUT);
		//		mClient.setReadTimeout(READTIMEOUT);
		//
		//		// Send request
		//		DataOutputStream pd = null;
		//		try {
		//		pd = new DataOutputStream(mClient.getOutputStream());
		//		pd.writeBytes(postData[0]);
		//		pd.flush();
		//		pd.close();
		//		} catch (SocketTimeoutException e) {
		//		return String.valueOf(CHECKRESULT_TIMEOUT) + "\n";
		//		} catch (UnknownHostException e) {
		//		return String.valueOf(CHECKRESULT_TIMEOUT) + "\n";
		//		} catch (IOException e) {
		//		Log.e("StChartCheckTask", "IOException: " + e.getMessage());
		//		return String.valueOf(CHECKRESULT_INTERNALERROR);
		//		}
		
		guard let mRequest = mChartCheckTask.mRequest
			//			, let mUrlSession = mUrlSession
			else { return }
		
		//			mRequest.httpBody = postData.data(using: String.Encoding.utf8);
		
		let bodyData = mRequest.httpBody!
		
		let bodyDataString = String(data: bodyData, encoding: String.Encoding.utf8)!
		
		print("\nhttpBody: ", bodyDataString)
		
		// Post data and get response
		//		InputStream is;
		//		try {
		//			int code = mClient.getResponseCode();
		//			if (code != 200) {
		//				return String.valueOf(CHECKRESULT_INTERNALERROR);
		//			}
		//
		//			is = mClient.getInputStream();
		//
		//			if (!(mCommand.equals(COMMAND_FILE) || mCommand.equals(COMMAND_FILEWW))) {
		
		if !(mChartCheckTask.mCommand == StChartCheckTask.COMMAND_FILE || mChartCheckTask.mCommand == StChartCheckTask.COMMAND_FILEWW) {
			// Response is not a file
			
			//				BufferedReader rd = new BufferedReader(new InputStreamReader(is));
			//
			//				String line;
			//				StringBuffer response = new StringBuffer();
			//
			//				while ((line = rd.readLine()) != null) {
			//					response.append(line);
			//					response.append('\n');
			//				}
			//
			//				rd.close();
			//				return response.toString();
			
			let urlSession = URLSession(configuration: .default, delegate: mChartCheckTask, delegateQueue: nil)
			
			let task = urlSession.dataTask(with: mRequest, completionHandler: {data, response, error -> Void in
				if let httpResponse = response as? HTTPURLResponse, let data = data {
					if httpResponse.statusCode == 200 {
						
						var responseString = String(data: data, encoding: .utf8)!
						
						print("httpResponse = 200", responseString)
						
						
						//						//testing:
						//						self.init()
						//						input.open()
						//
						//						let bufferSize = 1024
						//						let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: bufferSize)
						//						while input.hasBytesAvailable {
						//							let read = input.read(buffer, maxLength: bufferSize)
						//							self.append(buffer, count: read)
						//						}
						//						buffer.deallocate(capacity: bufferSize)
						//
						//						input.close()
						
						
						self.onPostExecute(aResponse: responseString)
					} else {

						print("Error with http request: code \(httpResponse.statusCode)")
						
					}
				} else {
					dout("Unwrapping HTTPResponse failed")
					
					if let error = error as? NSError, error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
						dout("Not connected")
						self.onPostExecute(aResponse: String(StChartCheckTask.CHECKRESULT_NOTCONNECTEDTOINTERNET))
					} else {
						self.onPostExecute(aResponse: String(StChartCheckTask.CHECKRESULT_INTERNALERROR))
					}
				}
			})
			task.resume()
			
		} else {
			
			//				long filesize = Long.parseLong(mClient.getHeaderField("Content-Length")) - 2;
			//				BufferedInputStream rd = new BufferedInputStream(is);
			//
			//				// read error code
			//				byte r[] = new byte[2];
			//				rd.read(r);
			//				String result = new String(r, "US-ASCII");
			//
			//				if (result.compareTo("0\n") != 0)
			//				return result;
			//
			//				// read file data
			//				//OutputStream output = new FileOutputStream(mOutputFile, mAppend);
			//				OutputStream output = mOutputFile.getOutputStream(mContext, mAppend);
			//				byte data[] = new byte[1024];
			//				int count;
			//				long total = 0;
			//				Bundle dlData = new Bundle();
			//				while ((count = rd.read(data)) != -1 && !isCancelled()) {
			//					total += count;
			//					//                Message msg = mHandler.obtainMessage(CHECKRESULT_PROGRESS, 0, 0);
			//					Message msg = mHandler.obtainMessage(CHECKRESULT_PROGRESS, Math.round((float)total / (float)filesize * 100), 0);
			//					dlData.putLong("total", total);
			//					msg.setData(dlData);
			//					mHandler.sendMessage(msg);
			//					output.write(data, 0, count);
			//				}
			//
			//				output.flush();
			//				output.close();
			//				rd.close();
			//				return result;
			
			//			let sessionIdentifier :String = "ChartDownload"+mRequest.mainDocumentURL!.path
			//			var urlSession: URLSession = {
			//				let config = URLSessionConfiguration.background(withIdentifier: sessionIdentifier)
			//				config.isDiscretionary = true
			//				config.sessionSendsLaunchEvents = true
			//				return URLSession(configuration: config, delegate: self, delegateQueue: nil)
			//			}()
			
			//			let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/wingps/wingps5litesetup.exe")!
			//			downloadBackground(url: testingUrl)
			
			//			let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
			//			let task = urlSession.downloadTask(with: mRequest)
			//			task.resume()
			
			
			//			let configuration = URLSessionConfiguration.background(withIdentifier: "backgroundDownloadSession")
			//			let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
			//			let task = urlSession.downloadTask(with: mRequest)
			//			task.resume()
			
			//			let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
			//			let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
			
			//			// Make sure to have one background session at a time. For this use the static class "DownloadService".
			//			let urlSession = DownloadService.service.backgroundSession
			//
			//			let task = urlSession.downloadTask(with: mRequest)
			//
			//			task.resume()
			
			// Pass request on to download service class
			
			//			DownloadService.service.doRequest(mRequest,
			//														 codedProductString: mChartCheckTask.mCodedProductString,
			//														 targetURL: mChartCheckTask.mOutputFile!,
			//														 append: mAppend,
			//														 progressHandler: mChartCheckTask.mHandler,
			//														 finishedHandler: {(result) in self.onPostExecute(errorCode: result)})
		}
		
		//
		////} catch (IOException e) {
		//		} catch (Exception e) { // catch all exceptions, incl FileNotFoundException
		//			return String.valueOf(CHECKRESULT_INTERNALERROR);
		//		}
		//		}
		
	}
	
	//	func application(_ application: UIApplication,
	//						  handleEventsForBackgroundURLSession identifier: String,
	//						  completionHandler: @escaping () -> Void) {
	//		backgroundCompletionHandler = completionHandler
	//	}
	//
	//	func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
	//		DispatchQueue.main.async {
	//			guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
	//				let backgroundCompletionHandler =
	//				appDelegate.backgroundCompletionHandler else {
	//					return
	//			}
	//			backgroundCompletionHandler()
	//		}
	//	}
	//
	//	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
	//						 didFinishDownloadingTo location: URL) {
	//		guard let httpResponse = downloadTask.response as? HTTPURLResponse,
	//			(200...299).contains(httpResponse.statusCode) else {
	//				print ("server error")
	//				return
	//		}
	//		do {
	//			let documentsURL = try
	//				FileManager.default.url(for: .documentDirectory,
	//												in: .userDomainMask,
	//												appropriateFor: nil,
	//												create: false)
	//			let savedURL = documentsURL.appendingPathComponent(
	//				location.lastPathComponent)
	//			try FileManager.default.moveItem(at: location, to: savedURL)
	//		} catch {
	//			print ("file error: \(error)")
	//		}
	//	}
	//
	//	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
	//						 didCompleteWithError: e) {
	//	}
	
	//@Override
	//protected void onCancelled(String aResponse) { // this function is called from API level 11 onwards
	//Message msg = mHandler.obtainMessage(CHECKRESULT_CANCELED);
	//mHandler.sendMessage(msg);
	//}
	//
	//@Override
	//protected void onCancelled() { // this function is called from API level 3
	//Message msg = mHandler.obtainMessage(CHECKRESULT_CANCELED);
	//mHandler.sendMessage(msg);
	//}
	
	//	@Override
	func onPostExecute(aResponse :String) {
		print("StChartCheckTask - onPostExecute - Response: " + aResponse)
		let useJson = true
		
		var errorCode :Int = StChartCheckTask.CHECKRESULT_INTERNALERROR
		var data :String = ""
		
		if useJson {
			
			// parse json
			if let responseDict = try? JSONSerialization.jsonObject(with: aResponse.data(using: .utf8)!) as! [String: Any]{
				print(responseDict)
				
				errorCode = Int(responseDict["Result"] as! Int)
				
				if errorCode == StChartCheckTask.CHECKRESULT_OK {
					data = aResponse
				}
			} else {
				// json failed, so check the first line for an error code
				if let range = aResponse.range(of: "\n") {
					var pos = range.lowerBound
					errorCode = Int(String(aResponse[..<pos])) ?? errorCode
				} else {
					// try whole string
					errorCode = Int(aResponse) ?? errorCode
				}
			}
			
		} else {
			
			// parse response line by line
			var pos :String.Index? = nil
			if let range = aResponse.range(of: "\n") {
				pos = range.lowerBound
			}
			
			if pos != nil {
				errorCode = Int(aResponse.substring(to: pos!)) ?? errorCode
			}
			
			if errorCode == StChartCheckTask.CHECKRESULT_OK {
				
				data = aResponse.substring(from: aResponse.index(pos!, offsetBy: 1))
			}
		}
		
		if mChartCheckTask.mHandler != nil {
			mChartCheckTask.mHandler!(mChartCheckTask.mCommand, errorCode, data)
		}
	}
	
	func onPostExecute(errorCode :Int) {
		if mChartCheckTask.mHandler != nil {
            mChartCheckTask.mHandler!(mChartCheckTask.mCommand, errorCode, "")
		}
	}
	
	//	func download(url :URL){
	//
	//		let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
	//
	//		let task = urlSession.downloadTask(with: url)
	//
	//		task.resume()
	//	}
	
	func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
		print("receiving \(data.count) bytes:")
		print(String(data: data, encoding: .ascii)!)
		// handle data; interpret error code, save to file
	}
	
	//	func downloadBackground(url :URL){
	//		let configuration = URLSessionConfiguration.background(withIdentifier: "backgroundDownloadSession")
	//
	//		let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
	//
	//		let task = urlSession.downloadTask(with: url)
	//
	//		task.resume()
	//	}
	
	
}

