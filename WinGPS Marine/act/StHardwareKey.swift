//
//  StHardwareKey.swift
//  WinGPS Marine
//
//  Created by Standaard on 28/08/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

public class StHardwareKey {
	
	//	protected static final String PREFS_FILE = "device_id.xml";
	let PREFS_DEVICE_ID = "device_id"
	
	static var uuid :UUID? = nil
	
	
	//	// singleton to return the same hardwarekey every time
	//	private static StHardwareKey mHWKey = null;
	//
	//	public static String getHardwareKey(Context aContext) {
	//	if (mHWKey == null) {
	//	mHWKey = new StHardwareKey(aContext);
	//	}
	//	return mHWKey.getDeviceKey();
	//	}
	
	static let initLock = DispatchQueue(label: "com.stentec.stlib.act.StHardwareKey.init")
	
	//	public StHardwareKey(Context context) {
	init() {
		
		if StHardwareKey.uuid == nil {
			//			synchronized (StHardwareKey.class) {
			StHardwareKey.initLock.sync{
				if StHardwareKey.uuid == nil {
					//					final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
					let userDefs = UserDefaults.standard
					let id = userDefs.string(forKey: PREFS_DEVICE_ID)
					//					final String id = prefs.getString(PREFS_DEVICE_ID, null );
					
					if id != nil {
						// Use the ids previously computed and stored in the prefs file
						StHardwareKey.uuid = UUID(uuidString: id!);
					} else {
						//						let androidId :String = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
						
						let iosDeviceID :String = UIDevice.current.identifierForVendor!.uuidString
						
						//						// Using the iOS's deviceID. This changes with every installation on the same device.
						//						try {
						//						if (!"9774d56d682e549c".equals(androidId)) {
						//						uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
						//						} else {
						//						String deviceId=null;
						//						try {
						//						deviceId = ((TelephonyManager) context.getSystemService( Context.TELEPHONY_SERVICE )).getDeviceId();
						//						}
						//						catch (Exception e) {
						//						// this can throw a security exception if permission READ_PHONE_STATE is not available, so we protect it
						//						}
						//						uuid = deviceId!=null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
						//						}
						//						} catch (UnsupportedEncodingException e) {
						//						throw new RuntimeException(e);
						//						}
						
						StHardwareKey.uuid = UUID(uuidString: iosDeviceID)
						
						// Write the value out to the prefs file
						//						prefs.edit().putString(PREFS_DEVICE_ID, uuid.toString() ).commit();
						userDefs.set(iosDeviceID, forKey: PREFS_DEVICE_ID)
					}
				}
			}
		}
	}
	
	/**
	* Returns a unique UUID for the current android device.  As with all UUIDs, this unique ID is "very highly likely"
	* to be unique across all Android devices.  Much more so than ANDROID_ID is.
	*
	* The UUID is generated by using ANDROID_ID as the base key if appropriate, falling back on
	* TelephonyManager.getDeviceID() if ANDROID_ID is known to be incorrect, and finally falling back
	* on a random UUID that's persisted to SharedPreferences if getDeviceID() does not return a
	* usable value.
	*
	* In some rare circumstances, this ID may change.  In particular, if the device is factory reset a new device ID
	* may be generated.  In addition, if a user upgrades their phone from certain buggy implementations of Android 2.2
	* to a newer, non-buggy version of Android, the device ID may change.  Or, if a user uninstalls your app on
	* a device that has neither a proper Android ID nor a Device ID, this ID may change on reinstallation.
	*
	* Note that if the code falls back on using TelephonyManager.getDeviceId(), the resulting ID will NOT
	* change after a factory reset.  Something to be aware of.
	*
	* Works around a bug in Android 2.2 for many devices when using ANDROID_ID directly.
	*
	* @see http://code.google.com/p/android/issues/detail?id=10603
	*
	* @return a UUID that may be used to uniquely identify your device for most purposes.
	*/
	func getDeviceKey() -> String {
		let uuidString :String = StHardwareKey.uuid!.uuidString

		//		var buffer1 = NSMutableData(length: 8)
//		buffer1.putLong(uuid.getMostSignificantBits());
		let buffer1 :Data = uuidString.data(using: .utf8)!.subdata(in: 0 ..< 8)
		var Part1 = compressBytes(buffer: buffer1 as NSData)
		Part1 = (Part1 & 0x0003ffff) ^ ((Part1 & 0xfffc0000) >> 18)

//		ByteBuffer buffer2 = ByteBuffer.wrap(new byte[8]);
//		buffer2.putLong(uuid.getLeastSignificantBits());
		let buffer2 = uuidString.data(using: .utf8)!.subdata(in: uuidString.count - 8 ..< uuidString.count)
		var Part2 = compressBytes(buffer: buffer2 as NSData)
		Part2 = (Part2 & 0x0007ffff) ^ ((Part2 & 0xfff80000) >> 19)

		let Data1 :UInt8 = UInt8(((Part1 & 0x00030000) >> 16) | ((Part2 & 0x00070000) >> 14))
		let Data2 :UInt16 = UInt16(Part1 & 0x0000ffff)
		let Data3 :UInt16 = UInt16(Part2 & 0x0000ffff)
		var B = [UInt32](repeating: 0, count: 2)
		B[0] = (UInt32(Data1) << 3) | ((UInt32(Data2) & 0x0fff) << 8) | 3 // versienummer van 3 naar 4 voor iOS
		B[1] = UInt32((Data2 & 0xf000) >> 12) | (UInt32(Data3) << 4)
		return binaryToStr(Buffer: B, Count: 2)
	}
	
	func binaryToStr(Buffer :[UInt32], Count :Int) -> String {
		let IndexToChar :String = "9BSFU8TGARVZQ2PHDW4MX7N3CE5LKYJ6"
		var result :String = ""
		
		for Index in 0 ..< Count {
			if Index > 0 {
				result += "-"
			}
			result += IndexToChar[Int((Buffer[Index] & 0x0000001f) >>  0)]
			result += IndexToChar[Int((Buffer[Index] & 0x000003e0) >>  5)]
			result += IndexToChar[Int((Buffer[Index] & 0x00007c00) >> 10)]
			result += IndexToChar[Int((Buffer[Index] & 0x000f8000) >> 15)]
		}
		
		return result
	}
	
	func compressBytes(buffer :NSData) -> UInt32 {
		var ID :UInt32 = 0
		do {
			try ID ^= getUInt32FromNSDataBuffer(buffer: buffer, start: 1)
			try ID ^= getUInt32FromNSDataBuffer(buffer: buffer, start: 0)
		} catch {
		}
		
		return ID
	}
}



