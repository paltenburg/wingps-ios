//
//  StActManager.swift
//  WinGPS Marine
//
//  Created by Standaard on 27/08/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation

public class StActManager {
	
//	public final static int SUBTYPE_INAPP = 0;
//	public final static int SUBTYPE_WEEKLY = 1;
//	public final static int SUBTYPE_MONTHLY = 2;
//	public final static int SUBTYPE_3MONTHLY = 3;
//	public final static int SUBTYPE_6MONTHLY = 4;
//	public final static int SUBTYPE_YEARLY = 5;
//
//	// only for debugging purposes
//	private final static boolean DEBUG_FORCE_MARINE_PLUS = false;
//	private final static boolean DEBUG_FORCE_MARINE_LITE = false;
//
	static let CHARTCHECKPREFSNAME = "ActDB"
	static let CHECKDB_PRODUCTCODE = "ProductCode" // formerly: CHECKDB_ACTCODE
	static let CHECKDB_PRODUCTCODECOUNT = "ActCodeCount" // : CHECKDB_ACTCODECOUNT
	static let CHECKDB_ACTENABLED = "ActCodeEnabled"
	static let CHECKDB_ACTSUBTYPE = "ActSubType"
	static let CHECKDB_ACTTIMED = "ActTimed"
	static let CHECKDB_EMAIL = "Email"
	static let CHECKDB_FILECOUNT = "FileCount"
	static let CHECKDB_FILE = "File";
	static let CHECKDB_PASS = "PasswordNew"
	static let CHECKDB_USERID = "UserID"

//	private static final byte[] SALT = new byte[] {
//	80, -12, -79, 48, -79, -107, 6, 36, 101, 90, -63, -62, -53, 106, -43, 51, 116, -32, 9, 123
//	};
//
	// singleton to return the same activation manager every time
	static var mActManager :StActManager? = nil

	//	private PreferenceObfuscator mActDB = null;
	var mProductCodeDB :UserDefaults? = nil // TODO: use encryption for these preferences
	
//	private int mAllowedWaterbodies;
	var mInitialized :Bool = false

	// The ProductId and Module of the chartset that is used in this app.
	// If this is an app that supports multiple chart sets use PID == -1 and MODULE == 999.
	var PID :Int = Resources.pid
	var MODULE :Int = Resources.module
	
	static func getActManager() -> StActManager {
		if StActManager.mActManager == nil {
			StActManager.mActManager = StActManager()
		}
		return StActManager.mActManager!
	}

	/**
	* Add an activation code to the activation database.
	*/
	func addProductCode(pid :Int, mod :Int, enabled :Bool, subType :Int) {
		guard mInitialized, let ProductCodeDB = mProductCodeDB
			else { return }
		
		var NewCount :Int = getProductCodeCount() + 1
				
		// Product code
		var key :String = StActManager.CHECKDB_PRODUCTCODE + "_" + String(NewCount - 1)
		var value :String = String(pid) + "_" + String(mod)
		ProductCodeDB.set(value, forKey: key)
		
		// Enabled
		key = StActManager.CHECKDB_ACTENABLED + "_" + String(NewCount - 1)
		value = enabled ? "1" : "0"
		ProductCodeDB.set(value, forKey: key)
		
		// Subscription type
		key = StActManager.CHECKDB_ACTSUBTYPE + "_" + String(NewCount - 1)
		ProductCodeDB.set(String(subType), forKey: key)
		
		ProductCodeDB.set(String(NewCount), forKey: StActManager.CHECKDB_PRODUCTCODECOUNT)
		
		ProductCodeDB.synchronize()
	}

//	/**
//	* Old deprecated method for marine and marine+
//	*/
//	@Deprecated
//	public void addProduct(String aPackageName, String aHWKey) {
//	if (!mInitialized)
//	return;
//
//	String value = aHWKey + "_" + String.valueOf(System.currentTimeMillis());
//	mActDB.putString(aPackageName, value);
//	mActDB.commit();
//	}
//
//	/**
//	* Old deprecated method for marine and marine+
//	*/
//	@Deprecated
//	public boolean checkProduct(String aPackageName, String aHWKey) {
//	if (!mInitialized)
//	return false;
//
//	String result = mActDB.getString(aPackageName, "");
//	if (result.isEmpty())
//	return false;
//
//	// todo: check time and hwkey
//	//    String value = aHWKey + "_" + String.valueOf(System.currentTimeMillis());
//	return true;
//	}
//
//	/**
//	* Adds the activation details of a product to the database.
//	*
//	* @param aPackageName The package name of the product.
//	* @param aHWKey The hardware key of the device for which the activation is valid.
//	* @param aActCode The activation code.
//	* @param aProductID The product id of the product.
//	*/
//	public void addProductAct(String aPackageName, String aHWKey, String aActCode, int aProductID) {
//	if (!mInitialized)
//	return;
//
//	String value = aHWKey + "_" + aActCode + "_" + String.valueOf(aProductID) + "_" + String.valueOf(System.currentTimeMillis());
//	mActDB.putString(aPackageName, value);
//	mActDB.commit();
//	}

	/**
	* Returns ActCode_ProductId (for older apps this returns ActCode only).
	*/
	func checkProductAct(aPackageName :String, aHWKey :String?) -> String {
		if (!mInitialized) {
			return ""
		}
		
		let result :String = mProductCodeDB!.string(forKey: aPackageName) ?? ""
		
		if result.isEmpty {
			return ""
		}
		
		var values :[String] = result.splitToStringArray(by: "_")
		if values.count < 3 {
			return ""
		}
		
		var Result :String = values[1]
		if values.count == 4 {
			Result += "_" + values[2]
		}
		
		// todo: check time and hwkey
		
		return Result
	}

	/**
	* Returns ActCode_ProductId (or ActCode when aActOnly is true, for older apps always returns ActCode only).
	*/
	func checkProductAct(aPackageName :String, aHWKey :String, aActOnly :Bool) -> String {
//	if (!mInitialized)
//		return "";
//
//	String result = mActDB.getString(aPackageName, "");
//	if (result.isEmpty())
//		return "";
//
//	String values[] = result.split("_");
//	if (values.length < 3)
//		return "";
//
//	String Result = values[1];
//	if (values.length == 4 && !aActOnly)
//	Result += "_" + values[2];
//
//	// todo: check time and hwkey
//
//	return Result;
		return ""
	}

//	/**
//	* Returns current used ProductId.
//	*/
//	public int checkProductAppID(String aPackageName, String aHWKey, int aDefaultID) {
//	//    return aDefaultID;
//	if (DEBUG_FORCE_MARINE_PLUS) return 69633;
//	if (DEBUG_FORCE_MARINE_LITE) return 1;
//
//	if (!mInitialized)
//	return aDefaultID;
//
//	String value = mActDB.getString(aPackageName, "");
//	if (value.isEmpty())
//	return aDefaultID;
//
//	String values[] = value.split("_");
//	if (values.length != 4)
//	return aDefaultID;
//
//	int Result = aDefaultID;
//	try {
//	Result = Integer.valueOf(values[2]);
//	} catch (NumberFormatException e) {
//	}
//	return Result;
//	}
	
	/**
	* Clears all file references for a product from the database.
	*
	* @param aPid The ProductId of the product.
	* @param aMod The Module of the product.
	*/
	func clearFiles(aPid :Int, aMod :Int) {
		let count = getFileNameCount(aPid: aPid, aMod: aMod)
		for i in 0 ..< count {
			let key = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILE + String(i)
			mProductCodeDB?.removeObject(forKey: key)
		}
		if (count > 0) {
			mProductCodeDB?.removeObject(forKey: String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILECOUNT)
			mProductCodeDB?.synchronize()
		}
	}

	/**
	* Checks whether a chartset contains the given filename.
	*
	* @param aPid The product id in which to check.
	* @param aMod The module in which to check.
	* @param aFilename The filename to find.
	* @return Returns true if the filename exists in the given product.
	*/
	func containsFilename(aPid :Int, aMod :Int, aFilename :String) -> Bool {
		
		if PID == -1 && MODULE == 999 {
			var count :Int = getFileNameCount(aPid: aPid, aMod: aMod)
			var fname :String = String()
			for i in 0 ..< count {
				fname = getFileName(aPid: aPid, aMod: aMod, aIndex: i)
				if fname == aFilename {
					return true
				}
			}
		} else {
			if aPid != PID {
				return false
			}
			
			if (aMod & MODULE) == aMod {
				var count :Int = getFileNameCount(aPid: PID, aMod: MODULE)
				var fname :String
				for i in 0 ..< count {
					fname = getFileName(aPid: PID, aMod: MODULE, aIndex: i)
					if fname.count != 0 && fname == aFilename {
						return true
					}
				}
			}
		}
		return false;
	}

//	public boolean containsFilename(int aPid, String aFilename) {
//
//	if (aPid != PID)
//	return false;
//
//	int count = getFileNameCount(PID, MODULE);
//	String fname;
//	for (int i = 0; i < count; i++) {
//	fname = getFileName(PID, MODULE, i);
//	if (fname.compareTo(aFilename) == 0)
//	return true;
//	}
//	return false;
//	}

	/**
	* Delete a product code from the database.
	*/
	func deleteProductCode(aPid :Int, aMod :Int) {
		
		if !mInitialized { return }
		
		let count = getProductCodeCount()
		var productCodes = [ChartProduct]()
		for i in 0 ..< count {
			let CP = ChartProduct()
			getActCode(aIndex: i, aCP: CP)
			
			// Controleren of CP wel geset is (gepassed by reference)
			
			var key = StActManager.CHECKDB_PRODUCTCODE + "_" + String(i)
			mProductCodeDB!.removeObject(forKey: key)
			key = StActManager.CHECKDB_ACTENABLED + "_" + String(i)
			mProductCodeDB!.removeObject(forKey: key)
			key = StActManager.CHECKDB_ACTSUBTYPE + "_" + String(i)
			mProductCodeDB!.removeObject(forKey: key)
			
			// do not add the product we want to delete
			if (CP.Pid == aPid && CP.Mod == aMod) {
				continue
			}
			productCodes.append(CP)
		}
		mProductCodeDB!.set("0", forKey: StActManager.CHECKDB_PRODUCTCODECOUNT)
		mProductCodeDB!.synchronize()
		for i in 0 ..< productCodes.count {
			addProductCode(pid: productCodes[i].Pid, mod: productCodes[i].Mod, enabled: productCodes[i].Enabled, subType: productCodes[i].SubType)
		}
		
		// clean up chart visibility settings
		
		for CDI in DKW2ChartManager.getChartManager().mCharts {
			if let tag = CDI.calibratedChart.dKW2Chart?.tag,
				tag.productID == aPid,
				tag.moduleBitIndex == aMod,
				let chartGUID = tag.guid.toString(),
				let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)
			{
				chartVisibilitySettings.removeObject(forKey: chartGUID)
				chartVisibilitySettings.synchronize()
			}
		}
	}
	
//	public void deleteProduct(String aPackageName) {
//	if (!mInitialized)
//	return;
//
//	mActDB.remove(aPackageName);
//	mActDB.commit();
//	}
//
//	public void enableActCode(int aPid, int aMod, boolean aEnabled) {
//	if (!mInitialized)
//	return;
//
//	int i = findProductCode(aPid, aMod);
//	if (i == -1)
//	return;
//
//	String key = CHECKDB_ACTENABLED + "_" + String.valueOf(i);
//	String value = (aEnabled) ? "1" : "0";
//	mActDB.putString(key, value);
//	mActDB.commit();
//	}
	
	/**
	* Initializes the activation database. This should be done immediately after creating the database.
	*
	* @param aContext Context needed for initializing.
	*/
//	func initialize(Context aContext) {
	func initialize() {
		if !mInitialized {
//			let HWKey = StHardwareKey()
			
//			SharedPreferences sp = aContext.getSharedPreferences(CHARTCHECKPREFSNAME, Context.MODE_PRIVATE);
			let sp = UserDefaults(suiteName: StActManager.CHARTCHECKPREFSNAME)
			
			// TODO: use encryption for these preferences
//			mActDB = new PreferenceObfuscator(sp, new AESObfuscator(SALT, aContext.getPackageName(), HWKey.getDeviceKey()));
			mProductCodeDB = sp
			
			// TODO: make PID and MODULE dynamic
//			PID = aContext.getResources().getInteger(R.integer.pid);
//			MODULE = aContext.getResources().getInteger(R.integer.module);
//			PID = 196661
//			MODULE = 255
			
			PID = Resources.pid
			MODULE = Resources.module
			
			mInitialized = true;
	

		}
	}
	
//	/**
//	* Used to determine if the app is a chart app or not.
//	*
//	* @return Returns true if this app is a chart app, false is this app is a Marine app.
//	*/
//	public boolean isChartApp() {
//	return !(PID == -1 && MODULE == 999);
//	}
//
//	/**
//	* Used to determine if the given ProductId is a chart product.
//	*
//	* @param aPid The ProductId to check.
//	* @return Returns true if the given ProductId is a chart product.
//	*/
//	public boolean isChartProduct(int aPid) {
//	return (aPid & 0x20000) == 0x20000;
//	}

	/**
	* Checks if a product code for the given ProductId and Module mask is present in the database.
	*
	* @return Returns the index in the activation database of a found activation. Returns -1 if no activation could be found or if the activation database has not been initialized.
	*/
	func findProductCode(pid :Int, mod :Int) -> Int {
		if !mInitialized {
			return -1
		}

		var count :Int = getProductCodeCount()
		var CP :ChartProduct = ChartProduct()
		for i in 0 ..< count {
			getActCode(aIndex: i, aCP: CP)
			if CP.Mod == mod && CP.Pid == pid {
				return i
			}
		}
		return -1
	}
	
	func getActCode( aPid :Int, aMod :Int) -> String {
		if !mInitialized {
			return ""
		}

        let key :String = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_PRODUCTCODE
//		return mActDB.getString(key, "")
        return mProductCodeDB!.string(forKey: key) ?? ""
	}

	/**
	* Returns the chartproduct data at the given index in the database.
	*/
	func getActCode(aIndex :Int, aCP :ChartProduct) {
		if (!mInitialized) { return }
		
		var key = StActManager.CHECKDB_PRODUCTCODE + "_" + String(aIndex)
		let CP :String = mProductCodeDB!.string(forKey: key) ?? ""
		if CP.count == 0 { return }
		
		let parts = CP.split(separator: "_")
		if parts.count < 2 || parts.count > 6 { return }
		
		aCP.Pid = Int(parts[0])!
		aCP.Mod = Int(parts[1])!

		key = StActManager.CHECKDB_ACTENABLED + "_" + String(aIndex)
		let En = mProductCodeDB!.string(forKey: key) ?? "1"
		aCP.Enabled = En == "1"
		
		key = StActManager.CHECKDB_ACTSUBTYPE + "_" + String(aIndex)
		let SubType = mProductCodeDB!.string(forKey: key) ?? "0"
		aCP.SubType = Int(SubType)!
	}

	/**
	* Returns the number of activations in the database.
	*/
	func getProductCodeCount() -> Int {
		guard mInitialized, let productDB = mProductCodeDB else {
			return 0
		}
		
		return Int(productDB.string(forKey: StActManager.CHECKDB_PRODUCTCODECOUNT) ?? "0") ?? 0
	}

	/**
	* Return the filename of a product at the specificed index. Use {@link #getFileNameCount(int, int)} to find out how many files the product contains.
	* @param aPid The ProductId of the product.
	* @param aMod The Module of the product.
	* @param aIndex The index of the file to find.
	* @return The filename at the given index or an empty string if the filename does not exist (when the index is out of bounds for example).
	*/
	func getFileName(aPid :Int, aMod :Int, aIndex :Int) -> String {
		guard mInitialized, let mActDB = mProductCodeDB else {
			return ""
		}

		let key :String = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILE + String(aIndex)
		return mActDB.string(forKey: key) ?? ""
	}

	/**
	* Returns how many times the given filename occurs in all chart products
	*
	* @param aFilename The filename to find.
	* @return Returns the number of times a given filename occurs.
	*/
	func getFilenameCount(aFilename :String) -> Int {
		if (!mInitialized){
			return 0
		}

		var result :Int = 0
		var count :Int = getProductCodeCount()
		
		for i in 0 ..< count {
			var CP :ChartProduct = ChartProduct()
			getActCode(aIndex: i, aCP: CP)
			if containsFilename(aPid: CP.Pid, aMod: CP.Mod, aFilename: aFilename){
				result += 1
			}
		}
		
		return result;
	}

	/**
	* Returns how many files are in the given chart product.
	*/
	func getFileNameCount(aPid :Int, aMod :Int) -> Int {
		if (!mInitialized){
			return 0
		}

		var key = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILECOUNT
		
		//// debug ////
		print("getFileNameCount \(key) \(mProductCodeDB?.string(forKey: key) ?? "<value not found>")")
		
		return Int(mProductCodeDB?.string(forKey: key) ?? "0") ?? 0
	}

	/**
	* Returns true if all the files belonging to the given chart product exist.
	*/
	func getProductComplete(aPid :Int, aMod :Int) -> Bool {
		
//	StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
//	StDocumentFile path = DKWSettings.getDKW2ChartsDir();
//	if (!path.exists())
//	return false;

		var FileCount :Int = getFileNameCount(aPid: aPid, aMod: aMod)
		for j in 0 ..< FileCount {
			var filename :String = getFileName(aPid: aPid, aMod :aMod, aIndex: j)

			//			if !new StDocumentFile(path, filename).exists() {
			//				return false
			//			}
//			let foundFile :URL? = URL(fileURLWithPath: Resources.DKW2ChartsDir + (filename))
			let foundFile :URL? = getDKW2Directory().appendingPathComponent(filename)

			if filename.count == 0 || !FileManager.default.fileExists(atPath: foundFile?.path ?? "") {
				return false
			}
		}
		return true
	}

//	/**
//	* @return The number of subscriptions stored in the database.
//	*/
//	public int getSubsCount() {
//	if (!mInitialized)
//	return 0;
//
//	int Result = 0;
//	int Count = getActCodeCount();
//	ChartProduct CP = new ChartProduct();
//	for (int i = 0; i < Count; i++) {
//	getActCode(i, CP);
//	if (CP.SubType != 0)
//	Result++;
//	}
//	return Result;
//	}

	/**
	* Returns the folder and filename of the waterway network for this app.
	*
	* @return Path and filename of waterway network
	*/
    func getWWFilePath() -> String {
//	String WWFilename;
//	int AppId = checkProductAppID(aContext.getPackageName(), StHardwareKey.getHardwareKey(aContext), aContext.getResources().getInteger(R.integer.appid));
//	if (AppId == aContext.getResources().getInteger(R.integer.DKW_NEDERLAND_BINNEN)) {
//	WWFilename = Environment.getExternalStorageDirectory() + "/stentec/899925/";
//	} else {
//	WWFilename = Environment.getExternalStorageDirectory() + "/stentec/";
//	}
//	if (aIncludeName)
//	WWFilename += "WaterwayNetwork.stwwn";
//	return WWFilename
        return "WaterwayNetwork.stwwn"
	}

	/**
	* Returns true if user data has been set in the activation database.
	* @return True, if user data has been set and the activation database has been initialized.
	*/
	func getUserDataAvailable() -> Bool {
		guard let mActDB = mProductCodeDB else {return false}
		return (mInitialized && mActDB.object(forKey: StActManager.CHECKDB_EMAIL) != nil && mActDB.object(forKey: StActManager.CHECKDB_PASS) != nil)
	}
	
	func getUserID() -> String {
		
		if !mInitialized {
				return ""
		}
		
		return mProductCodeDB!.string(forKey: StActManager.CHECKDB_USERID) ?? ""
	}

	/**
	* Return the email address of the logged in user.
	* @return Email address of the logged in user or an empty string if there is no logged in user or the activation database has not been initialized.
	*/
	func getUserMail() -> String {
		if !mInitialized { return "" }

		return mProductCodeDB!.string(forKey: StActManager.CHECKDB_EMAIL) ?? ""
	}

//	/**
//	* Return the Base64 encoded email address of the logged in user.
//	* @return The Base64 encoded email address of the logged in user or an empty string if there is no logged in user or the activation database has not been initialized.
//	*/
//	public String getUserMail64(int aUid, String googleUserID) {
//	if (!mInitialized)
//	return "";
//
//	String Mail = mActDB.getString(CHECKDB_EMAIL, "");
//	if (Mail.isEmpty())
//	return "";
//
//	Mail = String.valueOf(aUid) + "_" + Mail;
//
//	if (googleUserID != null && googleUserID != "")
//	Mail += "_" + googleUserID;
//
//	return Base64.encode(Mail.getBytes());
//	}

	func getUserPass() -> String {
		if !mInitialized { return "" }
		
		return mProductCodeDB!.string(forKey: StActManager.CHECKDB_PASS) ?? ""
	}
	
//	public boolean isInitialized() {
//	return mInitialized;
//	}

	func resetProducts() {
		if  !mInitialized {
			return
		}
		
		let count = getProductCodeCount()
		for i in 0 ..< count {
			var CP :ChartProduct = ChartProduct()
			getActCode(aIndex: i, aCP: CP)
			
			var key :String = StActManager.CHECKDB_PRODUCTCODE + "_" + String(i)
			mProductCodeDB!.removeObject(forKey: key)
			key = StActManager.CHECKDB_ACTENABLED + "_" + String(i)
			mProductCodeDB!.removeObject(forKey: key)
		}
		mProductCodeDB!.set("0", forKey: StActManager.CHECKDB_PRODUCTCODECOUNT)
		mProductCodeDB!.synchronize()
	}

	func resetUserData() {
		if !mInitialized {
			return
		}
		
		mProductCodeDB?.removeObject(forKey: StActManager.CHECKDB_EMAIL)
		mProductCodeDB?.removeObject(forKey: StActManager.CHECKDB_PASS)
		mProductCodeDB?.synchronize()
	}

//	public void setActCode(int aPid, int aMod, String aActCode) {
//	if (!mInitialized)
//	return;
//
//	String key = String.valueOf(aPid) + "_" + String.valueOf(aMod) + "_" + CHECKDB_ACTCODE;
//	mActDB.putString(key, aActCode);
//	mActDB.commit();
//	}

	func setUserData(aEmail :String, aPassword :String) {
		
		if !mInitialized {
			return
		}

		mProductCodeDB!.set(aEmail, forKey: StActManager.CHECKDB_EMAIL)
		mProductCodeDB!.set(aPassword, forKey: StActManager.CHECKDB_PASS)
		mProductCodeDB!.synchronize()
	}

//	public void setUserID(String aID) {
//	if (!mInitialized)
//	return;
//
//	mActDB.putString(CHECKDB_USERID, aID);
//	mActDB.commit();
//	}
	
	func updateFiles(aPid :Int, aMod :Int, aFiles :[String]) {
		if (!mInitialized) {
			return
		}
		
		clearFiles(aPid: aPid, aMod: aMod)
		
		if aFiles.count > 0 {
			var key = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILECOUNT
			mProductCodeDB!.set(String(aFiles.count), forKey: key)
			
			for i in 0 ..< aFiles.count {
				key = String(aPid) + "_" + String(aMod) + "_" + StActManager.CHECKDB_FILE + String(i)
				mProductCodeDB!.set(aFiles[i], forKey: key)
			}
			mProductCodeDB!.synchronize()
		}
	}

	init() {
		mInitialized = false
        
        initialize()
	}

	class ChartProduct {
		var Pid :Int = 0
		var Mod :Int = 0
		var SubType :Int = 0
//		var ActCode :String? = nil
//		var TimedStr :String = ""
//		var PidU :Int = -1
		var Enabled :Bool = false
	}
}
