//
//  StDKW2MapTile.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2MapTile {
    
    let zoomLevel :Int
    let x :Int
    let y :Int
    
    var fileHandle :DKWFileHandle?
    var offset :UInt64
    var size :Int
    var width :Int
    var height :Int
    var transparentColor :UInt32
    var includeAlpha :Bool
    
    init(fileHandle :DKWFileHandle, offset :UInt64, size :Int, width :Int, height :Int, transparentColor :UInt32, includeAlpha :Bool) {
        
        self.zoomLevel = 15
        self.x = 0
        self.y = 0
        
        self.fileHandle = fileHandle
        self.offset = offset
        self.size = size
        self.width = width
        self.height = height
        self.transparentColor = transparentColor
        self.includeAlpha = includeAlpha
    }
}
