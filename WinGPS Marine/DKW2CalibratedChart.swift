//
//  DKW2CalibratedChart.swift
//  MapDemo
//
//  Created by Standaard on 31/10/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

class DKW2CalibratedChart {
	
	var cal :GeodeticCalibration?
	var centerGC :GreatCirclePoint?
	var centerGeo :Geo2?
	var centerScale :Double?
	var dKW2Chart :DKW2Chart?
	var corners :[Geo2] = [Geo2](generating: {_ in Geo2()}, count: 4)
	var radius :Double?
	
	
	init(_ DKW2Chart :DKW2Chart) {
		cal = nil
		centerGC = nil
		centerGeo = nil
		centerScale = 0
		dKW2Chart = DKW2Chart
		corners = [Geo2](generating: {_ in Geo2()}, count: 4)
		radius = 0
		calibrateFromTag()
	}
	
	func getCalibration() -> GeodeticCalibration {
		return cal!
	}
	
	//    public GreatCirclePoint getCenterGC() {
	//    return mCenterGC;
	//    }
	//
	//    public Geo2 getCenterGeo() {
	//    return mCenterGeo;
	//    }
	//
	//    public double getCenterScale() {
	//    return mCenterScale;
	//    }
	
	func getCorner(_ aIndex :Int) -> Geo2 {
	    return corners[aIndex]
	}
	
	//    public StDKW2Chart getDKW2Chart() {
	//    return mChart;
	//    }
	
	// zoomFactor in DKW2 units; chartpixels per screenpixel
	func getMipmapLevel(_ zoomFactor :Double) -> Int {
//        ///zoomFactorTmp can give a scaled down version of the zoomfactor
////		let zoomFactorTmp = zoomFactor * 0.95
////		let zoomFactorTmp = zoomFactor * 0.5
//        let zoomFactorTmp = zoomFactor
//		var result :Int = 0
//		if (zoomFactorTmp < 1) {
//			let invZoom :Double = 1.0 / zoomFactorTmp
//			let mipmapLevel :Double = log(invZoom) / log(2)
//			//			result = min(Int(floor(mipmapLevel)), chart!.mipmapCount - 1)
//			result = min(Int(floor(mipmapLevel)), dKW2Chart!.mipmapCount - 1)
//		}
        
        let zoomFactorTmp = zoomFactor * 1.5
        var result :Int = 0
        let invZoom :Double = 1.0 / zoomFactorTmp
        let mipmapLevel :Double = log(invZoom) / log(2)
        //            result = min(Int(floor(mipmapLevel)), chart!.mipmapCount - 1)
        result = min(Int(floor(mipmapLevel)), dKW2Chart!.mipmapCount - 1)
        result = max(0, result)

        return result
	}
	
	//    public double getRadius() {
	//    return mRadius;
	//    }
	
	//	func isVisible(final GreatCirclePoint ViewerCenter, double ViewerRadius, final StGeodeticCalibration ViewerCalibration, final StGeodeticCalibration SecondViewerCalibration, zoom :inout Double) -> Bool {
	func isVisible(viewerCalibration :GeodeticCalibration , zoom :inout Double) -> Bool {
		
		zoom = 0.0
		var overlap = false
		
		// Check if the chart is inside the viewer using a fast but inaccurate check.
		//    if (GeodeticsGreatCircle.distAlongGreatCircle(ViewerCenter, mCenterGC) >= ViewerRadius + mRadius)
		//       return false;
		
		let ChartPix1 = Double2(x: -0.5, y: -0.5)
		let ChartPix2 = Double2(x: Double(cal!.width) - 0.5, y: -0.5)
		let ChartPix3 = Double2(x: Double(cal!.width) - 0.5, y: Double(cal!.height) - 0.5)
		let ChartPix4 = Double2(x: -0.5, y: Double(cal!.height) - 0.5)
		
		var ChartPoly = Array(generating: {_ in PolyLineD()}, count: 4)
		
		ChartPoly[0].coord = cal!.pixTransform(dstCal: viewerCalibration, srcPix: ChartPix1) 
		ChartPoly[1].coord = cal!.pixTransform(dstCal: viewerCalibration, srcPix: ChartPix2)
		ChartPoly[2].coord = cal!.pixTransform(dstCal: viewerCalibration, srcPix: ChartPix3)
		ChartPoly[3].coord = cal!.pixTransform(dstCal: viewerCalibration, srcPix: ChartPix4)
		Math.polygonInitialize(Poly: &ChartPoly, Count: 4)
		
		/*
		Double2 ViewerPix0 = mCal.pixTransform(ViewerCalibration, ChartPix1);
		Double2 ViewerPix1 = mCal.pixTransform(ViewerCalibration, ChartPix2);
		Double2 ViewerPix2 = mCal.pixTransform(ViewerCalibration, ChartPix3);
		Double2 ViewerPix3 = mCal.pixTransform(ViewerCalibration, ChartPix4);
		*/
		let Dist01 = Math.dist(A: ChartPoly[0].coord, B: ChartPoly[1].coord)
		let Dist12 = Math.dist(A: ChartPoly[1].coord, B: ChartPoly[2].coord)
		let Dist23 = Math.dist(A: ChartPoly[2].coord, B: ChartPoly[3].coord)
		let Dist30 = Math.dist(A: ChartPoly[3].coord, B: ChartPoly[0].coord)
		if max(Dist01, Dist23) < 0.5 || max(Dist12, Dist30) < 0.5 {
			return false
		}
		
		// check viewer rectangle
		let viewerRectangle = RectangleD()
		viewerRectangle.XMin = -0.5
		viewerRectangle.YMin = -0.5
		viewerRectangle.XMax = Double(viewerCalibration.width) - 0.5
		viewerRectangle.YMax = Double(viewerCalibration.height) - 0.5
		
		overlap = Math.rectOverlap(R: viewerRectangle, Count: 4, Poly: ChartPoly)
		
		//		TODO:
		//		// in case of circular maps, check viewercalibration on other side of the map.
		//		if (SecondViewerCalibration != null){
		//
		//			ChartPoly[0].Coord = mCal.pixTransform(SecondViewerCalibration, ChartPix1);
		//			ChartPoly[1].Coord = mCal.pixTransform(SecondViewerCalibration, ChartPix2);
		//			ChartPoly[2].Coord = mCal.pixTransform(SecondViewerCalibration, ChartPix3);
		//			ChartPoly[3].Coord = mCal.pixTransform(SecondViewerCalibration, ChartPix4);
		//			StMath.polygonInitialize(ChartPoly, 4);
		//
		//			overlap |= StMath.rectOverlap(ViewerRectangle, 4, ChartPoly);
		//		}
		
		if !overlap {
			return false
		}
		
		zoom = max(max(Dist01 / Double(cal!.width),
							Dist12 / Double(cal!.height)),
					  max(Dist23 / Double(cal!.width),
							Dist30 / Double(cal!.height)))
		
		return true
	}
	
	func posInChart(aPos :CLLocationCoordinate2D, aIncludeBorder :Bool) -> Bool {
		let Pix :Double2 = cal!.geoWGS84ToPix(srcGeo: aPos)
		if (!aIncludeBorder && dKW2Chart!.boundsTag.getCount() > 0) {
			return dKW2Chart!.boundsTag.pointInPolygon(Point: Pix)
		} else {
			return (Pix.x >= -0.5 && Pix.x < Double(dKW2Chart!.width) - 0.5 &&
				Pix.y >= -0.5 && Pix.y < Double(dKW2Chart!.height) - 0.5)
		}
	}
	
//	    @Override
//	    public String toString() {
//	    return mChart.toString()
//	    }
	
	func calibrateFromTag() {
		let tag = dKW2Chart!.tag
		let pix1 = tag.calPix1
		let pix2 = tag.calPix2
		let pix3 = tag.calPix3
		let proj1 = tag.calProj1
		let proj2 = tag.calProj2
		let proj3 = tag.calProj3
		let pointCount = tag.calPointCount
		
		cal = GeodeticCalibration()
		guard let cal = cal else { return }
		guard let chart = dKW2Chart else { return }
		
		if pointCount == 2 {
			cal.initialize2(calPix1: pix1, calPix2: pix2, calProj1: proj1, calProj2: proj2, width: chart.width, height: chart.height, ref: chart.tag.ref)
		} else if pointCount == 3 {
			cal.initialize3(calPix1: Double2(pix1), calPix2: Double2(pix2), calPix3: Double2(pix3), calProj1: Double2(proj1), calProj2: Double2(proj2), calProj3: Double2(proj3), width: chart.width, height: chart.height, ref: chart.tag.ref)
		} else {
			return
		}
		
		// Calculate center of chart
		var pix = cal.getPixCenter()
		centerGeo = cal.pixToGeoRadWGS84(pix)
		guard let centerGeo = centerGeo else { return }
		
		// Calculate great-circle point of center
		centerGC = GreatCirclePoint.geoToGreatCirclePoint(centerGeo)
		
		// Calc viewer radius
		var geo = Geo2()
		pix = cal.getPixTopLeft()
		geo = cal.pixToGeoRadWGS84(pix)
		let DistTL = GeodeticsGreatCircle.distAlongGreatCircle(geoRad1: centerGeo, geoRad2: geo)
		corners[0] = geo
		pix = cal.getPixTopRight()
		geo = cal.pixToGeoRadWGS84(pix)
		let DistTR = GeodeticsGreatCircle.distAlongGreatCircle(geoRad1: centerGeo, geoRad2: geo)
		corners[1] = geo
		pix = cal.getPixBottomLeft()
		geo = cal.pixToGeoRadWGS84(pix)
		let DistBL = GeodeticsGreatCircle.distAlongGreatCircle(geoRad1: centerGeo, geoRad2: geo)
		corners[2] = geo
		pix = cal.getPixBottomRight()
		geo = cal.pixToGeoRadWGS84(pix)
		let DistBR = GeodeticsGreatCircle.distAlongGreatCircle(geoRad1: centerGeo, geoRad2: geo)
		corners[3] = geo
		radius = max(max(DistTL, DistTR), max(DistBL, DistBR))
		centerScale = cal.getScaleMax(pix: pix)
	}
}
