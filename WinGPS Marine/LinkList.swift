//
//  LinkList.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class LinkInfo {
    var linkType :ELinkType?
    var link = String()
    var description = String()
}

class StLinkList {
    var list = [LinkInfo]()
    
    func add(type :ELinkType, link :String, description :String) -> Int? {
        var linkInfo = LinkInfo()
        linkInfo.linkType = type
        linkInfo.link = link
        linkInfo.description = description
        list.append(linkInfo)
        return list.count - 1
        
    }
}


enum ELinkType {
    case FILE
    case URL
}
