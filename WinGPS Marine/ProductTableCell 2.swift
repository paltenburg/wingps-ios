//
//  ProductTableCell.swift
//  WinGPS Marine
//
//  Created by Standaard on 13/12/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class ProductTableCell :UITableViewCell {
	
	@IBOutlet weak var chartCheckMarkImage: UIImageView!
	@IBOutlet weak var chartSetTitleLabel: UILabel!
	@IBOutlet weak var chartSetSubTitleLabel: UILabel!
	@IBOutlet weak var downloadProgress: UIProgressView!
	@IBOutlet weak var downloadProgressLabel: UILabel!
	
	@IBOutlet weak var chartSetSizeLabel: UILabel!
	@IBOutlet weak var checkMarkWidthConstraint: NSLayoutConstraint!
	
	@IBOutlet weak var downloadButtonOutlet: UIView!
	
	@IBOutlet weak var removeButtonOutlet: UIView!
	@IBOutlet weak var pauzeButtonOutlet: UIButton!
	@IBOutlet weak var stopButtonOutlet: UIButton!
	@IBOutlet weak var pauzeStopButtonOutlet: UIView!
    @IBOutlet weak var busyIndicatorOutlet: UIView!
    
	@IBOutlet weak var downloadProgressView: UIView!
	@IBOutlet weak var chartCheckMarkView: UIView!
	
	var controller :DownloadManagerController?
	
	var CSI :DownloadManagerController.ChartSetItem?
	
	var currentlyDownloading :String?
	
	//	var codedString :String?
	
	var pid = 0
	var mod = 0
	
	@IBAction func downloadButton(_ sender: UIButton) {
		controller?.downloadByCell(self)
	}
	
	@IBAction func pauseButton(_ sender: Any) {
	}
	
	@IBAction func stopButton(_ sender: Any) {
		
//	    let alert = UIAlertController(title: "dialog_title_cancel_download".localized, message: "dialog_message_cancel_download".localized, preferredStyle: .alert)
            let alert = UIAlertController(title: "dialog_message_cancel_download".localized, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
		alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {action in
			DownloadService.service.cancelDownload(pid: self.pid, mod: self.mod)
			
			self.controller?.remove(cell: self)
			//				self.updateCellState(state: .NotDownloaded)
		}))
		controller?.presentFromMain(alert, animated: true)
	}
	
	@IBAction func removeButton(_ sender: Any) {
		
//        let alert = UIAlertController(title: "dialog_title_remove_product".localized, message: "dialog_message_confirm_remove_product".localized, preferredStyle: .alert)
        let alert = UIAlertController(title: "dialog_message_confirm_remove_product".localized, message: nil, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
		alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {action in self.controller?.remove(cell: self)}))
		controller?.presentFromMain(alert, animated: true)
	}
	
	func updateCellState(state :DownloadManagerController.ProductState){
		
		checkMarkWidthConstraint.constant = 25
		
		
		chartCheckMarkImage.isHidden = true
		
		downloadProgressView.isHidden = true
		pauzeStopButtonOutlet.isHidden = true
		
		self.CSI?.state = state
		
		switch state {
		case .NotDownloaded:
			downloadButtonOutlet.isHidden = false
			
			removeButtonOutlet.isHidden = true
			downloadProgress.isHidden = true
			downloadProgressLabel.isHidden = true
			pauzeButtonOutlet.isHidden = true
			stopButtonOutlet.isHidden = true
            
            busyIndicatorOutlet.isHidden = true
		case .Incomplete:
			removeButtonOutlet.isHidden = false
			
			downloadButtonOutlet.isHidden = true
			downloadProgress.isHidden = true
			downloadProgressLabel.isHidden = true
			pauzeButtonOutlet.isHidden = true
			stopButtonOutlet.isHidden = true
            
            busyIndicatorOutlet.isHidden = true

		case .Complete:
			removeButtonOutlet.isHidden = false
			
			checkMarkWidthConstraint.constant = 60
			chartCheckMarkView.isHidden = false
			chartCheckMarkImage.isHidden = false
			downloadButtonOutlet.isHidden = true
			downloadProgress.isHidden = true
			downloadProgressLabel.isHidden = true
			pauzeButtonOutlet.isHidden = true
			stopButtonOutlet.isHidden = true
			pauzeStopButtonOutlet.isHidden = true
            
            busyIndicatorOutlet.isHidden = true

		case .Downloading:
			downloadProgressView.isHidden = false
			downloadProgress.isHidden = false
			downloadProgressLabel.isHidden = false
			
			
			//			if currentlyDownloading != nil {
			stopButtonOutlet.isHidden = false
			//			} else {
			//				stopButtonOutlet.isHidden = true
			//			}
			pauzeStopButtonOutlet.isHidden = false
			downloadButtonOutlet.isHidden = true
			removeButtonOutlet.isHidden = true
			//pauzeButtonOutlet.isHidden = false
            
            busyIndicatorOutlet.isHidden = true

			
		case .Busy:
            busyIndicatorOutlet.isHidden = false

			downloadProgress.isHidden = true
			downloadProgressLabel.isHidden = true
			stopButtonOutlet.isHidden = true
			downloadButtonOutlet.isHidden = true
			removeButtonOutlet.isHidden = true
			pauzeButtonOutlet.isHidden = true
			
		}
		
		
	}
}
