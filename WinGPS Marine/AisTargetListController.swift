//
//  AisTargetListController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 04/09/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class AisTableCell :UITableViewCell {
    
    @IBOutlet weak var classLabelView: UILabel!
    @IBOutlet weak var flagLabelView: UILabel!
    @IBOutlet weak var MMSINameLabelView: UILabel!
    @IBOutlet weak var sogLabelView: UILabel!
    @IBOutlet weak var rangeLabelView: UILabel!
    
    var controller :AisTargetListController?
    var aisObject :AisObject?
}

class AisTargetListController : UIViewController, UITableViewDelegate {

    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nrOfTargetsView: UILabel!
    
    var aisTableCells :[AisTableCell] = [AisTableCell]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        tableView.delegate = self
        tableView.dataSource = self
        
        Nmea.service.overlay?.aisTargetListController = self
    }
    
    override func viewDidLoad() {
        viewContainer.layer.cornerRadius = 8
        viewContainer.clipsToBounds = true
        
//        tableView.register(AisTableCell.self, forCellReuseIdentifier: "aisTableCell")
        nrOfTargetsView.text = String(Nmea.service.aisObjects.count)
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
                self.dismiss(animated: false, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if touch?.view == backgroundView {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    // Function to call from ais overlay when list needs to be refreshed
    func refreshList(){
        tableView.reloadData()
        nrOfTargetsView.text = String(Nmea.service.aisObjects.count)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let aisCell = tableView.cellForRow(at: indexPath) as? AisTableCell,
           let aisObject = aisCell.aisObject,
           let vc = ViewController.instance {
            
            let aisAnnotation = aisObject.getAisAnnotation()
            
            vc.selectAisTarget(aisAnnotation)
            
            performSegue(withIdentifier: "unwindSegueToMapview", sender: self)

            if var position = aisAnnotation.aisObject.position{
                var lonSpanTargetInfo = (Double(vc.aisTargetInfoView.bounds.width) / Double(vc.mapView.bounds.width)) * Double(vc.mapView.region.span.longitudeDelta)
                position.longitude -= lonSpanTargetInfo / 2
            
                vc.moveMapTo(coord: position, animated: true)
            }
            
        }
    }
}

extension AisTargetListController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var aisCount = Nmea.service.aisObjects.count
        aisTableCells = [AisTableCell](repeating: AisTableCell(), count: aisCount)
        return aisCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var aisTableCell :AisTableCell
        var row = indexPath.row
        var aisObject = Nmea.service.aisObjects[row]
        
        if let oldTableCell = tableView.dequeueReusableCell(withIdentifier: "aisTableCell", for: indexPath) as? AisTableCell {
            aisTableCell = oldTableCell
        } else {
            
            aisTableCell = AisTableCell()

        }
        aisTableCell.controller = self
        aisTableCell.aisObject = aisObject
        
        aisTableCell.classLabelView.text = aisObject.aisClass ?? ""
        aisTableCell.flagLabelView.text = aisObject.registration?.flag ?? ""
        aisTableCell.MMSINameLabelView.text = (aisObject.name ?? "").count > 1 ? aisObject.name : String(aisObject.mmsi)
        aisTableCell.sogLabelView.text = StUtils.formatSpeed(aisObject.sog != nil ? Double(aisObject.sog!) : nil) ?? ""
        
        aisTableCell.rangeLabelView.text = StUtils.formatDistance(aisObject.getDistance(userLocation: ViewController.instance?.userLocation?.coordinate)) ?? "-"

        return aisTableCell
    }
}

