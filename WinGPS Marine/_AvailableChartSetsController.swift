////
////  AvailableChartSetsView.swift
////  WinGPS Marine
////
////  Created by Standaard on 12/09/2018.
////  Copyright © 2018 Stentec. All rights reserved.
////
//
//import Foundation
//import UIKit
//
////class testTableViewController :UITableViewController{
////	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
////		return 3
////	}
////
////	override func tableView(_ tableView: UITableView,
////									cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////
////		let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableChartItem", for: indexPath)
////
////		cell.textLabel?.text = String(indexPath.row)
////		//		cell.detailTextLabel?.text = player.game
////		return cell
////	}
////}
//
////// Call Available-charts-screen from code
////func _showAvailableChartSetsScreen(caller :StatusCheck?){
////	// update UITableView
////
////	let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
////
////	//			let availableChartSetsController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsControllerID") as! AvailableChartSetsController
////	let availableChartSetsNavController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsNavControllerID") as! UINavigationController
////
////	let downloadManagerController = availableChartSetsNavController.viewControllers[0] as! DownloadManagerController
////
////	if caller != nil {
////		availableChartSetsController.mStatusCheck = caller
////	}
////
////	//			ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: {() -> Void in self.buttonDownloadClick()})
////	ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: nil)
////}
//
//class _DownloadManagerController :UITableViewController{
//
//	enum EChartProductState {
//		case PRODUCTSTATEAVAILABLE   // Product is available to download
//		case PRODUCTSTATECOMPLETE    // Product is activated and complete
//		case PRODUCTSTATEINCOMPLETE  // Product is activated but not complete
//	}
//
//	//	private boolean mDualPane;
//	var mStatusCheck :StatusCheck? = nil
//	var mChartCheckSt :StChartCheckTask?
//	var mChartCheckHandler : ((Int, String) -> Void)?
//	var mErrorCode :Int? = nil
//	var mActDB :StActManager = StActManager.getActManager()
//	var mCharts = [ChartSetItem]()
//	//	private Activity mActivity = null;
//	//	private Context mContext = null;
//	//	private Resources mRes;
//	//	private TextView mTextViewDesc;
//	//	private TextView mTextViewShop;
//	//	private AvailableChartSetsAdapter mAdapter;
//	var mUser :String = ""
//	var mPass :String = ""
//	//	//private boolean mCheckPem;
//	//	private ListView mListView;
//	//	private boolean mInternet = true;
//	//	private boolean mFirstDownload;
//	//
//	//	@Override
//	//	public void onActivityResult(int RequestCode, int ResultCode, Intent data) {
//	//
//
//	func callBackFromStatusCheck(){
//		//		if (RequestCode == AppConstants.ACTIVITY_STATUSCHECKACCOUNT) {
//		//			if (ResultCode == Activity.RESULT_OK) {
//		//				boolean Success = data.getBooleanExtra("Success", false);
//		//				if (Success) {
//		//					String cdata = data.getStringExtra("Data");
//		//					Intent resultIntent = new Intent();
//		//					resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERRESULT, ChartManagerActivity.CHARTMANAGERRESULT_RELOADCHARTS);
//		//					resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERDATA, cdata);
//		//					getActivity().setResult(ChartManagerActivity.RESULT_OK, resultIntent);
//		//					getActivity().finish();
//		//				}
//		//			}
//
////		mCaller.callBackFromChartManager()
//
//	}
//
//	//		} else if (RequestCode == AppConstants.ACTIVITY_USERACCOUNT) {
//	//			if (ResultCode == Activity.RESULT_OK) {
//	//				String Data[] = data.getStringArrayExtra(UserAccountActivity.UARESULT);
//	//				if (Data.length != 2 || Data[0].length() == 0 || Data[1].length() == 0) {
//	//				} else {
//	//					mTextViewDesc.setText(mRes.getString("availablesets_searching));
//	//
//	//					mUser = Data[0];
//	//					mPass = Data[1];
//	//
//	//					mActDB = StActManager.getActManager();
//	//					mActDB.setUserData(mUser, mPass);
//	//
//	//					getAvailableChartSets(mUser, mPass);
//	//					//                    mCheckPem = true;
//	//					//                    mChartCheckSt.checkPemFile();
//	//				}
//	//			} else {
//	//				getActivity().finish();
//	//			}
//	//		} else if (RequestCode == AppConstants.ACTIVITY_SHOPACTIVITY) {
//	//			getActivity().finish();
//	//			//        } else if (RequestCode == AppConstants.INTENT_REQUEST_LOLLIPOP_SDCARD_PERMISSION) {
//	//			//
//	//			//            StDKW2Settings.getSettings(getActivity()).onSDCardPermissionResult(ResultCode, data);
//	//		} else if (RequestCode == AppConstants.INTENT_REQUEST_LOLLIPOP_SDCARD_PERMISSION) {
//	//			StDKW2Settings.getSettings(getActivity()).onSDCardPermissionResult(ResultCode, data);
//	//		}
//	//	}
//
//	//	override convenience init(style: UITableViewStyle) {
//	//		self.init(style: .plain)
//	//	}
//
//	var mSelections = [Bool]()
//
////	required init?(coder aDecoder: NSCoder) {
////		super.init(coder: aDecoder)
////		//			fatalError("init(coder:) has not been implemented")
////	}
//
//	//	func onActivityCreated() {
////	func manualInit(caller :StatusCheck?, user :String, pass: String) {
////	override func viewDidLoad() {
//
//	override func viewWillAppear(_ animated: Bool) {
//
//		//		super.onActivityCreated(savedInstanceState);
//		//		super.init(nibName: nil, bundle: nil)
//
////		super.init(style: UITableViewStyle.plain)
//
//		print("AvailableChartSetsController:manualInit()")
//
//		mActDB = StActManager.getActManager()
//
//		if !mActDB.getUserDataAvailable() {
//			return
//		}
//
//		mUser = mActDB.getUserMail()
//		mPass = mActDB.getUserPass()
//
//		//		// Check if info frame is available
//		//		View infoFrame = getActivity().findViewById(R.id.fragments_ChartInfo);
//		//		mDualPane = infoFrame != null;// && infoFrame.getVisibility() == View.VISIBLE;
//		//
//		//		mActivity = getActivity();
//		//		if (mContext == null && mActivity != null)
//		//		mContext = mActivity.getApplicationContext();
//		//
//		//		mRes = getActivity().getResources();
//		//
//		//		mListView = (ListView)getView().findViewById(R.id.lvAvailableSets);
//		//		mTextViewShop = (TextView)getView().findViewById(R.id.tvAvailableSetsShopLink);
//		//		mTextViewShop.setPaintFlags(mTextViewShop.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//		//		mTextViewDesc = (TextView)getView().findViewById(R.id.tvAvailableSetsDesc);
//		//
//		//		mTextViewShop.setOnClickListener(new View.OnClickListener() {
//		//		public void onClick(View v) {
//		//		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(mRes.getString("action_availablesets_link))));
//		//		}
//		//		});
//		//
//		//		mTextViewDesc.setText(mRes.getString("availablesets_searching));
//
//		mChartCheckHandler = { (Msg, Data) -> Void in
//			self.chartCheckHandleMessages(Msg: Msg, Data: Data)
//		}
//
//		//		try {
//		mChartCheckSt = StChartCheckTask(aHandler: mChartCheckHandler)
//
//		//		} catch (Exception e) {
//		//			Log.e("AvailableChartSets", "Exception: " + e.getMessage());
//		//		}
//		//
//		//		//mCheckPem = false;
//		//		mFirstDownload = false;
//		//
//		//		//StDKW2ChartManager.getChartManager().getChartsInChartsDir();
//		//
//		////		mActDB = StActManager.getActManager()
//		//		mCharts = new ArrayList<>();
//		//
//		//		mListView.setCacheColorHint(Color.TRANSPARENT);
//		//		mListView.setOnItemClickListener(new OnItemClickListener() {
//		//
//		//			@Override
//		//			public void onItemClick(AdapterView<?> aParent, View aView, int aPosition, long aId) {
//		//				ActivityCompat.invalidateOptionsMenu(getActivity());
//		//			}
//		//		});
//		//
//		//		mAdapter = new AvailableChartSetsAdapter(getActivity(), R.layout.chartset_item);
//		//		mListView.setAdapter(mAdapter);
//		//		mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//		//
//		//		mInternet = StUtils.checkNetworkConnection(getActivity());
//		buttonGetAvailableSets()
//	}
//
//	@IBAction func close(_ segue: UIStoryboardSegue) {
//	}
//
//	@IBAction func downloadButton(_ sender: UIBarButtonItem) {
//
//		if !mSelections.isEmpty {
//			var selectedIndexes = [Int]()
//			for (i, e) in mSelections.enumerated() {
//				if e { selectedIndexes.append(i) }
//			}
//			buttonDownloadClick(itemIndexes: selectedIndexes)
//		}
//	}
//
//	@IBAction func removeButton(_ sender: UIBarButtonItem) {
//	}
//
//	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		return mCharts.count
//	}
//
//	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//		let cell = tableView.dequeueReusableCell(withIdentifier: "AvailableChartItem", for: indexPath)
//
//		let chartSet = mCharts[indexPath.row]
//		//		if chartSet.itemType == 0 {
//		//			cell.textLabel?.text = chartSet.pName
//		//		} else {
//		//			cell.detailTextLabel?.text = "\t"+(chartSet.mName ?? "")
//		//		}
//
//		cell.textLabel?.text = chartSet.pName
//		cell.detailTextLabel?.text = "\t"+(chartSet.mName ?? "")
//
//		//		cell.detailTextLabel?.text = player.game
//		return cell
//	}
//
//	override func tableView(_ tableView :UITableView, didSelectRowAt indexPath :IndexPath) {
//		if mSelections.count <= indexPath.row {
//			mSelections = [Bool](repeating: false, count: mCharts.count)
//		}
//
//		tableView.deselectRow(at: indexPath, animated: true)
//
//		// Other row is selected - need to deselect it
//		//		if let index = mSelectedGameIndex {
//		//			let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
//		//			cell?.accessoryType = .none
//		//		}
//
//		//		let selectedChartset = mCharts[indexPath.row]
//
//		// update the checkmark for the current row
//		let cell = tableView.cellForRow(at: indexPath)
//
//		if mSelections[indexPath.row] {
//			cell?.accessoryType = .none
//			mSelections[indexPath.row] = false
//		} else {
//			cell?.accessoryType = .checkmark
//			mSelections[indexPath.row] = true
//		}
//		//		cell?.accessoryType = .none
//	}
//	//
//	//	@Override
//	//	public void onCreate(Bundle savedInstanceState) {
//	//		super.onCreate(savedInstanceState);
//	//		setHasOptionsMenu(true);
//	//	}
//	//
//	//	@Override
//	//	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//	//		// Inflate the menu; this adds items to the action bar if it is present.
//	//		inflater.inflate(R.menu.available_chart_sets, menu);
//	//
//	//		boolean AccountAvailable = !StActManager.getActManager().getUserDataAvailable();
//	//		boolean DownloadAvailable = false, DeleteAvailable = false;
//	//		if (mListView != null) {
//	//			int Index = mListView.getCheckedItemPosition();
//	//			if (Index != ListView.INVALID_POSITION && Index < mListView.getCount()) {
//	//				ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
//	//				DownloadAvailable = (item.state == EChartProductState.PRODUCTSTATEAVAILABLE || item.state == EChartProductState.PRODUCTSTATEINCOMPLETE);
//	//				DeleteAvailable = (item.state != EChartProductState.PRODUCTSTATEAVAILABLE);
//	//			}
//	//		}
//	//
//	//		MenuItem m = menu.findItem(R.id.menu_availablesets_account);
//	//		//m.setEnabled(AccountAvailable);
//	//		m.setVisible(AccountAvailable);
//	//		m = menu.findItem(R.id.menu_availablesets_download);
//	//		//m.setEnabled(DownloadAvailable);
//	//		m.setVisible(DownloadAvailable);
//	//		m = menu.findItem(R.id.menu_availablesets_moveToSD);
//	//		StDKW2Settings settings = StDKW2Settings.getSettings(getActivity());
//	//		if (settings.sdCardAvailable()) {
//	//			m.setVisible(true);
//	//			if (settings.getUseSDCard())
//	//			m.setTitle("settings_title_button_move_to_intern);
//	//			else
//	//			m.setTitle("settings_title_button_move_to_sd);
//	//			m.setEnabled(true);
//	//		} else {
//	//			m.setVisible(false);
//	//		}
//	//
//	//		m = menu.findItem(R.id.menu_availablesets_delete);
//	//		//m.setEnabled(DeleteAvailable);
//	//		m.setVisible(DeleteAvailable);
//	//
//	//		m = menu.findItem(R.id.menu_availablesets_cleanUp);
//	//		m.setEnabled(true);
//	//	}
//	//
//	//	@Override
//	//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//	//		if (mContext == null && container != null && container.getContext() != null)
//	//		mContext = container.getContext();
//	//		return inflater.inflate(R.layout.fragment_available_chartsets, container, false);
//	//	}
//	//
//	//	@Override
//	//	public boolean onOptionsItemSelected(MenuItem item) {
//	//		if (item.getItemId() == android.R.id.home) {
//	//			// This ID represents the Home or Up button. In the case of this
//	//			// activity, the Up button is shown. Use NavUtils to allow users
//	//			// to navigate up one level in the application structure. For
//	//			// more details, see the Navigation pattern on Android Design:
//	//			//
//	//			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
//	//			//
//	//			NavUtils.navigateUpFromSameTask(getActivity());
//	//			return true;
//	//		} else if (item.getItemId() == R.id.menu_availablesets_account) {
//	//			openAccount();
//	//			return true;
//	//		} else if (item.getItemId() == R.id.menu_availablesets_download) {
//	//			buttonDownloadClick();
//	//			return true;
//	//		} else if (item.getItemId() == R.id.menu_availablesets_moveToSD) {
//	//			buttonMoveToSDClick(item);
//	//			return true;
//	//		} else if (item.getItemId() == R.id.menu_availablesets_delete) {
//	//			deleteSetClick();
//	//			return true;
//	//		} else if (item.getItemId() == R.id.menu_availablesets_cleanUp) {
//	//			cleanUpClick();
//	//			return true;
//	//		}
//	//		return super.onOptionsItemSelected(item);
//	//	}
//	//
//	//	@Override
//	//	public void onPrepareOptionsMenu(Menu menu) {
//	//		boolean AccountAvailable = !StActManager.getActManager().getUserDataAvailable();
//	//		boolean DownloadAvailable = false, DeleteAvailable = false;
//	//		if (mListView != null) {
//	//			int Index = mListView.getCheckedItemPosition();
//	//			if (Index != ListView.INVALID_POSITION && Index < mListView.getCount()) {
//	//				ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
//	//				DownloadAvailable = (item.state == EChartProductState.PRODUCTSTATEAVAILABLE || item.state == EChartProductState.PRODUCTSTATEINCOMPLETE);
//	//				DeleteAvailable = (item.state != EChartProductState.PRODUCTSTATEAVAILABLE);
//	//			}
//	//		}
//	//
//	//		MenuItem m = menu.findItem(R.id.menu_availablesets_account);
//	//		//m.setEnabled(AccountAvailable);
//	//		m.setVisible(AccountAvailable);
//	//		m = menu.findItem(R.id.menu_availablesets_download);
//	//		//m.setEnabled(DownloadAvailable);
//	//		m.setVisible(DownloadAvailable);
//	//		m = menu.findItem(R.id.menu_availablesets_moveToSD);
//	//		StDKW2Settings settings = StDKW2Settings.getSettings(getActivity());
//	//		if (settings.sdCardAvailable()) {
//	//			m.setVisible(true);
//	//			if (settings.getUseSDCard())
//	//			m.setTitle("settings_title_button_move_to_intern);
//	//			else
//	//			m.setTitle("settings_title_button_move_to_sd);
//	//			m.setEnabled(true);
//	//		} else {
//	//			m.setVisible(false);
//	//		}
//	//
//	//		m = menu.findItem(R.id.menu_availablesets_delete);
//	//		//m.setEnabled(DeleteAvailable);
//	//		m.setVisible(DeleteAvailable);
//	//
//	//		m = menu.findItem(R.id.menu_availablesets_cleanUp);
//	//		m.setEnabled(true);
//	//	}
//
//	func getAvailableChartSets(aEMail :String, aPass :String) {
//		//			if (mInternet) { // TODO online check
//		mUser = aEMail;
//		mPass = aPass;
//
//		//				mTextViewDesc.setText(mRes.getString("availablesets_searching));
//		//				ActivityCompat.invalidateOptionsMenu(getActivity());
//		mChartCheckSt?.requestAvailableChartSets(aActDB: mActDB, aUser: aEMail, aPass: aPass)
//
//		//			} else {
//		//				mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
//		//			}
//	}
//
//	//	public void setFirstDownload() {
//	//		mFirstDownload = true;
//	//	}
//
//	func buttonGetAvailableSets() {
//		getAvailableSets()
//	}
//
//	func getAvailableSets() {
//		//			if (!mInternet) {
//		//				mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
//		//			} else {
//		//				Bundle args = getArguments();
//		//				if (args != null) {
//		//			let EMail = args.String(forKey: "User")
//		//			let Pass = args.String(forKey: "Pass")
////		mChartCheckSt?.requestAvailableChartSets(aActDB: mActDB, aUser: mUser, aPass: mPass)
//		//				} else {
//							if mActDB.getUserDataAvailable() {
//								mChartCheckSt!.requestAvailableChartSets(aActDB: mActDB, aUser: mActDB.getUserMail(), aPass: mActDB.getUserPass())
//							} else {
////								mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
//								print("buttonGetAvailableSets: No account info available")
//							}
//		//				}
//		//			}
//
//	}
//
//	func buttonDownloadClick(itemIndexes :[Int]) {
//
//		guard itemIndexes.count >= 1 else { return }
//
//		var CheckedItem :String = ""
//		//		int Index = mListView.getCheckedItemPosition();
//		//		if (Index != ListView.INVALID_POSITION) {
//		//			ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
//
//		// TODO for now: only download the first chart set
//		let item :ChartSetItem = mCharts[itemIndexes[0]]
//
//		CheckedItem = String(item.productID ?? 0) + "_" + String(item.moduleBitIndex ?? 0)
//		if item.isSub ?? false {
//			CheckedItem += "_1"
//		}
//
//		//		}
//
//		//		if (CheckedItem.length() > 0) {
//		//			if (mFirstDownload) {
//		//				// This is used when no charts are present (activated) in the app
//		//				Intent resultIntent = new Intent();
//		//				resultIntent.putExtra(AvailableChartSetsActivity.CSRESULT, CheckedItem);
//		//
//		//				getActivity().setResult(Activity.RESULT_OK, resultIntent);
//		//				getActivity().finish();
//		//			} else {
//		//				Intent intent = new Intent(getActivity(), StatusCheckActivityAccountNew.class);
//		//				intent.putExtra(AvailableChartSetsActivity.CSRESULT, CheckedItem);
//		//				startActivityForResult(intent, AppConstants.ACTIVITY_STATUSCHECKACCOUNT);
//
////((ErrorCode, Bool, String) -> ())
//
//		StatusCheck.runFromChartManager(aViewController: self, callBackHandler: nil, checkedItem: CheckedItem)
//
//		//			}
//		//		}
//
//	}
//
//	//	private void buttonMoveToSDClick(final MenuItem item) {
//	//
//	//		final Context context = getActivity();
//	//
//	//		SettingsFragmentsAdaptiveActivity.onChartsMoveClickStatic(
//	//			context,
//	//			new Runnable() {
//	//				@Override
//	//				public void run() {
//	//					if (StDKW2Settings.getSettings(context).getUseSDCard()) {
//	//						item.setTitle("settings_title_button_move_to_intern);
//	//					} else {
//	//						item.setTitle("settings_title_button_move_to_sd);
//	//					}
//	//				}
//	//			},
//	//			this);
//	//
//	//
//	//	}
//	//
//
//	func chartCheckHandleMessages(Msg :Int, Data :String) {
//		mErrorCode = Msg
//		/*
//		if (mCheckPem) {
//		if (mErrorCode == StChartCheckTask.CHECKRESULT_OK) {
//		getAvailableChartSets(mUser, mPass);
//		mCheckPem = false;
//		} else {
//		showActError();
//		}
//		return;
//		}
//		*/
//		if mErrorCode == StChartCheckTask.CHECKRESULT_OK {
//			var paramStr :String = Data
//			//			var params :[String] = paramStr.split{$0 == "\n"}.map(String.init) ?? [String]()
//			var params :[String] = paramStr.split{$0 == "\n"}.map(String.init)
//
//			if params.count == 0 {
//				//TODO error msg				Toast.makeText(getActivity(), mRes.getString("statuscheck_acterror_servererror), Toast.LENGTH_LONG).show();
//				return //TODO: finish view
//			}
//
//			var i :Int = 1
//			var chartCount :Int = Int(params[i]) ?? 0
//			i += 1
//
//			mCharts.removeAll()
//			var csi :ChartSetItem
//
//			// nullpointerexception so check activity for null
//			var AppId :Int = 0
//			//			FragmentActivity activity = getActivity();
//			//			if (activity != null) {
//			//				AppId = StActManager.getActManager().checkProductAppID(activity.getPackageName(), StHardwareKey.getHardwareKey(activity), getResources().getInteger(R.integer.appid));
//			//			}
//
//			while i < chartCount * 12 {
//				var pid :Int = Int(params[i + 2]) ?? 0
//				var mod :Int = Int(params[i + 3]) ?? 0
//				var csi = ChartSetItem()
//				csi.itemType = 1
//				csi.pName = params[i]
//				csi.mName = params[i + 1]
//				csi.productID = Int64(params[i + 2])
//				csi.moduleBitIndex = Int(params[i + 3])
//				csi.count = Int(params[i + 4])
//				csi.size = Int64(params[i + 5])
//				csi.isSub = params[i + 6] == "1"
//				csi.expiry = Int64(params[i + 7])
//				csi.isCurrent = params[i + 8] == "1"
//				csi.groupIndex = Int(params[i + 9])
//				csi.orderIndex = Int(params[i + 10])
//				csi.edition = Int(params[i + 11])
//
//				if mActDB.findProductCode(pid: pid, mod: mod) > -1 {
//					csi.state = (mActDB.getProductComplete(aPid: pid, aMod: mod)) ? EChartProductState.PRODUCTSTATECOMPLETE : EChartProductState.PRODUCTSTATEINCOMPLETE;
//				} else {
//					csi.state = EChartProductState.PRODUCTSTATEAVAILABLE;
//				}
//
//				i += 12;
//
//				//				// In case of the VKNL subscription app, filter out non-subscription licenses
//				//				if (mContext != null && AppId == mContext.getResources().getInteger(R.integer.DKW_NEDERLAND_BINNEN) && !csi.isSub)
//				//				continue;
//
//				mCharts.append(csi)
//			}
//
//			//			// sort it
//			//			try{
//			//				Collections.sort(mCharts, new ChartSetComparator());
//			//			} catch (Exception e){
//			//				Log.e("popke", "Chart sorting error");
//			//			}
//
//			mCharts = mCharts.sorted(by: { $0.productID ?? 0 > $1.productID ?? 0 })
//
//			//			// add headers
//			//			var pid :Int64 = -1
//			//			i = 0
//			//			while i < mCharts.count {
//			//				var item : ChartSetItem = mCharts[i]
//			//				if item.productID != pid {
//			//					csi = ChartSetItem()
//			//					csi.itemType = 0
//			//					csi.pName = item.pName
//			//					pid = item.productID ?? 0
//			//					csi.productID = pid
//			//					mCharts.insert(csi, at: i)
//			//					i += 1
//			//				}
//			//				i += 1
//			//			}
//
//			//			// check if the newest products are available for the chart apps
//			//			if (AppId == 899923 || AppId == 899924 || AppId == 899925) {
//			//				boolean found = false;
//			//				for (i = 0; i < mCharts.size(); i++) {
//			//					ChartSetItem item = mCharts.get(i);
//			//					if (item.isCurrent) {
//			//						found = true;
//			//						break;
//			//					}
//			//				}
//			//				if (!found || mCharts.size() == 0) {
//			//					Intent intent = new Intent(activity, IabActivity.class);
//			//					startActivityForResult(intent, AppConstants.ACTIVITY_SHOPACTIVITY);
//			//					return;
//			//				}
//			//			}
//
//			//			if mCharts.count == 0 {
//			//				if mActDB.getActCodeCount() > 0{
//			//					mTextViewDesc.setText(mRes.getString("availablesets_noextrasets))
//			//				} else {
//			//					mTextViewDesc.setText(mRes.getString("availablesets_nosets))
//			//				}
//			//			} else {
//			//				mTextViewDesc.setText(mRes.getString("availablesets_selectset))
//			//			}
//
//			//			AvailableChartSetsAdapter adapter = (AvailableChartSetsAdapter)mListView.getAdapter()
//			//			adapter.notifyDataSetChanged()
//			// TODO update list
//
//			//        getListView().invalidate();
//
////			// update UITableView
////
////			let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
////
////			//			let availableChartSetsController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsControllerID") as! AvailableChartSetsController
////			let availableChartSetsNavController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsNavControllerID") as! UINavigationController
////
////			let availableChartSetsController = availableChartSetsNavController.viewControllers[0] as! AvailableChartSetsController
////
////			availableChartSetsController.availableChartSets = self
////
//////			ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: {() -> Void in self.buttonDownloadClick()})
////			ViewController.instance?.present(availableChartSetsNavController, animated: true, completion: nil)
//
//			self.tableView.reloadData()
//
//			//			let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//			//			let availableChartSetsController = storyBoard.instantiateViewController(withIdentifier: "AvailableChartSetsControllerID") as! AvailableChartSetsController
//			//			availableChartSetsController.mCharts = self.mCharts
//			//			ViewController.instance?.present(availableChartSetsController, animated: true, completion: nil)
//
//		} else {
//			//			showActError()
//		}
//	}
//
//	//	// private boolean checkNetworkConnection() {
//	//	//    ConnectivityManager connMgr = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
//	//	//    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//	//	//     return (networkInfo != null && networkInfo.isConnected());
//	//	// }
//	//
//	//	private void deleteChartsQuestion() {
//	//		Builder cd = new AlertDialog.Builder(getActivity());
//	//		cd.setTitle(getResources().getString("availablesets_deletetitle));
//	//		cd.setMessage(getResources().getString("availablesets_deletechartsmsg));
//	//		cd.setPositiveButton(getResources().getString("button_yes), new DialogInterface.OnClickListener() {
//	//
//	//			@Override
//	//			public void onClick(DialogInterface dialog, int which) {
//	//				deleteSet(false);
//	//			}
//	//		});
//	//		cd.setNegativeButton(getResources().getString("button_no), new DialogInterface.OnClickListener() {
//	//
//	//			@Override
//	//			public void onClick(DialogInterface dialog, int which) {
//	//				deleteSet(true);
//	//			}
//	//		});
//	//		if (!getActivity().isFinishing())
//	//		cd.show();
//	//	}
//	//
//	//	private void deleteSet(boolean aDeleteCharts) {
//	//		int Index = mListView.getCheckedItemPosition();
//	//		if (Index != ListView.INVALID_POSITION) {
//	//			ChartSetItem item = (ChartSetItem)mListView.getItemAtPosition(Index);
//	//			mTextViewDesc.setText("Deleting chartset"/*mRes.getString("availablesets_noaccount)*/);
//	//
//	//			if (aDeleteCharts) {
//	//				StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
//	//				StDocumentFile path = DKWSettings.getDKW2ChartsDir();
//	//
//	//				int filecount = mActDB.getFileNameCount((int)item.productID, item.moduleBitIndex);
//	//				for (int i = 0; i < filecount; i++) {
//	//					String filename = mActDB.getFileName((int)item.productID, item.moduleBitIndex, i);
//	//					StDocumentFile Chart = new StDocumentFile(path, filename);
//	//					if (mActDB.getFilenameCount(filename) < 2) {
//	//						Chart.delete();
//	//						// delete tmp files too
//	//						StDocumentFile tmpFile = new StDocumentFile(path, filename + ".tmp");
//	//						tmpFile.delete();
//	//					}
//	//				}
//	//			}
//	//			mActDB.deleteActCode((int)item.productID, item.moduleBitIndex);
//	//			Intent resultIntent = new Intent();
//	//			resultIntent.putExtra(ChartManagerActivity.CHARTMANAGERRESULT, ChartManagerActivity.CHARTMANAGERRESULT_RELOADCHARTS);
//	//			getActivity().setResult(ChartManagerActivity.RESULT_OK, resultIntent);
//	//			getActivity().finish();
//	//		}
//	//	}
//	//
//	//	private void deleteSetClick() {
//	//		Builder cd = new AlertDialog.Builder(getActivity());
//	//		cd.setTitle(getResources().getString("availablesets_deletetitle));
//	//		cd.setMessage(getResources().getString("availablesets_deletemsg));
//	//		cd.setPositiveButton(getResources().getString("button_yes), new DialogInterface.OnClickListener() {
//	//
//	//			@Override
//	//			public void onClick(DialogInterface dialog, int which) {
//	//				deleteChartsQuestion();
//	//			}
//	//		});
//	//		cd.setNegativeButton(getResources().getString("button_no), new DialogInterface.OnClickListener() {
//	//
//	//			@Override
//	//			public void onClick(DialogInterface dialog, int which) {
//	//			}
//	//		});
//	//		if (!getActivity().isFinishing())
//	//		cd.show();
//	//	}
//	//
//	//	private void cleanUpClick() {
//	//		if (!mInternet) {
//	//			mTextViewDesc.setText(mRes.getString("availablesets_nointernet));
//	//		} else {
//	//			Bundle args = getArguments();
//	//			if (args != null) {
//	//				String EMail = args.getString("User");
//	//				String Pass = args.getString("Pass");
//	//				mChartCheckSt.requestAvailableChartSets(mActDB, EMail, Pass);
//	//			} else {
//	//				if (mActDB.getUserDataAvailable()) {
//	//					mChartCheckSt.requestAvailableChartSets(mActDB, mActDB.getUserMail(), mActDB.getUserPass());
//	//				} else {
//	//					mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
//	//				}
//	//			}
//	//		}
//	//	}
//	//
//	//	private void openAccount() {
//	//		Intent intent = new Intent(getActivity(), UserAccountActivity.class);
//	//		intent.putExtra(UserAccountActivity.UACANCELABLE, true);
//	//		startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
//	//	}
//	//
//	//	private void showActError() {
//	//		Builder alertBuilderConnectError = new AlertDialog.Builder(mActivity);
//	//		if (!this.isAdded() || mContext == null) return; // check if still attached for exception below
//	//		Resources res = mContext.getResources();
//	//		if (mErrorCode == StChartCheckTask.CHECKRESULT_OK)
//	//		alertBuilderConnectError.setTitle(res.getString("statuscheck_connect_title_ok));
//	//		else
//	//		alertBuilderConnectError.setTitle(res.getString("statuscheck_connect_title_fail));
//	//		String errText;
//	//		switch (mErrorCode) {
//	//		case StChartCheckTask.CHECKRESULT_OK:
//	//			errText = res.getString("statuscheck_connect_ok); break;
//	//		case -7: // Unknown product
//	//			errText = res.getString("statuscheck_acterror_unknownproduct); break;
//	//		case 6: // Not activated
//	//			errText = res.getString("statuscheck_acterror_notactivated); break;
//	//		case 10: // No results
//	//			errText = res.getString("statuscheck_acterror_noresults); break;
//	//		case 12: // Chart not found
//	//			errText = res.getString("statuscheck_acterror_chartnotfound); break;
//	//		case 13: // Activation blocked
//	//			errText = res.getString("statuscheck_acterror_actblocked); break;
//	//		case  4: // User not found
//	//			errText = res.getString("statuscheck_connect_unknownuser);
//	//			mTextViewDesc.setText(mRes.getString("availablesets_noaccount));
//	//			break;
//	//		case -4: // Database error
//	//		case -5: // License (de)code error
//	//		case -6: // Hardwarekey error
//	//		case -8: // Invalid license
//	//		case 11: // No licensecode available
//	//			errText = res.getString("statuscheck_acterror_servererror); break;
//	//		case 14: // Connection exists
//	//			errText = res.getString("statuscheck_connect_exists); break;
//	//		case 15: // Connection too soon
//	//			errText = res.getString("statuscheck_connect_too_soon); break;
//	//		case 97: // Timeout
//	//			//errText = res.getString("statuscheck_noconnection);
//	//			mTextViewDesc.setText(mRes.getString("statuscheck_noconnection2));
//	//			return;
//	//		default:
//	//			errText = res.getString("statuscheck_acterror_unknownerror); break;
//	//		}
//	//		alertBuilderConnectError.setMessage(errText);
//	//		alertBuilderConnectError.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//	//
//	//			@Override
//	//			public void onClick(DialogInterface dialog, int which) {
//	//				if (mErrorCode == 4) {
//	//					mActDB.resetUserData();
//	//					Intent intent = new Intent(mActivity, UserAccountActivity.class);
//	//					intent.putExtra(UserAccountActivity.UACANCELABLE, true);
//	//					startActivityForResult(intent, AppConstants.ACTIVITY_USERACCOUNT);
//	//				} else {
//	//					if (mErrorCode != StChartCheckTask.CHECKRESULT_OK)
//	//					mActivity.finish();
//	//				}
//	//			}
//	//		});
//	//		if (!getActivity().isFinishing())
//	//		alertBuilderConnectError.show();
//	//	}
//	//
//	//	private class AvailableChartSetsAdapter extends ArrayAdapter<ChartSetItem> {
//	//
//	//		public AvailableChartSetsAdapter(Context context, int TextViewResourceId) {
//	//			super(context, TextViewResourceId, mCharts);
//	//		}
//	//
//	//		@Override
//	//		public View getView(int Position, View ConvertView, ViewGroup Parent) {
//	//
//	//			ViewContainer vc = null;
//	//
//	//			if (ConvertView == null) {
//	//				LayoutInflater vi = (LayoutInflater)(getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE));
//	//				ConvertView = vi.inflate(R.layout.availableset_item, null);
//	//
//	//				vc = new ViewContainer();
//	//				vc.ivStatus = (ImageView)ConvertView.findViewById(R.id.ivAvailableSetStatus);
//	//				vc.tvInfo = (TextView)ConvertView.findViewById(R.id.tvAvailableSetInfo);
//	//				vc.tvName = (TextView)ConvertView.findViewById(R.id.tvAvailableSetName);
//	//				LinearLayout ll = (LinearLayout)ConvertView.findViewById(R.id.availableSetInfoBox);
//	//				ll.setClickable(false);
//	//
//	//				ConvertView.setTag(vc);
//	//			} else {
//	//				vc = (ViewContainer)ConvertView.getTag();
//	//			}
//	//
//	//			// Android versions prior to honeycomb don't know the activated state in our
//	//			// list_selector, this code sets the activated drawable manually.
//	//			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
//	//				if (((ListView)Parent).getCheckedItemPosition() == Position)
//	//				ConvertView.setBackgroundResource(R.drawable.gradient_bg);
//	//				else
//	//				ConvertView.setBackgroundResource(0);
//	//			}
//	//
//	//			ChartSetItem csi = mCharts.get(Position);
//	//
//	//			if (csi.itemType == 0) {
//	//				vc.tvName.setText(csi.pName);
//	//				vc.tvInfo.setVisibility(View.GONE);
//	//				vc.ivStatus.setVisibility(View.GONE);
//	//				ConvertView.setPadding(0, 0, 0, 0);
//	//			} else {
//	//				vc.tvName.setText(csi.mName);
//	//				vc.tvInfo.setVisibility(View.VISIBLE);
//	//				if (csi.state == EChartProductState.PRODUCTSTATECOMPLETE || csi.state == EChartProductState.PRODUCTSTATEINCOMPLETE) {
//	//					vc.ivStatus.setVisibility(View.VISIBLE);
//	//					if (csi.state == EChartProductState.PRODUCTSTATECOMPLETE) {
//	//						vc.ivStatus.setImageResource(R.drawable.check);
//	//						vc.ivStatus.setContentDescription("Chartset usable");
//	//					} else {
//	//						vc.ivStatus.setImageResource(R.drawable.check_incomplete);
//	//						vc.ivStatus.setContentDescription("Chartset incomplete and not usable");
//	//					}
//	//				} else
//	//				vc.ivStatus.setVisibility(View.GONE);
//	//
//	//				vc.tvInfo.setText(csi.count + " " + mRes.getString("files) + " (" + StUtils.bytesToFileSize(csi.size) + ")");
//	//				//          ConvertView.setBackgroundResource(R.drawable.list_selector2);
//	//				ConvertView.setPadding(43, 0, 0, 0);
//	//			}
//	//
//	//			vc.tvName.setClickable(false);
//	//			vc.tvInfo.setClickable(false);
//	//			vc.ivStatus.setClickable(false);
//	//
//	//			return ConvertView;
//	//		}
//	//
//	//		@Override
//	//		public boolean isEnabled(int aPosition) {
//	//			ChartSetItem csi = mCharts.get(aPosition);
//	//			return csi.itemType == 1;
//	//		}
//	//	}
//	//
//	//	private class ChartSetComparator implements Comparator<ChartSetItem> {
//	//		@Override
//	//		public int compare(ChartSetItem item1, ChartSetItem item2) {
//	//
//	//			if (item1 == item2)
//	//			return 0;
//	//
//	//			if (item1.state == EChartProductState.PRODUCTSTATECOMPLETE && item2.state != EChartProductState.PRODUCTSTATECOMPLETE)
//	//			return -1;
//	//			if (item1.state != EChartProductState.PRODUCTSTATECOMPLETE && item2.state == EChartProductState.PRODUCTSTATECOMPLETE)
//	//			return 1;
//	//
//	//			// check product id
//	//			//            if (item1.productID > item2.productID)
//	//			//                return 1;
//	//			//            else if (item1.productID < item2.productID)
//	//			//                return -1;
//	//			//            // check product id
//	//			if (item1.productID > item2.productID)
//	//			return -1;
//	//			else if (item1.productID < item2.productID)
//	//			return 1;
//	//			else {
//	//				// check module bit
//	//				if (item1.moduleBitIndex > item2.moduleBitIndex)
//	//				return 1;
//	//				else if (item1.moduleBitIndex < item2.moduleBitIndex)
//	//				return -1;
//	//				else {
//	//					return 0;
//	//				}
//	//			}
//	//
//	//		}
//	//	}
//
//	class ChartSetItem {
//		var itemType :Int?
//		var pName :String?
//		var mName :String?
//		var moduleBitIndex :Int?
//		var productID :Int64?
//		var count :Int?
//		var size :Int64?
//		var isSub :Bool?
//		var expiry :Int64?
//		var isCurrent :Bool?
//		var groupIndex :Int?
//		var orderIndex :Int?
//		var edition :Int?
//		var state :EChartProductState?
//	}
//
//	//	private class ViewContainer {
//	//		ImageView ivStatus;
//	//		TextView tvName;
//	//		TextView tvInfo;
//	//	}
//	//
//	//	private class ChartCheckHandlerCallback implements Handler.Callback {
//	//
//	//		@Override
//	//		public boolean handleMessage(Message Msg) {
//	//			chartCheckHandleMessages(Msg);
//	//			return true;
//	//		}
//	//	}
//
//
//	//	super.init(nibName: nil, bundle: nil)
//
//
//}
//
//
