//
//  extensions.swift
//  WinGPS Marine
//
//  Created by Standaard on 18/10/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import MapKit
import UIKit

extension String {
	subscript (i: Int) -> Character {
		return self[index(startIndex, offsetBy: i)]
	}
	subscript (i: Int) -> String {
		return String(self[i] as Character)
	}
	//	subscript (r: Range<Int>) -> String {
	//		let start = index(startIndex, offsetBy: r.lowerBound)
	//		let end = index(startIndex, offsetBy: r.upperBound)
	//		return self[Range(start ..< end)]
	//	}
	
	public func addingPercentEncodingIncludingExtraCharacters() -> String? {
		let finalString = self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)?.replacingOccurrences(of: "&", with: "%26").replacingOccurrences(of: "+", with: "%2B")
		return finalString
	}
	
	func splitToStringArray(by separator :Character) -> [String] {
		return self.split(separator: separator, omittingEmptySubsequences: false).map{ String($0) }
	}
}

extension String {
	var localized: String {
		return NSLocalizedString(self, comment: "")
	}
	
	func subString(from: Int, to: Int) -> String {
		if from >= self.count ||
			from >= to ||
			to > self.count {
			return ""
		}
		
		var result : String
		// range
		let start = self.index(self.startIndex, offsetBy: from)
		let end = self.index(self.startIndex, offsetBy: to)
		let range = start ..< end
		result = String(self[range])
		return result
	}
	
	func subString(fromInt: Int) -> String {
		return subString(from: fromInt, to: self.count)
	}
	
	func split(usingRegex pattern: String) throws -> [String] {
		//### Crashes when you pass invalid `pattern`
		//	let regex = try! NSRegularExpression(pattern: pattern)
		let regex = try NSRegularExpression(pattern: pattern)
		let matches = regex.matches(in: self, range: NSRange(0 ..< self.utf16.count))
		let ranges = [startIndex ..< startIndex] + matches.map{Range($0.range, in: self)!} + [endIndex ..< endIndex]
		return (0 ... matches.count).map{String(self[ranges[$0].upperBound ..< ranges[$0 + 1].lowerBound])}
		
		//Todo: remove empty ones?
	}
	
	func trim() -> String {
//		return self.trimmingCharacters(in: .whitespacesAndNewlines)
		return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
	}
}

extension Data {
	public func toString() -> String? {
		return String(data: self, encoding: String.Encoding.utf8)
	}
}

extension Date {
    func asString() -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        return dateFormatter.string(from: self)
    }
    
	func asString(style: DateFormatter.Style) -> String {
		let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = style

		return dateFormatter.string(from: self)
	}
    
    func getMilliseconds() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

extension MKMapView {
	
	var MERCATOR_OFFSET : Double {
		return 268435456.0
	}
	
	var MERCATOR_RADIUS : Double  {
		return 85445659.44705395
	}
	
	private func stLongitudeToPixelSpaceX(longitude: Double) -> Double {
		return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * Double.pi / 180.0)
	}
	
	private func stLatitudeToPixelSpaceY(latitude: Double) -> Double {
		return round(MERCATOR_OFFSET - MERCATOR_RADIUS * log((1 + sin(latitude * Double.pi / 180.0)) / (1 - sin(latitude * Double.pi / 180.0))) / 2.0)
	}
	
	private func stPixelSpaceXToLongitude(pixelX: Double) -> Double {
		return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / Double.pi;
	}
	
	private func stPixelSpaceYToLatitude(pixelY: Double) -> Double {
		return (Double.pi / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / Double.pi;
	}
	
private func stCoordinateSpan(withMapView mapView: MKMapView, centerCoordinate: CLLocationCoordinate2D, zoomLevel: UInt) -> MKCoordinateSpan {
		
		let centerPixelX = stLongitudeToPixelSpaceX(longitude: centerCoordinate.longitude)
		let centerPixelY = stLatitudeToPixelSpaceY(latitude: centerCoordinate.latitude)
		
		let zoomExponent = Double(20 - zoomLevel)
		let zoomScale = pow(2.0, zoomExponent)
		
		let mapSizeInPixels = mapView.bounds.size
		let scaledMapWidth =  Double(mapSizeInPixels.width) * zoomScale
		let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale
		
		let topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
		let topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
		
		// find delta between left and right longitudes
		let minLng = stPixelSpaceXToLongitude(pixelX: topLeftPixelX)
		let maxLng = stPixelSpaceXToLongitude(pixelX: topLeftPixelX + scaledMapWidth)
		let longitudeDelta = maxLng - minLng;
		
		let minLat = stPixelSpaceYToLatitude(pixelY: topLeftPixelY)
		let maxLat = stPixelSpaceYToLatitude(pixelY: topLeftPixelY + scaledMapHeight)
		let latitudeDelta = -1 * (maxLat - minLat);
		
		let span = MKCoordinateSpan(latitudeDelta: latitudeDelta, longitudeDelta: longitudeDelta)
		return span
	}
	
	func stZoomTo(toCenterCoordinate centerCoordinate:CLLocationCoordinate2D, zoomLevel: UInt) {
		let zoomLevel = min(zoomLevel, 20)
		let span = self.stCoordinateSpan(withMapView: self, centerCoordinate: centerCoordinate, zoomLevel: zoomLevel)
		let region = MKCoordinateRegion(center: centerCoordinate, span: span)
		self.setRegion(region, animated: true)
		
	}
}

extension CLLocationCoordinate2D {
	init(_ geo2 :Geo2){
		self.init(latitude: geo2.lat, longitude: geo2.lon)
	}
}

extension CLLocationCoordinate2D {
	var dms: (latitude: String, longitude: String) {
		return (String(format:"%d° %d' %.3f\" %@",
							Int(abs(latitude)),
							Int(abs(latitude.minutes)),
							abs(latitude.seconds),
							latitude >= 0 ? "N" : "S"),
				  String(format:"%d° %d' %.3f\" %@",
							Int(abs(longitude)),
							Int(abs(longitude.minutes)),
							abs(longitude.seconds),
							longitude >= 0 ? "E" : "W"))
	}
	var dm: (latitude: String, longitude: String) {
		return (String(format:"%d° %.5f\' %@",
							Int(abs(latitude)),
							abs(latitude.minutes),
							latitude >= 0 ? "N" : "S"),
				  String(format:"%d° %.5f\' %@",
							Int(abs(longitude)),
							abs(longitude.minutes),
							longitude >= 0 ? "E" : "W"))
	}
	var d: (latitude: String, longitude: String) {
		return (String(format:"%.7f° %@",
							abs(latitude),
							latitude >= 0 ? "N" : "S"),
				  String(format:"%.7f° %@",
							abs(longitude),
							longitude >= 0 ? "E" : "W"))
	}
}

extension CLLocation {
	func distance(coordinate :CLLocationCoordinate2D) -> Double {
		return distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
	}
}

extension CLLocationCoordinate2D {
	func distance(coordinate :CLLocationCoordinate2D) -> Double {
		return CLLocation(latitude: self.latitude, longitude: self.longitude).distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
	}
}

//Double(userLocation.distance(from: CLLocation(latitude: activeRP.position.latitude, longitude: activeRP.position.longitude))

extension CGPoint {
	func distanceTo(_ p: CGPoint) -> Double {
		let xDist = self.x - p.x
		let yDist = self.y - p.y
		return Double(sqrt(xDist * xDist + yDist * yDist))
	}
}

extension Character {
	var isAscii: Bool {
		return unicodeScalars.allSatisfy { $0.isASCII }
	}
	var asciiValue: UInt32? {
		return isAscii ? unicodeScalars.first?.value : nil
	}
}

extension StringProtocol {
	var ascii: [UInt32] {
		return compactMap { $0.asciiValue }
	}
}

extension UIColor{
	convenience init(rgb: Int) {
		self.init(
			red: CGFloat((rgb >> 16) & 0xFF) / 255.0,
			green: CGFloat((rgb >> 8) & 0xFF) / 255.0,
			blue: CGFloat(rgb & 0xFF) / 255.0,
			alpha: 1
		)
	}
	
	func rgb() -> Int? {
		var fRed : CGFloat = 0
		var fGreen : CGFloat = 0
		var fBlue : CGFloat = 0
		var fAlpha: CGFloat = 0
		if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
			let iRed = Int(fRed * 255.0)
			let iGreen = Int(fGreen * 255.0)
			let iBlue = Int(fBlue * 255.0)
			let iAlpha = Int(fAlpha * 255.0)
			
			//  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
			let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
			return rgb
		} else {
			// Could not extract RGBA components:
			return nil
		}
	}
}

extension UIViewController {
    // force every presenting dialog to do it on the main thread
    func presentFromMain(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil){
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                self.present(viewControllerToPresent, animated: flag, completion: completion)
            }
        } else {
            self.present(viewControllerToPresent, animated: flag, completion: completion)
        }
    }
}
