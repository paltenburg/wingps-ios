//
//  StDKW2UpdateTag.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation


class DKW2UpdateTag {
    var infoText: String = ""
    var timeUTCLastApplied: Date = Date()
    var timeUTCLastChecked: Date = Date()
    var timeUTCUpdate: Date = Date()
    
    func getInfoText() -> String {
        return infoText
    }
    
    func getTimeUTCLastApplied() -> Date {
        return timeUTCLastApplied
    }
    
    func getTimeUTCLastChecked() -> Date {
        return timeUTCLastChecked
    }
    
    func getTimeUTCUpdate() -> Date {
        return timeUTCUpdate
    }
}
