//
//  StDKW2TileTable.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2TileTable {
    private var tiles = [DKW2TileTableEntry]()
    
    func getCount() -> Int {
        return tiles.count
    }
    
    func getHeight(index: Int) -> Int {
        return tiles[index].height
    }
    
    func getOffsetOrg(index: Int) -> Int64 {
        return tiles[index].offsetOrg
    }
    
    func getOffsetUpd(index: Int) -> Int64 {
        return tiles[index].offsetUpd
    }
    
    func getSizeAlphaOrg(index: Int) -> Int {
        return tiles[index].sizeAlphaOrg
    }
    
    func getSizeAlphaUpd(index: Int) -> Int {
        return tiles[index].sizeAlphaUpd
    }
    
    func getSizeRGBOrg(index: Int) -> Int {
        return tiles[index].sizeRGBOrg
    }
    
    func getSizeRGBUpd(index: Int) -> Int {
        return tiles[index].sizeRGBUpd
    }
    
    func getTile(index: Int) -> DKW2TileTableEntry {
        return tiles[index]
    }
    
    func getTileDesc(index: Int, updated: Bool) -> DKW2TileDesc {
        var TileDesc: DKW2TileDesc = DKW2TileDesc()
        
        if updated {
            TileDesc.offset = UInt64(tiles[index].offsetUpd)
            TileDesc.size = tiles[index].sizeRGBUpd + tiles[index].sizeAlphaUpd
        } else {
            TileDesc.offset = UInt64(tiles[index].offsetOrg)
            TileDesc.size = tiles[index].sizeRGBOrg + tiles[index].sizeAlphaOrg
        }
        TileDesc.width = tiles[index].width
        TileDesc.height = tiles[index].height
        TileDesc.tileIndex = index
        
        return TileDesc
    }
    
    func getWidth(index: Int) -> Int {
        return tiles[index].width
    }
    
    func initialize(count: Int, mipmapTable: DKW2MipmapTable) {
        
        // Initialize all tiles and set all sizes to 256
        for _ in 0..<count {
            var TileTableEntry: DKW2TileTableEntry = DKW2TileTableEntry()
            TileTableEntry.width = DKW2TILESIZEX
            TileTableEntry.height = DKW2TILESIZEY
            tiles.append(TileTableEntry)
        }
        
        // Adjust the tile size of all tiles at the bottom and right edges of the chart
        for i in (0..<mipmapTable.getCount()).reversed() {
            var TileIndex = mipmapTable.getFirstTile(i) + mipmapTable.getTilesX(at: i) * (mipmapTable.getTilesY(at: i) - 1)
            for index in (0..<mipmapTable.getTilesX(at: i)).reversed() {
                tiles[TileIndex + index].height = mipmapTable.getHeight(at: i) - DKW2TILESIZEY * (mipmapTable.getTilesY(at: i) - 1)
            }
            
            TileIndex = mipmapTable.getFirstTile(i) + mipmapTable.getTilesX(at: i) - 1
            for _ in (0..<mipmapTable.getTilesY(at: i)).reversed() {
                tiles[TileIndex].width = mipmapTable.getWidth(at: i) - DKW2TILESIZEX * (mipmapTable.getTilesX(at: i) - 1)
                TileIndex += mipmapTable.getTilesX(at: i)
                
            }
            
            
        }
    }
    
    func initFromTileTable(tileTable: DKW2TileTable) {
        for i in (0..<tileTable.getCount()).reversed() {
            var TileTableEntry: DKW2TileTableEntry = DKW2TileTableEntry()
            TileTableEntry.width = tileTable.getTile(index: i).width
            TileTableEntry.height = tileTable.getTile(index: i).height
            tiles.append(TileTableEntry)
        }
    }
    
    func initTileOrg(index: Int, offset: Int64, sizeRGB: Int, sizeAlpha: Int) {
        tiles[index].offsetOrg = offset;
        tiles[index].sizeRGBOrg = sizeRGB;
        tiles[index].sizeAlphaOrg = sizeAlpha;
    }
    
    func initTileUpd(index: Int, offset: Int64, sizeRGB: Int, sizeAlpha: Int) {
        tiles[index].offsetUpd = offset;
        tiles[index].sizeRGBUpd = sizeRGB;
        tiles[index].sizeAlphaUpd = sizeAlpha;
    }
 
    class DKW2TileTableEntry {
        var offsetOrg: Int64 = 0
        var offsetUpd: Int64 = 0
        var sizeRGBOrg: Int = 0
        var sizeAlphaOrg: Int = 0
        var sizeRGBUpd: Int = 0
        var sizeAlphaUpd: Int = 0
        var width: Int = 0
        var height: Int = 0
    }
}
