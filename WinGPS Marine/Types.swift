//
//  Types.swift
//  MapDemo
//
//  Created by Standaard on 03/11/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class Int2 {
	var x = Int(0)
	var y = Int(0)
}

class Double2 {
	var x = Double(0)
	var y = Double(0)
	
	init(){
		x = Double(0)
		y = Double(0)
	}
	
	init(x :Double, y :Double){
		self.x = x
		self.y = y
	}
	
	init(_ arr :[Double]){
		x = arr[0]
		y = arr[1]
	}
	
	init(_ arr :[Double]?){
		x = arr![0]
		y = arr![1]
	}
	
	func getArray() -> [Double] {
		return [x, y]
	}
}


class Double3 {
    var x = Double(0)
    var y = Double(0)
    var z = Double(0)
    
    init(){
    }
    
    init(x :Double, y :Double, z :Double){
        self.x = x
        self.y = y
        self.z = z
    }
    
    init(_ arr :[Double]){
        x = arr[0]
        y = arr[1]
        z = arr[2]
    }
    
    init(_ arr :[Double]?){
        x = arr![0]
        y = arr![1]
        z = arr![2]
    }
    
    init(_ double3 :Double3){
        x = double3.x
        y = double3.y
        z = double3.z
    }
    
    func getArray() -> [Double] {
        return [x, y, z]
    }
}

class Geo2 {
	var lat :Double = 0
	var lon :Double = 0
	
	init() {
	}
    
    init(_ loc :CLLocationCoordinate2D){
        lat = loc.latitude
        lon = loc.longitude
    }
    
    init(_ geo2 :Geo2){
        lat = geo2.lat
        lon = geo2.lon
    }
	
//	func toCLLocationCoordinate2D() -> CLLocationCoordinate2D {
//		return CLLocationCoordinate2D(latitude: lat, longitude: lon)
//	}
}

class GeoPointE6 {
    var lonE6 :Int = 0
    var latE6 :Int = 0
    
    init() {
    }
    
//    init(_ loc :CLLocationCoordinate2D){
//        lat = loc.latitude
//        lon = loc.longitude
//    }
//
//    init(_ geo2 :Geo2){
//        lat = geo2.lat
//        lon = geo2.lon
//    }
    
//    func toCLLocationCoordinate2D() -> CLLocationCoordinate2D {
//        return CLLocationCoordinate2D(latitude: lat, longitude: lon)
//    }
}

class GreatCirclePoint {
	
	var x = Double()
	var y = Double()
	var z = Double()
	
	init(){
	}
	
	init(x :Double, y :Double, z :Double){
		self.x = x
		self.y = y
		self.z = z
	}
	
	static func geoToGreatCirclePoint(_ Geo :Geo2) -> GreatCirclePoint {
		let SinLat :Double = sin(Geo.lat)
		let CosLat :Double = cos(Geo.lat)
		let SinLong :Double = sin(Geo.lon)
		let CosLong :Double = cos(Geo.lon)
		
		let res = GreatCirclePoint()
		res.x = CosLat * CosLong
		res.y = CosLat * SinLong
		res.z = SinLat
		return res
	}
}


class GeodeticsGreatCircle {
	
	static let REARTH :Double = 6371007.181
	static let GC_LINEAR_COSANGLE :Double = 0.99999998768161384
	
	static func distAlongGreatCircle(geoRad1 :Geo2, geoRad2 :Geo2) -> Double {
		var SinLat = sin(geoRad1.lat)
		var CosLat = cos(geoRad1.lat)
		var SinLong = sin(geoRad1.lon)
		var CosLong = cos(geoRad1.lon)
		let A = GreatCirclePoint(x: CosLat * CosLong, y: CosLat * SinLong, z: SinLat)
		
		SinLat = sin(geoRad2.lat)
		CosLat = cos(geoRad2.lat)
		SinLong = sin(geoRad2.lon)
		CosLong = cos(geoRad2.lon)
		let B = GreatCirclePoint(x: CosLat * CosLong, y: CosLat * SinLong, z: SinLat)
		
		return distAlongGreatCircle(A: A, B: B)
	}
	
	static func distAlongGreatCircle(A :GreatCirclePoint, B: GreatCirclePoint) -> Double {
		return REARTH * greatCircleCalcAngle(A: A, B: B);
	}
	
	static func greatCircleCalcAngle(A :GreatCirclePoint, B :GreatCirclePoint) -> Double {
		let CosA = A.x * B.x + A.y * B.y + A.z * B.z
		
		if CosA >= GC_LINEAR_COSANGLE {
			let dx :Double = A.x - B.x
			let dy :Double = A.y - B.y
			let dz :Double = A.z - B.z
			return sqrt(dx * dx + dy * dy + dz * dz)
		} else if CosA > -1 {
			return acos(CosA)
		} else {
			return Double.pi
		}
	}
}

class Rectangle {
	
}

//Struct to store a polygon. Each struct contains information about a point and the line segment to the next point. The
//last point contains information about the line segment from the last to the first point.
class PolyLineD {
	var coord = Double2()
	var XMin :Double = 0
	var YMin :Double = 0
	var XMax :Double = 0
	var YMax :Double = 0
	var dxdy :Double = 0
	var Horz :Bool = false
	var Vert :Bool = false
}

class Rect {
	var left :Int = 0
	var top :Int = 0
	var right :Int = 0
	var bottom :Int = 0
	init(left: Int, top: Int, right: Int, bottom: Int){
		self.left = left
		self.top = top
		self.right = right
		self.bottom = bottom
	}
}

class RectD {
	
	var left = Double()
	var top = Double()
	var right = Double()
	var bottom = Double()
	
	init(){
	}
	
	init(left: Double, top: Double, right: Double, bottom: Double){
		self.left = left
		self.top = top
		self.right = right
		self.bottom = bottom
	}
	
	func getBottomRight() -> Double2 {
		return Double2(x: right, y: bottom)
	}
	
	func getTopLeft() -> Double2 {
		return Double2(x: left, y: top)
	}
	
	func setBottomRight(_ BR :Double2) {
		right = BR.x
		bottom = BR.y
	}
	
	func setTopLeft(_ TL: Double2) {
		left = TL.x
		top = TL.y
	}
}

//Struct to store a polygon. Each struct contains information about a point and the line segment to the next point. The
//last point contains information about the line segment from the last to the first point.
class PolyLineI {
	var Coord :Int2 = Int2()
	var XMin :Int = 0
	var YMin :Int = 0
	var XMax :Int = 0
	var YMax :Int = 0
	var dxdy :Float = 0
	var Horz :Bool = false
	var Vert :Bool = false
}

class RectangleD {
	var XMin = Double()
	var YMin = Double()
	var XMax = Double()
	var YMax = Double()
 
	func getMin() -> Double2 {
		return Double2(x: XMin, y: YMin)
	}
 
	func getMax() -> Double2 {
		return Double2(x: XMax, y: YMax)
	}
	
	func setMin(aMin :Double2) {
		XMin = aMin.x
		YMin = aMin.y
	}
	
	func setMax(aMax :Double2) {
		XMax = aMax.x
		YMax = aMax.y
	}
 
	//	@Override
	//	public String toString() {
	//	return "(" + XMin + ", " + YMin + ") - (" + XMax + ", " + YMax + ")";
	//	}
}

// Array extension to avoid repeating references in an array of objects

extension Array {
	/// Create a new Array whose values are generated by the given closure.
	/// - Parameters:
	///     - count:            The number of elements to generate
	///     - elementGenerator: The closure that generates the elements.
	///                         The index into which the element will be
	///                         inserted is passed into the closure.
	public init(generating elementGenerator: (Int) -> Element, count: Int) {
		self = (0..<count).map(elementGenerator)
	}
}

//class Z {
//	var i: Int = 0
//}
//
//let z = Array(generating: { _ in Z() }, count: 10)
//
//print(z)

class DKWFileHandle {
	var handle :FileHandle
	var lock :Bool = false
	
	init(_ handle :FileHandle){
		self.handle = handle
	}
	
	static func getHandle(_ handle :FileHandle?) -> DKWFileHandle? {
		if let handle = handle {
			return DKWFileHandle(handle)
		} else {
			return nil
		}
	}
}

class SafeData {
	var data :NSData
	
	init(_ data :NSData) {
		self.data = data
	}
	
	func getBytes(_ buffer: UnsafeMutableRawPointer, range: NSRange) throws {
		if data.length < range.location + range.length {
			throw NSError()
		}
		data.getBytes(buffer, range: range)
	}
}

class TileLoading {
	static let queue = DispatchQueue(label: "com.stentec.tileLoading", attributes: .concurrent)
	static let syncQueue = DispatchQueue(label: "com.stentec.tileLoading")
}


class LockWithClosure {
    static var queueCount = 0
    static let useGCD = false
    static let allOnMainThread = false

    
    private var lock = NSLock()
    
    private var queue :DispatchQueue
    
    init(){
        var queueNr = LockWithClosure.queueCount
        LockWithClosure.queueCount += 1
        queue = DispatchQueue(label: "\(queueNr)")
    }
    
    func runLocked(_ command: () -> ()) {

        if LockWithClosure.useGCD {
            queue.sync{ command() }
        } else if LockWithClosure.allOnMainThread {
            lock.lock()
            DispatchQueue.main.sync{
                command()
            }
            lock.unlock()
        } else {
            // use NSLock

            lock.lock()
            command()
            lock.unlock()
        }
    }
}

class LockWithClosureMainThread {
    static var queueCount = 0
    
    private var lock = NSLock()
    
    private var queue :DispatchQueue
    
    private var globalCommand: () -> ()
    
    init(){
        var queueNr = LockWithClosure.queueCount
        LockWithClosure.queueCount += 1
        queue = DispatchQueue(label: "\(queueNr)")
        globalCommand = {}
    }
    
    func runLocked(_ command: @escaping () -> ()) {
//        self.lock.lock()
//        self.globalCommand = command
        
        DispatchQueue.main.async{
//            self.globalCommand()
            
            self.lock.lock()
            command()
            self.lock.unlock()
        }
        
        //        lock.unlock()
        
    }
}

//class PlaceholderLockWithClosure {
//    func runLocked(_ command: () -> ()) {
//        command()
//    }
//}

class StaticQueues {
	static let tileLocking = DispatchQueue(label: "com.stentec.tileLocking")
	static let tileDecoding = DispatchQueue(label: "com.stentec.tileDecoding", attributes: .concurrent)
	static let tileCachingLock = DispatchQueue(label: "com.stentec.tileCaching")
	static let fileLoadingLock = DispatchQueue(label: "com.stentec.fileLoading")
	static let tileDrawingLock = DispatchQueue(label: "com.stentec.tileDrawing")
	static let tileLoading = DispatchQueue(label: "com.stentec.tileLoading", attributes: .concurrent)
	static let tileRendererDrawLock = DispatchQueue(label: "com.stentec.tileRendererDrawLock")

//    static let wpAnnotationsLock = DispatchQueue(label: "com.stentec.wpAnnotationsLock")
    static let wpAnnotationsLock = LockWithClosure()
//    static let mapviewAnnotationsLock = DispatchQueue(label: "com.stentec.mapviewAnnotationsLock")
    
    
    static let mapviewAnnotationsLock = LockWithClosureMainThread()
//    static let mapviewAnnotationsLock = LockWithClosure()
    // use same lock to keep access to mapview overlays safe (since overlays are type of annotation)
    static let mapviewOverlaysLock = mapviewAnnotationsLock
    static let networkOverlaysLock = mapviewAnnotationsLock


//	static let mapviewAnnotationsLock = DispatchQueue(label: "com.stentec.mapviewAnnotationsLock")
	static let wpAnnotationsBGQueue = DispatchQueue(label: "com.stentec.wpAnnotationsBGQueue", attributes: .concurrent)

//	static let aisAnnotationsLock = DispatchQueue(label: "com.stentec.aisAnnotationsLock")
	static let aisAnnotationsBGQueue = DispatchQueue(label: "com.stentec.aisAnnotationsBGQueue", attributes: .concurrent)
	static let nmeaStreamReadingLock = DispatchQueue(label: "com.stentec.nmeaStreamReadingLock")
	static let aisUpdateQueueLock = DispatchQueue(label: "com.stentec.aisUpdateQueueLock")
    static let aisUpdateDrawListLock = DispatchQueue(label: "com.stentec.aisUpdateDrawListLock")

    static let aisObjectsLock = LockWithClosure()
    
    static let networkLoadingQueue = DispatchQueue(label: "com.stentec.networkLoadingQueue", attributes: .concurrent)
    

}

enum DialogResult {
	case ResultOk
	case ResultCancelled
}

class StTrackPolyline :MKPolyline, Codable {
	var isDashed :Bool = false
}

enum AppType {
	case lite 
	case plus
    case dkw1800
    case friesemeren
}

enum ColorCompatibility {
    static var systemBackground: UIColor {
        if #available(iOS 13, *) {
            return .systemBackground
        }
        return UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 1.0)
    }
    static var secondarySystemBackground: UIColor {
        if #available(iOS 13, *) {
            return .secondarySystemBackground
        }
        return UIColor(red: 242.0, green: 242.0, blue: 247.0, alpha: 1.0)
    }
}

//struct pointeredBuffer {
//    var data :NSData
//    var positions :UInt32 = 0
//    init(data :NSData){
//        self.data = aData
//    }
//}
