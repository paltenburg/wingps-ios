//
//  DKW2TileOverlayRenderer.swift
//  MapDemo
//
//  Created by Standaard on 29/06/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

class DKW2TileOverlayRenderer : MKTileOverlayRenderer {
	
	let tile_overlay: DKW2TileOverlay
	var zoom_scale: MKZoomScale?
	var mFillBackground = true
	//    let cache = NSCache()
	
	//    override init(overlay: MKOverlay) {
	//        self.tile_overlay = overlay as! DKW2TileOverlay
	//        super.init(overlay: overlay)
	//        //        NotificationCenter.defaultCenter().addObserver(self, selector: #selector(saveEditedTiles), name: "com.Coder.Wander.reachedMaxPoints", object: nil)
	//    }
	
	override init(overlay: MKOverlay) {
		self.tile_overlay = overlay as! DKW2TileOverlay
		super.init(overlay: overlay)
		//        NotificationCenter.defaultCenter().addObserver(self, selector: #selector(saveEditedTiles), name: "com.Coder.Wander.reachedMaxPoints", object: nil)
	}
	
	//    // There's some weird cache-ing thing that requires me to recall it
	//    // whenever I re-draw over the tile, I don't really get it but it works
	//    override func canDraw(_ mapRect: MKMapRect, zoomScale: MKZoomScale) -> Bool {
	//        self.setNeedsDisplayIn(mapRect, zoomScale: zoomScale)
	//        return true
	//    }
	
	//    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
	//        dout(.tiles, m:"DKW2 renderer draw()")
	//        zoom_scale = zoomScale
	//        let tile_path = self.tilePathForMapRect(mapRect: mapRect, andZoomScale: zoomScale)
	//        let tile_path_string = stringForTilePath(path: tile_path)
	//        //dout(.tiles, m:"redrawing tile: " + tile_path_string)
	//        self.tile_overlay.loadTile(at: tile_path, result: {
	//            data, error in
	//            if error == nil && data != nil {
	//                if let image = UIImage(data: data!) {
	//                    let draw_rect = self.rect(for: mapRect)
	////                    CGContextDrawImage(context, draw_rect, image.cgImage)
	//                    context.draw(image.cgImage!, in: draw_rect)
	//                    var path: [(CGMutablePath, CGFloat)]? = nil
	////                    self.tile_overlay.point_buffer.readPointsWithBlockAndWait({ points in
	////                        let total = self.getPathForPoints(points, zoomScale: zoomScale, offset: MKMapPointMake(0.0, 0.0))
	////                        path = total.0
	////                        //dout(.tiles, m:"number of points: " + String(path!.count))
	////                    })
	//                    if ((path != nil) && (path!.count > 0)) {
	//                        //dout(.tiles, m:"drawing path")
	//                        for segment in path! {
	//                            context.addPath(segment.0)
	////                            CGContextSetBlendMode(context, .Clear)
	////                            CGContextSetLineJoin(context, CGLineJoin.Round)
	////                            CGContextSetLineCap(context, CGLineCap.Round)
	////                            CGContextSetLineWidth(context, segment.1)
	////                            CGContextStrokePath(context)
	//                        }
	//                    }
	//                }
	//            }
	//        }
	//        )
	//    }
	
	
	
	override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
		//		dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
		//		UIGraphicsPushContext(context)
		
//		StaticQueues.tileRendererDrawLock.sync {

		let draw_rect = self.rect(for: mapRect)
		
		
		if(mFillBackground){
			context.setFillColor(UIColor.gray.cgColor)
			context.fill(draw_rect)
		}
		
		//		context.setLineWidth(8.0 / zoomScale.magnitude)
		//		context.setStrokeColor(UIColor.yellow.cgColor)
		//		context.move(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y + draw_rect.size.height))
		//		context.addLine(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y))
		//		context.addLine(to: CGPoint(x: draw_rect.origin.x + draw_rect.size.width, y: draw_rect.origin.y))
		//		context.strokePath()
		//		context.drawPath(using: .fillStroke)
		
		//		UIGraphicsPopContext()
		
		//		UIGraphicsPushContext(context)
		
		
		
		//		TileLoading.syncQueue.sync(){
		dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
		
		DKW2GenerateTileByCoords(mkTileBitmapContext: context, zoom: zoomScale, northWest: mapRect.origin.coordinate, southEast: MKMapPoint.init(x: mapRect.origin.x + mapRect.size.width, y: mapRect.origin.y + mapRect.size.height).coordinate, mapRect: mapRect, isBackground: false)
		//		}
		//		UIGraphicsPopContext()
			
//		}
	}
	
	//	override func _draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
	func _draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
		dout(.tiles, "DKW2TileOverlayRenderer:_draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
		
		//		self.tile_overlay.loadTile(at: tile_path, result: {})
		
		let draw_rect = self.rect(for: mapRect)
		
		//        dout(.tiles, m:"draw() mapRect ", mapRect.origin.x, mapRect.origin.y, mapRect.size.width, mapRect.size.height)
		//        dout(.tiles, m:"draw() draw_rect", draw_rect.origin.x, draw_rect.origin.y, draw_rect.size.width, draw_rect.size.height)
		
		//        let rect :CGRect = CGRect(dictionaryRepresentation: mapRect)
		//        context.setStrokeColor(UIColor.blue)
		//    CGContextSetLineWidth(context, 1.0/zoomScale);
		//        context.stroke(rect: rect);
		//
		//        override func draw(_ rect: CGRect) {
		//            let context = UIGraphicsGetCurrentContext()
		
		let coordRegion = MKCoordinateRegion.init(mapRect)
		//        dout(.tiles, m:"coords:", coordRegion.center, coordRegion.span)
		
		let coordPoint = mapRect.origin.coordinate
		//        dout(.tiles, m:"coordPoint: ", coordPoint, "calculated origin: ", coordRegion.center.latitude + 0.5 * coordRegion.span.latitudeDelta, coordRegion.center.longitude - 0.5 * coordRegion.span.longitudeDelta)
		
		
		//		let queue = DispatchQueue(label: "MyArrayQueue")
		//
		//		///// lock thread for debugging purposes
		//		queue.sync() {
		//			DKW2GenerateTileByCoords(zoom: zoomScale, northEast: MKCoordinateForMapPoint(mapRect.origin), southWest: MKCoordinateForMapPoint(MKMapPointMake(mapRect.origin.x + mapRect.size.width, mapRect.origin.y + mapRect.size.height)))
		//		}
		
		UIGraphicsPushContext(context)
		
		context.setFillColor(UIColor.red.cgColor)
		context.fill(draw_rect)
		
		context.setFillColor(UIColor.blue.cgColor)
		context.fill(CGRect(x: 0, y:0, width:200, height:200))
		
		context.setLineWidth(10.0 / zoomScale.magnitude)
		context.setStrokeColor(UIColor.green.cgColor)
		
		//        context.move(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y))
		//        context.addLine(to: CGPoint(x: draw_rect.origin.x + draw_rect.size.width, y: draw_rect.origin.y + draw_rect.size.height))
		
		context.move(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y + draw_rect.size.height))
		context.addLine(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y))
		context.addLine(to: CGPoint(x: draw_rect.origin.x + draw_rect.size.width, y: draw_rect.origin.y))
		
		//        context.addLine(to: CGPoint(x: 0, y: 0))
		context.strokePath()
		context.drawPath(using: .fillStroke)
		
		//context.setTextDrawingMode(<#T##mode: CGTextDrawingMode##CGTextDrawingMode#>)
		
		UIGraphicsPopContext()
		
	}
	
	
	
	//    func tilePathForMapRect(mapRect: MKMapRect, andZoomScale zoom: MKZoomScale) -> MKTileOverlayPath {
	//        let zoom_level = self.zoomLevelForZoomScale(zoomScale: zoom)
	//        let mercatorPoint = self.mercatorTileOriginForMapRect(mapRect: mapRect)
	//        //dout(.tiles, m:"mercPt: " + String(mercatorPoint))
	//
	//        let tilex = Int(floor(Double(mercatorPoint.x) * self.worldTileWidthForZoomLevel(zoomLevel: zoom_level)))
	//        let tiley = Int(floor(Double(mercatorPoint.y) * self.worldTileWidthForZoomLevel(zoomLevel: zoom_level)))
	//
	//        return MKTileOverlayPath(x: tilex, y: tiley, z: zoom_level, contentScaleFactor: UIScreen.main.scale)
	//    }
	//
	//    func stringForTilePath(path: MKTileOverlayPath) -> String {
	//        return String(format: "%d_%d_%d", path.z, path.x, path.y)
	//    }
	//
	//    func zoomLevelForZoomScale(zoomScale: MKZoomScale) -> Int {
	//        let real_scale = zoomScale / UIScreen.main.scale
	//        var z = Int((log2(Double(real_scale))+20.0))
	//        z += (Int(UIScreen.main.scale) - 1)
	//        return z
	//    }
	//
	//    func worldTileWidthForZoomLevel(zoomLevel: Int) -> Double {
	//        return pow(2, Double(zoomLevel))
	//    }
	//
	//    func mercatorTileOriginForMapRect(mapRect: MKMapRect) -> CGPoint {
	//        let map_region: MKCoordinateRegion = MKCoordinateRegionForMapRect(mapRect)
	//
	//        var x : Double = map_region.center.longitude * (.pi/180.0)
	//        var y : Double = map_region.center.latitude * (.pi/180.0)
	//        y = log10(tan(y) + 1.0/cos(y))
	//
	//        x = (1.0 + (x / .pi)) / 2.0
	//        y = (1.0 - (y / .pi)) / 2.0
	//
	//        return CGPoint(x: CGFloat(x), y: CGFloat(y))
	//    }
}


class DKW2BackgroundOverlayRenderer : MKTileOverlayRenderer {
	
	let tile_overlay: DKW2BackgroundOverlay
	var zoom_scale: MKZoomScale?
	
	override init(overlay: MKOverlay) {
		self.tile_overlay = overlay as! DKW2BackgroundOverlay
		super.init(overlay: overlay)
	}
	
//	override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
//		//		dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
//		//		UIGraphicsPushContext(context)
//		let draw_rect = self.rect(for: mapRect)
//
//		context.setFillColor(UIColor.blue.cgColor)
//		context.fill(draw_rect)
//		context.setLineWidth(8.0 / zoomScale.magnitude)
//		context.setStrokeColor(UIColor.yellow.cgColor)
//		context.move(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y + draw_rect.size.height))
//		context.addLine(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y))
//		context.addLine(to: CGPoint(x: draw_rect.origin.x + draw_rect.size.width, y: draw_rect.origin.y))
//		context.strokePath()
//		context.drawPath(using: .fillStroke)
//
//		//		UIGraphicsPopContext()
//
//		//		UIGraphicsPushContext(context)
//
//		//		TileLoading.syncQueue.sync(){
//		dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
//
//		DKW2GenerateTileByCoords(mkTileBitmapContext: context, zoom: zoomScale, northWest: mapRect.origin.coordinate, southEast: MKMapPoint.init(x: mapRect.origin.x + mapRect.size.width, y: mapRect.origin.y + mapRect.size.height).coordinate, mapRect: mapRect, isBackground: true)
//		//		}
//		//		UIGraphicsPopContext()
//	}
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        //        dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
        //        UIGraphicsPushContext(context)
        let draw_rect = self.rect(for: mapRect)
        
//        context.setFillColor(UIColor.lightGray.cgColor)
        context.setFillColor(UIColor.darkGray.cgColor)
        context.fill(draw_rect)
        //        context.setLineWidth(8.0 / zoomScale.magnitude)
        //        context.setStrokeColor(UIColor.yellow.cgColor)
//        context.move(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y + draw_rect.size.height))
//        context.addLine(to: CGPoint(x: draw_rect.origin.x, y: draw_rect.origin.y))
//        context.addLine(to: CGPoint(x: draw_rect.origin.x + draw_rect.size.width, y: draw_rect.origin.y))
//        context.strokePath()
//        context.drawPath(using: .fillStroke)
        
        //        UIGraphicsPopContext()
        
        //        UIGraphicsPushContext(context)
        
        //        TileLoading.syncQueue.sync(){
//        dout(.tiles, "DKW2TileOverlayRenderer:draw() \(mapRect.origin.x) \(mapRect.origin.y) \(zoomScale.magnitude)")
//
//        DKW2GenerateTileByCoords(mkTileBitmapContext: context, zoom: zoomScale, northWest: mapRect.origin.coordinate, southEast: MKMapPoint.init(x: mapRect.origin.x + mapRect.size.width, y: mapRect.origin.y + mapRect.size.height).coordinate, mapRect: mapRect, isBackground: true)
        //        }
        //        UIGraphicsPopContext()
    }
}

