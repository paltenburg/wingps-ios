//
//  StDKW2MipmapTable.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2MipmapTable {
    private var levels = [DKW2Mipmap]()
    
    func clear() {
        levels.removeAll()
    }
    
    func getCount() -> Int {
        return levels.count
    }
    
    func getHeight(at level: Int) -> Int {
        return levels[level].height
    }
    
    func getWidth(at level: Int) -> Int {
        return levels[level].width
    }
    
    func getTilesX(at level: Int) -> Int {
        return levels[level].tilesX
    }
    
    func getTilesY(at level: Int) -> Int {
        return levels[level].tilesY
    }
    
    func getFirstTile(_ level: Int) -> Int {
        return levels[level].firstTile
    }
    
    func getTileCount(_ level: Int) -> Int {
        return levels[level].tilesX * levels[level].tilesY
    }
    
    func initialize(count: Int, width0: Int, height0: Int) -> Int {
        if (count == DKW2MIPMAPCOUNTAUTO) {
            let c = Int64(max(width0, height0))
            var C = c
            var Count = 1
            while (C > 1) {
                let Divisor = Int64(1 << Count)
                C = divRndUp(dividend: c, Divisor)
                Count += 1
            }
        }
        
        var TileCount = 0
        for i in 0..<count {
            let Divisor = Int64(1 << i)
            var mipmap = DKW2Mipmap()
            mipmap.width = Int(divRndUp(dividend: Int64(width0), Divisor))
            mipmap.height = Int(divRndUp(dividend: Int64(height0), Divisor))
            mipmap.tilesX = Int(divRndUp(dividend: Int64(mipmap.width), Int64(DKW2TILESIZEX)))
            mipmap.tilesY = Int(divRndUp(dividend: Int64(mipmap.height), Int64(DKW2TILESIZEY)))
            mipmap.firstTile = TileCount
            TileCount += Int(mipmap.tilesX * mipmap.tilesY)
            levels.append(mipmap)
        }
        
        return TileCount
    }
    
    func divRndUp(dividend: Int64, _ divisor: Int64) -> Int64 {
        var Result = dividend / divisor
        let Remainder = dividend % divisor
        
        if (Remainder > 0) {
            Result += 1
        }
        
        return Result
    }
    
    class DKW2Mipmap {
        var width: Int = 0
        var height: Int = 0
        var tilesX: Int = 0
        var tilesY: Int = 0
        var firstTile: Int = 0
    }
    
    
    
    
}
