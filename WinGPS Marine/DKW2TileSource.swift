//
//  DKW2TileSource.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import UIKit


//class TileOffsetQueues {
//	static var queues = [NSString : DispatchQueue]()
//}

// Keep queue together with cached version, so they'll get cleaned up together
class TileAndQueueCache {
    class CachedTileAndQueue {
        var queue :DispatchQueue?
        var tile :UIImage?
    }
    static private var cache :NSCache<NSString, CachedTileAndQueue>?
    
    static func getCache() -> NSCache<NSString, CachedTileAndQueue>{
        if cache == nil {
            cache = NSCache<NSString, CachedTileAndQueue>()
            //			cache!.countLimit = 20
            //			cache!.countLimit = 1000000
            
        }
        return self.cache!
    }
    
    static func clearCache() {
        cache = nil
    }
}

class DKW2TileSource {
    
    //func getTileBmp(tile :DKW2MapTile, outputBuffer, fullyTransparent :inout Bool, hasTransparentColor :inout Bool) -> CGContext {
    
    // Reasonable defaults ..
    let minZoom = 6
    let maxZoom = 18
    let tileSizePixels = StDKW2Utils_DKW2TILESIZEY
    
    var bitCountTable :[UInt8] = [UInt8]()
    
    init(){
        
        // implement initialization of other variables as needed
        
        fillbitCountTable()
        
    }
    
    func getTileBmp(tile :DKW2MapTile, buffer :inout StDKW2ByteBuffer!, fullyTransparent :inout Bool, hasTransparentColor :inout Bool) -> UIImage? {
        
        // get file
        
        var bufferVar = buffer
        
        //		if (tile.offset != 32317189){ // 32317189
        //			return nil
        //		}
        
        if let fileHandle = tile.fileHandle {
            
            //			if fileHandle.lock {
            //				return nil
            //			}
            //			fileHandle.lock = true
            
            var decompressionSucces = false
            // officially: get bitmap from pool.
            
            
            
            
            //			let queue = DispatchQueue(label: "TileGenerationQueue"+String(tile.offset))
            //			queue.sync() {
            //			while locked {
            //				while fileHandle.lock {
            //					usleep(100000)
            //				}
            //				usleep(100000)
            
            //			objc_sync_enter(fileHandle)
            //
            //				if (!fileHandle.lock){
            //					fileHandle.lock = true
            //					locked = false
            //				}
            //			defer{
            //				objc_sync_exit(fileHandle)
            //			}
            
            // Try if tile is being decoded in another thread
            let tileKey = NSString(string: String(tile.offset))
            var tileCQ :TileAndQueueCache.CachedTileAndQueue?
            StaticQueues.tileLocking.sync {
                
                if let tileCacheAndQueueExisting = TileAndQueueCache.getCache().object(forKey: tileKey) {
                    tileCQ = tileCacheAndQueueExisting
                } else {
                    tileCQ = TileAndQueueCache.CachedTileAndQueue()
                    tileCQ!.queue = DispatchQueue(label: "")
                    TileAndQueueCache.getCache().setObject(tileCQ!, forKey: tileKey)
                    //					TileOffsetQueues.queues[tileKey] = tileSpecificQueue
                    
                }
            }
            
            // Using a thread specific to this dkw2-tile to ensure it is untouched elsewhere
            var tileImage :UIImage? = nil
            tileCQ!.queue!.sync{
                
                // Now it's not touched by another decoding thread, so see if it exists in cache
                var bitmapContext :CGContext?
                StaticQueues.tileCachingLock.sync{
                    if let cachedTile = tileCQ!.tile {
                        dout(.tiles, "Tile FOUND in cache")
                        tileImage = cachedTile.copy() as? UIImage  // testing,test without copy //
                        //						tileImage = cachedTile as UIImage
                        return
                    } else {
                        dout(.tiles, "Tile NOT found in cache, so create it, and later store the UIImage in cache")
                        
                        guard let bitmapContextTmp = CGContext(data: nil, width: tile.width, height: tile.height, bitsPerComponent: 8, bytesPerRow: 4 * tile.width, space:
                            CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue)
                            else {
                                dout(.tiles, "context is nil")
                                fileHandle.lock = false
                                return
                        }
                        bitmapContext = bitmapContextTmp
                        //				TileCache.tileCache.setObject(bitmapContext, forKey: NSString(string: String(tileKey)))
                    }
                }
                if let tileImage = tileImage {
                    return
                }
                
                
                dout(.tiles, "getTileBmp: 1", tile.offset)
                //			var tileDataPos = 0
                //			for i in 0 ..< 10 {
                //				dout(.tiles, "getTileBmp: ", i, Int(getShortFromNSDataBuffer(buffer: buffer.tileData as NSData, at: &tileDataPos)))
                //			}
                
                var dataPos :Int = 0
                
                
                for tryNr in 0 ..< 1 { //DEBUG repeat some times. Apparently not needed anymore
                    // set position in file
                    
                    StaticQueues.fileLoadingLock.sync{
                        fileHandle.handle.seek(toFileOffset: tile.offset)
                        buffer.tileData = fileHandle.handle.readData(ofLength: tile.size)
                    }
                    
                    //			var valuesString = ""
                    //			for i in 0 ..< 20 {
                    //				//			valuesString  += "\(String(tileDataPos)) \(String(Int(getUShortFromNSDataBuffer(buffer: src as NSData, at: &tileDataPos)))) "
                    //				var testByte :UInt8 = 0
                    //				(buffer.tileData as NSData).getBytes(&testByte, range: NSRange(location: i, length: 1))
                    //				valuesString  += "\(i) \(testByte) "
                    //			}
                    //			dout(.tiles, "tileDecompressBlock 1st values:", valuesString)
                    
                    // set buffer.tileData to filedata
                    
                    var fullyOpaque :Bool = false
                    
                    //            guard var buffer = bitmapContext.data else {
                    //                return nil
                    //            }
                    guard var pixelBuffer = bitmapContext!.data?.bindMemory(to: UInt32.self, capacity: tile.width * tile.height) else {
                        return
                    }
                    
                    
                    //            let bmpBuffer :NSMutableData = StDKW2Codec.tileDecompressBlock(
                    //                width: tile.width, height: tile.height, pitch: Int(calcPitch32(pixPerScanLine: UInt32(tile.width), bitsPerPixel: 32)),
                    //                bitCountTable: bitCountTable,
                    //                buffers: &buffer, tileDataPos: &dataPos,
                    //                includeAlpha: tile.includeAlpha, fullyTransparent: &fullyTransparent, fullyOpaque: &fullyOpaque,
                    //                hasTransparentColor: &hasTransparentColor, transparentColor: tile.transparentColor);
                    
                    dout(.tiles, "getTileBmp: 2", tile.offset)
                    
                    //			for tryNr in 1 ... 100 {
                    do {
                        decompressionSucces = try StDKW2Codec.tileDecompressBlock(into: pixelBuffer,
                                                                                  width: tile.width, height: tile.height, pitch: Int(calcPitch32(pixPerScanLine: UInt32(tile.width), bitsPerPixel: 32)),
                                                                                  bitCountTable: bitCountTable,
                                                                                  buffers: &buffer, tileDataPos: &dataPos,
                                                                                  includeAlpha: tile.includeAlpha,
                                                                                  fullyTransparent: &fullyTransparent,
                                                                                  fullyOpaque: &fullyOpaque,
                                                                                  hasTransparentColor: &hasTransparentColor, transparentColor: tile.transparentColor, tileOffset: tile.offset)
                        break
                    } catch {
                        dout(.tiles, "getTileBmp: error in tileDecompressBlock, try", tryNr)
                    }
                }
                //			}
                if !decompressionSucces {
                    return
                }
                
                dout(.tiles, "getTileBmp: 3", tile.offset)
                
                dout(.tiles, "Tile decompressed")
                
                fileHandle.lock = false
                
                if fullyTransparent {
                    return
                }
                
                var bmp :CGContext
                if decompressionSucces {
                    
                    tileImage = imageFromContext(bitmapContext!)
                    tileCQ!.tile = tileImage
                }
                
                //            return bmp
                //			return nil
            }
            
            //			if let tileImage = tileImage {
            //				TileCache.tileCache.setObject(tileImage, forKey: tileKey)
            //			}
            return tileImage
        }
        return nil
    }
    
    func calcPitch32(pixPerScanLine :UInt32, bitsPerPixel :UInt32) -> UInt32 {
        return (((pixPerScanLine * bitsPerPixel) + 31) & (~31)) >> UInt32(3)
    }
    
    func fillbitCountTable() {
        bitCountTable = [UInt8](repeating: 0, count: 65536)
        
        bitCountTable[0] = 1
        bitCountTable[1] = 1
        
        for i in 2 ..< 4 {bitCountTable[i] = 2}
        for i in 4 ..< 8 {bitCountTable[i] = 3}
        for i in 8 ..< 16 {bitCountTable[i] = 4}
        for i in 16 ..< 32 {bitCountTable[i] = 5}
        for i in 32 ..< 64 {bitCountTable[i] = 6}
        for i in 64 ..< 128 {bitCountTable[i] = 7}
        for i in 128 ..< 256 {bitCountTable[i] = 8}
        for i in 256 ..< 512 {bitCountTable[i] = 9}
        for i in 512 ..< 1024 {bitCountTable[i] = 10}
        for i in 1024 ..< 2048 {bitCountTable[i] = 11}
        for i in 2048 ..< 4096 {bitCountTable[i] = 12}
        for i in 4096 ..< 8192 {bitCountTable[i] = 13}
        for i in 8192 ..< 16384 {bitCountTable[i] = 14}
        for i in 16384 ..< 32768 {bitCountTable[i] = 15}
        for i in 32768 ..< 65536 {bitCountTable[i] = 16}
        
    }
}
