//
//  StDKW2Tag.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2Tag {
    var guid = StUUID()
    var productID = UInt32()
    var moduleBitIndex = Int32()
    var productStr = String()
    var productName = String()
    var moduleName = String()
    var moduleNameShort = String()
    var chartName = String()
    var chartNumber = String()
    var editionYear = Int16()
    var revisionDate = String()
    var expDate = Date()
    var digitalPublisher = String()
    var license = String()
    var licenseDisclaimer = String()
    var sourceChartName = String()
    var sourcePublisher = String()
    var sourceCopyright = String()
    var scale = String()
    var unitsDepth = String()
    var unitsHeight = String()
    
    var magVarDefined = false
    var magVar = Double()
    var magVarDrift = Double()
    var magVarMeasureDate :Date? = nil
    
    var legendFileName = String()
    
    var links = StLinkList()
    
    var calPointCount :Int = 0
    var calPix1 = [Double]()
    var calPix2 = [Double]()
    var calPix3 = [Double]()
    var calProj1 = [Double]()
    var calProj2 = [Double]()
    var calProj3 = [Double]()
    
    var ref = GeodeticRef()
    
    func setMagVar(magVarDefinedP :Bool, magVarP :Double, magVarDriftP :Double, magVarMeasureDateP :Date?){
        magVarDefined = magVarDefinedP
        if magVarDefined {
            magVar = magVarP
            magVarDrift = magVarDriftP
            magVarMeasureDate = magVarMeasureDateP
        } else {
            magVar = 0
            magVarDrift = 0
            magVarMeasureDate = nil
        }
    }
    
    func addLink(of linkType :ELinkType, link :String, description :String){
        links.add(type: linkType, link: link, description: description)
    }
    
    func setCalibration(pointCount :Int, pix1 :[Double], pix2 :[Double], pix3 :[Double], proj1 :[Double], proj2 :[Double], proj3 :[Double]) {
        
        calPointCount = pointCount
        calPix1 = pix1
        calPix2 = pix2
        
        calProj1 = proj1
        calProj2 = proj2
        
        if pointCount == 3 {
            calPix3 = pix3
            calProj3 = proj3
        } else {
            calPix3 = [0,0]
            calProj3 = [0,0]
        }
    }
    
    
}
