//
//  WGS84Ellipsoid.swift
//  MapDemo
//
//  Created by Standaard on 06/11/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class WGS84Ellipsoid {
    
    static var instance : WGS84Ellipsoid? = nil
    
    var a :Double = 0
    var b :Double = 0
    var e :Double = 0
    var eSqr :Double = 0
    var e1Sqr :Double = 0
    var f :Double = 0
    var fInv :Double = 0
    
    var A4 :Double = 0
    var A5 :Double = 0
    var A6 :Double = 0
    var A7 :Double = 0
    
    var B0 :Double = 0
    var B1 :Double = 0
    var B2 :Double = 0
    var B3 :Double = 0
    
    var C0 :Double = 0
    var C1 :Double = 0
    var C2 :Double = 0
    var C3 :Double = 0
    
    var D0 :Double = 0
    
    init() {
        a = 6378137
        fInv = 298.257223563
        f = 1 / fInv
        b = a - f * a 
        eSqr = 2 * f - f * f 
        e = sqrt(eSqr)
        e1Sqr = eSqr / (1 - eSqr) 
        
        A4 = (1.0/2.0 + (5.0/24.0 + (1.0/12.0 + 13.0/360.0 * eSqr) * eSqr) * eSqr) * eSqr 
        A5 = (7.0/48.0 + (29.0/240.0 + 811.0/11520.0 * eSqr) * eSqr) * eSqr * eSqr 
        A6 = (7.0/120.0 + 81.0/1120.0 * eSqr) * eSqr * eSqr * eSqr 
        A7 = 4279.0/161280.0 * eSqr * eSqr * eSqr * eSqr 
        
        A4 -= A6 
        A5 = 2.0 * A5 - 4.0 * A7 
        A6 *= 4.0 
        A7 *= 8.0 
        
        let E1USGS :Double = (1 - sqrt(1 - eSqr)) / (1 + sqrt(1 - eSqr))
        let E1USGSSqr :Double = E1USGS * E1USGS
        B0 = (3.0/2.0 - (27.0/32.0) * E1USGSSqr) * E1USGS 
        B1 = (21.0/16.0 - (55.0/32.0) * E1USGSSqr) * E1USGSSqr 
        B2 = (151.0/96.0) * E1USGSSqr * E1USGS 
        B3 = (1097.0/512.0) * E1USGSSqr * E1USGSSqr 
        B0 -= B2 
        B1 = 2.0 * B1 - 4.0 * B3 
        B2 *= 4.0 
        B3 *= 8.0 
        
        C0 = 1 - (1.0/4.0 + (3.0/64.0 + 5.0/256.0 * eSqr) * eSqr) * eSqr 
        C1 = (3.0/8.0 + (3.0/32.0 + 45.0/1024.0 * eSqr) * eSqr) * eSqr 
        C2 = (15.0/256.0 + 45.0/1024.0 * eSqr) * eSqr * eSqr 
        C3 = 35.0/3072.0 * eSqr * eSqr * eSqr 
        C1 = -C1 
        C3 = -C3 
        var C4 :Double = 0.0
        C1 -= C3 
        C2 = 2.0 * C2 - 4.0 * C4 
        C3 *= 4.0 
        C4 *= 8.0 
        
        D0 = 1 / (a * (1 - ((1.0/4.0) + ((3.0/64.0) + (5.0/256.0) * eSqr) * eSqr) * eSqr)) 
    }
    
    static func ellipsoid() -> WGS84Ellipsoid {
        if instance == nil {
            instance = WGS84Ellipsoid()
        }
        return instance!
    }
}

