//
//  StVersion.swift
//  MapDemo
//
//  Created by Standaard on 05/04/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation

class StVersion {
	
	var extra :Int
	var build :Int
	var minor :Int
	var major :Int
	
    convenience init(extra :Int, build :Int, minor :Int, major :Int){
        self.init(extra, build, minor, major)
    }
        
	init(_ extra :Int, _ build :Int, _ minor :Int, _ major :Int){
		self.extra = extra
		self.build = build
		self.minor = minor
		self.major = major
	}
	
	init(_ version :UInt32) {
		
		self.extra = Int(version >> 0 & 0xff)
		self.build = Int(version >> 8 & 0xff)
		self.minor = Int(version >> 16 & 0xff)
		self.major = Int(version >> 24 & 0xff)
	}
	
//    public static StVersion makeVersion(int Major, int Minor, int Build, int Extra) {
//        return new StVersion(Extra, Build, Minor, Major);
//    }
//
//
//    public int getExtra() {
//        return mExtra;
//    }
//
//    public int getBuild() {
//        return mBuild;
//    }
//
//    public int getMinor() {
//        return mMinor;
//    }
//
//    public int getMajor() {
//        return mMajor;
//    }

    func getVersion() -> Int {
        return Int((UInt8(extra) & 0xff) << 0) | Int((UInt8(build) & 0xff) << 8) | Int((UInt8(minor) & 0xff) << 16) | Int((UInt8(major) & 0xff) << 24)
    }
    
	func compareTo(_ version :StVersion) -> Int {
		if(major != version.major) {return major - version.major}
		if(minor != version.minor) {return minor - version.minor}
		if(build != version.build) {return build - version.build}
		if(extra != version.extra) {return extra - version.extra}
		
		return 0
	}
	
	func getVersionString() -> String {
		let sExtra = String(self.extra)
		let sBuild = String(self.build)
		let sMinor = String(self.minor)
		let sMajor = String(self.major)
		return "\(sMajor).\(sMinor).\(sBuild).\(sExtra)"
	}
}
