//
//  AccountSettingsController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 23/01/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class CreateAccountController : UIViewController, UITextFieldDelegate {
	
	
//	@IBOutlet var backgroundView: UIView!
	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var usernameTextField: UITextField!
	@IBOutlet weak var passwordTextField: UITextField!
	@IBOutlet weak var repeatPasswordTextField: UITextField!
	@IBOutlet weak var firstNameTextField: UITextField!
	@IBOutlet weak var lastNameTextField: UITextField!
	@IBOutlet weak var streetAndNumberTextField: UITextField!
	@IBOutlet weak var postalCodeTextField: UITextField!
	@IBOutlet weak var cityTextField: UITextField!
	@IBOutlet weak var countryTextField: UITextField!
	@IBOutlet weak var phoneTextField: UITextField!
	
	@IBOutlet weak var newsletterSwitch: UISwitch!
	
	@IBOutlet weak var loginStackView: UIStackView!
	@IBOutlet weak var busyIndicatorView: UIActivityIndicatorView!
	@IBOutlet weak var containerBottom: NSLayoutConstraint!

	var bottomHeight : CGFloat = 0.0
	
	@IBOutlet weak var createAccountButton: UIButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		bottomHeight = containerBottom.constant

		NotificationCenter.default.addObserver(
			self,
			selector: #selector(keyboardWillShow),
			name: UIResponder.keyboardWillShowNotification,
			object: nil
		)
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(keyboardWillHide),
			name: UIResponder.keyboardWillHideNotification,
			object: nil
		)
		
		containerView.layer.cornerRadius = 8
		containerView.clipsToBounds = true
		
		createAccountButton.layer.cornerRadius = 8
		createAccountButton.clipsToBounds = true

		usernameTextField.delegate = self
		usernameTextField.tag = 0
		usernameTextField.keyboardType = .emailAddress

		
		//usernameTextField.becomeFirstResponder()
		passwordTextField.delegate = self
		passwordTextField.tag = 1
		
		repeatPasswordTextField.delegate = self
		repeatPasswordTextField.tag = 2

		firstNameTextField.delegate = self
		firstNameTextField.tag = 3
		
		lastNameTextField.delegate = self
		lastNameTextField.tag = 4

		streetAndNumberTextField.delegate = self
		streetAndNumberTextField.tag = 5

		postalCodeTextField.delegate = self
		postalCodeTextField.tag = 6

		cityTextField.delegate = self
		cityTextField.tag = 7

		countryTextField.delegate = self
		countryTextField.tag = 8

		phoneTextField.delegate = self
		phoneTextField.tag = 9
	}
	
	@IBAction func backButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToSettings", sender: self)
	}

	@IBAction func closeButton(_ sender: Any) {
		dismiss(animated: false, completion: nil)
	}
	
	@IBAction func createAccountButtonAction(_ sender: UIButton) {
		// checks:
		
		// check if all fields are filled out and if passwords are the same

		if usernameTextField.text?.count == 0 ||
			passwordTextField.text?.count == 0 ||
			passwordTextField.text?.count ?? 0 < 6 ||
			passwordTextField.text ?? "" != repeatPasswordTextField.text ?? "" {
			
			let alert = UIAlertController(title: "create_account_error_invalid_input".localized, message: nil, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "ok".localized, style: .default))
			getTopViewController()?.presentFromMain(alert, animated: true)
			
			return
		}
		
		// TODO: validate email adress
		// TODO: do a secure password check

		// initialize chartCheckTask and assign handler
		let chartCheckTask = StChartCheckTask(aHandler: {(cmd, msg, data) -> Void in
            self.createRequestHandleMessages(cmd: cmd, msg: msg, data: data)
		}, aUseAct: true)
		
		// run account and device check
		chartCheckTask.requestCreateAccount (
			email: usernameTextField.text ?? "",
			pwd: passwordTextField.text ?? "",
			fn: firstNameTextField.text ?? "",
			ln: lastNameTextField.text ?? "",
			str_nr: streetAndNumberTextField.text ?? "",
			pc: postalCodeTextField.text ?? "",
			city: cityTextField.text ?? "",
			cc: countryTextField.text ?? "",
			phone: phoneTextField.text ?? "",
			newsletter: newsletterSwitch.isOn)
		
		
		
//			EditText email1 = (EditText)findViewById(R.id.UserAccountEditEmail);
//
//			EditText pw1 = (EditText)findViewById(R.id.UserAccountEditPassword);
//			EditText pw2 = (EditText)findViewById(R.id.UserAccountEditPassword2);
//
//			mEmail = email1.getText().toString();
//			mPwd = pw1.getText().toString();
//
//			if usernameTextField.text?.count == 0 {
//				mErrorCode = 94
//				showResult()
//				return
//			}
//
//			if (!validateEmail(mEmail)) {
//				mErrorCode = 95;
//				showResult();
//				return;
//			}
//
//			if (!mEmail.equals(email2.getText().toString())) {
//				mErrorCode = 96;
//				showResult();
//				return;
//			}
//
//			if (mPwd.length() == 0) {
//				mErrorCode = 97;
//				showResult();
//				return;
//			}
//
//			if (mPwd.length() < 6) {
//				mErrorCode = 98;
//				showResult();
//				return;
//			}
//
//			if (!mPwd.equals(pw2.getText().toString())) {
//				mErrorCode = 99;
//				showResult();
//				return;
//			}
//
//			mFn = ((EditText)findViewById(R.id.UserAccountEditFirstName)).getText().toString();
//			mLn = ((EditText)findViewById(R.id.UserAccountEditLastName)).getText().toString();
//			mSt = ((EditText)findViewById(R.id.UserAccountEditStreet)).getText().toString();
//			mSnr = ((EditText)findViewById(R.id.UserAccountEditStreetNo)).getText().toString();
//			mPc = ((EditText)findViewById(R.id.UserAccountEditPostCode)).getText().toString();
//			mCity = ((EditText)findViewById(R.id.UserAccountEditCity)).getText().toString();
//
//			mCc = "";
//			CountryListItem item = (CountryListItem)mCountrySpinner.getSelectedItem();
//			if (item != null)
//			mCc = item.CountryCode;
//			mPhone = ((EditText)findViewById(R.id.UserAccountEditPhone)).getText().toString();
//			mNl = ((CheckBox)findViewById(R.id.UserAccountCheckNewsletter)).isChecked();
//
//			mButtonOk.setEnabled(false);
//			//mCheckPem = true;
//			mAccountCheckSt.requestCreateAccount(mEmail, mPwd, mFn, mLn, mSt, mSnr, mPc, mCity, mCc, mPhone, mNl);
//			//		mAccountCheckSt.checkPemFile();
		

	}
	
    func createRequestHandleMessages(cmd :String, msg :Int, data :String){
		var errorCode = msg

		if errorCode == StChartCheckTask.CHECKRESULT_OK {
			
			let alert = UIAlertController(title: "create_account_succeeded_message".localized, message: nil, preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
				self.dismiss(animated: false, completion: {
					// back to accountsettings
					
					(getTopViewController() as? ViewController)?.performSegue(withIdentifier: "accountSegue", sender: nil)

				})
			}))
			
			DispatchQueue.main.async{
				getTopViewController()?.presentFromMain(alert, animated: true)
			}
			
//			self.dismiss(animated: true, completion: {() -> Void in
//			})
			
			//			let alert = UIAlertController(title: "error_dialog_title, message: errorMessage, preferredStyle: .alert)
			//			alert.addAction(UIAlertAction(title: "ok, style: .default, handler: {(alert) -> Void in
			//				// return to login screen
			//			}))
			//			self.presentFromMain(alert, animated: true)
			
		} else {
			
            let errorMessage = StatusCheck.serverErrorMessage(cmd: cmd, code: errorCode)
		
			let alert = UIAlertController(title: errorMessage, message: nil, preferredStyle: .alert)
//			alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in	}))
			alert.addAction(UIAlertAction(title: "ok".localized, style: .default))
			DispatchQueue.main.async{
				getTopViewController()?.presentFromMain(alert, animated: true)
			}
		}
	}

//	@IBAction func loginButtonAction(_ sender: Any) {
//		usernameTextField.endEditing(true)
//		passwordTextField.endEditing(true)
//
//		DispatchQueue.main.async{
//			self.loginStackView.isHidden = true
//			self.busyIndicatorView.startAnimating()
//		}
//
//		// check login data
//
//		var user = usernameTextField.text ?? ""
//		var pass = passwordTextField.text ?? ""
//
//		if StUtils.isDebug() {
//			if user.count == 0 && pass.count == 0 {
//				user = "stentecgalaxytabs28@gmail.com"
//				pass = "st4soft"
//			}
//		}
//
//		mActDB.setUserData(aEmail: user, aPassword: pass)
//
//		// initialize chartCheckTask and assign handler
//		let chartCheckTask = StChartCheckTask(aHandler: {(msg, data) -> Void in
//			self.loginHandleMessages(msg: msg, data: data)
//		})
//		// run account and device check
//		chartCheckTask.requestCheckAccountGetChartsetFiles(aActDB: mActDB)
//		//				self.statusCheckComplete(false)
//
//	}

//	func loginHandleMessages(msg :Int, data :String){
//		var errorCode = msg
//
//		if errorCode == StChartCheckTask.CHECKRESULT_OK {
//
//			self.dismiss(animated: true, completion: {() -> Void in
//				// always show download manager
//				self.showDownloadManagerView()
//			})
//
//			//			let alert = UIAlertController(title: "error_dialog_title, message: errorMessage, preferredStyle: .alert)
//			//			alert.addAction(UIAlertAction(title: "ok, style: .default, handler: {(alert) -> Void in
//			//				// return to login screen
//			//			}))
//			//			self.presentFromMain(alert, animated: true)
//
//		} else {
//
//			let errorMessage = StatusCheck.serverErrorMessage(byCode: errorCode)
//
////			// Errorcodes:
////			switch errorCode {
////			//		RESULT_DATABASE_ERROR (-4), error while doing something with database
////			case 4: // RESULT_USER_NOT_FOUND (4), no user found with the given email and password
////				errorMessage = "statuscheck_connect_unknownuser".localized
////
////				//		RESULT_NO_ANONYMOUS_ACTIVATION (2), no anonymous activation allowed
////				//		RESULT_APPDEVICECHECK_GET (-12), devicecheck get bitstate failed
////				//		RESULT_APPDEVICECHECK_SET (-13), devicecheck set bitstate failed
////				//		RESULT_APPWASACTIVE (30), there was still a blacklisted activation for a manually reset activation (1b2 situation)
////				//		RESULT_APPACTIVEDIFF (34), given activation is not the active one (2b situation)
////				//		RESULT_APPBLACKLISTED (32), given activation is blacklisted (3b1 situation)
////				//		RESULT_UNKNOWN_DEVTYPE (33), the given devicetype is not accepted (0 or 1 only)
////			//		RESULT_ALL_OK (0), everything ok
////			default:
////				errorMessage = "statuscheck_acterror_unknownerror".localized + " (code \(errorCode))"
////			}
//
//			mActDB.resetUserData()
//
//			DispatchQueue.main.async{
//				self.busyIndicatorView.stopAnimating()
//				self.loginStackView.isHidden = false
//			}
//
//			let alert = UIAlertController(title: "error_dialog_title".localized, message: errorMessage, preferredStyle: .alert)
//			alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
//				// return to login screen
//			}))
//			getTopViewController()?.presentFromMain(alert, animated: true)
//
//		}
//	}
//
//	@IBAction func logoutButtonAction(_ sender: Any) {
//		passwordTextField.text = ""
//
//		DispatchQueue.main.async{
//			self.loggedinStackView.isHidden = true
//			self.busyIndicatorView.startAnimating()
//
//		}
//
//		// initialize chartCheckTask and assign handler
//		let chartCheckTask = StChartCheckTask(aHandler: {(msg, data) -> Void in
//			self.logoutHandleMessages(msg: msg, data: data)
//		})
//		// run account and device check
//		chartCheckTask.requestDeactivateActiveDevice(aActDB: mActDB)
//
//	}
//
//	func logoutHandleMessages(msg :Int, data :String){
//		var errorCode = msg
//
//		if errorCode == StChartCheckTask.CHECKRESULT_OK {
//			//				RESULT_ALL_OK (0), everything ok. The active activation has been deleted
//
//			logout()
//
//			//			self.dismiss(animated: true, completion: {() -> Void in })
//
//			// // little confirmation dialog:
//			//			let alert = UIAlertController(title: "error_dialog_title, message: errorMessage, preferredStyle: .alert)
//			//			alert.addAction(UIAlertAction(title: "ok, style: .default, handler: {(alert) -> Void in
//			//			}))
//			//			self.presentFromMain(alert, animated: true)
//
//		} else {
////			var errorMessage = "statuscheck_acterror_unknownerror".localized + " (code \(errorCode))"
//
//			let errorMessage = StatusCheck.serverErrorMessage(byCode: errorCode)
//
//			switch errorCode {
//				//				errors:
//				//				RESULT_DATABASE_ERROR (-4), error while doing something with database
//			//				RESULT_USER_NOT_FOUND (4), no user found with the given email and password
//			case 4:
//				logout()
//				//				RESULT_NO_ANONYMOUS_ACTIVATION (2), no anonymous activation allowed
//				//				RESULT_APPDEVICECHECK_GET (-12), devicecheck get bitstate failed
//				//				RESULT_APPDEVICECHECK_SET (-13), devicecheck set bitstate failed
//				//				RESULT_UNKNOWN_DEVTYPE (33), the given devicetype is not accepted (0 or 1 only)
//			//				RESULT_NOT_ACTIVATED (6), there is no active activation for the given data
//			case 6:
//				logout()
//			default:
//				DispatchQueue.main.async{
//					self.busyIndicatorView.stopAnimating()
//					self.loggedinStackView.isHidden = false
//				}
//				print("LogoutHandleMessages: errorcode \(errorCode)")
//			}
//
//
//			let alert = UIAlertController(title: "error_dialog_title".localized, message: errorMessage, preferredStyle: .alert)
//			alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
//				// return to loggedin screen
//			}))
//			getTopViewController()?.presentFromMain(alert, animated: true)
//		}
//	}
//
//	@IBAction func createNewAccountButtonAction(_ sender: UIButton) {
//		UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/registers")! as URL)
//	}
//
//
//	func logout(){
//		mActDB.resetUserData()
//		mActDB.resetProducts()
//
//		DownloadManagerController.clearCharts()
//
//		DispatchQueue.main.async{
//			self.busyIndicatorView.stopAnimating()
//			self.loggedinStackView.isHidden = true
//			self.loginStackView.isHidden = false
//		}
//
//		DKW2ChartManager.refreshCharts()
//	}
//
//
//	@IBAction func forgatPasswordButtonAction(_ sender: Any) {
//		UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/lostpassword")! as URL)
//	}

	func textFieldShouldReturn(_ textField: UITextField) -> Bool
	{
		let nextTag: NSInteger = textField.tag + 1
		// Try to find next responder
		if let nextResponder: UIResponder = textField.superview!.viewWithTag(nextTag) {
			// Found next responder, so set it.
			nextResponder.becomeFirstResponder()
		} else {
			// Not found, so remove keyboard.
			textField.resignFirstResponder()
		}

		// Do not add a line break
		return false
	}

//	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//		let touch : UITouch? = touches.first
//		if (touch?.view == backgroundView){
//			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
//			self.dismiss(animated: true, completion: nil)
//		}
//	}

	@objc func keyboardWillShow(_ notification: Notification) {
//		if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//			let keyboardRectangle = keyboardFrame.cgRectValue
//			let keyboardHeight = keyboardRectangle.height
//			containerBottom.constant = keyboardHeight - 40
//		}
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
	}

	@objc func keyboardWillHide(_ notification: Notification) {
//		if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//			containerBottom.constant = bottomHeight
//		}
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
	}

}
