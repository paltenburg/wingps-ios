//
//  Error.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

enum ErrorCode {
	
	// General Errors
	case ER_OK
	case ER_UNKNOWN
	case ER_OUTOFMEMORY
	case ER_OVERFLOW
	case ER_INVALIDFLOAT
	
	// File Errors
	case ER_FILE_ERROR
	
	case ERR_FILE_MIN
	case ERR_GEODETICS_MIN
	// DKW2 error range.
	case ERR_DKW2_MIN
	
	case ER_FILENOTFOUND// File not found.
	case ER_FILEREAD // Error during reading of a file.
	case ER_FILECLOSE // Could not close the file.
	case ER_FILEFORMAT // The format of the file is incorrect.
	case ER_FILEHEADER // Invalid header.
	case ER_FILEVERSIONUNKNOWN // The version is unknown.
	case ER_FILEVERSIONNEWER // The version is newer than supported by this application.
	case ER_FILEVERSIONUNSUPPORTED // The version is not supported.
	
	case ER_GEO_UNKNOWN_PROJECTION // Unknown projection.
	case ER_GEOCAL_INVALID_POINT_COUNT // Number of calibration points is invalid.
	
	case ER_DKW2OPERATIONNOTSUPPORTED // Operation not supported on this version of a DKW2-file.
	case ER_DKW2CANNOTRENAMEFILE // Error during renaming of a DKW2-file.
	case ER_DKW2INVALIDWIDTH // Width of the chart is invalid.
	case ER_DKW2INVALIDHEIGHT // Height of the chart is invalid.
	case ER_DKW2INVALIDMIPMAPCOUNT // Mipmap count of the chart is invalid.
	case ER_DKW2INVALIDBOUNDSCOUNT // Number of bounds points is invalid.
	case ER_DKW2UNKNOWNDATUMTYPE // Datum type is unknown.
	case ER_DKW2UNKNOWNSOURCEFORMAT // Source format code is unknown.
	case ER_DKW2UNKNOWNLINKTYPE // Link type code is unknown.
	case ER_DKW2UPDATEDBMPSIZE // The updated bitmap and the original chart have different sizes.
	case ER_DKW2NOUPDATEFORTHISTILE // No update available for this tile.
	
	// Activation / chart checking
	
	case CHECKRESULT_INTERNALERROR
	case CHECKRESULT_OK
	case CHECKRESULT_DEVICETOKEN_ERROR
	
	case RESULT_OK
}

