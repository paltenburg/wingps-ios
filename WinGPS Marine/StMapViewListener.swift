//
//  StMapViewListener.swift
//  MapDemo
//
//  Created by Standaard on 03/05/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import MapKit

@objc public protocol StMapViewListener {
	
	
	@objc optional func mapViewListener(_ mapView: StMapView, rotationDidChange rotation: Double)
	// message is sent when map rotation is changed
	
	@objc optional func mapViewListener(_ mapView: StMapView, zoomDidChange zoom: Double)
	// message is sent when map rotation is changed
	
}
