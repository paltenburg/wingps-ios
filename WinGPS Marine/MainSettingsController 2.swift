//
//  SettingsController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 24/01/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit


class MainSettingsController : AllSettingsViewController {

	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var doneButton: UIButton!
	@IBOutlet weak var settingsContainer: UIView!

	override func viewDidLoad() {
		super.viewDidLoad()
		settingsContainer.layer.cornerRadius = 8
		settingsContainer.clipsToBounds = true

		// Hide/show buttons for standard paid version:
		let viewsInPlusVersion :[UIView?] = [tcpConnectionRowView, aisSettingsRowView]
        
		switch Resources.appType {
        case .lite, .dkw1800, .friesemeren:
			for view in viewsInPlusVersion {
				view?.isHidden = true
			}
		case .plus:
			for view in viewsInPlusVersion {
				view?.isHidden = false
			}
		}
    }
		
	@IBAction func doneButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func backButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToMainMenu", sender: self)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if (touch?.view == backgroundView){
			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
			self.dismiss(animated: false, completion: nil)
		}
	}
}
