//
//  DKW2Classes.swift
//  MapDemo
//
//  Created by Standaard on 25/08/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
//import PlaygroundSupport
import UIKit
import MapKit



func testSingleTile() {
	//let filepath = XCPlaygroundSharedDataDirectoryURL;
	
	var dkw2p :URL!
	
	
	let fileManager = FileManager.default
	
	let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
	
	dout(.tiles, urls)
	
	let dkw2181003g = getAppBundleDKW2Directory().appendingPathComponent("dkw18e17_-_1810.03g.dkw2")
	
	dkw2p = dkw2181003g
	
	// where are you?
	//        NSLog("Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
	
	//    let dkwimray = playgroundSharedDataDirectory.appendingPathComponent("DKWIMRAY2016_Y18_C.dkw2")
	//    let dkw2FrieseMeren = playgroundSharedDataDirectory.appendingPathComponent("friese_meren_2016_overzicht.dkw2")
	//    let dkw2181003g = playgroundSharedDataDirectory.appendingPathComponent("dkw18e17_-_1810.03g.dkw2")
	
	//let dkw2p = dkw2FrieseMeren
	
	//    let dkw2p = dkw2181003g
	
	//if dkw2p.checkResourceIsReachable() {
	//    dout(.tiles, "File exists")
	//} else {
	//    dout(.tiles, "File does not exist")
	//}
	
	do {
		try dkw2p.checkResourceIsReachable()
		dout(.tiles, "File exists")
	} catch {
		dout(.tiles, "File does not exist")
	}
	
	let fileMgr = FileManager.default
	let PathAvailable = dkw2p.path
	if fileMgr.fileExists(atPath: PathAvailable) {
		dout(.tiles, "File exists")
	} else {
		dout(.tiles, "File does not exist")
	}
	
	let chart :DKW2Chart = DKW2Chart(fileName: dkw2p)
	
	let mipMapTable = chart.mipmapTable
	let tileTable = chart.tileTableC
	
	let source = DKW2TileSource()
	
	
	for i in 0 ..< tileTable.getCount() {
		let tileDesc = tileTable.getTileDesc(index: i)
		
		
		// based on "Tiledesc"
		let bareFileHandle = FileHandle(forReadingAtPath: dkw2p.path)
		guard let fileHandle = DKWFileHandle.getHandle(bareFileHandle) else {
			exit(0)
		}
		
		//        let tile = DKW2MapTile(
		//            fileName :fileHandle,
		//            offset: 176,
		//            size: 3628,
		//            width: 227,
		//            height: 192,
		//            transparentColor: 0,
		//            includeAlpha: true)
		
		let tile = DKW2MapTile(
			fileHandle :fileHandle,
			offset: tileDesc.offset,
			size: tileDesc.size,
			width: tileDesc.width,
			height: tileDesc.height,
			transparentColor: 0,
			includeAlpha: true)
		
		//var buffer :StDKW2ByteBuffer! = StDKW2ByteBuffer(DKW2DstData: Data(),
		//                              DKW2DstSData: Data(),
		//                              tileData: Data(),
		//                              pal: [UInt32](),
		//                              used: false)
		
		var buffer :StDKW2ByteBuffer! = StDKW2ByteBuffer()
		var fullyTransparent = false
		var hasTransparentColor = false
		
		guard let bitmapContext = CGContext(data: nil, width: tile.width, height: tile.height, bitsPerComponent: 8, bytesPerRow: 4 * tile.width, space:
			CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue) else {
				dout(.tiles, "context is nil")
				return		}
		
		let bmp = source.getTileBmp(tile: tile, buffer: &buffer, fullyTransparent: &fullyTransparent, hasTransparentColor: &hasTransparentColor)		
	}
}

func getTile(z :Int, x :Int, y :Int) {
	
}

func loadTestingChart() -> DKW2Chart {
	var dkw2p :URL!
	
	let dkw2181003g = getAppBundleDKW2Directory().appendingPathComponent("dkw18e17_-_1810.03g.dkw2")
	
	dkw2p = dkw2181003g
	
	do {
		try dkw2p.checkResourceIsReachable()
		dout(.tiles, "File exists")
	} catch {
		dout(.tiles, "File does not exist")
	}
	
	if FileManager.default.fileExists(atPath: dkw2p.path) {
		dout(.tiles, "File exists")
	} else {
		dout(.tiles, "File does not exist")
	}
	
	let chart :DKW2Chart = DKW2Chart(fileName: dkw2p)
	
	return chart
}

func tranformCoordinate(latitude: Double, longitude: Double, withZoom zoom: Int) -> (x: Int, y: Int) {
	let tileX = Int(floor((longitude + 180) / 360.0 * pow(2.0, Double(zoom))))
	let tileY = Int(floor((1 - log( tan( latitude * Double.pi / 180.0 ) + 1 / cos( latitude * Double.pi / 180.0 )) / Double.pi ) / 2 * pow(2.0, Double(zoom))))
	
	return (tileX, tileY)
}

func tileToLatLon(tileX : Int, tileY : Int, mapZoom: Int) -> (lat_deg : Double, lon_deg : Double) {
	let n : Double = pow(2.0, Double(mapZoom))
	let lon = (Double(tileX) / n) * 360.0 - 180.0
	let lat = atan( sinh (.pi - (Double(tileY) / n) * 2 * Double.pi)) * (180.0 / .pi)
	
	return (lat, lon)
}

func DKW2GenerateTileByCoords(mkTileBitmapContext :CGContext, zoom :MKZoomScale, northWest :CLLocationCoordinate2D, southEast :CLLocationCoordinate2D, mapRect :MKMapRect, isBackground :Bool) -> Bool {
	dout(.tiles, "DKW2GenerateTileByCoords: ", northWest.latitude, " ", northWest.longitude)
	
	///// future member variables of DKW2TilesOverlay
	
	let DKW2ViewerCal = GeodeticCalibration()
	let MKViewerCal = GeodeticCalibration()
	let ref = GeodeticRef()
	var tileBnds = RectangleD()
	let dkw2ChartPath = getAppBundleDKW2Directory().appendingPathComponent("dkw18e17_-_1810.03g.dkw2")
	let source = DKW2TileSource()
	let chartManager = DKW2ChartManager.getChartManager()
	var inCoords :CLLocationCoordinate2D?
	
	//	// DEBUG: check MK-tile for pin-coordinate
	//	let pinLat = 52.3388
	//	let pinLon = 5.1302
	//	var pinTile = false
	//	if pinLat < northWest.latitude && pinLat > southEast.latitude && pinLon > northWest.longitude && pinLon < southEast.longitude {
	//		dout(.tiles, "DKW2GenerateTileByCoords: PIN TILE")
	//		pinTile = true
	//	}
	
	
	// initialize viewPort which is the requested tile
	let PixTL = Double2(x: -0.5, y: -0.5)
	let PixTR = Double2(x: Double(StDKW2Utils_DKW2TILESIZEX) - 0.5, y: -0.5)
	let PixBR = Double2(x: Double(StDKW2Utils_DKW2TILESIZEX) - 0.5, y: Double(StDKW2Utils_DKW2TILESIZEY) - 0.5)
	let ProjTL :Double2 = DKW2ViewerCal.geoToMercator(srcGeoDeg: northWest, dstRef: ref)
	let ProjTR :Double2 = DKW2ViewerCal.geoToMercator(srcGeoDeg: CLLocationCoordinate2D(latitude: northWest.latitude, longitude: southEast.longitude), dstRef: ref)
	let ProjBR :Double2 = DKW2ViewerCal.geoToMercator(srcGeoDeg: southEast, dstRef: ref)
	
	if !DKW2ViewerCal.initialize3(calPix1: PixTL, calPix2: PixTR, calPix3: PixBR, calProj1: ProjTL, calProj2: ProjTR, calProj3: ProjBR, width: StDKW2Utils_DKW2TILESIZEX, height: StDKW2Utils_DKW2TILESIZEY, ref: ref) {
		return false
	}
	
	// MapKit pix coords map-relative <-> DKW2 Mercator
	if !MKViewerCal.initialize3(
		calPix1: Double2(x: 0, y: 0),
		calPix2: Double2(x: mapRect.size.width, y: 0),
		calPix3: Double2(x: mapRect.size.width, y: mapRect.size.height),
		calProj1: ProjTL,
		calProj2: ProjTR,
		calProj3: ProjBR,
		width: Int(mapRect.size.width),
		height: Int(mapRect.size.height),
		ref: ref) {
		return false
	}
	
	let centerScale = DKW2ViewerCal.getScaleMax(pix: DKW2ViewerCal.getPixCenter())
	
	//	let centerLonRad :Double = degToRad(northWest.longitude + southEast.longitude) / 2.0
	//	let centerGeoRad :Geo2 = Geo2(lat: degToRad(northWest.latitude + southEast.latitude) / 2.0, lon: centerLonRad)
	//	let centerGC = GreatCirclePoint.geoToGreatCirclePoint(centerGeoRad)
	
	var visibleCharts = [DKW2ChartDisplayInfo]()
	chartManager.getVisibleCharts(centerScale: centerScale, calibration: DKW2ViewerCal, visibleCharts: &visibleCharts)
	
	var TotalTileCount = 0
	
	// load dkw2-tiles parallel or serial onto a mapkit tile
	//	let mkTileLoadingQueue = DispatchQueue(label: "com.stentec.mkTileLoading", attributes: .concurrent)
	//	let mkTileLoadingQueue = StaticQueues.tileLoading
	
	for cIndex in (0 ..< visibleCharts.count).reversed() {
		
		//		let chart = loadTestingChart()
		
		let chartInfo = visibleCharts[cIndex]
		let cChart :DKW2CalibratedChart = chartInfo.calibratedChart
		let chartCal = cChart.cal!
		
		// todo: here, chart should be assumed visible in the viewport //
		
		//		// manually set zoom (should be done in getvisiblecharts)
		//		chartInfo.chart.isVisible(viewerCalibration: DKW2ViewerCal, zoom: &(chartInfo.zoom))
		//
		//    StDKW2CalibratedChart Chart = ChartInfo.getChart();
		//		let chartCal :GeodeticCalibration = cChart.cal!
		
		//		var mp :CGPoint?
		
		if (chartInfo.cornerPoints == nil || chartInfo.CPtsInvalidate!) {
			
			var P_rad :Geo2
			var bounds :DKW2BoundsTag = chartInfo.calibratedChart.dKW2Chart!.boundsTag
			
			var borderPts :[Float] = (chartInfo.cornerPoints == nil) ? [Float](repeating: 0, count: bounds.getCount() * 2) : chartInfo.cornerPoints!
			
			for i in 0 ..< bounds.getCount() {
				P_rad = chartCal.pixToGeoRadWGS84(bounds.get(i))
				inCoords = CLLocationCoordinate2D(latitude: P_rad.lat * (180.0 / Double.pi) * 1E6, longitude: P_rad.lon * (180.0 / Double.pi) * 1E6)
				// to internal projected coordinates    mp = pj.toMapPixels(inCoords, mMP)
				//            BorderPts[I * 2] = (float)mp.x
				//            BorderPts[I * 2 + 1] = (float)mp.y
			}
			
			chartInfo.setCornerPoints(borderPts)
			
		}
		
		let mipmapTable = cChart.dKW2Chart!.mipmapTable
		let tileTable = cChart.dKW2Chart!.tileTableC
		
		var mipmapLevel = cChart.getMipmapLevel(chartInfo.zoom) // chartZoom originally set in getVisibleCharts()
		//				let mipmapLevel = 0 // chartZoom originally set in getVisibleCharts()
		if isBackground {
			mipmapLevel = cChart.dKW2Chart!.mipmapCount / 4 * 3
		}
		
		var viewerPoly = [PolyLineD].init(generating: {_ in PolyLineD()}, count: 4)
		let viewerBounds = updateViewerBounds(viewerCal: DKW2ViewerCal, chartCal: chartCal, Poly: &viewerPoly)
		
		// tile
		//        let tile = DKW2MapTile(
		//            fileName :fileHandle,
		//            offset: tileDesc.offset,
		//            size: tileDesc.size,
		//            width: tileDesc.width,
		//            height: tileDesc.height,
		//            transparentColor: 0,
		//            includeAlpha: true)
		
		//        Verts[] TileVerts = new Verts[MaxTileCount];
		//        DKW2MapTile[] DKW2MapTiles = new DKW2MapTile[MaxTileCount];
		
		let vb = viewerBounds
		let vp = viewerPoly
		
		//		// Load two different mipmaps: first the maximum level, later fill in the accurate one
		//		for mipmapRound in 0 ... 1 {
		//
		//			if mipmapRound == 0 {
		//				mipmapLevel = cChart.chart!.mipmapCount / 2 // chartZoom originally set in getVisibleCharts()
		//
		//			} else {
		//				mipmapLevel = cChart.getMipmapLevel(chartInfo.zoom) // chartZoom originally set in getVisibleCharts()
		//			}
		
		let TileMinX = max(0, Int((vb.left + 0.5) / Double(StDKW2Utils_DKW2TILESIZEX * (1 << mipmapLevel))))
		let TileMaxX = min(mipmapTable.getTilesX(at: mipmapLevel) - 1, Int((vb.right + 0.5) / Double(StDKW2Utils_DKW2TILESIZEX * (1 << mipmapLevel))))
		let TileMinY = max(0, Int((vb.top + 0.5) / Double(StDKW2Utils_DKW2TILESIZEY * (1 << mipmapLevel))))
		let TileMaxY = min(mipmapTable.getTilesY(at: mipmapLevel) - 1, Int((vb.bottom + 0.5) / Double(StDKW2Utils_DKW2TILESIZEY * (1 << mipmapLevel))))
		
		// check if chart is outside viewport //
		if TileMaxX < TileMinX || TileMaxY < TileMinY {
			return false
		}
		
		var TileCount :Int = 0
		
		let TileXCount = mipmapTable.getTilesX(at: mipmapLevel)
		
		if (TileMaxY >= TileMinY){
			for Y in (TileMinY ... TileMaxY).reversed() {
				
				dout(.tiles, "- DKW2GenerateTileByCoords: y:", Y)
				
				var X = TileMaxX
				while X != TileMinX - 1 {
					
					//                      for (int X = TileMaxX; X >= TileMinX; X--) {
					
					//                    Double2 Proj = pixToProj(SrcPix)
					
					tileBnds.XMin = Double(X * StDKW2Utils_DKW2TILESIZEX * (1 << mipmapLevel)) - 0.5 // pixel coordinates, chart relative
					tileBnds.YMin = Double(Y * StDKW2Utils_DKW2TILESIZEY * (1 << mipmapLevel)) - 0.5
					//                   mTileBnds.XMax = ((X + 1) * StDKW2Utils.DKW2TILESIZEX * (1 << mipmapLevel)) - 0.5;
					
					tileBnds.XMax = Double(min((X + 1) * StDKW2Utils_DKW2TILESIZEX * (1 << mipmapLevel), cChart.dKW2Chart!.width)) - 0.5
					tileBnds.YMax = Double(min((Y + 1) * StDKW2Utils_DKW2TILESIZEY * (1 << mipmapLevel), cChart.dKW2Chart!.height)) - 0.5
					
					// Check if the tile is visible in the viewer / mapkit-tile
					if Math.rectOverlap(R: tileBnds, Count: 4, Poly: vp) {
						
						let DstTL :Double2 = chartCal.pixTransform(dstCal: MKViewerCal, srcPix: tileBnds.getMin()) // transform the tile bounds to MK viewer relative
						//                      DstTL.x = fixRounding((float)DstTL.x);
						//                       DstTL.y = fixRounding((float)DstTL.y);
						let DstTR :Double2 = chartCal.pixTransform(dstCal: MKViewerCal, srcPix: Double2(x: tileBnds.XMax, y: tileBnds.YMin))
						
						let DstBR :Double2 = chartCal.pixTransform(dstCal: MKViewerCal, srcPix: tileBnds.getMax())
						//                          DstBR.x = fixRounding((float)DstBR.x);
						//                       DstBR.y = fixRounding((float)DstBR.y);
						let DstBL :Double2 = chartCal.pixTransform(dstCal: MKViewerCal, srcPix: Double2(x: tileBnds.XMin, y: tileBnds.YMax))
						
						// v : coordinaten in Mapkit-pixelcoords
						let v :VertsD = VertsD()
						v.verts[0] = mapRect.origin.x + DstTL.x
						v.verts[1] = mapRect.origin.y + DstTL.y
						v.verts[2] = mapRect.origin.x + DstTR.x
						v.verts[3] = mapRect.origin.y + DstTR.y
						v.verts[4] = mapRect.origin.x + DstBL.x
						v.verts[5] = mapRect.origin.y + DstBL.y
						v.verts[6] = mapRect.origin.x + DstBR.x
						v.verts[7] = mapRect.origin.y + DstBR.y
						
						/*
						RectF tileRect = new RectF((float)(viewPort.left + DstTL.x), (float)(viewPort.top + DstTL.y), (float)(viewPort.left + DstBR.x), (float)(viewPort.top + DstBR.y));
						if (tileRect.right < tileRect.left) {
						float t = tileRect.right;
						tileRect.right = tileRect.left;
						tileRect.left = t;
						}
						if (tileRect.bottom < tileRect.top) {
						float t = tileRect.bottom;
						tileRect.bottom = tileRect.top;
						tileRect.top = t;
						}
						
						*/
						// Check if this tile will be overlapped by another tile
						
						// < implement later >
						//                    var overlapped = false
						//                    if (HasOverlaps) {
						//                        for (int I = 0; I < ChartInfo.getChartOverlapCount(); I++) {
						//                            //                                    for (int j = 0; j < mVisibleCharts.size(); j++)
						//                            //                               Log.d("TileOverlapped", "doInBackground (" + mFNumber + "): " + j + ": " + mVisibleCharts.get(j).getChart().getDKW2Chart().getFileName());
						//                            if (ChartInfo.getChartOverlap(I).getEnabled()) {
						//                                int OIndex = VisibleCharts.indexOf(ChartInfo.getChartOverlap(I));
						//                                if (OIndex > -1 && OIndex > CIndex &&
						//                                    mChartManager.getTileHiddenByChart(mTileBnds, ChartCal, ChartInfo.getChartOverlap(I).getChart())) {
						//                                    //                                     Log.d("TileOverlapped", "doInBackground (" + mFNumber + "): Tile is overlapped by another charts.");
						//                                    Overlapped = true;
						//                                    break;
						//                                }
						//                            }
						//                        }
						//                    }
						// < \implement later >
						
						//    if (!Overlapped) {
						let TileX = (X > TileXCount - 1) ? X - TileXCount : X
						let TileIndex = mipmapTable.getFirstTile(mipmapLevel) + (Y * mipmapTable.getTilesX(at: mipmapLevel) + TileX)
						if TileIndex >= tileTable.getCount() {
							continue
						}
						//						var TileDesc :DKW2TileDesc = tileTable.getTileDesc(index: TileIndex)
						//					let tileDesc = chart.tileTableC.getTileDesc(index: i)
						
						//Tiles[TileCount] = TileDesc;
						//TileRects[TileCount] = tileRect;
						//					TileVerts[TileCount] = v;
						//                              TileAngles[TileCount] = rotationAngle;
						//                              Matrices[TileCount] = M;
						
						//final DKW2MapTile tile = new DKW2MapTile(Chart.getDKW2Chart().getFileName(), CT.Tiles[i].Offset, CT.Tiles[i].Size, CT.Tiles[i].Width, CT.Tiles[i].Height, includeAlpha);
						//					let includeAlpha :Bool = (mChartManager.getChartBorderVisible() != null) ? Chart != mChartManager.getChartBorderVisible().getChart() : true
						//					let TransparentColor = ChartInfo.getUseTransparentColor() ? ChartInfo.getTransparentColor() : 0
						//					let tile :DKW2MapTile = DKW2MapTile(Chart.getDKW2Chart().getFileName(), TileDesc.Offset, TileDesc.Size, TileDesc.Width, TileDesc.Height, TransparentColor, includeAlpha)
						//					DKW2MapTiles[TileCount] = tile;
						
						dout(.tiles, "GenerateTileByCoords: Tile loading: ", TileX, " ", TileIndex)
						
						let tileDesc = tileTable.getTileDesc(index: TileIndex)
						
						//						// based on "Tiledesc"
						//						guard let fileHandle = FileHandle(forReadingAtPath: dkw2ChartPath.path) else {
						//							exit(0)
						//						}
                        
                        var includeAlpha :Bool = true
                        if let chartBorderVisible = chartManager.getChartBorderVisible(){
                            includeAlpha = (cChart !== chartBorderVisible.calibratedChart)
                        }
                        
                        //int TransparentColor = ChartInfo.getUseTransparentColor() ? ChartInfo.getTransparentColor() : 0;
 
						let tile = DKW2MapTile(
							fileHandle :cChart.dKW2Chart!.fileHandle!,
							offset: tileDesc.offset,
							size: tileDesc.size,
							width: tileDesc.width,
							height: tileDesc.height,
							transparentColor: 0,
							includeAlpha: includeAlpha)
						
						var buffer :StDKW2ByteBuffer! = StDKW2ByteBuffer()
						var fullyTransparent = false
						var hasTransparentColor = false
						
						let tileID = "" + String(mapRect.origin.x) + "." + String(mapRect.origin.y) + "." + String(X) + "." + String(Y)
						
						//						StaticQueues.tileDecoding.async {
						////							<#code#>
						////						}
						//						mkTileLoadingQueue.async {
						//						mkTileLoadingQueue.sync {
						
						dout(.tiles, tileID, "starting async vvvvvvvvvvvvvvvv")
						//							let dkw2BitmapContext = source.getTileBmp(tile: tile, buffer: &buffer, fullyTransparent: &fullyTransparent, hasTransparentColor: &hasTransparentColor)
						let dkw2Image = source.getTileBmp(tile: tile, buffer: &buffer, fullyTransparent: &fullyTransparent, hasTransparentColor: &hasTransparentColor)
						
						//						// use CGImage
						//						if let image = dkw2BitmapContext.makeImage() {
						//							bitmapContext.setAlpha(1.0)
						//							bitmapContext.draw(image, in: CGRect(x: mapRect.origin.x, y: mapRect.origin.y, width: mapRect.size.width, height: mapRect.size.height))
						//						}
						
						// place UIImage in MK-Tile
						
						
						StaticQueues.tileDrawingLock.sync{
							//							if let dkw2Image = imageFromContext(dkw2BitmapContext) {
							if let dkw2Image = dkw2Image{
								//					UIGraphicsBeginImageContextWithOptions(CGSize(width: 256, height: 256), false, 0.0);
								//							bitmapContext.interpolationQuality = CGInterpolationQuality.high
								UIGraphicsPushContext(mkTileBitmapContext)
								
								mkTileBitmapContext.interpolationQuality = CGInterpolationQuality.low
								
								dkw2Image.draw(in: CGRect(x: v.verts[0], y: v.verts[1], width: v.verts[2] - v.verts[0], height: v.verts[5] - v.verts[1]))
								//					let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
								
								UIGraphicsPopContext()
								
								//							if let data = UIImagePNGRepresentation(imageFromContext(dkw2BitmapContext)!) {
								//								let filename = getDocumentsDirectory().appendingPathComponent("testImage-\(tile.offset).png")
								//								try? data.write(to: filename)
								//							}
								//
								//							dout(.tiles, "tile drawing. drawing tile width MK pix space:", v.verts[2] - v.verts[0], "mk tile width:", mapRect.size.width)
								//
								//
								//							// save to png as test
								//							if let image = imageFromContext(bitmapContext) {
								//								if let data = UIImagePNGRepresentation(image) {
								//									let filename = getDocumentsDirectory().appendingPathComponent("tile-\(tile.offset).png")
								//									try? data.write(to: filename)
								//								}
								//							}
								
							} else {
								dout(.tiles, "Error creating image from tile bitmap context")
							}
						}
						dout(.tiles, tileID, "finishing async ^^^^^^^^^^^^^^^ ")
						//						}
						let info = mkTileBitmapContext.bitmapInfo
						
						// as of now: supply bitmap context with this function
						dout(.tiles, "GenerateTileByCoords: after tile loading async ", mkTileBitmapContext.alphaInfo, mkTileBitmapContext.bitsPerComponent, mkTileBitmapContext.bitsPerPixel, mkTileBitmapContext.bytesPerRow, mkTileBitmapContext.colorSpace!, mkTileBitmapContext.width, mkTileBitmapContext.height)
						
						TileCount += 1
						
					}
					
					X -= 1
					if X < 0 && TileMinX != 0{
						X = TileXCount - 1
					}
					// go through all base tiles (mipmaps not needed)
				}
			}
			TotalTileCount += TileCount
			
			//			mkTileLoadingQueue.sync(flags: .barrier){
			//				// wait until all dkw2tiles have been loaded and drawn
			//				dout(.tiles, "Barrier sync --------------------------------" )
			//			}
		}
		//		}
	}
	return true
}


func updateViewerBounds(viewerCal :GeodeticCalibration, chartCal :GeodeticCalibration, Poly :inout [PolyLineD]) -> RectD {
	
	for i in 0 ..< 4 {
		Poly[i] = PolyLineD()
	}
	
	Poly[0].coord = viewerCal.pixTransform(dstCal: chartCal, srcPix: Double2(x: -0.5, y:-0.5))
	Poly[1].coord = viewerCal.pixTransform(dstCal: chartCal, srcPix: Double2(x: Double(viewerCal.width) - 0.5, y: -0.5))
	Poly[2].coord = viewerCal.pixTransform(dstCal: chartCal, srcPix: Double2(x: Double(viewerCal.width) - 0.5, y: Double(viewerCal.height) - 0.5))
	Poly[3].coord = viewerCal.pixTransform(dstCal: chartCal, srcPix: Double2(x: -0.5, y: Double(viewerCal.height) - 0.5))
	Math.polygonInitialize(Poly: &Poly, Count: 4)
	
	let Bounds = RectD()
	Bounds.setTopLeft(Poly[0].coord)
	Bounds.setBottomRight(Poly[0].coord)
	for I in 1 ... 3 {
		if Poly[I].coord.x < Bounds.left {
			Bounds.left = Poly[I].coord.x
		}
		if Poly[I].coord.x > Bounds.right {
			Bounds.right = Poly[I].coord.x
		}
		if Poly[I].coord.y < Bounds.top {
			Bounds.top = Poly[I].coord.y
		}
		if Poly[I].coord.y > Bounds.bottom {
			Bounds.bottom = Poly[I].coord.y
		}
	}
	if (Poly[0].coord.x > Poly[1].coord.x) {
		let Left = Bounds.left
		Bounds.left = Bounds.right
		Bounds.right = Left
	}
	return Bounds
}

class Verts {
	var verts = [Float](repeating: 0, count: 8)
	
}

class VertsD {
	var verts = [Double](repeating: 0, count: 8)
	
}
