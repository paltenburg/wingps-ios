//
//  GeodeticRef.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation


enum GeodeticProjection {
    case MERCATOR
    case TRANSVERSEMERCATOR
}


class GeodeticRef {
    var projection = GeodeticProjection.MERCATOR
    var k0 = Double(0)
    var long0 = Double(0)
    var M0 = Double(0)
    
//    public enum GeodeticProjection {
//        MERCATOR,
//        TRANSVERSEMERCATOR
//    }
//    
//    public GeodeticProjection Projection;
//    
//    public double k0;
//    public double Long0;
//    public double M0;
//    
//    public StGeodeticRef() {
//    Projection = GeodeticProjection.MERCATOR;
//    k0 = 0;
//    Long0 = 0;
//    M0 = 0;
//    }
    
    func isEqualRef(_ ref :GeodeticRef) -> Bool {
        if ref.projection != self.projection {
            return false
        }
        if self.projection == GeodeticProjection.TRANSVERSEMERCATOR {
            return (k0 == ref.k0) && (long0 == ref.long0)
        }
        return true
    }

}
