//
//  DKW2ByteBufferPool.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class StDKW2ByteBuffer {
    var DKW2DstData :NSMutableData
    var DKW2DstSData :NSMutableData
    var tileData :Data
    var pal :[UInt32]
    var used :Bool
    
    init() {
        DKW2DstData = NSMutableData(length: 256 * 256 * 4)!
        DKW2DstSData = NSMutableData(length: 256 * 256 * 4)!
        tileData = Data(count: 256 * 256 * 4)
        pal = [UInt32](repeating: 0, count: StDKW2Codec.BLOCKSIZE)
        used = false
    }
}
