//
//  aisSettingsController.swift
//  WinGPS Marine
//
//  Created by Standaard on 06/03/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import UIKit

class AisSettingsController: AllSettingsViewController {
	
	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var windowsContainer: UIView!
	

	override func viewDidLoad() {
		super.viewDidLoad()
		windowsContainer.layer.cornerRadius = 8
		windowsContainer.clipsToBounds = true
		
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
	}
    

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
        
//        //with animation:
//        if keyboardSize.height == offset.height {
//            if self.view.frame.origin.y == 0 {
//                UIView.animateWithDuration(0.1, animations: { () -> Void in
//                    self.view.frame.origin.y -= keyboardSize.height
//                })
//            }
//        } else {
//            UIView.animateWithDuration(0.1, animations: { () -> Void in
//                self.view.frame.origin.y += keyboardSize.height - offset.height
//            })
//        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    
	@IBAction func doneButton(_ sender: Any) {
		addTransition()
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func backButton(_ sender: Any) {
		addTransition()
		self.dismiss(animated: false, completion: nil)
//		performSegue(withIdentifier: "unwindSegueToSettings", sender: self)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if (touch?.view == backgroundView){
			addTransition()
			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
			self.dismiss(animated: false, completion: nil)
		}
	}

	
	func addTransition(){
		let transition: CATransition = CATransition()
		transition.duration = 0.2
		transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
		transition.type = CATransitionType.fade
		self.view.window!.layer.add(transition, forKey: nil)
		
	}
}
