//
//  StAIVDMContent.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 27/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class StAIVDMContent {
	
//	var s6b : String?
	var s6b : String = ""

	var content : String = ""
	var totalMsgCount : Int?
	var msgNumber = 1
	var seqMsgId : Int?
	var contentComplete : Bool = false
	
	private var id : Int = -1
	private var repeatIndicator : Int?
	private var mmsi : Int = -1
	private var navStatus : Int = -1
	private var rot : Int?
	private var sog : Int = -1
	private var posAcc : Int?
	private var lon : Int = 108600000
	private var lat : Int = 54600000
	private var cog : Int = -1
	private var trueHeading : Int = -1
	private var timeStamp : Int?
	private var specialManInd : Int?
	private var spare : Int?
	private var raimFlag : Int = -1
	private var comState : Int?
	
	private var unitFlagB : Int = -1
	private var displayFlagB : Int = -1
	private var dscFlagB : Int = -1
	private var bandFlagB : Int = -1
	private var msg22FlagB : Int = -1
	private var modeFlag : Int = -1
	private var comStateFlag : Int = -1
	
	private var aisVersionIndicator : Int?
	private var imoNumber : Int = -1
	private var callSign : String = ""
	private var name : String = ""
	private var ship_cargoType : Int = -1
	private var dimensions : [Int]?
	private var epfDeviceType : Int?
	private var eta : Int = -1
	private var maxStaticDraught : Int = -1
	private var destination : String = ""
	private var dte : Int?
	private var vendorId : String = ""
	
	private var safetyRelatedText : String = ""
	private var aisClass : String = ""
	private var mTime : Int64 = 0
	private var typeOfAtoN : Int?
	private var positionAccuracy : Int?
	private var typeOfEPosFixDevice : Int?
	private var offPosIndicator : Int?
	private var atoNStatus : Int?
	private var virtualAtoNFlag : Int?
	private var assignedModeFlag : Int?
	
	init(aivdm : StAIVDM) {
//		self.mTime = aivdm.getTime()
		self.mTime = aivdm.time
		self.content = aivdm.content
		self.totalMsgCount = aivdm.totalMsgCount
		self.seqMsgId = aivdm.seqMsgId
		
		if(totalMsgCount == 1){
			contentComplete = true
			decode(content: self.content)
		}
	}
	
	func addContent(_ aivdm :StAIVDM ) -> Bool {
		
		self.mTime = aivdm.time
		var result = false
		if (aivdm.msgNumber - self.msgNumber) == 1 {
			content = content + aivdm.content
			self.msgNumber = aivdm.msgNumber
			result = true
		}
		
		if self.msgNumber == totalMsgCount {
			contentComplete = true
			decode(content: content)
		}
		
		return result
	}
	
	
	func getAISObject() -> AisObject? {
		if mmsi > 0 && id != 14 {
			var ao :AisObject = AisObject(mmsi: mmsi)
			ao.id = id
			ao.navStatus = navStatus
//			ao.setLon(aAISMinutes: lon)
//			ao.setLat(aAISMinutes: lat)
			ao.setPos(lonMinutes: lon, latMinutes: lat)
			ao.callSign = callSign
			ao.name = name
            if (trueHeading > -1 && trueHeading < 511 ){
                ao.setTrueHeading(Float(trueHeading))
            }
			ao.setCog(Float(cog))
//			ao.sog = Float(sog)
			ao.setSog(Float(sog))
			ao.type = ship_cargoType
			ao.timeStamp =  timeStamp
			if aisClass == "A" { ao.color = UIColor.blue  }
			if aisClass == "B" { ao.color = UIColor.white }
			if mTime > 0 {
				ao.mTime = mTime
			}
			return ao
		} else if String(mmsi).subString(from: 0, to: 2) == "97" && id == 14 {
			var ao :AisObject = AisObject(mmsi: mmsi)
			ao.id = id
			ao.navStatus = 14
			ao.safetyRelatedText = safetyRelatedText
			if mTime > 0 {
				ao.mTime = mTime
			}
			return ao
		} else {
			return nil
		}
	}
	
	func decode(content: String){
		s6b = StNMEAUtils.stringToSixbit(string: content)
		
		if(s6b.count > 6){
			id = StNMEAUtils.bti(string: s6b, start: 0, end: 6)
		}
		
		if(id > 0 && id <= 3){
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			navStatus = StNMEAUtils.bti(string: s6b, start: 38, end: 42)
			rot = StNMEAUtils.bti(string: s6b, start: 42, end: 50)
			sog = StNMEAUtils.bti(string: s6b, start: 50, end: 60)
			posAcc = StNMEAUtils.bti(string: s6b, start: 60, end: 61)
			lon = StNMEAUtils.btpos(string: s6b, start: 61, end: 89)
			lat = StNMEAUtils.btpos(string: s6b, start: 89, end: 116)
			cog = StNMEAUtils.bti(string: s6b, start: 116, end: 128)
			trueHeading = StNMEAUtils.bti(string: s6b, start: 128, end: 137)
			timeStamp = StNMEAUtils.bti(string: s6b, start: 137, end: 143)
			specialManInd = StNMEAUtils.bti(string: s6b, start: 143, end: 145)
			spare = StNMEAUtils.bti(string: s6b, start: 145, end: 148)
			raimFlag = StNMEAUtils.bti(string: s6b, start: 148, end: 149)
			if(s6b.count >= 168){
				comState = -1
			} else {
				comState = StNMEAUtils.bti(string: s6b, start: 149, end: 168)
			}
			aisClass = "A"
		} else if(id == 5) {
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			aisVersionIndicator = StNMEAUtils.bti(string: s6b, start: 38, end: 40)
			imoNumber = StNMEAUtils.bti(string: s6b, start: 40, end: 70)
			callSign = StNMEAUtils.bts(string: s6b, start: 70, end: 112)
			name = StNMEAUtils.bts(string: s6b, start: 112, end: 232)
			ship_cargoType = StNMEAUtils.bti(string: s6b, start: 232, end: 240)
			dimensions = StNMEAUtils.bitsToDimensions(string: s6b, start: 240, end: 270)
			epfDeviceType = StNMEAUtils.bti(string: s6b, start: 270, end: 274)
			eta = StNMEAUtils.bti(string: s6b, start: 274, end: 294)
			maxStaticDraught = StNMEAUtils.bti(string: s6b, start: 294, end: 302)
			destination = StNMEAUtils.bts(string: s6b, start: 302, end: 422)
			dte = StNMEAUtils.bti(string: s6b, start: 422, end: 423)
			spare = StNMEAUtils.bti(string: s6b, start: 423, end: 424)
			aisClass = "A"
		} else if(id == 14) {
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			spare = StNMEAUtils.bti(string: s6b, start: 38, end: 40)
			safetyRelatedText = StNMEAUtils.bts(string: s6b, start: 40, end: s6b.count)
			
		} else if(id == 18) {
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			spare = StNMEAUtils.bti(string: s6b, start: 38, end: 46)
			sog = StNMEAUtils.bti(string: s6b, start: 46, end: 56)
			posAcc = StNMEAUtils.bti(string: s6b, start: 56, end: 57)
			lon = StNMEAUtils.btpos(string: s6b, start: 57, end: 85)
			lat = StNMEAUtils.btpos(string: s6b, start: 85, end: 112)
			cog = StNMEAUtils.bti(string: s6b, start: 116, end: 124)
			trueHeading = StNMEAUtils.bti(string: s6b, start: 124, end: 133)
			timeStamp = StNMEAUtils.bti(string: s6b, start: 133, end: 139)
			spare = StNMEAUtils.bti(string: s6b, start: 139, end: 141)
			unitFlagB = StNMEAUtils.bti(string: s6b, start: 141, end: 142)
			displayFlagB = StNMEAUtils.bti(string: s6b, start: 142, end: 143)
			dscFlagB = StNMEAUtils.bti(string: s6b, start: 143, end: 144)
			bandFlagB = StNMEAUtils.bti(string: s6b, start: 144, end: 145)
			msg22FlagB = StNMEAUtils.bti(string: s6b, start: 145, end: 146)
			modeFlag = StNMEAUtils.bti(string: s6b, start: 146, end: 147)
			raimFlag = StNMEAUtils.bti(string: s6b, start: 147, end: 148)
			comStateFlag = StNMEAUtils.bti(string: s6b, start: 148, end: 149)
			if(s6b.count < 168) {
				comState = StNMEAUtils.bti(string: s6b, start: 149, end: 168)
			} else {
				comState = -1
			}
			aisClass = "B"
		} else if(id == 21) {
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			typeOfAtoN = StNMEAUtils.bti(string: s6b, start: 38, end: 43)
			name = StNMEAUtils.bts(string: s6b, start: 43, end: 163)
			posAcc = StNMEAUtils.bti(string: s6b, start: 163, end: 164)
			lon = StNMEAUtils.btpos(string: s6b, start: 164, end: 192)
			lat = StNMEAUtils.btpos(string: s6b, start: 192, end: 219)
			dimensions = StNMEAUtils.bitsToDimensions(string: s6b, start: 219, end: 249)
			typeOfEPosFixDevice = StNMEAUtils.bti(string: s6b, start: 249, end: 253)
			timeStamp = StNMEAUtils.bti(string: s6b, start: 253, end: 259)
			offPosIndicator = StNMEAUtils.bti(string: s6b, start: 259, end: 260)
			atoNStatus = StNMEAUtils.bti(string: s6b, start: 260, end: 268)
			raimFlag = StNMEAUtils.bti(string: s6b, start: 268, end: 269)
			virtualAtoNFlag = StNMEAUtils.bti(string: s6b, start: 269, end: 270)
			assignedModeFlag = StNMEAUtils.bti(string: s6b, start: 270, end: 271)
			spare = StNMEAUtils.bti(string: s6b, start: 271, end: 272)
			name = String(name + StNMEAUtils.bts(string: s6b, start: 272))
			
			aisClass = "AtoN"
		} else if(id == 24) {
			repeatIndicator = StNMEAUtils.bti(string: s6b, start: 6, end: 8)
			mmsi = StNMEAUtils.bti(string: s6b, start: 8, end: 38)
			let partNr = StNMEAUtils.bti(string: s6b, start: 38, end: 40)
			if(partNr == 0){
				name = StNMEAUtils.bts(string: s6b, start: 40, end: 160)
			} else if(partNr == 1){
				ship_cargoType = StNMEAUtils.bti(string: s6b, start: 40, end: 48)
				vendorId = StNMEAUtils.bts(string: s6b, start: 48, end: 90)
				callSign = StNMEAUtils.bts(string: s6b, start: 90, end: 132)
				dimensions = StNMEAUtils.bitsToDimensions(string: s6b, start: 132, end: 162)
				spare = StNMEAUtils.bti(string: s6b, start: 162, end: 168)
			}
			aisClass = "B"
		}
	}
	
	func getID() -> Int {
		return id
	}
	
	func getMMSI() -> Int {
		return mmsi
	}
	
	func getNavStatus() -> Int {
		return navStatus
	}
	
	func getLongitude() -> Int {
		return lon
	}
	
	func getLatitude() -> Int {
		return lat
	}
	
	func getCS() -> String {
		return callSign
	}
	
	func getName() -> String {
		return name
	}
	
	func getCog() -> Int {
		return cog
	}
	
	func getTrueHeading() -> Int {
		return trueHeading
	}
	
	func getSog() -> Int {
		return sog
	}
	
	func getType() -> Int {
		return ship_cargoType
	}
	
	func getTimeStamp() -> Int? {
		return timeStamp
	}
	
	func getSafetyRelatedText() -> String {
		return safetyRelatedText
	}
	
	func getAisClass() -> String {
		return aisClass
	}
	
	func getDimensions() -> [Int]? {
		return dimensions
	}
	
	func getTime() -> Int64 {
		return mTime
	}
	
	func getDestination() -> String {
		return destination
	}
	
	func getMaxStaticDraught() -> Int {
		return maxStaticDraught
	}
	
	func getEta() -> Int {
		return eta
	}
	
	func getCargoType() -> Int {
		return ship_cargoType
	}
	
	func getVendorId() -> String {
		return vendorId
	}
	
	func getImoNumber() -> Int {
		return imoNumber
	}
	
}
