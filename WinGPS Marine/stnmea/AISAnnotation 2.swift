//
//  AISAnnotation.swift
//  WinGPS Marine
//
//  Created by Standaard on 17/01/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class AISAnnotation: MKPointAnnotation {
	
	static let imageSize = 24.0
	static let markerSize = 15.0 // original: 22

	var aisObject :AisObject
	var aisAnnotationView :AISAnnotationView?
	
	var textTooSmall :Bool = false

	var inPreviousView :Bool = false
	var inNewView :Bool = false

	init(_ aisObject: AisObject){
		self.aisObject = aisObject
		
		super.init()
		
	}
	
	func getImageView(fillColor :UIColor) -> UIImage {
		
		let height :CGFloat = CGFloat(AISAnnotation.imageSize)
		let width :CGFloat = CGFloat(AISAnnotation.imageSize)
		let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		
		let context = UIGraphicsGetCurrentContext()!
		
		context.translateBy(x: width/2, y: 0)
		context.setLineWidth(0.5)

		context.setFillColor(fillColor.cgColor)
		context.setStrokeColor(UIColor.black.cgColor)

		context.move(to: CGPoint(x: 0, y: 2))
		context.addLine(to: CGPoint(x: (6/22) * AISAnnotation.markerSize, y: AISAnnotation.markerSize))

//		context.addLine(to: CGPoint(x: 0, y: 18))
		context.addLine(to: CGPoint(x: -(6/22) * AISAnnotation.markerSize, y: AISAnnotation.markerSize))
		context.addLine(to: CGPoint(x: 0, y: 2/22 * AISAnnotation.markerSize))

		context.drawPath(using: .fillStroke)
		
		context.translateBy(x: 0, y: -(height / 2))
		
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return UIImage(cgImage: (image?.cgImage)!)
		
	}
}
