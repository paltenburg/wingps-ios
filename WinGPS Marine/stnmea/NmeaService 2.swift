//
//  NmeaService.swift
//  WinGPS Marine
//
//  Created by Standaard on 05/12/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation

class Nmea :NSObject{
	static let service = Nmea()
	
	var aisObjects = [AisObject]()
	var aivdmContent = [StAIVDMContent]()
	
	var aisListChanged = false
    
    var aisObjectListNeedsReSorting = false

	var overlay :AISOverlay?
	
	private override init(){
		super.init()
	}
	
	func onNmeaReceived(msgTxt :String) {
		onNmeaReceived(time: 0, msgTxt :msgTxt)
	}
	
	func onNmeaReceived(time :Int64, msgTxt :String) {

		if msgTxt.count < 1 { return }
		
		if msgTxt[0] == "$" {
			var nmea = StNMEA(time: time, s: msgTxt)

//			String instrString = "";
//			for(int dev : conn.getDevices()){
//				instrString += Integer.toString(dev) + "\n";
//			}
//
//			if(nmea.getType().equals("MWV"))Log.d("WIND", "NMEA Type: " + nmea.getType() + "\nConn: " + conn.getName() + "\nInstruments: " + instrString + "\nDevice Flags: " + conn.getDeviceFlags());
//
//			if (nmea.getType().equals("GPRMC") && !(conn.getDevices().contains(StNmeaDevice.NMEA_GPS)))
//			Log.e("popke", "StService: wel GPRMC, maar !(conn.getDevices().contains(StNmeaDevice.NMEA_GPS))");
//			if (nmea.getType().equals("GPRMC") && (conn.getDevices().contains(StNmeaDevice.NMEA_GPS))) {
//				//Log.d("popke", "onNmeaReceived "+msgTxt);
//
//				StGPRMC RMC = nmea.getStGPRMC();
//
//				//gprmc.setCog(80f);
//				//gprmc.setSog(10f);
//				//Log.d(TAG, "SETTING RMC!");
//
//				//location.setCog((float) (Math.PI/3));
//				//location.setSog(10);
//
//				//Log.d("popke", "onNmeaReceived gpsFilter: "+String.valueOf(gpsFilter));
//				sendWearValues(RMC);
//				if (location == null) setLocation(StLocation.getLocation(RMC));
//				else location.updateNmea(RMC, gpsFilter);
//
//				//Log.d("StNavTrack", "Gprmc msgText");
//
//				// test
//				////showSartDialog(0, "");
//
//				if (RMC.getStatus().equals("A")) {
//					// New location, update everything
//
//					ledStatus = Ledstatus.green;
//					led.blink(ledStatus);
//
//					// if (location == null) location = new StLocation(gprmc);
//					//  else location.updateNmea(gprmc, gpsFilter);
//
//					//addToTrack(mTrackList.get(0), location.clone());
//					if (tracking()) addToTrack(mCurrentTrack, location.clone());
//
//					setRMCStrings(RMC);
//					//sendWearValues(RMC);
//					alarmCounter = 0;
//					navWpTtg = 0;
//					if (location.getSog() > 0 && getNavPoint(mNavPoint))
//					navWpTtg = (int) Math.round(location.getDistanceTo(mNavPoint) / location.getSog());
//
//					aisSelf.setLat(location.getLat());
//					aisSelf.setLon(location.getLon());
//
//					// updateMapview location overlay! otherwise that would lag a second
//					Message msg = Message.obtain();
//					msg.what = AppConstants.LOCATIONSERVICE_REPORT;
//					mHandler.sendMessage(msg);
//
//					addCpaToAisObjects();
//					cog = (float) (RMC.getCog());
//
//					hasGprmc  = true;
//					//gprmcLastSeen = System.currentTimeMillis();
//
//					// update Ais distances
//					//                    sortAisListByRange(); // not needed anymore, because we draw all ais targets anyway
//
//				} else {
//					ledStatus = Ledstatus.red;
//					led.blink(ledStatus);
//
//					avgSog = (float)(0.9*avgSog)/2;
//					//navWpTtg = 0;
//				}
//
//			} else if (nmea.getType().equals("GPGSV") && (conn.getDevices().contains(StNmeaDevice.NMEA_GPS))){
//				//Log.d("popke", "GPGSV message");
//
//				StGPGSV GSV = nmea.getStGPGSV();
//				//          if(location==null) location = new StLocation(gpgsv); // gpgsv is toch alleen satelieten?
//				//          else location.updateNmea(gpgsv);
//				sat = GSV.getGpsCount();
//				/* GLL and VTG needs work...
//				*           } else if (nmea.getType().equals("GPGLL")
//				&& (conn.getDevices().contains(StNmeaDevice.NMEA_GPS))
//				&& (hasGprmc == false || System.currentTimeMillis() - gprmcLastSeen > gprmcLastSeenTimeout)){
//				if (hasGprmc) hasGprmc = false;
//
//
//				//TODO! dubbele code met gprmc er uit halen, VTG er in
//				gpgll = nmea.getStGPGLL();
//
//				if (location == null) location = new StLocation(gpgll);
//				else location.updateGPGLL(gpgll, gpsFilter);
//
//				if (gpgll.getStatus().equals("A")) {
//				ledStatus = Ledstatus.green;
//				led.blink(ledStatus);
//				if (tracking()) addToTrack(mCurrentTrack, location.clone());
//
//				if (gprmc == null) gprmc = new StGPRMC(new String[0]);
//				gprmc.setLat(gpgll.getLat());
//				gprmc.setLon(gpgll.getLon());
//				setGprmcStrings();
//				alarmCounter  = 0;
//				navWpTtg = Math.round(location.getDistanceTo(navWaypoint)*location.getSog());
//				Message msgText = Message.obtain();
//				msgText.what = AppConstants.LOCATIONSERVICE_REPORT;
//				mHandler.sendMessage(msgText);
//				} else {
//				ledStatus = Ledstatus.red;
//				led.blink(ledStatus);
//				}
//				addCpaToAisObjects();
//
//				} else if (nmea.getType().equals("GPVTG")
//				&& (conn.getDevices().contains(StNmeaDevice.NMEA_GPS))
//				&& (hasGprmc == false || System.currentTimeMillis() - gprmcLastSeen > gprmcLastSeenTimeout)){
//				if (hasGprmc) hasGprmc = false;
//
//				*/
//			} else if (nmea.getType().equals("GSA") && nmea.getId().equals("GP") && (conn.getDevices().contains(StNmeaDevice.NMEA_GPS))){
//				//Log.d("GSA", "GPGSA Message");
//				StGSA gsa = nmea.getStGSA();
//
//				sSatUsed = Integer.toString(gsa.getSatsUsed());
//
//			} else if (nmea.getType().equals("BWC") && (conn.getDevices().contains(StNmeaDevice.NMEA_AUTOPILOT))){
//				//          stbwc = nmea.getStBWC();
//
//			} else if (nmea.getType().equals("DBT" ) && (conn.getDevices().contains(StNmeaDevice.NMEA_DEPTH))){
//				StDBT DBT = nmea.getStDBT();
//				mDepthPrev = mDepth;
//				mUKC = DBT.getDepthMeters() - mOffsetKeel;
//				mDepth = mUKC + mOffsetKeel + mOffsetSurface;
//				setDepthStrings();
//				if (StSettingsInterface.settings().getAlarmDepthEnabled())
//				checkDepthAlarm(mDepth);
//			} else if (nmea.getType().equals("DPT") && (conn.getDevices().contains(StNmeaDevice.NMEA_DEPTH))){
//				StDPT DPT = nmea.getStDPT();
//				mDepthPrev = mDepth;
//				mUKC = DPT.getDepthMeters() - mOffsetKeel;
//				mDepth = mUKC + mOffsetKeel + mOffsetSurface;
//				setDepthStrings();
//				if (StSettingsInterface.settings().getAlarmDepthEnabled())
//				checkDepthAlarm(mDepth);
//			} else if (nmea.getType().equals("HDG") && (conn.getDevices().contains(StNmeaDevice.NMEA_COMPASS))){
//				StHDG HDG = nmea.getStHDG();
//				trueHeading = HDG.getTrueHeading();
//				if(location!=null) {
//					if (HDG.getVariationUnknownOrZero() && mApplyDeclination){
//						trueHeading += location.getMagneticDeclinationDegrees();
//					}
//					location.setTrueHeading(trueHeading);
//				}
//				Log.d("HDG", "TRUE HEADING: " + trueHeading);
//				setHeadingStrings();
//			} else if (nmea.getType().equals("HDT") && (conn.getDevices().contains(StNmeaDevice.NMEA_COMPASS))){
//				StHDT HDT = nmea.getStHDT();
//				trueHeading = HDT.getTrueHeading();
//				if(location!=null) {
//					if (!HDT.getTrue() && mApplyDeclination){
//						trueHeading += location.getMagneticDeclinationDegrees();
//					}
//					location.setTrueHeading(trueHeading);
//				}
//				setHeadingStrings();
//			} else if (nmea.getType().equals("MWV") && (conn.getDevices().contains(StNmeaDevice.NMEA_WIND))){
//				StMWV MWV = nmea.getStMWV();
//				if (MWV.getStatus().equals("A") && MWV.getReference().equals("R")) {
//					if(location!=null)
//					location.setMwv(MWV);
//					Log.d(TAG, "Setting MWV strings");
//					setMwvStrings();
//				}
//			} else if (nmea.getType().equals("VHW") && (conn.getDevices().contains(StNmeaDevice.NMEA_LOG))){
//				//Log.d("popke", "VHW message");
//				StVHW VHW = nmea.getStVHW();
//				speedThroughWater = VHW.getSpeedKmhr();
//				logLastUpdated = System.currentTimeMillis();
//			}
//			//Log.d("GPSDATA", msgTxt);
//
		}
		
		if msgTxt[0] == "!" {

			var nmea = StNMEA(time: StUtils.currentTimeMillis(), s: msgTxt)
			//Log.d(TAG, "nmea created");
			
			//Log.d(TAG, "Devices: " + conn.getDevices().size() + " : " + conn.getName() + " : " + conn.getConnectionType());
			if nmea.type == "AIVDM" { /////////////  && (conn.getDevices().contains(StNmeaDevice.NMEA_AIS))) {
				//Log.d(TAG, "Creating aivdm");
				var aivdm = nmea.stAIVDM
				//Log.d(TAG, "aivdm created");
				
				if(!addAivdmContent(aivdm)){
					//Log.d(TAG, "Error on sequent msgText");
				}
			}
			//sendUpdate(msgTxt);
		}
		//sendUpdate(msgTxt, conn);
		
		//// refer to marine about list handling and time-outs
		
	}

	
	func addAivdmContent(_ aivdm :StAIVDM?) -> Bool {
	
		if let aivdm = aivdm,
			aivdm.msgNumber ?? 0 > 1 {
			
			var i :Int = 0
			var found = false
			
			/*for(int x = 0; x<aisObjects.size()-1; x++){
			Log.d(TAG, x + ": " + aisObjects.get(i).getMMSI());
			}*/
			
			while !found && i < aivdmContent.count {
				
				if aivdm.seqMsgId == aivdmContent[i].seqMsgId {
					found = true
					
					////////// debugging: single out one ship //////////
//					if aivdmContent[i].getMMSI() != 244730297 { return true }
					
					if !aivdmContent[i].addContent(aivdm) {
						
						aivdmContent.remove(at: i)
						
					} else {
						//Log.d(TAG, "PART 2 : " + aivdm.getMsgId());
						if aivdmContent[i].contentComplete {
							updateList(aivdmContent[i])
							//Log.d(TAG, "Merged msgText added to list for: " + aivdmContent.get(i).getMMSI() + " With msgId: " + aivdmContent.get(i).getID());
							aivdmContent.remove(at: i)
						}
					}
					
				} else {
					i += 1
				}
			}
			if !found {
				return false
			}
			
		} else if let aivdm = aivdm {
			var content :StAIVDMContent = StAIVDMContent(aivdm: aivdm)
			
			////////// debugging: single out one ship //////////
//			if content.getMMSI() != 244730297 { return true }
			
			if content.contentComplete {
				updateList(content)
				dout("nmea message ais update \(content.getMMSI()) \(content.getName()) \(content.getSog())")
			} else {
				aivdmContent.append(content)
				dout("nmea message ais added  \(content.getMMSI()) \(content.getName()) \(content.getSog())")

			}
			
			overlay?.updateDrawList()
		}
		return true;
	}
	
	func updateList(_ content :StAIVDMContent) -> Bool {
		var changed = false
		
		// TODO implement SART
//		if content.getID() == 14 {
//			showSartDialog(content.getMMSI(), content.getSafetyRelatedText())
//			//sartAlerts.add(content.getAISObject());
//		}
		
		if aisObjects.count > 0 {
			var i = 0
			var found = false
			/*for(int x = 0; x<aisObjects.size()-1; x++){
			Log.d(TAG, x + ": " + aisObjects.get(i).getMMSI());
			}*/
			while !found && i < aisObjects.count {
				
				if content.getMMSI() == aisObjects[i].mmsi {
					found = true
					aisObjects[i].setNewValues(aivdm: content)
					
					// update the mapview annotation, if it is already created
					aisObjects[i].updateAnnotation()
					
					changed = true
				} else {
					i += 1
				}
			}
			if !found {
				changed = addAisObject(content)
			}
		} else {
			changed = addAisObject(content)
		}
		
		if changed {
			aisListChanged = true
		}
		return changed
	}
	
	func addAisObject(_ content :StAIVDMContent) -> Bool {
		
		var ao :AisObject? = content.getAISObject()
		if ao != nil {
			if let ao = ao,
				ao.posOK() {

				aisObjects.append(ao)
                
				aisObjectListNeedsReSorting = true
                
			}
			return true
		} else {
			return false
		}
		
	}
	
//	private void addCpaToAisObjects(){
//	if (location != null)
//	for(AISObject ao : aisObjects){ // TODO concurrentmodificationexception
//	ao.updateCpa(mCpaTime, location.getLat(), location.getLon(), location.getCog(), location.getSog(), mCpa);
//	}
//	}

    func userLocationUpdated(){
         
        // invalidate ranges of ais objects
        for aisObject in aisObjects {
            aisObject.invalidateDistance()
        }
        aisObjectListNeedsReSorting = true
    }
}
