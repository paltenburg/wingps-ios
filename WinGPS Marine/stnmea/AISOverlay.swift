//
//  AISOverlay.swift
//  WinGPS Marine
//
//  Created by Standaard on 19/12/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class AISOverlay {
    
    //    static let aisShipTimeOut = 6 * 60 * 1000// 6 minutes in ms
    //    static let aisShipTimeOut = 10 * 60 * 1000// 10 minutes in ms
    
    static let aisShipTimeOut = !StUtils.isDebug() ? 10 * 60 * 1000 // 10 minutes in ms
        //        : 1 * 60 * 1000// 10 minutes in ms
        : 10 * 60 * 1000
    
    //    static let aisShipTimeOut = 1 * 1000// secs in ms
    //    static let aisSogThreshold :Float = 0.01
    static let aisSogThreshold = 0.3 / Units.SPEED_CONVERSION_TO_KNOTS // WinGPS 5 uses a threshold of 0.3 knots.
    
    var vc :ViewController
    
    var aisAnnotations = [AISAnnotation]()
    let aisAnnotationsLock = LockWithClosure()
    
    
    var mapviewAnnotationsCopy = [AISAnnotation]()
    
    var updateTimestampLastUpdate = DispatchTime.now()
    var nextUpdatePlanned :Bool = false
    
    var updateTimer :Timer?
    
    var aisTargetListController :AisTargetListController?
    
    init(_ viewController :ViewController) {
        self.vc = viewController
        
        Nmea.service.overlay = self
        
        setOverlays()
    }
    
    
    //	func onSingleTap(_ sender :UIGestureRecognizer, tappedPoint :CGPoint, tappedLocation :CLLocationCoordinate2D){
    //
    //		guard let editRoute = self.editRoute else { return }
    //
    //		// check if tapped on route point -> make draggable
    //		if let routePoint = touchingRoutePoint(point: tappedPoint, inRoute: editRoute) {
    //			// touched a routepoint
    //			RPDrag = routePoint
    //		} else { // not tapped on existing point, so create new
    //
    //			// check if tapped on route leg -> insert new point
    //
    //			var RPAfter :RoutePoint? = nil
    //
    //			// check wwnetwork
    //			var section :WWSection? = nil
    //			var sectionPos :Double = 0
    //
    //			if let rpAfter = RPAfter  {
    //				var index = editRoute.findRoutePoint(rpAfter)
    //				RPDrag = editRoute.add(indexParam: index, location: tappedLocation, name: "", section: section, sectionPos: sectionPos)
    //			} else {
    //				// else: append new route point to end
    //				RPDrag = editRoute.add(location: tappedLocation, name: "", section: section, sectionPos: sectionPos)
    //			}
    //		}
    //
    //		if let da = self.dragAnnotation {
    //			vc.mapView.removeAnnotation(da)
    //		} else {
    //			self.dragAnnotation = DraggingAnnotation()
    //		}
    //
    //		if let da = self.dragAnnotation {
    //			da.coordinate = RPDrag!.position
    //			//			da.title = "Routepoint"
    //			vc.mapView.addAnnotation(da)
    //		}
    //
    //		editRoute.needsSaving = true
    //
    //		// Refresh overlay
    //		setOverlays(for: editRoute)
    //	}
    
    // create or update overlays
    func setOverlays() {
        
        // TODO add/update annotations
        
        
        
        ////		var oldOverlays = route.getAllOverlays()
        //		vc.mapView.addOverlays(generateOverlays())
        //		vc.mapView.removeOverlays(oldOverlays)
        //
        //		// create or update annotations
        //		var newAnnotations = route.updateAnnotations()
        //		vc.mapView.addAnnotations(newAnnotations)
        //
        //		if route.routePointAnnotations.count == route.routePoints.count + 1 {
        //			let annotationToRemove = route.routePointAnnotations.remove(at: route.routePointAnnotations.count - 1 )
        //			// remove last annotation
        //			vc.mapView.removeAnnotation(annotationToRemove)
        //		}
        
        runDrawListUpdater()
    }
    
    func runDrawListUpdater() {
        //if (mAisDrawListUpdater != null && !mAisDrawListUpdater.isCancelled() && mAisDrawListUpdater.getStatus() != AsyncTask.Status.FINISHED)
        // Make sure the updater isn't busy or cancelled
        
        //		mAisDrawListUpdater.cancel(true);
        
        //		StService service = MainActivity.instance.mBoundService;
        //		if (service == null || service.getAisObjects() == null || service.getAisObjects().size() == 0) return;
        // Make sure there are > 0 AIS objects
        
        
        //		for aisObject in Nmea.service.aisObjects {
        //			if let annotation = aisObject.aisAnnotation {
        //				aisAnnotations.append(annotation)
        //			}
        //		}
        //		vc.mapView.addAnnotations( aisAnnotations )
        
        updateDrawList()
        
    }
    
    func updateDrawList() {
        
        // don't update more often than a certain frequency
        //        let minTimeBetweenUpdates = DispatchTimeInterval.seconds(1) // TODO: back to 1 sec
        let minTimeBetweenUpdates :TimeInterval = 1 // TODO: back to 1 sec
        
        //        StaticQueues.aisUpdateDrawListLock.sync {
        
        if !nextUpdatePlanned {
            var timestamp = DispatchTime.now()
            if timestamp > updateTimestampLastUpdate + minTimeBetweenUpdates  {
                updateTimestampLastUpdate = timestamp
                
                dout("Running updateDrawList from inside locked function")
                updateDrawListPrivate(vc.getMapRectGeo())
                
                // schedule next update in one minute to check for timed-out targets
                // if already running, cancel and reset
                if let updateTimer = self.updateTimer {
                    updateTimer.invalidate()
                }
                self.updateTimer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: false) { timer in
                    self.updateDrawList()
                }
            } else {
                // check if the next one is already planned, and if not, plan it.
                
                //                    StaticQueues.aisUpdateQueueLock.sync{
                dout("updateDrawList(): scheduling next update \(DispatchTime.now().uptimeNanoseconds / 1000000)")
                //                                               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(minTimeBetweenUpdates * 1000)), execute: {
                //                        DispatchQueue.main.asyncAfter(deadline: updateTimestampLastUpdate + minTimeBetweenUpdates, execute: {
                let nextUpdateTimer = Timer.scheduledTimer(withTimeInterval: minTimeBetweenUpdates, repeats: false) { timer in
                    
                    dout("Running updateDrawList from inside locked function scheduled \(DispatchTime.now().uptimeNanoseconds / 1000000)")
                    
                    self.updateDrawListPrivate(self.vc.getMapRectGeo())
                    
                    self.updateTimestampLastUpdate = DispatchTime.now()
                    self.nextUpdatePlanned = false
                }
                //                })
                nextUpdatePlanned = true
                //                    }
            }
        }
        //        }
    }
    
    private func updateDrawListPrivate(_ mapRectGeo :CGRect) {
        
        // Update Target list
        aisTargetListController?.refreshList()
        
        // Update Target Info
        // TODO
        
        dout(". 1")
        // When disabled, remove waypoints from view one by one
        if !AisSettingsController.aisEnabled {
            self.aisAnnotationsLock.runLocked({
                for annotation in aisAnnotations {
                    StaticQueues.mapviewAnnotationsLock.runLocked({ self.vc.mapView.removeAnnotation(annotation) })
                }
            })
            return
        }
        
        // async {
        
        // if ((!filterMovingShips || (filterMovingShips && ao.getSog() >= minSpeed)) && System.currentTimeMillis()-ao.getTime() < mAisShipTimeOut) {
        
        // if within screen location
        
        // add to draw list
        
        //}
        
        //		for annotation in vc.mapView.annotations {
        //			if let annotation = annotation as? AISAnnotation {
        //				mapviewAnnotationsCopy.append(annotation)
        //			}
        //		}
        //		mapviewAnnotationsCopy = aisAnnotations
        
        
        
//        StaticQueues.aisAnnotationsBGQueue.async { ////////// DEBUG: try without async
//            StaticQueues.mapviewAnnotationsLock.runLocked({
                
                //				var annotationsCopy = [MKAnnotation]()
                //
                //				DispatchQueue.main.async{
                //					if self.vc.mapView.annotations.count > 0 {
                //						annotationsCopy = self.vc.mapView.annotations
                //					}
                //				}
                
                
                //				var previousWPs :[AISAnnotation] = self.mapviewAnnotationsCopy.filter({$0 is AISAnnotation}).map({$0 as! AISAnnotation})
                //				var previousWPs :[AISAnnotation] = self.aisAnnotations
                //				for annotation in previousWPs {
                //					annotation.inPreviousView = true
                //					annotation.inNewView = false
                //				}
                dout(". 2")
                
                var previousAnnotations = self.aisAnnotations.map{$0}
                self.aisAnnotationsLock.runLocked({
                    for annotation in previousAnnotations {
                        annotation.inPreviousView = true
                        annotation.inNewView = false
                    }
                    self.aisAnnotations = [AISAnnotation]()
                })
                
                //				var newAnnotations = [AISAnnotation]()
                
                if Nmea.service.aisObjectListNeedsReSorting {
                    if let userLoc = self.vc.userLocation {
                        StaticQueues.aisObjectsLock.runLocked({
                            Nmea.service.aisObjects = Nmea.service.aisObjects.sorted(by:
                                                                                        {
                                                                                            //                                $0.position?.distance(coordinate: self.vc.userLocation!.coordinate ) ?? 0 < $1.position?.distance(coordinate: self.vc.userLocation!.coordinate) ?? 0
                                                                                            $0.getDistance(userLocation: userLoc.coordinate) ?? 0 < $1.getDistance(userLocation: userLoc.coordinate) ?? 0
                                                                                        })
                        })
                        Nmea.service.aisObjectListNeedsReSorting = false
                    }
                }
                
                dout(". 3")
                
                for aisObject in Nmea.service.aisObjects {
                    
                    if self.aisAnnotations.count > AisSettingsController.maxTargets {
                        break;
                    }
                    
                    // check if outdated
                    if StUtils.currentTimeMillis() - (aisObject.mTime ?? 0) > AISOverlay.aisShipTimeOut {
                        
                        aisObject.timedOut() // Annotation removed and will be cleaned up below
                    } else {
                        
                        if let pos = aisObject.position {
                            
                            var withinHorizontalRange = pos.longitude > Double(mapRectGeo.origin.x) && pos.longitude < Double(mapRectGeo.origin.x + mapRectGeo.width)
                            if !withinHorizontalRange && mapRectGeo.maxX > 180 {
                                withinHorizontalRange = (pos.longitude + 360) > Double(mapRectGeo.origin.x) && (pos.longitude + 360) < Double(mapRectGeo.origin.x + mapRectGeo.width)
                            }
                            if withinHorizontalRange,
                               pos.latitude > Double(mapRectGeo.origin.y) && pos.latitude < Double(mapRectGeo.origin.y + mapRectGeo.height) {
                                
                                if let annotation = aisObject.aisAnnotation {
                                    if annotation.aisAnnotationView == nil {
                                        dout("Error: AnnotationView = nil")
                                        ///View will be constructed later
                                    }
                                    // update info
                                    //                                    annotation.coordinate = pos // EXC_BAD_ACCESS nr annotations: 319, 313, 308, x
                                    // With the crash above, test if all the vars are still there:
                                    let testPos = pos
                                    let testAnnotation = annotation
                                    let testCoord = testAnnotation.coordinate
                                    // retry the line:
                                    annotation.coordinate = pos
                                    
                                    annotation.aisAnnotationView?.setTextLabel()
                                    annotation.aisAnnotationView?.updateImageView() // if color data is changed, the color is updated here. Also if Sog is changed over threshold
                                    annotation.aisAnnotationView?.setScaleAndRotation(zoom: ViewController.instance?.mapView.getZoom() ?? -1)
                                    
                                    annotation.inNewView = true
                                    self.aisAnnotationsLock.runLocked({
                                        self.aisAnnotations.append(annotation)
                                    })
                                } else {
                                    let annotation = aisObject.getAisAnnotation()
                                    self.aisAnnotationsLock.runLocked({
                                        self.aisAnnotations.append(annotation)
                                    })
                                    annotation.inNewView = true
                                }
                            }
                        }
                    }
                }
                
                dout(". 4")
                
                var annotationsToRemove = previousAnnotations.filter({ !$0.inNewView })
                for a in annotationsToRemove {
                    a.aisObject.aisAnnotation = nil
                }
                //				self.aisAnnotations = self.aisAnnotations.filter({ $0.inNewView })
                dout(". 5")
                
                DispatchQueue.main.async {
                    
                    // update target info if a target is selected
                    if self.vc.selectedAisObject != nil {
                        self.vc.setValuesAISTargetInfo()
                    }
                    
                    dout("UPDATEDRAWLIST: nr of annotations: \(self.aisAnnotations.count) nr of objects: \(Nmea.service.aisObjects.count)")
                    StaticQueues.mapviewAnnotationsLock.runLocked({ self.vc.mapView.removeAnnotations(annotationsToRemove) })
                    dout(". 6")
                    
                    if let mapview = self.vc.mapView {
                        dout(". 7")
                        
                        //debug: crash?
                        
                        StaticQueues.mapviewAnnotationsLock.runLocked({
                            self.aisAnnotationsLock.runLocked({
                                self.vc.mapView.addAnnotations(self.aisAnnotations) // EXC_BAD_ACCESS nr. annotations: 259, 103, 203, 228, 211, 303, 202, 101, 198 , nr of objects: 2312
                            })
                        })
                        dout(". 8")
                        
                    }
                }
//            })
//        }
        
        DispatchQueue.main.async{ self.vc.mapView.setNeedsDisplay() }
    }
    
    func rotationOrZoomDidChange(zoom: Double){
        self.aisAnnotationsLock.runLocked({
            for annotation in self.aisAnnotations {
                if let view = annotation.aisAnnotationView {
                    view.setScaleAndRotation(zoom: zoom)
                }
            }
        })
    }
    
    func rotationDidChange(){
        self.aisAnnotationsLock.runLocked({
            for annotation in self.aisAnnotations {
                if let view = annotation.aisAnnotationView {
                    view.setRotation()
                }
            }
        })
    }
    
    //	func updateOverlays(forPoint routePoint :RoutePoint, inRoute route :Route){
    //		vc.mapView.removeOverlays(route.getOverlays(forRoutePoint: routePoint))
    //		vc.mapView.addOverlays(route.updateOverlays(forRoutePoint: routePoint, inRoute: route))
    //
    //		// update annotations
    //		route.getAnnotation(forRoutePoint: routePoint).coordinate = routePoint.position
    //	}
    //
    //	func touchingRoutePoint(point :CGPoint) -> RoutePoint? {
    //		for route in Routes.service.routes {
    //			if let touchedRP = touchingRoutePoint(point: point, inRoute: route) {
    //				return touchedRP
    //			}
    //		}
    //		return nil
    //	}
    //
    //	func touchingRoutePoint(point :CGPoint, inRoute editRoute :Route) -> RoutePoint? {
    //
    //		let hitRadius = 50
    //
    //		for routePoint in editRoute.routePoints {
    //			var screenPos = vc.mapView.convert(routePoint.position, toPointTo: vc.mapView)
    //
    //			if screenPos.distanceTo(point) < Double(hitRadius) {
    //				return routePoint
    //			}
    //		}
    //		return nil
    //	}
    //
    //	//	func refreshOverlays(){
    //	//		var oldOverlays = vc.mapView.overlays
    //	//		var newOverlays = Routes.service.getAllOverlays()
    //	//		for ol in oldOverlays {
    //	//			if !(ol is StRoutePolyline),
    //	//			!(ol is StRoutePointCircle) {
    //	//				newOverlays.append(ol)
    //	//			}
    //	//		}
    //	//		let serialQueue = DispatchQueue(label: "com.test.mySerialQueue")
    //	//
    //	//		vc.mapView.removeOverlays(vc.mapView.overlays)
    //	//		vc.mapView.addOverlays(newOverlays)
    //	//
    //	//	}
    //
    //
    //	func makeRoutePointLongpressMenu(stackView :inout UIStackView, routePoint :RoutePoint) {
    //
    //		longPressedRoutePoint = routePoint
    //
    //		//// show route options: start route from here, edit route, remove route point, remove route ////
    //
    //		for sv in stackView.arrangedSubviews {
    //			sv.removeFromSuperview()
    //		}
    //
    //		//		var startRouteButton = UIButton(frame: CGRect(x:0, y: 0, width: 100, height: 50))
    //
    //		if Routes.service.activeRoutePoint?.route === routePoint.route {
    //			var stopRouteButton = UIButton(type: .system)
    //			stopRouteButton.setTitle("Stop Route", for: .normal)
    //			stopRouteButton.addTarget(self, action: #selector(stopRouteButtonTapped), for: .touchUpInside)
    //			stackView.addArrangedSubview(stopRouteButton)
    //		}
    //
    //		//		if Routes.service.activeRoutePoint?.route !== routePoint.route {
    //
    //		if Routes.service.activeRoutePoint !== routePoint {
    //			var startRouteButton = UIButton(type: .system)
    //			startRouteButton.setTitle("Start Route From Point", for: .normal)
    //			startRouteButton.addTarget(self, action: #selector(startRouteButtonTapped), for: .touchUpInside)
    //			stackView.addArrangedSubview(startRouteButton)
    //		}
    //
    //		//		}
    //
    //		var editRouteButton = UIButton(type: .system)
    //		editRouteButton.setTitle("Edit Route", for: .normal)
    //		editRouteButton.addTarget(self, action: #selector(editRouteButtonTapped), for: .touchUpInside)
    //		stackView.addArrangedSubview(editRouteButton)
    //
    //		var removeRoutePointButton = UIButton(type: .system)
    //		removeRoutePointButton.setTitle("Remove Route Point", for: .normal)
    //		removeRoutePointButton.addTarget(self, action: #selector(removeRoutePoint), for: .touchUpInside)
    //		stackView.addArrangedSubview(removeRoutePointButton)
    //
    //		var removeRouteButton = UIButton(type: .system)
    //		removeRouteButton.setTitle("Remove Route", for: .normal)
    //		removeRouteButton.addTarget(self, action: #selector(removeRoute), for: .touchUpInside)
    //		stackView.addArrangedSubview(removeRouteButton)
    //
    //		return
    //
    //		//		let stackView = UIStackView(frame: CGRect(x:0, y: 0, width: 100, height: 50))
    //		//		stackView.axis = .vertical
    //		//		stackView.alignment = .fill // .Leading .FirstBaseline .Center .Trailing .LastBaseline
    //		//		stackView.distribution = .fill // .FillEqually .FillProportionally .EqualSpacing .EqualCentering
    //		////		stackView.distribution  = UIStackViewDistribution.equalSpacing
    //		//		stackView.alignment = UIStackView.Alignment.center
    //		//		stackView.spacing   = 16.0
    //		//
    //		//		let label = UIButton(type: .custom)
    //		//		label.setTitle("Label", for: .normal)
    //		//		stackView.addArrangedSubview(label)
    //		//		let label2 = UILabel()
    //		//		label2.text = "Label 2"
    //		//		stackView.addArrangedSubview(label2)
    //		//
    //		//		// for horizontal stack view, you might want to add width constraint to label or whatever view you're adding.
    //		//		return stackView
    //
    //	}
    //
    //	func makeRoutePointLongpressMenuEditMode(stackView :inout UIStackView, routePoint :RoutePoint) {
    //
    //		longPressedRoutePoint = routePoint
    //
    //		// show option to remove routepoint
    //
    //		for sv in stackView.arrangedSubviews {
    //			sv.removeFromSuperview()
    //		}
    //
    //		var removeRoutePointButton = UIButton(type: .system)
    //		removeRoutePointButton.setTitle("Remove Route Point", for: .normal)
    //		removeRoutePointButton.addTarget(self, action: #selector(removeRoutePoint), for: .touchUpInside)
    //		stackView.addArrangedSubview(removeRoutePointButton)
    //
    //	}
    //
    //	@objc func startRouteButtonTapped(sender: UIButton!) {
    //		dout("startRoute function")
    //		vc.generalUsageMenuContainer.isHidden = true
    //
    //		Routes.service.startRouteFromPoint(longPressedRoutePoint!)
    //
    //	}
    //
    //	@objc func stopRouteButtonTapped(sender: UIButton!) {
    //		dout("startRoute function")
    //		vc.generalUsageMenuContainer.isHidden = true
    //
    //		Routes.service.stopActiveRoute()
    //	}
    //
    //	@objc func editRouteButtonTapped(sender: UIButton!) {
    //		dout("editRoute function")
    //		vc.generalUsageMenuContainer.isHidden = true
    //
    //		if let routePoint = longPressedRoutePoint {
    //			startToEditRoute(route: routePoint.route)
    //		}
    //	}
    //
    //	@objc func removeRoutePoint(sender: UIButton!) {
    //		dout("removeRoutePoint function")
    //		vc.generalUsageMenuContainer.isHidden = true
    //
    //		longPressedRoutePoint?.removeSelf()
    //
    //		if longPressedRoutePoint === RPDrag {
    //			clearRPDrag()
    //		}
    //
    //		// refresh mapview overlays
    //		setOverlays(for: (longPressedRoutePoint?.route)!)
    //
    //	}
    //
    //	@objc func removeRoute(sender: UIButton!) {
    //		dout("removeRoute function")
    //		vc.generalUsageMenuContainer.isHidden = true
    //
    //		if let route = longPressedRoutePoint?.route {
    //			remove(route)
    //		}
    //	}
    //
    //	func remove(_ route :Route){
    //
    //		vc.mapView.removeOverlays(route.getAllOverlays())
    //		vc.mapView.removeAnnotations(route.routePointAnnotations)
    //
    //		route.removeSelf()
    //	}
    //
    //	func startToEditRoute(route :Route){
    //		route.setEdit(true)
    //
    //		if !vc.mRouteOverlayEnabled {
    //			// todo: if routes are not enabled: set all routes to visible
    //		}
    //
    //		// disable touch of waypointoverlay
    //		vc.waypointTouchEnabled = false
    //
    //		// set editmode of routeoverlay to this route
    //		setEditRoute(route: route, RPEdited: route.routePoints[0])
    //
    //		// disable follow/centermode
    //		vc.stopEditingRouteButtonView.isHidden = false
    //	}
    //
    //}
    //
    //class StRoutePolyline :MKPolyline { }
    //
    //class StRoutePointCircle :MKCircle { }
    //
    //class DraggingAnnotation :MKPointAnnotation {
    //	func setCoordinate(newCoord :CLLocationCoordinate2D){
    //
    //	}
    //}
    //
    //class RoutePointLabelAnnotation :MKPointAnnotation {
    //
    //	var routePoint :RoutePoint
    //	var annotationView :MKAnnotationView?
    //	//	var textLabel :UILabel?
    //
    //	init(_ routePoint :RoutePoint){
    //		self.routePoint = routePoint
    //		super.init()
    //		coordinate = routePoint.position
    //		routePoint.annotation = self
    //	}
    //
    //	func updateAnnotationView(){
    //		if let annotationView = self.annotationView {
    //			for sv in annotationView.subviews {
    //				sv.removeFromSuperview()
    //			}
    //			annotationView.addSubview(getTextLabelView())
    //		}
    //	}
    //
    //	func getTextLabelView() -> UILabel {
    //		let textLabel = UILabel()
    //		textLabel.textColor = UIColor.black
    //
    //		textLabel.textAlignment = .center
    //		textLabel.numberOfLines = 0
    //
    //		//		let title = annotation.title ?? "failed"
    //		//			textLabel.text = title ?? "failed"
    //
    //		textLabel.textAlignment = .left
    //		if routePoint.getDistance() > 0 {
    //			textLabel.text = "\(title ?? "")\n\(StUtils.formatDistance(routePoint.getDistance()))"
    //		} else {
    //			textLabel.text = "\(title ?? "")"
    //		}
    //
    //		textLabel.invalidateIntrinsicContentSize()
    //
    //		//			textLabel.frame = CGRect(x: 0, y: 0 , width: 400, height: 20)
    //		textLabel.sizeToFit()
    //		textLabel.frame.origin = CGPoint(x: 12, y: -(textLabel.frame.height / 2))
    //		return textLabel
    //	}
    
}
