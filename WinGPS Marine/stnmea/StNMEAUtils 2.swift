//
//  StNMEAUtils.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 03/09/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class StNMEAUtils {
	
	static func stringToSixbit(string: String) -> String {
		var stringSixbit : String = ""
		
		for character in string {
			guard let av = character.asciiValue else { return "" }
			var aChar : UInt8 = UInt8(av) + 40
			if(aChar > 128){
				aChar = aChar + 32
			} else {
				aChar = aChar + 40
			}
			
			stringSixbit = stringSixbit + intToBits(character: aChar)
		}
		
		return stringSixbit
	}
	
	static func intToBits(character: UInt8) -> String {  //Outputs a string containing 1 and 0
		var result : String = ""
		var mask : UInt8 = 0x80
		for _ in 1...8 {
			if((character & mask) == 0){
				mask = mask >> 1
				result = result + "0"
			} else {
				mask = mask >> 1
				result = result + "1"
			}
		}
		let indexStartOfText : String.Index = result.index(result.startIndex, offsetBy: 2)
		return String(result[indexStartOfText...])
	}
	
	// Functions converting bits (as a string) to usable values
	
	static func bti(string: String, start: Int, end: Int) -> Int {
		var result = 0
		if(end > string.count){
			return result
		}
		let resultString = string.subString(from: start, to: end)
		/*let startIndex = string.index(string.startIndex, offsetBy: 7)
		let endIndex = string.index(string.startIndex, offsetBy: end)
		let range = startIndex..<endIndex
		*/
		result = Int(resultString, radix: 2) ?? 0
		return result
	}
	
	static func btpos(string: String, start: Int, end: Int) -> Int {
		var result = 0
		if(end > string.count){
			return result
		}
		
		let temp = string.subString(from: start, to: end)
		if temp[0] == "1" {
			var flippedString : String = ""
			
			for char in temp.subString(from: 1, to: temp.count-1) {
				flippedString += (char == "1") ? "0" : "1"
			}
			result = -1 * ((Int(flippedString, radix: 2) ?? 0) + 1)
		} else {
			result = Int(temp, radix: 2) ?? 0
		}
		
		return result
	}
	
	static func bts(string: String, start: Int, end: Int) -> String {
		var result = ""
		if(end > string.count){
			return result
		}
		for i in 0...((end-start)/6) {
			var charInt = bti(string: string, start: start+(i*6), end: start+(6*i)+6)
			if(charInt<32) {
				charInt = charInt+64
			}
			if(charInt != 64){
				
				result = result + String(Character(UnicodeScalar(charInt) ?? " "))
			}
		}
		return result
	}
	
	static func bts(string: String, start: Int) -> String {
		return bts(string: string, start: start, end: string.count)
	}
	
	static func bitsToDimensions(string: String, start: Int, end: Int) -> [Int] {
		var result = [Int](repeating: 0, count: 4)
		
		let s2 = string.subString(from: start, to: end)
		
		result[0] = bti(string: s2, start: 0, end: 9)
		result[1] = bti(string: s2, start: 9, end: 18)
		result[2] = bti(string: s2, start: 18, end: 24)
		result[3] = bti(string: s2, start: 24, end: 30)
		return result
	}
	
	//
	//	public static double getDist(double x1, double y1, double x2, double y2){
	//	return Math.sqrt((Math.pow((x2-x1), 2) + Math.pow((x2-x1), 2)));
	//
	//	}
	//
	//	public static double minToRad(int min){
	//	return (double)min * (Math.PI/180.0/60.0/10000.0);
	//	}
	//
	//	public static double toMin(double rad){
	//	return rad * (60*180/Math.PI);
	//	}
	//
	//	public static int toDegInt(double rad){
	//	return (int)Math.round(rad * (180/Math.PI));
	//	}
	
	//	static func toDegString(rad :Double, format :Int, dec :Int) -> String {
	static func toDegString(radians :Double) -> String {
		
		var result :String = "N/A"
		
		//	try {
		//		if format < 3 {
		//			result = Location.convert(radiansToDegrees(rad), format)
		//		} else if (format < 6) {
		//			String[] parts = Location.convert(rad * 180 / Math.PI, format - 3).split(":")
		//			String[] symbols = { "\u{00B0}", "\'", "\"" }
		//			result = ""
		//			for (int i = 0; i < parts.length; i++) {
		//				int seperator
		//				if(dec == 0)seperator = 0;
		//				else seperator = 1;
		//				if (parts[i].replace(",", ".").contains(".")) {
		//					parts[i] = parts[i].substring(0, parts[i].replace(",", ".").indexOf(".") + seperator + dec)
		//				}
		//				result = result + parts[i] + symbols[i] + " "
		//			}
		//		}
		//	} catch (Exception e) {
		//	// TODO: handle exception
		//	}
		
		result = "\(String(format: "%.6f", radiansToDegrees(radians)))\u{00B0}"
		
		return result.trim()
	}
	
	//	public static Bitmap rotateBitmap(Bitmap b, int degrees, Matrix m) {
	//	if (degrees != 0 && b != null) {
	//	if (m == null) {
	//	m = new Matrix();
	//	}
	//	m.setRotate(degrees,
	//	(float) b.getWidth() / 2, (float) b.getHeight() / 2);
	//	try {
	//	Bitmap b2 = Bitmap.createBitmap(
	//	b, 0, 0, b.getWidth(), b.getHeight(), m, true);
	//	if (b != b2) {
	//	b.recycle();
	//	b = b2;
	//	}
	//	} catch (OutOfMemoryError ex) {
	//	// We have no memory to rotate. Return the original bitmap.
	//	Log.e(TAG, "Got oom exception ", ex);
	//	}
	//	}
	//	return b;
	//	}
	//
	//	public static Point rotate(Point point, double rot){
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	double xi1 = (point.x * cosRot) + (point.y * -sinRot);
	//	double yi1 = (point.x * sinRot) + (point.y * cosRot);
	//
	//	return new Point((int)(xi1), (int)(yi1));
	//	}
	//
	//	public static Point drawShipTriangle(ISafeCanvas canvas, SafePaint paint, int color, int size, int x, int y, float sog, float direction){
	//
	//	Point p1 = new Point(0, -size);
	//	Point p2 = new Point(-(size/2), size);
	//	Point p3 = new Point((size/2), size);
	//
	//	double rot = (double)direction;
	//
	//	//double l1 = Math.sqrt(Math.pow(p1.x, 2) + Math.pow(p1.y, 2));
	//	//double xi1 = Math.sin((Math.atan(p1.x/(-p1.y)))+rot)*l1;
	//	//double yi1 = Math.cos((Math.atan(p1.x/(-p1.y)))+rot)*l1;
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	double xi1 = (p1.x * cosRot) + (p1.y * -sinRot);
	//	double yi1 = (p1.x * sinRot) + (p1.y * cosRot);
	//
	//	p1.set((int)(xi1), (int)(yi1));
	//
	//	//double l2 = Math.sqrt(Math.pow(p2.x, 2) + Math.pow(p2.y, 2));
	//	//double xi2 = Math.sin((Math.atan(p2.x/(-p2.y)))+rot)*l2;
	//	//double yi2 = Math.cos((Math.atan(p2.x/(-p2.y)))+rot)*l2;
	//	double xi2 = (p2.x * cosRot) + (p2.y * -sinRot);
	//	double yi2 = (p2.x * sinRot) + (p2.y * cosRot);
	//	p2.set((int)(xi2), (int)(yi2));
	//
	//	//double l3 = Math.sqrt(Math.pow(p3.x, 2) + Math.pow(p3.y, 2));
	//	//double xi3 = Math.sin((Math.atan(p3.x/(-p3.y)))+rot)*l3;
	//	//double yi3 = Math.cos((Math.atan(p3.x/(-p3.y)))+rot)*l3;
	//	double xi3 = (p3.x * cosRot) + (p3.y * -sinRot);
	//	double yi3 = (p3.x * sinRot) + (p3.y * cosRot);
	//	p3.set((int)(xi3), (int)(yi3));
	//
	//	p1.offset(x, y);
	//	p2.offset(x, y);
	//	p3.offset(x, y);
	//
	//	SafeTranslatedPath path = new SafeTranslatedPath();
	//	//path.setFillType(Path.FillType.);
	//	path.onDrawCycleStart(canvas);
	//	path.moveTo((double)p1.x,(double)p1.y);
	//
	//	path.lineTo((double)p2.x,(double)p2.y);
	//	path.lineTo((double)p3.x,(double)p3.y);
	//	path.lineTo((double)p1.x,(double)p1.y);
	//
	//	path.close();
	//
	//
	//	paint.setColor(color);
	//	paint.setStyle(Paint.Style.FILL);
	//
	//
	//	canvas.drawPath(path, paint);
	//	paint.setStyle(Paint.Style.STROKE);
	//	paint.setColor(Color.BLACK);
	//	canvas.drawPath(path, paint);
	//	//Log.d("MAIN", "Drawing triangle at: " + p1.x + " : " + p1.y);
	//
	//	/*
	//	canvas.drawCircle(p1.x, p1.y, 2, paint);
	//	canvas.drawCircle(p2.x, p2.y, 2, paint);
	//	canvas.drawCircle(p3.x, p3.y, 2, paint);
	//	*/
	//
	//	return p1;
	//	/*
	//	* Debug circle
	//	paint.setColor(Color.MAGENTA);
	//	paint.setStyle(Style.STROKE);
	//	canvas.drawCircle(x, y, size, paint);
	//	*/
	//	//canvas.drawLine(point1_draw.x,point1_draw.y,point2_draw.x,point2_draw.y, paint);
	//
	//	}
	//
	//	public static Point drawShipTriangle(Canvas canvas, int color, int size, int x, int y, float sog, float direction){
	//
	//
	//	Point p1 = new Point(0, -size);
	//	Point p2 = new Point(-(size/2), size);
	//	Point p3 = new Point((size/2), size);
	//
	//	double rot = (double)direction;
	//
	//	//double l1 = Math.sqrt(Math.pow(p1.x, 2) + Math.pow(p1.y, 2));
	//	//double xi1 = Math.sin((Math.atan(p1.x/(-p1.y)))+rot)*l1;
	//	//double yi1 = Math.cos((Math.atan(p1.x/(-p1.y)))+rot)*l1;
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	double xi1 = (p1.x * cosRot) + (p1.y * -sinRot);
	//	double yi1 = (p1.x * sinRot) + (p1.y * cosRot);
	//
	//	p1.set((int)(xi1), (int)(yi1));
	//
	//	//double l2 = Math.sqrt(Math.pow(p2.x, 2) + Math.pow(p2.y, 2));
	//	//double xi2 = Math.sin((Math.atan(p2.x/(-p2.y)))+rot)*l2;
	//	//double yi2 = Math.cos((Math.atan(p2.x/(-p2.y)))+rot)*l2;
	//	double xi2 = (p2.x * cosRot) + (p2.y * -sinRot);
	//	double yi2 = (p2.x * sinRot) + (p2.y * cosRot);
	//	p2.set((int)(xi2), (int)(yi2));
	//
	//	//double l3 = Math.sqrt(Math.pow(p3.x, 2) + Math.pow(p3.y, 2));
	//	//double xi3 = Math.sin((Math.atan(p3.x/(-p3.y)))+rot)*l3;
	//	//double yi3 = Math.cos((Math.atan(p3.x/(-p3.y)))+rot)*l3;
	//	double xi3 = (p3.x * cosRot) + (p3.y * -sinRot);
	//	double yi3 = (p3.x * sinRot) + (p3.y * cosRot);
	//	p3.set((int)(xi3), (int)(yi3));
	//
	//
	//	p1.offset(x, y);
	//	p2.offset(x, y);
	//	p3.offset(x, y);
	//
	//	Path path = new Path();
	//	//path.setFillType(Path.FillType.);
	//	path.moveTo(p1.x,p1.y);
	//	path.lineTo(p2.x,p2.y);
	//	path.lineTo(p3.x,p3.y);
	//	path.lineTo(p1.x,p1.y);
	//	path.close();
	//
	//	SafePaint paint = new SafePaint();
	//
	//
	//	paint.setStrokeWidth(StUtils.dpToPx(2));
	//	paint.setColor(color);
	//	paint.setStyle(Paint.Style.FILL);
	//	paint.setAntiAlias(true);
	//
	//	canvas.drawPath(path, paint);
	//	paint.setStyle(Paint.Style.STROKE);
	//	paint.setColor(Color.BLACK);
	//	canvas.drawPath(path, paint);
	//	//Log.d("MAIN", "Drawing triangle at: " + p1.x + " : " + p1.y);
	//	return p1;
	//	/*
	//	* Debug circle
	//	paint.setColor(Color.MAGENTA);
	//	paint.setStyle(Style.STROKE);
	//	canvas.drawCircle(x, y, size, paint);
	//	*/
	//	//canvas.drawLine(point1_draw.x,point1_draw.y,point2_draw.x,point2_draw.y, paint);
	//
	//	}
	//
	//	public static void drawVector(Canvas canvas, int color, int x, int y, int sog, int cog, float scale){
	//
	//
	//
	//	Point p1 = new Point(0, (int)(-sog*scale));
	//
	//	double rot = (double)cog;
	//
	//
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	double xi1 = (p1.x * cosRot) + (p1.y * -sinRot);
	//	double yi1 = (p1.x * sinRot) + (p1.y * cosRot);
	//
	//	p1.set((int)(xi1), (int)(yi1));
	//	p1.offset(x, y);
	//	Paint paint = new Paint();
	//	paint.setColor(color);
	//	paint.setAntiAlias(true);
	//	paint.setStrokeWidth(StUtils.dpToPx(2));
	//	canvas.drawLine(x, y, p1.x, p1.y, paint);
	//	}
	//
	//	public static void drawVector(ISafeCanvas canvas, SafePaint paint, int color, int x, int y, float sog, float cog, float scale, float courseLineWidth){
	//
	//	Point p1 = new Point(0, (int)(-sog*scale));
	//
	//	double rot = (double)cog;
	//
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	double xi1 = (p1.x * cosRot) + (p1.y * -sinRot);
	//	double yi1 = (p1.x * sinRot) + (p1.y * cosRot);
	//
	//	p1.set((int)(xi1), (int)(yi1));
	//	p1.offset(x, y);
	//	paint = new SafePaint();
	//	paint.setColor(color);
	//	paint.setAntiAlias(true);
	//	paint.setStrokeWidth(courseLineWidth);
	//	canvas.drawLine(x, y, p1.x, p1.y, paint);
	//	}
	//
	//	public static String floatToString(float f){
	//	String s = Float.toString(f);
	//	if(s.indexOf(".")>0){
	//	s = s + "000";
	//	}
	//	return s.substring(0, s.indexOf(".") + 3);
	//	}
	
	static func aisTypeString(type :Int) -> String? {
		var result :String? = nil
		
		if type > -1 && type < 100 {
			let firstDigit = ["not available", "Reserved for future use",
									"WIG", "Vessel", "HSC", "Special craft", "Passenger ship",
									"Cargo ship", "Tanker", "Other type"]
			let secondDigit3 = ["Fishing",
									  "Towing", "Towing (large)", "Dredging/underwater operations",
									  "Diving operations",
									  "Military operations", "Sailing", "Pleasure craft", "",
									  ""]
			let secondDigit5 = ["Pilot vessel",
									  "Search and rescue vessel", "Tug", "Port tender",
									  "Vessel with anti-pollution facilities or equipment",
									  "Law enforcement vessel", "", "", "Medical transport",
									  "Ship according to RR Resolution No. 18 (Mob-83)"]
			let second = type % 10
			let first = (type - second) / 10
			if first == 5 {
				result = secondDigit5[second]
			} else if first == 3 {
				result = secondDigit3[second]
			} else {
				result = firstDigit[first]
			}
		}
		return result
	}
	
	//	public static void drawShipOnScale(ISafeCanvas canvas, SafePaint paint, float scale, int color, int x, int y, float rot, float[] dim) {
	//	drawShipOnScale(canvas, paint, scale, color, x, y, rot, dim, new double[10]);
	//	}
	//
	//	public static void drawShipOnScale(ISafeCanvas canvas, SafePaint paint, float scale, int color, int x, int y, float rot, float[] dim, double[] buffer) {
	//	double[] p = buffer;
	//
	//	//Lengte van rechte driehoek aan punt
	//
	//	//Log.d("AIS", "Drawing ship on scale");
	//
	//	if (dim[0]==0 && dim[2]==0 && dim[1]!=0 && dim[3]!=0) {
	//	//Log.d("AIS", "Ref. not available");
	//	// Voorpunt
	//	p[0] = 0;
	//	p[1] = -1*(dim[1]/2);
	//	//Rechts voor
	//	p[2] = (dim[3])/2;
	//	p[3] = -1*(dim[1]-dim[3])/2;
	//	//Rechts achter
	//	p[4] = dim[3]/2;
	//	p[5] = dim[1]/2;
	//	//Links achter
	//	p[6] = -1*dim[3]/2;
	//	p[7] = dim[1]/2;
	//	//Links voor
	//	p[8] = -1*(dim[3])/2;
	//	p[9] = -1*(dim[1]-dim[3])/2;
	//	} else {
	//	//Log.d("AIS", "Ref. available");
	//	double _x = ((dim[3]-dim[2])/2);
	//	double _y = (dim[2]+dim[3])/2;
	//	// Voorpunt
	//	p[0] = _x;
	//	p[1] = -dim[0];
	//
	//	//Rechts voor
	//	p[2] = dim[3];
	//	p[3] = -(dim[0] - _y);
	//
	//	//Rechts achter
	//	p[4] = dim[3];
	//	p[5] = dim[1];
	//
	//	//Links achter
	//	p[6] = -dim[2];
	//	p[7] = dim[1];
	//
	//	//Links voor
	//	p[8] = -dim[2];
	//	p[9] = -(dim[0] - _y);
	//
	//	}
	//	double cosRot = Math.cos(rot);
	//	double sinRot = Math.sin(rot);
	//
	//	for(int i = 0; i < 10; i+=2){
	//	//			Log.e("popke", "start of loop, value of i: "+i);
	//	if (i > 9) break;
	//	double _x = p[i];
	//	//			Log.e("popke", "value of i: "+i);
	//	double _y = p[i+1];
	//	//			Log.e("popke", "value of i: "+i);
	//	p[i] = scale*((_x * cosRot) + (_y * -sinRot)) + x;
	//	p[i+1] = scale*((_x * sinRot) + (_y * cosRot)) + y;
	//	//			Log.e("popke", "value of i: "+i);
	//	//		    i++;
	//	//			Log.e("popke", "value of i: "+i);
	//	}
	//
	//	SafeTranslatedPath path = new SafeTranslatedPath();
	//	//path.setFillType(Path.FillType.);
	//	path.onDrawCycleStart(canvas);
	//	path.moveTo(p[0], p[1]);
	//
	//	path.lineTo(p[2], p[3]);
	//	path.lineTo(p[4], p[5]);
	//	path.lineTo(p[6], p[7]);
	//	path.lineTo(p[8], p[9]);
	//
	//
	//	path.close();
	//
	//	int transparantColor = Color.argb(127, Color.red(color), Color.green(color), Color.blue(color));
	//	paint.setColor(transparantColor);
	//	paint.setStyle(Paint.Style.FILL);
	//
	//
	//	canvas.drawPath(path, paint);
	//	paint.setStyle(Paint.Style.STROKE);
	//	paint.setColor(Color.BLACK);
	//	canvas.drawPath(path, paint);
	//	}
	
	static func aisTypeColor(_ type :Int) -> UIColor {
		var result = UIColor.blue
		
		if type > -1 && type < 100 {
			let firstDigit :[Int] = [0xFFFF9F, 0xFFFF9F,
											 0xFFFF9F, 0xFFFFA2, 0xFF00FF, -1, 0x8000FF,
											 0x804040, 0x0080FF, 0xFFFF9F]
			
			let secondDigit3 :[Int] = [0xC0C0C0,
												0x00FFFF, 0x00FFFF, 0x800000,
												0xFFFFFF9F,
												0x000080, 0xFFFFFF, 0xFFFFFF, 0xFFFF9F,
												0xFFFF9F]
			
			let secondDigit5 :[Int] = [0xFFFF00,
												0xFF8000, 0xFFFF9F, 0x80FF00,
												0x0DFF0D,
												0x00FF00, 0xFFFF9F, 0xFFFF9F, 0xFFFF9F,
												0x030381]
			var second :Int = type % 10
			var first :Int = (type - second) / 10
			if first == 5 {
				result = UIColor(rgb: secondDigit5[second])
			} else if(first == 3){
				result = UIColor(rgb: secondDigit3[second])
			} else {
				result = UIColor(rgb: firstDigit[first])
			}
		}
		
		if type == 666 { result = UIColor.black }
		
		return result
	}
}
