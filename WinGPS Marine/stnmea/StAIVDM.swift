//
//  StAIVDM.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 27/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation


class StAIVDM {

    private final var parts : [String]
    var time : Int64 = 0
    
    var totalMsgCount :Int = 0
    var msgNumber :Int = 0
    var seqMsgId :Int = 0
    var channel :Int = 0
    var content :String = ""
    var fillBits :Int = 0
    var checksum :String?
    
    init(time : Int64, parts : [String]) {
        self.time = time
        self.parts = parts
        
        if (parts.count == 7){
            
            self.totalMsgCount = Int(parts[1]) ?? 0
            self.msgNumber = Int(parts[2]) ?? 0
            self.seqMsgId = Int(parts[3]) ?? 0
            self.channel = Int(parts[4]) ?? 0
            self.content = parts[5]
            self.fillBits = Int(String(parts[6].first ?? "0")) ?? 0
            self.checksum = String(parts[6].dropFirst())
        }
    }
    
//    func getTime() -> Int64 {
//        return time
//    }
}
