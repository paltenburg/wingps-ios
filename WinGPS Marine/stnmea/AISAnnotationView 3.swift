//
//  AISAnnotationView.swift
//  WinGPS Marine
//
//  Created by Standaard on 20/01/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class AISAnnotationView :MKAnnotationView {

	var aisAnnotation :AISAnnotation?
	var textLabel :UILabel?
	var imageView :UIImageView?
	var color :UIColor = UIColor.blue
	
    static let defaultDrawScale : CGFloat = 1
    static let zoomThreshold : Double = 11

//    func getScale(zoomLevel: Float) -> Float {
//        var drawScale :Float = 1
//        let scale :Float = 1
//        let metersPerPoint = 156412 / pow(2, zoomLevel)
//        let zoomResize =  log10(metersPerPoint / scale) / log10(sqrt(2))
//        if zoomResize > 0 {
//            drawScale = pow(sqrt(2), (-1 * zoomResize))
//        } else {
//            drawScale = 1
//        }
//        return drawScale
//    }
    
	func setScaleAndRotation(zoom: Double){
		
		DispatchQueue.main.async{
			
			var scale = AISAnnotationView.defaultDrawScale
			
//			if zoom > 0 {
//                scale = CGFloat(self.getScale(zoomLevel: Float(zoom)))
//			}

			
			var yMax :Int = 0
			var markerWidth :CGFloat = 0
			var markerHeight :CGFloat = 0
			var showAIS = false
			if let imageView = self.imageView,
				let aisAnnotation = self.aisAnnotation {
				
				// subviews[1] is the waypoint icon
				
				var rotationCorrection :Float = 0
                if let mapRotation = Nmea.service.overlay?.vc.mapRotation {
                    rotationCorrection = Float(mapRotation) * Float.pi/180
                }
				var markerWidth :CGFloat = 0

				markerWidth = imageView.image!.size.width
				
				imageView.transform = CGAffineTransform(translationX: -imageView.image!.size.width / 2, y: -imageView.image!.size.height / 2)
				imageView.transform = imageView.transform.scaledBy(x: scale, y: scale)
				imageView.transform = imageView.transform.rotated(by: CGFloat((aisAnnotation.aisObject.trueHeading ?? aisAnnotation.aisObject.cog ?? 0) - rotationCorrection))

				imageView.setNeedsDisplay()
				
//				markerWidth = imageView.image!.size.width * scale
				markerHeight = imageView.image!.size.height * scale
//
//				if markerWidth > ViewController.WP_SIZE_THRESHOLD || markerHeight > ViewController.WP_SIZE_THRESHOLD {
//					showAIS = true
//				}
                
                if let zoom = Nmea.service.overlay?.vc.mapView.getZoom() {
                    if zoom > AISAnnotationView.zoomThreshold {
                        showAIS = true
                    }
                }
			}
			
			if !self.isHidden {
				if !showAIS { self.isHidden = true }
			} else {
				if showAIS { self.isHidden = false }
			}
			
			// Scale and toggle textlabels
			
			var labelHeight :Int = 20
			
			if let label = self.textLabel,
				let aisAnnotation = self.aisAnnotation {
				label.frame = CGRect(x: -200, y: Int(markerHeight / 2) , width: 400, height: labelHeight)
	
				var newFontSize = min(ViewController.WP_TEXT_SIZE_DEF * scale, ViewController.WP_TEXT_SIZE_DEF)
				if newFontSize >= ViewController.WP_TEXT_SIZE_MIN {
					label.font = label.font.withSize(newFontSize)
					aisAnnotation.textTooSmall = false
					if label.isHidden { label.isHidden = false }
					label.sizeToFit()
					label.frame.size.width = 400
				} else {
					aisAnnotation.textTooSmall = true
					if !label.isHidden {
						label.isHidden = true
					}
				}
			}
			
			self.setNeedsDisplay()
		}
	}
    
    func setRotation(){
        
        DispatchQueue.main.async{
                   
            var yMax :Int = 0
            var showAIS = false
            if let imageView = self.imageView,
                let aisAnnotation = self.aisAnnotation {
                
                var rotationCorrection :Float = 0
                if let mapRotation = Nmea.service.overlay?.vc.mapRotation {
                    rotationCorrection = Float(mapRotation) * Float.pi/180
                }
                 
                imageView.transform = CGAffineTransform(translationX: -imageView.image!.size.width / 2, y: -imageView.image!.size.height / 2)
                imageView.transform = imageView.transform.rotated(by: CGFloat((aisAnnotation.aisObject.cog ?? 0) - rotationCorrection))
                imageView.setNeedsDisplay()
            }
            self.setNeedsDisplay()
        }
    }
    
	func setTextLabel(){
		if let aisAnnotation = self.aisAnnotation{
			var newTextLabel :String = (aisAnnotation.aisObject.name ?? "")
			if let sog = aisAnnotation.aisObject.sog,
				sog > 0.01 {
				newTextLabel += " (\(StUtils.formatSpeed(Double(sog)) ?? ""))"
			}
			DispatchQueue.main.async {
				self.textLabel!.text = newTextLabel
				self.textLabel!.invalidateIntrinsicContentSize()
			}
		}
	}
	
	func updateImageView() {
		
		if let aisAnnotation = aisAnnotation {
			
			let newColor = aisAnnotation.aisObject.getColor()
			if imageView == nil || newColor.rgb() != color.rgb() {
				color = newColor
//				if newColor.rgb() != color.rgb() {
//					dout("COLOR CHANGE updating imageview: type, color: \(self.aisAnnotation?.aisObject.type ?? -2) \(self.color.rgb())")
//				}
				
				DispatchQueue.main.async{
					self.imageView = UIImageView(image: aisAnnotation.getImageView(fillColor: self.color))
					
					if self.subviews.count == 2 {
						self.subviews[1].removeFromSuperview()
					}
					
					
					self.addSubview(self.imageView!)
					
					self.setNeedsDisplay()
					
					dout("updating imageview: type, color: \(self.aisAnnotation?.aisObject.type ?? -2) \(self.color.rgb())")
				}
			}
		}
	}
}
