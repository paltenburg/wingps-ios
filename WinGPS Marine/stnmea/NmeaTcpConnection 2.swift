//
//  NmeaTcpConnection.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 22/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import UIKit

protocol NmeaTcpConnectionDelegate: class {
	func received(message: String)
}


class NmeaTcpConnection: NSObject {
	
	static let service = NmeaTcpConnection()
	
	var host :String = ""
	var port :Int = 5101
	
	var inputStream: InputStream!
	var outputStream: OutputStream!
	
	var delegates = [NmeaTcpConnectionDelegate?]()
	//	weak var delegate: NmeaTcpConnectionDelegate?
	
	
	var connectionEnabled : Bool = false
	
	let maxReadLength = 4096
	
	var baseRegex :String = "[\\$!]\\w{5},"
	var splitCondition :String = ""
	var matchCondition :String = ""
	var splitRegex :NSRegularExpression?
	var matchRegex :NSRegularExpression?
	
	private override init() {
		//		super.init()
		
		splitCondition = "(?=(" + baseRegex + "))"
		matchCondition = "(" + baseRegex + ".*)"
		do {
			splitRegex = try NSRegularExpression(pattern: splitCondition)
			matchRegex = try NSRegularExpression(pattern: matchCondition)
		} catch {
		}
		
	}
	
	func startConnection(host :String?, port :Int?){
		
		if connectionEnabled {
			stopConnection()
		}
		
		self.host = host ?? self.host
		self.port = port ?? self.port
		setupNetworkCommunication(host: self.host, port: self.port)
		connectionEnabled = true
	}
	
	func setupNetworkCommunication(host: String, port: Int) {
		// 1
		var readStream: Unmanaged<CFReadStream>?
		var writeStream: Unmanaged<CFWriteStream>?
		
		// 2
		CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault,
													  host as CFString,
													  UInt32(port),
													  &readStream,
													  &writeStream)
		
		inputStream = readStream!.takeRetainedValue()
		outputStream = writeStream!.takeRetainedValue()
		
		inputStream.delegate = self
		
		inputStream.schedule(in: .current, forMode: .common)
		outputStream.schedule(in: .current, forMode: .common)
		
		inputStream.open()
		outputStream.open()
		
	}
	

	//    func joinChat(username: String) {
	//        //1
	//        let data = "iam:\(username)".data(using: .utf8)!
	//
	//        //2
	//        self.username = username
	//
	//        //3
	//        _ = data.withUnsafeBytes {
	//            guard let pointer = $0.baseAddress?.assumingMemoryBound(to: UInt8.self) else {
	//                print("Error joining chat")
	//                return
	//            }
	//            //4
	//            outputStream.write(pointer, maxLength: data.count)
	//        }
	//    }
	
	func _send(message: String) {
		let data = message.data(using: .utf8)!//"msg:\(message)".data(using: .utf8)!
		
		//		let rt :ResultType = data.withUnsafeBytes { ////////////////////
		//			guard let pointer = $0.baseAddress?.assumingMemoryBound(to: UInt8.self) else {
		//				print("Error joining connection")
		//				return
		//			}
		//			outputStream.write(pointer, maxLength: data.count)
		//		}
	}
	
	func stopConnection(){
		NmeaTcpConnection.service.connectionEnabled = false
		NmeaTcpConnection.service.stopNmeaSession()
	}
	
	func stopNmeaSession() {
		inputStream.close()
		outputStream.close()
	}
	
	func removeFromDelegates(_ delegateToRemove :NmeaTcpConnectionDelegate){
		for i in 0 ..< delegates.count {
			if delegates[i] === delegateToRemove {
				delegates.remove(at: i)
				return
			}
		}
	}
}

extension NmeaTcpConnection: StreamDelegate {
	func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
		switch eventCode {
		case .hasBytesAvailable:
//					readAvailableBytes(stream: aStream as! InputStream)
			readAvailableBytes()
		case .endEncountered:
			print("new message received")
			stopNmeaSession()
		case .errorOccurred:
			print("error occurred")
		case .hasSpaceAvailable:
			print("has space available")
		default:
			print("some other event...")
		}
	}
	
//	private func readAvailableBytes(stream: InputStream) {
	private func readAvailableBytes() {

		var loopTimes = 0

		while inputStream.hasBytesAvailable {

			loopTimes += 1
			
			let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: maxReadLength)
			let numberOfBytesRead = inputStream.read(buffer, maxLength: maxReadLength)
			
			if numberOfBytesRead < 0, let error = inputStream.streamError {
				dout("error reading stream: " + error.localizedDescription)
				break
			}
			
			
			// Construct the message object
			if let message = processedMessageString(buffer: buffer, length: numberOfBytesRead) {
				
				// Notify interested parties
				for delegate in delegates {
					delegate?.received(message: message)
				}

				//				String[] nmeamsgs = null;
				//				try {
				//				nmeamsgs = msg.split(splitCondition);
				//				} catch (Exception e) {
				//				Log.e("TCP", "Split, msgText: " + msg, e);
				//				}
				//
				//				if (nmeamsgs != null &&
				//					(nmeamsgs.length > 1
				//						// || (nmeamsgs.length == 1 && nmeamsgs[0].matches(matchCondition)))) { // appearantly a trailing "/r/n" make it not match anymore
				//						|| (nmeamsgs.length == 1 && matchPattern.matcher(nmeamsgs[0].trim()).matches()))) {
				//					for (int i = 0; i < nmeamsgs.length; i++) {
				//						if (nmeamsgs[i].startsWith("!") || nmeamsgs[i].startsWith("$")) {
				//							try {
				//							onNmeaReceived(mConn, 0, nmeamsgs[i].trim());
				//							} catch (Exception e) {
				//							Log.e("TCP", "onNmeaReceived error, msgText: " + nmeamsgs[i], e);
				//							}
				//						}
				//					}
				//				}
				
				var nmeaMsgs = [String]()
				do {
					try nmeaMsgs =
						message.split(usingRegex: splitCondition)
							.filter{$0.count > 0}
							.map{$0.trim()}
					if nmeaMsgs.count > 1
						|| nmeaMsgs.count == 1 && matchRegex?.matches(in: nmeaMsgs[0], range: NSRange(0 ..< nmeaMsgs[0].utf16.count)).count ?? 0 >= 1 {
						for message in nmeaMsgs {
							Nmea.service.onNmeaReceived(msgTxt: message)
						}
						Nmea.service.overlay?.setOverlays()
					}
				} catch {
				} // do
			} // if
		} // while
	}
	
	private func processedMessageString(buffer: UnsafeMutablePointer<UInt8>,
													length: Int) -> String? {
		//1
		guard
			let message = String(
				bytesNoCopy: buffer,
				length: length,
				encoding: .utf8,
				freeWhenDone: true)
			else {
				return nil
		}
		
		//        //2
		//        let messageSender: MessageSender = (name == self.username) ? .ourself : .someoneElse
		//        //3
		//        return Message(message: message, messageSender: messageSender, username: name)
		return message
	}
}
