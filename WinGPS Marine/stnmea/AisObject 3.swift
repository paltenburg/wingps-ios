//
//  AisObject.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 24/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class AisObject {
	
	var color = UIColor.blue
	
	final var mmsi : Int
	
	var id :Int = -1
	
	var navStatus : Int?
	var rot : Int?
	var sog : Float?
	var posAcc : Int?
	var position : CLLocationCoordinate2D?
	var cog : Float?
	var trueHeading : Float?
	var timeStamp : Int?
	var specialManInd : Int?
	var spare : Int?
	var raimFlag : Int?
	var comState : Int?
	
	var callSign : String?
	var name : String?
	var type : Int = 0
	
	var safetyRelatedText : String?
	var dimensions : [Int]?
	
	var aisClass : String?
	var destination : String?
	
//	private float cpa_angle = 0
//	private float cpa_speed = 0
//	private double cpa_x
//	private double cpa_y
	var cpaT :Double?
	var cpa :Double?
//	private GeoPoint mCpaGP = new GeoPoint(0, 0)
	var cpaDistance :Int?
	var cpaTDistance :Int?
	var tcpaSeconds :Int? 

	private var distance :Int? = nil
//	private long mTime = 0
//	private String mClass = ""
//	private String mDestination = ""
	
	var maxStaticDraught : Int?
	var eta : Int?
	var cargoType : Int?
	var vendorId : String?
	var bearing : Float?
	var imoNumber : Int?
	var mTime : Int64?
	var registration : StMID?
	
	var lastUpdate : Int64
	
	var aisAnnotation : AISAnnotation?
	
	init(mmsi: Int) {
		self.mmsi = mmsi
		self.registration = StMID(mmsi: mmsi)
		lastUpdate = Int64(Date().timeIntervalSince1970 * 1000)
	}
	
    func getDistance(userLocation :CLLocationCoordinate2D?) -> Int? {
        if let distance = self.distance {
            return distance
        } else {
            if let userLocation = userLocation,
                let distanceD = self.position?.distance(coordinate: userLocation) {
                let distance = Int(distanceD)
                self.distance = distance
                return distance
            } else {
                return nil
            }
        }
    }
    
	func isSart() -> Bool {
		return (navStatus == 14 || navStatus == 15) && String(mmsi).subString(from: 0, to:2) == "97"
	}
	
	func getLon() -> Double {
		return degreesToRadians(position?.longitude ?? 0)
	}
	
	func getLat() -> Double {
		return degreesToRadians(position?.latitude ?? 0)
	}
	
	func posOK() -> Bool {
		//	return getLon() <= Math.PI && getLat() <= StMath.HALFPI
		if let position = self.position {
			return getLon() <= Double.pi && getLat() <= Double.pi / 2.0
		} else {
			return false
		}
	}
	
	func getTrueHeading() -> Float? {
		guard let trueHeading = trueHeading else { return nil }
		if round(trueHeading * 180 / Float.pi) == 511  {
			return nil
		} else {
			return trueHeading
		}
	}
	
	func getTypeString() -> String? {
		return StNMEAUtils.aisTypeString(type: type)
	}
	
	func getColor() -> UIColor {
		if type >= 0 {
			return StNMEAUtils.aisTypeColor(type)
		}
		return color
	}
	
	func setNewValues(aivdm: StAIVDMContent) {
		id = aivdm.getID()
		if(aivdm.getNavStatus() > -1){
			navStatus = aivdm.getNavStatus()
		}
		if(aivdm.getLongitude() < 108600000 && aivdm.getLatitude() < 54600000){
			//			setPosition(aLonAISMinutes: aivdm.getLongitude(), aLatAISMinutes: aivdm.getLatitude())
			setPos(lonMinutes: aivdm.getLongitude(), latMinutes: aivdm.getLatitude())
		}
		if(aivdm.getCog() > -1 && aivdm.getCog() < 3600){
			cog = Float(Double(aivdm.getCog()) * Double.pi / 1800)
		}
		if(aivdm.getTrueHeading() > -1 && aivdm.getTrueHeading() < 511){
			trueHeading = Float(Double(aivdm.getTrueHeading()) * Double.pi / 180)
		}
		if(aivdm.getSog() > -1 && aivdm.getSog() < 1023) {
			sog = Float(Double(aivdm.getSog()) / 10.0 * (1852.0/3600.0))
		}
		if(aivdm.getCS() != ""){
			callSign = aivdm.getCS()
		}
		if(aivdm.getName() != ""){
			name = aivdm.getName()
		}
		if(aivdm.getType() > -1){
			type = aivdm.getType()
		}
		if let dims = aivdm.getDimensions(),
			dims.count == 4 {
			dimensions = aivdm.getDimensions()
		}
		timeStamp = aivdm.getTimeStamp()
		if(aivdm.getAisClass() == "A"){
			color = UIColor.blue
			aisClass = "A"
		} else if(aivdm.getAisClass() == "B"){
			color = UIColor.white
			aisClass = "B"
		}
		
		if(aivdm.getDestination() != ""){
			destination = aivdm.getDestination()
		}
		if(aivdm.getMaxStaticDraught() >= 0){
			maxStaticDraught = aivdm.getMaxStaticDraught()
		}
		if(aivdm.getEta() >= 0){
			eta = aivdm.getEta()
		}
		if(aivdm.getCargoType() >= 0){
			cargoType = aivdm.getCargoType()
		}
		if(aivdm.getVendorId() != ""){
			vendorId = aivdm.getVendorId()
		}
		if(aivdm.getImoNumber() >= 0){
			imoNumber = aivdm.getImoNumber()
		}
		if(aivdm.getSafetyRelatedText() != ""){
			safetyRelatedText = aivdm.getSafetyRelatedText()
		}
		if(isSart()){
			color = UIColor.red
		}
		if(aivdm.getTime() > 0){
			mTime = aivdm.getTime()
			
			/////DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//			print("asyncAfterTimer fired!")
//					}
		
		}
		lastUpdate = Int64(Date().timeIntervalSince1970 * 1000)
	}
	
	//	// AIS Minutes are 1/10000th minutes per the AIS specification.
	//	func setPosition(aLonAISMinutes: Int, aLatAISMinutes: Int){
	//		position = CLLocationCoordinate2DMake((Double(aLatAISMinutes)/0.6), (Double(aLonAISMinutes)/0.6))
	//	}
	
	func setLon(aRad :Double) {
		//		mPos.setLongitudeE6((int)(aRad * StUtils.RAD2DEG * 1E6));
		position?.longitude = radiansToDegrees(aRad)
	}
	
	func setLat(aRad :Double){
		//		mPos.setLatitudeE6((int)(aRad * StUtils.RAD2DEG * 1E6));
		position?.latitude = radiansToDegrees(aRad)
	}
	
	func setPos(lonMinutes :Int, latMinutes :Int) {
		position = CLLocationCoordinate2D(latitude: Double(latMinutes) / 0.6 / Double(1E6), longitude: Double(lonMinutes) / 0.6 / Double(1E6))
	}
	
	func setCog(_ c :Float){
		self.cog = c * Float.pi / 1800
	}
	
	func setTrueHeading(_ th :Float){
		self.trueHeading = th * Float.pi / 180
	}
	
	func setSog(_ s :Float){
		self.sog = s * 0.0514444
	}
	
	//	// aAISMinutes are 1/10000th minutes per the AIS specification
	//	private func setLon(aAISMinutes :Int) {
	////		var lonE6 :Int = Int(Double(aAISMinutes) / 0.6)
	////		mPos.setLongitudeE6(lonE6);
	//		position?.longitude = Double(aAISMinutes) / 0.6 / Double(1E6)
	//
	//	}
	//
	//	// aAISMinutes are 1/10000th minutes per the AIS specification
	//	private func setLat(aAISMinutes :Int) {
	////		int latE6 = (int)(aAISMinutes / 0.6d);
	////		mPos.setLatitudeE6(latE6);
	//		position?.latitude = Double(aAISMinutes) / 0.6 / Double(1E6)
	//	}

//	public void setTrueHeading(float th){
//	trueHeading = (float) (th*Math.PI/180);
//	}
//
//	public void setSog(float s){
//	mSog = (float) (s*(0.0514444));
//	//		mSog = (float) (s / 18.52);
//	//		 / 18.52
//	}
//
//	public void setType(int t){
//	type = t;
//	}
//
//	public void setTimeStamp(int t){
//	timeStamp = t;
//	}
//
//	public void setSafetyRelatedText(String s){
//	safetyRelatedText = s;
//	}
//
//	/*
//	public void setMapOverlay(AisMapOverlay amo){
//	mapOverlay = amo;
//	}*/
//
//	public void setColor(int c){
//	color = c;
//	}
//
//	public void updateCpa(float aTime, double aLat, double aLon, float aCog, float aSog, StCpa aStCpa) {
//	mStCpa = aStCpa;
//
//	double lat = getLat();
//	double lon = getLon();
//
//	double _x = (Math.sin(mCog) * (mSog)) - (Math.sin(aCog) * aSog);
//	double _y = (Math.cos(mCog) * (mSog)) - (Math.cos(aCog) * aSog);
//
//	//if(_y!=0)cpa_angle = (float) Math.atan(_x/_y);
//	cpa_angle = (float) Math.atan2(_x, _y);//(-1*(Math.atan2(_y, _x)-(Math.PI/2)));
//	cpa_speed = (float) Math.sqrt((_x*_x) + (_y*_y));
//
//	mGeoP2.setCoordsE6((int)(aLat * 180/Math.PI * 1E6), (int)(aLon * 180/Math.PI * 1E6));
//
//	mCpaT = Math.sqrt(Math.pow(_x * aTime, 2) + Math.pow(_y * aTime, 2));
//	StUtils.getDestPoint(mPos, mCpaT, (float)(cpa_angle*180/Math.PI), mGeoP);
//	mCpaTDistance = mGeoP.distanceTo(mGeoP2);
//	//		mCpaTGP = new GeoPoint(mLat*180/Math.PI, mLon*180/Math.PI).destinationPoint(mCpaT, (float) (cpa_angle*180/Math.PI));
//	//		mCpaTDistance = mCpaTGP.distanceTo(new GeoPoint(lat*180/Math.PI, lon*180/Math.PI));
//	double distRad = Math.sqrt(Math.pow(aLon - lon, 2) + Math.pow(aLat - lat, 2));
//	double dX = (aLon - lon) / distRad;
//	double dY = (aLat - lat) / distRad;
//
//	bearing = (float) (Math.atan2(dX, dY) + (Math.PI));
//	updateDistance(aLat, aLon);
//
//	mCpa = Math.abs((((dX * mDistance) * (_x * aTime)) + ((dY * mDistance) * (_y * aTime))) / mCpaT);
//	//GeomGP = new GeoPoint((mLon+(mCpa*Math.sin(cpa_angle)))*180/Math.PI, (mLat+(mCpa*Math.cos(cpa_angle)))*180/Math.PI);
//	StUtils.getDestPoint(mPos, mCpa, (float)(cpa_angle*180/Math.PI), mCpaGP);
//	mCpaDistance = mCpaGP.distanceTo(mGeoP2);
//	//mCpaGP = new GeoPoint(mLat*180/Math.PI, mLon*180/Math.PI).destinationPoint(mCpa, (float) (cpa_angle*180/Math.PI));
//	//mCpaDistance = mCpaGP.distanceTo(new GeoPoint(lat*180/Math.PI, lon*180/Math.PI));
//	mTcpa = (int)(getCpaDistance() / cpa_speed);
//	//mLastUpdate = System.currentTimeMillis();
//	}
//
//	public boolean getCpaAlarm(){
//	return mStCpa != null && getCpaDistance() <= mStCpa.getDistance();
//	}
//
//	private void updateDistance(double lat, double lon){
//	//float[] results = new float[1];
//	//results[0] = -1;
//
//	//Location.distanceBetween(Math.toDegrees(mLat), Math.toDegrees(mLon), Math.toDegrees(lat), Math.toDegrees(lon), results);
//	//Location.distanceBetween(mLat, mLon, lat, lon, results);
//	//mDistance = results[0];
//	mGeoP.setCoordsE6((int)(lat * (180 / Math.PI) * 1E6), (int)(lon * (180 / Math.PI) * 1E6));
//	mDistance = mPos.distanceTo(mGeoP);
//	}

	func getCpaDistance() -> Float? {
		if cpaT ?? 0 >= cpa ?? 0 {
			return cpaDistance != nil ? Float (cpaDistance!) : nil
		} else {
			return cpaTDistance != nil ? Float (cpaTDistance!) : nil
		}
	}

//	public GeoPoint getCpa(){
//	return mCpaGP;
//	}
//
//	public int getTcpa(){
//	return mTcpa;
//	}
//
//	public float getCpaSpeed(){
//	return cpa_speed;
//	}
//
//	public float getCpaAngle(){
//	return cpa_angle;
//	}
//
//	public double getCpaT(){
//	return mCpaT;
//	}
//
//	public void setTime(long aTime) {
//	mTime  = aTime;
//	}
//
//	public long getTime(){
//	return mTime;
//	}
//
//	public String getDestination() {
//	return mDestination;
//	}
//
//	public float getRotation(){
//	return mRot;
//	}
//
//	public int getMaxStaticDraught(){
//	return mMaxStaticDraught;
//	}
//
//	public int getEta(){
//	return mEta;
//	}
//
//	public int getCargoType(){
//	return mCargoType;
//	}
//
//	public String getVendorId(){
//	return mVendorId;
//	}
//
//	public int getImoNumber(){
//	return mImoNumber;
//	}

//	public long getLastUpdateTime(){
//	return mLastUpdate;
//	}
//
//	public void setLastUpdateTime(long lu){
//	mLastUpdate = lu;
//	}
//
//	public boolean isSelected() {
//	return mSelected;
//	}
//
//	public void setSelected(boolean mSelected) {
//	this.mSelected = mSelected;
//	}
//
//	public void setVisiblePoints(GeoPoint[] points){
//	mVisiblePoints = points;
//	}
//
//	public GeoPoint[] getVisiblePoints(){
//	return mVisiblePoints;
//	}
//
//	public void setVisiblePointsValid(boolean aValid) {
//	mVisiblePointsValid = aValid;
//	}
//
//	public boolean getVisiblePointsValid() {
//	return mVisiblePointsValid;
//	}
	
	func getAisAnnotation() -> AISAnnotation {
		
		if let aisAnnotation = aisAnnotation{
			
			return aisAnnotation
		} else {
			let aisAnnotation = AISAnnotation(self)
			self.aisAnnotation = aisAnnotation
			updateAnnotation()
			
			return aisAnnotation
		}
	}
	
	func updateAnnotation() {
		if let aisAnnotation = self.aisAnnotation{
			
			aisAnnotation.coordinate = self.position! // EXC_BAD_ACCESS nr annotations: 208, 190
			aisAnnotation.title = name
			
			self.aisAnnotation = aisAnnotation
		}
	}
    
    func invalidateDistance() {
        distance = nil
    }
}
