//
//  StNMEA.swift
//  WinGPS Marine
//
//  Created by Standaard on 06/12/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation

class StNMEA{
	
	//	//private final String line;
	
	let parts :[String]
	
	var type :String
	var stAIVDM :StAIVDM?
	//	private StGPRMC stGPRMC;
	//	private StGPGSV stGPGSV;
	//	private StGPGLL stGPGLL;
	//	private StGPVTG stGPVTG;
	//	private StGSA stGSA;
	//	private StHDG stHDG;
	//	private StHDT stHDT;
	//	private StMWV stMWV;
	//	private StVHW stVHW;
	//	private StDBT stDBT;
	//	private StDPT stDPT;
	//
	//	private StBWC stBWC;
	
	let time :Int64
	
	init(time :Int64, s :String){
		self.time = time
		
		//line = s;

		parts = s.components(separatedBy: ",")
		
		var msg :String = parts[0].count > 3 ? parts[0].subString(fromInt: 3).uppercased() : ""
		switch msg {
		case "VDM":
			stAIVDM = StAIVDM(time: self.time, parts: parts)
			type = "AIVDM"
//		} else if (msg.equals("RMC")){
//			stGPRMC = new StGPRMC(parts);
//			type = "GPRMC";
//		} else if(msg.equals("GSV")){
//			stGPGSV = new StGPGSV(parts);
//			type = "GPGSV";
//		} else if(msg.equals("GLL")){
//			stGPGLL = new StGPGLL(parts);
//			type = "GPGLL";
//		} else if(msg.equals("VTG")){
//			stGPVTG = new StGPVTG(parts);
//			type = "GPVTG";
//		} else if(msg.equals("GSA")){
//			stGSA = new StGSA(parts);
//			type = "GSA";
//		} else if(msg.equals("HDG")){
//			stHDG = new StHDG(parts);
//			type = "HDG";
//		} else if(msg.equals("HDT")){
//			stHDT = new StHDT(parts);
//			type = "HDT";
//		} else if(msg.equals("MWV")){
//			stMWV = new StMWV(parts);
//			type = "MWV";
//		} else if(msg.equals("VHW")){
//			stVHW = new StVHW(parts);
//			type = "VHW";
//		} else if(msg.equals("DBT")){
//			stDBT = new StDBT(parts);
//			type = "DBT";
//		} else if (msg.equals("DPT")) {
//			stDPT = new StDPT(parts);
//			type = "DPT";
//		} else if(msg.equals("BWC")){
//			stBWC = new StBWC(parts);
//			type = "BWC";
		default:
			type = "unknown"
		}
	}
	
	//
	//	public String getType(){
	//	return type;
	//	}
	//
	//	public StAIVDM getStAIVDM(){
	//	return stAIVDM;
	//	}
	//
	//	public StGPRMC getStGPRMC(){
	//	return stGPRMC;
	//	}
	//
	//	public StGPGSV getStGPGSV(){
	//	return stGPGSV;
	//	}
	//
	//	public StGPGLL getStGPGLL(){
	//	return stGPGLL;
	//	}
	//
	//	public StGPVTG getStGPVTG(){
	//	return stGPVTG;
	//	}
	//
	//	public StGSA getStGSA(){
	//	return stGSA;
	//	}
	//
	//	public StHDG getStHDG(){
	//	return stHDG;
	//	}
	//
	//	public StHDT getStHDT(){
	//	return stHDT;
	//	}
	//
	//	public StMWV getStMWV(){
	//	return stMWV;
	//	}
	//
	//	public StVHW getStVHW(){
	//	return stVHW;
	//	}
	//
	//	public StDBT getStDBT(){
	//	return stDBT;
	//	}
	//
	//	public StDPT getStDPT() {
	//	return stDPT;
	//	}
	//
	//	public StBWC getStBWC(){
	//	return stBWC;
	//	}
	//
	//	public String getId() {
	//
	//	return parts[0].substring(1, 3);
	//	}
}
