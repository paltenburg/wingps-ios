//
//  StMID.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 27/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation

class StMID {
    
    var mid : Int?
    var country : String?
    var flag : String?
    
    init(mmsi : Int) {
        self.mid = mmsiToMid(mmsi)
        country = midToCountry()
//        flag = countryToFlag()
        flag = flagByCountryCode(country)
    }
    
    func mmsiToMid(_ mmsi :Int) -> Int {
        if mmsi > 99999999 {
            return Int(String(mmsi).subString(from: 0, to: 3)) ?? 0
        } else {
            return 0
        }
    }
    
//    func countryToFlag() -> String {
//        return "ic_flag_" + country.lowercased() // + ".png";
//    }

    func midToCountry() -> String {
        var result = "unknown"
        switch self.mid {
        case 201:  result = "AL" // Albania
        case 202:  result = "AD" // Andorra
        case 203:  result = "AT" // Austria
        case 204:  result = "Azores" // Azores - Portugal
        case 205:  result = "BE" // Belgium
        case 206:  result = "BY" // Belarus
        case 207:  result = "BG" // Bulgaria
        case 208:  result = "VA" // Vatican
        case 209, 210: result = "CY" // Cyprus
        case 211:  result = "DE" // Germany
        case 212:  result = "CY" // Cyprus
        case 213:  result = "GE" // Germany
        case 214:  result = "MD" // Moldova
        case 215:  result = "MT" // Malta
        case 216:  result = "AM" // Armenia
        case 218:  result = "DE" // Germany
        case 219, 220:  result = "DK" // Denmark
        case 224, 225:  result = "ES" // Spain
        case 226, 227, 228:  result = "FR" // France
        case 229:  result = "MT" // Malta
        case 230:  result = "FI" // Finland
        case 231:  result = "FO" // Faroe Islands - Denmark
        case 232, 233, 234, 235:  result = "UK" // United Kingdom of Great Britain and Northern Ireland
        case 236:  result = "GI" // Gibraltar
        case 237:  result = "GR" // Greece
        case 238:  result = "HR" // Croatia (Republic of)
        case 239, 240, 241:  result = "GR" // Greece
        case 242:  result = "MA" // Morocco (Kingdom of)
        case 243:  result = "HU" // Hungary
        case 244, 245, 246:  result = "NL" // Netherlands (Kingdom of the)
        case 247:  result = "IT" // Italy
        case 248, 249:  result = "MT" // Malta
        case 250:  result = "IE" // Ireland
        case 251:  result = "IS" // Iceland
        case 252:  result = "LI" // Liechtenstein (Principality of)
        case 253:  result = "LU" // Luxembourg
        case 254:  result = "MC" // Monaco (Principality of)
        case 255:  result = "Madeira" // Madeira - Portugal
        case 256:  result = "MT" // Malta
        case 257, 258, 259:  result = "NO" // Norway
        case 261:  result = "PL" // Poland (Republic of)
        case 262:  result = "Montenegro" // Montenegro
        case 263:  result = "PT" // Portugal
        case 264:  result = "RO" // Romania
        case 265, 266:  result = "SE" // Sweden
        case 267:  result = "SK" // Slovak Republic
        case 268:  result = "SM" // San Marino (Republic of)
        case 269:  result = "CH" // Switzerland (Confederation of)
        case 270:  result = "CZ" // Czech Republic
        case 271:  result = "TR" // Turkey
        case 272:  result = "UA" // Ukraine
        case 273:  result = "RU" // Russian Federation
        case 274:  result = "MK" // The Former Yugoslav Republic of Macedonia
        case 275:  result = "LV" // Latvia (Republic of)
        case 276:  result = "EE" // Estonia (Republic of)
        case 277:  result = "LT" // Lithuania (Republic of)
        case 278:  result = "SI" // Slovenia (Republic of)
        case 279:  result = "Serbia" // Serbia (Republic of)
        case 301:  result = "AI" // Anguilla
        case 303:  result = "Alaska" // Alaska (State of) - United States of America
        case 304, 305:  result = "AG" // Antigua and Barbuda
        case 306:  result = "AN";
        case 307:  result = "Aruba";
        case 308, 309:  result = "BS" // Bahamas (Commonwealth of the)
        case 310:  result = "BM" // Bermuda - United Kingdom of Great Britain and Northern Ireland
        case 311:  result = "BS" // Bahamas (Commonwealth of the)
        case 312:  result = "BZ" // Belize
        case 314:  result = "BB" // Barbados
        case 316:  result = "CA" // Canada
        case 319:  result = "KY" // Cayman Islands - United Kingdom of Great Britain and Northern Ireland
        case 321:  result = "CR" // Costa Rica
        case 323:  result = "CU" // Cuba
        case 325:  result = "DM" // Dominica (Commonwealth of)
        case 327:  result = "DO" // Dominican Republic
        case 329:  result = "GP" // Guadeloupe (French Department of) - France
        case 330:  result = "GD" // Grenada
        case 331:  result = "GL" // Greenland - Denmark
        case 332:  result = "GT" // Guatemala (Republic of)
        case 334:  result = "HN" // Honduras (Republic of)
        case 336:  result = "HT" // Haiti (Republic of)
        case 338:  result = "US" // United States of America
        case 339:  result = "JM" // Jamaica
        case 341:  result = "KN" // Saint Kitts and Nevis (Federation of)
        case 343:  result = "LC" // Saint Lucia
        case 345:  result = "MX" // Mexico
        case 347:  result = "MQ" // Martinique (French Department of) - France
        case 348:  result = "MS" // Montserrat - United Kingdom of Great Britain and Northern Ireland
        case 350:  result = "NI" // Nicaragua
        case 351, 352, 353, 354, 355, 356, 357:  result = "PA" // Panama (Republic of)
        case 358:  result = "PR" // Puerto Rico - United States of America
        case 359:  result = "SV" // El Salvador (Republic of)
        case 361:  result = "PM" // Saint Pierre and Miquelon (Territorial Collectivity of) - France
        case 362:  result = "TT" // Trinidad and Tobago
        case 364:  result = "TC" // Turks and Caicos Islands - United Kingdom of Great Britain and Northern Ireland
        case 366, 367, 368, 369:  result = "US" // United States of America
        case 370, 371, 372, 373:  result = "PA" // Panama (Republic of)
        case 375, 376, 377:  result = "VC" // Saint Vincent and the Grenadines
        case 378:  result = "VG" // British Virgin Islands - United Kingdom of Great Britain and Northern Ireland
        case 379:  result = "VI" // United States Virgin Islands - United States of America
        case 401:  result = "AF" // Afghanistan
        case 403:  result = "SA" // Saudi Arabia (Kingdom of)
        case 405:  result = "BD" // Bangladesh (People's Republic of)
        case 408:  result = "BH" // Bahrain (Kingdom of)
        case 410:  result = "BT" // Bhutan (Kingdom of)
        case 412, 413, 414:  result = "CN" // China (People's Republic of)
        case 416:  result = "TW" // Taiwan (Province of China) - China (People's Republic of)
        case 417:  result = "LK" // Sri Lanka (Democratic Socialist Republic of)
        case 419:  result = "IN" // India (Republic of)
        case 422:  result = "IR" // Iran (Islamic Republic of)
        case 423:  result = "AZ" // Azerbaijani Republic
        case 425:  result = "IQ" // Iraq (Republic of)
        case 428:  result = "IL" // Israel (State of)
        case 431, 432:  result = "JP" // Japan
        case 434:  result = "TM" // Turkmenistan
        case 436:  result = "KZ" // Kazakhstan (Republic of)
        case 437:  result = "UZ" // Uzbekistan (Republic of)
        case 438:  result = "JO" // Jordan (Hashemite Kingdom of)
        case 440, 441:  result = "KR" // Korea (Republic of)
        case 443:  result = "PS" // Palestine (In accordance with Resolution 99 Rev. Guadalajara, 2010)
        case 445:  result = "KP" // Democratic People's Republic of Korea
        case 447:  result = "KW" // Kuwait (State of)
        case 450:  result = "LB" // Lebanon
        case 451:  result = "KG" // Kyrgyz Republic
        case 453:  result = "MO" // Macao (Special Administrative Region of China) - China (People's Republic of)
        case 455:  result = "MV" // Maldives (Republic of)
        case 457:  result = "MN" // Mongolia
        case 459:  result = "NP" // Nepal (Federal Democratic Republic of)
        case 461:  result = "OM" // Oman (Sultanate of)
        case 463:  result = "PK" // Pakistan (Islamic Republic of)
        case 466:  result = "QA" // Qatar (State of)
        case 468:  result = "SY" // Syrian Arab Republic
        case 470:  result = "AE" // United Arab Emirates
        case 473, 475:  result = "YE" // Yemen (Republic of)
        case 477:  result = "HK" // Hong Kong (Special Administrative Region of China) - China (People's Republic of)
        case 478:  result = "BA" // Bosnia and Herzegovina
        case 501:  result = "AdelieLand" // Adelie Land - France
        case 503:  result = "AU" // Australia
        case 506:  result = "MM" // Myanmar (Union of)
        case 508:  result = "BN" // Brunei Darussalam
        case 510:  result = "FM" // Micronesia (Federated States of)
        case 511:  result = "PW" // Palau (Republic of)
        case 512:  result = "NZ" // New Zealand
        case 514, 515:  result = "KH" // Cambodia (Kingdom of)
        case 516:  result = "CX" // Christmas Island (Indian Ocean) - Australia
        case 518:  result = "CK" // Cook Islands - New Zealand
        case 520:  result = "FJ" // Fiji (Republic of)
        case 523:  result = "CC" // Cocos (Keeling) Islands - Australia
        case 525:  result = "ID" // Indonesia (Republic of)
        case 529:  result = "KI" // Kiribati (Republic of)
        case 531:  result = "LA" // Lao People's Democratic Republic
        case 533:  result = "MY" // Malaysia
        case 536:  result = "MP" // Northern Mariana Islands (Commonwealth of the) - United States of America
        case 538:  result = "MH" // Marshall Islands (Republic of the)
        case 540:  result = "NC" // New Caledonia - France
        case 542:  result = "NU" // Niue - New Zealand
        case 544:  result = "NR" // Nauru (Republic of)
        case 546:  result = "PF" // French Polynesia - France
        case 548:  result = "PH" // Philippines (Republic of the)
        case 553:  result = "PG" // Papua New Guinea
        case 555:  result = "PN" // Pitcairn Island - United Kingdom of Great Britain and Northern Ireland
        case 557:  result = "SB" // Solomon Islands
        case 559:  result = "AS" // American Samoa - United States of America
        case 561:  result = "WS" // Samoa (Independent State of)
        case 563, 564, 565, 566:  result = "SG" // Singapore (Republic of)
        case 567:  result = "TH" // Thailand
        case 570:  result = "TO" // Tonga (Kingdom of)
        case 572:  result = "TV" // Tuvalu
        case 574:  result = "VN" // Viet Nam (Socialist Republic of)
        case 576, 577:  result = "VU" // Vanuatu (Republic of)
        case 578:  result = "WF" // Wallis and Futuna Islands - France
        case 601:  result = "ZA" // South Africa (Republic of)
        case 603:  result = "AO" // Angola (Republic of)
        case 605:  result = "DZ" // Algeria (People's Democratic Republic of)
        case 607:  result = "TF" // Saint Paul and Amsterdam Islands - France
        case 608:  result = "Ascension" // Ascension Island - United Kingdom of Great Britain and Northern Ireland
        case 609:  result = "BI" // Burundi (Republic of)
        case 610:  result = "BJ" // Benin (Republic of)
        case 611:  result = "BW" // Botswana (Republic of)
        case 612:  result = "CF" // Central African Republic
        case 613:  result = "CM" // Cameroon (Republic of)
        case 615:  result = "CG" // Congo (Republic of the)
        case 616:  result = "KM" // Comoros (Union of the)
        case 617:  result = "CV" // Cape Verde (Republic of)
        case 618:  result = "TF" // Crozet Archipelago - France
        case 619:  result = "CI" // CÙte d'Ivoire (Republic of)
        case 621:  result = "DJ" // Djibouti (Republic of)
        case 622:  result = "EG" // Egypt (Arab Republic of)
        case 624:  result = "ET" // Ethiopia (Federal Democratic Republic of)
        case 625:  result = "ER" // Eritrea
        case 626:  result = "GA" // Gabonese Republic
        case 627:  result = "GH" // Ghana
        case 629:  result = "GM" // Gambia (Republic of the)
        case 630:  result = "GW" // Guinea-Bissau (Republic of)
        case 631:  result = "GQ" // Equatorial Guinea (Republic of)
        case 632:  result = "GN" // Guinea (Republic of)
        case 633:  result = "BF" // Burkina Faso
        case 634:  result = "KE" // Kenya (Republic of)
        case 635:  result = "TF" // Kerguelen Islands - France
        case 636, 637:  result = "LR" // Liberia (Republic of)
        case 642:  result = "LY" // Libya;
        case 644:  result = "LS" // Lesotho (Kingdom of)
        case 645:  result = "MU" // Mauritius (Republic of)
        case 647:  result = "MG" // Madagascar (Republic of)
        case 649:  result = "ML" // Mali (Republic of)
        case 650:  result = "MZ" // Mozambique (Republic of)
        case 654:  result = "MR" // Mauritania (Islamic Republic of)
        case 655:  result = "MW" // Malawi
        case 656:  result = "NE" // Niger (Republic of the)
        case 657:  result = "NG" // Nigeria (Federal Republic of)
        case 659:  result = "NA" // Namibia (Republic of)
        case 660:  result = "RE" // Reunion (French Department of) - France
        case 661:  result = "RW" // Rwanda (Republic of)
        case 662:  result = "SD" // Sudan (Republic of the)
        case 663:  result = "SN" // Senegal (Republic of)
        case 664:  result = "SC" // Seychelles (Republic of)
        case 665:  result = "SH" // Saint Helena - United Kingdom of Great Britain and Northern Ireland
        case 666:  result = "SO" // Somali Democratic Republic
        case 667:  result = "SL" // Sierra Leone
        case 668:  result = "ST" // Sao Tome and Principe (Democratic Republic of)
        case 669:  result = "SZ" // Swaziland (Kingdom of)
        case 670:  result = "TD" // Chad (Republic of)
        case 671:  result = "TG" // Togolese Republic
        case 672:  result = "TN" // Tunisia
        case 674:  result = "TZ" // Tanzania (United Republic of)
        case 675:  result = "UG" // Uganda (Republic of)
        case 676:  result = "CD" // Democratic Republic of the Congo
        case 677:  result = "TZ" // Tanzania (United Republic of)
        case 678:  result = "ZM" // Zambia (Republic of)
        case 679:  result = "ZW" // Zimbabwe (Republic of)
        case 701:  result = "AR" // Argentine Republic
        case 710:  result = "BR" // Brazil (Federative Republic of)
        case 720:  result = "BO" // Bolivia (Plurinational State of)
        case 725:  result = "CL" // Chile
        case 730:  result = "CO" // Colombia (Republic of)
        case 735:  result = "EC" // Ecuador
        case 740:  result = "FK" // Falkland Islands (Malvinas) - United Kingdom of Great Britain and Northern Ireland
        case 745:  result = "GF" // Guiana (French Department of) - France
        case 750:  result = "GY" // Guyana
        case 755:  result = "PY" // Paraguay (Republic of)
        case 760:  result = "PE" // Peru
        case 765:  result = "SR" // Suriname (Republic of)
        case 770:  result = "UY" // Uruguay (Eastern Republic of)
        case 775:  result = "VE" // Venezuela (Bolivarian Republic of)
        default:
            result = "unknown"
        }
        return  result
    }
    
    func flagByCountryCode(_ cc :String?) -> String {
        
        guard let cc = cc else { return "" }
        
        let base : UInt32 = 127397
        var s = ""
        for v in cc.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
}
