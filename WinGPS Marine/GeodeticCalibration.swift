//
//  GeodeticCalibration.swift
//  MapDemo
//
//  Created by Standaard on 31/10/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

class GeodeticCalibration {
	let GETSCALEMAX_SAMPLECOUNT :Int = 6
	//
	var calPix1 :[Double]?       // Position of first calibration point in pixel space.
	var calPix2 :[Double]?       // Position of second calibration point in pixel space.
	var calPix3 :[Double]?       // Position of third calibration point in pixel space.
	var calPointCount :Int?      // Number of calibration points (2 or 3).
	var calProj1 :[Double]?      // Position of first calibration point in projection space.
	var calProj2 :[Double]?      // Position of second calibration point in projection space.
	var calProj3 :[Double]?      // Position of third calibration point in projection space.
	var calRz :Double?         // Angle between projection space X-axis and pixel space X-axis [rad].
	var calScale :Double2?      // Scale between projection space and pixel space in pixel space X- and Y-direction [pix/proj-unit].
	var height :Int = 0      // Height of the surface in pixels.
	var initialized :Bool?   // True, if the calibration has been initialized.
	var matrix = CalibrationMatrix()        // Matrix to transform from projection space to pixel space.
	var matrixInv = CalibrationMatrix()     // Matrix to transform from pixel space to projection space.
	var ref :GeodeticRef?       // Geodetic reference.
	var width :Int = 0       // Width of the surface in pixels.
	//
	//    public StGeodeticCalibration() {
	//    matrix = CalibrationMatrix()
	//    matrixInv = CalibrationMatrix()
	//    mRef = null;
	//    mInitialized = false;
	//    }
	//
	//    public Double2 glbWGS84ToPix(final Double3 PosGlb) {
	//    Geo2 Geo = globalToGeo2(PosGlb, null);
	//    return geoWGS84ToPix(Geo);
	//    }
	//
	//    public static Double3 geoToGlobal(final Geo2 aSrc) {
	//    double SinLat = Math.sin(aSrc.Lat);
	//    double CosLat = Math.cos(aSrc.Lat);
	//    double SinLong = Math.sin(aSrc.Long);
	//    double CosLong = Math.cos(aSrc.Long);
	//
	//    // Calculate radius of curvature in prime vertical [ref 1, eq 4]
	//    double N = StWGS84Ellipsoid.ellipsoid().a / Math.sqrt(1.0 - StWGS84Ellipsoid.ellipsoid().eSqr * SinLat * SinLat);
	//
	//    // Calculate X, Y, and Z [ref 1, eq 6]
	//    double R = N * CosLat;
	//    return new Double3(R * CosLong, -R * SinLong, N * (1.0 -  StWGS84Ellipsoid.ellipsoid().eSqr) * SinLat);
	//    }
	//
	//    public static Double3 geoToGlobal(final Geo2 aSrc, Double3 aReuse) {
	//    double SinLat = Math.sin(aSrc.Lat);
	//    double CosLat = Math.cos(aSrc.Lat);
	//    double SinLong = Math.sin(aSrc.Long);
	//    double CosLong = Math.cos(aSrc.Long);
	//
	//    // Calculate radius of curvature in prime vertical [ref 1, eq 4]
	//    double N = StWGS84Ellipsoid.ellipsoid().a / Math.sqrt(1.0 - StWGS84Ellipsoid.ellipsoid().eSqr * SinLat * SinLat);
	//
	//    // Calculate X, Y, and Z [ref 1, eq 6]
	//    double R = N * CosLat;
	//    Double3 Res = (aReuse == null) ? new Double3() : aReuse;
	//    Res.x = R * CosLong;
	//    Res.y = -R * SinLong;
	//    Res.z = N * (1.0 -  StWGS84Ellipsoid.ellipsoid().eSqr) * SinLat;
	//    return Res;
	//    }
	//
	//    public static Double3 geoToGlobal(final Geo3 aSrc, Double3 aReuse) {
	//    StWGS84Ellipsoid WGS84 = StWGS84Ellipsoid.ellipsoid();
	//
	//    double SinLat = Math.sin(aSrc.Lat);
	//    double CosLat = Math.cos(aSrc.Lat);
	//    double SinLong = Math.sin(aSrc.Long);
	//    double CosLong = Math.cos(aSrc.Long);
	//
	//    // Calculate radius of curvature in prime vertical [ref 1, eq 4]
	//    double N = WGS84.a / Math.sqrt(1 - WGS84.eSqr * SinLat * SinLat);
	//
	//    // Calculate X, Y, and Z [ref 1, eq 6]
	//    double R = (N + aSrc.Height) * CosLat;
	//    Double3 Res = (aReuse == null) ? new Double3() : aReuse;
	//    Res.x = R * CosLong;
	//    Res.y = -R * SinLong;   // Added a minus because the coordinate system is left-handed
	//    Res.z = (N * (1 - WGS84.eSqr) + aSrc.Height) * SinLat;
	//    return Res;
	//    }
	
	func geoToMercator(srcGeoDeg :CLLocationCoordinate2D, dstRef :GeodeticRef) -> Double2 {

		var geoRad = Geo2()
		
		geoRad.lat = degToRad(srcGeoDeg.latitude)
		geoRad.lon = degToRad(srcGeoDeg.longitude)
		
		return geoToMercator(srcGeoRad: geoRad, dstRef: dstRef)
		
	}
	
	func geoToMercator(srcGeoRad :Geo2, dstRef :GeodeticRef) -> Double2 {
		
		let dlon = srcGeoRad.lon - dstRef.long0
		//     while (dlon < -Math.PI)
		//        dlon += StMath.TWOPI;
		//        while (dlon >= Math.PI)
		//         dlon -= StMath.TWOPI;
		
		var res = Double2()
		res.x = WGS84Ellipsoid.ellipsoid().a * dlon
		
		let sinLat = sin(srcGeoRad.lat)
		
		res.y = 0.5 * WGS84Ellipsoid.ellipsoid().a * log((1.0 + sinLat) / (1.0 - sinLat) *
			exp(WGS84Ellipsoid.ellipsoid().e * log((1.0 - WGS84Ellipsoid.ellipsoid().e * sinLat) / (1.0 + WGS84Ellipsoid.ellipsoid().e * sinLat))))
		/*
		return 0.5 * aE->a * Ln((1.0 + SinLat) / (1.0 - SinLat) *
		Exp(aE->e * Ln((1.0 - aE->e * SinLat) / (1.0 + aE->e * SinLat)))); // [ref 2, eq 7-7a]
		*/
		return res;
	}
	
	func geoToProj(SrcRad :Geo2, DstRef: GeodeticRef) -> Double2 {
		if DstRef.projection == GeodeticProjection.TRANSVERSEMERCATOR {
			return geoToTransverseMercator(SrcGeoRad: SrcRad, DstRef: DstRef)
		} else {
			return geoToMercator(srcGeoRad: SrcRad, dstRef: DstRef)
		}
	}
	
	func geoToTransverseMercator(SrcGeoRad :Geo2, DstRef :GeodeticRef) -> Double2 {
		let SinLat = sin(SrcGeoRad.lat)
		let CosLat = cos(SrcGeoRad.lat)
		let TanLat = SinLat / CosLat
		let N = WGS84Ellipsoid.ellipsoid().a / sqrt(1 - WGS84Ellipsoid.ellipsoid().eSqr * SinLat * SinLat)
		let T = TanLat * TanLat
		let C = WGS84Ellipsoid.ellipsoid().e1Sqr * CosLat * CosLat
		let A1 = (SrcGeoRad.lon - DstRef.long0) * CosLat
		let A2 = A1 * A1
		
		let M = (SrcGeoRad.lat)
		
		var res = Double2()
		res.x = DstRef.k0 * N * (1.0 + ((1.0 - T + C) * (1.0 / 6.0) + (5.0 - 18.0 * T + T * T + 72.0 * C - 58.0 * WGS84Ellipsoid.ellipsoid().e1Sqr) *
			(1.0 / 120.0) * A2) * A2) * A1
		res.y = DstRef.k0 * (M - DstRef.M0 + N * TanLat * (0.5 + ((5.0 - T + 9.0 * C + 4.0 * C * C) * (1.0 / 24.0) +
			(61.0 - 58.0 * T + T * T + 600.0 * C - 330.0 * WGS84Ellipsoid.ellipsoid().e1Sqr) * (1.0 / 720.0) * A2) * A2) * A2)
		return res
	}
	
	func geoWGS84ToPix(srcGeo :CLLocationCoordinate2D) -> Double2 {
		let proj :Double2
		if ref!.projection == GeodeticProjection.TRANSVERSEMERCATOR {
			proj = geoToTransverseMercator(SrcGeoRad: Geo2(srcGeo), DstRef: ref!)
		} else {
			proj = geoToMercator(srcGeoDeg: srcGeo, dstRef: ref!)
		}
		var pix :Double2 = matrixProjToPix(Proj: proj, Matrix: matrix)
		return pix
	}
	//
	//    //    public Double2 geoWGS84ToPix(final Geo2 SrcGeo) {
	//    //        return geoWGS84ToPix(Double2.makeDouble2(SrcGeo.Long, SrcGeo.Lat));
	//    //    }
	//
	//    public double getBoundingSphereWGS84(Double3 aCenterGlb) {
	//    int PointCount = 5;
	//    BoundingSphere BoundingSphere = new BoundingSphere(PointCount * PointCount);
	//    for (int y = 0; y < PointCount; y++) {
	//    for (int x = 0; x < PointCount; x++) {
	//    Double2 PosPix = new Double2((x / (PointCount - 1.0)) * mWidth - 0.5, (y / (PointCount - 1.0)) * mHeight - 0.5);
	//    Double3 PosGlb = pixToGlbWGS84(PosPix);
	//    BoundingSphere.addPoint(PosGlb);
	//    }
	//    }
	//    return BoundingSphere.calc(aCenterGlb);
	//    }
	//
	//    public int getHeight() {
	//    return mHeight;
	//    }
	//
	//    public boolean getInitialized() {
	//    return mInitialized;
	//    }
	
	func getPixBottomLeft() -> Double2 {
		return Double2(x: -0.5, y: Double(height) - 0.5)
	}
	
	func getPixBottomRight() -> Double2 {
		return Double2(x: Double(width) - 0.5, y: Double(height) - 0.5)
	}
	
	func getPixCenter() -> Double2 {
		return Double2(x: Double(width) / 2.0 - 0.5, y: Double(height) / 2.0 - 0.5)
	}
	
	//    public void getPixCenter(Double2 aCenterPix) {
	//    aCenterPix.x = mWidth / 2.0 - 0.5;
	//    aCenterPix.y = mHeight / 2.0 - 0.5;
	//    }
	
	func getPixTopLeft() -> Double2 {
		return Double2(x: -0.5, y: -0.5)
	}
	
	func getPixTopRight() -> Double2 {
		return Double2(x: Double(width) - 0.5, y: -0.5)
	}
	
	//    public StGeodeticRef getRef() {
	//    return mRef;
	//    }
	
	// Scale expressed in distance per pixel
	func getScale(pix :Double2, dir :Double2) -> Double {
		let N = Math.normalize(dir)
		let P1 = Double2(x: pix.x - 0.5 * N.x, y: pix.y - 0.5 * N.y)
		let P2 = Double2(x: pix.x + 0.5 * N.x, y: pix.y + 0.5 * N.y)
		
		let geo1 = pixToGeoRadWGS84(P1)
		let geo2 = pixToGeoRadWGS84(P2)
		
		return GeodeticsGreatCircle.distAlongGreatCircle(geoRad1: geo1, geoRad2: geo2)
	}
	
	func getScaleMax(pix :Double2) -> Double {
		var scaleMax = Double(0)
		var dir = Double2()
		for i in (0 ..< GETSCALEMAX_SAMPLECOUNT).reversed() {
			let A :Double = Double(i) * (Double.pi / Double(GETSCALEMAX_SAMPLECOUNT))
			dir.y = sin(A)
			dir.x = cos(A)
			let scale = getScale(pix: pix, dir: dir)
			if scale > scaleMax {
				scaleMax = scale
			}
		}
		return scaleMax
	}
	
	//    public int getWidth() {
	//    return mWidth;
	//    }
	//
	//    public static Geo2 globalToGeo2(final Double3 aSrc, Geo2 aReuse) {
	//    Geo2 Dst = (aReuse == null) ? new Geo2() : aReuse;
	//
	//    double r = Math.sqrt(aSrc.x * aSrc.x + aSrc.y * aSrc.y);
	//    if ((aSrc.z != 0) && (Math.abs(r / aSrc.z) < 1e-10)) {  // The point lays less then 1 mm from the North or South pole
	//    if (aSrc.z >= 0)
	//    Dst.Lat = StMath.HALFPI;
	//    else
	//    Dst.Lat = -StMath.HALFPI;
	//    Dst.Long = 0;
	//    } else {
	//    double A = Math.atan((aSrc.z * StWGS84Ellipsoid.ellipsoid().a) / (r * StWGS84Ellipsoid.ellipsoid().b));
	//    double Sin3Theta = Math.sin(A);
	//    double Cos3Theta = Math.cos(A);
	//    Sin3Theta = Sin3Theta * Sin3Theta * Sin3Theta;
	//    Cos3Theta = Cos3Theta * Cos3Theta * Cos3Theta;
	//
	//    Dst.Lat = Math.atan((aSrc.z + StWGS84Ellipsoid.ellipsoid().e1Sqr * StWGS84Ellipsoid.ellipsoid().b * Sin3Theta) / (r - StWGS84Ellipsoid.ellipsoid().eSqr * StWGS84Ellipsoid.ellipsoid().a * Cos3Theta));
	//    Dst.Long = Math.atan2(-aSrc.y, aSrc.x);  // Added a minus because coordinate system is left-handed
	//    }
	//    return Dst;
	//    }
	//
	//    public static Geo3 globalToGeo3(final Double3 aSrc) {
	//    Geo3 Dst = new Geo3();
	//
	//    double r = Math.sqrt(aSrc.x * aSrc.x + aSrc.y * aSrc.y);
	//    if ((aSrc.z != 0) && (Math.abs(r / aSrc.z) < 1e-10)) {  // The point lays less then 1 mm from the North or South pole
	//    if (aSrc.z >= 0)
	//    Dst.Lat = StMath.HALFPI;
	//    else
	//    Dst.Lat = -StMath.HALFPI;
	//    Dst.Long = 0;
	//    Dst.Height = Math.abs(aSrc.z) - StWGS84Ellipsoid.ellipsoid().b;
	//    } else {
	//    double A = Math.atan((aSrc.z * StWGS84Ellipsoid.ellipsoid().a) / (r * StWGS84Ellipsoid.ellipsoid().b));
	//    double Sin3Theta = Math.sin(A);
	//    double Cos3Theta = Math.cos(A);
	//    Sin3Theta = Sin3Theta * Sin3Theta * Sin3Theta;
	//    Cos3Theta = Cos3Theta * Cos3Theta * Cos3Theta;
	//
	//    Dst.Lat = Math.atan((aSrc.z + StWGS84Ellipsoid.ellipsoid().e1Sqr * StWGS84Ellipsoid.ellipsoid().b * Sin3Theta) / (r - StWGS84Ellipsoid.ellipsoid().eSqr * StWGS84Ellipsoid.ellipsoid().a * Cos3Theta));
	//    Dst.Long = Math.atan2(-aSrc.y, aSrc.x);  // Added a minus because coordinate system is left-handed
	//    double SinLat = Math.sin(Dst.Lat);
	//    double CosLat = Math.cos(Dst.Lat);
	//    double N = StWGS84Ellipsoid.ellipsoid().a / Math.sqrt(1 - StWGS84Ellipsoid.ellipsoid().eSqr * SinLat * SinLat);  // Radius of curvature in prime vertical
	//    Dst.Height = r / CosLat - N;
	//    }
	//    return Dst;
	//    }
    
    static func globalToGeoPoint(_ src :Double3) -> GeoPointE6 {
        let geoPoint :GeoPointE6 = GeoPointE6()
        let r :Double = (src.x * src.x + src.y * src.y).squareRoot()
        if ((src.z != 0) && (abs(r / src.z) < 1e-10)) {  // The point lays less then 1 mm from the North or South pole
            if (src.z >= 0) {
                geoPoint.latE6 = StUtils.radToDegE6(Double.pi / 2)
            } else {
                geoPoint.latE6 = StUtils.radToDegE6(-Double.pi / 2)
            }
            geoPoint.lonE6 = 0
        } else {
            let A :Double = atan((src.z * WGS84Ellipsoid.ellipsoid().a) / (r * WGS84Ellipsoid.ellipsoid().b))
            var Sin3Theta :Double = sin(A)
            var Cos3Theta :Double = cos(A)
            Sin3Theta = Sin3Theta * Sin3Theta * Sin3Theta
            Cos3Theta = Cos3Theta * Cos3Theta * Cos3Theta
            
            geoPoint.latE6 = StUtils.radToDegE6(atan((src.z + WGS84Ellipsoid.ellipsoid().e1Sqr * WGS84Ellipsoid.ellipsoid().b * Sin3Theta) / (r - WGS84Ellipsoid.ellipsoid().eSqr * WGS84Ellipsoid.ellipsoid().a * Cos3Theta)))
            geoPoint.lonE6 = StUtils.radToDegE6(atan2(-src.y, src.x)) // Added a minus because coordinate system is left-handed
        }
        return geoPoint
    }
    
	//    public static double headingAlongLine(final Geo2 aGeo1, final Geo2 aGeo2) {
	//
	//    // Transform both points to global space.
	//    Double3 P1, P2;
	//    P1 = geoToGlobal(aGeo1);
	//    P2 = geoToGlobal(aGeo2);
	//
	//    // Create horizontal space at P1.
	//    Double3x4 MatrixHorToGlb = new Double3x4();
	//    Double3x4 MatrixGlbToHor = new Double3x4();
	//    double SinLat = Math.sin(aGeo1.Lat);
	//    double CosLat = Math.cos(aGeo1.Lat);
	//    double SinLong = Math.sin(aGeo1.Long);
	//    double CosLong = Math.cos(aGeo1.Long);
	//
	//    matrixCreateHorToGlb(MatrixHorToGlb, SinLat, CosLat, SinLong, CosLong, P1);
	//    StMathMatrices.invert(MatrixGlbToHor, MatrixHorToGlb);
	//
	//    // Transform P2 to horizontal space.
	//    StMathMatrices.mul(P2, P2, MatrixGlbToHor);
	//
	//    // Calculate heading. P1 is at the origin of the horizontal space.
	//    return Math.atan2(P2.y, P2.x);
	//    }
	
	func initialize2(calPix1 :[Double], calPix2 :[Double], calProj1 :[Double], calProj2 :[Double], width :Int, height :Int, ref :GeodeticRef) {
		init1(calPointCount: 2, calPix1: calPix1, calPix2: calPix2, calPix3: [0,0], calProj1: calProj1, calProj2: calProj2, calProj3: [0,0], width: width, height: height, ref: ref)
		init2()
		initialized = true
	}
	
	func initialize3(calPix1 :Double2, calPix2 :Double2, calPix3 :Double2, calProj1 :Double2, calProj2 :Double2, calProj3 :Double2, width :Int, height :Int, ref :GeodeticRef) -> Bool {
		
		init1(calPointCount: 3, calPix1: calPix1.getArray(), calPix2: calPix2.getArray(), calPix3: calPix3.getArray(), calProj1: calProj1.getArray(), calProj2: calProj2.getArray(), calProj3: calProj3.getArray(), width: width, height: height, ref: ref)
		init3()
		initialized = true
		return initialized!
	}
	
	//    public static double latToMercatorNorthing(double aLat) {
	//    StWGS84Ellipsoid WGS84 = StWGS84Ellipsoid.ellipsoid();
	//
	//    double SinLat = Math.sin(aLat);
	//    return 0.5 * WGS84.a * Math.log((1.0 + SinLat) / (1.0 - SinLat) * Math.exp(WGS84.e * Math.log((1.0 - WGS84.e * SinLat) / (1.0 + WGS84.e * SinLat))));
	//    }
	
	func pixToGeoRadWGS84(_ pix :Double2) -> Geo2 {
		let proj :Double2 = matrixPixToProj(pix: pix, matrix: matrixInv)
		var Geo = Geo2()
		if ref!.projection == GeodeticProjection.TRANSVERSEMERCATOR {
			Geo = transverseMercatorToGeoRad(src: proj, srcRef: ref!)
		} else {
			Geo = mercatorToGeoRad(src: proj, srcRef: ref!)
		}
		return Geo
	}
	
	//    public Double3 pixToGlbWGS84(final Double2 Pix) {
	//    Geo2 Geo = pixToGeoWGS84(Pix);
	//    return geoToGlobal(Geo);
	//    }
	//
	func pixToProj(Pix :Double2) -> Double2 {
		return matrixPixToProj(pix: Pix, matrix: matrixInv)
	}
	
	func pixTransform(dstCal :GeodeticCalibration, srcPix :Double2) -> Double2 {
		var Proj = pixToProj(Pix: srcPix)
		
		if !ref!.isEqualRef(dstCal.ref!) {
			let geoRad = projToGeoRad(Src: Proj, Ref: ref!)
			Proj = geoToProj(SrcRad: geoRad, DstRef: dstCal.ref!)
		}
		
		return dstCal.projToPix(Proj: Proj)
	}
	
	
	func projToGeoRad(Src :Double2, Ref :GeodeticRef) -> Geo2 {
		if Ref.projection == GeodeticProjection.TRANSVERSEMERCATOR {
			return transverseMercatorToGeoRad(src: Src, srcRef: Ref)
		} else {
			return mercatorToGeoRad(src: Src, srcRef: Ref)
		}
	}
	
	func projToPix(Proj :Double2) -> Double2 {
		return matrixProjToPix(Proj: Proj, Matrix: matrix)
	}
	
	func calcParams3(centroidPix :inout Double2, centroidProj :inout Double2, scale :inout Double2, sinRz :inout Double, cosRz :inout Double) {
		
		var pix :[Double2] = [Double2(x: 0, y: 0), Double2(x: 0, y: 0), Double2(x: 0, y: 0)]
		var proj :[Double2] = [Double2(x: 0, y: 0), Double2(x: 0, y: 0), Double2(x: 0, y: 0)]
		
		centroidPix.x = (1.0/3.0) * (calPix1![0] + calPix2![0] + calPix3![0])
		centroidPix.y = (1.0/3.0) * (calPix1![1] + calPix2![1] + calPix3![1])
		var margin :Double = Math.distSqrD(A: calPix1!, B: calPix2!)
		margin = min(margin, Math.distSqrD(A: calPix2!, B: calPix3!))
		margin = min(margin, Math.distSqrD(A: calPix3!, B: calPix1!))
		margin = Double(0.16666667) * margin.squareRoot()
		
		if abs(centroidPix.x) >= margin && abs(centroidPix.y) >= margin {
			pix[0].x = calPix1![0]
			pix[0].y = calPix1![1]
			pix[1].x = calPix2![0]
			pix[1].y = calPix2![1]
			pix[2].x = calPix3![0]
			pix[2].y = calPix3![1]
			proj[0] = Double2(calProj1!)
			proj[1] = Double2(calProj2!)
			proj[2] = Double2(calProj3!)
		} else {
			if (mirrorPix(pix: &pix, centroidPix: &centroidPix, mirrorPix: Double2(calPix1!), margin: margin)){
				mirrorProj(proj: &proj, mirrorProj: Double2(calProj1!))
			} else if (mirrorPix(pix: &pix, centroidPix: &centroidPix, mirrorPix: Double2(calPix2!), margin: margin)) {
				mirrorProj(proj: &proj, mirrorProj: Double2(calProj2!))
			} else if (mirrorPix(pix: &pix, centroidPix: &centroidPix, mirrorPix: Double2(calPix3!), margin: margin)) {
				mirrorProj(proj: &proj, mirrorProj: Double2(calProj3!))
			} else {
				//                throw new Exception("StGeodeticCalibration::CalcParams3: No suitable calibration triangle found.")
				print("StGeodeticCalibration::CalcParams3: No suitable calibration triangle found.")
				return
			}
		}
		
		centroidProj.x = (1.0/3.0) * (proj[0].x + proj[1].x + proj[2].x);
		centroidProj.y = (1.0/3.0) * (proj[0].y + proj[1].y + proj[2].y);
		
		let XAxis :Double2 = Double2(x: 1.0, y: 0.0)
		var pixOriginProjX = Double2()
		var SXPix = Double2()
		var SXProj = Double2()
		calcPixelOriginProj(pixOriginProj: &pixOriginProjX, sPix: &SXPix, sProj: &SXProj, axis: XAxis, cPix: centroidPix, cProj: centroidProj, pix: pix, proj: proj)
		
		let YAxis = Double2(x: 0.0, y: 1.0)
		var pixOriginProjY = Double2(); var SYPix = Double2(); var SYProj = Double2()
		calcPixelOriginProj(pixOriginProj: &pixOriginProjY, sPix: &SYPix, sProj: &SYProj, axis: YAxis, cPix: centroidPix, cProj: centroidProj, pix: pix, proj: proj)
		
		let pixOriginProj = Double2(x: 0.5 * (pixOriginProjX.x + pixOriginProjY.x), y: 0.5 * (pixOriginProjX.y + pixOriginProjY.y))
		
		scale.x = abs(SXPix.x) / Math.dist(A: pixOriginProj, B: SXProj)
		scale.y = abs(SYPix.y) / Math.dist(A: pixOriginProj, B: SYProj)
		
		var Dir :Double2 = Double2(x: SXProj.x - pixOriginProj.x, y: SXProj.y - pixOriginProj.y)
		if SXPix.x < 0 {
			Dir.x = -Dir.x
			Dir.y = -Dir.y
		}
		Dir = Math.normalize(Dir)
		sinRz = Dir.y
		cosRz = Dir.x
	}
	
	func calcPixelOriginProj(pixOriginProj :inout Double2, sPix :inout Double2, sProj :inout Double2, axis :Double2,
	                         cPix :Double2, cProj :Double2, pix :[Double2], proj :[Double2]) {
		
		var hasS :[Bool] = [false, false, false]
		var S = [Double2](generating: {_ in  Double2()}, count: 3)
		var normDir = [Double2](generating: {_ in Double2()}, count: 3)
		var temp = Double2()
		
		for i in (0 ... 2).reversed() {
			
			hasS[i] = Math.intersectLines(A: Double2(), B: axis, P: cPix, Q: pix[i], S: &S[i]);
			if (hasS[i]) {
				temp.x = S[i].x - cPix.x
				temp.y = S[i].y - cPix.y
				normDir[i] = Math.normalize(temp)
			}
		}
		
		var SAPix = Double2(); var SBPix = Double2(); var PAPix = Double2(); var PBPix = Double2(); var PAProj = Double2(); var PBProj = Double2()
		var angleMax :Double = -1
		for i in (0 ... 2).reversed() {
			var iNext = i + 1
			if iNext > 2 {
				iNext = 0
			}
			if (hasS[i] && hasS[iNext]) {
				var N = Double2()
				temp.x = S[i].x - S[iNext].x
				temp.y = S[i].y - S[iNext].y
				N = Math.normalize(temp)
				var A1 :Double = Math.angleAbsNormalized(A: normDir[iNext], B: normDir[i])
				let A2 :Double = Math.angleAbsNormalized(A: normDir[i], B: N)
				let A3 :Double = Double.pi - A1 - A2
				A1 = min(A1, min(A2, A3))
				if (A1 > angleMax) {
					angleMax = A1
					SAPix = S[i]
					SBPix = S[iNext]
					PAPix = pix[i]
					PBPix = pix[iNext]
					PAProj = proj[i]
					PBProj = proj[iNext]
				}
			}
		}
		
		var SAProj = Double2(); var SBProj = Double2()
		interpolate(projP: &SAProj, projA: cProj, projB: PAProj, pixP: SAPix, pixA: cPix, pixB: PAPix)
		interpolate(projP: &SBProj, projA: cProj, projB: PBProj, pixP: SBPix, pixA: cPix, pixB: PBPix)
		
		interpolate(projP: &pixOriginProj, projA: SAProj, projB: SBProj, pixP: Double2(), pixA: SAPix, pixB: SBPix)
		if (Math.lenSqr(SAPix) > Math.lenSqr(SBPix)) {
			sPix.x = SAPix.x
			sPix.y = SAPix.y
			sProj.x = SAProj.x
			sProj.y = SAProj.y
		} else {
			sPix.x = SBPix.x
			sPix.y = SBPix.y
			sProj.x = SBProj.x
			sProj.y = SBProj.y
		}
	}
	
	func distanceAlongMeridianFromEquator(Lat :Double) -> Double {
		let WGS84 = WGS84Ellipsoid.ellipsoid()
		
		let Sin2Lat = sin(2 * Lat)
		let Cos2Lat = cos(2 * Lat)
		
		return WGS84.a * (WGS84.C0 * Lat + Sin2Lat * (WGS84.C1 + Cos2Lat * (WGS84.C2 + Cos2Lat * WGS84.C3)))
	}
	
	func init1(calPointCount :Int, calPix1 :[Double], calPix2 :[Double], calPix3 :[Double],
	           calProj1 :[Double], calProj2 :[Double], calProj3 :[Double], width :Int, height :Int, ref :GeodeticRef) {
		
		self.calPointCount = calPointCount
		self.calPix1 = calPix1
		self.calPix2 = calPix2
		self.calPix3 = calPix3
		self.calProj1 = calProj1
		self.calProj2 = calProj2
		self.calProj3 = calProj3
		self.height = height
		self.width = width
		self.ref = ref
	}
	
	func init2() {
		var scale = Double2()
		
		//        scale[0].x = (mCalPix2.x - mCalPix1.x) / (mCalProj2.x - mCalProj1.x);
		scale.x = (calPix2![0] - calPix1![0]) / (calProj2![0] - calProj1![0])
		scale.y = (calPix2![1] - calPix1![1]) / (calProj1![1] - calProj2![1])
		matrixUpdate(calOriginPix: Double2(calPix1!), calOriginProj: Double2(calProj1!), calScale: scale, sinRz: 0, cosRz: 1);
	}
	
	func init3()  { //throws Exception
		
		var sinRz = Double(0)
		var cosRz = Double(0)
		
		var centroidProj = Double2()
		var centroidPix  = Double2()
		var scale = Double2()
		
		calcParams3(centroidPix: &centroidPix, centroidProj: &centroidProj, scale: &scale, sinRz: &sinRz, cosRz: &cosRz);
		matrixUpdate(calOriginPix: centroidPix, calOriginProj: centroidProj, calScale: scale, sinRz: sinRz, cosRz: cosRz);
	}
	
	func interpolate(projP :inout Double2, projA :Double2, projB :Double2,
	                 pixP :Double2, pixA :Double2, pixB :Double2) {
		
		let dir :Double2 = Double2(x: pixB.x - pixA.x, y: pixB.y - pixA.y)
		var F = Double()
		if abs(dir.x) > abs(dir.y) {
			F = (pixP.x - pixA.x) / dir.x
		} else {
			F = (pixP.y - pixA.y) / dir.y
		}
		
		projP.x = projA.x + (projB.x - projA.x) * F
		projP.y = projA.y + (projB.y - projA.y) * F
		
	}
	
	//    private static void matrixCreateHorToGlb(Double3x4 aM, double aSinLat, double aCosLat, double aSinLong, double aCosLong, final Double3 aPosGlb) {
	//    aM._11 = -aSinLat * aCosLong;
	//    aM._12 =  aSinLat * aSinLong;
	//    aM._13 =  aCosLat;
	//    aM._21 = -aSinLong;
	//    aM._22 = -aCosLong;
	//    aM._31 =  aCosLat *  aCosLong;
	//    aM._32 =  aCosLat * -aSinLong;
	//    aM._33 =  aSinLat;
	//    aM._23 = 0.0;
	//    aM.setPos(aPosGlb);
	//    }
	//
	//    public static void matrixCreateHorToGlb(Double3x4 aM, final Geo3 aPosGeo) {
	//    // Calculate sines and cosines.
	//    double SinLat = Math.sin(aPosGeo.Lat);
	//    double CosLat = Math.cos(aPosGeo.Lat);
	//    double SinLong = Math.sin(aPosGeo.Long);
	//    double CosLong = Math.cos(aPosGeo.Long);
	//
	//    // Transform position from geographic space to global space.
	//    Double3 PosGlb = geoToGlobal(aPosGeo, null);
	//
	//    // Create matrix.
	//    matrixCreateHorToGlb(aM, SinLat, CosLat, SinLong, CosLong, PosGlb);
	//    }
	
	func matrixPixToProj(pix :Double2, matrix :CalibrationMatrix) -> Double2 {
		let x = pix.x + matrix.T1!.x
		let y = pix.y + matrix.T1!.y
		
		var res = Double2()
		res.x = x * matrix._11! + y * matrix._21! + matrix.T2!.x
		res.y = x * matrix._12! + y * matrix._22! + matrix.T2!.y
		
		return res;
	}
	
	func matrixProjToPix(Proj :Double2, Matrix :CalibrationMatrix) -> Double2 {
		let x = Proj.x + Matrix.T1!.x
		let y = Proj.y + Matrix.T1!.y
		
		var res = Double2()
		res.x = x * Matrix._11! + y * Matrix._21! + Matrix.T2!.x
		res.y = x * Matrix._12! + y * Matrix._22! + Matrix.T2!.y
		
		return res
	}
	
	func matrixUpdate(calOriginPix :Double2, calOriginProj :Double2, calScale :Double2, sinRz :Double, cosRz :Double) {
		self.calScale = calScale
		self.calRz = atan2(sinRz, cosRz)
		
		matrix._11 = calScale.x * cosRz
		matrix._12 = calScale.y * sinRz
		matrix._21 = calScale.x * sinRz
		matrix._22 = -calScale.y * cosRz
		matrix.T2 = calOriginPix
		matrix.T1 = Double2(x: -calOriginProj.x, y: -calOriginProj.y)
		
		matrixInv._11 = cosRz / calScale.x
		matrixInv._12 = sinRz / calScale.x
		matrixInv._21 = sinRz / calScale.y
		matrixInv._22 = -cosRz / calScale.y
		matrixInv.T2 = calOriginProj
		matrixInv.T1 = Double2(x: -calOriginPix.x, y: -calOriginPix.y)
	}
	
	func mercatorToGeoRad(src :Double2, srcRef :GeodeticRef) -> Geo2 {
		let WGS84 = WGS84Ellipsoid.ellipsoid()
		
		let t :Double = exp(-src.y / WGS84.a)
		let Ksi :Double = Double.pi / 2 - 2 * atan(t)
		
		var dst = Geo2()
		
		let Sin2KSi :Double = sin(2 * Ksi)
		let Cos2Ksi :Double = cos(2 * Ksi)
		let GeodeticLat = Ksi + Sin2KSi * (WGS84.A4 + Cos2Ksi * (WGS84.A5 + Cos2Ksi * (WGS84.A6 + WGS84.A7 * Cos2Ksi)))
		dst.lat = GeodeticLat
		// long0 = 0 for Mercator
		dst.lon = src.x / WGS84.a + srcRef.long0;
		//        while (dst.x < -Math.PI)
		//         dst.x += (2 * Math.PI);
		//        while (dst.x >= Math.PI)
		//         dst.x -= (2 * Math.PI);
		return dst
	}
	
	func mirrorPix(pix :inout [Double2], centroidPix :inout Double2, mirrorPix :Double2, margin :Double) -> Bool {
		pix[0].x = Double(2) * mirrorPix.x - calPix1![0]
		pix[1].x = Double(2) * mirrorPix.x - calPix2![0]
		pix[2].x = Double(2) * mirrorPix.x - calPix3![0]
		centroidPix.x = (1.0/3.0) * (pix[0].x + pix[1].x + pix[2].x)
		if abs(centroidPix.x) < margin {
			return false
		}
		pix[0].y = 2 * mirrorPix.y - calPix1![1]
		pix[1].y = 2 * mirrorPix.y - calPix2![1]
		pix[2].y = 2 * mirrorPix.y - calPix3![1]
		centroidPix.y = (1.0/3.0) * (pix[0].y + pix[1].y + pix[2].y)
		return abs(centroidPix.y) >= margin
	}
	
	func mirrorProj(proj :inout [Double2], mirrorProj :Double2) {
		proj[0].x = 2 * mirrorProj.x - calProj1![0]
		proj[0].y = 2 * mirrorProj.y - calProj1![1]
		proj[1].x = 2 * mirrorProj.x - calProj2![0]
		proj[1].y = 2 * mirrorProj.y - calProj2![1]
		proj[2].x = 2 * mirrorProj.x - calProj3![0]
		proj[2].y = 2 * mirrorProj.y - calProj3![1]
	}
	
	func rectifyingLatToGeodeticLat(_ mu :Double) -> Double {
		let WGS84 = WGS84Ellipsoid.ellipsoid()
		
		let sin2Mu :Double = sin(2 * mu)
		let cos2Mu :Double = cos(2 * mu)
		return mu + sin2Mu * (WGS84.B0 + cos2Mu * (WGS84.B1 + cos2Mu * (WGS84.B2 + WGS84.B3 * cos2Mu)))
	}
	
	func transverseMercatorToGeoRad(src :Double2, srcRef :GeodeticRef) -> Geo2 {
		let WGS84 = WGS84Ellipsoid.ellipsoid()
		
		let M = srcRef.M0 + src.y / srcRef.k0
		let Mu = M * WGS84.D0
		let Lat1 = rectifyingLatToGeodeticLat(Mu)
		let SinLat1 = sin(Lat1)
		let CosLat1 = cos(Lat1)
		let TanLat1 = SinLat1 / CosLat1
		let SqrSinLat1 = SinLat1 * SinLat1
		let SqrCosLat1 = CosLat1 * CosLat1
		let C1 = WGS84.e1Sqr * SqrCosLat1
		let T1 = TanLat1 * TanLat1
		let C = 1 - WGS84.eSqr * SqrSinLat1
		let SqrtC = sqrt(C)
		let N1 = WGS84.a / SqrtC
		let R1 = WGS84.a * (1.0 - WGS84.eSqr) / (C * SqrtC)
		let D = src.x / (N1 * srcRef.k0)
		let DSqr = D * D
		
		var res = Geo2()
		res.lat = Lat1 - (N1 * TanLat1 / R1) * (0.5 + ((-1.0/24.0) * (5.0 + 3.0 * T1 + 10.0 * C1 - 4.0 * C1 * C1 - 9.0 * WGS84.e1Sqr) +
			(1.0/720.0) * (61.0 + 90.0 * T1 + 298.0 * C1 + 45.0 * T1 * T1 - 252.0 * WGS84.e1Sqr - 3.0 * C1 * C1) * DSqr) * DSqr) * DSqr
		
		res.lon = srcRef.long0 + (1.0 + ((-1.0/6.0) * (1.0 + 2.0 * T1 + C1) + (1.0/120.0) *
			(5.0 - 2.0 * C1 + 28.0 * T1 - 3.0 * C1 * C1 + 8 * WGS84.e1Sqr + 24.0 * T1 * T1) * DSqr) * DSqr) * D / CosLat1
		
		return res
	}
	
	//    // This is a special matrix to transform pixels to projection coordinates and back. To get an accurate transformation
	//    // using three-point calibration, it is important to move the point of rotation inside the boundaries of the chart. To
	//    // achieve this, a translation is performed prior to rotation.
	class CalibrationMatrix {
		var _11 :Double?  // Rotation and scaling
		var _12 :Double?  // Rotation and scaling
		var _21 :Double?  // Rotation and scaling
		var _22 :Double?  // Rotation and scaling
		var T2 :Double2?  // Translation after rotation
		var T1 :Double2?  // Translation before rotation
	}
	//
	//    private class BndBoxAAD {
	//        public Double3 Max = new Double3()
	//        public Double3 Min = new Double3()
	//
	//        public void setMax(Double3 aMax) {
	//        Max.x = aMax.x;
	//        Max.y = aMax.y;
	//        Max.z = aMax.z;
	//        }
	//
	//        public void setMin(Double3 aMin) {
	//        Min.x = aMin.x;
	//        Min.y = aMin.y;
	//        Min.z = aMin.z;
	//        }
	//    }
	//
	//    private class Sphere {
	//
	//        public Double3 Center;    // Center of the sphere.
	//        public double Radius;    // Radius of the sphere.
	//        public double RadiusSqr; // Squared radius of the sphere.
	//
	//        Sphere() {
	//        Center = new Double3();
	//        Radius = 0;
	//        RadiusSqr = 0;
	//        }
	//        //CSphere(const CSphere &aSphere);
	//
	//        Sphere(final Double3 aCenter, double aRadius) {
	//        Center = aCenter;
	//        Radius = aRadius;
	//        RadiusSqr = aRadius * aRadius;
	//        }
	//
	//        // Sets the center and radius to the smallest sphere through two unique points on the sphere's surface. If
	//        // aForceContainPoints is true, the function corrects the radius for rounding errors to contain all points.
	//        //  CSphere(const double3&aP1, const double3&aP2, bool aForceContainPoints);
	//
	//        // Sets the center and radius to the smallest sphere through three unique points on the sphere's surface. The points
	//        // should not be colinear. If aForceContainPoints is true, the function corrects the radius for rounding errors to
	//        // contain all points.
	//        Sphere(final Double3 aP1, final Double3 aP2, final Double3 aP3, boolean aForceContainPoints) {
	//        Center = new Double3();
	//        Radius = 0;
	//        RadiusSqr = 0;
	//        setByPoints(aP1, aP2, aP3, aForceContainPoints);
	//        }
	//
	//        // Sets the center and radius to the smallest sphere through four unique points on the sphere's surface. The points
	//        // should not be coplanar. If aForceContainPoints is true, the function corrects the radius for rounding errors to
	//        // contain all points.
	//        // CSphere(const double3&aP1, const double3&aP2, const double3&aP3, const double3&aP4, bool aForceContainPoints);
	//
	//        // Returns true if the sphere contains point aP.
	//        //        bool Contains(const double3&aP)
	//
	//        //        const
	//
	//        //        {
	//        //            return DistSqr(mCenter, aP) <= mRadiusSqr;
	//        //        }
	//
	//        void assign(Sphere aSphere) {
	//        Center.x = aSphere.Center.x;
	//        Center.y = aSphere.Center.y;
	//        Center.z = aSphere.Center.z;
	//        Radius = aSphere.Radius;
	//        RadiusSqr = aSphere.RadiusSqr;
	//        }
	//
	//        boolean contains(final Double3 aP, double aMargin) {
	//        return StMath.distSqr(Center, aP) <= StMath.sqr(Radius + aMargin);
	//        }
	//
	//        // Sets the center and radius to the smallest sphere through two unique points on the sphere's surface. If
	//        // aForceContainPoints is true, the function corrects the radius for rounding errors to contain all points.
	//        void setByPoints(final Double3 aP1, final Double3 aP2, boolean aForceContainPoints) {
	//        RadiusSqr = StMath.calcSphere(Center, aP1, aP2);
	//
	//        if (aForceContainPoints) {
	//        // mRadiusSqr was calculated using aP1.
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP2));
	//        }
	//        Radius = Math.sqrt(RadiusSqr);
	//        }
	//
	//
	//        // Sets the center and radius to the smallest sphere through three unique points on the sphere's surface. The points
	//        // should not be colinear. If aForceContainPoints is true, the function corrects the radius for rounding errors to
	//        // contain all points.
	//        void setByPoints(final Double3 aP1, final Double3 aP2, final Double3 aP3, boolean aForceContainPoints) {
	//        RadiusSqr = StMath.calcSphere(Center, aP1, aP2, aP3);
	//
	//        if (aForceContainPoints) {
	//        // mRadiusSqr was calculated using aP1.
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP2));
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP3));
	//        }
	//        Radius = Math.sqrt(RadiusSqr);
	//        }
	//
	//
	//        // Sets the center and radius to the smallest sphere through four unique points on the sphere's surface. The points
	//        // should not be coplanar. If aForceContainPoints is true, the function corrects the radius for rounding errors to
	//        // contain all points.
	//        void setByPoints(final Double3 aP1, final Double3 aP2, final Double3 aP3, final Double3 aP4, boolean aForceContainPoints) {
	//        RadiusSqr = StMath.calcSphere(Center, aP1, aP2, aP3, aP4);
	//
	//        if (aForceContainPoints) {
	//        // mRadiusSqr was calculated using aP1.
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP2));
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP3));
	//        RadiusSqr = Math.max(RadiusSqr, StMath.distSqr(Center, aP4));
	//        }
	//        Radius = Math.sqrt(RadiusSqr);
	//        }
	//
	//
	//        //        void SetCenter(const double3&aCenter) {
	//        //            mCenter = aCenter;
	//        //        }
	//
	//        //        void SetRadius(double aRadius) {
	//        //            mRadius = aRadius;
	//        //            mRadiusSqr = aRadius * aRadius;
	//        //        }
	//
	//        void setRadiusSqr(double aRadiusSqr) {
	//        Radius = Math.sqrt(aRadiusSqr);
	//        RadiusSqr = aRadiusSqr;
	//        }
	//
	//        //      CSphere&operator=(const CSphere&aSphere);
	//    }
	//
	//    private class BoundingSphere {
	//
	//        private int mCount;
	//        private double mMargin;
	//        private double mMarginSqr;
	//        private Double3 mPoints[];
	//        private Double3 mPointsCenter[];
	//
	//        BoundingSphere(int aCapacity) {
	//        mCount = 0;
	//        mMargin = 0;
	//        mMarginSqr = 0;
	//        mPoints = new Double3[aCapacity];
	//        mPointsCenter = new Double3[aCapacity];
	//        }
	//
	//        void addPoint(final Double3 aP) {
	//        mPoints[mCount] = aP;
	//        mCount++;
	//        }
	//
	//        double calc(Double3 aCenter) {
	//        if (mCount == 0) {
	//        aCenter.x = aCenter.y = aCenter.z = 0;
	//        return 0.0f;
	//        }
	//
	//        // Determine the bounding box.
	//        BndBoxAAD BndBox = new BndBoxAAD();
	//
	//        BndBox.setMin(mPoints[0]);
	//        BndBox.setMax(mPoints[0]);
	//        for (int i = 1; i < mCount; i++) {
	//        Double3 P = mPoints[i];
	//        if (P.x < BndBox.Min.x)
	//        BndBox.Min.x = P.x;
	//        else if (P.x > BndBox.Max.x)
	//        BndBox.Max.x = P.x;
	//        if (P.y < BndBox.Min.y)
	//        BndBox.Min.y = P.y;
	//        else if (P.y > BndBox.Max.y)
	//        BndBox.Max.y = P.y;
	//        if (P.z < BndBox.Min.z)
	//        BndBox.Min.z = P.z;
	//        else if (P.z > BndBox.Max.z)
	//        BndBox.Max.z = P.z;
	//        }
	//
	//        // Determine center and radius of bounding box and the margins used in some tests.
	//        Double3 BoundsCenter = new Double3(
	//        0.5 * (BndBox.Min.x + BndBox.Max.x),
	//        0.5 * (BndBox.Min.y + BndBox.Max.y),
	//        0.5 * (BndBox.Min.z + BndBox.Max.z));
	//        double BoundsRadius = StMath.dist(BoundsCenter, BndBox.Max);
	//        mMargin = 1e-8 * BoundsRadius;
	//        mMarginSqr = StMath.sqr(mMargin);
	//
	//        // Copy all points and move the origin to the center of the bounding box. This optimizes floating-point precision.
	//        for (int i = mCount - 1; i >= 0; i--) {
	//        mPointsCenter[i] = new Double3();
	//        mPointsCenter[i].subtract(mPoints[i], BoundsCenter);
	//        }
	//
	//        // Randomize the ordering of the points. This helps to prevent the worst case ordering of points which makes the
	//        // algorithm very slow.
	//        Random random = new Random();
	//        for (int i = mCount - 1; i > 0; i--) {
	//        int j = random.nextInt(mCount);
	//        if (i != j) {
	//        Double3 temp = mPointsCenter[i];
	//        mPointsCenter[i] = mPointsCenter[j];
	//        mPointsCenter[j] = temp;
	//        }
	//        }
	//
	//        // The support set contains up to 4 points. The support set defines the points that define the minimum bounding sphere.
	//        final Double3 Supports[] = new Double3[4];
	//        int SupportCount = 0;
	//
	//        // Statistics.
	//        int TotalIterations = 0;
	//        int TotalRestarts = 0;
	//
	//        // Handle the first point.
	//        Double3 P = mPointsCenter[0];
	//        Supports[SupportCount++] = P;
	//        Sphere MinSphere = new Sphere(P, 0.0);
	//        int Index = 1;
	//
	//        // Iterate all points to check if they are inside the current bounding sphere. If not, adjust the bounding sphere.
	//        while (Index < mCount) {
	//        TotalIterations++;
	//
	//        // Check if P is equal to part of the support set. If it is, there is no need to the support set contain the point processed.
	//        P = mPointsCenter[Index];
	//        if (!inSupports(Supports, SupportCount, P)) {
	//        // Check if the point is outside the current minimum sphere.
	//        if (!MinSphere.contains(P, mMargin)) {
	//        // Update the minimum sphere to contain the point.
	//        Sphere NewSphere = new Sphere();
	//        SupportCount = update(NewSphere, Supports, SupportCount, P);
	//
	//        // Theoretically, NewSphere always has a greater radius than MinSphere. However, due to rounding errors it might
	//        // be slightly smaller. In that case use the new sphere with the old, larger radius.
	//        if (NewSphere.RadiusSqr > MinSphere.RadiusSqr)
	//        MinSphere.assign(NewSphere);
	//        else {
	//        MinSphere.Center.x = NewSphere.Center.x;
	//        MinSphere.Center.y = NewSphere.Center.y;
	//        MinSphere.Center.z = NewSphere.Center.z;
	//        }
	//
	//        // Restart the algorithm.
	//        Index = 0;
	//        TotalRestarts++;
	//        continue;
	//        }
	//        }
	//        Index++;
	//        }
	//
	//        // Adjust the center by BoundsRadius and calculate the radius including mMargin. This guarantees that all points are
	//        // within the bounding sphere.
	//        aCenter.add(MinSphere.Center, BoundsCenter);
	//        return Math.sqrt(MinSphere.RadiusSqr) + mMargin;
	//        }
	//
	//        private boolean inSupports(final Double3[] aSupports, int aSupportCount, final Double3 aP) {
	//        for (int i = aSupportCount - 1; i >= 0; i--) {
	//        if (StMath.distSqr(aSupports[i], aP) <= mMarginSqr)
	//        return true;
	//        }
	//        return false;
	//        }
	//
	//        private boolean isCoplanar(final Double3 aP1, final Double3 aP2, final Double3 aP3, final Double3 aP4) {
	//        Double4 PlaneDef = new Double4();
	//        StMath.initPlaneDef(PlaneDef, aP1, aP2, aP3);
	//        return Math.abs(StMath.distToPlane(PlaneDef, aP4)) < mMargin;
	//        }
	//
	//        private int update(Sphere aSphere, final Double3[] aSupports, int aSupportCount, final Double3 aP) {
	//
	//        int SupportCount = aSupportCount;
	//
	//        switch (aSupportCount) {
	//        case 4:
	//        aSphere.setByPoints(aSupports[0], aP, true);
	//        if (aSphere.contains(aSupports[1], mMargin) && aSphere.contains(aSupports[2], mMargin) && aSphere.contains(aSupports[3], mMargin)) {
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // If the sphere supported by (p1, aP) contains p0, p2 and p3, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[1], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin) && aSphere.contains(aSupports[2], mMargin) && aSphere.contains(aSupports[3], mMargin)) {
	//        aSupports[0] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // If the sphere supported by (p2, aP) contains p0, p1 and p3, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[2], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin) && aSphere.contains(aSupports[1], mMargin) && aSphere.contains(aSupports[3], mMargin)) {
	//        aSupports[0] = aSupports[2];
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // If the sphere supported by (p3, aP) contains p0, p1 and p2, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[3], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin) && aSphere.contains(aSupports[1], mMargin) && aSphere.contains(aSupports[2], mMargin)) {
	//        aSupports[0] = aSupports[3];
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // There is no sphere defined by two supports that is a valid minimum sphere. Test all combinations of
	//        // spheres defined by three or four points and if valid ones are found, return the smallest. It is crucial
	//        // to test all combinations because the first one is not neccesarily the smallest one.
	//        aSphere.setRadiusSqr(Double.MAX_VALUE);
	//        int Index = -1;
	//
	//        // If the sphere supported by (p0, p1, aP) contains p2 and p3, this is the minimum sphere.
	//        Sphere sphere = new Sphere(aSupports[0], aSupports[1], aP, true);
	//        if (sphere.contains(aSupports[2], mMargin) && sphere.contains(aSupports[3], mMargin)) {
	//        Index = 0;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p0, p2, aP) contains p1 and p3, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[0], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[1], mMargin) && sphere.contains(aSupports[3], mMargin)) {
	//        Index = 1;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p0, p3, aP) contains p1 and p2, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[0], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[1], mMargin) && sphere.contains(aSupports[2], mMargin)) {
	//        Index = 2;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p1, p2, aP) contains p0 and p3, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[1], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[0], mMargin) && sphere.contains(aSupports[3], mMargin)) {
	//        Index = 3;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p1, p3, aP) contains p0 and p2, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[1], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[0], mMargin) && sphere.contains(aSupports[2], mMargin)) {
	//        Index = 4;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p2, p3, aP) contains p0 and p1, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[2], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[0], mMargin) && sphere.contains(aSupports[1], mMargin)) {
	//        Index = 5;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p0, p1, p2, aP) contains p3, this is the minimum sphere.
	//        if (!isCoplanar(aSupports[0], aSupports[1], aSupports[2], aP)) {
	//        sphere.setByPoints(aSupports[0], aSupports[1], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[3], mMargin)) {
	//        Index = 6;
	//        aSphere.assign(sphere);
	//        }
	//        }
	//
	//        // If the sphere supported by (p0, p1, p3, aP) contains p2, this is the minimum sphere.
	//        if (!isCoplanar(aSupports[0], aSupports[1], aSupports[3], aP)) {
	//        sphere.setByPoints(aSupports[0], aSupports[1], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[2], mMargin)) {
	//        Index = 7;
	//        aSphere.assign(sphere);
	//        }
	//        }
	//
	//        // If the sphere supported by (p0, p2, p3, aP) contains p1, this is the minimum sphere.
	//        if (!isCoplanar(aSupports[0], aSupports[2], aSupports[3], aP)) {
	//        sphere.setByPoints(aSupports[0], aSupports[2], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[1], mMargin)) {
	//        Index = 8;
	//        aSphere.assign(sphere);
	//        }
	//        }
	//
	//        // The sphere supported by (p1, p2, p3, aP) is the minimum sphere.
	//        if (!isCoplanar(aSupports[1], aSupports[2], aSupports[3], aP)) {
	//        sphere.setByPoints(aSupports[1], aSupports[2], aSupports[3], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[0], mMargin)) {
	//        Index = 9;
	//        aSphere.assign(sphere);
	//        }
	//        }
	//
	//        // Update the supports.
	//        switch (Index) {
	//        case 0:
	//        aSupports[2] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 1:
	//        aSupports[1] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 2:
	//        aSupports[1] = aSupports[3];
	//        aSupports[2] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 3:
	//        aSupports[0] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 4:
	//        aSupports[0] = aSupports[3];
	//        aSupports[2] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 5:
	//        aSupports[0] = aSupports[3];
	//        aSupports[1] = aP;
	//        SupportCount = 3;
	//        break;
	//        case 6:
	//        aSupports[3] = aP;
	//        break;
	//        case 7:
	//        aSupports[2] = aP;
	//        break;
	//        case 8:
	//        aSupports[1] = aP;
	//        break;
	//        case 9:
	//        aSupports[0] = aP;
	//        break;
	//        //default:
	//        //                                            throw CException(L"Update(case 4): no sphere found");
	//        }
	//        }
	//        }
	//        }
	//        }
	//        break;
	//        case 3:
	//        // If the sphere supported by (p0, aP) contains p1 and p2, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[0], aP, true);
	//        if (aSphere.contains(aSupports[1], mMargin) && aSphere.contains(aSupports[2], mMargin)) {
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // If the sphere supported by (p1, aP) contains p0 and p2, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[1], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin) && aSphere.contains(aSupports[2], mMargin)) {
	//        aSupports[0] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // If the sphere supported by (p2, aP) contains p0 and p1, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[2], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin) && aSphere.contains(aSupports[1], mMargin)) {
	//        aSupports[0] = aSupports[2];
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        } else {
	//        // There is no sphere defined by two supports that is a valid minimum circle. Test all combinations of
	//        // spheres defined by three or four points and if valid ones are found, return the smallest. It is crucial
	//        // to test all combinations because the first one is not neccesarily the smallest one.
	//        aSphere.RadiusSqr = Double.MAX_VALUE;
	//        int Index = -1;
	//
	//        // If the sphere supported by (p0, p1, aP) contains p2, this is the minimum sphere.
	//        Sphere sphere = new Sphere(aSupports[0], aSupports[1], aP, true);
	//        if (sphere.contains(aSupports[2], mMargin)) {
	//        Index = 0;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p0, p2, aP) contains p1, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[0], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[1], mMargin)) {
	//        Index = 1;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // If the sphere supported by (p1, p2, aP) contains p0, this is the minimum sphere.
	//        sphere.setByPoints(aSupports[1], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr && sphere.contains(aSupports[0], mMargin)) {
	//        Index = 2;
	//        aSphere.assign(sphere);
	//        }
	//
	//        // The sphere supported by (p0, p1, p2, aP) is the minimum sphere.
	//        if (!isCoplanar(aSupports[0], aSupports[1], aSupports[2], aP)) {
	//        sphere.setByPoints(aSupports[0], aSupports[1], aSupports[2], aP, true);
	//        if (sphere.RadiusSqr < aSphere.RadiusSqr) {
	//        Index = 3;
	//        aSphere.assign(sphere);
	//        }
	//        }
	//
	//        switch (Index) {
	//        case 0:
	//        aSupports[2] = aP;
	//        break;
	//        case 1:
	//        aSupports[1] = aP;
	//        break;
	//        case 2:
	//        aSupports[0] = aP;
	//        break;
	//        case 3:
	//        aSupports[3] = aP;
	//        SupportCount = 4;
	//        break;
	//        //default: throw CException(L"Update(case 3): no sphere found");
	//        }
	//        }
	//        }
	//        }
	//        break;
	//        case 2: {
	//        // If the sphere supported by (p0, aP) contains p1, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[0], aP, true);
	//        if (aSphere.contains(aSupports[1], mMargin)) {
	//        aSupports[1] = aP;
	//        } else {
	//        // If the sphere supported by (p1, aP) contains p0, this is the minimum sphere.
	//        aSphere.setByPoints(aSupports[1], aP, true);
	//        if (aSphere.contains(aSupports[0], mMargin)) {
	//        aSupports[0] = aP;
	//        } else {
	//        // The sphere supported by (p0, p1, aP) is the minimum sphere.
	//        aSphere.setByPoints(aSupports[0], aSupports[1], aP, true);
	//        aSupports[2] = aP;
	//        SupportCount = 3;
	//        }
	//        }
	//        break;
	//        }
	//        default: // Second point added.
	//        aSphere.setByPoints(aSupports[0], aP, true);
	//        aSupports[1] = aP;
	//        SupportCount = 2;
	//        }
	//
	//        return SupportCount;
	//        }
	//    }
	
}
