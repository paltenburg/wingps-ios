//
//  NmeaTcpConnectionController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 22/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class NmeaTcpConnectionController: UIViewController {
	
	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var hostTextField: UITextField!
	@IBOutlet weak var portTextField: UITextField!
	@IBOutlet weak var nmeaTextView: UITextView!
	@IBOutlet weak var connectButton: UIButton!
	
//	let nmeaTcpConnection = NmeaTcpConnection()
	var messages: [String] = []
	
//	var connectionEnabled : Bool = false
	
	@IBAction func connectButtonAction(_ sender: Any) {
		if NmeaTcpConnection.service.connectionEnabled == false {
			UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(true, forKey: Resources.SETTINGS_TCP_CONNECTION_ENABLED)

			let host = hostTextField.text
			let port :Int = Int(portTextField.text ?? "") ?? 5101

			UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(host, forKey: Resources.SETTINGS_TCP_HOST)
			UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(port, forKey: Resources.SETTINGS_TCP_PORT)

//			NmeaTcpConnection.service.startConnection(host: host, port: port)
			ViewController.instance?.updateSettings()
			
			connectButton.setTitle("Stop", for: .normal)
			
		} else {
			UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(false, forKey: Resources.SETTINGS_TCP_CONNECTION_ENABLED)

			ViewController.instance?.updateSettings()

//			NmeaTcpConnection.service.stopConnection()
			connectButton.setTitle("Connect", for: .normal)
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		NmeaTcpConnection.service.delegates.append(self)
		
//		if StUtils.isDebug() {
//			hostTextField.text = "ais.localhost.asia"
//			portTextField.text = "5101"
//		}
		
		if NmeaTcpConnection.service.connectionEnabled {
			connectButton.setTitle("Stop", for: .normal)
		} else {
			connectButton.setTitle("Connect", for: .normal)
		}
		
		hostTextField.text = NmeaTcpConnection.service.host
		portTextField.text = String(NmeaTcpConnection.service.port)

		hostTextField.text = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.string(forKey: Resources.SETTINGS_TCP_HOST) ?? ""
        var port = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.object(forKey: Resources.SETTINGS_TCP_PORT) as? Int ?? NmeaTcpConnection.service.port
		portTextField.text = String(port ?? 0)

		nmeaTextView.text = ""
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		containerView.layer.cornerRadius = 8
		containerView.clipsToBounds = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

	}
	
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height / 2
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		if NmeaTcpConnection.service.connectionEnabled == true {
			NmeaTcpConnection.service.removeFromDelegates(self)
		}
	}
	
	func insertNewMessage(_ message: String){
		let limit = 10000
		var newText = String("\(message)\(nmeaTextView.text ?? "")")
		if(newText.count > limit){
			newText.removeLast(newText.count-limit)
		}
		nmeaTextView.text = newText //// malloc
	}

	@IBAction func backButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToSettings", sender: self)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if (touch?.view == backgroundView && NmeaTcpConnection.service.connectionEnabled == false){
			//performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
			self.dismiss(animated: false, completion: nil)
		}
	}
	
	@IBAction func doneButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
}

//MARK - Message Input Bar
//extension NmeaTcpConnectionController: MessageInputDelegate {
//    func sendWasTapped(message: String) {
//        nmeaTcpConnection.send(message: message)
//    }
//}

extension NmeaTcpConnectionController: NmeaTcpConnectionDelegate {
	func received(message: String) {
		insertNewMessage(message)
		
	}
}
