//
//  ChartManagerController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 14/12/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

struct SectionRowData {
	var cInfo :DKW2ChartManager.DKW2ChartSetInfo?
	var opened = Bool()
	var title = String()
	//	var enabled = Bool()
	var sectionData :[DKW2ChartDisplayInfo]?
    
//    var tableCell :ChartsTableCell?
    var tableCells :[ChartsTableCell]?
    
//    var overviewChartIfAny :Int? = nil // If there is an overview chart, this is the index
    var overviewChartIfAny :Int? = 0 // If there is an overview chart, this is the index
}

class ChartManagerController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    static let appleMapsTitle = "apple_maps".localized
	static let NoaaChartsTitle = "NOAA_charts".localized
    static var fixedCharts = [ChartManagerController.appleMapsTitle]
    
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var chartManagerContainer: UIView!
	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var noChartsInstalledLabel: UILabel!

    @IBOutlet weak var showOverviewChartsSwitchOutlet: UISwitch!
    @IBAction func showOverviewChartsSwitch(_ sender: UISwitch) {
        // TODO adapt code for WinGPS Marine!
        showOverviewCharts = sender.isOn
        
        // save setting:
        let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
        defaultSettings?.set(showOverviewCharts, forKey: Resources.SETTINGS_SHOW_OVERVIEWCHARTS)
        defaultSettings?.synchronize()
                
        // toggle first chart of every section. With the 1800 series this is the overview chart
        for (i, cellData) in tableViewData.enumerated() {
            if let sectionData = cellData.sectionData,
            sectionData.count >= 1 { // make sure it's a chartset and not a fixed option
                
                if let overviewChartIndex = cellData.overviewChartIfAny {
                    
                    let cdi = sectionData[overviewChartIndex]
                    
                    if let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY),
                        let chartGUID = cdi.calibratedChart.dKW2Chart?.tag.guid.toString() {
                        chartVisibilitySettings.set(showOverviewCharts, forKey:chartGUID)
                        chartVisibilitySettings.synchronize()
                    }
                    
                    if let cell = tableView.cellForRow(at: IndexPath(row: overviewChartIndex + 1, section: i)) as? ChartsTableCell,
                        cell.chartEnabledSwitch.isOn != showOverviewCharts {
                        cell.chartEnabledSwitch.setOn(showOverviewCharts, animated: true)
                    }
                    
                    setChartVisible(cdi: cdi, visible: showOverviewCharts)

                }
            }
        }
        DKW2ChartManager.refreshCharts()
    }
    
    var mChartManager : DKW2ChartManager?
	
	var tableViewData = [SectionRowData]()
	
    var showOverviewCharts :Bool = true

    var selectedIndexPath :IndexPath = IndexPath(row: -1, section: 0)

	override func viewDidLoad() {
		super.viewDidLoad()
		chartManagerContainer.layer.cornerRadius = 8
		chartManagerContainer.clipsToBounds = true
		
		tableView.delegate = self
		tableView.dataSource = self
		/*let sectionData1 = [" - Chart1", " - Chart2", " - Chart3"]
		tableViewData = [cellData(opened: false, title: "Set 1", sectionData: sectionData1),
		cellData(opened: false, title: "Set 2", sectionData: sectionData1),
		cellData(opened: false, title: "Set 3", sectionData: sectionData1)]
		*/
		
        let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
        showOverviewCharts = defaultSettings?.object(forKey: Resources.SETTINGS_SHOW_OVERVIEWCHARTS) as? Bool ?? showOverviewCharts
        showOverviewChartsSwitchOutlet.setOn(showOverviewCharts, animated: false)

        switch Resources.appType {
        case .lite, .plus:
            ChartManagerController.fixedCharts = [ChartManagerController.appleMapsTitle, ChartManagerController.NoaaChartsTitle]
        case .dkw1800:
            ChartManagerController.fixedCharts = [ChartManagerController.appleMapsTitle]
        case .friesemeren:
            ChartManagerController.fixedCharts = [ChartManagerController.appleMapsTitle]
        }
        
		// Add apple maps and Noaa Tiles
		for fcTitle in ChartManagerController.fixedCharts {
			tableViewData.append(SectionRowData(cInfo: nil, opened: false, title: fcTitle, sectionData: nil))
		}
		
		mChartManager = DKW2ChartManager.getChartManager()
		
		let chartSets : [DKW2ChartManager.DKW2ChartSetInfo] = mChartManager!.mChartSets
		for chartSet in chartSets {
			//			tableViewData.append(cellData(CSI: chartSet, opened: false, title: chartSet.mName!, enabled: chartSet.visible!, sectionData: chartSet.charts))
            
            // Hardcode overview chart indexes here for 1800 series charts!
            var overviewChartIndexIfAny :Int? = nil
            if let chartName = chartSet.mName,
               chartName.contains("180") || chartName.contains("181"){
                overviewChartIndexIfAny = 0
            }
            
			tableViewData.append(SectionRowData(cInfo: chartSet, opened: false, title: chartSet.mName!, sectionData: chartSet.charts, overviewChartIfAny: overviewChartIndexIfAny))
		}
		
		tableView.tableFooterView = UIView()
		
		if tableViewData.count == 0 {
			tableView.isHidden = true
			noChartsInstalledLabel.isHidden = false
		} else {
			tableView.isHidden = false
			noChartsInstalledLabel.isHidden = true
		}
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return tableViewData.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // Check if this section contains the chart with borders. If so: expand
        for cdi in tableViewData[section].sectionData ?? [DKW2ChartDisplayInfo]() {
            if cdi === mChartManager?.getChartBorderVisible() {
                tableViewData[section].opened = true
                break
            }
        }
        
		if tableViewData[section].sectionData != nil,
			tableViewData[section].opened {
			return tableViewData[section].sectionData!.count + 1
		} else {
			return 1
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ChartsTableCell else { return UITableViewCell() }
        
        cell.expandedView?.isHidden = true
        cell.expandButtonImageView.isHidden = false
        cell.minimizeButtonImageView.isHidden = true
        
        cell.controller = self

        
		if indexPath.row == 0 { // chartset
			
//			cell.fileNameLabelLeadingConstraint.constant = 10
//			cell.enabledSwitchLeadingConstraint.constant = 10
            cell.leadingSpacingWidthConstraint.constant = 10

			cell.centerChartButton.isEnabled = false
			cell.centerChartButton.isHidden = true
			
			var cellData = tableViewData[indexPath.section]
			
			let label = cell.fileNameLabel!
			//let label : UILabel = (cell.accessoryView) as! UILabel
			label.text = cellData.title
			label.font = UIFont.boldSystemFont(ofSize: 16.0)
			
			// TODO read state from settings
			//			cell.chartEnabledSwitch!.setOn(tableViewData[indexPath.section].enabled, animated: true)
//			cell.chartEnabledSwitch!.setOn(cellData.cInfo.visible ?? true, animated: false)
			
			var chartSetString = ""
			if let cellData_cInfo = cellData.cInfo {
				
				chartSetString = "module-\(cellData_cInfo.productID ?? -1)-\(cellData_cInfo.moduleBitIndex ?? -1)"
				var chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)
				if chartVisibilitySettings?.object(forKey: chartSetString) != nil {
					cell.chartEnabledSwitch!.setOn(chartVisibilitySettings?.object(forKey: chartSetString) as? Bool ?? true, animated: false)
				} else {
					cell.chartEnabledSwitch!.setOn(true, animated: false)
				}
				
                if tableViewData[indexPath.section].opened {
                     cell.expandButtonImageView.isHidden = true
                     cell.minimizeButtonImageView.isHidden = false
                }
                
			} else {	// in case it's one of the fixed options:
				
				chartSetString = "module-\(cellData.title)"
				let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
					cell.chartEnabledSwitch!.setOn(defaultSettings?.object(forKey: chartSetString) as? Bool ??
						// default settings: true for apple maps, false for noaa
						(((cellData.title == ChartManagerController.appleMapsTitle) ? ViewController.instance?.mSettingsShowAppleMaps :
							(cellData.title == ChartManagerController.NoaaChartsTitle ? ViewController.instance?.mSettingsShowNoaaCharts : false ))!)
						, animated: false)
                
                cell.expandButtonImageView.isHidden = true
                cell.minimizeButtonImageView.isHidden = true
			}
	
            cell.cellContainerView.backgroundColor = ColorCompatibility.systemBackground

		} else { // individual chart
			
//			cell.fileNameLabelLeadingConstraint.constant = 10
//			cell.enabledSwitchLeadingConstraint.constant = 25
            cell.leadingSpacingWidthConstraint.constant = 25

			cell.centerChartButton.isEnabled = true
			cell.centerChartButton.isHidden = false
			
			// set center-chart-button
			
			let dataIndex = indexPath.row - 1
			
			guard let cInfo = tableViewData[indexPath.section].sectionData?[dataIndex] else { return cell }
			
            let chartTag = mChartManager?.mCharts.firstIndex(where: { (item) -> Bool in item === cInfo })
			cell.centerChartButton.tag = chartTag ?? cell.centerChartButton.tag
            cell.centerChartButton.addTarget(self, action: #selector(chartCenterButtonAction), for: .touchUpInside)
            
            cell.showBordersBottonOutlet.tag = chartTag ?? cell.showBordersBottonOutlet.tag
            cell.showBordersBottonOutlet.addTarget(self, action: #selector(showBordersAction), for: .touchUpInside)
            
            // Set title of show border button
            if let chartManager = mChartManager,
                chartManager.getChartBorderVisible() === cInfo {
                
//                mChartManager?.setChartBorderVisible(aChart: cInfo.calibratedChart.dKW2Chart!.tag.guid)
                //                DKW2ChartManager.refreshCharts()
                
                //                cell.showBordersBottonOutlet.titleLabel?.text = "button_hide_chart_border".localized
                
                cell.showBordersBottonOutlet.setTitle("button_hide_chart_border".localized, for: .normal)
            } else {
                cell.showBordersBottonOutlet.setTitle("button_show_chart_border".localized, for: .normal)
            }
            
			let label = cell.fileNameLabel!
            
			//let label : UILabel = (cell.accessoryView) as! UILabel
			
//			label.text = cInfo.calibratedChart.dKW2Chart?.fileName?.lastPathComponent

			var dkw2Chart = cInfo.calibratedChart.dKW2Chart!
			var t :String = ""
			if dkw2Chart.tag.chartNumber.count > 0 {
				t = dkw2Chart.tag.chartNumber + " - "
			}
			t += dkw2Chart.tag.chartName
			
            label.text = t
            label.font = UIFont.systemFont(ofSize: 12.0)
            
            let enabledSwitch = cell.chartEnabledSwitch!
            
            if let chartGUID = cInfo.calibratedChart.dKW2Chart?.tag.guid.toString(),
                UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)?.object(forKey: chartGUID) != nil {
                enabledSwitch.setOn(
                    UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)?.object(forKey: chartGUID) as? Bool ?? true
                    , animated: false)
            } else {
                enabledSwitch.setOn(true, animated: false)
            }
            
            var expandCell = false
            if selectedIndexPath == indexPath { expandCell = true }
            if let chartManager = mChartManager,
                chartManager.getChartBorderVisible() === cInfo {  expandCell = true }
            if expandCell {
                cell.expandedView?.isHidden = false
                cell.expandButtonImageView.isHidden = true
                cell.minimizeButtonImageView.isHidden = false
            }
            
            cell.cellContainerView.backgroundColor = ColorCompatibility.secondarySystemBackground

		}
		return cell
		
	}
	
	@IBAction func closeButton(_ sender: Any) {
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
		self.dismiss(animated: true, completion: nil)
	}
	
	@objc func chartCenterButtonAction(sender: UIButton!) {
        
		print("Button tapped with mCharts index : \(sender.tag)")
        
        if let chart :DKW2ChartDisplayInfo = mChartManager?.mCharts[sender.tag] {
			
            // Enable chart and its chartset
            if let indexPath = getIndexPathForCDI(cdi: chart),
                let sectionCell = tableView.cellForRow(at: IndexPath(row: 0, section: indexPath.section)) as? ChartsTableCell,
                let cell = tableView.cellForRow(at: indexPath) as? ChartsTableCell {
                
                sectionCell.chartEnabledSwitch.setOn(showOverviewCharts, animated: true)
                cell.chartEnabledSwitch.setOn(showOverviewCharts, animated: true)
                
                setEnabled(sectionCell, enabled: true)
                setChartVisible(cdi: chart, visible: true)
                
            }
            
            
			let cal = chart.calibratedChart.getCalibration()
			
			// use chart center in chart pixels, then make rectangle with the size of mapview in points.
//			let topLeft = Double2(x: chartCenter.x - Double(viewSizePts.width) / 2, y: chartCenter.y - Double(viewSizePts.height) / 2 )
//			let bottomRight = Double2(x: chartCenter.x + Double(viewSizePts.width) / 2, y: chartCenter.y + Double(viewSizePts.height) / 2 )
//			let topLeftGeoDeg = radToDeg(cal.pixToGeoRadWGS84(topLeft))
//			let bottomRightGeoDeg = radToDeg(cal.pixToGeoRadWGS84(bottomRight))

			let centerLocation = CLLocationCoordinate2D(radToDeg(cal.pixToGeoRadWGS84(cal.getPixCenter())))
			let topLeftGeoDeg = radToDeg(cal.pixToGeoRadWGS84(cal.getPixTopLeft()))
			let bottomRightGeoDeg = radToDeg(cal.pixToGeoRadWGS84(cal.getPixBottomRight()))
			
			let region = MKCoordinateRegion(center: centerLocation,
													  span: MKCoordinateSpan(
														latitudeDelta: topLeftGeoDeg.lat - bottomRightGeoDeg.lat,
														longitudeDelta: abs(bottomRightGeoDeg.lon - topLeftGeoDeg.lon)))
			
//			self.dismiss(animated: true, completion: {
				ViewController.instance!.mapView.setRegion(region, animated: true)
//			})
			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
		}
    }
    
    func setChartVisible(cdi: DKW2ChartDisplayInfo, visible: Bool){
        cdi.setInViewer(visible)
        if let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY),
            let chartGUID = cdi.calibratedChart.dKW2Chart?.tag.guid.toString() {
            chartVisibilitySettings.set(visible, forKey:chartGUID)
            chartVisibilitySettings.synchronize()
        }
        
        
        if visible,
            !showOverviewCharts,
            let cellData = getCellDataForCDI(cdi: cdi),
            let overviewChartIndex = cellData.overviewChartIfAny,
            cellData.sectionData?[overviewChartIndex] === cdi {
            
            // if this function just enabled an overview chart: Also enable the "view overview charts" switch
            showOverviewChartsSwitchOutlet.setOn(true, animated: true)
            showOverviewChartsSwitch(showOverviewChartsSwitchOutlet) // programmatically activate this switch
            
        }
        

        
    }
    
    func getIndexPathForCDI(cdi :DKW2ChartDisplayInfo) -> IndexPath? {
                
        for (i, cellData) in tableViewData.enumerated() {
            if let sectionData = cellData.sectionData,
                sectionData.count > 0 { // make sure it's a chart and not a fixed option
                for (j, chartCdi) in sectionData.enumerated() {
                    if cdi === chartCdi {
                        return IndexPath(row: j, section: i)
                    }
                }
            }
        }
        return nil
    }
    
    func getCellDataForCDI(cdi :DKW2ChartDisplayInfo) -> SectionRowData? {
               
        for (i, cellData) in tableViewData.enumerated() {
            if let sectionData = cellData.sectionData,
                sectionData.count > 0 { // make sure it's a chart and not a fixed option
                for cdiCandidate in sectionData {
                    if cdi === cdiCandidate {
                        return cellData
                    }
                }
            }
        }
        
        return nil
    }
    
    @objc func showBordersAction(sender: UIButton!) {
        print("Button tapped with mCharts index : \(sender.tag)")
        
        if let chart = mChartManager?.mCharts[sender.tag],
            let chartManager = mChartManager,
//            chartManager.getChartBorderVisible() !== chartManager.getChartByGUID(chart.calibratedChart.dKW2Chart!.tag.guid) {
            chartManager.getChartBorderVisible() !== chart {
            
            mChartManager?.setChartBorderVisible(aChart: chart.calibratedChart.dKW2Chart!.tag.guid)
            DKW2ChartManager.refreshCharts()
            
            sender.titleLabel?.text = "button_hide_chart_border".localized
            
            chartCenterButtonAction(sender: sender)
            
        } else {
            
            // Hide chart borders and return to regular mode
            
            mChartManager?.setChartBorderVisible(aChart: nil)
            
            DKW2ChartManager.refreshCharts()

            sender.titleLabel?.text = "button_show_chart_border".localized
            
            
            performSegue(withIdentifier: "unwindSegueToMapView", sender: self)

        }
    }
    
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
		if indexPath.row == 0 { // Chart Set
            
			if tableViewData[indexPath.section].opened == true {
				tableViewData[indexPath.section].opened = false
                
				let sections = IndexSet.init(integer: indexPath.section)
				tableView.reloadSections(sections, with: .none)
                
                if let chartTableCell = tableView.cellForRow(at: indexPath) as? ChartsTableCell {
                    chartTableCell.expandButtonImageView.isHidden = false
                    chartTableCell.minimizeButtonImageView.isHidden = true
                }
			} else {
                openChartsetSection(indexPath)
			}
            
        } else { // Single Chart
            
            let previouslySelected = selectedIndexPath
            if selectedIndexPath.row == indexPath.row && selectedIndexPath.section == indexPath.section { // if item was already selected
                selectedIndexPath = IndexPath(row: -1, section: 0)
            } else {
                selectedIndexPath = indexPath
            }
            
            if let listItemCell = tableView.cellForRow(at: indexPath) as? ChartsTableCell {
                if previouslySelected.row == indexPath.row && previouslySelected.section == indexPath.section {
                    listItemCell.expandedView?.isHidden = true
                    tableView.reloadRows(at: [indexPath], with: .none)

                    listItemCell.expandButtonImageView.isHidden = false
                    listItemCell.minimizeButtonImageView.isHidden = true
                    
                } else {
                    expandChartCell(indexPath)
                    
                    if let previous = tableView.cellForRow(at: previouslySelected) as? ChartsTableCell {
                        
                        previous.expandedView?.isHidden = true
                        tableView.reloadRows(at: [previouslySelected], with: .none)
                        
                        previous.expandButtonImageView.isHidden = false
                        previous.minimizeButtonImageView.isHidden = true
                    }
                }
            }
        }
	}
    
    func openChartsetSection(_ indexPath :IndexPath){
        tableViewData[indexPath.section].opened = true
        
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
        
        if let chartTableCell = tableView.cellForRow(at: indexPath) as? ChartsTableCell {
            chartTableCell.expandButtonImageView.isHidden = true
            chartTableCell.minimizeButtonImageView.isHidden = false
        }
    }
    
    func expandChartCell(_ indexPath :IndexPath){
        if let listItemCell = tableView.cellForRow(at: indexPath) as? ChartsTableCell {
            listItemCell.expandedView?.isHidden = false
            tableView.reloadRows(at: [indexPath], with: .none)
            
            listItemCell.expandButtonImageView.isHidden = true
            listItemCell.minimizeButtonImageView.isHidden = false
            
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if (touch?.view == backgroundView){
			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
			self.dismiss(animated: true, completion: nil)
		}
	}
	
	func setEnabled(_ cell :ChartsTableCell, enabled :Bool){
		if let indexPath = tableView.indexPath(for: cell) {
			if indexPath.row == 0 { // tapped on a section ( = chartset or a fixed option)
				
				var cellData = tableViewData[indexPath.section]

				var chartSetString = ""
				if let cellData_cInfo = cellData.cInfo {

					cellData_cInfo.visible = enabled
					updateChartsVisibility(aProductID: cellData_cInfo.productID!, aModuleBitIndex: cellData_cInfo.moduleBitIndex!, aModuleName: cellData_cInfo.mName!, aVisible: enabled)
					chartSetString = "module-\(cellData_cInfo.productID ?? -1)-\(cellData_cInfo.moduleBitIndex ?? -1)"
					if let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY) {
						chartVisibilitySettings.set(enabled, forKey:chartSetString)
						chartVisibilitySettings.synchronize()
					}

					// refresh chart visibility in mapview
					DKW2ChartManager.refreshCharts()
					
				} else {	// in case it's one of the fixed options
					chartSetString = "module-\(cellData.title)"
					
					let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
					defaultSettings?.set(enabled, forKey: chartSetString)
					defaultSettings?.synchronize()
					ViewController.instance?.updateSettings()

				}
				
			} else {
				if let cInfo = tableViewData[indexPath.section].sectionData?[indexPath.row - 1] {
                    setChartVisible(cdi: cInfo, visible: enabled)
				}
                
				// refresh chart visibility in mapview
				DKW2ChartManager.refreshCharts()
			}
		}
	}
	
	func updateChartsVisibility(aProductID :Int64, aModuleBitIndex :Int, aModuleName :String, aVisible :Bool) {
		var chartList = DKW2ChartManager.getChartManager().mCharts
		
		for cInfo in chartList {
			if let tag = cInfo.calibratedChart.dKW2Chart?.tag,
				tag.productID == aProductID,
				tag.moduleBitIndex == aModuleBitIndex,
				tag.moduleName == aModuleName {
				
				cInfo.setModuleInViewer(aVisible)
				
				//				if let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY),
				//					let chartGUID = tag.guid.toString() {
				//					chartVisibilitySettings.set(aVisible, forKey:chartGUID)
				//					chartVisibilitySettings.synchronize()
				//				}
				
			}
		}
	}
    
//    // Convert from Routetablemanager to expand chart entries for extra data and options.
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //        dout("heightForRowAt \(indexPath.section) \(indexPath.row) selected: \(routeTableCells[indexPath.row].isSelected)")
//
//        if let listItemCell = tableView.cellForRow(at: indexPath) as? ChartsTableCell{
//
//            //        dout("heightForRowAt \(indexPath.section) \(indexPath.row) selected: \(selectedIndexPath.row == indexPath.row) listItemView: \(listItemCell.listItemView != nil)")
//
//            //        if let rtCell = (tableView.cellForRow(at: indexPath) as? RouteTableCell){
//            if selectedIndexPath.row == indexPath.row && selectedIndexPath.section == indexPath.section {
//                //            listItemCell.listItemView?.isHidden = true
//                listItemCell.expandedView?.isHidden = false
//
//                return 155
//            } else {
//                //            listItemCell.listItemView?.isHidden = false
//                listItemCell.expandedView?.isHidden = true
//
//                return 65
//            }
//        }
//        return 65
//    }
}
