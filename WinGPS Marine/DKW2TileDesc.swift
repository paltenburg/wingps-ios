//
//  DKW2TileDesc.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation


class DKW2TileDesc {
    var offset: UInt64 = 0
    var size: Int = 0
    var width: Int = 0
    var height: Int = 0
    var tileIndex: Int = 0
}
