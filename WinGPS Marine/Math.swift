//
//  Math.swift
//  MapDemo
//
//  Created by Standaard on 01/11/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

public class Math {
	
	//    public static final double HALFPI = 0.5 * Math.PI;
	//    public static final double TWOPI = 2 * Math.PI;
	//    public static final double TWOPIINV = (1.0 / TWOPI);
	//    public static final float PIf = (float)Math.PI;
	//    public static final float TWOPIf = (float)TWOPI;
	//    public static final float TWOPIINVf = (float)TWOPIINV;
	//
	//    public static final double SQRT2 = 1.414213562373095050; // ?2.
	//    public static final float SQRT2f = (float)SQRT2;         // ?2 in single precision.
	
	static let DEGTORAD :Double = Double.pi / 180.0
	static let RADTODEG :Double = 180.0 / Double.pi
	
	/**
	* Calculates the angle between normalized vector A and normalized vector B. Returned angles are between 0 and PI.
	*
	* @param aA Normalized vector A.
	* @param aB Normalized vector B.
	* @return The angle between A and B. Angle is between 0 and PI.
	*/
	static func angleAbsNormalized(A :Double2, B :Double2) -> Double {
		let cosAngle :Double = A.x * B.x + A.y * B.y
		if cosAngle >= 1.0 {
			return 0.0
		} else if (cosAngle <= -1.0) {
			return Double.pi
		}
		return acos(cosAngle)
	}
	
	//    /**
	//     * Determines if Value is between Bound1 and Bound2. Bound1 can be smaller, greater or equal to Bound2.
	//     * The result is also true if Value is equal to Bound1 or Bound2.
	//     *
	//     * @param aBound1 The first boundary.
	//     * @param aBound2 The second boundary.
	//     * @param aValue The value to test.
	//     *
	//     * @return True, if Value lies between Bound1 and Bound2.
	//     */
	//    public static boolean between(float aBound1, float aBound2, float aValue) {
	//    if (aBound1 < aBound2)
	//    return (aValue >= aBound1) && (aValue <= aBound2);
	//    else
	//    return (aValue >= aBound2) && (aValue <= aBound1);
	//    }
	//
	//    /**
	//     * Calculates the mimimum sphere through two points.
	//     *
	//     * @param aCenter Center of the sphere.
	//     * @param aP1 First point.
	//     * @param aP2 Second point.
	//     * @return The radius squared of the minimum sphere.
	//     */
	//    public static double calcSphere(Double3 aCenter, final Double3 aP1, final Double3 aP2) {
	//    aCenter.x = 0.5 * (aP1.x + aP2.x);
	//    aCenter.y = 0.5 * (aP1.y + aP2.y);
	//    aCenter.z = 0.5 * (aP1.z + aP2.z);
	//    return StMath.distSqr(aCenter, aP1);
	//    }
	//
	//    /**
	//     * Calculates the mimimum sphere through three points.
	//     *
	//     * @param aCenter Center of the sphere.
	//     * @param aP1 First point.
	//     * @param aP2 Second point.
	//     * @param aP3 Third point.
	//     * @return The radius squared of the minimum sphere.
	//     */
	//    public static double calcSphere(Double3 aCenter, final Double3 aP1, final Double3 aP2, final Double3 aP3) {
	//    Double3 a = new Double3();
	//    a.subtract(aP1, aP3);
	//    double aSqr = lenSqr(a);
	//    Double3 b = new Double3();
	//    b.subtract(aP2, aP3);
	//    double bSqr = lenSqr(b);
	//
	//    Double3 ab = new Double3();
	//    cross(ab, a, b);
	//
	//    Double3 q = new Double3();
	//    a.multiply(bSqr);
	//    b.multiply(aSqr);
	//    Double3 t = new Double3();
	//    t.subtract(b, a);
	//    cross(q, t/*aSqr * b - bSqr * a*/, ab);
	//    q.divide(2 * lenSqr(ab));
	//    aCenter.add(q, aP3);
	//
	//    return distSqr(aCenter, aP1);
	//    }
	//
	//    /**
	//     * Calculates the mimimum sphere through four points.
	//     *
	//     * @param aCenter Center of the sphere.
	//     * @param aP1 First point.
	//     * @param aP2 Second point.
	//     * @param aP3 Third point.
	//     * @param aP4 Fourth point.
	//     * @return The radius squared of the minimum sphere.
	//     */
	//    public static double calcSphere(Double3 aCenter, final Double3 aP1, final Double3 aP2, final Double3 aP3, final Double3 aP4) {
	//    double P1Sqr = lenSqr(aP1);
	//    double P2Sqr = lenSqr(aP2);
	//    double P3Sqr = lenSqr(aP3);
	//    double P4Sqr = lenSqr(aP4);
	//
	//    double A1 = aP3.y * (aP1.z - aP2.z) - aP3.z * (aP1.y - aP2.y) + (aP1.y * aP2.z - aP1.z * aP2.y);
	//    double A2 = aP3.x * (aP1.z - aP2.z) - aP3.z * (aP1.x - aP2.x) + (aP1.x * aP2.z - aP1.z * aP2.x);
	//    double A3 = aP3.x * (aP1.y - aP2.y) - aP3.y * (aP1.x - aP2.x) + (aP1.x * aP2.y - aP1.y * aP2.x);
	//    double A4 = aP3.x * (aP1.y * aP2.z - aP1.z * aP2.y) - aP3.y * (aP1.x * aP2.z - aP1.z * aP2.x) + aP3.z * (aP1.x * aP2.y - aP1.y * aP2.x);
	//    double M11 = -aP4.x * A1 + aP4.y * A2 - aP4.z * A3 + A4;
	//
	//    A1 = aP3.y * (aP1.z - aP2.z) - aP3.z * (aP1.y - aP2.y) + (aP1.y * aP2.z - aP1.z * aP2.y);
	//    A2 = P3Sqr * (aP1.z - aP2.z) - aP3.z * (P1Sqr - P2Sqr) + (P1Sqr * aP2.z - aP1.z * P2Sqr);
	//    A3 = P3Sqr * (aP1.y - aP2.y) - aP3.y * (P1Sqr - P2Sqr) + (P1Sqr * aP2.y - aP1.y * P2Sqr);
	//    A4 = P3Sqr * (aP1.y * aP2.z - aP1.z * aP2.y) - aP3.y * (P1Sqr * aP2.z - aP1.z * P2Sqr) + aP3.z * (P1Sqr * aP2.y - aP1.y * P2Sqr);
	//    double M12 = -P4Sqr * A1 + aP4.y * A2 - aP4.z * A3 + A4;
	//
	//    A1 = aP3.x * (aP1.z - aP2.z) - aP3.z * (aP1.x - aP2.x) + (aP1.x * aP2.z - aP1.z * aP2.x);
	//    A2 = P3Sqr * (aP1.z - aP2.z) - aP3.z * (P1Sqr - P2Sqr) + (P1Sqr * aP2.z - aP1.z * P2Sqr);
	//    A3 = P3Sqr * (aP1.x - aP2.x) - aP3.x * (P1Sqr - P2Sqr) + (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    A4 = P3Sqr * (aP1.x * aP2.z - aP1.z * aP2.x) - aP3.x * (P1Sqr * aP2.z - aP1.z * P2Sqr) + aP3.z * (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    double M13 = -P4Sqr * A1 + aP4.x * A2 - aP4.z * A3 + A4;
	//
	//    A1 = aP3.x * (aP1.y - aP2.y) - aP3.y * (aP1.x - aP2.x) + (aP1.x * aP2.y - aP1.y * aP2.x);
	//    A2 = P3Sqr * (aP1.y - aP2.y) - aP3.y * (P1Sqr - P2Sqr) + (P1Sqr * aP2.y - aP1.y * P2Sqr);
	//    A3 = P3Sqr * (aP1.x - aP2.x) - aP3.x * (P1Sqr - P2Sqr) + (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    A4 = P3Sqr * (aP1.x * aP2.y - aP1.y * aP2.x) - aP3.x * (P1Sqr * aP2.y - aP1.y * P2Sqr) + aP3.y * (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    double M14 = -P4Sqr * A1 + aP4.x * A2 - aP4.y * A3 + A4;
	//
	//    A1 = aP3.x * (aP1.y * aP2.z - aP1.z * aP2.y) - aP3.y * (aP1.x * aP2.z - aP1.z * aP2.x) + aP3.z * (aP1.x * aP2.y - aP1.y * aP2.x);
	//    A2 = P3Sqr * (aP1.y * aP2.z - aP1.z * aP2.y) - aP3.y * (P1Sqr * aP2.z - aP1.z * P2Sqr) + aP3.z * (P1Sqr * aP2.y - aP1.y * P2Sqr);
	//    A3 = P3Sqr * (aP1.x * aP2.z - aP1.z * aP2.x) - aP3.x * (P1Sqr * aP2.z - aP1.z * P2Sqr) + aP3.z * (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    A4 = P3Sqr * (aP1.x * aP2.y - aP1.y * aP2.x) - aP3.x * (P1Sqr * aP2.y - aP1.y * P2Sqr) + aP3.y * (P1Sqr * aP2.x - aP1.x * P2Sqr);
	//    double M15 = -P4Sqr * A1 + aP4.x * A2 - aP4.y * A3 + aP4.z * A4;
	//
	//    aCenter.x = 0.5 * M12 / M11;
	//    aCenter.y = -0.5 * M13 / M11;
	//    aCenter.z = 0.5 * M14 / M11;
	//    return distSqr(aCenter, aP1);
	//    }
	//
	//    /**
	//     * Calculates the cross-product of two vectors: P = A x B. This is not the same as P = B x A !!!
	//     *
	//     * @param aP The calculated cross-product.
	//     * @param aA Vector A.
	//     * @param aB Vector B.
	//     */
	//    public static void cross(Double3 aP, final Double3 aA, final Double3 aB) {
	//    aP.x = aA.y * aB.z - aA.z * aB.y;
	//    aP.y = aA.z * aB.x - aA.x * aB.z;
	//    aP.z = aA.x * aB.y - aA.y * aB.x;
	//    }
	//
	//    /**
	//     * Calculates the distance of point aA to point aB.
	//     *
	//     * @param aA Point A.
	//     * @param aB Point B.
	//     * @return The distance between A and B.
	//     */
	//    public static float dist(final Float2 aA, final Float2 aB) {
	//    float dx = aA.x - aB.x;
	//    float dy = aA.y - aB.y;
	//    return (float)Math.sqrt(dx *dx + dy * dy);
	//    }
	
	static func dist(A :Double2, B :Double2) -> Double {
		let dx = A.x - B.x
		let dy = A.y - B.y
		return sqrt(dx * dx + dy * dy)
	}
	
	//    /**
	//     * Calculates the distance of point aA to point aB.
	//     *
	//     * @param aA Point A.
	//     * @param aB Point B.
	//     * @return The distance between A and B.
	//     */
	//    public static float dist(final Float3 aA, final Float3 aB) {
	//    double dx = aA.x - aB.x;
	//    double dy = aA.y - aB.y;
	//    double dz = aA.z - aB.z;
	//    return (float)Math.sqrt(dx * dx + dy * dy + dz * dz);
	//    }
	//
	//    public static double dist(final Double3 aA, final Double3 aB) {
	//    double dx = aA.x - aB.x;
	//    double dy = aA.y - aB.y;
	//    double dz = aA.z - aB.z;
	//    return Math.sqrt(dx * dx + dy * dy + dz * dz);
	//    }
	//
	//    /**
	//     * Calculates the distance of point aP to line aA-aB. The functions that have a parameter aFraction,
	//     * return the relative location (at aA = 0.0; at aB = 1.0) of the point on the line closest to aP.
	//     */
	//    public static float dist(final Float2 aA, final Float2 aB, final Float2 aP, MutableFloat aFraction) {
	//    Float2 V = new Float2(aB.x - aA.x, aB.y - aA.y);
	//    Float2 W = new Float2(aP.x - aA.x, aP.y - aA.y);
	//
	//    float c1 = dot(W, V);
	//    float c2 = dot(V, V);
	//    float fraction = c1 / c2;
	//    Float2 Pb = Float2.makeFloat2(aA.x + fraction * V.x, aA.y + fraction * V.y);
	//    aFraction.setValue(fraction);
	//    return dist(aP, Pb);
	//    }
	
	/**
	* Calculates the squared distance of point aA to point aB.
	*
	* @param aA Point A.
	* @param aB Point B.
	* @return The distance squared between A and B.
	*/
	static func distSqrF(A :[Float], B :[Float]) -> Float {
		let x = A[0] - B[0]
		let y = A[1] - B[1]
		return x * x + y * y
	}
	
	static func distSqrD(A :[Double], B :[Double]) -> Double {
		let dx = A[0] - B[0]
		let dy = A[1] - B[1]
		return dx * dx + dy * dy
	}
	
	//    public static double distSqr(final Double3 aA, final Double3 aB) {
	//    double dx = aA.x - aB.x;
	//    double dy = aA.y - aB.y;
	//    double dz = aA.z - aB.z;
	//    return dx * dx + dy * dy + dz * dz;
	//    }
	
	    /**
	     * Calculates the distance of point aP to a line segment aA-aB.
	     *
	     * @param aA Point A.
	     * @param aB Point B.
	     * @param aP Point P
	     * @param aFraction Returns the relative location (at A = 0.0; at B = 1.0) of the point on the line closest to aP.
	     * @return The distance from point P to line A-B.
	     */

//    static func distToLineSegment(aA :Float2, aB :Float2, aP :Float2, aFraction :inout Float) {
//        var V :Float2 = Float2(x: aB.x - aA.x, y: aB.y - aA.y)
//        var W :Float2 = Float2(x: aP.x - aA.x, y: aP.y - aA.y)
//
//	    float c1 = dot(W, V);
//	    if (c1 <= 0.0) {
//	    aFraction.setValue(0.0f);
//	    return dist(aP, aA);
//	    }
//
//	    float c2 = dot(V, V);
//	    if (c2 <= c1) {
//	    aFraction.setValue(1.0f);
//	    return dist(aP, aB);
//	    }
//
//	    float fraction = c1 / c2;
//	    Float2 Pb = Float2.makeFloat2(aA.x + fraction * V.x, aA.y + fraction * V.y);
//	    aFraction.setValue(fraction);
//	    return dist(aP, Pb);
//	    }
    
    static func distToLineSegment(aA :Double2, aB :Double2, aP :Double2, aFraction :inout Double) -> Double {
        var V :Double2 = Double2(x: aB.x - aA.x, y: aB.y - aA.y)
        var W :Double2 = Double2(x: aP.x - aA.x, y: aP.y - aA.y)

        var c1 :Double = dot(W, V)
        if c1 <= 0.0 {
            aFraction = 0.0
            return dist(A: aP, B: aA)
        }

        var c2 :Double = dot(V, V)
        if c2 <= c1 {
            aFraction = 1.0
            return dist(A: aP, B: aB)
        }

        var fraction :Double = c1 / c2
        var Pb :Double2 = Double2(x: aA.x + fraction * V.x, y: aA.y + fraction * V.y)
        aFraction = fraction
        return dist(A: aP, B: Pb)
    }
    
//    static func  distToLineSegment(aA :Double3 aA, final Double3 aB, final Double3 aP, MutableDouble aFraction) -> Double {
//        Double3 V = new Double3(aB.x - aA.x, aB.y - aA.y, aB.z - aA.z);
//        Double3 W = new Double3(aP.x - aA.x, aP.y - aA.y, aP.z - aA.z);
//
//        double c1 = dot(W, V);
//        if (c1 <= 0.0) {
//            aFraction.setValue(0.0);
//            return dist(aP, aA);
//        }
//
//        double c2 = dot(V, V);
//        if (c2 <= c1) {
//            aFraction.setValue(1.0);
//            return dist(aP, aB);
//        }
//
//        double fraction = c1 / c2;
//        Double3 Pb = Double3.makeDouble3(aA.x + fraction * V.x, aA.y + fraction * V.y, aA.z + fraction * V.z);
//        aFraction.setValue(fraction);
//        return dist(aP, Pb);
//    }
    
	//
	//    public static double distToPlane(final Double4 aPlaneDef, final Double3 aP) {
	//    return aPlaneDef.x * aP.x + aPlaneDef.y * aP.y + aPlaneDef.z * aP.z + aPlaneDef.w;
	//    }
	//
	//    /*
	//     * Calculates the dot-product of two vectors.
	//     */
	//    public static float dot(final Float2 aA, final Float2 aB) {
    //    return aA.x * aB.x + aA.y * aB.y;
    //    }
    
    static func dot(_ aA :Double2, _ aB :Double2) -> Double {
        return aA.x * aB.x + aA.y * aB.y
    }
    
	//    public static double dot(final Double3 aA, final Double3 aB) {
	//    return aA.x * aB.x + aA.y * aB.y + aA.z * aB.z;
	//    }
	//
	//    public static void initPlaneDef(Double4 aPlaneDef, final Double3 aA, final Double3 aB, final Double3 aC) {
	//    Double3 AB = new Double3(aB.x - aA.x, aB.y - aA.y, aB.z - aA.z);
	//    Double3 AC = new Double3(aC.x - aA.x, aC.y - aA.y, aC.z - aA.z);
	//    Double3 N = new Double3();
	//    cross(N, AB, AC);
	//    initPlaneDef(aPlaneDef, aA, N);
	//    }
	//
	//    public static void initPlaneDef(Double4 aPlaneDef, final Double3 aA, final Double3 aNormal) {
	//    Double3 N = new Double3();
	//    normalize(N, aNormal);
	//    aPlaneDef.setXYZ(N);
	//    aPlaneDef.w = -(N.x * aA.x + N.y * aA.y + N.z * aA.z);
	//    }
	
	/**
	* Calculates the intersection of two lines (a line has infinite length).
	*/
	static func intersectLines(A :Double2, B :Double2, P :Double2, Q :Double2, S :inout Double2) -> Bool {
		let C = Double2(x: A.x - B.x, y: A.y - B.y)
		let R = Double2(x: P.x - Q.x, y: P.y - Q.y)
		
		let a :Double = A.x * B.y - A.y * B.x
		let b :Double = P.x * Q.y - P.y * Q.x
		let c :Double = C.x * R.y - C.y * R.x
		
		S.x = (a * R.x - C.x * b) / c
		S.y = (a * R.y - C.y * b) / c
		
		return !S.x.isInfinite && !S.y.isInfinite
	}
	
	//    /**
	//     * Calculates the intersection of two line segments (a line segment has limited length).
	//     */
	//    public static boolean intersectLineSegments(final Point A, final Point B, final Point P, final Point Q, Point S) {
	//    boolean HasIntersection = false;
	//    // AB = A + Labda C
	//    int Cx = B.x - A.x;
	//    int Cy = B.y - A.y;
	//    // PQ = P + Labda R
	//    int Rx = Q.x - P.x;
	//    int Ry = Q.y - P.y;
	//    //Point C = new Point(B.x - A.x, B.y - A.y);    // AB = A + Labda C
	//    //Point R = new Point(Q.x - P.x, Q.y - P.y);    // PQ = P + Labda R
	//
	//    // Calculate Labda of line PQ.
	//    float LabdaPQ;
	//    if (Math.abs(Cy) <= Math.abs(Cx)) {
	//    // Line AB has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = (float)Cy / (float)Cx;
	//    LabdaPQ = (P.y - A.y + (A.x - P.x) * Gradient) / (Rx * Gradient - Ry);
	//    } else {
	//    // Line AB has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = (float)Cx / (float)Cy;
	//    LabdaPQ = (P.x - A.x + (A.y - P.y) * Gradient) / (Ry * Gradient - Rx);
	//    }
	//    // If LabdaPQ is between 0 and 1, the intersection is between P and Q.
	//    if (LabdaPQ >= 0 && LabdaPQ <= 1) {
	//    float LabdaAB;
	//    if (Math.abs(Ry) <= Math.abs(Rx)) {
	//    // Line PQ has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = (float)Ry / (float)Rx;
	//    LabdaAB = (A.y - P.y + (P.x - A.x) * Gradient) / (Cx * Gradient - Cy);
	//    } else {
	//    // Line PQ has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = (float)Rx / (float)Ry;
	//    LabdaAB = (A.x - P.x + (P.y - A.y) * Gradient) / (Cy * Gradient - Cx);
	//    }
	//    // If LabdaAB is between 0 and 1, the intersection is between A and B.
	//    if (LabdaAB >= 0 && LabdaAB <= 1) {
	//    S.x = (int)(P.x + LabdaPQ * Rx);
	//    S.y = (int)(P.y + LabdaPQ * Ry);
	//
	//    // An intersection was found if S.x and S.y are both finite.
	//    HasIntersection = (!Float.isInfinite(S.x) && !Float.isNaN(S.x)) && (!Float.isInfinite(S.y) && !Float.isNaN(S.y));
	//    }
	//    }
	//    return HasIntersection;
	//    }
	//
	//    /**
	//     * Calculates the intersection of two line segments (a line segment has limited length).
	//     */
	//    public static boolean intersectLineSegments(final Point A, final Point B, final Point P, final Point Q) {
	//    boolean HasIntersection = false;
	//    // AB = A + Labda C
	//    int Cx = B.x - A.x;
	//    int Cy = B.y - A.y;
	//    // PQ = P + Labda R
	//    int Rx = Q.x - P.x;
	//    int Ry = Q.y - P.y;
	//    //Point C = new Point(B.x - A.x, B.y - A.y);    // AB = A + Labda C
	//    //Point R = new Point(Q.x - P.x, Q.y - P.y);    // PQ = P + Labda R
	//
	//    // Calculate Labda of line PQ.
	//    float LabdaPQ;
	//    if (Math.abs(Cy) <= Math.abs(Cx)) {
	//    // Line AB has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = (float)Cy / (float)Cx;
	//    LabdaPQ = (P.y - A.y + (A.x - P.x) * Gradient) / (Rx * Gradient - Ry);
	//    } else {
	//    // Line AB has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = (float)Cx / (float)Cy;
	//    LabdaPQ = (P.x - A.x + (A.y - P.y) * Gradient) / (Ry * Gradient - Rx);
	//    }
	//    // If LabdaPQ is between 0 and 1, the intersection is between P and Q.
	//    if (LabdaPQ >= 0 && LabdaPQ <= 1) {
	//    float LabdaAB;
	//    if (Math.abs(Ry) <= Math.abs(Rx)) {
	//    // Line PQ has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = (float)Ry / (float)Rx;
	//    LabdaAB = (A.y - P.y + (P.x - A.x) * Gradient) / (Cx * Gradient - Cy);
	//    } else {
	//    // Line PQ has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = (float)Rx / (float)Ry;
	//    LabdaAB = (A.x - P.x + (P.y - A.y) * Gradient) / (Cy * Gradient - Cx);
	//    }
	//    // If LabdaAB is between 0 and 1, the intersection is between A and B.
	//    if (LabdaAB >= 0 && LabdaAB <= 1) {
	//    int Sx = (int)(P.x + LabdaPQ * Rx);
	//    int Sy = (int)(P.y + LabdaPQ * Ry);
	//
	//    // An intersection was found if S.x and S.y are both finite.
	//    HasIntersection = (!Float.isInfinite(Sx) && !Float.isNaN(Sx)) && (!Float.isInfinite(Sy) && !Float.isNaN(Sy));
	//    }
	//    }
	//    return HasIntersection;
	//    }
	//
	//    /**
	//     * Calculates the intersection of two line segments (a line segment has limited length).
	//     */
	//    public static boolean intersectLineSegments(final Float2 A, final Float2 B, final Float2 P, final Float2 Q, Float2 S) {
	//    boolean HasIntersection = false;
	//    Float2 C = new Float2(B.x - A.x, B.y - A.y);    // AB = A + Labda C
	//    Float2 R = new Float2(Q.x - P.x, Q.y - P.y);    // PQ = P + Labda R
	//
	//    // Calculate Labda of line PQ.
	//    float LabdaPQ;
	//    if (Math.abs(C.y) <= Math.abs(C.x)) {
	//    // Line AB has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = C.y / C.x;
	//    LabdaPQ = (P.y - A.y + (A.x - P.x) * Gradient) / (R.x * Gradient - R.y);
	//    } else {
	//    // Line AB has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = C.x / C.y;
	//    LabdaPQ = (P.x - A.x + (A.y - P.y) * Gradient) / (R.y * Gradient - R.x);
	//    }
	//    // If LabdaPQ is between 0 and 1, the intersection is between P and Q.
	//    if (LabdaPQ >= 0 && LabdaPQ <= 1) {
	//    float LabdaAB;
	//    if (Math.abs(R.y) <= Math.abs(R.x)) {
	//    // Line PQ has an absolute gradient of less than 45 deg. Use the gradient.
	//    float Gradient = R.y / R.x;
	//    LabdaAB = (A.y - P.y + (P.x - A.x) * Gradient) / (C.x * Gradient - C.y);
	//    } else {
	//    // Line PQ has an absolute gradient of more than 45 degrees. Use the inverse gradient.
	//    float Gradient = R.x / R.y;
	//    LabdaAB = (A.x - P.x + (P.y - A.y) * Gradient) / (C.y * Gradient - C.x);
	//    }
	//    // If LabdaAB is between 0 and 1, the intersection is between A and B.
	//    if (LabdaAB >= 0 && LabdaAB <= 1) {
	//    S.x = P.x + LabdaPQ * R.x;
	//    S.y = P.y + LabdaPQ * R.y;
	//
	//    // An intersection was found if S.x and S.y are both finite.
	//    HasIntersection = (!Float.isInfinite(S.x) && !Float.isNaN(S.x)) && (!Float.isInfinite(S.y) && !Float.isNaN(S.y));
	//    }
	//    }
	//    return HasIntersection;
	//    }
	
	static func intersectLineSegments(A :Double2, B :Double2, P :Double2, Q :Double2, S :inout Double2) -> Bool {
		var HasIntersection :Bool = false
		let C = Double2(x: B.x - A.x, y: B.y - A.y)    // AB = A + Labda C
		let R = Double2(x: Q.x - P.x, y: Q.y - P.y)    // PQ = P + Labda R
		
		// Calculate Labda of line PQ.
		var LabdaPQ = Double()
		if abs(C.y) <= abs(C.x) {
			// Line AB has an absolute gradient of less than 45 deg. Use the gradient.
			let Gradient = C.y / C.x
			LabdaPQ = (P.y - A.y + (A.x - P.x) * Gradient) / (R.x * Gradient - R.y)
		} else {
			// Line AB has an absolute gradient of more than 45 degrees. Use the inverse gradient.
			var Gradient = C.x / C.y
			LabdaPQ = (P.x - A.x + (A.y - P.y) * Gradient) / (R.y * Gradient - R.x)
		}
		// If LabdaPQ is between 0 and 1, the intersection is between P and Q.
		if LabdaPQ >= 0 && LabdaPQ <= 1 {
			var LabdaAB :Double
			if abs(R.y) <= abs(R.x) {
				// Line PQ has an absolute gradient of less than 45 deg. Use the gradient.
				var Gradient = R.y / R.x
				LabdaAB = (A.y - P.y + (P.x - A.x) * Gradient) / (C.x * Gradient - C.y)
			} else {
				// Line PQ has an absolute gradient of more than 45 degrees. Use the inverse gradient.
				var Gradient :Double = R.x / R.y
				LabdaAB = (A.x - P.x + (P.y - A.y) * Gradient) / (C.y * Gradient - C.x);
			}
			// If LabdaAB is between 0 and 1, the intersection is between A and B.
			if LabdaAB >= 0 && LabdaAB <= 1 {
				S.x = P.x + LabdaPQ * R.x
				S.y = P.y + LabdaPQ * R.y
				
				// An intersection was found if S.x and S.y are both finite.
				HasIntersection = (!S.x.isInfinite && !S.x.isNaN) && (!S.y.isInfinite && !S.y.isNaN)
			}
		}
		return HasIntersection
	}
	
	//    public static float len(final Float2 aA) {
	//    return (float)Math.sqrt(aA.x * aA.x + aA.y * aA.y);
	//    }
	//
	//    public static double len(final Double2 aA) {
	//    return Math.sqrt(aA.x * aA.x + aA.y * aA.y);
	//    }
	//
	//    public static float len(final Float3 aA) {
	//    return (float)Math.sqrt(aA.x * aA.x + aA.y * aA.y + aA.z * aA.z);
	//    }
	//
	//    public static double len(final Double3 aA) {
	//    return Math.sqrt(aA.x * aA.x + aA.y * aA.y + aA.z * aA.z);
	//    }
	
	/**
	* Calculates the squared length of a vector.
	*/
	static func lenSqr(_ A: Double2) -> Double {
		return A.x * A.x + A.y * A.y
	}
	
	//    public static double lenSqr(final Double3 aA) {
	//    return aA.x * aA.x + aA.y * aA.y + aA.z * aA.z;
	//    }
	//
	//    public static long lerp(long aA, long aB, float aFactor) { return (long)(aA + aFactor * (aB - aA)); }
	//
	//    public static float lerp(float aA, float aB, float aFactor) {
	//    return aA + aFactor * (aB - aA);
	//    }
    
    static func lerp(aA :Double, aB :Double, aFactor :Double) -> Double {
        return aA + aFactor * (aB - aA)
    }
    
    /**
     * Performs a linear interpolation between aA and aB based on the following formula: r = a + factor(b - a).
     *
     * @param aR Result.
     * @param aA Point A.
     * @param aB Point B.
     * @param aFactor Interpolation factor between 0.0 and 1.0.
     */
    static func lerp(aR :inout Double3, aA :Double3, aB :Double3, aFactor :Double)    {
        
        aR.x = aA.x + aFactor * (aB.x - aA.x)
        aR.y = aA.y + aFactor * (aB.y - aA.y)
        aR.z = aA.z + aFactor * (aB.z - aA.z)
    }
    
    /**
     * Performs a linear interpolation between aA and aB based on the following formula: r = a + factor(b - a).
     */
    static func lerp(aA :MKMapPoint, aB :MKMapPoint, aFactor :Double) -> MKMapPoint {
        var interpolatedMapPoint = MKMapPoint()
        interpolatedMapPoint.x = aA.x + aFactor * (aB.x - aA.x)
        interpolatedMapPoint.y = aA.y + aFactor * (aB.y - aA.y)
        return interpolatedMapPoint
    }
    
	//    /**
	//     * Determines if line segment P1-P2 overlaps Rect.
	//     *
	//     * @return True, if there is overlap.
	//     */
	//    public static boolean lineSegmentOverlapsRectangle(final RectF aRect, final Float2 aP1, final Float2 aP2) {
	//
	//    // If at least one of the points is inside the rectangle, the line segment overlaps the rectangle.
	//    if (pointInRect(aRect, aP1) || pointInRect(aRect, aP2))
	//    return true;
	//
	//    // Both points are outside the rectangle. If the line segment intersects one of the edges of the triangle, the line
	//    // segment overlaps the rectangle.
	//    Float2 P1 = new Float2(aRect.left, aRect.top);
	//    Float2 P2 = new Float2(aRect.right, aRect.top);
	//    Float2 P3 = new Float2(aRect.right, aRect.bottom);
	//    Float2 P4 = new Float2(aRect.left, aRect.bottom);
	//    Float2 S = new Float2();
	//    return intersectLineSegments(aP1, aP2, P1, P2, S) || intersectLineSegments(aP1, aP2, P2, P3, S) ||
	//    intersectLineSegments(aP1, aP2, P3, P4, S) || intersectLineSegments(aP1, aP2, P4, P1, S);
	//    }
	//
	//    /**
	//     * Determines if line segment P1-P2 overlaps Rect.
	//     *
	//     * @return True, if there is overlap.
	//     */
	//    public static boolean lineSegmentOverlapsRectangle(final Rect aRect, final Point aP1, final Point aP2) {
	//
	//    // If at least one of the points is inside the rectangle, the line segment overlaps the rectangle.
	//    if (pointInRect(aRect, aP1) || pointInRect(aRect, aP2))
	//    return true;
	//
	//    // Both points are outside the rectangle. If the line segment intersects one of the edges of the rectangle, the line
	//    // segment overlaps the rectangle.
	//    Point P1 = new Point(aRect.left, aRect.top);
	//    Point P2 = new Point(aRect.right, aRect.top);
	//    Point P3 = new Point(aRect.right, aRect.bottom);
	//    Point P4 = new Point(aRect.left, aRect.bottom);
	//    Point S = new Point();
	//    return intersectLineSegments(aP1, aP2, P1, P2, S) || intersectLineSegments(aP1, aP2, P2, P3, S) ||
	//    intersectLineSegments(aP1, aP2, P3, P4, S) || intersectLineSegments(aP1, aP2, P4, P1, S);
	//    }
	//
	//    /**
	//     * Determines if line segment P1-P2 overlaps a rectangle with corner points aRectLT, aRectRT, aRectLB and aRectRB.
	//     *
	//     * @return True, if there is overlap.
	//     */
	//    public static boolean lineSegmentOverlapsRectangle(final Point aRectLT, final Point aRectRT, final Point aRectLB, final Point aRectRB, final Point aP1, final Point aP2) {
	//
	//    // If at least one of the points is inside the rectangle, the line segment overlaps the rectangle.
	//    if (pointInRect(aRectLT.x, aRectRT.x, aRectLT.y, aRectLB.y, aP1) || pointInRect(aRectLT.x, aRectRT.x, aRectLT.y, aRectLB.y, aP2))
	//    return true;
	//
	//    // Both points are outside the rectangle. If the line segment intersects one of the edges of the rectangle, the line
	//    // segment overlaps the rectangle.
	//    return intersectLineSegments(aP1, aP2, aRectLT, aRectRT) || intersectLineSegments(aP1, aP2, aRectRT, aRectRB) ||
	//    intersectLineSegments(aP1, aP2, aRectRB, aRectLB) || intersectLineSegments(aP1, aP2, aRectLB, aRectLT);
	//    }
	
	/**
	* Normalizes vector A.
	*/
	static func normalize(_ A :Double2) -> Double2 {
		let L :Double = 1 / sqrt(A.x * A.x + A.y * A.y)
		
		let N = Double2(x: A.x * L, y: A.y * L)
		return N
	}
	
	//    public static void normalize(Float2 N, final Float2 A) {
	//    float L = 1 / (float)Math.sqrt(A.x * A.x + A.y * A.y);
	//    N.x = A.x * L;
	//    N.y = A.y * L;
	//    }
	//
	//    public static void normalize(Double3 N, final Double3 A)
	//{
	//    double L = 1 / Math.sqrt(A.x * A.x + A.y * A.y + A.z * A.z);
	//    N.x = A.x * L;
	//    N.y = A.y * L;
	//    N.z = A.z * L;
	//    }
	//
	//    /**
	//     * Determines if point P is inside a circle.
	//     */
	//    public static boolean pointInCircle(final Double2 Center, final double Radius, final Double2 P) {
	//    return distSqr(Center, P) <= Radius;
	//    }
	//
	//    /**
	//     * Determines if a point is inside a polygon. For points on the polygon's line segments, the result is undefined meaning
	//     * that it can be true or false depending on situation and implementation.
	//     */
	//    public static boolean pointInPolygon(final Point P, final PolyLineI Poly[], int Count) {
	//    int LCount = 0;
	//    for (int LineIndex = Count - 1; LineIndex >= 0; LineIndex--) {
	//    PolyLineI L = Poly[LineIndex];
	//    if (!L.Horz) {
	//    if ((P.y >= L.YMin) && (P.y < L.YMax)) {
	//    float SX = L.Coord.x + (P.y - L.Coord.y) * L.dxdy;
	//    if (SX < P.x)
	//    LCount++;
	//    }
	//    }
	//    }
	//    return (LCount & 1) != 0;
	//    }
	
	/**
	* Determines if a point is inside a polygon. For points on the polygon's line segments, the result is undefined meaning
	* that it can be true or false depending on situation and implementation.
	*/
	static func pointInPolygon(P :Double2, Poly :[PolyLineD], Count :Int) -> Bool {
		var LCount :Int = 0
		for LineIndex in (0 ..< Count).reversed() {
			let L = Poly[LineIndex]
			if !L.Horz {
				if (P.y >= L.YMin) && (P.y < L.YMax) {
					var SX = L.coord.x + (P.y - L.coord.y) * L.dxdy
					if SX < P.x {
						LCount += 1
					}
				}
			}
		}
		return (LCount & 1) != 0;
	}
	
	/**
	* The following PointInRect routine uses the top-left filling convention.
	* A point that is exactly on a left or top edge, is inside the rectangle.
	* A point that is exactly on a right or bottom edge, is outside the rectangle.
	*/
	static func pointInRect(R :RectangleD, P :Double2) -> Bool {
		return P.x >= R.XMin && P.x < R.XMax && P.y >= R.YMin && P.y < R.YMax;
	}
	
	//	func pointInRect(R :Rect, P: Point) -> Bool {
	//	    return P.x >= R.left && P.x < R.right && P.y >= R.top && P.y < R.bottom;
	//	    }
	//
	//	func pointInRect(left :Int, right :Int, top :Int, bottom :Int, P :Point) -> Bool {
	//	    return P.x >= left && P.x < right && P.y >= top && P.y < bottom;
	//	    }
	//
	//	func pointInRect(R :RectF, P :Float2) -> Bool {
	//	    return P.x >= R.left && P.x < R.right && P.y >= R.top && P.y < R.bottom;
	//	    }
	
	//    /**
	//     * Determines if a point is on a line or if not, on which side of the line it is.
	//     *
	//     * @param aA The first point that defines the line.
	//     * @param aB The second point that defines the line.
	//     * @param aP Point P
	//     * @return Right-handed coordinate system:
	//     *         Returns -1 if point P is at the left of line AB, 0 if point P is on line AB and 1 if point P is at the right of
	//     *         line AB when looking along AB from A to B.
	//     *         Left-handed coordinate system:
	//     *         Returns 1 if point P is at the left of line AB, 0 if point P is on line AB and -1 if point P is at the right of
	//     *         line AB when looking along AB from A to B.
	//     */
	//    public static int pointOnLine(final Float2 aA, final Float2 aB, final Float2 aP) {
	//    float D = (aB.y - aA.y) * (aP.x - aA.x) - (aB.x - aA.x) * (aP.y - aA.y);
	//    if (D < 0.0f)
	//    return -1;
	//    else if (D > 0.0f)
	//    return 1;
	//    else
	//    return 0;
	//    }
	//
	//    public static int pointOnLine(final Double2 aA, final Double2 aB, final Double2 aP) {
	//    double D = (aB.y - aA.y) * (aP.x - aA.x) - (aB.x - aA.x) * (aP.y - aA.y);
	//    if (D < 0.0)
	//    return -1;
	//    else if (D > 0.0)
	//    return 1;
	//    else
	//    return 0;
	//    }
	//
	/**
	* Initializes all members in each polygon point except Coord. Before calling this function, the Coord member of each
	* polygon point should be initialized.
	*
	* @param Poly Array of polygon points.
	* @param Count The number of points in Poly.
	*/
	static func polygonInitialize(Poly :[PolyLineI], Count :Int) {
		
		var L2 = Poly[0]
		for nIndex in (0 ..< Count).reversed() {
			var L1 = Poly[nIndex]
			var P2 = L2.Coord
			L1.XMin = min(L1.Coord.x, P2.x)
			L1.YMin = min(L1.Coord.y, P2.y)
			L1.XMax = max(L1.Coord.x, P2.x)
			L1.YMax = max(L1.Coord.y, P2.y)
			if L1.Coord.y == P2.y {
				L1.Horz = true
				L1.dxdy = 0
			} else {
				L1.Horz = false
				L1.dxdy = Float(P2.x - L1.Coord.x) / Float(P2.y - L1.Coord.y)
			}
			L1.Vert = L1.Coord.x == P2.x
			L2 = L1
		}
	}
	
	/**
	* Initializes all members in each polygon point except Coord. Before calling this function, the Coord member of each
	* polygon point should be initialized.
	*
	* @param Poly Array of polygon points.
	* @param Count The number of points in Poly.
	*/
	static func polygonInitialize(Poly :inout [PolyLineD], Count :Int) {
		
		var L2 = Poly[0]
		for nIndex in (0 ..< Count).reversed() {
			var L1 = Poly[nIndex]
			var P2 = L2.coord
			L1.XMin = min(L1.coord.x, P2.x)
			L1.YMin = min(L1.coord.y, P2.y)
			L1.XMax = max(L1.coord.x, P2.x)
			L1.YMax = max(L1.coord.y, P2.y)
			if L1.coord.y == P2.y {
				L1.Horz = true
				L1.dxdy = 0
			} else {
				L1.Horz = false
				L1.dxdy = (P2.x - L1.coord.x) / (P2.y - L1.coord.y)
			}
			L1.Vert = L1.coord.x == P2.x
			L2 = L1
		}
	}
	
	//    public static boolean polygonOverlap(final PolyLineI[] aPoly1, int aCount1, final PolyLineI[] aPoly2, int aCount2) {
	//
	//    // check if there is an intersection of all lines of polygon1 with all the lines of polygon2
	//    Point B = aPoly1[0].Coord;
	//    for (int I = aCount1 - 1; I >= 0; I--) {
	//    Point A = aPoly1[I].Coord;
	//    if (A != B) {
	//    Point Q = aPoly2[0].Coord;
	//    final PolyLineI PL1 = aPoly1[I];
	//    for (int J = aCount2 - 1; J >= 0; J--) {
	//    Point P = aPoly2[J].Coord;
	//    if (P != Q) {
	//    final PolyLineI PL2 = aPoly2[J];
	//    // first bounds checking for faster intersection calculation
	//    if ((PL1.XMax >= PL2.XMin && PL1.XMin <= PL2.XMax)
	//    && (PL1.YMax >= PL2.YMin && PL1.YMin <= PL2.YMax)) {
	//    if (StMath.intersectLineSegments(A, B, P, Q))
	//    return true;
	//    }
	//    Q = P;
	//    }
	//    }
	//    B = A;
	//    }
	//    }
	//    // No intersections found
	//
	//    // Check if first point of Poly1 is inside Poly2
	//    if (pointInPolygon(aPoly1[0].Coord, aPoly2, aCount2))
	//    return true;
	//
	//    // Check if first point of Poly2 is inside Poly1
	//    if (pointInPolygon(aPoly2[0].Coord, aPoly1, aCount1))
	//    return true;
	//
	//    // No Overlap
	//    return false;
	//    }
	//
	//
	//
	/**
	* Determines if a rectangle overlaps a polygon or triangle.
	*/
	static func rectOverlap(R :RectangleD, Count :Int, Poly :[PolyLineD]) -> Bool {
		// Both figures are overlapping if one of the following three conditions are met:
		// 1. One or more points of Poly are inside R.
		// 2. One or more points of R are inside Poly.
		// 3. One or more lines of R intersect with one or more lines of Poly.
		
		// Check condition 1: One or more points of Poly are inside R.
		for pIndex in (0 ..< Count).reversed() {
			if (pointInRect(R: R, P: Poly[pIndex].coord)) {
				return true
			}
		}
		
		// Check condition 2: One or more points of R are inside Poly.
		var Rect = [Double2](generating: {_ in Double2()}, count: 4)
		Rect[0] = R.getMin()
		Rect[1] = Double2(x: R.XMax, y: R.YMin)
		Rect[2] = R.getMax()
		Rect[3] = Double2(x: R.XMin, y:R.YMax)
		
		if pointInPolygon(P: Rect[0], Poly: Poly, Count: Count) || pointInPolygon(P: Rect[1], Poly: Poly, Count: Count) ||
			pointInPolygon(P: Rect[2], Poly: Poly, Count: Count) || pointInPolygon(P: Rect[3], Poly: Poly, Count: Count) {
			return true
		}
		
		// Check condition 3: One or more lines of R intersect with one or more lines of Poly.
		for pIndex in (0 ..< Count).reversed() {
			for rIndex in (0 ... 3).reversed() {
				var S = Double2()
				if intersectLineSegments(A: Rect[(rIndex+1) % 4], B: Rect[rIndex], P: Poly[(pIndex + 1) % Count].coord, Q: Poly[pIndex].coord, S: &S) {
					return true
				}
			}
		}
		return false
	}
	
	//    static public float reduceAngle(float aAngle) {
	//    float Result = aAngle;
	//    if (Math.abs(Result) > PIf) {
	//    // Adjust angle with NrOfPeriods*2*Pi to limit the angle to it's valid range.
	//    float Value = Result * TWOPIINVf;
	//    int NrOfPeriods = (int)Math.floor(Value + 0.5f);
	//    Result -= NrOfPeriods * TWOPIf;
	//    // Compensate for rounding errors
	//    if (Result < -PIf)
	//    Result += TWOPIf;
	//    else if (Result > PIf)
	//    Result -= TWOPIf;
	//    }
	//    return Result;
	//    }
	//
	//    static public double reduceAngle(double aAngle) {
	//    double Result = aAngle;
	//    if (Math.abs(Result) > Math.PI) {
	//    double Value = Result * StMath.TWOPIINV;
	//    long NrOfPeriods = (long)Math.floor(Value + 0.5);
	//    Result -= NrOfPeriods * StMath.TWOPI;
	//    if (Result < -Math.PI)
	//    Result += StMath.TWOPI;
	//    else if (Result > Math.PI)
	//    Result -= StMath.TWOPI;
	//    }
	//    return Result;
	//    }
	//
	//    /**
	//     * Reduces an angle by adding or subtracting an integer multiple of 2*Pi to return an angle between 0 and 2*Pi.
	//     * This function has the following limitations:
	//     *  - aAngle should be between -2^63 and 2^63 in case of double precision and between -2^31 and 2^31 in case of single precision to prevent integer overflow.
	//     *  - The function result becomes inaccurate for very large positive and negative numbers because of inaccuracies in the floating point numbers itself.
	//     *
	//     * @param aAngle The angle [rad].
	//     * @return Reduced angle between 0 and 2*Pi [rad]
	//     */
	//    static public double reduceAngle0(double aAngle) {
	//    double A = reduceAngle(aAngle);
	//    if (A < 0.0)
	//    A += StMath.TWOPI;
	//    return A;
	//    }
	//
	//    static public void rotate(Float2 Dst, final Float2 Src, float SinA, float CosA) {
	//    // Save the the Src can be same as Dst
	//    float X = Src.x;
	//    float Y = Src.y;
	//    // Apply rotation
	//    Dst.x = CosA * X - SinA * Y;
	//    Dst.y = SinA * X + CosA * Y;
	//    }
	//
	//    static public double sinSqr(double aX) {
	//    double SinX = Math.sin(aX);
	//    return (SinX * SinX);
	//    }
	//
	//    static public double sqr(double aValue) {
	//    return aValue * aValue;
	//    }
}
