//
//  ChartsTableCell.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 24/12/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class ChartsTableCell : UITableViewCell{
	
	var controller :ChartManagerController?
	
    @IBOutlet weak var chartEnabledSwitch: UISwitch!
    @IBOutlet weak var fileNameLabel: UILabel!
    
//    @IBOutlet weak var fileNameLabelLeadingConstraint: NSLayoutConstraint!
//    @IBOutlet weak var enabledSwitchLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingSpacingWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerChartButton: UIButton!
	
    @IBOutlet weak var expandButtonLabel: UILabel!
    
    @IBOutlet weak var expandedView: UIStackView!
    
    @IBOutlet weak var expandButtonImageView: UIImageView!
    @IBOutlet weak var minimizeButtonImageView: UIImageView!
    
    @IBOutlet weak var expandButton: UIButton!
    @IBAction func expandButtonPressed(_ sender: UIButton) {
        dout("Expand button pressed")
        expandedView.isHidden = true
    }
    
    @IBAction func chartEnabledSwitchAction(_ sender: UISwitch) {
		controller?.setEnabled(self, enabled: sender.isOn)
	}
    
    @IBOutlet weak var showBordersBottonOutlet: UIButton!

    @IBOutlet weak var cellContainerView: UIView!
    
    
}
