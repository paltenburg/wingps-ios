//
//  DKW2TileOverlay.swift
//  MapDemo
//
//  Created by Standaard on 19/05/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import MapKit

class DKW2TileOverlay : MKTileOverlay {
	
	let cache = NSCache<NSString, AnyObject>()
	let operationQueue = OperationQueue()
	
	override func url(forTilePath path: MKTileOverlayPath) -> URL {
		return URL(string: String(format: "https://tile.openstreetmap.org/%d/%d/%d.png", path.z, path.x, path.y))!
		//        return URL(string: String(format: "https://tile.openstreetmap.org/0/0/0.png", path.z, path.x, path.y))!
	}
	
	//	override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
	//		dout(.tiles, "empty loadTile")
	//		result(nil, nil)
	//	}
	
	//	override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
	//
	//
	////		func _loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
	//		dout(.tiles, "DKW2TileOverlay:loadtile() path:", path)
	//
	//		let url = self.url(forTilePath: path)
	//
	//		if let cachedData = cache.object(forKey: url.absoluteString as NSString) as? Data {
	//			dout(.tiles, "loadtile from cache, url: ", url.absoluteString)
	//			result(cachedData, nil)
	//
	//		} else {
	//
	////			let queue = DispatchQueue(label: "MyArrayQueue")
	////			///// lock thread for debugging purposes
	////			queue.sync() {
	////				DKW2GenerateTileByCoords(zoom: 0, northEast: MKCoordinateForMapPoint(mapRect.origin), southWest: MKCoordinateForMapPoint(MKMapPointMake(mapRect.origin.x + mapRect.size.width, mapRect.origin.y + mapRect.size.height)))
	////			}
	//
	//			//            let request = URLRequest(url: url)
	//			//            NSURLConnection.sendAsynchronousRequest(request, queue: operationQueue) {
	//			//                [weak self]
	//			//                response, data, error in
	//			//                if let data = data {
	//			//                    self?.cache.setObject(data as AnyObject, forKey: url.absoluteString as NSString)
	//			//                }
	//			//                result(data, error)
	//			//            }
	//
	//			//            UIGraphicsBeginImageContextWithOptions(CGSize(width: 265, height: 265), true, 1 )
	//			//            if let context :CGContext = UIGraphicsGetCurrentContext() {
	//
	//			let context = CGContext(data: nil, width: 256, height: 256, bitsPerComponent: 8, bytesPerRow: 0, space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: CGBitmapInfo.byteOrder32Little.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue)!
	//			context.translateBy(x: 0, y: CGFloat(256))
	//			context.scaleBy(x: 1, y: -1)
	//
	//			//            UIGraphicsPushContext(context!)
	//			//            image.drawAtPoint(CGPointZero)
	//			//            UIGraphicsPopContext()
	//
	//			if context != nil {
	//
	//				context.setFillColor(UIColor.blue.cgColor)
	//				context.fill(CGRect(x: 0, y:0, width:200, height:200))
	//
	//				let imageData :Data = UIImagePNGRepresentation(imageFromContext(context)!)!
	//
	//				if imageData != nil{
	//					cache.setObject(imageData as AnyObject, forKey: url.absoluteString as NSString)
	//				}
	//
	//				result(imageData, nil)
	//			} else {
	//				dout(.tiles, "context equals nil")
	//			}
	//		}
	//	}
	//
	override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {

		dout(.tiles, "loadtile at path:", path)

		UIGraphicsBeginImageContextWithOptions(CGSize(width: 265, height: 265), true, 1 )
		if let context :CGContext = UIGraphicsGetCurrentContext() {
			
			//	            context.setFillColor(UIColor.blue.cgColor)
			//	            context.fill(CGRect(x: 0, y:0, width:200, height:200))
			
			dout(.tiles, "loadtile context dimensions:", context.width, context.height)
			let imageData :Data = imageFromContext(context)!.pngData()!
			UIGraphicsEndImageContext()
			result(imageData, nil)
		} else {
			dout(.tiles, "context equals nil")
		}
		UIGraphicsEndImageContext()
	}
	//}
}

class DKW2BackgroundOverlay : MKTileOverlay {
	
//	override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
//		UIGraphicsBeginImageContextWithOptions(CGSize(width: 265, height: 265), true, 1 )
//		if let context :CGContext = UIGraphicsGetCurrentContext() {
//
//			dout(.tiles, "loadtile context dimensions:", context.width, context.height)
//			let imageData :Data = imageFromContext(context)!.pngData()!
//			UIGraphicsEndImageContext()
//			result(imageData, nil)
//		} else {
//			dout(.tiles, "context equals nil")
//		}
//		UIGraphicsEndImageContext()
//	}
    
    override func loadTile(at path: MKTileOverlayPath, result: @escaping (Data?, Error?) -> Void) {
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 265, height: 265), true, 1 )
        if let context :CGContext = UIGraphicsGetCurrentContext() {
            
            dout(.tiles, "loadtile context dimensions:", context.width, context.height)
            let imageData :Data = imageFromContext(context)!.pngData()!
            UIGraphicsEndImageContext()
            result(imageData, nil)
        } else {
            dout(.tiles, "context equals nil")
        }
        UIGraphicsEndImageContext()
    }
}

// convenience function
func imageFromContext(_ context: CGContext?) -> UIImage? {
	guard let context = context else {
		return nil
	}
	guard let cgImage = context.makeImage() else {
		return nil
	}
	return UIImage.init(cgImage: cgImage)
}
