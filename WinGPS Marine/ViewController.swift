//
//  ViewController.swift
//  MapDemo
//
//  Created by Standaard on 05/05/17.
//  Copyright © 2017 Stentec. All rights reserved.
//

import UIKit
import MapKit
import Foundation
import EFColorPicker
import Network

class ViewController: UIViewController, EFColorSelectionViewControllerDelegate, UIPopoverPresentationControllerDelegate
{
    func colorViewController(_ colorViewCntroller: EFColorSelectionViewController, didChangeColor color: UIColor) {
        mUserLocationColor = color
    }
    
    var navCtrlColor : UINavigationController?
    
    @IBOutlet weak var debugLabel: UILabel!
    
    @IBOutlet weak var mapView: StMapView!
    
    var dKW2Overlay :DKW2TileOverlay!
    var dKW2TileRenderer :DKW2TileOverlayRenderer?
    
    var dKW2BackgroundOverlay :DKW2BackgroundOverlay?
    
    var noaaOverlay :MKTileOverlay?
    
    var routeOverlay :RouteOverlay?
    var aisOverlay :AISOverlay?
    var trackOverlay :TrackOverlay?
    
    var networkOverlay :NetworkOverlay? = NetworkOverlay.instance // initialized earlier.
    
    static var instance :ViewController?
    
    static let WP_SCALE_THRESHOLD :CGFloat = 0.45
    static let WP_SIZE_THRESHOLD :CGFloat = 8
    static let WP_TEXT_SIZE_MIN :CGFloat = 8
    static let WP_TEXT_SIZE_MAX :CGFloat = 24
    static let WP_TEXT_SIZE_DEF :CGFloat = 14
    
    //	var locationManager = CLLocationManager()
    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        // Set up your manager properties here
        manager.allowsBackgroundLocationUpdates = false
        //		manager.distanceFilter = 1
        manager.delegate = self
        return manager
    }()
    var locationUpdatesEnabled :Bool = false // extra flag to keep track manually
    
    var stDB :[StDB] = [StDB]()
    var wpAnnotations :[StDBWaypointAnnotation] = [StDBWaypointAnnotation]()
    var wpAnnotationsOnMapview :[StDBWaypointAnnotation] = [StDBWaypointAnnotation]()
    
    var annotationVisibleGlobalBool :Bool = false // global variable used in updateWaypoints
    
    var mapRotation :Double = 0
    var zoomLevel :Double = 0
    
    var mChartOrientationMode = Resources.SETTING_CHART_ORIENTATION_DEFAULT_SETTING
    var courseUp :Bool = false
    
    
    var mFollowing :Bool = false
    
    @IBOutlet weak var InstrumentInfoLabel: UILabel!
    
    @IBOutlet weak var mainMenu: UIButton!
    @IBOutlet weak var centreer: UIButton!
    @IBOutlet weak var chartManagerButton: UIButton!
    
    @IBOutlet weak var onscreenInfoContainer: UIStackView!
    
    @IBOutlet weak var navigationInfoView: UIView!
    @IBOutlet weak var cogInfoLabel: UILabel!
    @IBOutlet weak var sogInfoLabel: UILabel!
    @IBOutlet weak var buttonsBackgroundView: UIView!
    
    @IBOutlet weak var navWaypointInfoView: UIView!
    @IBOutlet weak var ctsInfoLabel: UILabel!
    @IBOutlet weak var dtgInfoLabel: UILabel!
    
    //	@IBOutlet weak var touchInterceptingView: UIView!
    //	@IBOutlet var longPressPopupStandard: UIView!
    
    @IBOutlet var longPressPopupStandard: UIView!
    @IBOutlet var longPressPopupLite: UIView!
    
    @IBOutlet var NavwaypointOptionMenu: UIView!
    
    // Waypoint Info View and all it's outlets
    @IBOutlet var waypointInfoView: UIView!
    @IBOutlet weak var waypointInfoNameLabel: UILabel!
    @IBOutlet weak var waypointInfoSubnameView: UIView!
    @IBOutlet weak var waypointInfoSubnameLabel: UILabel!
    @IBOutlet weak var waypointInfoTypeLabel: UILabel!
    @IBOutlet weak var waypointInfoUrlView: UIView!
    @IBOutlet weak var waypointInfoUrlLabel: UILabel!
    @IBOutlet weak var waypointInfoUrlTextviewView: UIView!
    @IBOutlet weak var waypointInfoUrlTextView: UITextView!
    
    @IBOutlet weak var waypointInfoLatLabel: UILabel!
    @IBOutlet weak var waypointInfoLonLabel: UILabel!
    
    @IBAction func waypointInfoClose(_ sender: UIButton) {
        waypointInfoView.isHidden = true
    }
    
    @IBOutlet weak var wpInfoConstraintLeading: NSLayoutConstraint!
    @IBOutlet weak var wpInfoConstraintBottom: NSLayoutConstraint!
    
    @IBOutlet weak var generalUsageMenuLeft: NSLayoutConstraint!
    @IBOutlet weak var generalUsageMenuTop: NSLayoutConstraint!
    
    @IBOutlet weak var inScreenNotifications: UILabel!
    
    @IBOutlet weak var indicatorChartDownloading: UIStackView!
    
    @IBOutlet weak var connectionStatusLed: UIImageView!
    
    @IBOutlet weak var stopEditingRouteButtonView: UIView!
    
    @IBOutlet weak var generalUsageMenuContainer: UIView!
    @IBOutlet weak var generalUsageMenu: UIStackView!
    
    @IBOutlet weak var aisTargetInfoView: UIView!
    
    
    // User location related views
    var headingImageView: UIImageView?
    var CourseLineImageView: UIImageView?
    //	var NavWaypointLineImageView: UIImageView?
    var distanceCircleViews = [MKCircle]()
    
    var userLocation: CLLocation?
    var userLocationLastUpdateNavLine: CLLocation?
    var userHeading: CLLocationDirection?
    var userCourse: CLLocationDirection = 0
    var userSpeed: CLLocationSpeed = 0
    
    // Settings
    
    var mSettingsShowWaypoints : Bool = true
    var mSettingsShowWaypointNames : Bool = false
    
    var mSettingsShowAppleMaps : Bool = true
    var mSettingsShowNoaaCharts : Bool = false
    
    //    var mSettingsShowTailTrack : Bool = false
    
    //	var mSettingsUnitsNautical : Bool = true
    var mRouteOverlayEnabled : Bool = true
    var mAISOverlayEnabled : Bool = true
    var mTrackOverlayEnabled : Bool = false // initially set to false, so when switched on, it will initialize tracking.
    
    var mNetworkOverlayEnabled : Bool = false // initially set to false, so when switched on, we can do initialization
    
    var mLoadNetwork :Bool = false // initially set to false, so when switched on, it will initialize
    
    //	var mSogUnit = ""
    //	var mUnitDistSmall :String = Resources.SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT
    //	var mUnitDistLarge :String = Resources.SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT
    //	var mSpeedConversion : Float = 1
    
    // Distance circles
    var mSettingsDistanceCirclesEnabled = false
    var mSettingsDistanceCircleCount = 0
    var mSettingsDistanceCircleDistance = 0 // meters
    
    // Distance circles settings view
    @IBOutlet weak var distanceCirclesSettingsView: UIView!
    @IBOutlet weak var distanceCirclesEnabledSwitch: UISwitch!
    @IBAction func distanceCirclesEnabledSwitch(_ sender: Any) {
        if distanceCirclesEnabledSwitch.isOn != mSettingsDistanceCirclesEnabled {
            mSettingsDistanceCirclesEnabled = distanceCirclesEnabledSwitch.isOn
            UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(mSettingsDistanceCirclesEnabled, forKey: Resources.SETTING_DISTANCE_CIRCLES_ENABLED)
            drawDistanceCircles()
        }
    }
    @IBOutlet weak var distanceCirclesDistanceTF: UITextField!
    @IBOutlet weak var distanceCirclesUnitLabel: UILabel!
    var mdistanceCirclesUnitLabelUnit = ""
    @IBAction func distanceCirclesDistanceTFEndEditing(_ sender: UITextField) {
        // parse textfield to number
        if var valueText = sender.text,
           mdistanceCirclesUnitLabelUnit != "" {
            valueText = valueText.replacingOccurrences(of: ",", with: ".")
            if let value = Double(valueText) {
                mSettingsDistanceCircleDistance = Int(StUtils.unitConvert(value: value, fromUnit: mdistanceCirclesUnitLabelUnit, targetUnit: Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER))
                
                UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(mSettingsDistanceCircleDistance, forKey: Resources.SETTING_DISTANCE_CIRCLES_DISTANCE)
                
                distanceCirclesUnitLabel.text = StUtils.getUnitStringForDistance(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
                distanceCirclesUnitLabel.setNeedsDisplay()
                
                distanceCirclesDistanceTF.text = StUtils.formatDistanceValueOnly(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
                distanceCirclesDistanceTF.setNeedsDisplay()
                
                mdistanceCirclesUnitLabelUnit = StUtils.getUnitForDistance(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
                
                drawDistanceCircles()
            }
        }
    }
    @IBOutlet weak var distanceCirclesNumberLabel: UILabel!
    @IBOutlet weak var distanceCirclesNumberStepper: UIStepper!
    @IBAction func distanceCirclesNumberChanged(_ sender: UIStepper) {
        let stepperValue = Int(sender.value)
        if stepperValue != mSettingsDistanceCircleCount {
            mSettingsDistanceCircleCount = stepperValue
            UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.set(mSettingsDistanceCircleCount, forKey: Resources.SETTING_DISTANCE_CIRCLES_COUNT)
            distanceCirclesNumberLabel.text = String(mSettingsDistanceCircleCount)
            drawDistanceCircles()
        }
    }
    @IBAction func distanceCirclesViewClose(_ sender: UIButton) {
        distanceCirclesSettingsView.isHidden = true
        self.view.endEditing(true)
    }
    
    @IBAction func distanceCirclesBackButton(_ sender: Any) {
        distanceCirclesSettingsView.isHidden = true
        self.view.endEditing(true)
        
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
        
    }
    
    var mLongPressPoint : CGPoint?
    //	var positionImageView : UIImageView?
    var navwaypointIconImageView : UIImageView?
    
    var mNavWaypoint : NavigationWaypointAnnotation?
    var mNavigationWPView : MKAnnotationView?
    var mNavWaypointLine : MKPolyline?
    var mNavWaypointOnNetwork = false
    
    var mTailTrackPolyline :MKPolyline?
    
    //	var mLastUserLocation : CLLocation?
    var mLocationView :MKAnnotationView?
    var mUserLocationColor = UIColor(red: 0.4, green: 0, blue: 1, alpha: 1)
    
    var mAnnotationViewTapped : MKAnnotationView?
    var mWaypointTapped : StDBWaypoint?
    var waypointBorderView : UIImageView?
    var mWaypointInfoViewDragged : Bool = false
    
    var mFillBackground = false //draws gray overlay over tiles not covered by DKW2 content. Covering Apple Maps
    
    var canDetermineConnectionType = false // is only available in IOS 12+
    var connectedWithWifi :Bool = false
    var connectedWithCellular :Bool = false
    
    var mapViewBlurLayerView :UIView? = nil
    
    override var prefersHomeIndicatorAutoHidden : Bool { return true }
    
    var deselectSelectedAnnotationOnTap : UIGestureRecognizer? = nil
    
    var updateWaypointsTimestampLastUpdate = Date()
    var nextUpdatewaypointsPlanned = false
    
    var distanceCirclesZoomedOut = false
    var distanceCirclesZoomThreshold :Double = 11
    
    var waypointTouchEnabled :Bool = true
    
    //	var longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(didLongPress(_:)))
    
    var routePointTapGesture :RoutePointTapGestureRecognizer?
    
    var longPressGesture :UILongPressGestureRecognizer?
    
    var longPressPopup :UIView?
    var longPressOngoing :Bool = false
    
    //    var annotationsCopy = [MKAnnotation]()
    
    var storedMapPosition :MKCoordinateRegion? = nil
    
    var accountSettingsWindowIsLocked = false
    // Segue's:
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "downloadManagerSegue"  {
            //downloadManagerButton.image = UIImage(named: "icons8-download-filled-100")
        } else if segue.identifier == "chartManagerSegue" {
            //  chartManagerButton.im = UIImage(named: "icon_chart_manager")
            //		} else if let settingsController = segue.destination as? SettingsController {
            //			settingsController.delegate = self
        } else if segue.identifier == "waypointInfoSegue" {
            let vc = segue.destination as? WaypointInfoController
            vc?.annotationView = mAnnotationViewTapped!
            vc?.waypoint = mWaypointTapped!
        } else if segue.identifier == "accountSegue" {
            
            if let vc = segue.destination as? AccountSettingsController {
                vc.windowIsLocked = accountSettingsWindowIsLocked
            }
        }
    }
    
    @IBAction func fromMainMenuToSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    @IBAction func fromSettingsToUnitSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "unitSettingsSegue", sender: self)
    }
    
    @IBAction func fromUnitSettingsToSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    @IBAction func fromSettingsToTcpConnectionSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "tcpConnectionSegue", sender: self)
    }
    
    @IBAction func fromTCPSettingsToSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "settingsSegue", sender: self)
    }
    
    @IBAction func fromSettingsToDistanceCirclesSettings(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        
        if distanceCirclesSettingsView.superview == nil {
            self.view.addSubview(distanceCirclesSettingsView)
            
            self.view.addConstraints([
                NSLayoutConstraint(item: distanceCirclesSettingsView,
                                   attribute: .centerX,
                                   relatedBy: .equal,
                                   toItem: self.view,
                                   attribute: .centerX,
                                   multiplier: 1.0,
                                   constant: 0
                ),
                NSLayoutConstraint(item: distanceCirclesSettingsView,
                                   attribute: .top,
                                   relatedBy: .equal,
                                   toItem: onscreenInfoContainer,
                                   attribute: .bottom,
                                   multiplier: 1.0,
                                   constant: 8
                )
            ])
            
            //			//add toolbar with done-button
            //			let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 30))
            //			//create left side empty space so that done button set on right side
            //			let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            //			let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction(_:)))
            //			toolbar.setItems([flexSpace, doneBtn], animated: false)
            //			toolbar.sizeToFit()
            //			//setting toolbar as inputAccessoryView
            //			distanceCirclesDistanceTF.inputAccessoryView = toolbar
            
            // set options values
            distanceCirclesEnabledSwitch.isOn = mSettingsDistanceCirclesEnabled
            distanceCirclesNumberLabel.text = String(mSettingsDistanceCircleCount)
            distanceCirclesNumberStepper.value = Double(mSettingsDistanceCircleCount)
            
        } else {
            distanceCirclesSettingsView.isHidden = false
        }
        
        mdistanceCirclesUnitLabelUnit = StUtils.getUnitForDistance(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
        distanceCirclesUnitLabel.text = StUtils.getUnitStringForDistance(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
        distanceCirclesDistanceTF.text = StUtils.formatDistanceValueOnly(distInM: Double(mSettingsDistanceCircleDistance), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
        
        distanceCirclesSettingsView.setNeedsDisplay()
        
        //		distanceCirclesSettingsView.center = CGPoint(x: 300, y: 300)
    }
    
    @objc func doneButtonAction(_ sender: Any? = nil) {
        self.view.endEditing(true)
    }
    
    @IBAction func fromMainMenuToAccount(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        
        // close main menu, then open account settings
        //
        //		let transition: CATransition = CATransition()
        //		transition.duration = 0.1
        //		transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //		transition.type = CATransitionType.fade
        //		MainMenuController.instance?.view.window!.layer.add(transition, forKey: nil)
        //
        //		MainMenuController.instance?.dismiss(animated: false, completion: {
        //		})
        
        self.performSegue(withIdentifier: "accountSegue", sender: self)
        
    }
    
    @IBAction func fromSettingsToMainMenu(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
    }
    
    @IBAction func fromAccountToMainMenu(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "mainMenuSegue", sender: self)
    }
    
    @IBAction func fromAccountToCreateAccount(segue: UIStoryboardSegue){
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
        self.performSegue(withIdentifier: "createAccountSegue", sender: self)
    }
    
    ///////////// is deze action wel gekoppeld?
    @IBAction func unwindToMapView(segue: UIStoryboardSegue) {
        //downloadManagerButton.image = UIImage(named: "icons8-download-100")
        //chartManagerButton.image = UIImage(named: "icon_chart_manager")
        chartManagerButton.isSelected = false
        mainMenu.isSelected = false
    }
    
    @IBAction func longPressSetNavWP(_ sender: Any) {
        // remove popups and old navwaypoint
        longPressPopup?.removeFromSuperview()
        //		positionImageView?.removeFromSuperview()
        navwaypointIconImageView?.removeFromSuperview()
        
        if mLongPressPoint != nil {
            let location = mapView.convert(mLongPressPoint!, toCoordinateFrom: mapView)
            setNavigationWaypoint(location)
        }
    }
    
    @IBAction func longPressCreateRoute(_ sender: UIButton) {
        
        if mLongPressPoint != nil {
            let location :CLLocationCoordinate2D = mapView.convert(mLongPressPoint!, toCoordinateFrom: mapView)
            createNewRoute(location)
        }
        longPressPopup?.removeFromSuperview()
    }
    
    @IBAction func removeNavwaypointButtonPressed(_ sender: UIButton) {
        NavwaypointOptionMenu.removeFromSuperview()
        
        removeNavwaypoint()
    }
    
    @IBAction func StopEditingRouteButton(_ sender: UIButton) {
        stopEditMode()
    }
    
    func addLightBlurEffect(to view :UIView?){
        if let view = view {
            let lightBlurEffect = UIVisualEffectView(effect: UIBlurEffect(style: .light))
            //var bounds = navigationInfoView.bounds
            //bounds.size.width += 20
            //bounds.origin.x -= 10
            lightBlurEffect.frame = view.bounds
            lightBlurEffect.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.insertSubview(lightBlurEffect, at: 0)
            lightBlurEffect.layer.zPosition = -1
        }
    }
    
    override func viewDidLoad() {
        
        //        /// DEBUG Testing IAP
        //
        //        //        InAppPurchases.service.checkProducts()
        //        let purchases = AppStorePurchases()
        //        purchases.verifyReceipt()
        
        
        //		// set paid version in debug: //////// test setting it back
        //		if StUtils.isDebug() {
        //			Resources.appType = .standard
        //		} else {
        //			Resources.appType = .lite
        //		}
        
        ////  TODO Debugging ////       paid version always on, for testing in Testflight
        //        		Resources.appType = .standard
        
        switch Resources.appType {
        case .lite:
            dout("Lite version")
            longPressPopup = longPressPopupLite
        case .plus:
            dout("Standard paid version")
            longPressPopup = longPressPopupStandard
        case .dkw1800:
            dout("DKW 1800")
            longPressPopup = longPressPopupLite
        case .friesemeren:
            dout("Friese Meren")
            longPressPopup = longPressPopupLite
            
        }
        
        enableMapviewBlur()
        
        if #available(iOS 12.0, *) {
            canDetermineConnectionType = true
            
            let monitor = NWPathMonitor()
            monitor.pathUpdateHandler = { path in
                if path.status == .satisfied {
                    if path.isExpensive {
                        self.connectedWithCellular = true
                        self.connectedWithWifi = false
                    } else {
                        self.connectedWithWifi = true
                        self.connectedWithCellular = false
                    }
                } else {
                    print("No connection.")
                    self.connectedWithWifi = false
                    self.connectedWithCellular = false
                }
                dout("Dataconnection updated: connectedWithWifi = \(self.connectedWithWifi), connectedWithCellular = \(self.connectedWithCellular)")
            }
            
            let queue = DispatchQueue(label: "Monitor")
            monitor.start(queue: queue)
        }
        
        navigationInfoView.layer.cornerRadius = 8
        navigationInfoView.clipsToBounds = true
        addLightBlurEffect(to: navigationInfoView)
        
        //        let lightBlurEffect   = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        //        //var bounds = navigationInfoView.bounds
        //        //bounds.size.width += 20
        //        //bounds.origin.x -= 10
        //        lightBlurEffect.frame = navigationInfoView.bounds
        //        lightBlurEffect.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        navigationInfoView.addSubview(lightBlurEffect)
        //        lightBlurEffect.layer.zPosition = -1
        //
        
        navWaypointInfoView.layer.cornerRadius = 8
        navWaypointInfoView.clipsToBounds = true
        addLightBlurEffect(to: navWaypointInfoView)
        
        distanceCirclesSettingsView.layer.cornerRadius = 8
        distanceCirclesSettingsView.clipsToBounds = true
        
        aisTargetInfoView.layer.cornerRadius = 8
        aisTargetInfoView.clipsToBounds = true
        
        generalUsageMenuContainer.layer.cornerRadius = 8
        addLightBlurEffect(to: generalUsageMenuContainer)
        
        
        stopEditingRouteButtonView.layer.cornerRadius = 8
        stopEditingRouteButtonView.clipsToBounds = true
        addLightBlurEffect(to: stopEditingRouteButtonView)
        
        //        let visualEffectView2 = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        //        visualEffectView2.frame = navWaypointInfoView.bounds
        //        visualEffectView2.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //        navWaypointInfoView.addSubview(visualEffectView2)
        //        visualEffectView2.layer.zPosition = -1
        addLightBlurEffect(to: navWaypointInfoView)
        
        // Only in route edit mode:
        // RoutePointTapGestureRecognizer activates with downtap, so the routepoint is draggable rightaway, instead of having to select it first, than
        routePointTapGesture = RoutePointTapGestureRecognizer(target: self, action: #selector(self.didRoutePointTap(_:)))
        routePointTapGesture?.delegate = self
        mapView.addGestureRecognizer(routePointTapGesture!)
        
        let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didSingleTap(_:)))
        singleTapGesture.delegate = self
        singleTapGesture.require(toFail: routePointTapGesture!)
        mapView.addGestureRecognizer(singleTapGesture)
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(didLongPress(_:)))
        longPressGesture?.delegate = self
        mapView.addGestureRecognizer(longPressGesture!)
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        panGesture.delegate = self
        mapView.addGestureRecognizer(panGesture)
        
        
        buttonsBackgroundView.layer.cornerRadius = 8
        buttonsBackgroundView.clipsToBounds = true
        addLightBlurEffect(to: buttonsBackgroundView)
        
        waypointInfoView.translatesAutoresizingMaskIntoConstraints = false
        waypointInfoView.layer.cornerRadius = 8
        waypointInfoView.clipsToBounds = true
        addLightBlurEffect(to: waypointInfoView)
        
        let waypointInfoPanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedWaypointInfoView(_:)))
        waypointInfoView.isUserInteractionEnabled = true
        waypointInfoView.addGestureRecognizer(waypointInfoPanGesture)
        
        longPressPopup?.layer.cornerRadius = 8
        addLightBlurEffect(to: longPressPopup)
        
        NavwaypointOptionMenu.layer.cornerRadius = 8
        addLightBlurEffect(to: NavwaypointOptionMenu)
        
        //		let positionImage = UIImage(named: "longpress-location")
        //		positionImageView = UIImageView(image: positionImage)
        
        let visualEffectView3   = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView3.frame = buttonsBackgroundView.bounds
        visualEffectView3.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        buttonsBackgroundView.addSubview(visualEffectView3)
        
        visualEffectView3.layer.zPosition = -1
        
        mapView.showsCompass = false
        
        if #available(iOS 11.0, *) {
            let compassBtn = MKCompassButton(mapView:mapView)
            view.addSubview(compassBtn)
            compassBtn.translatesAutoresizingMaskIntoConstraints = false
            compassBtn.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -30).isActive = true
            compassBtn.topAnchor.constraint(equalTo: onscreenInfoContainer.bottomAnchor, constant: 10).isActive = true
            compassBtn.compassVisibility = .adaptive
            
            mapView.showsScale = false
            let scale = MKScaleView(mapView: mapView)
            scale.scaleVisibility = .visible // always visible
            //			scale.frame.origin = CGPoint(x: self.view.frame.minX + 20, y: 90)
            
            view.addSubview(scale)
            scale.translatesAutoresizingMaskIntoConstraints = false
            scale.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
            scale.topAnchor.constraint(equalTo: onscreenInfoContainer.bottomAnchor, constant: 10).isActive = true
            
        } else {
            mapView.showsScale = true
        }
        
        // loading map view that the user last turned the screen off with
        let settings = UserDefaults.standard
        let storedMapPosX :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_X)) as? Double
        let storedMapPosY :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_Y)) as? Double
        let storedMapPosWidth :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_WIDTH)) as? Double
        let storedMapPosHeight :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_HEIGHT)) as? Double
        if let x = storedMapPosX,
           let y = storedMapPosY,
           let width = storedMapPosWidth,
           let height = storedMapPosHeight {
            self.storedMapPosition = MKCoordinateRegion(MKMapRect(x: x, y: y, width: width, height: height))
            if let mapPos = storedMapPosition {
                self.mapView.setRegion(mapPos, animated: false)
            }
        }
        
    }
    
    @objc func didSingleTap(_ sender: UIGestureRecognizer) {
        
        dout("didSingleTap")
        
        if let st = longPressGesture?.state {
            print("state rawvalue: \(String(st.rawValue)) \(String(st.rawValue) )")
        }
        
        if longPressGesture?.state == .began ||
            longPressGesture?.state == .changed ||
            longPressGesture?.state == .ended
        { return }
        
        // cancel popups
        // remove old/other popups
        generalUsageMenuContainer.isHidden = true
        waypointInfoView.isHidden = true
        aisTargetInfoView.isHidden = true
        selectedAisObject = nil
        
        longPressPopup?.removeFromSuperview()
        
        var tappedPoint = sender.location(in: mapView)
        let location = mapView.convert(tappedPoint, toCoordinateFrom: mapView)
        
        // handle route tap
        routeOverlay?.onSingleTap(sender, tappedPoint: tappedPoint, tappedLocation: location)
        
        // handle AIS taps via onselect of waypoints
        
        // handle network tap
        networkOverlay?.onSingleTap(sender, tappedPoint: tappedPoint, tappedLocation: location)
    }
    
    // Only in edit mode
    @objc func didRoutePointTap(_ sender: UIGestureRecognizer) {
        
        // Send to route overlay
        if let state = routePointTapGesture?.state {
            if state == .began {
                self.mapView.isScrollEnabled = false
                
                var tappedPoint = sender.location(in: mapView)
                let location = mapView.convert(tappedPoint, toCoordinateFrom: mapView)
                
                routeOverlay?.onSingleTap(sender, tappedPoint: tappedPoint, tappedLocation: location)
                
            } else if state == .ended {
                self.mapView.isScrollEnabled = true
                
            } else {
                var tappedPoint = sender.location(in: mapView)
                let location = mapView.convert(tappedPoint, toCoordinateFrom: mapView)
                
                routeOverlay?.draggedPinAnnotationView(sender)
            }
        }
    }
    
    @objc func didDragMap(_ sender: UIGestureRecognizer) {
        if sender.state == .began {
            // Apply little delay for following operation, otherwise mapView.userTrackingMode won't yet be updated
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
                if self.mapView.userTrackingMode != MKUserTrackingMode.follow {
                    
                    //			mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
                    
                    self.mFollowing = false
                    self.centreer.isSelected = false
                }
            })
        }
    }
    
    @objc func didLongPress(_ sender: UILongPressGestureRecognizer){
        
        // only respond to the first frame of the longpress
        if sender.state != .began { return }
        
        // remove old/other popups
        generalUsageMenuContainer.isHidden = true
        waypointInfoView.isHidden = true
        longPressPopup?.removeFromSuperview()
        
        mLongPressPoint = sender.location(in: mapView)
        
        // check for hit on routepoint
        if let lpPoint = mLongPressPoint,
           let touchedRoutePoint = routeOverlay?.touchingRoutePoint(point: lpPoint) {
            
            dout("touched route point:", touchedRoutePoint.name)
            
            if routeOverlay?.editRoute != nil {
                // if the mapview is in route-edit mode, and the touchedpoint is on the edited route
                
                if touchedRoutePoint.route === routeOverlay?.editRoute  {
                    // only show "delete routepoint" as menu option
                    routeOverlay?.makeRoutePointLongpressMenuEditMode(stackView: &generalUsageMenu, routePoint: touchedRoutePoint)
                    
                    generalUsageMenuLeft.constant = mLongPressPoint!.x
                    generalUsageMenuTop.constant = mLongPressPoint!.y
                    generalUsageMenuContainer.isHidden = false
                }
                
            } else {
                
                routeOverlay?.makeRoutePointLongpressMenu(stackView: &generalUsageMenu, routePoint: touchedRoutePoint)
                
                generalUsageMenuLeft.constant = mLongPressPoint!.x
                generalUsageMenuTop.constant = mLongPressPoint!.y
                generalUsageMenuContainer.isHidden = false
            }
            
        } else {
            
            // Did not hit anything specific
            
            // if the mapview is in route-edit mode:
            if routeOverlay?.editRoute != nil {
                
            } else {
                
                if let lpp = self.longPressPopup {
                    self.view.addSubview(lpp)
                    lpp.center = CGPoint(x: mLongPressPoint!.x, y: mLongPressPoint!.y - (lpp.frame.height / 2) - 50)
                }
                
                if navwaypointIconImageView == nil {
                    navwaypointIconImageView = Waypoints.getCustomWaypointIconView()
                }
            }
        }
        
        //		self.view.addSubview(navwaypointIconImageView!)
        ////		navwaypointIconImageView!.center = mLongPressPoint!
        //		navwaypointIconImageView!.bounds.origin = mLongPressPoint!
        
        //		self.view.addSubview(positionImageView!)
        //		positionImageView!.center = mLongPressPoint!
        
    }
    
    @objc func didDoubleTap(_ sender: UILongPressGestureRecognizer){
        // Zoom to underlying chart
        dout(.def, "doubleTapAction()")
        
        let locationPts = sender.location(in: mapView)
        let location = mapView.convert(locationPts, toCoordinateFrom: mapView)
        
        var centerCoords :CLLocationCoordinate2D
        if mFollowing,
           let userCoord = userLocation?.coordinate {
            centerCoords = userCoord
        } else {
            centerCoords = location
        }
        
        if let chart :DKW2ChartDisplayInfo = DKW2ChartManager.getChartManager().findDetailChartAtPos(aPos: location) {
            
            // zoom to the level of this chart
            
            let viewSizePts = mapView.bounds.size
            
            let cal = chart.calibratedChart.getCalibration()
            
            var chartCenter = cal.getPixCenter()
            // use chart center in chart pixels, then make rectangle with the size of mapview in points.
            let topLeft = Double2(x: chartCenter.x - Double(viewSizePts.width) / 2, y: chartCenter.y - Double(viewSizePts.height) / 2 )
            let bottomRight = Double2(x: chartCenter.x + Double(viewSizePts.width) / 2, y: chartCenter.y + Double(viewSizePts.height) / 2 )
            let topLeftGeoDeg = radToDeg(cal.pixToGeoRadWGS84(topLeft))
            let bottomRightGeoDeg = radToDeg(cal.pixToGeoRadWGS84(bottomRight))
            
            let region = MKCoordinateRegion(center: centerCoords,
                                            span: MKCoordinateSpan(
                                                latitudeDelta: topLeftGeoDeg.lat - bottomRightGeoDeg.lat,
                                                longitudeDelta: (bottomRightGeoDeg.lon > topLeftGeoDeg.lon ?
                                                                    bottomRightGeoDeg.lon - topLeftGeoDeg.lon
                                                                    : bottomRightGeoDeg.lon + 360 - topLeftGeoDeg.lon)))
            
            if courseUp == false {
                mapView.setRegion(region, animated: true)
            } else {
                mapView.setRegion(region, animated: false)
                mapView.camera.heading = userCourse
                mapView.setCamera(mapView.camera, animated: false)
            }
        }
        
    }
    
    @objc func draggedWaypointInfoView(_ sender:UIPanGestureRecognizer){
        //self.view.bringSubviewToFront(containerView)
        let translation = sender.translation(in: self.view)
        //		waypointInfoView.center = CGPoint(x: waypointInfoView.center.x + translation.x, y: waypointInfoView.center.y + translation.y)
        
        wpInfoConstraintLeading.constant += translation.x
        wpInfoConstraintBottom.constant += translation.y
        
        sender.setTranslation(CGPoint.zero, in: self.view)
        mWaypointInfoViewDragged = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        
        self.performSegue(withIdentifier: "splashScreenSegue", sender: self)
        
        if StUtils.isDebug() {
            dout("Debug version")
        } else {
            dout("Production version")
        }
        
        let chartVisibilityDefaults = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)!
        
        //		// clear chart visibility settings
        //		UserDefaults.standard.removePersistentDomain(forName:  Resources.SETTINGS_CHART_VISIBILLITY)
        //		chartVisibilityDefaults.synchronize()
        
        dout("Chart visibility by GUID:")
        var keys1 = Array(chartVisibilityDefaults.dictionaryRepresentation().keys)
        for i in 0 ..< keys1.count {
            dout("key: \(keys1[i]) \(chartVisibilityDefaults.string(forKey: keys1[i]))")
        }
        
        let chartCheckDefaults = UserDefaults(suiteName: StActManager.CHARTCHECKPREFSNAME)!
        
        //		 // clear settings
        //				UserDefaults.standard.removePersistentDomain(forName: StActManager.CHARTCHECKPREFSNAME)
        //				chartCheckDefaults.synchronize()
        
        dout("Chart check defaults:")
        var keys = Array(chartCheckDefaults.dictionaryRepresentation().keys)
        for i in 0 ..< keys.count {
            dout("key: \(keys[i]) \(chartCheckDefaults.string(forKey: keys[i]))")
        }
        
        //		dout("print out of user defaults:  \( String(Array(forName: Bundle.main.bundleIdentifier!.dictionaryRepresentation().keys).count) )")
        
        
        ViewController.instance = self
        mapView.listener = self
        
        // load waypoint-database files
        //		 loadWaypointFiles() // happens in refresh in mapViewSetup
        
        mapView.mapType = MKMapType.standard
        
        if let files = try? FileManager.default.contentsOfDirectory(at: getDKW2Directory(), includingPropertiesForKeys: nil, options: []){
            for file in files {
                dout(file)
            }
        }
        
        if let filepath = Bundle.main.path(forResource: "test", ofType: "txt") {
            dout("bundletest:", filepath)
        }
        
        if storedMapPosition == nil {
            let locationHeeg = CLLocationCoordinate2D(latitude: 52.3388, longitude: 5.1302)
            //		let span = MKCoordinateSpanMake(0.3, 0.3)
            let span = MKCoordinateSpan.init(latitudeDelta: 1, longitudeDelta: 1)
            let region : MKCoordinateRegion = MKCoordinateRegion(center: locationHeeg, span: span)
            mapView.setRegion(region, animated: true)
        }
        
        //		let template = "https://tile.openstreetmap.org/{z}/{x}/{y}.png"
        //		let template = "https://tileservice.charts.noaa.gov/tiles/50000_1/{z}/{x}/{y}.png"
        //        let template = "https://tile.openstreetmap.org/0/0/0.png"
        //		let customOverlay = MKTileOverlay(urlTemplate: template)
        //		customOverlay.canReplaceMapContent = true
        
        let noaaTileserverTemplate = "https://tileservice.charts.noaa.gov/tiles/50000_1/{z}/{x}/{y}.png"
        
        noaaOverlay = MKTileOverlay(urlTemplate: noaaTileserverTemplate)
        noaaOverlay?.canReplaceMapContent = false
        
        dKW2Overlay = DKW2TileOverlay()
        
        //        let customOverlay = ExploredTileOverlay(urlTemplate: template)
        
        //        let customOverlay = MKTileOverlay(urlTemplate: template)
        
        //        customOverlay.isGeometryFlipped = true
        //        customOverlay.tileSize = CGSize(width: 80, height:80)
        
        dKW2Overlay.canReplaceMapContent = !mSettingsShowAppleMaps
        
        StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.addOverlay(self.dKW2Overlay, level: .aboveLabels) })
        
        mapView.delegate = self
        

        
        //mapView.showsCompass = true
        
        checkLocationAuthorizationStatus()
        
        //
        
        let userLocation = mapView.userLocation
        dout("viewcontroller userLocation:", userLocation.coordinate)
        mapView.showsUserLocation = true
        //        let userRegion :MKCoordinateRegion = MKCoordinateRegion(center: userLocation.coordinate, span: span)
        //        //		mapView.setRegion(userRegion, animated: true)
        if storedMapPosition == nil {
            //            mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
            mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: false)
        }
        
        
        locationManager.startUpdatingHeading()
        locationManager.headingOrientation = CLDeviceOrientation.faceUp
//        locationManager.startUpdatingLocation()
//        locationManager.stopUpdatingLocation()
//        locationManager.startUpdatingLocation()
        locationManagerEnableUpdating()
        locationManagerDisableUpdating()
        locationManagerEnableUpdating()
        
        // Remove existing double tap recognizer, and add custom one
        for v in mapView!.subviews{
            if let grs = v.gestureRecognizers {
                for r in grs {
                    if let tgr = r as? UITapGestureRecognizer {
                        if tgr.numberOfTapsRequired == 2 {
                            v.removeGestureRecognizer(r)
                        }
                    }
                }
            }
        }
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didDoubleTap(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.numberOfTouchesRequired = 1
        doubleTapGesture.delegate = self
        //		doubleTapGR.cancelsTouchesInView = false
        //		touchInterceptingView.addGestureRecognizer(doubleTapGRMV)
        mapView.addGestureRecognizer(doubleTapGesture)
        
        //        let tapGR = UITapGestureRecognizer(target: self, action: #selector(ViewController.singleTapAction))
        //        tapGR.require(toFail: doubleTapGesture)
        //        tapGR.numberOfTapsRequired = 1
        //        mapView.addGestureRecognizer(tapGR)
        
        mapView.isPitchEnabled = false
        
        
        // This triggers a redraw, so that DKW2 tiles are shown over apple maps:
        StaticQueues.mapviewOverlaysLock.runLocked({
            self.mapView.removeOverlay(self.dKW2Overlay)
            self.mapView.addOverlay(self.dKW2Overlay)
        })
        
        //        if Resources.appType == .lite, Resources.appType == .standard {
        startChartCheck()
//        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100), execute: { self.startChartCheck() })
        //        }
        
        // Load and apply settings. Put at the end to draw extra overlays on top of maps.
        updateSettings()
        
        
        // setup chart manager and load network.
        // these are delayed to not block the booting of the app too much.
//        mapViewSetup()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000), execute: {
                                        self.mapViewSetup()
                                        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000), execute: { self.initializeWWNetwork() })
            
        })

        // Load and display network. Placed at the end so that the initial start up is quicker.
//        DispatchQueue.main.async{self.initializeWWNetwork()}
        
    }
    
    //	func showWaypointInfoView(view: MKAnnotationView){
    //		performSegue(withIdentifier: "waypointInfoSegue", sender: self)
    //
    //	}
    
    func centerOnLocation(location: CLLocationCoordinate2D){
        //let location = CLLocationCoordinate2D(latitude: 52.3388, longitude: 5.1302)
        
        //        let span = MKCoordinateSpanMake(0.3, 0.3)
        let span = MKCoordinateSpan.init(latitudeDelta: 1, longitudeDelta: 1)
        let region : MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func setNavigationWaypoint(_ location : CLLocationCoordinate2D){
        
        StaticQueues.mapviewAnnotationsLock.runLocked({
            if let navWP = self.mNavWaypoint {
                self.mapView.removeAnnotation(navWP)
            }
        })
        
        StaticQueues.mapviewOverlaysLock.runLocked({
            if let navWPLine = self.mNavWaypointLine {
                self.mapView.removeOverlay(navWPLine)
            }
        })
        
        //        mNavWaypoint = NavigationWaypointAnnotation(location)
        
        // check if on network
        var locationOnNetwork = networkOverlay?.getLocationOnNetwork(location: location)
        if let locationOnNetwork = locationOnNetwork {
            mNavWaypoint = NavigationWaypointAnnotation(locationOnNetwork)

            mNavWaypointOnNetwork = true
        } else {
            
            mNavWaypoint = NavigationWaypointAnnotation(location)
            mNavWaypointOnNetwork = false
        }
        
        StaticQueues.mapviewAnnotationsLock.runLocked({ self.mapView.addAnnotation(self.mNavWaypoint!) })
        
        drawNavigationWaypointLine()

        navWaypointInfoView.isHidden = false
        updateNavpointInfo()
    }
    
    func drawNavigationWaypointLine(){
        
        StaticQueues.mapviewOverlaysLock.runLocked({
            if let navWPLine = self.mNavWaypointLine {
                self.mapView.removeOverlay(navWPLine)
            }
        })
        
        if mNavWaypoint != nil {
            
            if mNavWaypointOnNetwork == false {
                let locations = [mapView.userLocation.coordinate, mNavWaypoint!.coordinate]
                mNavWaypointLine = MKPolyline(coordinates: locations, count: locations.count)
                
                StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.addOverlay(self.mNavWaypointLine!) })
                //			mapView.addOverlay(mNavWaypointLine!, level: MKOverlayLevel.aboveLabels)
                
            } else {
                // find starting point on network, i.e. nearest point on network from user.
                
                var startingLocation = networkOverlay?.getNearestLocationOnNetwork(location: mapView.userLocation.coordinate)
                
                // draw lines

                if mNavWaypoint != nil {
                    let locations = [mapView.userLocation.coordinate, startingLocation ?? mapView.userLocation.coordinate, mNavWaypoint!.coordinate]
                    mNavWaypointLine = MKPolyline(coordinates: locations, count: locations.count)
                    StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.addOverlay(self.mNavWaypointLine!) })
                }
            }
        }
        
        mapView.setNeedsDisplay()
        
    }
    
    
    
    func loadWaypointFiles(){
        
        //		DispatchQueue.global().async{
        var DBFiles = [URL]()
        
        do { // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory(at: getDKW2Directory(), includingPropertiesForKeys: nil, options: [])
            
            DBFiles = directoryContents.filter{ $0.pathExtension == "db" }
            dout(.wp, "DB urls:", DBFiles)
            //			let fileNames = files.map{ $0.deletingPathExtension().lastPathComponent }
            //			dout("list:", fileNames)
        } catch {
            dout(error.localizedDescription)
        }
        
        //			stDB.removeAll()
        
        // remove db's that have been deleted on storage
        var i = 0
        while i < self.stDB.count {
            if !DBFiles.contains(self.stDB[i].fileName!) {
                self.stDB.remove(at: i)
            } else {
                i += 1
            }
        }
        
        StaticQueues.wpAnnotationsLock.runLocked({
            //        StaticQueues.mapviewAnnotationsLock.runLocked({
            
            // check which files on disk are new
            var alreadyLoaded :Bool
            for DBFileOnDisk in DBFiles {
                alreadyLoaded = false
                for db in self.stDB {
                    if db.fileName == DBFileOnDisk {
                        alreadyLoaded = true
                        break
                    }
                }
                
                if !alreadyLoaded {
                    
                    var newDB = StDB(fileName: DBFileOnDisk)
                    
                    for waypoint in newDB.dbWaypoints {
                        
                        let wpLocation = CLLocationCoordinate2D(latitude: waypoint.getLatitude(), longitude: waypoint.getLongitude())
                        
                        let wpAnnotation = StDBWaypointAnnotation()
                        wpAnnotation.coordinate = wpLocation
                        wpAnnotation.title = waypoint.name
                        //dout("Waypoint :",waypoint.name)
                        wpAnnotation.subtitle = waypoint.subName
                        
                        wpAnnotation.waypoint = waypoint
                        
                        self.wpAnnotations.append(wpAnnotation)
                        //if(mapView.annotationVisibleRect.contains(wpAnnotation.accessibilityActivationPoint)){
                        //	mapView.addAnnotation(wpAnnotation)
                        //}
                    }
                    
                    self.stDB.append(newDB)
                    
                }
            }
            
            self.wpAnnotations.sort(by: {
                $1.coordinate.latitude > $0.coordinate.latitude
            })
        })
        
        //			DispatchQueue.main.async {
        //		self.updateWaypoints(mapView: self.mapView)
        self.updateWaypoints()
        //			}
        //		}
    }
    
    func mapViewSetup(){
        //		int zoomLevel = StSettingsConstants.settings().getMapViewZoomLevel();// mPrefs.getInt("zoomLevel", 12);
        //		float centerX;
        //		float centerY;
        //
        //		// Create settings and get tile size percentage
        //		StDKW2Settings DKWSettings = StDKW2Settings.getSettings(getApplicationContext());
        //
        //		float TileCachePercentage = DKWSettings.getTileCachePercentage();
        //		ActivityManager am = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        //		mTotalAppHeap = am.getMemoryClass() * 1024 * 1024;
        
        // Get and initialize act manager
        let ActM :StActManager = StActManager.getActManager()
        ActM.initialize()
        
        //		// Create chart manager and load charts
        //
        //		// Create and/or get the chart settings database
        //		mChartDB = new StDKW2ChartDatabaseHelper(this);
        
        DKW2ChartManager.refreshCharts()
    }
    
    //	static func refreshCharts(){
    //		let pid = Resources.pid
    //		let mod = Resources.module
    //
    //		//		SharedPreferences chartPrefs = getSharedPreferences(StDKW2ChartManager.CHARTMANAGER_SHAREDPREFS, MODE_PRIVATE);
    //		var prefs = UserDefaults.standard
    //		var PidShow = prefs.integer(forKey: DKW2ChartManager.CHARTMANAGER_PIDTOSHOW)
    //		var ModShow = prefs.integer(forKey: DKW2ChartManager.CHARTMANAGER_MODTOSHOW)
    //
    //		//		StDKW2ChartManager ChartManager = StDKW2ChartManager.getChartManagerRefresh(mChartDB, mUIHandler, pid, mod, PidShow, ModShow, false);
    //		var chartManager = DKW2ChartManager.getChartManagerRefresh(aPid: pid, aMod: mod, aPidShow: PidShow, aModShow: ModShow, aSaveVisibility: false)
    //	}
    //
    
    
    //	@objc func singleTapAction() {
    //		//		mapView.setUserTrackingMode( MKUserTrackingMode.follow, animated: true)
    //		dout("singleTapAction()")
    //	}
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
            || CLLocationManager.authorizationStatus() == .authorizedAlways {
                mapView.showsUserLocation = true
            //			centerMapOnLocation(locationManager.location!, map: mapView, radius: regionRadius)
        } else {
            locationManager.requestAlwaysAuthorization() //requestWhenInUseAuthorization()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        dout("didReceiveMemoryWarning() TODO: Flush Cache")
        // Dispose of any resources that can be recreated.
    }
    
    func waypointSetScaleAndRotation(_ wpView :MKAnnotationView, _ wpAnnotation :StDBWaypointAnnotation, zoom: Double){
        
        DispatchQueue.main.async{
            
            var scale = CGFloat(wpAnnotation.waypoint.drawScale)
            
            if zoom > 0 {
                scale = CGFloat(wpAnnotation.waypoint.getScale(zoomLevel: Float(zoom)))
            }
            
            // Marekrites vallen snel weg, dus die maar even wat groter
            if wpAnnotation.waypoint.type?.name == "Marrekrite aanlegplaats" {
                scale *= 1.8
            }
            
            var yMax :Int = 0
            var markerWidth :CGFloat = 0
            var markerHeight :CGFloat = 0
            var showWP = false
            if let imageView = (wpView as? CustomAnnotationView)?.imageView {
                // wpView.subviews[0] is the waypoint icon
                
                var rotationCorrection :Float = 0
                if wpAnnotation.waypoint!.type!.fixedToChart {
                    rotationCorrection = Float(self.mapRotation) * Float.pi/180
                }
                
                //			imageView.frame.size = CGSize(width: imageView.image!.size.width * scale, height: imageView.image!.size.height * scale)
                //			imageView.frame.origin.x = -imageView.frame.size.width / 2
                //			imageView.frame.origin.y = -imageView.frame.size.height / 2
                
                var markerWidth :CGFloat = 0
                //			DispatchQueue.main.async{
                markerWidth = imageView.image!.size.width
                
                imageView.transform = CGAffineTransform(translationX: -imageView.image!.size.width / 2, y: -imageView.image!.size.height / 2)
                imageView.transform = imageView.transform.scaledBy(x: scale, y: scale)
                imageView.transform = imageView.transform.rotated(by: CGFloat(wpAnnotation.waypoint.angle - rotationCorrection))
                
                imageView.setNeedsDisplay()
                //			}
                
                markerWidth = imageView.image!.size.width * scale
                markerHeight = imageView.image!.size.height * scale
                
                if markerWidth > ViewController.WP_SIZE_THRESHOLD || markerHeight > ViewController.WP_SIZE_THRESHOLD {
                    showWP = true
                }
            }
            
            
            //		if (wpView.frame.size.width > 5) || (wpView.frame.size.height > 5) {
            //				if scale < 0.1 {
            //					wpView.isHidden = false
            //					wpView.isEnabled = true
            //				} else {
            //					wpView.isHidden = true
            //					wpView.isEnabled = false
            //				}
            
            if !wpView.isHidden {
                if !showWP { wpView.isHidden = true }
            } else {
                if showWP {	wpView.isHidden = false }
            }
            
            // Scale and toggle textlabels
            
            var labelHeight :Int = 20
            
            //            var showLabel :Bool = false
            
            if let label = (wpView as? CustomAnnotationView)?.textLabel {
                //		label.frame = CGRect.init(x: 0, y: yMax + 3 , width: Int(label.intrinsicContentSize.width), height: Int(label.intrinsicContentSize.height))
                label.frame = CGRect(x: -200, y: Int(markerHeight / 2) , width: 400, height: labelHeight)
//                label.frame.origin.y = CGFloat(markerHeight / 2)
//                label.frame.size.height = CGFloat(labelHeight)
 

                //		label.frame.origin.x = -label.intrinsicContentSize.width / 2
                //			label.setNeedsDisplay()
                //		(wpView.subviews[0] as! UILabel).frame = CGRect.init(x: 0, y: yMax + 3 , width: 400, height: labelHeight)
                
                var newFontSize = min(ViewController.WP_TEXT_SIZE_DEF * scale, ViewController.WP_TEXT_SIZE_DEF)
                if newFontSize >= ViewController.WP_TEXT_SIZE_MIN {
                    label.font = label.font.withSize(newFontSize)
                    wpAnnotation.textTooSmall = false
                    if label.isHidden && self.labelIsEnabled(wpAnnotation) {
                        label.isHidden = false
                    }
                    label.sizeToFit()
                    label.frame.size.width = 400
                    
                    // Center text label in case of road labels
                    if wpAnnotation.waypoint?.type?.name == "Dutch N road"
                        || wpAnnotation.waypoint?.type?.name == "Dutch A road"
                        || wpAnnotation.waypoint?.type?.name == "Dutch S road"
                        || wpAnnotation.waypoint?.type?.name == "European E road" {
                        label.frame.origin.y = CGFloat(-label.frame.size.height / 2)
    //                    label.frame = CGRect(x: -200, y: -Int(labelHeight / 2) , width: 400, height: labelHeight)

                    }
                } else {
                    wpAnnotation.textTooSmall = true
                    if !label.isHidden {
                        label.isHidden = true
                    }
                }
            }
            //		DispatchQueue.main.async{
            //unhide wpView
            wpView.isHidden = false
            wpView.setNeedsDisplay()
            //		}
        }
    }
    
    func setLabelVisibility(_ annotation :StDBWaypointAnnotation){
        
        if let annotationView = mapView.view(for: annotation){
            //				if(mSettingsShowWaypointNames){//} || annotation!.waypoint?.type?.name == "City"){
            if labelIsEnabled(annotation) {
                if !annotation.textTooSmall {
                    annotationView.subviews[1].isHidden = false
                }
                
                if annotation.waypoint?.type?.name == "Brug",
                   let cAView = annotationView as? CustomAnnotationView {
                    setBridgeLabel(cAView, annotation: annotation)
                }
                
            } else {
                annotationView.subviews[1].isHidden = true
            }
            annotationView.setNeedsDisplay()
        }
        
    }
    
    func labelIsEnabled(_ annotation :StDBWaypointAnnotation) -> Bool {
        return mSettingsShowWaypointNames
            || annotation.waypoint?.type?.name == "City"
            || annotation.waypoint?.type?.name == "Vaarwegdiepte DM"
            || annotation.waypoint?.type?.name == "Brug"
            || annotation.waypoint?.type?.name == "Dutch N road"
            || annotation.waypoint?.type?.name == "Dutch A road"
            || annotation.waypoint?.type?.name == "Dutch S road"
            || annotation.waypoint?.type?.name == "European E road"
    }
    
    func updateWaypoints(){
        updateWaypoints(mapRectGeo: getMapRectGeo())
    }
    
    func updateWaypoints(mapRectGeo :CGRect){
        
        // When disabled, remove waypoints from view one by one
        if !self.mSettingsShowWaypoints {
            StaticQueues.wpAnnotationsLock.runLocked({
                for annotation in self.wpAnnotationsOnMapview {
                    StaticQueues.mapviewAnnotationsLock.runLocked({
                        self.mapView.removeAnnotation(annotation)
                        annotation.inPreviousView = false
                    })
                }
                self.wpAnnotationsOnMapview.removeAll()
            })
            return
        }
        
        let z = Float(self.mapView.getZoom())
        
        var scale :CGFloat = 1
        //		let mapRegion :MKCoordinateRegion = mapView.region
        //		var west = mapRegion.center.longitude - (mapRegion.span.longitudeDelta / 2)
        //		if west < -180 { west += 360 }
        //		let south = mapRegion.center.latitude - (mapRegion.span.latitudeDelta / 2)
        //		let mapRectGeo :CGRect = CGRect(
        //			x: west,
        //			y: south,
        //			width: mapRegion.span.longitudeDelta,
        //			height: mapRegion.span.latitudeDelta)
        
        //        self.annotationsCopy = self.mapView.annotations // *** -[__NSSetM addObject:]: object cannot be nil
        //        self.annotationsCopy = [MKAnnotation]()
        //        for annotation in self.mapView.annotations {
        //            annotationsCopy.append(annotation)
        //        }
        
        // Code with annotationsCopy has crashes
        //        var annotationsCopy = [MKAnnotation?]()
        //            self.annotationsCopy = self.mapView.annotations.map{ $0 } // "object cannot be nil"
        
        //            for a in self.mapView.annotations { if a != nil { annotationsCopy.append(a) } } //*** -[__NSSetM addObject:]: object cannot be nil'
        //            if self.mapView != nil
        //                && self.mapView.annotations != nil {
        
        //            if let testMapview = self.mapView {
        //
        //                if let testAnnotations = testMapview.annotations {
        //
        //                    for a in testAnnotations {
        //                        if a != nil {
        //                            let testA = a
        //                            var testAnnotationsCopy = annotationsCopy
        //                            testAnnotationsCopy.append(testA)
        //                        }
        //                    }
        //                }
        //            }
        //            }
        
        //        do {
        //            //                try for a in self.mapView.annotations { if a != nil { annotationsCopy.append(a) } } //*** -[__NSSetM addObject:]: object cannot be nil'
        //            //                try annotationsCopy = self.mapView.annotations.map{ $0 }
        //
        //            try StaticQueues.mapviewAnnotationsLock.runLocked({
        //
        //                if let testMapview = self.mapView {
        //
        //                    let testMapViewIsNil = testMapview == nil
        //                    dout("testMapViewIsNil: \(testMapViewIsNil)")
        ////                    let testRectAnnotations :[MKAnnotation] = testMapview.annotations(in: MKMapRect(origin: testMapview.annotationVisibleRect.origin, size: testMapview.annotationVisibleRect.size)) as! [MKAnnotation]
        //
        //                    withUnsafePointer(to: testMapview.annotations) { //*** -[__NSSetM addObject:]: object cannot be nil'
        //                        //                    print("\($0)")
        //                        dout("testMapview.annotations has address: \($0)")
        //                    }
        //
        //                    let testMapViewAnnotationsIsNil = testMapview.annotations == nil //*** -[__NSSetM addObject:]: object cannot be nil'
        //
        //                    let testAnnotations = testMapview.annotations //*** -[__NSSetM addObject:]: object cannot be nil'
        //
        //                    for a in testAnnotations {
        //                        if a != nil {
        //                            let testA = a
        //                            var testAnnotationsCopy = annotationsCopy
        //                            testAnnotationsCopy.append(testA)
        //                        }
        //                    }
        //                }
        //
        //            })
        //
        //        } catch let error {
        //            print("EXCEPTION in updateWaypoints (line 1408). Specified error: \(error)")
        //        } catch {
        //            print("EXCEPTION in updateWaypoints (line 1410), not specified")
        //        }
        
        
        //        self.annotationsCopy = annotationsCopy
        
        StaticQueues.wpAnnotationsBGQueue.async {  //*** -[__NSSetM addObject:]: object cannot be nil' //////////////////////////
            StaticQueues.wpAnnotationsLock.runLocked({ // lock around self.wpAnnotations so it only runs after wp loading has finished
                //                StaticQueues.mapviewAnnotationsLock.runLocked({
                
                //				var annotationsCopy = [MKAnnotation]()
                //				DispatchQueue.main.async{ annotationsCopy = self.mapView.annotations }
                
                StaticQueues.mapviewAnnotationsLock.runLocked({
                    var previousWPsOLD :[StDBWaypointAnnotation?] = self.mapView.annotations.filter({$0 is StDBWaypointAnnotation}).map({$0 as! StDBWaypointAnnotation})
                    dout("Waypoints in mapview: \(previousWPsOLD.count)")
                })
                
                let previousWPs = self.wpAnnotationsOnMapview.map({$0}) // make a shallow copy
                //                  self.wpAnnotationsOnMapview = [StDBWaypointAnnotation]() // reset and fill again during this refresh.
                self.wpAnnotationsOnMapview.removeAll() // reset and fill again during this refresh.
                
                for wp in previousWPs {
                    wp.inPreviousView = true
                    wp.inNewView = false // initially set to false, might change later
                }
                
                dout(.wp, "Reloading waypoints to current mapview")
                
                let updateWaypointsTimestamp1 = Date()
                
                var newAnnotations = [MKAnnotation]()
                
                // The annotations list is ordered from north to south, so find the range in
                var startI = StUtils.findIbyV(arr: self.wpAnnotations, v: Double(mapRectGeo.minY))
                let endI = StUtils.findIbyV(arr: self.wpAnnotations, v: Double(mapRectGeo.maxY))
                //					for i in 0 ..< self.wpAnnotations.count {
                if startI != -1 && endI != -1 { // if either of these == -1, the marRect is outside of all wpAnnotations
                    for i in startI ... endI {
                        
                        if newAnnotations.count > 3000 {
                            break
                        }
                        
                        var annotation = self.wpAnnotations[i]
                        annotation.inNewView = false // initially set to false, might change later
                        
                        self.annotationVisibleGlobalBool = false
                        
                        self.annotationVisibleGlobalBool = annotation.coordinate.longitude > Double(mapRectGeo.origin.x) && annotation.coordinate.longitude < Double(mapRectGeo.origin.x + mapRectGeo.width)
                        if !self.annotationVisibleGlobalBool,
                           mapRectGeo.maxX > 180 {
                            self.annotationVisibleGlobalBool = (annotation.coordinate.longitude + 360) > Double(mapRectGeo.origin.x) && (annotation.coordinate.longitude + 360) < Double(mapRectGeo.origin.x + mapRectGeo.width)
                        }
                        
                        if self.annotationVisibleGlobalBool,
                           DKW2ChartManager.getChartManager().inChartsRegion(aGPDeg: Geo2(annotation.coordinate)){
                            
                            if z > 0 {
                                scale = CGFloat(annotation.waypoint.getScale(zoomLevel: z))
                            }
                            //				if(CGFloat(annotation.waypoint!.type!.width) * scale > 5 || CGFloat(annotation.waypoint!.type!.height) * scale > 5) {
                            if let wp = annotation.waypoint,
                               let type = wp.type,
                               CGFloat(type.width) * scale > ViewController.WP_SIZE_THRESHOLD || CGFloat(type.height) * scale > ViewController.WP_SIZE_THRESHOLD
                            {
                                
                                if annotation.inPreviousView {
                                    StaticQueues.mapviewAnnotationsLock.runLocked({
                                        if let wpView = self.mapView.view(for: annotation){
                                            self.waypointSetScaleAndRotation(wpView, annotation, zoom: Double(z))
                                        }
                                    })
                                } else {
                                    newAnnotations.append(annotation)
                                    annotation.inPreviousView = true
                                }
                                annotation.inNewView = true
                                
                                // this is supposed to be an exact copy of mapView.annotations after this refresh.
                                self.wpAnnotationsOnMapview.append(annotation)
                            }
                        }
                    }
                }
                
                for wp in previousWPs {
                    wp.inPreviousView = false // make sure this value is reset voor all waypoint annotations
                }
                
                DispatchQueue.main.async{
                    StaticQueues.mapviewAnnotationsLock.runLocked({
                        self.mapView.removeAnnotations(previousWPs.filter({ !$0.inNewView }).map{$0})
                        self.mapView.addAnnotations(newAnnotations)
                        self.mapView.setNeedsDisplay()
                        
                        //                        dout("Waypoints in mapview: \(previousWPsOLD.count)")
                        //                        dout("Count of removing annotations: \(previousWPs.filter({ !$0.inNewView }).count)")
                        //                        dout("Count of adding annotations: \(newAnnotations.count)")
                        //                        dout("Count of mapview annotations: \(self.mapView.annotations.filter({$0 is StDBWaypointAnnotation}).count)")
                        //                        dout("Count of wpAnnotationsOnMapview: \(self.wpAnnotationsOnMapview.count)")
                        
                    })
                }
                
                dout("Done - Reloading waypoints to current mapview. time: \(Date().timeIntervalSince(updateWaypointsTimestamp1)) nr: \(newAnnotations.count)")
                //                })
            })
        }
    }
    
    func drawLocationView(toAnnotationView annotationView: MKAnnotationView) {
        
        if CourseLineImageView == nil {
            var scale = CGFloat(0.3)
            let image :UIImage = getCourseLineImage()
            CourseLineImageView = UIImageView(image: image)
            CourseLineImageView!.frame = CGRect(x: (annotationView.frame.size.width - scale * image.size.width)/2, y: (annotationView.frame.size.height - scale * image.size.height)/2, width: scale * image.size.width, height: scale * image.size.height)
            annotationView.addSubview(CourseLineImageView!)
            CourseLineImageView!.isHidden = true
        }
        
        if headingImageView != nil && annotationView.subviews.contains(headingImageView!){
            headingImageView!.removeFromSuperview()
        }
        
//        let image :UIImage = getLocationMarkerImage(mUserLocationColor)
//        headingImageView = UIImageView(image: image)
//        headingImageView!.frame = CGRect(x: (annotationView.frame.size.width - image.size.width)/2, y: (annotationView.frame.size.height - image.size.height)/2, width: image.size.width, height: image.size.height)
        headingImageView = getLocationMarkerImageView(mUserLocationColor)
        annotationView.addSubview(headingImageView!)
        headingImageView!.isHidden = true
        
    }
    
    func getCourseLineImage() -> UIImage {
        //		let color = UIColor.blue
        let height :CGFloat = 2000.0;
        let width :CGFloat = 5.0;
        let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        
        //		color.setFill()
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: width/2, y: height/2)
        context.setLineWidth(2)
        //		context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
        context.setStrokeColor(UIColor.black.cgColor)
        context.move(to: CGPoint(x: 0, y: 0))
        context.addLine(to: CGPoint(x: 0, y: -height/2))
        context.strokePath()
        
        //		context.translateBy(x: 0, y: -10)
        
        //		color.setFill()
        //UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //		guard let cgImage = image?.cgImage else { return nil }
        return UIImage(cgImage: (image?.cgImage)!)
    }
    
    func getLocationMarkerImageView(_ ulColor :UIColor) -> UIImageView {
        
        let headingImageScale :CGFloat = 0.7

        var imageView = UIImageView(image: getLocationMarkerImage(ulColor))
        imageView.frame = CGRect(x: ( -imageView.frame.size.width * headingImageScale)/2, y: (-imageView.frame.size.height * headingImageScale)/2, width: imageView.frame.size.width * headingImageScale, height: imageView.frame.size.height * headingImageScale)
        return imageView
    }
    
    func getLocationMarkerImage(_ ulColor :UIColor) -> UIImage {
        //		let color = UIColor.blue
        
//        let sizeScale :CGFloat = 0.1
        
        let height :CGFloat = 24.0;
        let width :CGFloat = 24.0;
        let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        
        //		color.setFill()
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: width/2, y: 0)
        context.setLineWidth(0.5)
        //		context.setFillColor(mUserLocationColor.cgColor)
        context.setFillColor(ulColor.cgColor)
        context.setStrokeColor(UIColor.black.cgColor)
        
        //context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
        context.move(to: CGPoint(x: 0, y: 2))
        context.addLine(to: CGPoint(x: 8, y: 22))
        //		context.addLine(to: CGPoint(x: 0, y: 16))
        context.addLine(to: CGPoint(x: 0, y: 18))
        context.addLine(to: CGPoint(x: -8, y: 22))
        context.addLine(to: CGPoint(x: 0, y: 2))
        //		context.strokePath()
        //				context.fillPath()
        context.drawPath(using: .fillStroke)
        
        context.translateBy(x: 0, y: -12)
        
//        context.scaleBy(x: sizeScale, y: sizeScale)
        
        //		color.setFill()
        //UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //		guard let cgImage = image?.cgImage else { return nil }
        return UIImage(cgImage: (image?.cgImage)!)
    }
    
    //    func addNavWaypointLine(toAnnotationView annotationView: MKAnnotationView, navWPscreenPoint: CGPoint) {
    //        if NavWaypointLineImageView == nil {
    //            var scale = CGFloat(0.3)
    //            let image :UIImage = getNavWaypointLineImage(navwpX: navWPscreenPoint.x, navwpY: navWPscreenPoint.y)
    //            NavWaypointLineImageView = UIImageView(image: image)
    //            NavWaypointLineImageView!.frame = CGRect(x: (annotationView.frame.size.width - scale * image.size.width)/2, y: (annotationView.frame.size.height - scale * image.size.height)/2, width: scale * image.size.width, height: scale * image.size.height)
    //            annotationView.addSubview(NavWaypointLineImageView!)
    //            NavWaypointLineImageView!.isHidden = true
    //        }
    //    }
    //
    //
    //
    //    func getNavWaypointLineImage(navwpX: CGFloat, navwpY: CGFloat) -> UIImage {
    //        //        let color = UIColor.blue
    //        let height :CGFloat = 2000.0;
    //        let width :CGFloat = 5.0;
    //
    //        let rect = CGRect(origin: .zero, size: CGSize(width: navwpX, height: navwpY))
    //        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
    //
    //        //        color.setFill()
    //
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        context.setLineWidth(1)
    //        //        context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
    //        context.setStrokeColor(UIColor.black.cgColor)
    //        context.move(to: CGPoint(x: 0, y: 0))
    //        context.addLine(to: CGPoint(x: navwpX, y: -navwpY))
    //        context.strokePath()
    //
    //        //        context.translateBy(x: 0, y: -10)
    //
    //        //        color.setFill()
    //        //UIRectFill(rect)
    //        let image = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        //        guard let cgImage = image?.cgImage else { return nil }
    //        return UIImage(cgImage: (image?.cgImage)!)
    //    }
    
    func updateLocationCursorRotation(forceUsercursorNorthWhenNeeded :Bool) {
        
        //		if let heading = userHeading,
        //			let headingImageView = headingImageView {
        //		if let course = userCourse,
        
        var rotationRelativeToDisplay :CLLocationDirection
        if courseUp && mFollowing && forceUsercursorNorthWhenNeeded {
            rotationRelativeToDisplay = 0
        } else {
            rotationRelativeToDisplay = userCourse - self.mapRotation
        }
        
        if let headingImageView = headingImageView {
            
            headingImageView.isHidden = false
            let rotation = CGFloat( (rotationRelativeToDisplay) / 180 * Double.pi)
            headingImageView.transform = CGAffineTransform(rotationAngle: rotation)
        }
        
        if let courseLineImageView = CourseLineImageView {
            
            courseLineImageView.isHidden = false
            let rotation = CGFloat( (rotationRelativeToDisplay) / 180 * Double.pi)
            courseLineImageView.transform = CGAffineTransform(rotationAngle: rotation)
        }
    }
    
    func updateInstrumentInfo() {
        
        cogInfoLabel.text = "\(String(format: "%.0f", userCourse))°"
        
        sogInfoLabel.text = StUtils.formatSpeed(userSpeed) ?? ""
        
    }
    
    @IBAction func mainMenuButton(_ sender: Any) {
        stopEditMode()
        
        chartManagerButton.isSelected = false
        mainMenu.isSelected = true
        
    }
    
    @IBAction func chartManagerButton(_ sender: Any) {
        stopEditMode()
        
        chartManagerButton.isSelected = true
        mainMenu.isSelected = false
        
    }
    
    @IBAction func centerButton(_ sender: Any) {
        
        dout("Centreer knop")
        
        if(!mFollowing){
            centreer.isSelected = true
            mFollowing = true
            
            //			let currentRegion = MKCoordinateRegion(center: (userLocation?.coordinate)!, span: mapView.region.span)
            //			mapView.setRegion(currentRegion, animated: false)
            
            
            // Tracking has been moved to locationManager -> didUpdateLocations //
            //            mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
            
            // set location
            
            if self.courseUp {
                mapView.camera.heading = userCourse
            }
            if let coordinate = userLocation?.coordinate {
                mapView.camera.centerCoordinate = coordinate
                mapView.setCamera(mapView.camera, animated: true)
            }
            
        } else {
            centreer.isSelected = false
            mFollowing = false
            //            mapView.setUserTrackingMode(MKUserTrackingMode.none, animated: true)
            
        }
        
        //		//		let span = MKCoordinateSpanMake(0.3, 0.3)
        //		//		let region : MKCoordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, span: span)
        //		//		mapView.setRegion(region, animated: true)
        //		if(courseUp) {
        //			courseUp = false
        //		} else {
        //			courseUp = true
        //		}
        
        //mapView.isUserInteractionEnabled = false
        //		//		mainToolbar.barTintColor = UIColor.darkGray
        
        //		let chartCheckTask = StChartCheckTask()
        
        //		showLoginDialog()
        
        //		let sessionIdentifier :String = "ChartDownload testingUrl"
        //		var urlSession: URLSession = {
        //			let config = URLSessionConfiguration.background(withIdentifier: sessionIdentifier)
        //			config.isDiscretionary = true
        //			config.sessionSendsLaunchEvents = true
        //			return URLSession(configuration: config, delegate: self, delegateQueue: nil)
        //		}()
        //
        //		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/bu353/GpsInfo.exe")!
        //
        //		let backgroundTask = urlSession.downloadTask(with: testingUrl) // OR URLRequest instance
        //		//			backgroundTask.earliestBeginDate = Date().addingTimeInterval(60 * 60)
        //		//			backgroundTask.countOfBytesClientExpectsToSend = 200
        //		//			backgroundTask.countOfBytesClientExpectsToReceive = 500 * 1024
        //		backgroundTask.resume()
        
    }
    
    //	func testDownload(){
    //		let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
    //
    //		//		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/bu353/GpsInfo.exe")!
    //		let testingUrl :URL = URL(string: "https://www.stentec.com/anonftp/pub/wingps/wingps5litesetup.exe")!
    //
    //		let task = urlSession.downloadTask(with: testingUrl)
    //
    //		task.resume()
    //	}
    
    func showColorPicker(sender: UIView){
        
        let colorSelectionController = EFColorSelectionViewController()
        
        navCtrlColor = UINavigationController(rootViewController: colorSelectionController)
        navCtrlColor!.navigationBar.backgroundColor = UIColor.white
        navCtrlColor!.navigationBar.isTranslucent = false
        navCtrlColor!.isModalInPopover = true
        navCtrlColor!.modalPresentationStyle = UIModalPresentationStyle.popover
        navCtrlColor!.popoverPresentationController?.delegate = self
        
        navCtrlColor!.popoverPresentationController?.sourceView = mapView
        
        let xOffset : CGFloat = 10
        let yOffset : CGFloat = 0
        //navCtrlColor!.popoverPresentationController?.sourceRect = mLocationView!.frame.offsetBy(dx: xOffset, dy: yOffset)
        navCtrlColor!.popoverPresentationController?.sourceRect = mLocationView!.frame.insetBy(dx: -20, dy: -20)
        navCtrlColor!.preferredContentSize = colorSelectionController.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        //navCtrlColor!.preferredContentSize = CGSize(width: 400, height: 500)
        colorSelectionController.delegate = self
        colorSelectionController.color = mUserLocationColor
        
        //if UIUserInterfaceSizeClass.compact == self.traitCollection.horizontalSizeClass {
        let doneBtn: UIBarButtonItem = UIBarButtonItem(
            title: NSLocalizedString("Done", comment: ""),
            style: UIBarButtonItem.Style.done,
            target: self,
            action: #selector(dismissColorPicker(sender:))
        )
        colorSelectionController.navigationItem.rightBarButtonItem = doneBtn
        //}
        self.present(navCtrlColor!, animated: true, completion: nil)
    }
    
    @objc func dismissColorPicker(sender: UIView){
        
        if(mLocationView != nil) {
            drawLocationView(toAnnotationView: mLocationView!)
        }
        navCtrlColor?.dismiss(animated: true, completion: nil)
    }
    
    //	func showModalMenu(sender: UIView){
    //
    //		let colorSelectionController = EFColorSelectionViewController()
    //
    //		navCtrlColor = UINavigationController(rootViewController: colorSelectionController)
    //		navCtrlColor!.navigationBar.backgroundColor = UIColor.white
    //		navCtrlColor!.navigationBar.isTranslucent = false
    //		navCtrlColor!.isModalInPopover = true
    //		navCtrlColor!.modalPresentationStyle = UIModalPresentationStyle.popover
    //		navCtrlColor!.popoverPresentationController?.delegate = self
    //
    //		navCtrlColor!.popoverPresentationController?.sourceView = mapView
    //
    //		let xOffset : CGFloat = 10
    //		let yOffset : CGFloat = 0
    //		//navCtrlColor!.popoverPresentationController?.sourceRect = mLocationView!.frame.offsetBy(dx: xOffset, dy: yOffset)
    //		navCtrlColor!.popoverPresentationController?.sourceRect = mLocationView!.frame.insetBy(dx: -20, dy: -20)
    //		navCtrlColor!.preferredContentSize = colorSelectionController.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    //		//navCtrlColor!.preferredContentSize = CGSize(width: 400, height: 500)
    //		colorSelectionController.delegate = self
    //		colorSelectionController.color = mUserLocationColor
    //
    //		//if UIUserInterfaceSizeClass.compact == self.traitCollection.horizontalSizeClass {
    //		let doneBtn: UIBarButtonItem = UIBarButtonItem(
    //			title: NSLocalizedString("Done", comment: ""),
    //			style: UIBarButtonItem.Style.done,
    //			target: self,
    //			action: #selector(dismissColorPicker(sender:))
    //		)
    //		colorSelectionController.navigationItem.rightBarButtonItem = doneBtn
    //		//}
    //		self.present(navCtrlColor!, animated: true, completion: nil)
    //	}
    
    @IBAction func toolbarMenuButton(_ sender: Any) {
        dout("Menu knop")
        
        //		mainToolbar.barTintColor = UIColor.clear
        //		mainToolbar.isTranslucent = true
        
        let alertController = UIAlertController(title: "Main menu", message: "", preferredStyle: .alert)
        let chartAction = UIAlertAction(title: NSLocalizedString("Chart Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            //Add your code
        })
        let waypointAction = UIAlertAction(title: NSLocalizedString("Waypoint Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            //Your code
        })
        let routeAction = UIAlertAction(title: NSLocalizedString("Route Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            //Your code
        })
        let trackAction = UIAlertAction(title: NSLocalizedString("Track Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            dout("cancel action")
        })
        let cancelAction = UIAlertAction(title: NSLocalizedString("Track Manager", comment: ""), style: .default, handler: {(action: UIAlertAction) -> Void in
            dout("cancel action")
        })
        alertController.addAction(chartAction)
        alertController.addAction(waypointAction)
        alertController.addAction(routeAction)
        alertController.addAction(trackAction)
        alertController.preferredAction = chartAction
        
        
        //		self.presentFromMain(alertController, animated: true, completion: nil)
        self.presentFromMain(alertController, animated: true) {
            alertController.view.superview?.isUserInteractionEnabled = true
            alertController.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
        }
    }
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func startChartCheck(){
        
        let _ = StatusCheck()
        
        //		// for testing
        //		let userPopke = "paltenburg@gmail.com"
        //		let pwPopke = "1oposse1"
        //
        //		statusCheck.mActDB.setUserData(aEmail: userPopke, aPassword: pwPopke)
        //		//mChartCheckState = EChartCheckState.StCheckPem;
        //		statusCheck.checkCharts(true)
        
    }
    
    @IBAction func onReturningToMapViewFromChartManager(_ segue: UIStoryboardSegue) {
        // refresh mapview
        
        // from:	MainActivity:onActivityResult -> RequestCode == AppConstants.ACTIVITY_CHARTMANAGER
        chartManagerButton.isSelected = false
        //		DKW2ChartManager.refreshCharts() //// not needed, as charts are refreshed directly with every toggle
        
    }
    
    class ConstantWidthPolylineRenderer: MKPolylineRenderer {
        
        override func applyStrokeProperties(to context: CGContext, atZoomScale zoomScale: MKZoomScale) {
            super.applyStrokeProperties(to: context, atZoomScale: zoomScale)
            UIGraphicsPushContext(context)
            if let ctx = UIGraphicsGetCurrentContext() {
                if(zoomScale==0){
                    
                } else {
                    ctx.setLineWidth(self.lineWidth/zoomScale)
                }
                
            }
        }
    }
    
    func updateNavpointInfo() {
        // Update nav waypoint info CTS/DTG
        if let navWaypointLocation = mNavWaypoint?.getCLLocation(),
           let userLocation = self.userLocation {
            var courseToStear = getBearingBetweenTwoPoints1(point1: userLocation, point2: navWaypointLocation)// CLLocation(latitude: (mNavWaypoint?.coordinate.latitude)!, longitude: (mNavWaypoint?.coordinate.longitude)!))
            if(courseToStear < 0){
                courseToStear += 360
            }
            ctsInfoLabel.text = "\(String(format: "%.0f", courseToStear))°"
            
            var distanceToGoInM :Double = 0
            // check if a route is followed, or a navigation waypoint
            if let activeRP = Routes.service.activeRoutePoint {
                
                // Navigating to route point, so calculate total distance
                var totalDistance = Double(userLocation.distance(coordinate: activeRP.position))
                var currentRP :RoutePoint = activeRP
                while true {
                    currentRP.setDistance(totalDistance)
                    if let nextRP = currentRP.next {
                        totalDistance += currentRP.position.distance(coordinate: nextRP.position)
                        
                        nextRP.setDistance(totalDistance)
                        
                        currentRP = nextRP
                    } else {
                        break
                    }
                }
                distanceToGoInM = totalDistance
                
                
            } else {
                // A regular navigation point
                distanceToGoInM = Double(userLocation.distance(from: navWaypointLocation))
            }
            
            dtgInfoLabel.text = StUtils.formatDistance(distanceToGoInM)
        }
    }
    
    func showTailTrack(){
        if let track = TrackingService.service.runningTrack {
            
            //			mTailTrackPolyline = track.getNewTrackPolyline()
            //			mapView.addOverlay(mTailTrackPolyline!)
            //            track.addOverLaysTo(mapView)
            trackOverlay?.setOverlays(for: track)
            mapView.setNeedsDisplay()
        }
    }
    
    func updateRunningTrack(){
        dout("updateTailTrack")
        //		if let trackLine = mTailTrackPolyline {
        //			mapView.removeOverlay(trackLine)
        //		}
        //
        //		if let track = TrackingService.service.runningTrack {
        //			mTailTrackPolyline = track.getNewTrackPolyline()
        //			mapView.addOverlay(mTailTrackPolyline!)
        //		}
        //		mapView.setNeedsDisplay()
        
        if let track = TrackingService.service.runningTrack {
            
            //            track.removeOverlaysFrom(mapView)
            //            track.addOverLaysTo(mapView)
            trackOverlay?.setOverlays(for: track)
            
            mapView?.setNeedsDisplay()
        }
    }
    
    func hideTailTrack(){
        //		if let trackLine = mTailTrackPolyline {
        //			mapView.removeOverlay(trackLine)
        //
        //			mapView.setNeedsDisplay()
        //			mTailTrackPolyline = nil
        //		}
        
        if let track = TrackingService.service.runningTrack {
            //            track.removeOverlaysFrom(mapView)
            trackOverlay?.setOverlays(for: track)
            mapView.setNeedsDisplay()
            
        }
    }
    
    func setTrackOverlayVisibillity(_ visible :Bool){
        trackOverlay?.setLayerVisibility(visible)
        mapView.setNeedsDisplay()
    }
    
    func drawDistanceCircles(){
        
        // toggle distanceCircles
        
        if mSettingsDistanceCirclesEnabled,
           mSettingsDistanceCircleCount >= 1,
           self.mapView.getZoom() > distanceCirclesZoomThreshold {
            guard let userLocation = self.userLocation else { return }
            
            let referenceToOldDistCircleViews = distanceCircleViews
            distanceCircleViews = [MKCircle]()
            for i in 1 ... mSettingsDistanceCircleCount {
                distanceCircleViews.append(MKCircle(center: userLocation.coordinate,
                                                    radius: CLLocationDistance(mSettingsDistanceCircleDistance * i)))
            }
            
            StaticQueues.mapviewOverlaysLock.runLocked({
                self.mapView.removeOverlays(referenceToOldDistCircleViews)
                self.mapView.addOverlays(self.distanceCircleViews)
            })
            mapView.setNeedsDisplay()
        } else {
            StaticQueues.mapviewOverlaysLock.runLocked({
                self.mapView.removeOverlays(self.distanceCircleViews)
            })
            mapView.setNeedsDisplay()
        }
        
    }
    
    
    func exitButtonPressed(){
        
        // Stop all processes
        
        // pause tracking
        // save track somehow
        stopEditMode()
        
//        locationManager.stopUpdatingLocation()
        locationManagerDisableUpdating()
        
        if mTrackOverlayEnabled {
            TrackingService.service.pause()
        }
        
        //// TODO: Stop route tracking
        
        // blur over de achtergrond
        enableMapviewBlur()
        
        self.performSegue(withIdentifier: "splashScreenSegue", sender: self)
    }
    
    
    
    
    func continueIfFromExit(){
        
        removeMapviewBlur()
        
        // This might be an unneccesary call, it does nothing if it's already updating.
//        locationManager.startUpdatingLocation()
        locationManagerEnableUpdating()
        
        //        if showTailTrack {
        //            TrackingService.service.start()
        //        }
    }
    
    // blur background
    
    // blur over de achtergrond
    func enableMapviewBlur(){
        if let mainView = mapView?.superview {
            
            if mapViewBlurLayerView == nil {
                mapViewBlurLayerView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
                mapViewBlurLayerView!.frame = mainView.bounds
                mapViewBlurLayerView!.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                //            mapViewBlurLayerView!.layer.zPosition = -1
            }
            mainView.addSubview(mapViewBlurLayerView!)
            mainView.setNeedsDisplay()
        }
    }
    
    // remove blur in background
    func removeMapviewBlur(){
        if let blurLayer = mapViewBlurLayerView {
            blurLayer.removeFromSuperview()
        }
    }
    
    // Appdelegate methods around lifecycle methods
    
    func didEnterBackground(){
        
        stopEditMode()
        
        locationManager.distanceFilter = TrackingService.service.distanceFilterValue
        //		locationManager.stopUpdatingLocation()
        //		locationManager.startUpdatingLocation()
        
        // save mapview
        storeMapPosition()
    }
    
    func willEnterForeground(){
        locationManager.distanceFilter = kCLDistanceFilterNone
        
        //		locationManager.startUpdatingLocation()
        //		locationManager.stopUpdatingLocation()
        //		locationManager.startUpdatingLocation()
        
        var desiredAccuracy: CLLocationAccuracy
        
    }
    
    func willTerminate(){
        stopEditMode()
        
        storeMapPosition()
    }
    
    func storeMapPosition(){
        // loading map view that the user last turned the screen off with
        let mapRect = mapView.visibleMapRect
        
        let settings = UserDefaults.standard
        settings.set(mapRect.origin.x, forKey: Resources.STORED_MAP_POSITION_RECT_X)
        settings.set(mapRect.origin.y, forKey: Resources.STORED_MAP_POSITION_RECT_Y)
        settings.set(mapRect.size.width, forKey: Resources.STORED_MAP_POSITION_RECT_WIDTH)
        settings.set(mapRect.size.height, forKey: Resources.STORED_MAP_POSITION_RECT_HEIGHT)
        
        let storedMapPosY :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_Y)) as? Double
        let storedMapPosWidth :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_WIDTH)) as? Double
        let storedMapPosHeight :Double? = (settings.object(forKey: Resources.STORED_MAP_POSITION_RECT_HEIGHT)) as? Double
        
    }
    
    
    func stopEditMode(){
        
        //		if (dm.getWptEditMode()) {
        //			dm.setWptEditMode(false);
        //			updateWptEdit(null);
        //			mStWaypointsOverlay.setEditMode(false, null);
        //		} else
        
        if let editRoute = Routes.service.getEditRoute() {
            Routes.service.stopRouteEdit()
            routeOverlay?.setEditRoute(route: nil, RPEdited: nil)
            waypointTouchEnabled = true
        }
        
        stopEditingRouteButtonView.isHidden = true
        //		mMapView.invalidate();
    }
    
    func removeNavwaypoint(){
        
        if mNavWaypoint != nil {
            StaticQueues.mapviewAnnotationsLock.runLocked({ self.mapView.removeAnnotation(self.mNavWaypoint!) })
            mNavWaypoint = nil
        }
        
        if let wpLine = mNavWaypointLine {
            StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.removeOverlay(wpLine) })
        }
        
        navWaypointInfoView.isHidden = true
    }
    
    
    func createNewRoute(_ location :CLLocationCoordinate2D){
        // todo: look up section in network
        
        let route = Routes.service.createNewRoute(location: location)
        
        route.setEdit(true)
        
        if !mRouteOverlayEnabled {
            // todo: if routes are not enabled: set all routes to visible
        }
        
        // disable touch of waypointoverlay
        waypointTouchEnabled = false
        
        // disable edit mode of waypointoverlay
        
        // set editmode of routeoverlay to this route
        routeOverlay?.setEditRoute(route: route, RPEdited: route.routePoints[0])
        
        // disable follow/centermode
        stopEditingRouteButtonView.isHidden = false
        
        // update mapview
    }
    
    func createNewRouteInCenter(){
        let location :CLLocationCoordinate2D = mapView.convert(CGPoint(x: mapView.frame.width/2, y: mapView.frame.height/2), toCoordinateFrom: mapView)
        createNewRoute(location)
    }
    
    func moveMapTo(coord :CLLocationCoordinate2D, animated :Bool){
        
        let currentRegion = MKCoordinateRegion(center: coord, span: mapView.region.span)
        mapView.setRegion(currentRegion, animated: animated)
    }
    
    //MARK: AIS target info panel
    
    @IBOutlet weak var aisTargetInfoSecondsSinceUpdate: UILabel!
    var selectedAisObject :AisObject?
    
    @IBAction func aisTargetInfoFindButtonPressed(_ sender: Any) {
        if let aisObject = selectedAisObject,
           var position = aisObject.position {
            //			centerOnLocation(location: position)
            
            var lonSpanTargetInfo = (CGFloat(aisTargetInfoView.bounds.width) / CGFloat(mapView.bounds.width)) * CGFloat(mapView.region.span.longitudeDelta)
            position.longitude -= Double(lonSpanTargetInfo) / 2
            
            moveMapTo(coord: position, animated: true)
        }
    }
    
    @IBAction func aisTargetInfoCloseButton(_ sender: Any) {
        aisTargetInfoView.isHidden = true
    }
    
    //	aisTargetInfoName
    //	aisTargetInfoClass
    //	aisTargetInfoMMSI
    //	aisTargetInfoRegistration
    //	aisTargetInfoDestination
    //	aisTargetInfoDimensions
    //	aisTargetInfoCPA
    //	aisTargetInfoTCPA
    //	aisTargetInfoBRG
    //	aisTargetInfoRNG
    //	aisTargetInfoPosition
    //	aisTargetInfoDevice
    //	aisTargetInfoSOG
    //	aisTargetInfoHDG
    //	aisTargetInfoCOG
    //	aisTargetInfoROT
    //	aisTargetInfoIMO
    //	aisTargetInfoType
    //	aisTargetInfoMaxDraught
    //	aisTargetInfoETA
    //	aisTargetInfoCargo
    //	aisTargetInfoStatus
    //	aisTargetInfoAISVendor
    
    @IBOutlet weak var aisTargetInfoName: UILabel!
    @IBOutlet weak var aisTargetInfoClass: UILabel!
    @IBOutlet weak var aisTargetInfoMMSI: UILabel!
    @IBOutlet weak var aisTargetInfoRegistration: UILabel!
    @IBOutlet weak var aisTargetInfoDestination: UILabel!
    @IBOutlet weak var aisTargetInfoDimensions: UILabel!
    @IBOutlet weak var aisTargetInfoCPA: UILabel!
    @IBOutlet weak var aisTargetInfoTCPA: UILabel!
    @IBOutlet weak var aisTargetInfoBRG: UILabel!
    @IBOutlet weak var aisTargetInfoRNG: UILabel!
    @IBOutlet weak var aisTargetInfoPosition: UILabel!
    @IBOutlet weak var aisTargetInfoDevice: UILabel!
    @IBOutlet weak var aisTargetInfoSOG: UILabel!
    @IBOutlet weak var aisTargetInfoHDG: UILabel!
    @IBOutlet weak var aisTargetInfoCOG: UILabel!
    @IBOutlet weak var aisTargetInfoROT: UILabel!
    @IBOutlet weak var aisTargetInfoIMO: UILabel!
    @IBOutlet weak var aisTargetInfoType: UILabel!
    @IBOutlet weak var aisTargetInfoMaxDraught: UILabel!
    @IBOutlet weak var aisTargetInfoETA: UILabel!
    @IBOutlet weak var aisTargetInfoCargo: UILabel!
    @IBOutlet weak var aisTargetInfoStatus: UILabel!
    @IBOutlet weak var aisTargetInfoAISVendor: UILabel!
    
    func setValuesAISTargetInfo() {
        
        if let aisObject = selectedAisObject {
            
            aisTargetInfoName.text = aisObject.name ?? "-"
            aisTargetInfoClass.text = aisObject.aisClass ?? "-"
            aisTargetInfoMMSI.text = String(aisObject.mmsi)
            aisTargetInfoRegistration.text = "\(aisObject.registration?.country ?? "") \(aisObject.registration?.flag ?? "")"
            aisTargetInfoDestination.text = aisObject.destination ?? "-"
            
            if let dimensions = aisObject.dimensions {
                var width = dimensions[2] + dimensions[3]
                var length = dimensions[0] + dimensions[1]
                aisTargetInfoDimensions.text = " \(length)m \u{00D7}\(width)m"
            }
            //		tvCPA.setText(String.format("%.0f", StUnits.unitConvert(mAisObject.getCpaDistance(), Unit.M, mUnitDistanceSmall) ) + " " + StUnits.unitToStr(mUnitDistanceSmall));
            aisTargetInfoCPA.text = StUtils.formatDistance( aisObject.getCpaDistance() != nil ? Double(aisObject.getCpaDistance() ?? 0) : nil) ?? "-"
            //		tvTCPA.setText(StUnits.formatTimeInterval(mAisObject.getTcpaSeconds()));
            aisTargetInfoTCPA.text = StUtils.formatTimeInterval(aisObject.tcpaSeconds) ?? "-"
            aisTargetInfoBRG.text = aisObject.bearing != nil ? "\(radiansToDegrees(Double(aisObject.bearing!)))\u{00B0}" : "-"
            
            //            aisTargetInfoRNG.text = StUtils.formatDistance(aisObject.distance != nil ? Double(aisObject.distance ?? 0) : nil) ?? "-"
            aisTargetInfoRNG.text = StUtils.formatDistance(aisObject.getDistance(userLocation: userLocation?.coordinate)) ?? "-"
            
            aisTargetInfoPosition.text = "\(StNMEAUtils.toDegString(radians: aisObject.getLat())) / \(StNMEAUtils.toDegString(radians: aisObject.getLon()))"
            //		aisTargetInfoDevice.text = aisObject.getDevice() //// getDevice() isn't implemented here and in the Android-version
            aisTargetInfoSOG.text = StUtils.formatSpeed(aisObject.sog != nil ? Double(aisObject.sog!) : nil) ?? "-"
            var trueHeading = "-"
            if aisObject.getTrueHeading() ?? -1 >= Float(0) {
                trueHeading = String(Int(radiansToDegrees(Double(aisObject.getTrueHeading()!)))) + "\u{00B0}"
            }
            aisTargetInfoHDG.text = trueHeading
            aisTargetInfoCOG.text = aisObject.cog != nil ? "\(Int(radiansToDegrees(aisObject.cog!)))\u{00B0}" : "-"
            aisTargetInfoROT.text = aisObject.rot != nil ? String(format: "%.0f", aisObject.rot!) + "\u{00B0}/min" : "-"
            aisTargetInfoIMO.text = aisObject.imoNumber != nil ? aisObject.imoNumber! != 0 ? String(aisObject.imoNumber!) : "-" : "-"
            aisTargetInfoType.text = aisObject.getTypeString()
            aisTargetInfoMaxDraught.text = aisObject.maxStaticDraught != nil ? String(aisObject.maxStaticDraught!) : "-"
            aisTargetInfoETA.text = aisObject.eta != nil ? aisObject.eta!.asString() : "-"
            aisTargetInfoCargo.text = aisObject.cargoType != nil ? String(aisObject.cargoType!) : "-"
            aisTargetInfoStatus.text = aisObject.navStatus != nil ? aisObject.navStatus! >= 0 ? String(aisObject.navStatus!) : "-" : "-"
            aisTargetInfoAISVendor.text = aisObject.vendorId ?? "-"
            
            setSecondsSinceUpdate()
            
            aisTargetInfoView.setNeedsDisplay()
            aisTargetInfoView.setNeedsLayout()
        }
        
    }
    
    var aisTargetInfoSecondsUpdating = false
    func setSecondsSinceUpdate(){
        if let aisObject = selectedAisObject,
           aisTargetInfoView.isHidden == false {
            aisTargetInfoSecondsSinceUpdate.text = StUtils.formatTimeInterval(Int(round(Double(StUtils.currentTimeMillis() - aisObject.lastUpdate) / 1000.0)))
            
            if aisTargetInfoSecondsUpdating == false {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(1000)), execute: {
                    self.setSecondsSinceUpdate()
                    self.aisTargetInfoSecondsUpdating = false
                    
                })
                aisTargetInfoSecondsUpdating = true
            }
        }
    }
    
    func initializeWWNetwork(){
        // start loading waterway network
        if Resources.useWWNetwork {
            dout("AppDelegate: start loading network")
            var allowedWaterbodyMask :Int = 0
            let actDB = StActManager.getActManager();
            if actDB.getProductCodeCount() > 0 {

                var CP = StActManager.ChartProduct()
                for i in 0 ..< actDB.getProductCodeCount() {
                    actDB.getActCode(aIndex: i, aCP: CP)
                    allowedWaterbodyMask = allowedWaterbodyMask | WWDefs.chartToWaterbody(CP.Pid, CP.Mod)
                }

                if (allowedWaterbodyMask > 0) {

                    //SharedPreferences Prefs = getSharedPreferences("Stentec.DKW1800.Settings", MODE_PRIVATE);
                    //                let LoadNetwork :Bool = ViewController.instance?.mLoadNetwork ?? false
                    let loadNetwork = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.object(forKey: Resources.SETTINGS_LOAD_WWNETWORK) as? Bool ?? true


                    //                ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                    //                int MemClass = am.getLargeMemoryClass();
                    //                if (MemClass < 300) {
                    //
                    //                    if (LoadNetwork) {
                    //                        boolean NetworkLoadedOk = StSettingsInterface.settings().getLoadWWNetworkOk();// Prefs.getBoolean(StSettingsInterface.LOAD_WWNETWORK_OK, false);
                    //                        if (!NetworkLoadedOk) {
                    //                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_wwnetwork_memory), Toast.LENGTH_LONG).show();
                    //                            LoadNetwork = false;
                    //                            StSettingsInterface.settings().setLoadNetwork(LoadNetwork);// Prefs.edit().putBoolean(StSettingsInterface.LOAD_WWNETWORK, false).commit();
                    //                        }
                    //                    }
                    //                }

                    //                StSettingsInterface.settings().setLoadWWNetworkOk(false);

                    if loadNetwork {
                        dout("VC: WWNetwork.getNetwork")

                        let WWFilename :String = StActManager.getActManager().getWWFilePath()
                        WWNetwork.getNetwork(aFilename: WWFilename, mainHandler: nil)

                        dout("VC: done loading network, generate overlays")

                        NetworkOverlay.instance.generateNetworkOverlays()
                        dout("VC: done generating, try if a reload of visible elements is needed")

                        
                        NetworkOverlay.instance.addVisibleNetworkLinesToMapview()
                        dout("VC: done trying ")

                        
                    }
                }
            }
        }
    }
}

extension ViewController : MKMapViewDelegate {
    
    func mapView(_ ProfileMapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if let wpAnnotation = annotation as? StDBWaypointAnnotation {
            //						dout(.wp, "mapView(viewFor:_): annotation view request for: \(wpAnnotation.waypoint.name)")
            
            //			let reuseId = wpAnnotation.waypoint?.GUID?.toString()! ?? ""
            let reuseId = "StDBWaypoint"
            var wpView :CustomAnnotationView
            
            //var iconImageView :UIImageView?
            //var label :UILabel?
            
            if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
                // hide view until it is resized later
                dequededView.isHidden = true
                
                dequededView.annotation = wpAnnotation
                wpView = dequededView as! CustomAnnotationView
                //iconImageView = UIImageView(image: wpView.image)
                //if(wpView.subviews.count > 0){
                //	label = wpView.subviews[1] as! UILabel
                //}
                
                // replace imageview with the right one
                DispatchQueue.main.async{
                    wpView.subviews[0].removeFromSuperview()
                    wpView.imageView = UIImageView(image: wpAnnotation.waypoint?.type?.iconImage)
                    wpView.insertSubview(wpView.imageView!, at: 0)
                }

            } else {
                wpView = CustomAnnotationView(annotation: wpAnnotation, reuseIdentifier: reuseId)
                
                wpView.bounds = CGRect.init(x: -22, y: -22, width: 44, height: 44)
                
                wpView.canShowCallout = false
                
                //				let button = UIButton(type: .detailDisclosure)
                
                //				wpView.rightCalloutAccessoryView = button
                
                //if(customAnnotation.waypoint?.type?.name == "City" || mShowWaypointNamesFromSettings) {
                
                wpView.imageView = UIImageView(image: wpAnnotation.waypoint?.type?.iconImage)
                
                //                // hide imageView until it is resized later
                //                wpView.imageView?.isHidden = true
                
                wpView.addSubview(wpView.imageView!)
                
                
                wpView.textLabel = UILabel()
                
                wpView.textLabel!.textColor = UIColor.black
                //				wpView.textLabel!.backgroundColor = UIColor.orange
                wpView.textLabel!.backgroundColor = UIColor.clear
//                wpView.textLabel!.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7)

                wpView.textLabel!.textAlignment = .center
                wpView.textLabel!.numberOfLines = 0
                
                wpView.addSubview(wpView.textLabel!)
                
                //wpView.autoresizesSubviews = false
                //wpView.translatesAutoresizingMaskIntoConstraints = false
                //}
                

                
                //				let resizedSize = CGSize(width: 100, height: 100)
                //				UIGraphicsBeginImageContext(resizedSize)
                //				iconImage?.draw(in: CGRect(origin: CGPoint(x: -50, y: -50), size: resizedSize))
                //				let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
                //				UIGraphicsEndImageContext()
                //				wpView.image = resizedImage
                
            }
            
            let titleText = wpAnnotation.title!
            wpView.textLabel!.text = titleText
            wpView.textLabel!.invalidateIntrinsicContentSize()
            
            //label.frame = CGRect.init(x: 0, y: wpView.bounds.height, width: 400, height: 20)
            
            //			if (wpAnnotation.waypoint?.type?.name == "City" || mSettingsShowWaypointNames || wpAnnotation.waypoint?.type?.name = "Vaarwegdiepte DM") {
            
            if labelIsEnabled(wpAnnotation) {
                wpView.textLabel!.isHidden = false
                
                // handle bridge waypoints. By default, show H,W, with waypointlabels enabled, also show name
                if wpAnnotation.waypoint?.type?.name == "Brug" {
                    setBridgeLabel(wpView, annotation: wpAnnotation)
                }

            } else {
                wpView.textLabel!.isHidden = true
            }
            
            // hide wpView until it is resized later
            wpView.isHidden = true
            
            waypointSetScaleAndRotation(wpView, wpAnnotation, zoom: mapView.getZoom())
            
            //			}
            
            return wpView
            
        } else if annotation is MKUserLocation {
            
            let reuseId = "user location"
            
            //var iconImageView :UIImageView?
            //var label :UILabel?
            if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
                //it shoudl be always the same dequededView.annotation = customAnnotation
                mLocationView = dequededView
                //iconImageView = UIImageView(image: wpView.image)
                //if(wpView.subviews.count > 0){
                //	label = wpView.subviews[1] as! UILabel
                //}
            } else {
                mLocationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
                mLocationView!.canShowCallout = true
                let infoButton = UIButton(type: .detailDisclosure)
                
                mLocationView!.rightCalloutAccessoryView = infoButton
                
                
                //				let iconImage :UIImage? = annotation.waypoint?.type?.iconImage!
                //iconImageView = UIImageView(image: iconImage)
                
                mLocationView!.image = nil
            }
            
            //						locationMarker = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            //						locationMarker.pinTintColor = UIColor.purple
            
            drawLocationView(toAnnotationView: mLocationView!)
            //            if mNavWaypoint != nil {
            //                let navWP = mapView.convert(mNavWaypoint!.coordinate, toPointTo: locationView)
            //                addNavWaypointLine(toAnnotationView: locationView, navWPscreenPoint: navWP)
            //            }
            
            return mLocationView
            
        } else if annotation is NavigationWaypointAnnotation {
            
            dout(.wp, "annotation view request for nav waypoint")
            
            // don't draw the waypoint if it's navigating to a route point
            if Routes.service.activeRoutePoint != nil {
                return MKAnnotationView(annotation: annotation, reuseIdentifier: "navWaypoint")
            }
            
            var wpView :MKAnnotationView
            
            if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: "navWaypoint"){
                dequededView.annotation = annotation
                wpView = dequededView
                
            } else {
                wpView = MKAnnotationView(annotation: annotation, reuseIdentifier: "navWaypoint")
                
                wpView.bounds = CGRect.init(x: -22, y: -22, width: 44, height: 44)
                
                //				wpView.canShowCallout = true
                //				wpView.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
                wpView.canShowCallout = false
                
                let label = UILabel()
                label.text = "Navigation waypoint"
                label.invalidateIntrinsicContentSize()
                label.textColor = UIColor.black
//                label.backgroundColor = UIColor.clear
                label.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7)

                wpView.addSubview(label)
                
                wpView.addSubview(Waypoints.getCustomWaypointIconView())
            }
            
            return wpView
        } else if annotation is DraggingAnnotation {
            
            //			return nil
            
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            pinAnnotationView.pinTintColor = .red
            //				pinAnnotationView.isDraggable = true
            //				pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = false
            
            //			let pinAnnotationViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(routeOverlay!.draggedPinAnnotationView(_:)))
            let pinAnnotationViewPanGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedPinAnnotationView(_:)))
            pinAnnotationView.isUserInteractionEnabled = true
            pinAnnotationView.addGestureRecognizer(pinAnnotationViewPanGesture)
            return pinAnnotationView
            
        } else if let rPLAnnotation = annotation as? RoutePointLabelAnnotation {
            
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.title ?? "")
            
            //			annotationView.addSubview(rPLAnnotation.getTextLabelView())
            rPLAnnotation.annotationView = annotationView
            
            rPLAnnotation.updateAnnotationView()
            
            return rPLAnnotation.annotationView
            
            
        } else if let aisAnnotation = annotation as? AISAnnotation {
            
            //            let reuseId = String(aisAnnotation.aisObject.mmsi)
            let reuseId = "aisTargetView"
            var aisView :AISAnnotationView
            
            if let dequededView = ProfileMapView.dequeueReusableAnnotationView(withIdentifier: reuseId){
                dequededView.annotation = aisAnnotation
                aisView = dequededView as! AISAnnotationView
                
                //				// replace imageview with the right one
                //				DispatchQueue.main.async{ aisView.subviews[1].removeFromSuperview() }
                
            } else {
                aisView = AISAnnotationView(annotation: aisAnnotation, reuseIdentifier: reuseId)
                aisView.aisAnnotation = aisAnnotation
                aisAnnotation.aisAnnotationView = aisView
                
                aisView.bounds = CGRect.init(x: -22, y: -22, width: 44, height: 44)
                
                aisView.canShowCallout = false
                
                aisView.textLabel = UILabel()
                aisView.textLabel!.textColor = UIColor.black
                aisView.textLabel!.backgroundColor = UIColor.clear
//                aisView.textLabel!.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7)

                aisView.textLabel!.textAlignment = .center
                aisView.textLabel!.numberOfLines = 0
                
                aisView.addSubview(aisView.textLabel!)
            }
            
            // crosslink objects
            aisView.aisAnnotation = aisAnnotation
            aisAnnotation.aisAnnotationView = aisView
            
            var color = aisAnnotation.aisObject.getColor()
            
            withUnsafePointer(to: aisView) { dout("aisView address: \($0)") } // for crash monitoring
            dout("Aisview == nil: \(Bool(aisView == nil))") // for crash monitoring
            aisView.updateImageView() // create or replace imageview // crash: EXC_BAD_ACCES
            
            aisView.setTextLabel()
            
            //            let currentMapZoom = mapView.getZoom()
            //            dout("Current map zoom for ais view: \(currentMapZoom)")
            aisView.isHidden = true // will be unhidden in next function
            aisView.setScaleAndRotation(zoom: mapView.getZoom())
            
            aisAnnotation.aisAnnotationView = aisView
            
            return aisView
            
        } else if let networkSelectionAnnotation = annotation as? NetworkSelectionAnnotation {
            
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotation.title ?? "")
            
            //            annotationView.addSubview(rPLAnnotation.getTextLabelView())
            networkSelectionAnnotation.annotationView = annotationView
            networkSelectionAnnotation.updateAnnotationView()
            
            return networkSelectionAnnotation.annotationView
        }
        
        return nil
    }
    
    @objc func draggedPinAnnotationView(_ sender :UIPanGestureRecognizer){
        dout("dragged pin annotation view")
        
        routeOverlay?.draggedPinAnnotationView(sender)
    }
    
    func setBridgeLabel(_ cAView :CustomAnnotationView, annotation :StDBWaypointAnnotation){
        cAView.textLabel!.frame.size.width = 400
        if mSettingsShowWaypointNames {
            cAView.textLabel!.numberOfLines = 2
            cAView.textLabel!.text = "\(annotation.title!)\n\(annotation.subtitle!)"
        } else {
            cAView.textLabel!.numberOfLines = 1
            cAView.textLabel!.text = annotation.subtitle!
        }
        cAView.textLabel!.sizeToFit()
        cAView.textLabel!.frame.size.width = 400
        
        cAView.textLabel!.layoutIfNeeded()
        cAView.textLabel!.setNeedsDisplay()
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        //        view.subviews.filter { $0 is CustomCalloutView }.forEach{
        //            $0.removeFromSuperview()
        //        }
        
        dout("mapView didSelect")
        
        networkOverlay?.deselect()
        
        if !waypointTouchEnabled { return }
        
        
        if (view.subviews.count) > 1 {
            waypointBorderView = UIImageView(frame: (view.subviews[0].frame.insetBy(dx: -4, dy: -4)))//CGRect(origin: view.center, size: CGSize(width: view.frame.width + 2, height: view.frame.height + 2)))
            waypointBorderView?.layer.borderWidth = 2
            waypointBorderView?.layer.borderColor = UIColor(red:0.00, green:0.29, blue:0.53, alpha:1.0).cgColor //UIColor.blue.cgColor
            view.addSubview(waypointBorderView!)
            
            view.layoutIfNeeded()
        }
        
        if let annotation = view.annotation as? StDBWaypointAnnotation {
            
            let waypoint = annotation.waypoint
            waypointInfoNameLabel.text = waypoint?.name
            waypointInfoSubnameLabel.text = waypoint?.subName
            if(waypointInfoSubnameLabel.text == ""){
                waypointInfoSubnameView.isHidden = true
            } else {
                waypointInfoSubnameView.isHidden = false
            }
            waypointInfoTypeLabel.text = waypoint?.type?.name
            var wpURL : [String] = [String]()
            if let lightChar = waypoint?.lightChar {
                
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let matches = detector.matches(in: lightChar, options: [], range: NSRange(location: 0, length: lightChar.utf16.count))
                
                for match in matches {
                    guard let range = Range(match.range, in: lightChar) else { continue }
                    let url = lightChar[range]
                    wpURL.append(String(url))
                }
                
                if wpURL.count > 0 {
                    //					waypointInfoUrlLabel.attributedText = NSAttributedString(string:
                    //						lightChar, attributes: [NSAttributedString.Key.link: wpURL[0]])
                    //					let tap = UITapGestureRecognizer(target: self, action: Selector("urlTap:"))
                    //					waypointInfoUrlLabel.addGestureRecognizer(tap)
                    waypointInfoUrlLabel.text = ""
                    
                    waypointInfoUrlTextView.text = wpURL[0]
                    waypointInfoUrlTextView.isEditable = false
                    waypointInfoUrlTextView.dataDetectorTypes = .link
                    
                } else {
                    waypointInfoUrlLabel.text = lightChar
                    waypointInfoUrlTextView.text = lightChar
                }
            } else {
                waypointInfoUrlLabel.text = ""
                waypointInfoUrlTextView.text = ""
            }
            
            if waypointInfoUrlLabel.text == "" {
                waypointInfoUrlView.isHidden = true
            } else {
                waypointInfoUrlView.isHidden = false
            }
            
            if waypointInfoUrlTextView.text == "" {
                waypointInfoUrlTextviewView.isHidden = true
            } else {
                waypointInfoUrlTextviewView.isHidden = false
            }
            
            let coord = CLLocationCoordinate2D(latitude: waypoint?.getLatitude() ?? 0, longitude: waypoint?.getLongitude() ?? 0)
            
            waypointInfoLatLabel.text = coord.dms.latitude
            waypointInfoLonLabel.text = coord.dms.longitude
            
            //			let callout = waypointInfoView!//CustomCalloutView()
            //
            //			//callout.translatesAutoresizingMaskIntoConstraints = false
            //			if(callout.superview == nil){
            //				self.view.addSubview(callout)
            ////				callout.center = CGPoint(x: view.frame.origin.x, y: view.frame.minY - (view.frame.height / 2))
            //				callout.center = CGPoint(x: 300, y: 500)
            ////				callout.bottomAnchor.
            //
            ////				self.view.addConstraints([
            ////					NSLayoutConstraint(item: callout,
            ////											 attribute: .centerX,
            ////											 relatedBy: .equal,
            ////											 toItem: view,
            ////											 attribute: .centerX,
            ////											 multiplier: 1.0,
            ////											 constant: 0
            ////					),
            ////					NSLayoutConstraint(item: callout,
            ////											 attribute: .bottom,
            ////											 relatedBy: .equal,
            ////											 toItem: view,
            ////											 attribute: .top,
            ////											 multiplier: 1.0,
            ////											 constant: -25.0
            ////					)
            ////					])
            //
            ////				callout.alpha = 0.0
            ////				callout.contentScaleFactor = 0.0
            ////				UIView.animate(withDuration: 0.5) {
            ////					callout.alpha = 1.0
            ////					callout.contentScaleFactor = 1.0
            ////				}
            //				mWaypointInfoViewDragged = false
            //			}
            
            //			if(waypointInfoView.superview == nil){
            //				self.view.addSubview(waypointInfoView)
            //				mWaypointInfoViewDragged = false
            //			}
            
            if waypointInfoView.isHidden == true {
                waypointInfoView.isHidden = false
                mWaypointInfoViewDragged = false
            }
            
            waypointInfoView.alpha = 0.0
            waypointInfoView.contentScaleFactor = 0.0
            
            UIView.animate(withDuration: 0.2) {
                self.waypointInfoView.alpha = 1.0
                self.waypointInfoView.contentScaleFactor = 1.0
            }
            
            
            //			waypointInfoView.center = CGPoint(x: 100, y: 400)
            wpInfoConstraintLeading.constant = view.frame.origin.x
            wpInfoConstraintBottom.constant = view.frame.origin.y
            if view.subviews.count > 1 {
                wpInfoConstraintBottom.constant += view.subviews[0].frame.minY - 20
            }
            
            waypointInfoView.setNeedsDisplay()
            self.view.layoutIfNeeded()
            
        } else if view.annotation is NavigationWaypointAnnotation {
            
            self.view.layoutIfNeeded()
            
            self.view.addSubview(NavwaypointOptionMenu)
            //			NavwaypointOptionMenu.center = CGPoint(x: view.frame.origin.x, y: view.frame.minY - (NavwaypointOptionMenu.frame.height / 2) - 20)
            
            NavwaypointOptionMenu.center = CGPoint(x: view.center.x, y: view.frame.minY - 20)
            
        } else if let aisAnnotation = view.annotation as? AISAnnotation {
            
            selectAisTarget(aisAnnotation)
            
        }
        
        // Add gesture recognizer
        
        deselectSelectedAnnotationOnTap = UITapGestureRecognizer(target: self, action: #selector(self.deselectSelectedWP(_:)))
        view.addGestureRecognizer(deselectSelectedAnnotationOnTap!)
    }
    
    func selectAisTarget(_ aisAnnotation :AISAnnotation){
        
        
        
        if aisTargetInfoView.superview == nil {
            self.view.addSubview(aisTargetInfoView)
            
            self.view.addConstraints([
                NSLayoutConstraint(item: aisTargetInfoView,
                                   attribute: .left,
                                   relatedBy: .equal,
                                   toItem: self.view,
                                   attribute: .left,
                                   multiplier: 1.0,
                                   constant: 8
                ),
                //                    NSLayoutConstraint(item: onscreenInfoContainer,
                //                                       attribute: .bottomMargin,
                //                                       relatedBy: .equal,
                //                                       toItem: aisTargetInfoView,
                //                                       attribute: .top,
                //                                       multiplier: 1.0,
                //                                       constant: 0
                //                    ),
                NSLayoutConstraint(item: aisTargetInfoView,
                                   attribute: .top,
                                   relatedBy: .equal,
                                   toItem: self.mapView,
                                   attribute: .top,
                                   multiplier: 1.0,
                                   constant: 35
                ),
                NSLayoutConstraint(item: aisTargetInfoView,
                                   attribute: .bottom,
                                   relatedBy: .lessThanOrEqual,
                                   toItem: buttonsBackgroundView,
                                   attribute: .top,
                                   multiplier: 1.0,
                                   constant: -20
                )
            ])
        }
        
        aisTargetInfoView.isHidden = false
        
        if let view = aisAnnotation.aisAnnotationView,
           aisTargetInfoView.bounds.contains(view.center) || !mapView.bounds.contains(view.center),
           var position = aisAnnotation.aisObject.position {
            
            var lonSpanTargetInfo = (Double(aisTargetInfoView.bounds.width) / Double(mapView.bounds.width)) * Double(mapView.region.span.longitudeDelta)
            position.longitude -= lonSpanTargetInfo / 2
            
            moveMapTo(coord: position, animated: true)
        }
        
        
        selectedAisObject = aisAnnotation.aisObject
        setValuesAISTargetInfo()
        aisTargetInfoView.setNeedsDisplay()
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        //        var callout = view.subviews.filter({ (subview) -> Bool in
        //            subview is CustomCalloutView
        //        }).first
        
        waypointBorderView?.removeFromSuperview()
        
        if view.annotation is StDBWaypointAnnotation {
            if (mWaypointInfoViewDragged == false){
                
                //			for constraint in self.view.constraints {
                //				if constraint.firstItem === callout {
                //					self.view.removeConstraint(constraint)
                //				}
                //			}
                
                //			callout?.removeFromSuperview()
                //			callout = nil
                
                waypointInfoView.isHidden = true
            }
        } else if view.annotation is NavigationWaypointAnnotation {
            if NavwaypointOptionMenu.superview != nil {
                NavwaypointOptionMenu.removeFromSuperview()
            }
        }
        
        if let deselect = deselectSelectedAnnotationOnTap {
            view.removeGestureRecognizer(deselect)
        }
        
    }
    
    @objc func deselectSelectedWP(_ sender: UITapGestureRecognizer){
        StaticQueues.mapviewAnnotationsLock.runLocked({ self.mapView.deselectAnnotation(self.mapView.selectedAnnotations.first, animated: false) })
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if view.annotation is MKUserLocation {
            let rect = CGRect(x: 100, y: 200, width: 400, height: 400)
            
            let pickerView = UIView(frame: rect)
            
            showColorPicker(sender: pickerView)
            
        }
        
        //		else if view.annotation is StDBWaypointAnnotation {
        //			let wp = (view.annotation as! StDBWaypointAnnotation)
        //			mAnnotationViewTapped = view
        //			mWaypointTapped = wp.waypoint!
        //			showWaypointInfoView(view: view)
        //
        //		}
        view.setSelected(false, animated: false)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        if let tileOverlay = overlay as? DKW2TileOverlay {
            dout(.tiles, "mapview() rendererFor: DKW2TileOverlay")
            //            let renderer = MKTileOverlayRenderer(overlay: tileOverlay)
            let renderer = DKW2TileOverlayRenderer(overlay: tileOverlay)
            renderer.mFillBackground = self.mFillBackground
            //Set the renderer alpha to be overlay alpha
            //            renderer.alpha = (overlay as MyTileOverlay).alpha
            renderer.alpha = 1
            //            dout("alpha: ", renderer.alpha)
            
            dKW2TileRenderer = renderer
            return renderer
        } else if let tileOverlay = overlay as? DKW2BackgroundOverlay {
            dout(.tiles, "mapview() rendererFor: DKW2BackgroundOverlay")
            let renderer = DKW2BackgroundOverlayRenderer(overlay: tileOverlay)
            //Set the renderer alpha to be overlay alpha
            //				renderer.alpha = 0.7
            //            dout("alpha: ", renderer.alpha)
            
            return renderer
            //		} else if overlay is MKPolyline {
        } else if overlay === mNavWaypointLine {
            dout(.tiles, "mapview() rendererFor: mNavWaypointLine")
            
            //let polylineRenderer = ConstantWidthPolylineRenderer(overlay: overlay)
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue//.withAlphaComponent(0.8)
            
            polylineRenderer.lineWidth = 3//CGFloat(10 * zoomLevel)
            
            return polylineRenderer//
            
        } else if let ttpl = overlay as? StTrackPolyline {
            dout(.tiles, "mapview() rendererFor: mTailTrackPolyline")
            
            //let polylineRenderer = ConstantWidthPolylineRenderer(overlay: overlay)
            let polylineRenderer = MKPolylineRenderer(overlay: ttpl)
            polylineRenderer.strokeColor = UIColor.red//.withAlphaComponent(0.8)
            
            polylineRenderer.lineWidth = 3//CGFloat(10 * zoomLevel)
            
            if ttpl.isDashed {
                polylineRenderer.lineDashPattern = [NSNumber(value: 1),NSNumber(value:10)]
            }
            
            return polylineRenderer
            
        }
        
        // Route overlay renderers:
        else if let rpl = overlay as? StRoutePolyline {
            dout(.tiles, "mapview() rendererFor: StRoutePolyline")
            
            let renderer = MKPolylineRenderer(overlay: rpl)
            renderer.strokeColor = UIColor.red//.withAlphaComponent(0.8)
            renderer.lineWidth = 4//CGFloat(10 * zoomLevel)
            return renderer
            
        } else if let rpc = overlay as? StRoutePointCircle {
            dout(.tiles, "mapview() rendererFor: StRoutePointCircle")
            
            let renderer = MKCircleRenderer(overlay: rpc)
            renderer.strokeColor = UIColor.red//.withAlphaComponent(0.8)
            renderer.lineWidth = 10 //CGFloat(10 * zoomLevel)
            renderer.fillColor = renderer.strokeColor
            return renderer
            
        } else if let polyLine = overlay as? NetworkSegmentPolyline {
            dout(.tiles, "mapview() rendererFor: NetworkSegmentPolyline")
            
            //            let renderer = MKPolylineRenderer(overlay: polyLine)
            let renderer = NetworkSegmentPolylineRenderer(overlay: polyLine)
            //            renderer.strokeColor = UIColor.red//.withAlphaComponent(0.8)
            
            renderer.strokeColor = polyLine.color //.withAlphaComponent(0.8)
            
            /// Testing: dynamic color to segment length
            let dynamicColor = false
            if dynamicColor {
                let length = polyLine.polyLinePos?[polyLine.pointCount - 1] ?? 0
                //            dout("polyLine = overlay as? NetworkSegmentPolyline; length: \(length)")
                let colorvalue: CGFloat = CGFloat( Double(length ?? 0) / 2000.0 )
                let color :UIColor = UIColor(red: colorvalue, green: colorvalue, blue: colorvalue, alpha: 1.0)
                renderer.strokeColor = color
            }
            
            //            renderer.lineWidth = 1.5//CGFloat(10 * zoomLevel)
                        renderer.lineWidth = 2 //CGFloat(10 * zoomLevel)
            //            renderer.lineWidth = 3//CGFloat(10 * zoomLevel)
            
            return renderer
            
        } else if overlay is MKPolyline {
            dout(.tiles, "mapview() rendererFor: MKPolyline")
            
            //let polylineRenderer = ConstantWidthPolylineRenderer(overlay: overlay)
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.yellow//.withAlphaComponent(0.8)
            
            polylineRenderer.lineWidth = 4//CGFloat(10 * zoomLevel)
            
            return polylineRenderer
        } else if overlay is MKCircle { // Distance circles
            dout(.tiles, "mapview() rendererFor: MKCircle")
            
            let renderer = MKCircleRenderer(overlay: overlay)
            renderer.strokeColor = UIColor.black
            renderer.lineWidth = 1
            return renderer
        } else {
            dout("mapview() tileOverlay is no DKW2 overlay")
            return MKTileOverlayRenderer(overlay: overlay)
        }
    }
    
    //func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
    
    //}
    
    //    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    //        // This is the final step. This code can be copied and pasted into your project
    //        // without thinking on it so much. It simply instantiates a MKTileOverlayRenderer
    //        // for displaying the tile overlay.
    //        if let tileOverlay = overlay as? ExploredTileOverlay {
    //            dout("mapview() tileOverlay = ExploredTileOverlay")
    ////                        let renderer = MKTileOverlayRenderer(overlay: tileOverlay)
    //            let renderer = ExploredTileRenderer(overlay: tileOverlay)
    //            //Set the renderer alpha to be overlay alpha
    //            //            renderer.alpha = (overlay as MyTileOverlay).alpha
    //            renderer.alpha = 0.7
    //            dout("alpha: ", renderer.alpha)
    //
    //            return renderer
    //        } else {
    //            dout("mapview() tileOverlay is not ExploredTileOverlay")
    //            return MKTileOverlayRenderer(overlay: overlay)
    //        }
    //    }
    
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated: Bool) {
        
        // Update annotations like waypoints and Ais //
        
        // don't update more often than a certain frequency
        let minTimeBetweenUpdates :TimeInterval = 1
        var timestamp = Date()
        
        if timestamp.timeIntervalSince(updateWaypointsTimestampLastUpdate) > minTimeBetweenUpdates {
            updateWaypointsTimestampLastUpdate = timestamp
            
            ////// Update all view-windows based annotations
            
            updateViewportBasedLayers()
            
        } else {
            // check if the next one is already planned, and if not, plan it.
            if !nextUpdatewaypointsPlanned {
                nextUpdatewaypointsPlanned = true
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.milliseconds(Int(minTimeBetweenUpdates * 1000)), execute: { // NSSetM addObject:]: object cannot be nil
                    
                    self.updateViewportBasedLayers()
                    
                    self.updateWaypointsTimestampLastUpdate = Date()
                    self.nextUpdatewaypointsPlanned = false
                })
            }
        }
    }
    
    
    func updateViewportBasedLayers(){
        
        let mapRectGeo = getMapRectGeo()
        updateWaypoints(mapRectGeo: mapRectGeo)
        
        aisOverlay?.updateDrawList()
    }
    
    func getMapRectGeo() -> CGRect {
        // Determine visible map region
        let mapRegion :MKCoordinateRegion = mapView.region
        var west = mapRegion.center.longitude - (mapRegion.span.longitudeDelta / 2)
        if west < -180 { west += 360 }
        let south = mapRegion.center.latitude - (mapRegion.span.latitudeDelta / 2)
        let mapRectGeo :CGRect = CGRect(
            x: west,
            y: south,
            width: mapRegion.span.longitudeDelta,
            height: mapRegion.span.latitudeDelta)
        
        return mapRectGeo
    }
    
    // Handle draggable annotation to edit route points
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationView.DragState, fromOldState oldState: MKAnnotationView.DragState) {
        dout("mapView didChange state")
        
        switch newState {
        case .starting:
            view.dragState = .dragging
        case .ending, .canceling:
            //New cordinates
            dout("dragged to new location")
            view.dragState = .none
        default: break
        }
    }
}

//class CustomCalloutView: UIView {
//    convenience init() {
//
//        self.translatesAutoresizingMaskIntoConstraints = false
//        let tap = UITapGestureRecognizer(target: self, action: #selector(onTap))
//        tap.numberOfTapsRequired = 1
//        self.addGestureRecognizer(tap)
//    }
//
//    @objc func onTap() {}
//}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        
        if newHeading.headingAccuracy >= 0 {
            let heading = newHeading.trueHeading > 0 ? newHeading.trueHeading : newHeading.magneticHeading
            userHeading = heading
            //			updateLocationRotation()
        }
        
        //		dout(.def, "didUpdateHeading: newHeading:", newHeading.trueHeading)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location :CLLocation = locations.last!
        
        var distance :Double = -1
        
        if let userLocationLastUpdateNavLine = self.userLocationLastUpdateNavLine {
            distance = location.distance(from: userLocationLastUpdateNavLine)
        }
        
        userLocation = location
        
        let course = location.course
        
        // If there is a known course, set the heading/rotation accordingly
        if course >= 0 {
            userCourse = course
            
            if self.courseUp,
               self.mFollowing {
                mapView.camera.heading = userCourse
            }
        }
        
        // set location
        if self.mFollowing,
           let coordinate = userLocation?.coordinate {
            mapView.camera.centerCoordinate = coordinate
            mapView.setCamera(mapView.camera, animated: false)
        }
        
        let speed = location.speed
        if speed >= 0 {
            userSpeed = speed
        }
        
        updateInstrumentInfo()
        
        /// Update location related overlays: navwaypoint line (and info) and distance circles
        
        updateNavpointInfo()
        
        //		// Update Route following
        //		if Routes.service.activeRoutePoint != nil,
        //			let userLocation = self.userLocation {
        //			Routes.service.updateUserLocationForRouting(userLocation)
        //		}
        
        let distanceThreshold = Double(5)
        if distance > distanceThreshold || distance == -1 {
            // redraw navigation line and distance circles
            
            userLocationLastUpdateNavLine = userLocation
            drawNavigationWaypointLine()
            drawDistanceCircles()
            
        }
        
        /// Update tracking
        dout("ViewController didUpdateLocation GPS update \(location.speed) \(location.course) \(location.coordinate.longitude) \(location.coordinate.latitude)")
        
        if StUtils.isDebug() {
            debugLabel.numberOfLines = 4
            debugLabel.text =
                "sp: \(speed)\n hor.acc: \(location.horizontalAccuracy)\n vert.acc: \(location.verticalAccuracy)"
            //		debugLabel.layoutIfNeeded()
            //		debugLabel.setNeedsDisplay()
            //		debugLabel.superview?.layoutSubviews()
        }
        
        /// With larger accuracies: blink red and turn mkuserlocation red
        
        let accuracyThreshold = CLLocationAccuracy(20) // was: 10
        
        if location.horizontalAccuracy <= accuracyThreshold {
            // blink status led
            blinkStatusLed("green")
            
            // set userlocation to blue
            if let ulView = mapView.view(for: mapView.userLocation) {
                
                if headingImageView != nil && ulView.subviews.contains(headingImageView!) {
                    headingImageView!.removeFromSuperview()
                }
//                let image = getLocationMarkerImage(UIColor.blue)
//                headingImageView = UIImageView(image: image)
//                headingImageView!.frame = CGRect(x: (ulView.frame.size.width - image.size.width * headingImageScale)/2, y: (ulView.frame.size.height - image.size.height * headingImageScale)/2, width: image.size.width * headingImageScale, height: image.size.height * headingImageScale)
                headingImageView = getLocationMarkerImageView(UIColor.blue)
                ulView.addSubview(headingImageView!)
                
            }
        } else {
            blinkStatusLed("red")
            
            // set userlocation to red
            if let ulView = mapView.view(for: mapView.userLocation) {
                
                if headingImageView != nil && ulView.subviews.contains(headingImageView!){
                    headingImageView!.removeFromSuperview()
                }
//                let image = getLocationMarkerImage(UIColor.red)
//                headingImageView = UIImageView(image: image)
//                headingImageView!.frame = CGRect(x: (ulView.frame.size.width - image.size.width)/2, y: (ulView.frame.size.height - image.size.height)/2, width: image.size.width, height: image.size.height)
                headingImageView = getLocationMarkerImageView(UIColor.red)
                ulView.addSubview(headingImageView!)
                
            }
        }
        
        updateLocationCursorRotation(forceUsercursorNorthWhenNeeded: true) // Update the rotation of the location view (the position is updated by mapkit)
        
        
        //        //////// try with setcenter to avoid zooming problems!! /////////////////
        //        if mFollowing,
        //           let locationCoordinate = userLocation?.coordinate {
        //            mapView.setCenter(locationCoordinate, animated: true)
        //        }
        
        Nmea.service.userLocationUpdated()
        
        
        // try resetting the locations to not let it sleep when in a stationary position
        locationManager.desiredAccuracy = 999999
        locationManager.distanceFilter = CLLocationDistanceMax
        locationManager.stopUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(1000), execute: {
            // check for "location enabled" to make sure it isn't stopped elsewhere
            if self.locationUpdatesEnabled {
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.distanceFilter = kCLDistanceFilterNone
                self.locationManager.startUpdatingLocation()
                
                // todo: replace with self.locationManager.requestLocation()
            }
        })
    }
    
    func blinkStatusLed(_ color :String){
        
        switch color {
        case "green" :
            connectionStatusLed.image = UIImage(named: "ic_led_green")
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
                self.connectionStatusLed.image = UIImage(named: "ic_led_off")
            })
        case "red" :
            connectionStatusLed.image = UIImage(named: "ic_led_red")
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200), execute: {
                self.connectionStatusLed.image = UIImage(named: "ic_led_off")
            })
        default:
            dout("")
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManagerEnableUpdating() {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.startUpdatingLocation()
        locationUpdatesEnabled = true
    }
    
    func locationManagerDisableUpdating() {
        locationManager.desiredAccuracy = 999999
        locationManager.distanceFilter = CLLocationDistanceMax
        locationManager.stopUpdatingLocation()
        locationUpdatesEnabled = false

    }
}

extension ViewController : StMapViewListener {
    
    func mapViewListener(_ mapView: StMapView, rotationDidChange rotation: Double) {
        // process new map rotation
        self.zoomLevel = mapView.getZoom()
        self.mapRotation = rotation
        //dout("ROTATING :",rotation, "Zoom: ", zoomLevel)
        
        updateLocationCursorRotation(forceUsercursorNorthWhenNeeded: false)
        
        // update rotation and zoom of visible waypoints
        var annotationsTemp = [MKAnnotation]()
        StaticQueues.mapviewAnnotationsLock.runLocked({
            //            annotationsTemp = mapView.annotations.map{ $0 }
            for a in self.mapView.annotations { if a != nil { annotationsTemp.append(a) } }
        })
        
        for annotation in annotationsTemp {
            StaticQueues.mapviewAnnotationsLock.runLocked({
                if let wpAnnotation = annotation as? StDBWaypointAnnotation,
                   let wpView = mapView.view(for: wpAnnotation) {
                    self.waypointSetScaleAndRotation(wpView, wpAnnotation, zoom: -1)
                }
            })
        }
        
        // update Ais targets rotation
        aisOverlay?.rotationDidChange()
        
        mapView.setNeedsDisplay()
    }
    
    func mapViewListener(_ mapView: StMapView, zoomDidChange zoom: Double) {
        // process new map rotation
        //		self.zoomLevel = mapView.getZoom()
        
        //        StaticQueues.wpAnnotationsBGQueue.async { // try to make async and lock because of the crash.
        //            StaticQueues.mapviewAnnotationsLock.sync { // -[__NSSetM addObject:]: object cannot be nil
        
        // update rotation and zoom of visible waypoints
        //        let annotationsTemp = mapView.annotations // Possible: 'NSInvalidArgumentException', reason: '*** -[__NSSetM addObject:]: object cannot be nil'
        
        var annotationsTemp = [MKAnnotation]()
        StaticQueues.mapviewAnnotationsLock.runLocked({
            //            annotationsTemp = mapView.annotations.map{ $0 }
            if let mv = self.mapView {
                for a in self.mapView.annotations { if a != nil { annotationsTemp.append(a) } }// '*** -[__NSSetM addObject:]: object cannot be nil'
            }
        })
        for annotation in annotationsTemp {
            if let wpAnnotation = annotation as? StDBWaypointAnnotation,
               let wpView = mapView.view(for: wpAnnotation) {
                self.waypointSetScaleAndRotation(wpView, wpAnnotation, zoom: zoom)
            }
        }
        
        //        for wpa in self.wpAnnotationsOnMapview {
        //            if let wpView = mapView.view(for: wpa){
        //                self.waypointSetScaleAndRotation(wpView, wpa, zoom: zoom)
        //            }
        //        }
        
        aisOverlay?.rotationOrZoomDidChange(zoom: zoom)
        
        let zoom = mapView.getZoom()
        if distanceCirclesZoomedOut{
            if zoom > distanceCirclesZoomThreshold {
                distanceCirclesZoomedOut = false
                drawDistanceCircles()
            }
        } else {
            if zoom <= distanceCirclesZoomThreshold {
                distanceCirclesZoomedOut = true
                drawDistanceCircles()
            }
        }
    }
}

extension ViewController : SettingsDelegate {
    
    func updateSettings() {
        
        let settings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
        
        let showWaypoints :Bool = settings?.object(forKey: Resources.DEFAULT_SHOW_WAYPOINTS) as? Bool ?? mSettingsShowWaypoints
        if mSettingsShowWaypoints != showWaypoints {
            mSettingsShowWaypoints = showWaypoints
            //			updateWaypoints(mapView: mapView)
            updateWaypoints()
        }
        
        let showWaypointNames :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_WAYPOINT_NAMES) as? Bool ?? mSettingsShowWaypointNames
        if mSettingsShowWaypointNames != showWaypointNames {
            mSettingsShowWaypointNames = showWaypointNames
            
            //            var annotationsTemp = [MKAnnotation]()
            //            StaticQueues.mapviewAnnotationsLock.runLocked({
            ////                annotationsTemp = mapView.annotations.map{ $0 }
            //                for a in self.mapView.annotations { if a != nil { annotationsTemp.append(a) } }
            //            })
            
            StaticQueues.wpAnnotationsLock.runLocked({
                for annotation in self.wpAnnotationsOnMapview {
                    setLabelVisibility(annotation)
                }
            })
            mapView.setNeedsDisplay()
        }
        
        let showRoutes :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_ROUTES) as? Bool ?? mRouteOverlayEnabled
        if mRouteOverlayEnabled != showRoutes {
            mRouteOverlayEnabled = showRoutes
            //            if mRouteOverlayEnabled {
            //                // Initialize routes / set route visibility settings
            //            }
        }
        
        // TODO create ui-switch
        let showAIS :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_AIS) as? Bool ?? mAISOverlayEnabled
        if mAISOverlayEnabled != showAIS {
            mAISOverlayEnabled = showAIS
            //            if mAISOverlayEnabled {
            //                // Initialize AIS / set AIS visibility settings
            //            }
        }
        
        let showTracks :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_TRACKS) as? Bool ?? true 
        if mTrackOverlayEnabled != showTracks {
            mTrackOverlayEnabled = showTracks
            //            if mTrackOverlayEnabled {
            //                // Initialize tracks / set track visibility settings
            //            }
            
            // switch tracking
            if mTrackOverlayEnabled {
                if TrackingService.service.paused || TrackingService.service.runningTrack == nil {
                    TrackingService.service.start()
                    //                showTailTrack()
                }
                if Resources.appType != .plus { // this for all versions that just use 1 track
                    setTrackOverlayVisibillity(true)
                }
                
            } else {
                //                    TrackingService.service.pause()
                TrackingService.service.stop()
                //                hideTailTrack()
                if Resources.appType != .plus { // this for all versions that just use 1 track
                    setTrackOverlayVisibillity(false)
                }
            }
        }
        
        let mNetworkOverlayEnabledDefault = true
        let showNetwork :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_NETWORK) as? Bool ?? mNetworkOverlayEnabledDefault
        if mNetworkOverlayEnabled != showNetwork {
            mNetworkOverlayEnabled = showNetwork
            if mNetworkOverlayEnabled {
                // Initialize network / set network visibility settings
            }
        }
        
        let showAppleMaps :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_APPLEMAPS) as? Bool ?? mSettingsShowAppleMaps
        if mSettingsShowAppleMaps != showAppleMaps {
            mSettingsShowAppleMaps = showAppleMaps
            
            
            if !mSettingsShowAppleMaps {
                if #available(iOS 13.0, *) {
                    mapView.pointOfInterestFilter = .excludingAll
                } else {
                    mapView.showsPointsOfInterest = false
                }
                
                // Add a background overlay to hide apple maps names
                if dKW2BackgroundOverlay == nil {
                    dKW2BackgroundOverlay = DKW2BackgroundOverlay()
                } else {
                    StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.removeOverlay(self.dKW2BackgroundOverlay! ) })
                }
                StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.addOverlay(self.dKW2BackgroundOverlay!) })
                
            } else {
                if #available(iOS 13.0, *) {
                    mapView.pointOfInterestFilter = .includingAll
                } else {
                    mapView.showsPointsOfInterest = true
                }
                
                // Add a background overlay to hide apple maps names
                if let dKW2BackgroundOverlay = dKW2BackgroundOverlay {
                    StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.removeOverlay(dKW2BackgroundOverlay) })
                }
            }
            
            StaticQueues.mapviewOverlaysLock.runLocked({
                self.mapView.removeOverlay(self.dKW2Overlay)
                self.dKW2Overlay?.canReplaceMapContent = !self.mSettingsShowAppleMaps
                self.mapView.addOverlay(self.dKW2Overlay)
            })
        }
        
        let showNoaaCharts :Bool = settings?.object(forKey: Resources.SETTINGS_SHOW_NOAACHARTS) as? Bool ?? mSettingsShowNoaaCharts
        if mSettingsShowNoaaCharts != showNoaaCharts {
            mSettingsShowNoaaCharts = showNoaaCharts
            if mSettingsShowNoaaCharts{
                //				if !mapView.overlays.contains(where: {$0 == noaaOverlay!}) {
                StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.addOverlay(self.noaaOverlay!) })
                //				}
            } else {
                StaticQueues.mapviewOverlaysLock.runLocked({ self.mapView.removeOverlay(self.noaaOverlay!) })
            }
        }
        
        UIApplication.shared.isIdleTimerDisabled = settings?.object(forKey: Resources.SETTINGS_KEEP_SCREEN_ON) as? Bool ?? true
        
        //        let tailTrackVisible = settings?.object(forKey: Resources.SETTINGS_TAILTRACK_VISIBLE) as? Bool ?? Resources.SETTINGS_TAILTRACK_VISIBLE_DEFAULT
        //        if mSettingsShowTailTrack != tailTrackVisible {
        //            mSettingsShowTailTrack = tailTrackVisible
        //            if mSettingsShowTailTrack {
        //                // start static tracking service
        //                TrackingService.service.start()
        //                showTailTrack()
        //            } else {
        //                hideTailTrack()
        //                TrackingService.service.stop()
        //            }
        //        }
        
        var unitSpeed :String = settings?.string(forKey: Resources.SETTINGS_UNIT_SPEED) ?? Resources.SETTINGS_UNIT_SPEED_DEFAULT
        if unitSpeed == Resources.SETTINGS_UNIT_SPEED_KN {
            Units.mSpeedConversion = Units.SPEED_CONVERSION_TO_KNOTS 
        } else if unitSpeed == Resources.SETTINGS_UNIT_SPEED_KMH {
            Units.mSpeedConversion = Units.SPEED_CONVERSION_TO_KMH
        } else if unitSpeed == Resources.SETTINGS_UNIT_SPEED_MPH {
            Units.mSpeedConversion = Units.SPEED_CONVERSION_TO_MPH
        }
        
        unitSpeed = unitSpeed.splitToStringArray(by: " ")[0]
        if Units.mSogUnit != unitSpeed {	Units.mSogUnit = unitSpeed }
        
        let unitDistSmall = settings?.string(forKey: Resources.SETTINGS_UNIT_DISTANCE_SMALL) ?? Resources.SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT
        if Units.mUnitDistSmall != unitDistSmall { Units.mUnitDistSmall = unitDistSmall }
        
        let unitDistLarge = settings?.string(forKey: Resources.SETTINGS_UNIT_DISTANCE_LARGE) ?? Resources.SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT
        if Units.mUnitDistLarge != unitDistLarge { Units.mUnitDistLarge = unitDistLarge }
        
        // reload unit-related displays
        updateNavpointInfo()
        updateInstrumentInfo()
        aisOverlay?.updateDrawList()
        
        //// Distance circles
        
        let dcEnabled = settings?.object(forKey: Resources.SETTING_DISTANCE_CIRCLES_ENABLED) as? Bool ?? true
        if mSettingsDistanceCirclesEnabled != dcEnabled {
            mSettingsDistanceCirclesEnabled = dcEnabled
            drawDistanceCircles()
        }
        
        let dcCount :Int = (settings?.object(forKey: Resources.SETTING_DISTANCE_CIRCLES_COUNT)) as? Int ?? 5
        if dcCount != mSettingsDistanceCircleCount {
            mSettingsDistanceCircleCount = dcCount
            drawDistanceCircles()
        }
        
        let dcDistance :Int = settings?.object(forKey: Resources.SETTING_DISTANCE_CIRCLES_DISTANCE) as? Int ?? 1000
        if dcDistance != mSettingsDistanceCircleDistance {
            mSettingsDistanceCircleDistance = dcDistance
            drawDistanceCircles()
        }
        
        //// TCP Connection
        
        let tcpEnabled = settings?.object(forKey: Resources.SETTINGS_TCP_CONNECTION_ENABLED) as? Bool
        let tcpHost = settings?.string(forKey: Resources.SETTINGS_TCP_HOST)
        let tcpPort = settings?.object(forKey: Resources.SETTINGS_TCP_PORT) as? Int
        
        if let tcpEnabled = tcpEnabled,
           let tcpHost = tcpHost,
           let tcpPort = tcpPort {
            
            if tcpEnabled != NmeaTcpConnection.service.connectionEnabled {
                if tcpEnabled {
                    NmeaTcpConnection.service.startConnection(host: tcpHost, port: tcpPort)
                } else {
                    NmeaTcpConnection.service.stopConnection()
                }
            } else {
                if tcpEnabled {
                    if tcpHost != NmeaTcpConnection.service.host ||
                        tcpPort != NmeaTcpConnection.service.port  {
                        
                        NmeaTcpConnection.service.startConnection(host: tcpHost, port: tcpPort)
                    }
                }
            }
        }
        
        //// AIS Settings - ais settings are referenced from AllSettingsController statically.
        
        AisSettingsController.aisEnabled = settings?.object(forKey: Resources.SETTINGS_AIS_ENABLED) as? Bool ?? AisSettingsController.aisEnabled
        AisSettingsController.maxTargets = settings?.object(forKey: Resources.SETTINGS_AIS_MAX_TARGETS) as? Int ?? AisSettingsController.maxTargets
        
        /// Chart orientation
        
        var chartOrientationMode = settings?.integer(forKey: Resources.SETTING_CHART_ORIENTATION) ?? Resources.SETTING_CHART_ORIENTATION_DEFAULT_SETTING
        if chartOrientationMode != mChartOrientationMode {
            mChartOrientationMode = chartOrientationMode
            
            if (mChartOrientationMode == Resources.SETTING_CHART_ORIENTATION_NORTH_UP){
                courseUp = false
                // Set orientation back to north
                
                mapView.camera.heading = 0
                mapView.setCamera(mapView.camera, animated: true)
                
            } else if (mChartOrientationMode == Resources.SETTING_CHART_ORIENTATION_COURSE_UP){
                courseUp = true
                
                mapView.camera.heading = userCourse
                mapView.setCamera(mapView.camera, animated: true)
            }
        }
        
        let loadNetwork = settings?.object(forKey: Resources.SETTINGS_LOAD_WWNETWORK) as? Bool ?? true
        if mLoadNetwork != loadNetwork {
            mLoadNetwork = loadNetwork
            if mLoadNetwork {
                // room for (re)initialization
            }
        }
        
        //// (re)init overlays
        
        if mNetworkOverlayEnabled {
            //            self.networkOverlay = NetworkOverlay(self)
            self.networkOverlay?.linkToViewcontroller(self)
        } else {
            self.networkOverlay?.disableOverlay()

        }
        
        if mRouteOverlayEnabled {
            self.routeOverlay = RouteOverlay(self)
        }
        
        if mAISOverlayEnabled {
            self.aisOverlay = AISOverlay(self)
        }
        
        if mTrackOverlayEnabled {
            self.trackOverlay = TrackOverlay(self)
        }
        
    }
}

extension ViewController : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
