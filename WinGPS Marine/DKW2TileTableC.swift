//
//  StDKW2TileTableC.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2TileTableC {
    
    var tiles = [DKW2TileTableEntryC]()
    
    func getCount() -> Int {
        return tiles.count
    }
    
    func getTileDesc(index: Int) -> DKW2TileDesc {
        var desc = DKW2TileDesc()
        
        desc.offset = (UInt64(tiles[index].offsetMS) << 32) | UInt64(tiles[index].offsetLS)
        desc.size = tiles[index].size
        desc.width = Int(tiles[index].widthMinusOne) + 1
        desc.height = Int(tiles[index].heightMinusOne) + 1
        desc.tileIndex = index
        
        return desc
    }
    
    init(){
    }
    
    init(with tileTable :DKW2TileTable){
        let count = tileTable.getCount()
        
		tiles = [DKW2TileTableEntryC](generating: {_ in DKW2TileTableEntryC()}, count: count)
        
        for i in stride(from: count - 1, through: 0, by: -1) {
            tiles[i].size = tileTable.getSizeRGBUpd(index: i) + tileTable.getSizeAlphaUpd(index: i)
            
            let offsetUpd = tileTable.getOffsetUpd(index: i)
            tiles[i].offsetLS = UInt32(truncatingIfNeeded: offsetUpd & 0xffffffff)
            let offsetMS = UInt8(truncatingIfNeeded: (offsetUpd & 0x000000ff00000000) >> 32)
            tiles[i].offsetMS = offsetMS
            //            print(" heightMinusOne: ", i, " ",tileTable.getHeight(index: i) - 1)
            tiles[i].heightMinusOne = UInt8(tileTable.getHeight(index: i) - 1)
            //            print(" widthMinusOne: ", i, "  ", UInt8(tileTable.getWidth(index: i) - 1), " ", count, "  ", tiles[i].widthMinusOne)
            //            print(" widthMinusOne: ", i, " ",tileTable.getWidth(index: i) - 1)
            tiles[i].widthMinusOne = UInt8(tileTable.getWidth(index: i) - 1)
            
        }
    }
    
    class DKW2TileTableEntryC {
        var size :Int = 0
        var offsetLS :UInt32 = 0
        var offsetMS :UInt8 = 0
        var widthMinusOne :UInt8 = 0
        var heightMinusOne :UInt8 = 0
    }
}
