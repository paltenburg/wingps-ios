//
//  Units.swift
//  WinGPS Marine
//
//  Created by Standaard on 24/10/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation

let Units = UnitsClass.instance

class UnitsClass {

	static let instance = UnitsClass()

    let SPEED_CONVERSION_TO_KNOTS = 1.943844
    let SPEED_CONVERSION_TO_KMH = 3.6
    let SPEED_CONVERSION_TO_MPH = 2.23693629


	var mSogUnit = ""
	var mUnitDistSmall :String = Resources.SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT
	var mUnitDistLarge :String = Resources.SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT
	var mSpeedConversion :Double = 1

	private init(){
	}
    
    func unitToStr(_ unit :String) -> String {
        return unit.splitToStringArray(by: " ")[0]
    }
}
