//
//  ProductTableCell.swift
//  WinGPS Marine
//
//  Created by Standaard on 13/12/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class RouteTableCell :UITableViewCell {
	
	@IBOutlet weak var routeNameLabel: UILabel!

	@IBOutlet weak var routeDateLabel: UILabel!
	@IBOutlet weak var routeLengthLabel: UILabel!
	@IBOutlet weak var routeNrPointsLabel: UILabel!

	@IBOutlet weak var routeNameLabelDetailView: UILabel!
	@IBOutlet weak var dateLabelDetailView: UILabel!
	@IBOutlet weak var lengthLabelDetailView: UILabel!
	@IBOutlet weak var nPointsLabelDetailView: UILabel!
	
	@IBOutlet weak var editNameButton: UIButton!
	
	@IBOutlet weak var listItemView: UIView!
	@IBOutlet weak var expandedView: UIView!

	var controller :RouteManagerController?
	var route :Route?
	
//	init(_ controller :RouteManagerController, _ route :Route){
//		self.controller = controller
//		self.route = route
//		super.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "routeTableCell")
//	}
//
//	required init?(coder aDecoder: NSCoder) {
////		fatalError("init(coder:) has not been implemented")
//		super.init(coder: aDecoder)
//	}
	
	@IBAction func editNamePressed(_ sender: UIButton) {
		
		let alert = UIAlertController(title: "Edit name".localized, message: nil, preferredStyle: .alert)
//		alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: { action in
//			continueWith(.ResultCancelled, "")
//		}))
		alert.addAction(UIAlertAction(title: "cancel".localized, style: .cancel, handler: nil ))

		alert.addTextField(configurationHandler: { nameField in
			//			nameField.placeholder = self.routeNameLabelDetailView.text
			nameField.text = self.routeNameLabelDetailView.text
			
		})
		
		alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
			
			guard let newName = alert.textFields?[0].text else { return }
			
			print("edited name: \(newName)")
			
			self.route?.name = newName
			if self.route?.routePointAnnotations.count ?? 0 >= 1 {
				self.route?.routePointAnnotations[0].title = self.route?.name ?? "" + "\n1"
				self.route?.routePointAnnotations[0].updateAnnotationView()
				self.route?.needsSaving = true

			}
			
			self.routeNameLabel.text = newName
			self.routeNameLabelDetailView.text = newName
			
		}))
		
		getTopViewController()?.presentFromMain(alert, animated: true)
	}
	
//	func showEditRoutenameDialog(continueWith: @escaping (DialogResult, String) -> Void){
//
//		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
//
//		//		self.dismiss(animated: false, completion: nil)
//	}
	
//	var CSI :DownloadManagerController.ChartSetItem?
	
//	@IBAction func stopButton(_ sender: Any) {
//
//		let alert = UIAlertController(title: "dialog_title_cancel_download".localized, message: "dialog_message_cancel_download".localized, preferredStyle: .alert)
//		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
//		alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {action in
//			DownloadService.service.cancelDownload(pid: self.pid, mod: self.mod)
//
//			self.controller?.remove(cell: self)
//			//				self.updateCellState(state: .NotDownloaded)
//		}))
//		controller?.presentFromMain(alert, animated: true)
//	}
//
//	@IBAction func removeButton(_ sender: Any) {
//
//		let alert = UIAlertController(title: "dialog_title_remove_product".localized, message: "dialog_message_confirm_remove_product".localized, preferredStyle: .alert)
//		alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
//		alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {action in self.controller?.remove(cell: self)}))
//		controller?.presentFromMain(alert, animated: true)
//	}
	
	@IBAction func findButtonPressedAction(_ sender: UIButton) {
		
		if let route = route,
			route.routePoints.count > 0 {
            ViewController.instance?.moveMapTo(coord: route.routePoints[0].position, animated: false)
		}
		
		controller?.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func startRouteButtonPressedAction(_ sender: UIButton) {
		
		if let route = route,
			route.routePoints.count > 0{
			Routes.service.startRouteFromPoint(route.routePoints[0])
		}
		
		controller?.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func editRouteButtonPressedAction(_ sender: UIButton) {

		if let route = self.route{
			Routes.service.overlay?.startToEditRoute(route: route)
		}
		controller?.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func deleteRouteButtonPressedAction(_ sender: UIButton) {

        let alert = UIAlertController(title: "dialog_message_confirm_remove_route".localized, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "cancel".localized, style: .default))
        alert.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: { action in
            
            if let route = self.route{
                Routes.service.overlay?.remove(route)
            }
            self.controller?.selectedIndexPath = IndexPath(row: -1, section: 0)
            self.controller?.tableView.reloadData()
            
        }))
        
        self.controller?.presentFromMain(alert, animated: true)

    }
	
	@IBAction func reverseButtonPressedAction(_ sender: UIButton) {
		// At the moment, this button is set to hidden in UIBuilder

		controller?.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
}
