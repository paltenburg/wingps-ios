//
//  DKW2Utils.swift
//  MapDemo
//
//  Created by Standaard on 19/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

let StDKW2Utils_DKW2TILESIZEX = 256 // Horizontal DKW2 tile size [pixels
let StDKW2Utils_DKW2TILESIZEY = 256 // Vertical DKW2 tile size [pixels]
let StDKW2Utils_DKW2TAGMASK = 0xA6  // Encryption mask
let StDKW2Utils_MAXLICENSEMODULECOUNT = 48 // Maximum number of modules in licensed product.

let DKW2MIPMAPCOUNTAUTO = -1
let DKW2TILESIZEX = 256
let DKW2TILESIZEY = 256
let DKW2_MAX_LINCENSE_MODULE_COUNT = 48


