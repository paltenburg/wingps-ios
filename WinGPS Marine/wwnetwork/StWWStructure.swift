//
//  StWWStructure.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

enum WWManagerType {
    case UNKNOWN
    case NL_MINISTRYOFDEFENCE
    case NL_DGSM
    case NL_RVOB
    case ENERGYTRANSPORT
    case NL_MUNICIPALITY
    case FOREIGNMUNICIPALITY
    case MUNICIPALPORTADMINISTRATION
    case PORTADMINISTRATION
    case HOOGHEEMRAADSCHAP
    case HEEMRAADSCHAP
    case PORTAUTHORITY
    case NL_MINISTRYOFAGRICULTURE
    case NL_NS
    case PRIVATE
    case PROVINCE
    case BE_HETRIJK
    case RECREATION
    case NL_RIJKSWATERSTAAT
    case DE_WASSERUNDSCHIFFFAHRTSAMT
    case WATERSCHAP
    case FR_VNFDT
}


class StWWStructure :StWWLocation {
    
    var mAddress :String
    var  mAdministration :StWWAdministration
    var  mComplexName :String
    //    private final String mManDepartment
    //    private final String mManFaxNr
    //    private final String mManMailAddress
    //    private final String mManMailPlace
    //    private final String mManMailPostalCode
    //    private final String mManName
    //    private final String mManPhoneNr
    //    private final WWManagerType mManType
    //    private final String mManVisitAddress
    //    private final String mManVisitPlace
    var mPassageCount :Int
    var mPhoneNr :String
    var mPlace :String
    var mPostalCode :String
    var mTollable :Bool
    var mVHFRadioChannel :String
    
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aClass :WWLocationClass, aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
         aPlace :String, aPostalCode :String, aTollable :Bool, aVHFRadioChannel :String) {
        
        
        mAddress = aAddress
        mAdministration = aAdmin
        mComplexName = aComplexName
        ////        mManDepartment = aManDepartment
        ////        mManFaxNr = aManFaxNr
        ////        mManMailAddress = aManMailAddress
        ////        mManMailPlace = aManMailPlace
        ////        mManMailPostalCode = aManMailPostalCode
        ////        mManName = aManName
        ////        mManPhoneNr = aManPhoneNr
        ////        mManType = aManType
        ////        mManVisitAddress = aManVisitAddress
        ////        mManVisitPlace = aManVisitPlace
        mPassageCount = aPassageCount
        mPhoneNr = aPhoneNr
        mPlace = aPlace
        mPostalCode = aPostalCode
        mTollable = aTollable
        mVHFRadioChannel = aVHFRadioChannel
        
        ////        super(aNetwork, aGUID, aDaClass, aName, aPosGlb, aSection, aSectionPos)
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: aClass, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt)
        
    }
    
    /**
     * Returns info like the name and dimensions about this structure.
     *
     * @param aWaterLevelNAP The NAP waterlevel, which is needed to calculate depth and height for structures that are referenced to NAP.
     * @param aBoatDef The definition of the boat.
     * @param aPassable A boolean that determines if this structure is passable in reference to the given boatdef.
     * @return Info about this structure.
     */
    func getInfoText(aWaterLevelNAP :Float, aBoatDef :StLPBoatDef, aPassable :inout Bool) -> String {
        aPassable = true
        var result = self.mName.trim()
        if result.count == 0 {
            result = mPlace
        }
        return result
    }
    
    //    public String getManDepartment() {
    //        return mAdministration.mDepartment
    //    }
    //
    //    public String getManFaxNr() {
    //        return mAdministration.mFaxNr
    //    }
    //
    //    public String getManMailAdress() {
    //        return mAdministration.mMailAddress
    //    }
    //
    //    public String getManMailPlace() {
    //        return mAdministration.mMailPlace
    //    }
    //
    //    public String getManMailPostalCode() {
    //        return mAdministration.mMailPostalCode
    //    }
    //
    //    public String getManName() {
    //        return mAdministration.mName
    //    }
    //
    //    public String getManPhoneNr() {
    //        return mAdministration.mPhoneNr
    //    }
    //
    //    public WWManagerType getManType() {
    //        return mAdministration.mManType
    //    }
    //
    //    public String getManVisitAddress() {
    //        return mAdministration.mVisitAddress
    //    }
    //
    //    public String getManVisitPlace() {
    //        return mAdministration.mVisitPlace
    //    }
}

// MARK: Operated-structure related classes:

class StWWDate {
    
    let Day :UInt8
    let Month :UInt8
    let Date :Int
    
    init() {
        Day = 0
        Month = 0
        Date = 0
    }
    
    init(aDate :Int) {
        Date = aDate
        Day = UInt8(aDate >> 0 & 0xff)
        Month = UInt8(aDate >> 8 & 0xff)
    }
    
    init(aDay :Int, aMonth :Int) {
        Month = UInt8(aMonth)
        Day = UInt8(aDay)
        Date = (Int(Month) << 8) | Int(Day)
    }
}

class StWWDateRange {
    
    var End :StWWDate
    var Start :StWWDate
    
    init() {
        End = StWWDate()
        Start = StWWDate()
    }
    
    init(aStartAndEnd :Int64) {
        let E = Int(aStartAndEnd >> 0 & 0xffff)
        let S = Int(aStartAndEnd >> 16 & 0xffff)
        End = StWWDate(aDate: E)
        Start = StWWDate(aDate: S)
    }
    
    func getEnd() -> StWWDate {
        return End
    }
    
    func getStart() -> StWWDate {
        return Start
    }
}

class StWWTime {
    
    var Hour :UInt8
    var Minute :UInt8
    var Time :Int
    
    init() {
        Minute = 0
        Hour = 0
        Time = 0
    }
    
    init(aTime :Int) {
        Minute = UInt8(aTime >> 0 & 0xff)
        Hour = UInt8(aTime >> 8 & 0xff)
        Time = aTime
    }
    
    init(aHour :Int, aMinute :Int) {
        Minute = UInt8(aMinute)
        Hour = UInt8(aHour)
        Time = Int(Hour << 8) | Int(Minute)
    }
    
    func setTime(aTime :Int) {
        Time = aTime
        Minute = UInt8(aTime >> 0 & 0xff)
        Hour = UInt8(aTime >> 8 & 0xff)
    }
}

class StWWTimeRange {
    
    var End :StWWTime
    var Start :StWWTime
    
    init() {
        End = StWWTime()
        Start = StWWTime()
    }
    
    init(aStartAndEnd :Int64) {
        let E = Int(aStartAndEnd >> 0 & 0xffff)
        let S = Int(aStartAndEnd >> 16 & 0xffff)
        End = StWWTime(aTime: E)
        Start = StWWTime(aTime: S)
    }
}

class StWWOperationTime {
    
    let mDirection :WWDefs.WWDirection
    let mIndication :String
    let mShippingFlags :UInt8
    let mTimeRange :StWWTimeRange
    
    init(aDirection :WWDefs.WWDirection, aIndication :String, aShippingFlags :UInt8, aTimeRange :StWWTimeRange) {
        mDirection = aDirection
        mIndication = aIndication
        mShippingFlags = aShippingFlags
        mTimeRange = aTimeRange
    }
    
    func appliesToShippingClass(aCommercial :Bool) -> Bool {
        if aCommercial {
            return (mShippingFlags & WWDefs.WSF_COMMERCIAL) != 0
        } else {
            return (mShippingFlags & WWDefs.WSF_RECREATION) != 0
        }
    }
}

class StWWOperationDaySchedule {
    
    let mDayMask :UInt8
    var mTimes = [StWWOperationTime]()
    
    init(aDayMask :UInt8, aTimes :[StWWOperationTime]) {
        mDayMask = aDayMask
        //        mTimes = new ArrayList<>()
        mTimes.append(contentsOf: aTimes)
    }
    
    func findOperationTime(aTimeIndex :inout Int, aTime :StWWTime, aCommercial :Bool) -> Bool {
        let TimeCount = mTimes.count
        for i in 0 ..< TimeCount {
            var Time = mTimes[i]
            if (aTime.Time < Time.mTimeRange.End.Time && Time.appliesToShippingClass(aCommercial: aCommercial)) {
                aTimeIndex = i
                return true
            }
        }
        aTimeIndex = -1
        return false
    }
    
    func getTime(aIndex :Int) -> StWWOperationTime{
        return mTimes[aIndex]
    }
    
    func getTimeCount() -> Int {
        return mTimes.count
    }
}

class StWWOperationPeriod {
    
    static let StartOfYear = StWWDate(aDay: 1, aMonth: 1)
    static let EndOfYear = StWWDate(aDay: 31, aMonth: 12)
    
    var mDays :[StWWOperationDaySchedule?]
    var mDaySchedules = [StWWOperationDaySchedule]()
    var mDescription :String
    var mRange :StWWDateRange
    
    init(aRange :StWWDateRange, aDescription :String) {
        mDays = Array(repeating: nil , count: 7)
        
        //        mDaySchedules = new ArrayList<>()
        mDescription = aDescription
        mRange = aRange
    }
    
    func addDaySchedule(aDayMask :UInt8, aTimes :[StWWOperationTime]) -> StWWOperationDaySchedule {
        var DaySchedule = StWWOperationDaySchedule(aDayMask: aDayMask, aTimes: aTimes)
        mDaySchedules.append(DaySchedule)
        
        // Set the day-pointers of each day that is included in the mask.
        var Mask :UInt8 = 1
        for i in 0 ..< 7 {
            if ((aDayMask & Mask) == Mask) {
                mDays[i] = DaySchedule
            }
            Mask <<= 1
        }
        
        return DaySchedule
    }
    
    func getDaySchedule(aIndex :Int) -> StWWOperationDaySchedule {
        return mDaySchedules[aIndex]
    }
    
    func getDayScheduleCount() -> Int {
        return mDaySchedules.count
    }
    
    func getDayScheduleForDay(aDay :StDateTime.WeekDay) -> StWWOperationDaySchedule?{
        return mDays[aDay.rawValue] ?? nil
    }
    
    func inPeriod(aDate :StWWDate) -> Bool {
        if (mRange.getStart().Date <= mRange.getEnd().Date) {
            // A normal range.
            return aDate.Date >= mRange.Start.Date && aDate.Date <= mRange.End.Date
        } else {
            // A range from the start date to the end of the year, and from the beginning the year to the end date.
            return (aDate.Date >= mRange.Start.Date && aDate.Date <= StWWOperationPeriod.EndOfYear.Date) ||
                (aDate.Date >= StWWOperationPeriod.StartOfYear.Date && aDate.Date <= mRange.End.Date)
        }
    }
}

class StWWDay {
    
    static let EASTER_OFFSET_GOODFRIDAY :Int =    -2
    static let EASTER_OFFSET_ASCENSION :Int =     39
    static let EASTER_OFFSET_PENTECOST :Int =     49
    static let EASTER_OFFSET_CORPUSCHRISTI :Int = 60
    
    enum WWDay {
        case FIXEDDATE         // A day with a fixed date.
        
        // Days related to Easter.
        case GOODFRIDAY        // Good Friday           - Goede vrijdag       - 2 days before Easter.
        case EASTER            // Easter                - Eerste Paasdag      -
        case EASTERMONDAY      // Easter Monday         - Tweede Paasdag      - 1 day after Easter.
        case ASCENSIONDAY      // Ascension day         - Hemelvaartsdag      - 39 days after Easter.
        case PENTECOST         // Pentecost             - Eerste Pinksterdag  - 49 days after Easter.
        case PENTECOSTMONDAY   // Pentecost Monday      - Tweede Pinksterdag  - 50 days after Easter.
        case CORPUSCHRISTI     // Corpus Christi        - Corpus Christi      - 60 days after Easter.
        
        // Dutch days.
        case NLKONINGINNEDAG   // Dutch Queen' s Day    - Koninginnedag       - 30 April (29 if 30 is a Sunday).
        case NLBEVRIJDINGSDAG5 // Dutch Liberation Day  - Bevrijdingsdag      - 5 May (every 5 years).
        
        // German days.
        case DEBUSSUNDBETTAG    // German Buﬂ- und Bettag                    - Last Wednesday before 23 November.
    }
    
    private let mDay :WWDay       // Day.
    let mFixedDate :StWWDate // Fixed date (only valid if mDay == wdFixedDate).
    let mOffset :UInt8    // Day offset to mDay in days (e.g. mDay = wdGoodFriday, mDayOffset = -1: the day before Good Friday).
    
    init(aDay :WWDay, aFixedDate :StWWDate, aOffset :UInt8) {
        mDay = aDay
        mFixedDate = aFixedDate
        mOffset = aOffset
    }
    
    //    public boolean calcDate(StWWDate aDate, int aYear) {
    //        int Offset = mOffset
    //        if (mDay == WWDay.FIXEDDATE) {
    //            // If the date is 29 February, check if the year is a leap year.
    //            if (mFixedDate.Month == 2 && mFixedDate.Day == 29 && !StDateTime.isLeapYear(aYear))
    //                return false
    //            aDate = mFixedDate
    //        } else if (isEasterRelated()) {
    //            MutableInteger M = new MutableInteger()
    //            MutableInteger D = new MutableInteger()
    //
    //            if (!StDateTime.calcDateEaster(M, D, aYear))
    //                return false
    //            aDate.Month = (byte)M.getValue()
    //            aDate.Day = (byte)D.getValue()
    //            switch (mDay) {
    //                case GOODFRIDAY: Offset -= EASTER_OFFSET_GOODFRIDAY break
    //                case EASTERMONDAY: Offset += 1 break
    //                case ASCENSIONDAY: Offset += EASTER_OFFSET_ASCENSION break
    //                case PENTECOST: Offset += EASTER_OFFSET_PENTECOST break
    //                case PENTECOSTMONDAY: Offset += EASTER_OFFSET_PENTECOST + 1 break
    //                case CORPUSCHRISTI: Offset += EASTER_OFFSET_CORPUSCHRISTI break
    //            }
    //        } else if (mDay == WWDay.NLKONINGINNEDAG) {
    //            MutableInteger M = new MutableInteger()
    //            MutableInteger D = new MutableInteger()
    //            if (!StDateTime.calcDateDutchKoningsdag(M, D, aYear))
    //                return false
    //            aDate.Month = (byte)M.getValue()
    //            aDate.Day = (byte)D.getValue()
    //        } else if (mDay == WWDay.NLBEVRIJDINGSDAG5) {
    //            if (aYear <= 1945 || aYear % 5 != 0)
    //                return false
    //            aDate.Month = 5
    //            aDate.Day = 5
    //        } else if (mDay == WWDay.DEBUSSUNDBETTAG) {
    //            MutableInteger M = new MutableInteger()
    //            MutableInteger D = new MutableInteger()
    //
    //            StDateTime.calcDateGermanBussUndBetTag(M, D, aYear)
    //            aDate.Month = (byte)M.getValue()
    //            aDate.Day = (byte)D.getValue()
    //        } else
    //            return false
    //
    //        // Add the offset to the date.
    //        if (Offset != 0) {
    //            GregorianCalendar c = new GregorianCalendar()
    //            c.set(aYear, aDate.Month, aDate.Day)
    //            c.add(Calendar.DAY_OF_YEAR, Offset)
    //            aDate.Month = (byte)c.get(Calendar.MONTH)
    //            aDate.Day = (byte)c.get(Calendar.DAY_OF_MONTH)
    //        }
    //        return true
    //    }
    //
    //    public boolean isEasterRelated() {
    //        return (mDay == WWDay.GOODFRIDAY || mDay == WWDay.EASTER || mDay == WWDay.EASTERMONDAY || mDay == WWDay.ASCENSIONDAY ||
    //                mDay == WWDay.PENTECOST || mDay == WWDay.PENTECOSTMONDAY || mDay == WWDay.CORPUSCHRISTI)
    //    }
}

class StWWOperationRule {
    
    enum WWOperationRule {
        case NOOPERATION
        case ASNORMAL
        case ASWEEKDAY
        case ASSPECIFIED
    }
    
    let mDayOfWeek :StDateTime.WeekDay   // Set if mRule == worAsWeekDay.
    var mDays :[StWWDay]       // The days when the rule applies.
    let mDaySchedule :StWWOperationDaySchedule  // Set if mRule == worAsSpecified.
    let mNotAfter :Bool     // True if there is no operation after a specified time.
    let mNotAfterTime :StWWTime // Set if mNotAfter is true.
    let mNotBefore :Bool    // True if there is no operation before a specified time.
    let mNotBeforeTime :StWWTime // Set if mNotBefore is true.
    let mRule :WWOperationRule         // Operation rule.
    let mShippingFlags :UInt8 // Shipping flags.
    
    init(aDays :[StWWDay], aRule :WWOperationRule, aDayOfWeek :StDateTime.WeekDay,
         aDirection: WWDefs.WWDirection,
         aTimes :[StWWOperationTime], aNotAfter :Bool, aNotAfterTime :StWWTime, aNotBefore :Bool,
         aNotBeforeTime :StWWTime, aShippingFlags :UInt8) {
        
        mDayOfWeek = aDayOfWeek
        
        mDays = [StWWDay]()
        mDays.append(contentsOf: aDays)
        
        mDaySchedule = StWWOperationDaySchedule(aDayMask: UInt8(0), aTimes: aTimes)
        mNotAfter = aNotAfter
        mNotAfterTime = aNotAfterTime
        mNotBefore = aNotBefore
        mNotBeforeTime = aNotBeforeTime
        mRule = aRule
        mShippingFlags = aShippingFlags
    }
    
    func appliesToShippingClass(aCommercial :Bool) -> Bool {
        if (aCommercial) {
            return (mShippingFlags & WWDefs.WSF_COMMERCIAL) != 0
        } else {
            return (mShippingFlags & WWDefs.WSF_RECREATION) != 0
        }
    }
    
    func  getDay(aIndex :Int) -> StWWDay {
        return mDays[aIndex]
    }
    
    func getDayCount() -> Int {
        return mDays.count
    }
}

class StWWOperation {
    
    enum WWOperationType {
        case wotUnknown
        case wotSelfService
        case wotRemoteControl
        case wotSelfRemote
        case wotDetection
        case wotButton
        case wotMobileCrew
        case wotKeeper
        case wotKey
        case wotReadError
    }
    
    var Rule :StWWOperationRule?             // Rule (NULL if no rule applies).
    var Period :StWWOperationPeriod?          // Period (NULL if the day schedule from the rule is used).
    var DaySchedule :StWWOperationDaySchedule? // Day schedule (NULL if no operation).
    var Time :StWWOperationTime?              // Time (NULL if no operation).
    
    init() {
        Rule = nil
        Period = nil
        DaySchedule = nil
        Time = nil
    }
}

class StWWOperatedStructure :StWWStructure {
    
    var mOperationExplanation :String
    var mOperationPeriods = [StWWOperationPeriod]()
    var mOperationRules = [StWWOperationRule]()
    var mOperationType :StWWOperation.WWOperationType
    
    var mDelay :Int = -1
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aClass :WWLocationClass,
         aISRS :String, aName :String, aPosGlb :Double3,
         aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
         aPlace :String, aPostalCode :String, aVHFRadioChannel :String, aOperationExplanation :String,
         aOperationType :StWWOperation.WWOperationType, aOperationPeriods :[StWWOperationPeriod],
         aOperationRules :[StWWOperationRule]) {
        
        mDelay = -1
        mOperationExplanation = aOperationExplanation
        //        mOperationPeriods = new ArrayList<>(aOperationPeriods)
        //        mOperationRules = new ArrayList<>(aOperationRules)
        mOperationType = aOperationType
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: aClass, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt, aAdmin: aAdmin,
                   aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode,
                   // Tollable if:
                   aTollable: aOperationPeriods.count > 0 &&       //   1. at least one operation period,
                    aOperationType != StWWOperation.WWOperationType.wotRemoteControl &&                 //   2. not remote controlled,
                    aAdmin.mManType == WWManagerType.NL_MUNICIPALITY, //   3. managed by Dutch Municipality.
                   aVHFRadioChannel: aVHFRadioChannel)
        
    }
    
    //    public boolean findNearestOperation(StWWOperation aOperation, MutableDouble aDelay, long aDateTimeUTC, int aDelayMax, boolean aCommercial) {
    //
    //        aOperation.Rule = null
    //        aOperation.Period = null
    //        aOperation.DaySchedule = null
    //        aOperation.Time = null
    //        aDelay.setValue(0.0)
    //
    //        // If there are no periods and no rules, the structure will never be operated.
    //        if (mOperationPeriods.size() == 0 && mOperationRules.size() == 0)
    //            return false
    //
    //        // Convert the UTC date and time to the local time of the structure.
    //        GregorianCalendar DateTimeLocal = new GregorianCalendar()
    //        DateTimeLocal.setTimeInMillis(aDateTimeUTC)
    //
    //        // Separate the local date and time and convert the time to an SWWTime.
    //        StWWTime Time = new StWWTime(DateTimeLocal.get(Calendar.HOUR_OF_DAY), DateTimeLocal.get(Calendar.MINUTE))
    //
    //        // Calculate the number of days to check for operation.
    //        //   - limit the number of days to 366 (1 year) regardless of the passed aDelayMax.
    //        //   - take 2 additional hours to compensate for daylight saving (the calculation is done in local time).
    //        //int DelayMax = Math.min(366 * SECSPERDAY, aDelayMax) + 2 * SECSPERHOUR
    //        GregorianCalendar DateTimeLocalMax = (GregorianCalendar)DateTimeLocal.clone()
    //        DateTimeLocalMax.add(Calendar.SECOND, Math.min(366 * StDateTime.SECSPERDAY, aDelayMax) + 2 * StDateTime.SECSPERHOUR)
    ////        TDateTime DateLocalMax = DateTimeLocal + DelayMax / SECSPERDAY
    //
    //        int MaxDayCount = (int)((DateTimeLocalMax.getTimeInMillis() - DateTimeLocal.getTimeInMillis()) / StDateTime.MSECSPERDAY)
    ////        int MaxDayCount = Rnd(RndUp(DateLocalMax) - DateLocal)
    //
    //        // Find the first operation time from now until a year in the future, checking day by day.
    //        for (int i = 0 i < MaxDayCount i++) {
    //            // Check if the structure is operated on this particular day.
    //            if (findNearestOperationOnDay(aOperation, Time, DateTimeLocal, aCommercial)) {
    //                // Convert the time of operation to UTC.
    //
    //                DateTimeLocal.set(Calendar.HOUR_OF_DAY, Time.Hour)// = DateLocal + ((Time.Hour * 60) + Time.Minute) / (double)MINSPERDAY
    //                DateTimeLocal.set(Calendar.MINUTE, Time.Minute)
    //                long DateTimeUTC = DateTimeLocal.getTime().getTime()
    //
    //                // Calculate the delay in UTC.
    //                aDelay.setValue((DateTimeUTC - aDateTimeUTC) / 1000)
    //                return aDelay.getValue() <= aDelayMax
    //            }
    //
    //            // The structure is not operated on this day. Advance DateLocal and Time to the start of the next day.
    //            DateTimeLocal.add(Calendar.DAY_OF_YEAR, 1)
    //            Time.setTime(0)
    //        }
    //        return false
    //    }
    //
    //    private boolean findNearestOperationOnDay(StWWOperation aOperation, StWWTime aTime, GregorianCalendar aDate, boolean aCommercial) {
    //
    //        // Decode the date.
    //        int Year = aDate.get(Calendar.YEAR)
    //        StWWDate Date = new StWWDate(aDate.get(Calendar.DAY_OF_MONTH), aDate.get(Calendar.MONTH) + 1)
    //
    //        // Check if any rule applies to this day.
    //        StWWOperationPeriod Period
    //        StWWOperationDaySchedule DaySchedule
    //        StWWOperationRule Rule = findOperationRule(Date, Year, aCommercial)
    //        if (Rule != null) {
    //            switch (Rule.getRule()) {
    //                case NOOPERATION:
    //                    // No operation on this day.
    //                    return false
    //                case ASNORMAL:
    //                case ASWEEKDAY: {
    //                    // Find the day schedule that apply normally or on the specified day of the week.
    //                    Period = findOperationPeriod(Date)
    //                    if (Period == null)
    //                        return false
    //                    StDateTime.WeekDay WeekDay = Rule.getRule() == StWWOperationRule.WWOperationRule.ASNORMAL ? StDateTime.dayOfWeek(aDate) : Rule.getDayOfWeek()
    //                    DaySchedule = Period.getDayScheduleForDay(WeekDay)
    //                    if (DaySchedule == null)
    //                        return false
    //                    break
    //                }
    //                case ASSPECIFIED:
    //                    Period = null
    //                    DaySchedule = Rule.getDaySchedule()
    //                    break
    //                default:
    //                    return false
    //            }
    //            // Apply the 'not before' criterium if required.
    //            if (Rule.getNotBefore())
    //                aTime.setTime(Math.max(aTime.Time, Rule.getNotBeforeTime().Time))
    //        } else {
    //            // Find the day schedule that applies.
    //            Period = findOperationPeriod(Date)
    //            if (Period == null)
    //                return false
    //            StDateTime.WeekDay WeekDay = StDateTime.dayOfWeek(aDate)
    //            DaySchedule = Period.getDayScheduleForDay(WeekDay)
    //            if (DaySchedule == null)
    //                return false
    //        }
    //
    //        // Find the operation time.
    //        MutableInteger TimeIndex = new MutableInteger()
    //        if (!DaySchedule.findOperationTime(TimeIndex, aTime, aCommercial))
    //            return false
    //        StWWOperationTime Time = DaySchedule.getTime(TimeIndex.getValue())
    //        aTime.setTime(Math.max(aTime.Time, Time.getTimeRange().Start.Time))
    //
    //        // Apply the 'not after' criterium if required.
    //        if (Rule != null && Rule.getNotAfter() && aTime.Time >= Rule.getNotAfterTime().Time)
    //            return false
    //
    //        aOperation.Rule = Rule
    //        aOperation.Period = Period
    //        aOperation.DaySchedule = DaySchedule
    //        aOperation.Time = Time
    //        return true
    //    }
    //
    //    private StWWOperationPeriod findOperationPeriod(final StWWDate aDate) {
    //        for (int i = mOperationPeriods.size() - 1 i >= 0 i--) {
    //            final StWWOperationPeriod Period = mOperationPeriods.get(i)
    //            if (Period.inPeriod(aDate))
    //                return Period
    //        }
    //        return null
    //    }
    //
    //    private StWWOperationRule findOperationRule(final StWWDate aDate, int aYear, boolean aCommercial) {
    //
    //        StWWOperationRule Rule = null
    //        int RuleCount = mOperationRules.size()
    //        for (int i = 0 i < RuleCount i++) {
    //            Rule = mOperationRules.get(i)
    //            if (Rule.appliesToShippingClass(aCommercial)) {
    //                for (int d = Rule.getDayCount() - 1 d >= 0 d--) {
    //                    final StWWDay Day = Rule.getDay(d)
    //                    StWWDate Date = new StWWDate()
    //                    if (Day.calcDate(Date, aYear) && Date.Date == aDate.Date)
    //                        return Rule
    //                }
    //            }
    //        }
    //        return null
    //    }
    //
    //    public StWWOperationPeriod getOperationPeriod(int aIndex) {
    //        return mOperationPeriods.get(aIndex)
    //    }
    //
    //    public int getOperationPeriodCount() {
    //        return mOperationPeriods.size()
    //    }
    //
    //    public ArrayList<StWWOperationPeriod> getOperationPeriods() {
    //        return mOperationPeriods
    //    }
}


// MARK: Barrier

enum WWBarrierType {
    case WEIR
    case FLOODGATE
    case STORMSURGEBARRIER
}

class StWWBarrier :StWWStructure {
    
    var mDescPassages = [StWWBarrierPassage]()
    var mNormallyOpened :Bool
    var mType :WWBarrierType
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
         aPlace :String, aPostalCode :String, aVHFRadioChannel :String, aNormallyOpened :Bool,
         aType :WWBarrierType) {
        
        
        mNormallyOpened = aNormallyOpened
        //        mDescPassages = new ArrayList<>()
        mType = aType
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.BARRIER, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                   aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aTollable: false, aVHFRadioChannel: aVHFRadioChannel)
        
    }
    
    
    func createPassage(aHeightOpened :StWWVertValue, aNumber :Int, aRemark :String,
                       aThresholdDepth :StWWVertValue, aWidth :Float) -> StWWBarrierPassage {
        
        let Passage :StWWBarrierPassage = StWWBarrierPassage(aHeightOpened: aHeightOpened, aNumber: aNumber, aRemark: aRemark, aThresholdDepth: aThresholdDepth, aWidth: aWidth)
        mDescPassages.append(Passage)
        return Passage
    }
    
    func getDescPassage(aIndex :Int) -> StWWBarrierPassage {
        return mDescPassages[aIndex]
    }
    
    func getDescPassageCount() -> Int {
        return mDescPassages.count
    }
    
    class StWWBarrierPassage {
        
        let mHeightOpened :StWWVertValue
        let mNumber :Int
        let mRemark :String
        let mThresholdDepth :StWWVertValue
        let mWidth :Float
        
        init(aHeightOpened :StWWVertValue, aNumber :Int, aRemark :String,
             aThresholdDepth :StWWVertValue, aWidth :Float) {
            
            mHeightOpened = aHeightOpened
            mNumber = aNumber
            mRemark = aRemark
            mThresholdDepth = aThresholdDepth
            mWidth = aWidth
        }
    }
}

// MARK: Fixed Structure

enum WWFixedStructureType {
    case UNDEFINED
    case AQUEDUCT
    case TUNNEL
    case SIPHON
    case CABLE
    case PIPELINE
    case CONVEYERBELT
    case FORMERSTRUCTURE
    case POWERLINE
}

class StWWFixedStructure :StWWStructure {
    
    var mDescPassages = [StWWFixedStructurePassage]()
    var mType :WWFixedStructureType
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
         aPlace :String, aPostalCode :String, aVHFRadioChannel :String, aType :WWFixedStructureType) {
        
        //        mDescPassages = new ArrayList<>()
        mType = aType
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.FIXEDSTRUCTURE, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos,
                   aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aTollable: false, aVHFRadioChannel: aVHFRadioChannel)
        
    }
    
    func createPassage(aHeight :StWWVertValue, aNumber :Int, aRemark :String,
                       aThresholdDepth :StWWVertValue, aWidth :Float) -> StWWFixedStructurePassage {
        
        var Passage = StWWFixedStructurePassage(aHeight: aHeight, aNumber: aNumber, aRemark: aRemark, aThresholdDepth: aThresholdDepth, aWidth: aWidth)
        mDescPassages.append(Passage)
        return Passage
    }
    
    override func getInfoText(aWaterLevelNAP :Float, aBoatDef :StLPBoatDef, aPassable :inout Bool) -> String {
        var result = super.getInfoText(aWaterLevelNAP: aWaterLevelNAP, aBoatDef: aBoatDef, aPassable: &aPassable)
        if mType == WWFixedStructureType.POWERLINE {
            result = ""
        }
        return result
    }
    
    func getDescPassageCount() -> Int {
        return mDescPassages.count
    }
    
    func getDescPassage(aIndex :Int) -> StWWFixedStructurePassage {
        return mDescPassages[aIndex]
    }
    
    class StWWFixedStructurePassage {
        
        var mHeight :StWWVertValue
        var mNumber :Int
        var mRemark :String
        var mThresholdDepth :StWWVertValue
        var mWidth :Float
        
        init(aHeight :StWWVertValue, aNumber :Int, aRemark :String,
             aThresholdDepth :StWWVertValue, aWidth :Float) {
            
            mHeight = aHeight
            mNumber = aNumber
            mRemark = aRemark
            mThresholdDepth = aThresholdDepth
            mWidth = aWidth
        }
    }
}

// MARK: Operated Structure subtypes

enum WWBridgePassageType {
    case UNDEFINED
    case BASCULEBRIDGE
    case DOUBLEBASCULEBRIDGE
    case DOUBLESWINGBRIDGE
    case DRAWBRIDGE
    case DOUBLEDRAWBRIDGE
    case SWINGBRIDGE
    case ROLLINGBASCULEBRIDGE
    case DOUBLEROLLINGBASCULEBRIDGE
    case LIFTBRIDGE
    case TRAPDOORBRIDGE
    case PONTOONSWINGBRIDGE
    case PONTOONBRIDGE
    case RETRACTABLEBRIDGE
    case FIXEDSPAN
    case STRAUSSBASCULEBRIDGE
    case TRANSPORTERBRIDGE
}

class StWWBridge :StWWOperatedStructure {
    
    var mDescPassages = [StWWBridgePassage]()
    var mNormallyOpened :Bool
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection,
         aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
         aPostalCode :String, aVHFRadioChannel :String, aOperationExplanation :String,
         aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
         aOperationPeriods :[StWWOperationPeriod],
         aOperationRules :[StWWOperationRule], aNormallyOpened :Bool) {
        
        
        //        mDescPassages = new ArrayList<>()
        mNormallyOpened = aNormallyOpened
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.BRIDGE, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                   aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel,
                   aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules)
        
    }
    
    func createPassage(aHeightClosed :StWWVertValue, aHeightOpened :StWWVertValue, aISRS :String,
                       aNumber :Int, aRemark :String,
                       aThresholdDepth :StWWVertValue, aType :WWBridgePassageType, aWidth :Float) -> StWWBridgePassage {
        
        var Passage = StWWBridge.StWWBridgePassage(aHeightClosed: aHeightClosed, aHeightOpened: aHeightOpened, aISRS: aISRS, aNumber: aNumber, aRemark: aRemark, aThresholdDepth: aThresholdDepth, aType: aType, aWidth: aWidth)
        mDescPassages.append(Passage)
        return Passage
    }
    
    func getDescPassage(aIndex :Int) -> StWWBridgePassage {
        return mDescPassages[aIndex]
    }
    
    func getDescPassageCount() -> Int {
        return mDescPassages.count
    }
    
    //    public String getInfoText(float aWaterLevelNAP, StLPBoatDef aBoatDef, MutableBoolean aPassable) {
    //        String result = super.getInfoText(aWaterLevelNAP, aBoatDef, aPassable)
    //
    //        boolean BB = false
    //        float HeightClosed = 0
    //        float Depth = 0
    //        float MaxWidth = 0
    //        int Index = 0
    //
    //        for (int j = 0 j < mDescPassages.size() j++) {
    //            StWWBridgePassage BPassage = mDescPassages.get(j)
    //            if (BPassage.mType != WWBridgePassageType.UNDEFINED && BPassage.mType != WWBridgePassageType.FIXEDSPAN)
    //                BB = true
    //
    //            float Clearance = 0
    //            StWWVertValue PassageHeight = BPassage.mHeightClosed
    //            switch (PassageHeight.Datum) {
    //                case UNKNOWN: Clearance = StWWDefs.WW_INF break
    //                case NAP:
    //                    switch (PassageHeight.NAPType) {
    //                        case NAPDEFAULT   : Clearance = PassageHeight.Value - aWaterLevelNAP break
    //                        case NAPWATERLEVEL: Clearance = PassageHeight.Value - PassageHeight.NAPValue break
    //                        case CLEARANCE    : Clearance = PassageHeight.NAPValue break
    //                        default           : Clearance = StWWDefs.WW_INF
    //                    }
    //                    break
    //                default:
    //                    Clearance = PassageHeight.Value
    //            }
    //
    //            if (Clearance > HeightClosed) {
    //                HeightClosed = Clearance
    //                Index = j
    //            }
    //            MaxWidth = Math.max(MaxWidth, BPassage.mWidth)
    //        }
    //        String SubText = (BB) ? "BB " : ""
    //        if (HeightClosed != StWWDefs.WW_INF) {
    //            if (mDescPassages.get(Index).mHeightClosed.Datum == StWWVertValue.WWVertRefDatum.NAP) {
    //                SubText += "H" + String.format("%.1f", HeightClosed)
    //                float NAPLevel = 0
    //                switch (mDescPassages.get(Index).mHeightClosed.NAPType) {
    //                    case NAPDEFAULT   : NAPLevel = aWaterLevelNAP break
    //                    case NAPWATERLEVEL: NAPLevel = mDescPassages.get(Index).mHeightClosed.NAPValue break
    //                    case CLEARANCE    : NAPLevel = mDescPassages.get(Index).mHeightClosed.Value - mDescPassages.get(Index).mHeightClosed.NAPValue break
    //                }
    //                SubText += " (NAP " + String.format("%.1f", NAPLevel) + ") "
    //            } else {
    //                SubText += "H" + String.format("%.1f", HeightClosed) + " "
    //            }
    //        }
    //        if (MaxWidth != StWWDefs.WW_INF)
    //            SubText += "W" + String.format("%.1f", MaxWidth) + " "
    //        if (getVHFRadioChannel().length() > 0)
    //            SubText += "VHF" + getVHFRadioChannel()
    //        if (SubText.length() > 0)
    //            result += "\n" + SubText
    //
    //        if (MaxWidth < aBoatDef.getWidth() || (!BB && HeightClosed < aBoatDef.getHeight()))
    //            aPassable.setValue(false)
    //
    //        return result
    //    }
    
    class StWWBridgePassage {
        
        let mHeightClosed :StWWVertValue
        let mHeightOpened :StWWVertValue
        let mISRS :String
        let mNumber :Int
        let mRemark :String
        let mThresholdDepth :StWWVertValue
        let mType :WWBridgePassageType
        let mWidth :Float
        
        
        init(aHeightClosed :StWWVertValue, aHeightOpened :StWWVertValue, aISRS :String,
             aNumber :Int, aRemark :String,
             aThresholdDepth :StWWVertValue, aType :WWBridgePassageType, aWidth :Float) {
            
            mHeightClosed = aHeightClosed
            mHeightOpened = aHeightOpened
            mISRS = aISRS
            mNumber = aNumber
            mRemark = aRemark
            mThresholdDepth = aThresholdDepth
            mType = aType
            mWidth = aWidth
        }
    }
}


enum WWNonPhysBarrierType {
    case wnbtUndefined
    case wnbtOnewayTraffic // Start of one-way traffic.
}

class StWWNonPhysBarrier :StWWOperatedStructure {
    
    class StWWNonPhysBarrierPassage {
        let mNumber :Int
        let mRemark :String
        init(aNumber :Int, aRemark :String){
            mNumber = aNumber
            mRemark = aRemark
        }
    }
    
    var mDescPassages = [StWWNonPhysBarrierPassage]()
    var mNonPhysBarrierType :WWNonPhysBarrierType
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection,
         aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
         aPostalCode :String, aVHFRadioChannel :String, aOperationExplanation :String,
         aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
         aOperationPeriods :[StWWOperationPeriod],
         aOperationRules :[StWWOperationRule],
         aType :WWNonPhysBarrierType ){
        
        //        mDescPassages = new ArrayList<>()
        mNonPhysBarrierType = aType
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.NONPHYSBARRIER, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                   aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel,
                   aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules)
    }
    
    func createPassage(aNumber :Int, aRemark :String) -> StWWNonPhysBarrierPassage {
        let Passage = StWWNonPhysBarrierPassage(aNumber: aNumber, aRemark: aRemark)
        mDescPassages.append(Passage)
        return Passage
    }
}

class StWWLock :StWWOperatedStructure {
    
    var mDescPassages = [StWWLockChamber]()
    var mFall :Float
    var mNormallyOpened :Bool
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String,
         aName :String, aPosGlb :Double3, aSection :StWWSection,
         aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
         aPostalCode :String, aVHFRadioChannel :String,
         aOperationExplanation :String,
         aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
         aOperationPeriods :[StWWOperationPeriod],
         aOperationRules :[StWWOperationRule], aFall :Float, aNormallyOpened :Bool) {
        
        //        mDescPassages = [StWWLockChamber]()
        mFall = aFall
        mNormallyOpened = aNormallyOpened
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.LOCK, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                   aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel,
                   aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules)
    }
    
    func createLockChamber(aChamberLength1 :Float, aChamberLength2 :Float, aChamberLengthTotal :Float, aChamberWidth :Float,
                           aDepthInside :StWWVertValue, aDepthMiddle :StWWVertValue, aDepthOutside :StWWVertValue,
                           aHeight :StWWVertValue, aHasTidalDoors :Bool, aISRS :String,
                           aLockingLength :Float,
                           aLockingLengthLowTide :Float, aMinGateWidth :Float, aName :String, aNumber :Int,
                           aRemark :String) -> StWWLockChamber {
        
        var LockChamber = StWWLockChamber(aChamberLength1: aChamberLength1, aChamberLength2: aChamberLength2, aChamberLengthTotal: aChamberLengthTotal, aChamberWidth: aChamberWidth, aDepthInside: aDepthInside, aDepthMiddle: aDepthMiddle,
                                          aDepthOutside: aDepthOutside, aHeight: aHeight, aHasTidalDoors: aHasTidalDoors, aISRS: aISRS, aLockingLength: aLockingLength, aLockingLengthLowTide: aLockingLengthLowTide, aMinGateWidth: aMinGateWidth, aName: aName, aNumber: aNumber, aRemark: aRemark)
        mDescPassages.append(LockChamber)
        return LockChamber
    }
    
    func getDescPassage(aIndex :Int) -> StWWLockChamber {
        return mDescPassages[aIndex]
    }
    
    func getDescPassageCount() -> Int {
        return mDescPassages.count
    }
    
    //    public String getInfoText(float aWaterLevelNAP, StLPBoatDef aBoatDef, MutableBoolean aPassable) {
    //        String result = super.getInfoText(aWaterLevelNAP, aBoatDef, aPassable)
    //        String SubText = ""
    //
    //        float MaxWidth = 0
    //        float MaxLength = 0
    //
    //        for (int j = 0; j < mDescPassages.size(); j++) {
    //            StWWLockChamber LockChamber = mDescPassages.get(j)
    //
    //            float WidthMax = Math.min(LockChamber.getChamberWidth(), LockChamber.getMinGateWidth())
    //            MaxWidth = Math.max(MaxWidth, WidthMax)
    //
    //            float LockingLen = LockChamber.getLockingLength()
    //            if (LockChamber.getHasTidalDoors()) {
    //                // Because no tidal information is available currently, use the shortest of two lengths.
    //                LockingLen = Math.min(LockingLen, LockChamber.getLockingLengthLowTide())
    //            }
    //            MaxLength = Math.max(MaxLength, LockingLen)
    //        }
    //
    //        SubText = "W" + String.format("%.1f", MaxWidth) + " "
    //        SubText += "L" + String.format("%.0f", MaxLength)
    //        if (getVHFRadioChannel().length() > 0)
    //            SubText += " VHF" + getVHFRadioChannel()
    //        if (SubText.length() > 0)
    //            result += "\n" + SubText
    //
    //        if (MaxWidth < aBoatDef.getWidth() || (MaxLength < aBoatDef.getLength()))
    //            aPassable.setValue(false)
    //
    //        return result
    //    }
    
    class StWWLockChamber {
        
        var mChamberLength1 :Float       // Length of the first chamber part.
        var mChamberLength2 :Float        // Length of the second chamber part.
        var mChamberLengthTotal :Float    // Total length of the chamber (kolklengte).
        var mChamberWidth :Float          // Chamber width [m].
        var mDepthInside :StWWVertValue          // Threshold depth of inside gate.
        var mDepthMiddle :StWWVertValue           // Threshold depth of middle gate.
        var mDepthOutside :StWWVertValue          // Threshold depth of outside gate.
        var mHasTidalDoors :Bool        // True if the lock chamber has tidal doors.
        var mHeight :StWWVertValue                // Height.
        var mLockingLength :Float         // Locking length. When the lock chamber has tidal doors, it contains the locking length at high tide.
        var mLockingLengthLowTide :Float  // Locking length at low tide when the lock chamber has tidal doors.
        var mMinGateWidth :Float          // Minimum gate width (minimale wijdte hoofd).
        var mName :String                  // Chamber name.
        var mNumber :Int               // Passage number
        var mRemark :String                // Remark
        var mISRS :String                // ISRS
        
        init(aChamberLength1 :Float, aChamberLength2 :Float, aChamberLengthTotal :Float, aChamberWidth :Float,
             aDepthInside :StWWVertValue, aDepthMiddle :StWWVertValue, aDepthOutside :StWWVertValue,
             aHeight :StWWVertValue, aHasTidalDoors :Bool, aISRS :String,
             aLockingLength :Float,
             aLockingLengthLowTide :Float, aMinGateWidth :Float, aName :String, aNumber :Int,
             aRemark :String) {
            
            mChamberLength1 = aChamberLength1
            mChamberLength2 = aChamberLength2
            mChamberLengthTotal = aChamberLengthTotal
            mChamberWidth = aChamberWidth
            mDepthInside = aDepthInside
            mDepthMiddle = aDepthMiddle
            mDepthOutside = aDepthOutside
            mHasTidalDoors = aHasTidalDoors
            mHeight = aHeight
            mLockingLength = aLockingLength
            mLockingLengthLowTide = aLockingLengthLowTide
            mMinGateWidth = aMinGateWidth
            mName = aName
            mNumber = aNumber
            mRemark = aRemark
            mISRS = aISRS
        }
        
    }
}

class StWWTunnel :StWWOperatedStructure {
    
    class WWTunnelTube{
        
        var mDepth :StWWVertValue
        var mHeight :StWWVertValue
        var mLength :Float
        var mNumber :Int
        var mRemark :String
        var mWidth :Float
        init(aDepth :StWWVertValue, aHeight :StWWVertValue, aLength :Float,
             aNumber :Int, aRemark :String, aWidth :Float){
            mDepth = aDepth
            mHeight = aHeight
            mLength = aLength
            mNumber = aNumber
            mRemark = aRemark
            mWidth = aWidth
        }
    }
    
    var mDescPassages = [WWTunnelTube]()
    
    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aISRS :String,
         aName :String, aPosGlb :Double3, aSection :StWWSection,
         aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
         aPostalCode :String, aVHFRadioChannel :String,
         aOperationExplanation :String,
         aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
         aOperationPeriods :[StWWOperationPeriod],
         aOperationRules :[StWWOperationRule]){
        
        super.init(aNetwork: aNetwork, aGUID: aGUID, aDataIndex: aDataIndex, aClass: WWLocationClass.SHIPTUNNEL, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos,
                   aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel,
                   aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules)
        
        //        mDescPassages = new ArrayList<>()
    }
    
    func createTunnelTube(aDepth :StWWVertValue, aHeight :StWWVertValue, aLength :Float,
                          aNumber :Int, aRemark :String, aWidth :Float) -> WWTunnelTube {
        
        var Tube = WWTunnelTube(aDepth: aDepth, aHeight: aHeight, aLength: aLength, aNumber: aNumber, aRemark: aRemark, aWidth: aWidth)
        mDescPassages.append(Tube)
        return Tube
    }
}

