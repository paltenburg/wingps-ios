//
//  WWDefs.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 21/01/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class WWWaterbodyID {

    static let NORTHSEA :Int = 0                // North Sea.
    static let WADDENSEA :Int = 1               // Wadden Sea (The Netherlands, Germany, Denmark).
    static let IJSSELMEERANDMARKERMEER :Int = 2 // IJsselmeer and Markermeer (The Netherlands).
    static let INLANDNETHERLANDS :Int = 3      // Inland water bodies in The Netherlands.
    static let INLANDBELGIUMLUXEMBURG :Int = 4  // Inland water bodies in Belgium and Luxemburg.
    static let INLANDGERMANY :Int = 5          // Inland water bodies in Germany.
    static let INLANDFRANCE :Int = 6            // Inland water bodies in France.
    static let INLANDGREATBRITAIN :Int = 7      // Inland water bodies in Great Britain.
    static let BALTICSEA :Int = 8              // Baltic Sea.
    static let INLANDDANMARK :Int = 9           // Inland water bodies in Danmark.
    static let INLANDSWEDEN :Int = 10           // Inland water bodies in Sweden.
    static let DONAU :Int = 11

    private var mID :Int

    init(_ aWaterBodyID :Int) {
        mID = aWaterBodyID
    }

    init(aWaterBodyID :WWWaterbodyID) {
        mID = aWaterBodyID.getValue()
    }

    func getValue() -> Int {
        return mID
    }

//        FIRST      = NorthSea,
//        bLast       = wwbInlandSweden,

}

public class WWDefs {

    static let WW_INF :Float = 1e37

    // Shipping flags
    static let WSF_RECREATION :UInt8 = 0x01;
    static let WSF_COMMERCIAL :UInt8 = 0x02;
    static let WSF_ALL        :UInt8 = WSF_RECREATION | WSF_COMMERCIAL;

    static let WWBF_NORTHSEA :Int = 1 << WWWaterbodyID.NORTHSEA
    static let WWBF_WADDENSEA :Int = 1 << WWWaterbodyID.WADDENSEA
    static let WWBF_IJSSELMEERANDMARKERMEER :Int = 1 << WWWaterbodyID.IJSSELMEERANDMARKERMEER
    static let WWBF_INLANDNETHERLANDS :Int = 1 << WWWaterbodyID.INLANDNETHERLANDS
    static let WWBF_INLANDBELGIUMLUXEMBURG :Int = 1 << WWWaterbodyID.INLANDBELGIUMLUXEMBURG
    static let WWBF_INLANDGERMANY :Int = 1 << WWWaterbodyID.INLANDGERMANY
    static let WWBF_INLANDFRANCE :Int = 1 << WWWaterbodyID.INLANDFRANCE
    static let WWBF_INLANDGREATBRITAIN :Int = 1 << WWWaterbodyID.INLANDGREATBRITAIN
    static let WWBF_BALTICSEA :Int = 1 << WWWaterbodyID.BALTICSEA
    static let WWBF_INLANDDANMARK :Int = 1 << WWWaterbodyID.INLANDDANMARK
    static let WWBF_INLANDSWEDEN :Int = 1 << WWWaterbodyID.INLANDSWEDEN
    static let WWBF_DONAU :Int = 1 << WWWaterbodyID.DONAU

//    public static final int WWBF_ALL = WWBF_NORTHSEA | WWBF_WADDENSEA | WWBF_IJSSELMEERANDMARKERMEER | WWBF_INLANDNETHERLANDS | WWBF_INLANDBELGIUMLUXEMBURG |
//            WWBF_INLANDGERMANY | WWBF_INLANDFRANCE | WWBF_INLANDGREATBRITAIN | WWBF_BALTICSEA | WWBF_INLANDDANMARK | WWBF_INLANDSWEDEN | WWBF_DONAU;
//    public static final int WWBF_INLAND = WWBF_INLANDNETHERLANDS | WWBF_INLANDBELGIUMLUXEMBURG | WWBF_INLANDGERMANY | WWBF_INLANDFRANCE |
//            WWBF_INLANDGREATBRITAIN | WWBF_INLANDDANMARK | WWBF_INLANDSWEDEN | WWBF_DONAU;

    static private let ChartWaterbodies :[ChartWaterbody] = [

            // INLAND NL
            ChartWaterbody(196659, 4096, WWBF_INLANDNETHERLANDS), // INLAND NL 2014
            ChartWaterbody(196675, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2015
            ChartWaterbody(196684, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2016
            ChartWaterbody(196693, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2017
            ChartWaterbody(196699, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2018
            ChartWaterbody(196705, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2019
            ChartWaterbody(196711, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2020
            ChartWaterbody(196717, 0, WWBF_INLANDNETHERLANDS), // INLAND NL 2021
            ChartWaterbody(196651, 1, WWBF_INLANDNETHERLANDS), // NL FR 2012
            ChartWaterbody(196664, 0, WWBF_INLANDNETHERLANDS), // REGIO NL
// INLAND BE
            ChartWaterbody(196660, 8, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2014
            ChartWaterbody(196681, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2016
            ChartWaterbody(196690, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2017
            ChartWaterbody(196696, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2018
            ChartWaterbody(196702, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2019
            ChartWaterbody(196708, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2020
            ChartWaterbody(196714, 256, WWBF_INLANDBELGIUMLUXEMBURG), // INLAND BE 2021
// INLAND FR
            ChartWaterbody(196673, 16, WWBF_INLANDFRANCE), // INLAND N FR 2015
            ChartWaterbody(196681, 8, WWBF_INLANDFRANCE), // INLAND N FR 2016
            ChartWaterbody(196690, 8, WWBF_INLANDFRANCE), // INLAND N FR 2017
            ChartWaterbody(196696, 8, WWBF_INLANDFRANCE), // INLAND N FR 2018
            ChartWaterbody(196702, 8, WWBF_INLANDFRANCE), // INLAND N FR 2019
            ChartWaterbody(196708, 8, WWBF_INLANDFRANCE), // INLAND N FR 2020
            ChartWaterbody(196714, 8, WWBF_INLANDFRANCE), // INLAND N FR 2021
            ChartWaterbody(196678, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2015
            ChartWaterbody(196690, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2017
            ChartWaterbody(196696, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2018
            ChartWaterbody(196702, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2019
            ChartWaterbody(196708, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2020
            ChartWaterbody(196714, 16, WWBF_INLANDFRANCE), // INLAND Z FR 2021
            ChartWaterbody(196702, 512, WWBF_INLANDFRANCE), // INLAND REM 2019
            ChartWaterbody(196708, 512, WWBF_INLANDFRANCE | WWBF_INLANDGERMANY), // INLAND REM 2020 (also include germany)
            ChartWaterbody(196714, 512, WWBF_INLANDFRANCE | WWBF_INLANDGERMANY), // INLAND REM 2021 (also include germany)
        // INLAND DE
            ChartWaterbody(196673, 128, WWBF_INLANDGERMANY), // INLAND NWD 2015
            ChartWaterbody(196681, 1, WWBF_INLANDGERMANY), // INLAND NWD 2016
            ChartWaterbody(196690, 1, WWBF_INLANDGERMANY), // INLAND NWD 2017
            ChartWaterbody(196696, 1, WWBF_INLANDGERMANY), // INLAND NWD 2018
            ChartWaterbody(196702, 1, WWBF_INLANDGERMANY), // INLAND NWD 2019
            ChartWaterbody(196708, 1, WWBF_INLANDGERMANY), // INLAND NWD 2020
            ChartWaterbody(196714, 1, WWBF_INLANDGERMANY), // INLAND NWD 2021
            ChartWaterbody(196678, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2015
            ChartWaterbody(196681, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2016
            ChartWaterbody(196690, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2017
            ChartWaterbody(196696, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2018
            ChartWaterbody(196702, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2019
            ChartWaterbody(196708, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2020
            ChartWaterbody(196714, 2, WWBF_INLANDGERMANY), // INLAND ZWD 2021
            ChartWaterbody(196681, 4, WWBF_INLANDGERMANY), // INLAND NOD 2016
            ChartWaterbody(196690, 4, WWBF_INLANDGERMANY), // INLAND NOD 2017
            ChartWaterbody(196696, 4, WWBF_INLANDGERMANY), // INLAND NOD 2018
            ChartWaterbody(196702, 4, WWBF_INLANDGERMANY), // INLAND NOD 2019
            ChartWaterbody(196708, 4, WWBF_INLANDGERMANY), // INLAND NOD 2020
            ChartWaterbody(196714, 4, WWBF_INLANDGERMANY), // INLAND NOD 2021
            ChartWaterbody(196681, 32, WWBF_INLANDGERMANY), // INLAND DONAU1 2016
            ChartWaterbody(196702, 512, WWBF_INLANDGERMANY), // INLAND REM 2019
// DONAU
            ChartWaterbody(196690, 32, WWBF_DONAU), // DONAU1 2017
            ChartWaterbody(196690, 64, WWBF_DONAU), // DONAU2 2017
            ChartWaterbody(196690, 128, WWBF_DONAU), // DONAU3 2017
            ChartWaterbody(196702, 32, WWBF_DONAU), // DONAU1 2019
            ChartWaterbody(196702, 64, WWBF_DONAU), // DONAU2 2019
            ChartWaterbody(196702, 128, WWBF_DONAU), // DONAU3 2019
            ChartWaterbody(196708, 32, WWBF_DONAU), // DONAU1 2020
            ChartWaterbody(196708, 64, WWBF_DONAU), // DONAU2 2020
            ChartWaterbody(196708, 128, WWBF_DONAU), // DONAU3 2020
            ChartWaterbody(196714,   32, WWBF_DONAU), // DONAU1 2021
            ChartWaterbody(196714,   64, WWBF_DONAU), // DONAU2 2021
            ChartWaterbody(196714,  128, WWBF_DONAU), // DONAU3 2021
    ]

    
    // Enum for direction
    enum WWDirection {
//        wwdAll       = 0,      // Both directions.
//        wwdPositive  = 1,      // Direction is in fairway direction.
//        wwdNegative  = 2,      // Direction is opposite of fairway direction.
//        wwdForceLong = 0x7fffffff
        case wwdAll      // Both directions.
        case wwdPositive     // Direction is in fairway direction.
        case wwdNegative      // Direction is opposite of fairway direction.
        case wwdForceLong
    }

    static func chartToWaterbody(_ aProductID :Int, _ aModule :Int) -> Int {
        for ChartWaterbody in ChartWaterbodies {
            if ChartWaterbody.ProductID == aProductID {
                if ChartWaterbody.Module == aModule || ChartWaterbody.Module == 0 {
                    return ChartWaterbody.Waterbody
                }
            }
        }
        return 0
    }

//    public static int checkAllowedWaterbody(int aWantedMask, int aAllowedMask) {
//        int Result = 0;
//        if ((aWantedMask & WWBF_NORTHSEA) == WWBF_NORTHSEA && (aAllowedMask & WWBF_NORTHSEA) == WWBF_NORTHSEA)
//            Result |= WWBF_NORTHSEA;
//        if ((aWantedMask & WWBF_WADDENSEA) == WWBF_WADDENSEA && (aAllowedMask & WWBF_WADDENSEA) == WWBF_WADDENSEA)
//            Result |= WWBF_WADDENSEA;
//        if ((aWantedMask & WWBF_IJSSELMEERANDMARKERMEER) == WWBF_IJSSELMEERANDMARKERMEER && (aAllowedMask & WWBF_IJSSELMEERANDMARKERMEER) == WWBF_IJSSELMEERANDMARKERMEER)
//            Result |= WWBF_IJSSELMEERANDMARKERMEER;
//        if ((aWantedMask & WWBF_INLANDNETHERLANDS) == WWBF_INLANDNETHERLANDS && (aAllowedMask & WWBF_INLANDNETHERLANDS) == WWBF_INLANDNETHERLANDS)
//            Result |= WWBF_INLANDNETHERLANDS;
//        if ((aWantedMask & WWBF_INLANDBELGIUMLUXEMBURG) == WWBF_INLANDBELGIUMLUXEMBURG && (aAllowedMask & WWBF_INLANDBELGIUMLUXEMBURG) == WWBF_INLANDBELGIUMLUXEMBURG)
//            Result |= WWBF_INLANDBELGIUMLUXEMBURG;
//        if ((aWantedMask & WWBF_INLANDGERMANY) == WWBF_INLANDGERMANY && (aAllowedMask & WWBF_INLANDGERMANY) == WWBF_INLANDGERMANY)
//            Result |= WWBF_INLANDGERMANY;
//        if ((aWantedMask & WWBF_INLANDFRANCE) == WWBF_INLANDFRANCE && (aAllowedMask & WWBF_INLANDFRANCE) == WWBF_INLANDFRANCE)
//            Result |= WWBF_INLANDFRANCE;
//        if ((aWantedMask & WWBF_INLANDGREATBRITAIN) == WWBF_INLANDGREATBRITAIN && (aAllowedMask & WWBF_INLANDGREATBRITAIN) == WWBF_INLANDGREATBRITAIN)
//            Result |= WWBF_INLANDGREATBRITAIN;
//        if ((aWantedMask & WWBF_BALTICSEA) == WWBF_BALTICSEA && (aAllowedMask & WWBF_BALTICSEA) == WWBF_BALTICSEA)
//            Result |= WWBF_BALTICSEA;
//        if ((aWantedMask & WWBF_INLANDDANMARK) == WWBF_INLANDDANMARK && (aAllowedMask & WWBF_INLANDDANMARK) == WWBF_INLANDDANMARK)
//            Result |= WWBF_INLANDDANMARK;
//        if ((aWantedMask & WWBF_INLANDSWEDEN) == WWBF_INLANDSWEDEN && (aAllowedMask & WWBF_INLANDSWEDEN) == WWBF_INLANDSWEDEN)
//            Result |= WWBF_INLANDSWEDEN;
//        if ((aWantedMask & WWBF_DONAU) == WWBF_DONAU && (aAllowedMask & WWBF_DONAU) == WWBF_DONAU)
//            Result |= WWBF_DONAU;
//        return Result;
//    }
//
//    public static int WWWaterbodyIDToMask(WWWaterbodyID aWaterBodyID) {
//        return 1 << aWaterBodyID.getValue();
//    }
//
    private struct ChartWaterbody {
//        int ProductID;
//        int Module;
//        int Waterbody;
//
//        private ChartWaterbody(int aProductID, int aModule, int aWaterbody) {
//            ProductID = aProductID;
//            Module = aModule;
//            Waterbody = aWaterbody;
//        }
        var ProductID :Int
        var Module :Int
        var Waterbody :Int
        
        init(_ aProductID :Int, _ aModule :Int, _ aWaterbody :Int) {
            ProductID = aProductID
            Module = aModule
            Waterbody = aWaterbody
        }
    }
}

