//
//  StDateTime.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 19/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

public class StDateTime {

//    public static final int MSECSPERDAY = 86400000;
//    public static final int SECSPERDAY = 86400;
//    public static final int SECSPERHOUR = 3600;

    enum WeekDay :Int {
        case SUNDAY = 0
        case MONDAY = 1
        case TUESDAY = 2
        case WEDNESDAY = 3
        case THURSDAY = 4
        case FRIDAY = 5
        case SATURDAY = 6
    }

//    public static boolean calcDateDutchKoningsdag(MutableInteger aMonth, MutableInteger aDay, int aYear) {
//        if (aYear < 1949)
//            return false;
//        else if (aYear < 2014) {
//            GregorianCalendar c = new GregorianCalendar();
//            c.set(aYear, 4, 30);
//            if (dayOfWeek(c.getTime()) == WeekDay.SUNDAY) {
//                if (aYear < 1980) {
//                    aMonth.setValue(5);
//                    aDay.setValue(1);
//                } else {
//                    aMonth.setValue(4);
//                    aDay.setValue(29);
//                }
//            } else {
//                aMonth.setValue(4);
//                aDay.setValue(30);
//            }
//        } else {
//            GregorianCalendar c = new GregorianCalendar();
//            c.set(aYear, 4, 27);
//            if (dayOfWeek(c.getTime()) == WeekDay.SUNDAY) {
//                aMonth.setValue(4);
//                aDay.setValue(26);
//            } else {
//                aMonth.setValue(4);
//                aDay.setValue(27);
//            }
//        }
//
//        return true;
//    }
//
//    public static boolean calcDateEaster(MutableInteger aMonth, MutableInteger aDay, int aYear) {
//        if (aYear < 1583 || aYear > 4099)
//            return false;
//
//        // Calculate PFM date.
//        int FirstDig = aYear / 100;  // First 2 digits of year.
//        int Remain19 = aYear % 19;   // Remainder of aYear / 19.
//        int Temp = (FirstDig - 15) / 2 + 202 - 11 * Remain19;
//        switch (FirstDig) {
//            case 21:
//            case 24:
//            case 25:
//            case 27:
//            case 28:
//            case 29:
//            case 30:
//            case 31:
//            case 32:
//            case 34:
//            case 35:
//            case 38:
//                Temp -= 1; break;
//            case 33:
//            case 36:
//            case 37:
//            case 39:
//            case 40:
//                Temp -= 2; break;
//        }
//        Temp %= 30;
//
//        int tA = Temp + 21;
//        if (Temp == 29)
//            tA -= 1;
//        if (Temp == 28 && Remain19 > 10)
//            tA -= 1;
//
//        // Find the next Sunday.
//        int tB = (tA - 19) % 7;
//
//        int tC = (40 - FirstDig) % 4;
//        if (tC == 3)
//            tC += 1;
//        if (tC > 1)
//            tC += 1;
//
//        Temp = aYear % 100;
//        int tD = (Temp + Temp / 4) % 7;
//
//        int tE = ((20 - tB - tC - tD) % 7) + 1;
//        int Day = tA + tE;
//        int Month;
//
//        // Return the date.
//        if (Day > 31) {
//            Day -= 31;
//            Month = 4;
//        } else
//            Month = 3;
//
//        aDay.setValue(Day);
//        aMonth.setValue(Month);
//
//        return true;
//    }
//
//    public static void calcDateGermanBussUndBetTag(MutableInteger aMonth, MutableInteger aDay, int aYear) {
//        aMonth.setValue(11);
//        GregorianCalendar c = new GregorianCalendar();
//        c.set(aYear, 11, 23);
//        int Day = 20;
//        switch (dayOfWeek(c.getTime())) {
//            case SUNDAY: Day = 19; break;
//            case MONDAY: Day = 18; break;
//            case TUESDAY: Day = 17; break;
//            case WEDNESDAY: Day = 16; break;
//            case THURSDAY: Day = 22; break;
//            case FRIDAY: Day = 21; break;
//            case SATURDAY:
//            default: Day = 20; break;
//        }
//        aDay.setValue(Day);
//    }
//
//    private static int dayDiff(WeekDay aDay1, WeekDay aDay2) {
//        int Day1 = aDay1.ordinal();
//        int Day2 = aDay2.ordinal();
//        if (Day1 < Day2)
//            Day1 += 7;
//        return Day1 - Day2;
//    }
//
//    public static WeekDay dayOfWeek(GregorianCalendar aCalendar) {
//        return dayOfWeek(aCalendar.getTime().getTime());
//    }
//
//    public static WeekDay dayOfWeek(Date aDateTime) {
//        return dayOfWeek(aDateTime.getTime());
//    }
//
//    public static WeekDay dayOfWeek(long aDateTime) {
//        GregorianCalendar c = new GregorianCalendar();
//        c.setTimeInMillis(aDateTime);
//        int day = c.get(Calendar.DAY_OF_WEEK);
//        switch (day) {
//            case Calendar.SUNDAY:
//                return WeekDay.SUNDAY;
//            case Calendar.MONDAY:
//                return WeekDay.MONDAY;
//            case Calendar.TUESDAY:
//                return WeekDay.TUESDAY;
//            case Calendar.WEDNESDAY:
//                return WeekDay.WEDNESDAY;
//            case Calendar.THURSDAY:
//                return WeekDay.THURSDAY;
//            case Calendar.FRIDAY:
//                return WeekDay.FRIDAY;
//            case Calendar.SATURDAY:
//            default:
//                return WeekDay.SATURDAY;
//        }
//    }
//
//    public static String dayToStrShort(WeekDay aDay) {
//        DateFormatSymbols dfs = DateFormatSymbols.getInstance();
//        String[] shortdays = dfs.getShortWeekdays();
//        return shortdays[aDay.ordinal() + 1];
//    }
//
//    private static WeekDay findDay(byte aWeekDayFlags, MutableInteger aDay) {
//        int Day = aDay.getValue();
//        while ((aWeekDayFlags & (1 << Day)) == 0) {
//            Day++;
//            if (Day > 6)
//                Day = 0;
//        }
//        WeekDay Result = WeekDay.values()[Day];
//        Day++;
//        if (Day > 6)
//            Day = 0;
//        aDay.setValue(Day);
//        return Result;
//    }
//
//    private static WeekDay findExcludedDay(byte aWeekDayFlags, MutableInteger aDay) {
//        int Day = aDay.getValue();
//        while ((aWeekDayFlags & (1 << Day)) != 0) {
//            Day++;
//            if (Day > 6)
//                Day = 0;
//        }
//        WeekDay Result = WeekDay.values()[Day];
//        Day++;
//        if (Day > 6)
//            Day = 0;
//        aDay.setValue(Day);
//        return Result;
//    }
//
//    public static boolean isLeapYear(int aYear) {
//        return ((aYear % 4 == 0) && ((aYear % 100 != 0) || (aYear % 400 == 0)));
//    }
//
//    public static String monthToStr(int aMonth) {
//        DateFormatSymbols dfs = DateFormatSymbols.getInstance();
//        String[] months = dfs.getMonths();
//        return months[aMonth];
//    }
//
//    public static String weekDayFlagsToStr(byte aWeekDayFlags, WeekDay aFirstDay, String aDailyStr, String aDailyExcStr) {
//        int BitCount = 0;
//        for (int i = 0; i < 7; i++) {
//            if ((aWeekDayFlags & (1 << i)) != 0)
//                BitCount++;
//        }
//
//        MutableInteger Day = new MutableInteger(aFirstDay.ordinal());
//        switch (BitCount) {
//            case 0:
//                return "";
//            case 1: {
//                WeekDay Day1 = findDay(aWeekDayFlags, Day);
//                return dayToStrShort(Day1);
//            }
//            case 2: {
//                WeekDay Day1 = findDay(aWeekDayFlags, Day);
//                WeekDay Day2 = findDay(aWeekDayFlags, Day);
//                return dayToStrShort(Day1) + ", " + dayToStrShort(Day2);
//            }
//            case 3: {
//                WeekDay Day1 = findDay(aWeekDayFlags, Day);
//                WeekDay Day2 = findDay(aWeekDayFlags, Day);
//                WeekDay Day3 = findDay(aWeekDayFlags, Day);
//                if (dayDiff(Day3, Day1) == 2)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day3);
//                else
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + ", " + dayToStrShort(Day3);
//            }
//            case 4: {
//                WeekDay Day1 = findDay(aWeekDayFlags, Day);
//                WeekDay Day2 = findDay(aWeekDayFlags, Day);
//                WeekDay Day3 = findDay(aWeekDayFlags, Day);
//                WeekDay Day4 = findDay(aWeekDayFlags, Day);
//                if (dayDiff(Day4, Day1) == 3)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day4);
//                else if (dayDiff(Day3, Day1) == 2)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day3) + ", " + dayToStrShort(Day4);
//                else if (dayDiff(Day4, Day2) == 2)
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + "-" + dayToStrShort(Day4);
//                else return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + ", " + dayToStrShort(Day3) + ", " + dayToStrShort(Day4);
//            }
//            case 5: {
//                WeekDay Day1 = findDay(aWeekDayFlags, Day);
//                WeekDay Day2 = findDay(aWeekDayFlags, Day);
//                WeekDay Day3 = findDay(aWeekDayFlags, Day);
//                WeekDay Day4 = findDay(aWeekDayFlags, Day);
//                WeekDay Day5 = findDay(aWeekDayFlags, Day);
//                if (dayDiff(Day5, Day1) == 4)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day5);
//                else if (dayDiff(Day4, Day1) == 3)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day4) + ", " + dayToStrShort(Day5);
//                else if (dayDiff(Day5, Day2) == 3)
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + "-" + dayToStrShort(Day5);
//                else if (dayDiff(Day3, Day1) == 2)
//                    return dayToStrShort(Day1) + "-" + dayToStrShort(Day3) + ", " + dayToStrShort(Day4) + ", " + dayToStrShort(Day5);
//                else if (dayDiff(Day4, Day2) == 2)
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + "-" + dayToStrShort(Day4) + ", " + dayToStrShort(Day5);
//                else if (dayDiff(Day5, Day3) == 2)
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + ", " + dayToStrShort(Day3) + "-" + dayToStrShort(Day5);
//                else
//                    return dayToStrShort(Day1) + ", " + dayToStrShort(Day2) + ", " + dayToStrShort(Day3) + ", " + dayToStrShort(Day4) + ", " + dayToStrShort(Day5);
//            }
//            case 6: {
//                WeekDay Day1 = findExcludedDay(aWeekDayFlags, Day);
//                return aDailyExcStr + " " + dayToStrShort(Day1);
//            }
//            default:
//                return aDailyStr;
//        }
//    }
}
