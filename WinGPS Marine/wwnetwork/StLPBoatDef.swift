//
//  StLPBoatDef.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 22/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StLPBoatDef {

    let mCommercial :Bool // True if the vessel is a commercial vessel.
    let mDraught :Float      // Draught of the boat from the waterline [m].
    let mHeight :Float       // Height of the boat from the waterline [m].
    let mLength :Float       // Length of the boat [m].
    let mWidth :Float        // Width of the boat [m].

    init() {
        mCommercial = false
        mDraught = 0.0
        mHeight = 0.0
        mLength = 0.0
        mWidth = 0.0
    }
}
