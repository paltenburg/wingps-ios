//
//  WWNetwork.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 21/01/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class WWNetwork {
    
    private static let WWNETWORKFILEID :String = "Stentec Waterway Network"
    private static let WWNETWORKFILEVERSION1_0_0_1 :StVersion = StVersion(1, 0, 0, 1)
    private static let WWNETWORKFILEVERSION1_0_0_2 :StVersion = StVersion(2, 0, 0, 1)
    private static let WWNETWORKFILEVERSION1_0_0_3 :StVersion = StVersion(3, 0, 0, 1)
    
    var mLoading :Bool
    private var mNetworkLoader :WWNetworkLoader?
    
    private var mAdministrations = [StWWAdministration]()
    private var mFairways = [StWWFairway]()
    private var mLocations = [StWWLocation]()
    private var mNodes = [StWWNode]()
    private var mSections = [StWWSection]()
    private var mStructures = [StWWStructure]()
    
    // singleton to return the same network everytime
    public static var network :WWNetwork?
    
    // This is a handler to notify when databases have been loaded
    //    private var mainHandler :((ErrorCode, Bool, String) -> ())?
    private var mMainHandler :(() -> ())?
    
    init( aFilename :String, aMainHandler :(() -> ())? ) {
        
        mMainHandler = aMainHandler
        
        //        mAdministrations = new ArrayList<>();
        //        mFairways = new ArrayList<>();
        //        mLocations = new ArrayList<>();
        //        mNodes = new ArrayList<>();
        //        mSections = new ArrayList<>();
        //        mStructures = new ArrayList<>();
        
        mLoading = true
        loadFromFile(aFilename)
    }
    
    func cancelLoading() {
        if mLoading {
            /*mNetworkLoader.cancel() */
        }
    }
    
    func createAdministration(aDepartment :String, aFaxNr :String, aMailAddress :String, aMailPlace :String,
                              aMailPostalCode :String, aName :String, aPhoneNr :String,
                              aManType :WWManagerType, aVisitAddress :String, aVisitPlace :String,
                              aWebsite :String) -> StWWAdministration {
        
        let Admin = StWWAdministration(aDepartment: aDepartment, aFaxNr: aFaxNr, aMailAddress: aMailAddress, aMailPlace: aMailPlace, aMailPostalCode: aMailPostalCode,
                                       aName: aName, aPhoneNr: aPhoneNr, aManType: aManType, aVisitAddress: aVisitAddress, aVisitPlace: aVisitPlace, aWebsite: aWebsite)
        mAdministrations.append(Admin)
        return Admin
    }
    
    func createBarrier(aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
                       aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                       aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
                       aPlace :String, aPostalCode :String, aVHFRadioChannel :String, aNormallyOpened :Bool,
                       aType :WWBarrierType) -> StWWBarrier {
        
        var Barrier = StWWBarrier(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                                  aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName/*, aManDepartment, aManFaxNr, aManMailAddress, aManMailPlace, aManMailPostalCode, aManName,
                                  aManPhoneNr, aManType, aManVisitAddress, aManVisitPlace*/, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode,
                                  aVHFRadioChannel: aVHFRadioChannel, aNormallyOpened: aNormallyOpened, aType: aType)
        
        mLocations.append(Barrier)
        mStructures.append(Barrier)
        
        return Barrier
    }
    
    
    func createBridge(aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
                      aPosGlb :Double3, aSection :StWWSection,
                      aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                      aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
                      aPostalCode :String, aVHFRadioChannel :String, aOperationExplanation :String,
                      aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
                      aOperationPeriods :[StWWOperationPeriod],
                      aOperationRules :[StWWOperationRule], aNormallyOpened :Bool) -> StWWBridge {
    
        var Bridge = StWWBridge(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos,
                                aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount,
                                aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel, aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, /*timezone? */
                                aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules, aNormallyOpened: aNormallyOpened);
        
        mLocations.append(Bridge)
        mStructures.append(Bridge)
        return Bridge
    }
    
    func createFairway(aGUID :StUUID, aName :String, aMarksInFDir :Bool) -> StWWFairway {
        var Fairway :StWWFairway = StWWFairway(aGUID: aGUID, aName: aName, aMarksInFDir: aMarksInFDir)
        mFairways.append(Fairway)
        return Fairway
    }
    
    func createFixedStructure(aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
                              aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                              aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String,
                              aPlace :String, aPostalCode :String, aVHFRadioChannel :String, aType :WWFixedStructureType) -> StWWFixedStructure {
        
        
        var FixedStructure = StWWFixedStructure(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName,
                                                aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount,
                                                aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel, aType: aType)
        mLocations.append(FixedStructure)
        mStructures.append(FixedStructure)
        return FixedStructure
    }
    
    func createLock(aGUID :StUUID, aDataIndex :Int, aISRS :String,
                    aName :String, aPosGlb :Double3, aSection :StWWSection,
                         aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                         aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
                         aPostalCode :String, aVHFRadioChannel :String,
                    aOperationExplanation :String,
                         aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
                         aOperationPeriods :[StWWOperationPeriod],
                         aOperationRules :[StWWOperationRule], aFall :Float, aNormallyOpened :Bool) -> StWWLock {
        

        var Lock = StWWLock(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos,
                            aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount,
                            aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel, aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, /*timezone? */
                            aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules, aFall: aFall, aNormallyOpened: aNormallyOpened)
        mLocations.append(Lock)
        mStructures.append(Lock)
        return Lock
    }
    
    func createNode(aGUID :StUUID, aDataIndex :Int, aPosGlb :Double3) -> StWWNode {
        var Node = StWWNode(aGUID, aDataIndex, aPosGlb)
        mNodes.append(Node)
        return Node
    }
    
    func createNonPhysBarrier(aGUID :StUUID, aDataIndex :Int, aISRS :String, aName :String,
                              aPosGlb :Double3, aSection :StWWSection,
                              aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                              aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
                              aPostalCode :String, aVHFRadioChannel :String, aOperationExplanation :String,
                              aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
                              aOperationPeriods :[StWWOperationPeriod],
                              aOperationRules :[StWWOperationRule],
                              aType :WWNonPhysBarrierType) -> StWWNonPhysBarrier {
        
        
        var NonPhysBarrier = StWWNonPhysBarrier(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos,
                                                aGenerateWpt: aGenerateWpt, aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel,
                                                aOperationExplanation: aOperationExplanation, aOperationType: aOperationType, /*aTimeZone,*/ aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules, aType: aType)
        mStructures.append(NonPhysBarrier)
        mLocations.append(NonPhysBarrier)
        return NonPhysBarrier
    }
    
    func createSection(aGUID :StUUID, aDataIndex :Int, aHeadNode :StWWNode, aTailNode :StWWNode, aBoundsCenterGlb :Double3,
                       aBoundsRadius :Double, aLen :Double, aName :String, aWaterBodyID :WWWaterbodyID) -> StWWSection {
        
        let Section = StWWSection(aGUID: aGUID, aDataIndex: aDataIndex, aHeadNode: aHeadNode, aTailNode: aTailNode, aBoundsCenterGlb: aBoundsCenterGlb, aBoundsRadius: aBoundsRadius, aLen: aLen, aName: aName, aWaterBodyID: aWaterBodyID)
        mSections.append(Section)
        aHeadNode.addSection(Section)
        aTailNode.addSection(Section)
        return Section
    }
    
    func createTunnel(aGUID :StUUID, aDataIndex :Int, aISRS :String,
                            aName :String, aPosGlb :Double3, aSection :StWWSection,
                            aSectionPos :Double, aGenerateWpt :Bool, aAdmin :StWWAdministration,
                            aAddress :String, aComplexName :String, aPassageCount :Int, aPhoneNr :String, aPlace :String,
                            aPostalCode :String, aVHFRadioChannel :String,
                            aOperationExplanation :String,
                            aOperationType :StWWOperation.WWOperationType, /* aTimeZone? */
                            aOperationPeriods :[StWWOperationPeriod],
                            aOperationRules :[StWWOperationRule]) -> StWWTunnel
    {
        var Tunnel = StWWTunnel(aNetwork: self, aGUID: aGUID, aDataIndex: aDataIndex, aISRS: aISRS, aName: aName, aPosGlb: aPosGlb, aSection: aSection, aSectionPos: aSectionPos, aGenerateWpt: aGenerateWpt,
                                aAdmin: aAdmin, aAddress: aAddress, aComplexName: aComplexName, aPassageCount: aPassageCount, aPhoneNr: aPhoneNr, aPlace: aPlace, aPostalCode: aPostalCode, aVHFRadioChannel: aVHFRadioChannel, aOperationExplanation: aOperationExplanation,
                                aOperationType: aOperationType, /*aTimeZone, */ aOperationPeriods: aOperationPeriods, aOperationRules: aOperationRules)
        mStructures.append(Tunnel)
        mLocations.append(Tunnel)
        return Tunnel
    }
    
    //    public int findAdministration(WWManagerType aType, final String aName){
    //        for (int i = 0; i < mAdministrations.size(); i++) {
    //            if (mAdministrations.get(i).mManType == aType && mAdministrations.get(i).mName.equals(aName)) {
    //                return i;
    //            }
    //        }
    //        return -1;
    //    }
    //
    //    public NearestSectionPosResult findNearestSectionPos(final Geo2 aPosGeoRad) {
    //        NearestSectionPosResult Result = new NearestSectionPosResult();
    //        Result.Section = null;
    //        Result.SectionPos = 0.0;
    //        Result.DistToSection = 1e32;
    //        Double3 PosGlb = StGeodeticCalibration.geoToGlobal(aPosGeoRad);
    //        MutableDouble F = new MutableDouble();
    //        for (int i = mSections.size() - 1; i >= 0; i--) {
    //            final StWWSection Section = mSections.get(i);
    //            if (StMath.distSqr(PosGlb, Section.getBoundsCenterGlb()) < StMath.sqr(Section.getBoundsRadius() + Result.DistToSection)) {
    //                StWWSectionPoint Point = Section.getSectionPoint(Section.getSectionPointCount() - 1);
    //                for (int j = Section.getSectionPointCount() - 2; j >= 0; j--) {
    //                    final StWWSectionPoint PointNext = Point;
    //                    Point = Section.getSectionPoint(j);
    //                    double D = StMath.distToLineSegment(Point.getPosGlb(), PointNext.getPosGlb(), PosGlb, F);
    //                    if (D < Result.DistToSection) {
    //                        Result.SectionPos = StMath.lerp(Point.getDistance(), PointNext.getDistance(), F.getValue());
    //                        Result.Section = Section;
    //                        Result.DistToSection = D;
    //                    }
    //                }
    //            }
    //        }
    //        return Result;
    //    }
    //
    //    public boolean findSection(@NonNull UUID aSectionGUID, MutableInteger aIndex) {
    //        for (int i = 0; i < mSections.size(); i++) {
    //            if (mSections.get(i).getGUID().compareTo(aSectionGUID) == 0) {
    //                aIndex.setValue(i);
    //                return true;
    //            }
    //        }
    //        return false;
    //    }
    //
    //    public boolean findStructure(@NonNull UUID aStructureGUID, MutableInteger aIndex) {
    //        for (int i = 0; i < mStructures.size(); i++) {
    //            if (mStructures.get(i).getGUID().compareTo(aStructureGUID) == 0) {
    //                aIndex.setValue(i);
    //                return true;
    //            }
    //        }
    //        return false;
    //    }
    
        static func getNetwork() -> WWNetwork? { // synchronized
            return self.network
        }
    
    //    public static synchronized StWWNetwork getNetwork(Context aContext) {
    //    if (mNetwork == null) {
    //    String WWFilename = StActManager.getActManager().getWWFilePath(aContext, true);//String WWFilename = Environment.getExternalStorageDirectory() + "/stentec/WaterwayNetwork.stwwn";
    //    mNetwork = new StWWNetwork(WWFilename, null);
    //    }
    //    return mNetwork;
    //    }
    
    static func getNetwork(aFilename :String, mainHandler :(()->())?) -> WWNetwork {
        if let network = self.network {
            return network
        } else {
            let network = WWNetwork(aFilename: aFilename, aMainHandler: mainHandler)
            self.network = network
            return network
        }
    }
    
    static func reloadNetwork(aFilename :String, aMainHandler :(()->())?) -> WWNetwork {
        let network = WWNetwork(aFilename: aFilename, aMainHandler: aMainHandler)
        WWNetwork.network = network
        return network
    }
    
    func getAdministration(aIndex :Int) -> StWWAdministration {
        return mAdministrations[aIndex]
    }
    
    func getAdministrationCount() -> Int {
        return mAdministrations.count
    }
    
    func getNode(_ aIndex :Int) -> StWWNode {
        return mNodes[aIndex]
    }
    
    func getNodeCount() -> Int {
        return mNodes.count
    }
    
    func  getSection(aIndex :Int) -> StWWSection {
        return mSections[aIndex]
    }
    
    func getSectionCount() -> Int {
        return mSections.count
    }
    
    func getStructure(aIndex :Int) -> StWWStructure {
        return mStructures[aIndex]
    }
    
    func getStructureCount() -> Int {
        return mStructures.count
    }
    
    private func loadFromFile(_ aFilename :String) {
        
        mNetworkLoader = WWNetworkLoader(aNetwork: self, aFilename: aFilename)
        mNetworkLoader?.execute()
    }
    
    //    public void setMainHandler(Handler aHandler) {
    //        mMainHandler = aHandler;
    //    }
    
    //    private class WWNetworkLoader extends AsyncTask<Void, Void, Boolean> {
    class WWNetworkLoader {
        
        //    private StError mError;
        private var mError :String = ""
        private var mFilename :String
        private var mWWNetwork :WWNetwork
        
        var isCancelled :Bool = false
        
        private var mFileHandle :FileHandle? = nil
        
        init(aNetwork :WWNetwork, aFilename :String) {
            //            mError = new StError(StError.ER_OK);
            mFilename = aFilename
            mWWNetwork = aNetwork
            
            isCancelled = false
            
            do {
                try self.mFileHandle = FileHandle(forReadingFrom: StUtils.getUrlByFilename(mFilename))
            } catch {
                mError = StError.ER_FILEREADINGERROR
                return
            }
        }
        
        func execute(){
            
//            StaticQueues.networkLoadingQueue.async(group: nil, qos: .background, flags: .barrier) {
                StaticQueues.networkLoadingQueue.sync() {
     
                //                FileChannel fc = null;
                //                FileInputStream fs = null;
                //                try {
                //                fs = new FileInputStream(mFilename);
                //                fc = fs.getChannel();
                
                
                
                
                //                 Read the header and version
                //                ByteBuffer fileheader = StUtils.readBuffer(fc, WWNETWORKFILEID.length() + 4);
                
                do {
                    
                    if let fileHandle = self.mFileHandle {
                        
                        //                    testing:
                        //                        let fileSize = getFileSize((StUtils.getUrlByFilename(self.mFilename)).path)
                        //                        dout("getFileSize: \(fileSize ?? 0)")
                        //                        let Reader :WWNetworkReader1_0_0_3 = WWNetworkReader1_0_0_3(aWWNetwork: self.mWWNetwork, aLoader: self)
                        //                        try Reader.read(aFh: fileHandle, aError: &self.mError)
                        
                        var fileheader = try fileHandle.readData(ofLength: WWNETWORKFILEID.count + 4) as NSData
                        
                        var pos = 0
                        // this check is skipped:
                        //                if (!StUtils.checkHeader(WWNETWORKFILEID, fileheader)) { // this check is skipped
                        //                    mError.setErrorCode(StError.ER_FILEHEADER);
                        //                    return false;
                        //                }
                        pos += WWNETWORKFILEID.count
                        
                        //                 Read version and file header
                        //                int v = fileheader.getInt();
                        let v :UInt32 = try getUInt32FromNSDataBuffer(buffer: fileheader, start: pos)
                        let Version = StVersion(v)
                        
                        //                Check the version, only version 1.0.0.3 is supported.
                        let networkVersion = Version.getVersion()
                        let version1001 :Int = WWNETWORKFILEVERSION1_0_0_1.getVersion()
                        let version1002 :Int = WWNETWORKFILEVERSION1_0_0_2.getVersion()
                        if /*Version.getVersion() != WWNETWORKFILEVERSION1_0_0_1.getVersion() &&
                             Version.getVersion() != WWNETWORKFILEVERSION1_0_0_2.getVersion() &&*/
                            Version.getVersion() != WWNETWORKFILEVERSION1_0_0_3.getVersion() {
                            
                            if Version.compareTo(WWNETWORKFILEVERSION1_0_0_3) > 0 {
                                self.mError = StError.ER_FILEVERSIONNEWER
                                return
                            } else if Version.compareTo(WWNETWORKFILEVERSION1_0_0_3) < 0 {
                                self.mError = StError.ER_FILEVERSIONUNSUPPORTED
                                return
                            }
                        }
                        
                        do {
                            //                            if Version.compareTo(version: WWNETWORKFILEVERSION1_0_0_2) <= 0 {
                            //                                let Reader :StWWNetworkReader1_0_0_1 = StWWNetworkReader1_0_0_1(mWWNetwork, this)
                            //                                try Reader.read(fileHandle, &mError)
                            //
                            //                            } else
                            if Version.compareTo(WWNETWORKFILEVERSION1_0_0_3) == 0 {
                                let Reader :WWNetworkReader1_0_0_3 = WWNetworkReader1_0_0_3(aWWNetwork: self.mWWNetwork, aLoader: self)
                                try Reader.read(aFh: fileHandle, aError: &self.mError)
                            }
                            
                            //                } catch (OutOfMemoryError e) {
                            //                mFairways.clear();
                            //                mLocations.clear();
                            //                mNodes.clear();
                            //                mSections.clear();
                            //                mStructures.clear();
                            //                mAdministrations.clear();
                            //
                            //                e.printStackTrace();
                            //                mError.setErrorCode(StError.ER_OUTOFMEMORY);
                            //                return false;
                            //                } catch (StackOverflowError e){
                            //                for now this is a copy of the OOM above
                            //                mFairways.clear();
                            //                mLocations.clear();
                            //                mNodes.clear();
                            //                mSections.clear();
                            //                mStructures.clear();
                            //
                            //
                            //                e.printStackTrace();
                            //                mError.setErrorCode(StError.ER_OUTOFMEMORY);
                            //                return false;
                            //                }
                        } catch {
                            self.mError = StError.ER_FILEREADINGERROR
                            
                            self.mWWNetwork.mFairways.removeAll()
                            self.mWWNetwork.mLocations.removeAll()
                            self.mWWNetwork.mNodes.removeAll()
                            self.mWWNetwork.mSections.removeAll()
                            self.mWWNetwork.mStructures.removeAll()
                            self.mWWNetwork.mAdministrations.removeAll()
                        }
                        for i in 0 ..< self.mWWNetwork.mSections.count {
                            self.mWWNetwork.mSections[i].generatePlannerData()
                            if self.isCancelled {
                                return
                            }
                        }
                    }
                } catch let error {
                    dout(error.localizedDescription)
                    self.mError = StError.ER_FILEREADINGERROR
                }
                //                } catch (FileNotFoundException e) {
                //                    mError.setErrorCode(StError.ER_FILENOTFOUND)
                //                } catch (IOException e) {
                //                    mError.setErrorCode(StError.ER_FILEREAD)
                //                }
                //
                //                if (fs != null) {
                //                    do {
                //                        try fs.close();
                //                    } catch (IOException e) {
                //                        mError.setErrorCode(StError.ER_FILECLOSE);
                //                    }
                //                }
                
                DispatchQueue.main.async{
                    self.onPostExecute()
                }
                
                return
            }
        }
        
        //        public StError getLastError() {
        //            return mError;
        //        }
        
        //            @Override
        func onPostExecute(/*Boolean Result*/) {
            
            WWNetwork.network?.mLoading = false
            
//            if Result && mMainHandler != null {
//                Message msg = Message.obtain()
//                msg.what = AppConstants.WWNETWORK_LOADED
//                mMainHandler.sendMessage(msg)
//            }
            

//do this manually             ViewController.instance?.networkOverlay?.reloadNetworkOverlays() 
            
        }
        
        //    @Override
        //    protected void onCancelled(Boolean Result) { // this function is called from API level 11 onwards
        //    mLoading = false;
        //    }
        //
        //    @Override
        //    protected void onCancelled() { // this function is called from API level 3
        //    mLoading = false;
        //    }
        
        // keep checking cancelled status in code above
        func cancel(){
            WWNetwork.network?.mLoading = false
        }
    }
    
    //    public class NearestSectionPosResult {
    //        public StWWSection Section;
    //        public double SectionPos;
    //        public double DistToSection;
    //    }
    
    func removeAllNetworkData(){
        dout("Debug: remove all")
        
        mAdministrations.removeAll()
        mFairways.removeAll()
        mLocations.removeAll()
        mNodes.removeAll()
        mSections.removeAll()
        mStructures.removeAll()
    }
}
