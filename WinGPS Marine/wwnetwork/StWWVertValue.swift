//
//  StWWVertValue.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 17/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

enum WWNAPValueType {
    case UNKNOWN
    case NAPDEFAULT
    case NAPWATERLEVEL
    case CLEARANCE
}

enum WWVertRefDatum {
    case UNKNOWN
    case BOEZEMLEVEL
    case MEANWATERLEVEL
    case CANALLEVEL
    case LAKELEVEL
    case NAP
    case POLDERLEVEL
    case WEIRLEVEL
    case HSWLEVEL
}

class StWWVertValue {
    
    var Value :Float
    var Datum :WWVertRefDatum
    var NAPValue :Float
    var NAPType :WWNAPValueType
    
    init() {
        Value = WWDefs.WW_INF
        Datum = WWVertRefDatum.UNKNOWN
        NAPValue = WWDefs.WW_INF
        NAPType = WWNAPValueType.UNKNOWN
    }
    
    init(aValue :Float, aDatum :WWVertRefDatum, aNAPValue :Float, aNAPType :WWNAPValueType) {
        Value = aValue
        Datum = aDatum
        NAPValue = aNAPValue
        NAPType = aNAPType
    }
    
    func setValue(aValue :Float, aDatum :WWVertRefDatum, aNAPValue :Float, aNAPType :WWNAPValueType) {
        Value = aValue
        Datum = aDatum
        NAPValue = aNAPValue
        NAPType = aNAPType
    }
}
