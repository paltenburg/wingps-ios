//
//  StWWSection.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StWWPlannerPoint {

    var mCOGNext :Float              // COG from this planner point to the next planner point [rad].
    var mCOGPrev :Float             // COG from this planner point to the previous planner point [rad].
    var mDistance :Double            // Distance along the section [m].
    //    SNLTidesPoints  mNLTidesPoints;       // Nearest NL-Tides points.
//    bool            mNLTidesPointsValid;  // True if NL-Tides points are valid.
    var mPosGeo :Geo2              // Position of the planner point in geographic space [rad].
    var mPosGlb :Double3              // Position of the planner point in global space [m].
    //int             mUseTideHeight;       // -1: use tide ref-station of start route point of leg; 0: no tide height; 1: use tide ref-station of end route point of leg.
    var mVMaxNext :Float            // Speed limit from this planner point to the next planner point [m/s].
    var mVMaxPrev :Float            // Speed limit from this planner point to the previous planner point [m/s].
    var mWWCurrentNext :Float       // WW network current from this planner point to the next planner point [m/s].
    var mWWCurrentPrev :Float       // WW network current from this planner point to the previous planner point [m/s].

    init(aDistance :Double, aPosGlb :Double3, aPosGeo :Geo2, aCOGPrev :Float, aCOGNext :Float,
         aCurrentPrev :Float, aCurrentNext :Float, aVMaxPrev :Float, aVMaxNext :Float) {

        mCOGNext = aCOGNext
        mCOGPrev = aCOGPrev
        mDistance = aDistance
//                mNLTidesPoints(),
//                mNLTidesPointsValid(false),
        mPosGeo = Geo2(aPosGeo)
        mPosGlb = Double3(aPosGlb)
//                mUseTideHeight(aUseTideHeight),
        mVMaxNext = aVMaxNext
        mVMaxPrev = aVMaxPrev
        mWWCurrentNext = aCurrentNext
        mWWCurrentPrev = aCurrentPrev
    }
}

class StWWSectionPart {

    var mConstraints = [StWWConstraint]()             // Constraints that affect this part (if contains items, mStructure should be NULL).
    var mCurrent :Float                // Current [m/s] (positive in direction of section).
    var mPlannerPointIndexEnd :Int    // Index of last planner point in this part.
    var mPlannerPointIndexStart :Int  // Index of first planner point in this part.
    var mPosEnd :Double
    var mPosStart :Double
    var mSection :StWWSection
    var mStructure :StWWStructure?               // Structure that affects this part (if set, mConstraints should be empty).
    var mVMax :Float

    init(aSection :StWWSection, aPosStart :Double, aPosEnd :Double, aVMax :Float) {
//        mConstraints = new ArrayList<>()
        mCurrent = 0
        mPlannerPointIndexEnd = -1
        mPlannerPointIndexStart = -1
        mPosEnd = aPosEnd
        mPosStart = aPosStart
        mSection = aSection
        mStructure = nil
        mVMax = aVMax
    }

    func addConstraint(aConstraint :StWWConstraint) {
        mConstraints.append(aConstraint)
        if aConstraint.mConstraintClass == WWConstraintClass.CURRENT,
           let Constraint = aConstraint as? StWWConstraintCurrent {
            mCurrent = Constraint.mPositive ? Constraint.mCurrent : -Constraint.mCurrent
        }
    }
}

class StWWSectionPoint {
    
    let mDistance :Double
    let mDistanceMark :Int
    let mPosGlb :Double3
    let mSection :StWWSection
    
    init(aSection :StWWSection, aPosGlb :Double3, aDistance :Double, aDistanceMark :Int) {
        mDistance = aDistance
        mDistanceMark = aDistanceMark
        mPosGlb = Double3(aPosGlb)
        mSection = aSection
    }
}

//MARK: StWWSection class definition

public class StWWSection {

    private static let WWPLANNER_POINT_DIST_MAX :Float = 500.0

    var mBoundsCenterGlb :Double3
    var mBoundsRadius :Double
    var mDataIndex :Int
    var mFairway :StWWFairway?
    var mFairwayIndex :Int
    var mFairwayPos :Double
    var mGUID :StUUID
    var mHeadNode :StWWNode
    var mLen :Double
    var mName :String
    var mPlannerPoints = [StWWPlannerPoint]()
    var mSectionParts = [StWWSectionPart]()
    var mSectionPoints = [StWWSectionPoint]()
    var mTailNode :StWWNode
    var mWaterBodyID :WWWaterbodyID
    
//    public StWWSection(final UUID aGUID, final int aDataIndex, final StWWNode aHeadNode, final StWWNode aTailNode,
//        final Double3 aBoundsCenterGlb, final double aBoundsRadius, final double aLen, final String aName, final WWWaterbodyID aWaterBodyID) {
    
    init(aGUID :StUUID, aDataIndex :Int, aHeadNode :StWWNode, aTailNode :StWWNode, aBoundsCenterGlb :Double3,
         aBoundsRadius :Double, aLen :Double, aName :String, aWaterBodyID :WWWaterbodyID){
        
        
        mBoundsCenterGlb = Double3(aBoundsCenterGlb)
        mBoundsRadius = aBoundsRadius
        mDataIndex = aDataIndex
        mFairway = nil
        mFairwayIndex = 0
        mFairwayPos = 0
        mGUID = aGUID
        mHeadNode = aHeadNode
        mLen = aLen
        mName = aName
//        mPlannerPoints = new ArrayList<>();
//        mSectionParts = new ArrayList<>();
//        mSectionPoints = new ArrayList<>();
        mTailNode = aTailNode
        mWaterBodyID = aWaterBodyID
    }
    
//    private void addPlannerPoint(double aDistance, final Double3 aPosGlb, final Geo2 aPosGeo, float aCurrentPrev, float aCurrentNext, float aVMaxPrev, float aVMaxNext) {
//        StWWPlannerPoint PlannerPoint = new StWWPlannerPoint(aDistance, aPosGlb, aPosGeo, 0.0f, 0.0f, aCurrentPrev, aCurrentNext, aVMaxPrev, aVMaxNext);
//        mPlannerPoints.add(PlannerPoint);
//    }

    func addSectionPart(aPartStartPos :Double, aPartEndPos :Double, aSpeedMax :Float) -> StWWSectionPart {
        var SectionPart :StWWSectionPart = StWWSectionPart(aSection: self, aPosStart: aPartStartPos, aPosEnd: aPartEndPos, aVMax: aSpeedMax)
        mSectionParts.append(SectionPart)
        return SectionPart
    }
    
    func addSectionPoint(aPosGlb :Double3, aDistance :Double, aDistanceMark :Int) {
//        func addSectionPoint(_ aPosGlb :Double3, _ aDistance :Double) {
        var SectionPoint = StWWSectionPoint(aSection: self, aPosGlb: aPosGlb, aDistance: aDistance, aDistanceMark: aDistanceMark)
        mSectionPoints.append(SectionPoint)
        return
    }

//    private float calcCOG(final StWWPlannerPoint aP1, final StWWPlannerPoint aP2) {
//        return (float)StGeodeticCalibration.headingAlongLine(aP1.getPosGeo(), aP2.getPosGeo());
//    }

    private func calcPosGlbRecursive(aIndex1 :inout Int, aIndex2 :inout Int, aSectionPos :Double) {
        if aIndex2 - 1 > aIndex1 {
            let IndexHalf :Int = (aIndex1 + aIndex2) / 2
            if mSectionPoints[IndexHalf].mDistance < aSectionPos {
                aIndex1 = IndexHalf
                calcPosGlbRecursive(aIndex1: &aIndex1, aIndex2: &aIndex2, aSectionPos: aSectionPos)
            } else {
                aIndex2 = IndexHalf
                calcPosGlbRecursive(aIndex1: &aIndex1, aIndex2: &aIndex2, aSectionPos: aSectionPos)
            }
        }
    }
    
//    func calcPosGlb(aPosGlb :Double3, aSectionPos :Double) {
//        //Assert(aSectionPos >= 0 && aSectionPos <= mSectionPoints.GetLast()->mDistance, L"");
//        var Index1 :Int = 0
//        var Index2 :Int = mSectionPoints.count - 1
//        calcPosGlbRecursive(aIndex1: &Index1, aIndex2: &Index2, aSectionPos: aSectionPos)
//        var Factor :Double = (aSectionPos - mSectionPoints[Index1].mDistance) / (mSectionPoints[Index2].mDistance - mSectionPoints[Index1].mDistance)
//
//        StMath.lerp(aPosGlb, mSectionPoints[Index1].getPosGlb(), mSectionPoints[Index2].getPosGlb(), Factor)
//
//
//    }

//    public StWWPlannerPoint createPlannerPoint(MutableInteger aIndex, double aSectionPos/*, const CNavNLTidesDB*aNLTidesDB, int aUseTideHeight*/) {
//        //Assert(aSectionPos >= 0.0 && aSectionPos <= mLen, L"aSectionPos out of bounds");
//
//        // Find the points that preceed and succeed aSectionPos.
//        int Index = 0;
//        while (mPlannerPoints.get(Index + 1).getDistance() < aSectionPos) // sometimes IndexOutOfBoundsException
//            Index++;
//
//        final StWWPlannerPoint P1 = mPlannerPoints.get(Index);
//        final StWWPlannerPoint P2 = mPlannerPoints.get(Index + 1);
//        aIndex.setValue(Index);
//
//        // Lerp global space position and calculate geographic position.
//        double Factor = (aSectionPos - P1.getDistance()) / (P2.getDistance() - P1.getDistance());
//        Double3 PosGlb = new Double3();
//        StMath.lerp(PosGlb, P1.getPosGlb(), P2.getPosGlb(), Factor);
//        Geo3 PosGeo = new Geo3(StGeodeticCalibration.globalToGeo2(PosGlb, null));
//
//        // Recalculate global position to move it on the ellipsoid.
//        StGeodeticCalibration.geoToGlobal(PosGeo, PosGlb);
//
//        return new StWWPlannerPoint(aSectionPos, PosGlb, PosGeo.getGeo2(), P2.getCOGPrev(), P1.getCOGNext(), P2.getWWCurrentPrev(),
//                P1.getWWCurrentNext(), P2.getVMaxPrev(), P1.getVMaxNext()/*, aNLTidesDB, aUseTideHeight*/);
//    }
    
    func generatePlannerData() {
        
        //TODO: Convert rest of function:
        
//        // Get the first section point.
//        int SectionPointIndex = 0;
//        StWWSectionPoint P1 = mSectionPoints.get(0);
//        StWWSectionPoint P2 = mSectionPoints.get(1);
//        Double3 PosGlb1 = P1.getPosGlb();
//        Geo2 PosGeo1 = StGeodeticCalibration.globalToGeo2(PosGlb1, null);
//        double Distance1 = P1.mDistance;
//
//        // Create a planner point.
//        addPlannerPoint(0.0, PosGlb1, PosGeo1, 0.0f, 0.0f, 0.0f, 0.0f);
//
//        int SectionPartCount = mSectionParts.size();
//        for (int c = 0; c < SectionPartCount; c++) {
//            // Set the constraint part's starting planner point index.
//            StWWSectionPart SectionPart = mSectionParts.get(c);
//            SectionPart.mPlannerPointIndexStart = mPlannerPoints.size() - 1;
//
//            // If the constraint part has length, generate planner points along the constraint part.
//            if (SectionPart.getPosEnd() > SectionPart.getPosStart()) {
//                boolean Done = false;
//                do {
//                    mPlannerPoints.get(mPlannerPoints.size() - 1).mVMaxNext = SectionPart.mVMax;
//                    mPlannerPoints.get(mPlannerPoints.size() - 1).mWWCurrentNext = SectionPart.mCurrent;
//
//                    Double3 PosGlb2 = null;
//                    Geo2 PosGeo2 = new Geo2();
//                    double Distance2;
//                    if (P2.mDistance <= SectionPart.getPosEnd()) {
//                        PosGlb2 = P2.getPosGlb();
//                        StGeodeticCalibration.globalToGeo2(PosGlb2, PosGeo2);
//                        Distance2 = P2.mDistance;
//                        if (P2.mDistance < SectionPart.getPosEnd()) {
//                            SectionPointIndex++;
//                            P1 = P2;
//                            P2 = mSectionPoints.get(SectionPointIndex + 1);
//                        } else
//                            Done = true;
//                    } else {
//                        double LerpFactor = (SectionPart.getPosEnd() - Distance1) / (P2.mDistance - Distance1);
//                        if (PosGlb2 == null)
//                            PosGlb2 = new Double3();
//                        StMath.lerp(PosGlb2, PosGlb1, P2.getPosGlb(), LerpFactor);
//                        StGeodeticCalibration.globalToGeo2(PosGlb2, PosGeo2);
//                        StGeodeticCalibration.geoToGlobal(PosGeo2, PosGlb2);
//                        Distance2 = SectionPart.getPosEnd();
//                        Done = true;
//                    }
//                    generatePlannerPoints(PosGlb1, Distance1, PosGlb2, PosGeo2, Distance2, SectionPart.mVMax, SectionPart.mCurrent);
//
//                    // Generate last point.
//                    addPlannerPoint(Distance2, PosGlb2, PosGeo2, SectionPart.mCurrent, 0.0f, SectionPart.mVMax, 0.0f);
//                    PosGlb1 = PosGlb2;
//                    PosGeo1 = PosGeo2;
//                    Distance1 = Distance2;
//                } while (!Done);
//            }
//            SectionPart.mPlannerPointIndexEnd = mPlannerPoints.size() - 1;
//        }
//
//        // Generate COGs for every planner point.
//        StWWPlannerPoint Point2 = mPlannerPoints.get(0);
//        int Count = mPlannerPoints.size();
//        for (int i = 1; i < Count; i++) {
//            StWWPlannerPoint Point1 = Point2;
//            Point2 = mPlannerPoints.get(i);
//            Point1.mCOGNext = calcCOG(Point1, Point2);
//            Point2.mCOGPrev = StMath.reduceAngle(Point1.mCOGNext + StMath.PIf);
//        }
//
//        // Set the COGPrev, COGNext, VMaxPrev, VMaxNext, CurrentPrev and CurrentNext of the first and last point to correct values.
//        int Last = mPlannerPoints.size() - 1;
//        mPlannerPoints.get(0).mCOGPrev = StMath.reduceAngle(mPlannerPoints.get(0).mCOGNext + StMath.PIf);
//        mPlannerPoints.get(Last).mCOGNext = StMath.reduceAngle(mPlannerPoints.get(Last).mCOGPrev + StMath.PIf);
//        mPlannerPoints.get(0).mVMaxPrev = mPlannerPoints.get(0).mVMaxNext;
//        mPlannerPoints.get(Last).mVMaxNext = mPlannerPoints.get(Last).mVMaxPrev;
//        mPlannerPoints.get(0).mWWCurrentPrev = mPlannerPoints.get(0).mWWCurrentNext;
//        mPlannerPoints.get(Last).mWWCurrentNext = mPlannerPoints.get(Last).mWWCurrentPrev;
    }

    
//    private void generatePlannerPoints(final Double3 aPosGlb1, double aDistance1, final Double3 aPosGlb2, final Geo2 aPosGeo2, double aDistance2, float aVMax, float aCurrent) {
//        // Calculate how many points should be added between P1 and P2.
//        int Count = (int)Math.ceil((aDistance2 - aDistance1) / WWPLANNER_POINT_DIST_MAX);
//
//        // Generate points except the last.
//        Double3 PosGlb = new Double3();
//        Geo2 PosGeo = new Geo2();
//        for (int i = 1; i < Count; i++) {
//            double Distance = StMath.lerp(aDistance1, aDistance2, i / (double)Count);
//            StMath.lerp(PosGlb, aPosGlb1, aPosGlb2, i / (double)Count);
//
//            // Calculate position in geographic space and recalculate PosGlb to get a point on the ellipsoid.
//            StGeodeticCalibration.globalToGeo2(PosGlb, PosGeo);
//            StGeodeticCalibration.geoToGlobal(PosGeo, PosGlb);
//
//            addPlannerPoint(Distance, PosGlb, PosGeo, aCurrent, aCurrent, aVMax, aVMax);
//        }
//    }
//
//    public Double3 getBoundsCenterGlb() {
//        return mBoundsCenterGlb;
//    }
//
//    public double getBoundsRadius() {
//        return mBoundsRadius;
//    }
//
//    public int getDataIndex() {
//        return mDataIndex;
//    }

    func getDistanceFromMarks(aSectionPos :Double) -> Double {
        if mFairway == nil {
            return -1
        }

        var distance :Double = 0
        var d1 = -1

        // get section points around sectionpos
        let sectionPointCount = mSectionPoints.count
        var found = false
        var sIndex :Int = 0
        while sIndex < sectionPointCount - 1 {
            if aSectionPos >= mSectionPoints[sIndex].mDistance && aSectionPos < mSectionPoints[sIndex + 1].mDistance {
                found = true
                break
            }
            sIndex += 1
        }
        
        if found && mFairwayIndex >= 1{
            // get fairway to determine direction of distance marks
            if let fairway = mFairway {
                if fairway.mMarksInFDir {
                    distance += aSectionPos - mSectionPoints[sIndex].mDistance
                    d1 = mSectionPoints[sIndex].mDistanceMark
                    if d1 == -1 {
                        d1 = getDistanceMarkPrev(aSectionPointIndex: sIndex, aDistance: &distance)
                        if d1 == -1 {
                            for i in (0 ... mFairwayIndex - 1).reversed() {
                                d1 = fairway.mSections[i].getDistanceMarkPrev(aSectionPointIndex: fairway.mSections[i].mSectionPoints.count - 1, aDistance: &distance)
                                if d1 > -1 {
                                    break
                                }
                            }
                        }
                    }
                } else {
                    distance += mSectionPoints[sIndex + 1].mDistance - aSectionPos
                    d1 = mSectionPoints[sIndex + 1].mDistanceMark
                    if d1 == -1 {
                        d1 = getDistanceMarkNext(aSectionPointIndex: sIndex + 1, aDistance: &distance)
                        if d1 == -1 {
                            for i in mFairwayIndex + 1 ..< fairway.mSections.count - 1 {
                                d1 = fairway.mSections[i].getDistanceMarkNext(aSectionPointIndex: 0, aDistance: &distance)
                                if (d1 > -1) {
                                    break
                                }
                            }
                        }
                    }
                }
            }
        }
        return (d1 > -1 && found) ? Double(d1) + distance : -1
    }

    func getDistanceMarkNext(aSectionPointIndex :Int, aDistance :inout Double) -> Int {
        var d :Int = -1
        if aSectionPointIndex < 0 || aSectionPointIndex >= mSectionPoints.count{
            return d
        }

        var i :Int = aSectionPointIndex
        while i < mSectionPoints.count {
            d = mSectionPoints[i].mDistanceMark
            i += 1
            if d > -1 {
                break
            }
        }
        aDistance += mSectionPoints[i - 1].mDistance - mSectionPoints[aSectionPointIndex].mDistance
        return d
    }

    func getDistanceMarkPrev(aSectionPointIndex :Int, aDistance :inout Double) -> Int {
        var d :Int = -1
        if aSectionPointIndex < 0 || aSectionPointIndex >= mSectionPoints.count {
            return d
        }

        var i :Int = aSectionPointIndex - 1
        while i >= 0 {
            d = mSectionPoints[i].mDistanceMark
            i -= 1
            if d > -1 {
                break
            }
        }
        aDistance += mSectionPoints[aSectionPointIndex].mDistance - mSectionPoints[i + 1].mDistance
        return d
    }

//    public StWWFairway getFairway() {
//        return mFairway;
//    }
//
//    public UUID getGUID() {
//        return mGUID;
//    }
//
//    public StWWNode getHeadNode() {
//        return mHeadNode;
//    }

//    func getInfoText(aSectionPos :Double, aUseSecondLine :Bool) -> String {
//        var Name :String = mName.trim()
//        double DMark = (mFairway != null) ? getDistanceFromMarks(aSectionPos) : -1;
//        if (DMark >= 0) {
//            DMark = StUnits.unitConvert(DMark, StUnits.Unit.M, StUnits.Unit.KM);
//            DecimalFormatSymbols dfs = new DecimalFormatSymbols();
//            int Fraction = (int)((DMark % 1) * 10);
//            Name += " (km " + String.valueOf((int)Math.floor(DMark)) + dfs.getDecimalSeparator() + String.valueOf(Fraction) + ")";
//        }
//        if (aUseSecondLine) {
//            String DimStr = "";
///*
//        float VMax = getVMax(aSectionPos);
//        if (VMax > 0 && VMax != StWWDefs.WW_INF) {
//            if (DimStr.length() > 0)
//                DimStr += " ";
//            DimStr += "V" + String.format("%.1f", StUnits.unitConvert(VMax, StUnits.Unit.MPS, aUnitSpeed)) + " " + StUnits.unitToStr(aUnitSpeed);
//        }
//*/
//            MutableInteger VHFChannel = new MutableInteger();
//            MutableString Temp = new MutableString();
//            if (getVHFSector(aSectionPos, VHFChannel, Temp)) {
//                if (DimStr.length() > 0)
//                    DimStr += " ";
//                DimStr += "VHF" + String.valueOf(VHFChannel);
//            }
//
//            if (DimStr.length() > 0)
//                Name += "\n" + DimStr;
//        }
//        return Name
//    }

//    func getInfoText(double aSectionPos, StLPBoatDef aBoatDef, aUnitDepth :String, StUnits.Unit aUnitSpeed, aPassable :inout Bool) -> String {
    func getInfoText(aSectionPos :Double, aUnitDepth :String) -> String {
//        aPassable = true
        var Name :String = mName.trim()
        var DMark :Double = (mFairway != nil) ? getDistanceFromMarks(aSectionPos: aSectionPos) : -1
        if DMark >= 0 {
            
            
//            DMark = StUnits.unitConvert(DMark, StUnits.Unit.M, StUnits.Unit.KM);
            
            DMark = StUtils.unitConvert(valueDefaultUnit: DMark, targetUnit: Resources.SETTINGS_UNIT_DISTANCE_LARGE_KM)
//            DecimalFormatSymbols dfs = new DecimalFormatSymbols()
//            int Fraction = (int)((DMark % 1) * 10)
//            Name += " (km " + String.valueOf((int)Math.floor(DMark)) + dfs.getDecimalSeparator() + String.valueOf(Fraction) + ")"
            let valueString = String(format: "%.1f", DMark)
            Name += " (km \(valueString))"
        }

        
//        MutableFloat DepthM = new MutableFloat();
//        MutableFloat HeightM = new MutableFloat();
//        MutableFloat LengthM = new MutableFloat();
//        MutableFloat WidthM = new MutableFloat();
//        String DimStr = "";
//        if (getMaxAllowedDimensions(aSectionPos, DepthM, HeightM, LengthM, WidthM)) {
//            if (aBoatDef.getDraught() > DepthM.getValue() || aBoatDef.getHeight() > HeightM.getValue() ||
//                    aBoatDef.getLength() > LengthM.getValue() || aBoatDef.getWidth() > WidthM.getValue())
//                aPassable.setValue(false);
//
//            float Depth = DepthM.getValue();
//            //, Height, Length, Width;
//            if (Depth > 0.0f && Depth != StWWDefs.WW_INF) {
//                Depth = StUnits.unitConvert(Depth, StUnits.Unit.M, aUnitDepth);
//                DimStr = "D" + String.format("%.1f", Depth) + StUnits.unitToStr(aUnitDepth);
//            }
//    /*
//    if (Height > 0.0f && Height != WW_INF) {
//    DimStr += (DimStr.GetLen() > 0) ? L" H" : L"H";
//    int decimals = (Frac(Height) < 0.01 || Frac(Height) > 0.99) ? 0 : 1;
//    DimStr += FloatToStr(Height, 7, decimals, lsUser);
//    }
//    if (Length > 0.0f && Length != WW_INF) {
//    DimStr += (DimStr.GetLen() > 0) ? L" L" : L"L";
//    int decimals = (Frac(Length) < 0.01 || Frac(Length) > 0.99) ? 0 : 1;
//    DimStr += FloatToStr(Length, 7, decimals, lsUser);
//    }
//    if (Width > 0.0f && Width != WW_INF) {
//    DimStr += (DimStr.GetLen() > 0) ? L" W" : L"W";
//    int decimals = (Frac(Width) < 0.01 || Frac(Width) > 0.99) ? 0 : 1;
//    DimStr += FloatToStr(Width, 7, decimals, lsUser);
//    }
//    */
//        }
        var DepthM :Float = 0
        var HeightM :Float = 0
        var LengthM :Float = 0
        var WidthM :Float = 0
        var DimStr :String = ""
        if getMaxAllowedDimensions(aSectionPos: aSectionPos, aDepth: &DepthM, aHeight: &HeightM, aLength: &LengthM, aWidth: &WidthM) {
            // TODO: implement boat dimensions
//            if (aBoatDef.getDraught() > DepthM.getValue() || aBoatDef.getHeight() > HeightM.getValue() ||
//                    aBoatDef.getLength() > LengthM.getValue() || aBoatDef.getWidth() > WidthM.getValue()){
//            aPassable = false
//            }
            
            var Depth = DepthM
            //, Height, Length, Width;
            if Depth > 0.0 && Depth != WWDefs.WW_INF {
//                Depth = StUtils.unitConvert(Depth, StUnits.Unit.M, aUnitDepth)
                Depth = Float( StUtils.unitConvert(valueDefaultUnit: Double(Depth), targetUnit: Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER) )
                DimStr = "D" + String(format: "%.1f", Depth) + Units.unitToStr(aUnitDepth)
            }
        }

//        // if the section is passable at this point, also check the shipping class
//        if (aPassable.getValue()) {
//            MutableInteger Shipping = new MutableInteger();
//            if (getShippingClass(aSectionPos, Shipping)) {
//               if (aBoatDef.getCommercial()) {
//                   aPassable.setValue((Shipping.getValue() & StWWDefs.WSF_COMMERCIAL) == StWWDefs.WSF_COMMERCIAL);
//               } else {
//                   aPassable.setValue((Shipping.getValue() & StWWDefs.WSF_RECREATION) == StWWDefs.WSF_RECREATION);
//               }
//            }
//        }

        // TODO Complete selection description ////////////

        var VMax = getVMax(aSectionPos: aSectionPos)
        if VMax > 0 && VMax != WWDefs.WW_INF {
            if (DimStr.count > 0){
                DimStr += " "
            }
//            DimStr += "V" + String.format("%.1f", StUnits.unitConvert(VMax, StUnits.Unit.MPS, aUnitSpeed)) + " " + StUnits.unitToStr(aUnitSpeed)
            DimStr += "V\(StUtils.formatSpeed(Double(VMax)) ?? "")"
            
        }

        var VHFChannel :Int = 0
        var Temp :String = ""
        if getVHFSector(aSectionPos: aSectionPos, aChannel: &VHFChannel, aCallsign: &Temp) {
            if DimStr.count > 0 {
                DimStr += " "
            }
            DimStr += "VHF" + String(VHFChannel)
        }

        if DimStr.count > 0 {
            Name += "\n" + DimStr
        }
        
        return Name
    }
//
//    public double getLen() {
//        return mLen;
//    }
//
//    public String getName() {
//        return mName;
//    }

    func getMaxAllowedDimensions(aSectionPos :Double, aDepth :inout Float, aHeight :inout Float, aLength :inout Float, aWidth :inout Float) -> Bool {
        let sectionPartCount = mSectionParts.count
        var sectionPartOptional :StWWSectionPart? = nil
        for c in 0 ..< sectionPartCount {
            if aSectionPos >= mSectionParts[c].mPosStart && aSectionPos <= mSectionParts[c].mPosEnd {
                sectionPartOptional = mSectionParts[c]
                break
            }
        }
        guard let sectionPart = sectionPartOptional else {
            return false
        }

        for i in 0 ..< sectionPart.mConstraints.count {
            if sectionPart.mConstraints[i].mConstraintClass != WWConstraintClass.VESSELSIZE {
                continue
            }
            
            if let constraint :StWWConstraintVesselSize = sectionPart.mConstraints[i] as? StWWConstraintVesselSize {
                aDepth = constraint.mDepthMax
                aHeight = constraint.mHeightMax
                aLength = constraint.mLengthMax
                aWidth = constraint.mWidthMax
                return true
            }
        }

        return false
    }
    
//    public StWWPlannerPoint getPlannerPoint(int aIndex) {
//        return mPlannerPoints.get(aIndex);
//    }
//
//    public int getPlannerPointCount() {
//        return mPlannerPoints.size();
//    }
//
//    public Float2 getSectionDir(double aSectionPos, final StGeodeticCalibration aCalibration) {
//        Float2 Res = Float2.zeroFloat2();
//        int SIndex = 0;
//        boolean Found = false;
//        while (SIndex < mSectionPoints.size() - 1) {
//            if (aSectionPos >= mSectionPoints.get(SIndex).getDistance() && aSectionPos < mSectionPoints.get(SIndex + 1).getDistance()) {
//                Found = true;
//                break;
//            }
//            SIndex++;
//        }
//        if (Found) {
//            Double2 PosPix1 = aCalibration.glbWGS84ToPix(mSectionPoints.get(SIndex).getPosGlb());
//            Double2 PosPix2 = aCalibration.glbWGS84ToPix(mSectionPoints.get(SIndex + 1).getPosGlb());
//            PosPix2.subtract(PosPix1);
//            double L = StMath.len(PosPix2);
//            if (L >= 1e-9) {
//                Res.x = (float)(PosPix2.x / L);
//                Res.y = (float)(PosPix2.y / L);
//            }
//        }
//        return Res;
//    }
//
//    public StWWSectionPart getSectionPart(int aIndex) {
//        return mSectionParts.get(aIndex);
//    }
//
//    public int getSectionPartCount() {
//        return mSectionParts.size();
//    }

    func getSectionPoint(_ aIndex :Int) -> StWWSectionPoint {
        return mSectionPoints[aIndex]
    }

    func getSectionPointCount() -> Int {
        return mSectionPoints.count
    }

//    public boolean getShippingClass(double aSectionPos, MutableInteger aFlags) {
//        int SectionPartCount = mSectionParts.size();
//        StWWSectionPart SectionPart = null;
//        for (int c = 0; c < SectionPartCount; c++) {
//            if (aSectionPos >= mSectionParts.get(c).getPosStart() && aSectionPos <= mSectionParts.get(c).getPosEnd()) {
//                SectionPart = mSectionParts.get(c);
//                break;
//            }
//        }
//        if (SectionPart == null)
//            return false;
//
//        for (int i = 0; i < SectionPart.getConstraintCount(); i++) {
//            if (SectionPart.getConstraint(i).getConstraintClass() != StWWConstraint.WWConstraintClass.SHIPPINGCLASS)
//                continue;
//
//            final StWWConstraintShippingClass Constraint = (StWWConstraintShippingClass)SectionPart.getConstraint(i);
//            aFlags.setValue(Constraint.getShippingFlags());
//            return true;
//        }
//
//        return false;
//    }
//
//    public StWWNode getTailNode() {
//        return mTailNode;
//    }
//
//    public boolean getVHFReport(double aSectionPos, boolean aPositive, MutableInteger aChannel, MutableString aCallsign) {
//        int SectionPartCount = mSectionParts.size();
//        StWWSectionPart SectionPart = null;
//        for (int c = 0; c < SectionPartCount; c++) {
//            if (aSectionPos >= mSectionParts.get(c).getPosStart() && aSectionPos <= mSectionParts.get(c).getPosEnd()) {
//                SectionPart = mSectionParts.get(c);
//                break;
//            }
//        }
//        if (SectionPart == null)
//            return false;
//
//        for (int i = 0; i < SectionPart.getConstraintCount(); i++) {
//            if (SectionPart.getConstraint(i).getConstraintClass() != StWWConstraint.WWConstraintClass.VHFREPORT)
//                continue;
//
//            final StWWConstraintVHFReport Constraint = (StWWConstraintVHFReport)SectionPart.getConstraint(i);
//            StWWConstraintVHFReport.WWConstraintDirection Direction = Constraint.getDirection();
//            if (Direction == StWWConstraintVHFReport.WWConstraintDirection.BOTH ||
//                    (Direction == StWWConstraintVHFReport.WWConstraintDirection.POSITIVE && aPositive) ||
//                    (Direction == StWWConstraintVHFReport.WWConstraintDirection.NEGATIVE && !aPositive)) {
//                aChannel.setValue(Constraint.getChannel());
//                aCallsign.setValue(Constraint.getCallsign());
//                return true;
//            }
//        }
//
//        return false;
//    }

    func getVHFSector(aSectionPos :Double, aChannel :inout Int, aCallsign :inout String) -> Bool {
        var sectionPartCount :Int = mSectionParts.count
        var sectionPartOpt :StWWSectionPart? = nil
        for c in 0 ..< sectionPartCount {
            if aSectionPos >= mSectionParts[c].mPosStart && aSectionPos <= mSectionParts[c].mPosEnd {
                sectionPartOpt = mSectionParts[c]
                break
            }
        }
        guard let sectionPart = sectionPartOpt else {
            return false
        }

        for i in 0 ..< (sectionPart.mConstraints.count ?? 0) {
            if sectionPart.mConstraints[i].mConstraintClass != WWConstraintClass.VHFSECTOR {
                continue
            }

            let constraint : StWWConstraintVHFSector = sectionPart.mConstraints[i] as! StWWConstraintVHFSector
            aChannel = constraint.mChannel
            aCallsign = constraint.mCallsign
            return true
        }

        return false
    }

    func getVMax(aSectionPos :Double) -> Float {
        var sectionPartCount = mSectionParts.count
        for c in 0 ..< sectionPartCount {
            var sectionPart :StWWSectionPart = mSectionParts[c]

            if aSectionPos >= sectionPart.mPosStart && aSectionPos <= sectionPart.mPosEnd {
                return sectionPart.mVMax
            }
        }
        return WWDefs.WW_INF
    }

//    public WWWaterbodyID getWaterBodyID() {
//        return mWaterBodyID;
//    }
//
    func setFairway(aFairway :StWWFairway, aFairwayIndex :Int, aFairwayPos :Double) {
        mFairway = aFairway
        mFairwayIndex = aFairwayIndex
        mFairwayPos = aFairwayPos
    }
    
//    @Override
//    public String toString() {
//        return mName;
//    }
}
