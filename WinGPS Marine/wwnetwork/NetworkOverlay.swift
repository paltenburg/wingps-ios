//
//  NetworkOverlay.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 23/03/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation
import MapKit

class NetworkSegmentPolyline :MKPolyline {
    
    var polyLinePos :[Double]?
    
    var section :StWWSection?
    
    //    var color :UIColor = UIColor.red
    // android color:
    //    var color :UIColor = UIColor(red: 126.0/255.0, green: 186.0/255.0, blue: 229.0/255.0, alpha: 1.0)
    var color :UIColor = UIColor(red: 0/255.0, green: 149.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    //    var color :UIColor = UIColor.blue
    
}


class NetworkSegmentPolylineRenderer :MKPolylineRenderer {
    
    override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {
        super.draw(mapRect, zoomScale: zoomScale, in: context)
    }
    
    override func canDraw(_ mapRect: MKMapRect, zoomScale: MKZoomScale) -> Bool {
        //        dout("NetworkSegmentPolylineRenderer zoomScale:\(zoomScale)")
        
        //        return false
        
        if zoomScale < 0.001 { // 0.0075, 0.020 , 0.010
                    return false
                }
        
        
        
        return super.canDraw(mapRect, zoomScale: zoomScale)
    }
}

class NetworkSelectionAnnotation :MKPointAnnotation {
    
    var annotationView :MKAnnotationView?
    var section :StWWSection?
    var sectionPos :Double = 0
    
    //    init(_ routePoint :RoutePoint){
    //        self.routePoint = routePoint
    //        super.init()
    //        coordinate = routePoint.position
    //        routePoint.annotation = self
    //    }
    
    init(mapPoint :MKMapPoint, wwSection :StWWSection?, sectionPos :Double){
        super.init()
        coordinate = mapPoint.coordinate
        self.section = wwSection
        self.sectionPos = sectionPos
    }
    
    func updateAnnotationView(){
        if let annotationView = self.annotationView {
            for sv in annotationView.subviews {
                sv.removeFromSuperview()
            }
            annotationView.addSubview(getSelectionTextLabelView())
        }
        
        annotationView?.addSubview(getSelectionView())
        //        annotationView?.addSubview(Waypoints.getCustomWaypointIconView())
        
    }
    
    func getSelectionTextLabelView() -> UILabel {
        let textLabel = UILabel()
        
        textLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7)
        // proberen: label.layer.backgroundColor = UIColor(red: 0/255, green: 159/255, blue: 184/255, alpha: 1.0).cgColor
        
        textLabel.textColor = UIColor.black
        
        textLabel.textAlignment = .center
        textLabel.numberOfLines = 0
        
        textLabel.textAlignment = .left
        //        textLabel.text = "\(title ?? "")\n\(StUtils.formatDistance(routePoint.getDistance()))"
        //                textLabel.text = "\("Selected networkpoint")\nD0m V0 km/h"
        textLabel.text = "unset"
        
        //        textLabel.text = section.getInfoText(sectionPos, StDBManager.getManager().getBoatDef(), StUnits.Unit.M, mUnitSpeed, mValueBool);
        textLabel.text = section?.getInfoText(aSectionPos: sectionPos, aUnitDepth: Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER)
        
        
        dout("section.getInfoText: \(textLabel.text)")
        
        textLabel.invalidateIntrinsicContentSize()
        
        //            textLabel.frame = CGRect(x: 0, y: 0 , width: 400, height: 20)
        textLabel.sizeToFit()
        textLabel.frame.origin = CGPoint(x: 12, y: -(textLabel.frame.height / 2))
        return textLabel
    }
    
    func getSelectionView() -> UIImageView {
        
        let scale :CGFloat = 0.10
        
        let height :CGFloat = 100.0;
        let width :CGFloat = 100.0;
        let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: width/2, y: height/2)
        context.setLineCap(CGLineCap.round)
        context.setLineJoin(CGLineJoin.round)
        
        //        // first, draw white outline as background
        //        context.setLineWidth(15.0)
        //        context.setStrokeColor(UIColor.white.cgColor)
        //        context.addEllipse(in: CGRect(x: -width/4, y: -width/4, width: width/2, height: width/2))
        //        context.drawPath(using: .stroke)
        
        // next, draw red circle
        context.setLineWidth(6.0)
        context.setStrokeColor(UIColor.black.cgColor)
        context.addEllipse(in: CGRect(x: -width/4, y: -width/4, width: width/2, height: width/2))
        context.drawPath(using: .stroke)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let uiImage = UIImage(cgImage: (image?.cgImage)!)
        let uiImageView = UIImageView(image: uiImage)
        
        uiImageView.transform = CGAffineTransform(translationX: -uiImageView.image!.size.width / 2, y: -uiImageView.image!.size.height / 2)
        uiImageView.transform = uiImageView.transform.scaledBy(x: scale, y: scale)
        
        return uiImageView
    }
}

class NetworkOverlay {
    
    static let disableNetworkNavigation = true
    
    static let instance = NetworkOverlay()

    var firstAddedNetworkLines = true
    
    // distance threshold between tapped location and segment in display coordinates. e.g.: iPad Air 2's display is 768 x 1024
    static let tapDistanceThreshold :Double = 25
    
    var vc :ViewController?
    
    var allNetworkOverlays = [MKOverlay]()
    
    var networkSegmentPolylines = [NetworkSegmentPolyline]()
    
    var selectionAnnotation :NetworkSelectionAnnotation?
    
    
    var segmentsE6 = [[GeoPointE6]]()
    
    func isEnabled() -> Bool { // move to more general Overlay class
        return Resources.useWWNetwork
    }
    
    private init(){
        dout("NetworkOverlay-init")
        
    }
    
    func linkToViewcontroller(_ viewController :ViewController) {
        dout("linkToViewcontroller")
        
        self.vc = viewController
        //        .service.overlay = self
        
        // init networkoverlay
        //        reloadNetworkOverlays()
        
//        vc?.mapView.addOverlays(allNetworkOverlays)
        
        addVisibleNetworkLinesToMapview() // done in refreshcharts already
        
    }
    
    func disableOverlay(){
        StaticQueues.mapviewOverlaysLock.runLocked({ self.vc?.mapView.removeOverlays(self.networkSegmentPolylines) })

    }
    
    var addVisibleNetworkLinesToMapviewRunning :Bool = false

    func addVisibleNetworkLinesToMapview(){
        if !self.addVisibleNetworkLinesToMapviewRunning {
            addVisibleNetworkLinesToMapviewRunning = true
            StaticQueues.networkOverlaysLock.runLocked({
                
                dout("addVisibleNetworkLinesToMapview()")
                
                var filteredPolyLines = [NetworkSegmentPolyline]()
                
                //        StaticQueues.mapviewAnnotationsLock.runLocked({ // use lock that's made for mapViewAnnotations here
                
                for polyLine in self.networkSegmentPolylines {
                    
                    var segmentInChartsRegion = true
                    
                    //                for j in 0 ..< polyLine.pointCount {
                    for coordinate in polyLine.coordinates {
                        
                        //                    var mapPoint = polyLine.points()[j]
                        
                        if !DKW2ChartManager.getChartManager().inChartsRegion(aGPDeg: Geo2(coordinate)) {
                            segmentInChartsRegion = false
                            break
                        }
                    }
                    
                    if segmentInChartsRegion {
                        filteredPolyLines.append(polyLine)
                    }
                    
                }
                
                dout("addVisibleNetworkLinesToMapview() filtering done")
                
                if let vc = self.vc
                //               firstAddedNetworkLines || networkSegmentPolylines.count != filteredPolyLines.count {
                //                firstAddedNetworkLines = false
                {
                    
                    StaticQueues.mapviewOverlaysLock.runLocked({ vc.mapView.removeOverlays(self.networkSegmentPolylines) })
                    
                    dout("addVisibleNetworkLinesToMapview() overlays removed")
                    
                    //keep hidden polylines in memory// networkSegmentPolylines = filteredPolyLines
                    
                    dout("addVisibleNetworkLinesToMapview() adding \(self.networkSegmentPolylines.count) polylines")
                    
                    for polyLine in filteredPolyLines {
                        StaticQueues.mapviewOverlaysLock.runLocked({ vc.mapView.addOverlay(polyLine) })
                    }
                    
                    dout("addVisibleNetworkLinesToMapview() overlays added")
                    
                }
                
                //        })
                
                dout("/addVisibleNetworkLinesToMapview()")
            })
            addVisibleNetworkLinesToMapviewRunning = false
        }
    }
    
    func onChartManagerRefresh(){
//        StaticQueues.networkOverlaysLock.runLocked({
//
//            dout("onChartManagerRefresh() filtering")
//
//            var removePolyLines = [NetworkSegmentPolyline]()
//
//            for polyLine in networkSegmentPolylines {
//
//                var segmentInChartsRegion = true
//
//                for coordinate in polyLine.coordinates {
//
//                    if !DKW2ChartManager.getChartManager().inChartsRegion(aGPDeg: Geo2(coordinate)) {
//                        segmentInChartsRegion = false
//                        break
//                    }
//                }
//
//                if !segmentInChartsRegion {
//                    removePolyLines.append(polyLine)
//                }
//
//            }
//
//            if let vc = vc,
//               removePolyLines.count > 0 {
//                dout("onChartManagerRefresh() removing")
//                vc.mapView.removeOverlays(removePolyLines)
//            }
//            dout("/onChartManagerRefresh()")
//        })
        
        dout("onChartManagerRefresh()")
        
        addVisibleNetworkLinesToMapview()
    }
    
    func onChartManagerChartEnabled(){
        StaticQueues.networkOverlaysLock.runLocked({
            
            dout("onChartManagerRefresh() filtering")
            
            var removePolyLines = [NetworkSegmentPolyline]()
            
            for polyLine in self.networkSegmentPolylines {
                
                var segmentInChartsRegion = true
                
                for coordinate in polyLine.coordinates {
                    
                    if !DKW2ChartManager.getChartManager().inChartsRegion(aGPDeg: Geo2(coordinate)) {
                        segmentInChartsRegion = false
                        break
                    }
                }
                
                if !segmentInChartsRegion {
                    removePolyLines.append(polyLine)
                }
                
            }
            
            if let vc = self.vc,
               removePolyLines.count > 0 {
                dout("onChartManagerRefresh() removing")
                StaticQueues.mapviewOverlaysLock.runLocked({ vc.mapView.removeOverlays(removePolyLines) })
            }
            dout("/onChartManagerRefresh()")
        })
    }
    
    //    func reloadNetworkOverlays() {
    func generateNetworkOverlays() {
        
        let loadWholeNetwork = true
        
        var segmentCount = 0
        var lineCount = 0
        
        var addedSegmentCount = 0
        var addedLineCount = 0
        
        if let network = WWNetwork.getNetwork() {
            
            dout("realoadNetworkOverlays: network.getSectionCount(): \(network.getSectionCount())")
            
            for i in 0 ..< network.getSectionCount() {
                let section = network.getSection(aIndex: i)
                
                // addsection
                
                // sectionInfo
                //                // Create a new section info struct.
                //                WWSectionDrawInfo SectionInfo = mSectionPool.get(aSection.getDataIndex());
                //
                //                boolean InfoValid = SectionInfo.InfoValid;
                //                if (SectionInfo.Section == null) {
                //                    SectionInfo.Section = aSection;
                //                    int Count = aSection.getSectionPointCount();
                //                    // PolyLine max length is all section points and start and end nodes
                //                    SectionInfo.PolyLine = new Point[Count + 2];
                //                    SectionInfo.PolyLinePos = new double[Count + 2];
                //                    // GeoPoints is collection of geopoints for all section points
                //                    SectionInfo.GeoPoints = new GeoPoint[Count];
                //
                //                    for (int i = 0; i < Count; i++) {
                //                        GeoPoint GP = new GeoPoint(0, 0);
                //                        StGeodeticCalibration.globalToGeoPoint(aSection.getSectionPoint(i).getPosGlb(), GP);
                //                        SectionInfo.GeoPoints[i] = GP;
                //                        SectionInfo.PolyLine[i] = new Point();
                //                    }
                //
                //                    SectionInfo.PolyLine[Count] = new Point();
                //                    SectionInfo.PolyLine[Count + 1] = new Point();
                //                    InfoValid = false;
                //                }
                
                let pointCount = section.getSectionPointCount()
                
                var polyLine = [Int2](repeating: Int2(), count: pointCount + 2)
                var polyLinePos = [Double](repeating: 0, count: pointCount + 2)
                var geoPoints = [GeoPointE6](repeating: GeoPointE6(), count: pointCount)
                
                var locationCoords = [CLLocationCoordinate2D]()
                var segmentInChartsRegion = true
                
                for j in 0 ..< pointCount {
                    
                    var sectionPoint = section.getSectionPoint(j)
                    
                    var geoPoint = GeodeticCalibration.globalToGeoPoint(sectionPoint.mPosGlb )
                    geoPoints[j] = geoPoint
                    //                    polyLine[j] = Int2()
                    
                    //                    dout("Debug latE6, LonE6: \(geoPoint.latE6), \(geoPoint.lonE6)")
                    
                    let coordinate = CLLocationCoordinate2D(latitude: Double(geoPoint.latE6) / 1000000.0, longitude: Double(geoPoint.lonE6) / 1000000.0)
                    
                    if loadWholeNetwork
                        || DKW2ChartManager.getChartManager().inChartsRegion(aGPDeg: Geo2(coordinate)){
                        
                        locationCoords.append(coordinate)
                        
                        // store distance value for monitoring at selection
                        polyLinePos[j] = sectionPoint.mDistance
                        
                        addedLineCount += 1
                    } else {
                        segmentInChartsRegion = false
                        break
                    }
                    
                    lineCount += 1
                }
                
                //                polyLine[pointCount] = Int2()
                //                polyLine[pointCount + 1] = Int2()
                
                //                if locationCoords.count >= 2 {
                
                if segmentInChartsRegion {
                    
                    var networkSegmentPolyline = NetworkSegmentPolyline(coordinates: locationCoords, count: locationCoords.count)
                    networkSegmentPolyline.polyLinePos = polyLinePos
                    
                    // link associated segment: todo:
                    networkSegmentPolyline.section = section // TODO: Test with this disabled
                    
                    //segmentsE6.append(geoPoints)
                    
                    networkSegmentPolylines.append(networkSegmentPolyline)
                }
                segmentCount += 1
                
            }
            
            dout("realoadNetworkOverlays: added segments to alloverlays")
            
            dout("segmentCount: \(segmentCount), lineCount: \(lineCount), addedLineCount: \(addedLineCount)")
            
            network.removeAllNetworkData() // clean up network data
            
        }
        
        allNetworkOverlays.append(contentsOf: networkSegmentPolylines)
        
    }
    
    class NearestSegmentResult {
        var nearestnetworkSegmentPolyline :NetworkSegmentPolyline? = nil
        var nearestLineStartingPoint :Int = -1
        var FSelected :Double = 0
        var Distance :Double = Double.greatestFiniteMagnitude
        
        var sectionPos :Double = 0
        var nearestMapPoint :MKMapPoint? = nil
    }
    
    func getNearestSegment(mapPoint :MKMapPoint, distanceThreshold :Double) -> NearestSegmentResult? {
        
        var result :NearestSegmentResult? = nil
        
        let mapView = ViewController.instance!.mapView
        let SRect = mapView!.visibleMapRect
        let displayRect = mapView!.bounds
        let mapCoordsToDisplayCoords = Double(displayRect.width) / SRect.width

        dout("onSingleTap \(mapPoint) \(SRect) \(displayRect) ")

        var P = Double2(x: mapPoint.x - 0.5, y: mapPoint.y - 0.5)
        var A = Double2()
        var B = Double2()
        var F :Double = 0.0
        
        //                float DistMax = WWHT_NETWORK * mScreenScale;
        //            var mapPointSelected = Double2()
        //            var nextMapPointSelected = Double2()
        
        var DistMin :Double = Double.greatestFiniteMagnitude

        
        //                ArrayList<WWSectionDrawInfo> sectionDrawCopy = new ArrayList<>();
        //                synchronized (mSectionsDraw) {
        //                    sectionDrawCopy = new ArrayList<>(mSectionsDraw); // because of concurrentmodification exception
        //                }
        
        
        //                for (WWSectionDrawInfo Section : sectionDrawCopy) {
        for (i, networkSegmentPolyline) in networkSegmentPolylines.enumerated() { // TODO: Lock and run in background
            
            //                    int SectionMask = StWWDefs.WWWaterbodyIDToMask(Section.Section.getWaterBodyID());
            //                    boolean InMask = (aWaterBodyMask & SectionMask) != 0;
            //                    if (InMask/* && dsqr < rs*/) {
            
            //                        for (int j = Section.PolyLineLen - 2; j >= 0; j--) {
            for j in 0 ..< networkSegmentPolyline.pointCount - 1 {
                var mapPoint = networkSegmentPolyline.points()[j]
                
                //                    A.x = mapPoint.x - SRect.minX
                //                    A.y = mapPoint.y - SRect.minY
                // tapped point not screen relative
                A.x = mapPoint.x
                A.y = mapPoint.y
                
                var nextMapPoint = networkSegmentPolyline.points()[j + 1]
                
                //                    B.x = nextMapPoint.x - SRect.minX
                //                    B.y = nextMapPoint.y - SRect.minY
                // tapped point not screen relative
                B.x = nextMapPoint.x
                B.y = nextMapPoint.y
                
                var D :Double = Math.distToLineSegment(aA: A, aB: B, aP: P, aFraction: &F)
                
                //                    var debugDist = Math.dist(A: Double2(x: tappedMappoint.x, y: tappedMappoint.y), B: Double2(x: mapPoint.x, y: mapPoint.y))
                
                //                    dout("tappedMappoint: \(tappedMappoint.x) \(tappedMappoint.y) mapPoint: \(mapPoint.x) \(mapPoint.y) D: \(D) dist: \(debugDist))")
                
                if D * mapCoordsToDisplayCoords < distanceThreshold, // check mininum distance between tapped point
                   D < DistMin {
                    
                    //                        dout("tappedMappoint: \(tappedMappoint.x) \(tappedMappoint.y) mapPoint: \(mapPoint.x) \(mapPoint.y) D: \(D) dist: \(debugDist) F: \(F))")
                    
                    //                                aHitSection.Section = Section.Section
                    //                                aHitSection.SectionPos = StMath.lerp(Section.PolyLinePos[j], Section.PolyLinePos[j + 1], F.getValue()) // closest point on the section
                    //                        let sectionPos = Math.lerp(mapPoint, nextMapPoint, F) // closest point on the section
                    
                    // TODO: Set new closest segmentpoint or segment ///////////
                    
                    DistMin = D
                    
                    if result == nil {
                        result = NearestSegmentResult()
                    }
                    
                    result?.Distance = DistMin
                    result?.FSelected = F
                    //                        mapPointSelected.x = A.x
                    //                        mapPointSelected.y = A.y
                    //                        nextMapPointSelected.x = B.x
                    //                        nextMapPointSelected.y = B.y
                    
                    result?.nearestnetworkSegmentPolyline = networkSegmentPolyline
                    result?.nearestLineStartingPoint = j
                    
                    //                        dout("new closest: \(D)")
                }
                
            }
            //                    }
        }
        
        if result != nil,
           let nearestnetworkSegmentPolyline = result?.nearestnetworkSegmentPolyline {
            
            
            let mapPoint = nearestnetworkSegmentPolyline.points()[result!.nearestLineStartingPoint]
            let nextMapPoint = nearestnetworkSegmentPolyline.points()[result!.nearestLineStartingPoint + 1]
            //                                aHitSection.SectionPos = StMath.lerp(Section.PolyLinePos[j], Section.PolyLinePos[j + 1], F.getValue()) // closest point on the section
            
            //                let sectionPos = Math.lerp(nearestnetworkSegmentPolyline.section.PolyLinePos[nearestLineStartingPoint], nearestnetworkSegmentPolyline.section.PolyLinePos[nearestLineStartingPoint + 1], FSelected) // closest point on the section
            
            result!.sectionPos = nearestnetworkSegmentPolyline.polyLinePos?[result!.nearestLineStartingPoint] ?? 0
            
            // Determine position:
            //                StMath.lerp(aPosGlb, mSectionPoints[Index1].getPosGlb(), mSectionPoints[Index2].getPosGlb(), Factor)
            result!.nearestMapPoint = Math.lerp(aA: mapPoint, aB: nextMapPoint, aFactor: result!.FSelected)
            
            dout("mapPointSelected: \(mapPoint.x) \(mapPoint.y) nextMapPointSelected: \(nextMapPoint.x) \(nextMapPoint.y) nearestLineStartingPoint: \(result!.nearestLineStartingPoint) D: \(result!.Distance) F: \(result!.FSelected))")
            
        }
        
        return result
    }
    
    func onSingleTap(_ sender :UIGestureRecognizer, tappedPoint :CGPoint, tappedLocation :CLLocationCoordinate2D){
        
        //        aHitSection.Section = null;
        //        aHitSection.SectionPos = 0;
        //
        //        final Projection pj = aMapView.getProjection();
        //        final Rect SRect = pj.getIntrinsicScreenRect();
        ////        final float MinSize = 5.0f / mScreenScale;
        //
        //        if (/*(mDrawFlags & wwdfSections) == 0 ||*/ aWaterBodyMask == 0 || !mDrawSections)
        //            return false;
        //
        //        if (isEnabled()) {
        //
        //            Float2 P = new Float2(aX - 0.5f, aY - 0.5f);
        //            Float2 A = new Float2();
        //            Float2 B = new Float2();
        //            MutableFloat F = new MutableFloat();
        //            float DistMax = WWHT_NETWORK * mScreenScale;
        //
        //            ArrayList<WWSectionDrawInfo> sectionDrawCopy = new ArrayList<>();
        //            synchronized (mSectionsDraw) {
        //                sectionDrawCopy = new ArrayList<>(mSectionsDraw); // because of concurrentmodification exception
        //            }
        //            for (WWSectionDrawInfo Section : sectionDrawCopy) {
        //                int SectionMask = StWWDefs.WWWaterbodyIDToMask(Section.Section.getWaterBodyID());
        //                boolean InMask = (aWaterBodyMask & SectionMask) != 0;
        ////                double dsqr = StMath.distSqr(P, Section.BoundsCenterPix);
        ////                double rs = StMath.sqr(Section.BoundsRadius + DistMax);
        //                if (InMask/* && dsqr < rs*/) {
        //
        ////                if ((aWaterBodyMask & SectionMask) != 0 && StMath.distSqr(P, Section.BoundsCenterPix) < StMath.sqr(Section.BoundsRadius + DistMax)) {
        //                    for (int j = Section.PolyLineLen - 2; j >= 0; j--) {
        //                        A.x = Section.PolyLine[j].x - SRect.left;
        //                        A.y = Section.PolyLine[j].y - SRect.top;
        //                        B.x = Section.PolyLine[j + 1].x - SRect.left;
        //                        B.y = Section.PolyLine[j + 1].y - SRect.top;
        //
        //                        float D = StMath.distToLineSegment(A, B, P, F);
        //                        if (D < DistMax) {
        //                            aHitSection.Section = Section.Section;
        //                            aHitSection.SectionPos = StMath.lerp(Section.PolyLinePos[j], Section.PolyLinePos[j + 1], F.getValue());
        //                            DistMax = D;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        return aHitSection.Section != null;
        
        let tappedMappoint = MKMapPoint(tappedLocation)
        
        //            aHitSection.Section = null;
        //            aHitSection.SectionPos = 0;
        
        //            final Projection pj = aMapView.getProjection();
        //            final Rect SRect = pj.getIntrinsicScreenRect();
        

        
        //            if (/*(mDrawFlags & wwdfSections) == 0 ||*/ aWaterBodyMask == 0 || !mDrawSections) {
        //                return false;
        //            }
        
        if isEnabled(),
           let vc = vc {
            
            let nearestSegmentResult = getNearestSegment(mapPoint: tappedMappoint, distanceThreshold: NetworkOverlay.tapDistanceThreshold)
            
            if let result = nearestSegmentResult,
               let nearestnetworkSegmentPolyline = result.nearestnetworkSegmentPolyline,
               let nearestMapPoint = result.nearestMapPoint{
                
                //                vc.mapView.removeOverlay(nearestnetworkSegmentPolyline)
                
                
                
                
                //                // toggle color
                //                if nearestnetworkSegmentPolyline.color == UIColor.red {
                //                    nearestnetworkSegmentPolyline.color = UIColor.blue
                //                } else {
                //                    nearestnetworkSegmentPolyline.color = UIColor.red
                //                }
                //                // remove/add to refresh the color
                //                vc.mapView.removeOverlay(nearestnetworkSegmentPolyline)
                //                vc.mapView.addOverlay(nearestnetworkSegmentPolyline)
                
                

                // set point and text
                //                var selectionCircle = MKPointAnnotation()
                //                vc.mapView.addOverlay(selectionCircle)
                
                if let selectionAnnotation = selectionAnnotation {
                    StaticQueues.mapviewAnnotationsLock.runLocked({ vc.mapView.removeAnnotation(selectionAnnotation) })
                }
                
                selectionAnnotation = NetworkSelectionAnnotation(mapPoint: nearestMapPoint, wwSection: nearestnetworkSegmentPolyline.section, sectionPos: result.sectionPos)
                //                selectionAnnotation?.coordinate = mapPoint.coordinate
                
                StaticQueues.mapviewAnnotationsLock.runLocked({ self.vc?.mapView.addAnnotation(self.selectionAnnotation!) })
                
            } else {
                if let selectionAnnotation = selectionAnnotation {
                    StaticQueues.mapviewAnnotationsLock.runLocked({ vc.mapView.removeAnnotation(selectionAnnotation) })
                }
                
            }
        }
        //            return aHitSection.Section != null
    }
    
    //    func drawSectionName(ISafeCanvas canvas, Point aPos, Rect aScreenRect) {
    //        String InfoText = mSectionSelected.Section.getInfoText(mSectionSelected.SectionPos, StDBManager.getManager().getBoatDef(), StUnits.Unit.M, mUnitSpeed, mValueBool);
    //        String Lines[] = InfoText.split("\\n");
    //        String Name = (Lines.length > 0) ? Lines[0] : "";
    //        String SubName = (Lines.length > 1) ? Lines[1] : "";
    //
    //        canvas.drawCircle(aPos.x, aPos.y, StUtils.dpToPx(5), mSectionPaint);
    //        mStructureTextPaint.setColor(mValueBool.getValue() ? Color.BLACK : Color.RED);
    //        mPoint.set(aPos.x - aScreenRect.left, aPos.y - aScreenRect.top);
    //        drawName(canvas.getWrappedCanvas(), Name, SubName, mPoint, true);
    //        mStructureTextPaint.setColor(Color.BLACK);
    //    }
    
    func getLocationOnNetwork(location :CLLocationCoordinate2D) -> CLLocationCoordinate2D? {
        
        if NetworkOverlay.disableNetworkNavigation { return nil }
        
        let mappoint = MKMapPoint(location)
        let nearestSegmentResult = getNearestSegment(mapPoint: mappoint, distanceThreshold: NetworkOverlay.tapDistanceThreshold)

        //
        
        return nearestSegmentResult?.nearestMapPoint?.coordinate
    }
    
    func getNearestLocationOnNetwork(location :CLLocationCoordinate2D) -> CLLocationCoordinate2D? {
        
        if NetworkOverlay.disableNetworkNavigation { return nil }

        
        let nearestSegmentResult = getNearestSegment(mapPoint: MKMapPoint(location), distanceThreshold: Double.greatestFiniteMagnitude)
        
        //
        
        return nearestSegmentResult?.nearestMapPoint?.coordinate
    }
    
    func deselect(){
        
        if let vc = vc,
           let selectionAnnotation = selectionAnnotation {
            StaticQueues.mapviewAnnotationsLock.runLocked({ vc.mapView.removeAnnotation(selectionAnnotation) })
        }
    }
}
