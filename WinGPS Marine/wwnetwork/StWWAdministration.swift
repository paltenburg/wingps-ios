//
//  StWWAdministration.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StWWAdministration {

//    // Administration

    let mDepartment :String
    let mFaxNr :String
    let mMailAddress :String
    let mMailPlace :String
    let mMailPostalCode :String
    let mName :String
    let mPhoneNr :String
    let mManType :WWManagerType
    let mVisitAddress :String
    let mVisitPlace :String
    let mWebsite :String

    init(aDepartment :String, aFaxNr :String, aMailAddress :String, aMailPlace :String,
    aMailPostalCode :String, aName :String, aPhoneNr :String,
    aManType :WWManagerType, aVisitAddress :String, aVisitPlace :String,
    aWebsite :String){

        mDepartment = aDepartment
        mFaxNr = aFaxNr
        mMailAddress = aMailAddress
        mMailPlace = aMailPlace
        mMailPostalCode = aMailPostalCode
        mName = aName
        mPhoneNr = aPhoneNr
        mManType = aManType
        mVisitAddress = aVisitAddress
        mVisitPlace = aVisitPlace
        mWebsite = aWebsite
    }
}
