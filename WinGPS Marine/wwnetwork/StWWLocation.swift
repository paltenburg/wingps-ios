//
//  StWWLocation.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

public enum WWLocationClass {
    case BARRIER
    case BRIDGE
    case FIXEDSTRUCTURE
    case LOCK
    case NONPHYSBARRIER
    case SHIPTUNNEL
    case BOATLIFT
}

class StWWLocation {

    let mClass :WWLocationClass
    let mDataIndex :Int
    let mGUID :StUUID
    let mGenerateWpt :Bool
    let mISRS :String
    let mName :String
    let mNetwork :WWNetwork
    let mPosGlb :Double3
    let mSection :StWWSection
    let mSectionPos :Double

    init(aNetwork :WWNetwork, aGUID :StUUID, aDataIndex :Int, aClass :WWLocationClass,
         aISRS :String, aName :String,
         aPosGlb :Double3, aSection :StWWSection, aSectionPos :Double, aGenerateWpt :Bool) {
        
        mClass = aClass
        mDataIndex = aDataIndex
        mGenerateWpt = aGenerateWpt
        mGUID = aGUID
        mISRS = aISRS
        mName = aName
        mNetwork = aNetwork
        mPosGlb = Double3(aPosGlb)
        mSection = aSection
        mSectionPos = aSectionPos
    }
}
