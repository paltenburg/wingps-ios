//
//  WWNetworkReader1_0_0_3.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 03/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class WWNetworkReader1_0_0_3 {
    
    private static let WW_BPT_UNDEFINED                  :UInt8 =  0
    private static let WW_BPT_BASCULEBRIDGE              :UInt8 =  1
    private static let WW_BPT_DOUBLEBASCULEBRIDGE        :UInt8 =  2
    private static let WW_BPT_DOUBLESWINGBRIDGE          :UInt8 =  3
    private static let WW_BPT_DRAWBRIDGE                 :UInt8 =  4
    private static let WW_BPT_DOUBLEDRAWBRIDGE           :UInt8 =  5
    private static let WW_BPT_SWINGBRIDGE                :UInt8 =  6
    private static let WW_BPT_ROLLINGBASCULEBRIDGE       :UInt8 =  7
    private static let WW_BPT_DOUBLEROLLINGBASCULEBRIDGE :UInt8 =  8
    private static let WW_BPT_LIFTBRIDGE                 :UInt8 =  9
    private static let WW_BPT_TRAPDOORBRIDGE             :UInt8 = 10
    private static let WW_BPT_PONTOONSWINGBRIDGE         :UInt8 = 11
    private static let WW_BPT_PONTOONBRIDGE              :UInt8 = 12
    private static let WW_BPT_RETRACTABLEBRIDGE          :UInt8 = 13
    private static let WW_BPT_FIXEDSPAN                  :UInt8 = 14
    private static let WW_BPT_STRAUSSBASCULEBRIDGE       :UInt8 = 15
    private static let WW_BPT_TRANSPORTERBRIDGE          :UInt8 = 16
    
    private static let WW_BT_WEIR              :UInt8 = 0
    private static let WW_BT_FLOODGATE         :UInt8 = 1
    private static let WW_BT_STORMSURGEBARRIER :UInt8 = 2
    
    private static let WW_CONSTRAINT_DIRECTION_NEGATIVE :UInt8 = 0
    private static let WW_CONSTRAINT_DIRECTION_BOTH     :UInt8 = 1
    private static let WW_CONSTRAINT_DIRECTION_POSITIVE :UInt8 = 2
    
    private static let WW_CONSTRAINT_TYPE_VESSELSIZE    :UInt8 = 0
    private static let WW_CONSTRAINT_TYPE_DIRECTION     :UInt8 = 1
    private static let WW_CONSTRAINT_TYPE_SHIPPINGCLASS :UInt8 = 2
    private static let WW_CONSTRAINT_TYPE_CURRENTS      :UInt8 = 3
    private static let WW_CONSTRAINT_TYPE_PERMIT        :UInt8 = 4
    private static let WW_CONSTRAINT_TYPE_VHFREPORT     :UInt8 = 5
    private static let WW_CONSTRAINT_TYPE_VHFSECTOR     :UInt8 = 6
    private static let WW_CONSTRAINT_TYPE_BANK          :UInt8 = 7  // added in 1.0.0.3
    private static let WW_CONSTRAINT_TYPE_COMMERCEEXCEPT :UInt8 = 8  // added in 1.0.0.3
    private static let WW_CONSTRAINT_TYPE_INFORMATION    :UInt8 = 9  // added in 1.0.0.3
    
    private static let WW_DAY_FIXEDDATE       :UInt8 =  0
    private static let WW_DAY_GOODFRIDAY      :UInt8 =  1
    private static let WW_DAY_EASTER          :UInt8 =  2
    private static let WW_DAY_EASTERMONDAY    :UInt8 =  3
    private static let WW_DAY_ASCENSIONDAY    :UInt8 =  4
    private static let WW_DAY_PENTECOST       :UInt8 =  5
    private static let WW_DAY_PENTECOSTMONDAY :UInt8 =  6
    private static let WW_DAY_CORPUSCHRISTI   :UInt8 =  7
    private static let WW_DAY_KONINGINNEDAG   :UInt8 =  8
    private static let WW_DAY_BEVRIJDINGSDAG5 :UInt8 =  9
    private static let WW_DAY_BUSSUNDBETTAG   :UInt8 = 10
    
    private static let WW_FST_UNDEFINED       :UInt8 = 0
    private static let WW_FST_AQUEDUCT        :UInt8 = 1
    private static let WW_FST_TUNNEL          :UInt8 = 2
    private static let WW_FST_SIPHON          :UInt8 = 3
    private static let WW_FST_CABLE           :UInt8 = 4
    private static let WW_FST_PIPELINE        :UInt8 = 5
    private static let WW_FST_CONVEYERBELT    :UInt8 = 6
    private static let WW_FST_FORMERSTRUCTURE :UInt8 = 7
    private static let WW_FST_POWERLINE       :UInt8 = 8
    private static let WW_FST_GANTRY          :UInt8 = 9
    
    private static let WW_LOCATIONCLS_BARRIER        :UInt8 = 0
    private static let WW_LOCATIONCLS_BRIDGE         :UInt8 = 1
    private static let WW_LOCATIONCLS_FIXEDSTRUCTURE :UInt8 = 2
    private static let WW_LOCATIONCLS_LOCK           :UInt8 = 3
    private static let WW_LOCATIONCLS_NONPHYSBARRIER  :UInt8 = 4
    private static let WW_LOCATIONCLS_SHIPTUNNEL        :UInt8 = 5
    private static let WW_LOCATIONCLS_BOATLIFT           :UInt8 = 6
    
    
    private static let WW_MT_UNKNOWN                     :UInt8 =  0
    private static let WW_MT_NL_MINISTRYOFDEFENCE        :UInt8 =  1
    private static let WW_MT_NL_DGSM                     :UInt8 =  2
    private static let WW_MT_NL_RVOB                     :UInt8 =  3
    private static let WW_MT_ENERGYTRANSPORT             :UInt8 =  4
    private static let WW_MT_NL_MUNICIPALITY             :UInt8 =  5
    private static let WW_MT_FOREIGNMUNICIPALITY         :UInt8 =  6
    private static let WW_MT_MUNICIPALPORTADMINISTRATION :UInt8 =  7
    private static let WW_MT_PORTADMINISTRATION          :UInt8 =  8
    private static let WW_MT_HOOGHEEMRAADSCHAP           :UInt8 =  9
    private static let WW_MT_HEEMRAADSCHAP               :UInt8 = 10
    private static let WW_MT_PORTAUTHORITY               :UInt8 = 11
    private static let WW_MT_NL_MINISTRYOFAGRICULTURE    :UInt8 = 12
    private static let WW_MT_NL_NS                       :UInt8 = 13
    private static let WW_MT_PRIVATE                     :UInt8 = 14
    private static let WW_MT_PROVINCE                    :UInt8 = 15
    private static let WW_MT_BE_HETRIJK                  :UInt8 = 16
    private static let WW_MT_RECREATION                  :UInt8 = 17
    private static let WW_MT_NL_RIJKSWATERSTAAT          :UInt8 = 18
    private static let WW_MT_DE_WASSERUNDSCHIFFFAHRTSAMT :UInt8 = 19
    private static let WW_MT_WATERSCHAP                  :UInt8 = 20
    private static let WW_MT_FR_VNFDT                    :UInt8 = 21
    
    
    private static let WW_NVT_UNKNOWN       :UInt8 = 0
    private static let WW_NVT_NAPDEFAULT    :UInt8 = 1
    private static let WW_NVT_NAPWATERLEVEL :UInt8 = 2
    private static let WW_NVT_CLEARANCE     :UInt8 = 3
    
    private static let WW_OPERATIONRULE_NOOPERATION :UInt8 = 0
    private static let WW_OPERATIONRULE_ASNORMAL    :UInt8 = 1
    private static let WW_OPERATIONRULE_ASWEEKDAY   :UInt8 = 2
    private static let WW_OPERATIONRULE_ASSPECIFIED :UInt8 = 3
    
    private static let WW_SF_RECREATION :UInt8 = 1
    private static let WW_SF_COMMERCIAL :UInt8 = 2
    
    private static let WW_VRD_UNKNOWN        :UInt8 = 0
    private static let WW_VRD_BOEZEMLEVEL    :UInt8 = 1
    private static let WW_VRD_MEANWATERLEVEL :UInt8 = 2
    private static let WW_VRD_CANALLEVEL     :UInt8 = 3
    private static let WW_VRD_LAKELEVEL      :UInt8 = 4
    private static let WW_VRD_NAP            :UInt8 = 5
    private static let WW_VRD_POLDERLEVEL    :UInt8 = 6
    private static let WW_VRD_WEIRLEVEL      :UInt8 = 7
    private static let WW_VRD_HSWLEVEL       :UInt8 = 8
    
    private static let WW_WATERBODYID_NORTHSEA                  :UInt8 = 0
    private static let WW_WATERBODYID_WADDENSEA                 :UInt8 =   1
    private static let WW_WATERBODYID_IJSSELMEERANDMARKERMEER   :UInt8 =   2
    private static let WW_WATERBODYID_INLANDNETHERLANDS         :UInt8 =   3
    private static let WW_WATERBODYID_INLANDBELGUIMANDLUXEMBURG :UInt8 =   4
    private static let WW_WATERBODYID_INLANDGERMANY             :UInt8 =   5
    private static let WW_WATERBODYID_INLANDFRANCE              :UInt8 =   6
    private static let WW_WATERBODYID_INLANDGREATBRITAIN        :UInt8 =   7
    private static let WW_WATERBODYID_BALTICSEA                 :UInt8 =   8
    private static let WW_WATERBODYID_INLANDDANMARK             :UInt8 =   9
    private static let WW_WATERBODYID_INLANDSWEDEN              :UInt8 =  10
    private static let WW_WATERBODYID_DONAU                     :UInt8 =  11
    
    private static let WW_WEEKDAY_SUNDAY    :UInt8 = 0
    private static let WW_WEEKDAY_MONDAY    :UInt8 = 1
    private static let WW_WEEKDAY_TUESDAY   :UInt8 = 2
    private static let WW_WEEKDAY_WEDNESDAY :UInt8 = 3
    private static let WW_WEEKDAY_THURSDAY  :UInt8 = 4
    private static let WW_WEEKDAY_FRIDAY    :UInt8 = 5
    private static let WW_WEEKDAY_SATURDAY  :UInt8 = 6
    
    private static let  WW_WWDIRECTION_ALL               :UInt8 = 0
    private static let  WW_WWDIRECTION_POSITIVE          :UInt8 = 1
    private static let  WW_WWDIRECTION_NEGATIVE          :UInt8 = 2
    
    private static let WW_OPERATIONTYPE_UNKNOWN         :UInt8 = 0
    private static let WW_OPERATIONTYPE_SELFSERVICE     :UInt8 = 1
    private static let WW_OPERATIONTYPE_REMOTECONTROL   :UInt8 = 2
    private static let WW_OPERATIONTYPE_SELFREMOTE      :UInt8 = 3
    private static let WW_OPERATIONTYPE_DETECTION       :UInt8 = 4
    private static let WW_OPERATIONTYPE_BUTTON          :UInt8 = 5
    private static let WW_OPERATIONTYPE_MOBILECREW      :UInt8 = 6
    private static let WW_OPERATIONTYPE_KEEPER          :UInt8 = 7
    private static let WW_OPERATIONTYPE_KEY             :UInt8 = 8
    
    // Non physical barrier type constants
    private static let WW_NPBT_UNDEFINED      :UInt8 = 0
    private static let WW_NPBT_ONEWAYTRAFFIC  :UInt8 = 1
    
    private var mFixupConstraint = [WWFixup]()
    private var mFixupStructure = [WWFixup]()
    private var mWWNetwork :WWNetwork
    
    private var mLoader : WWNetwork.WWNetworkLoader
    
    private var mDays = [StWWDay]()
    
    var mPos :Int = 0 // running position in databuffer
    
    init( aWWNetwork :WWNetwork, aLoader :WWNetwork.WWNetworkLoader) {
        //        mFixupConstraint = new ArrayList<>()
        //        mFixupStructure = new ArrayList<>()
        mWWNetwork = aWWNetwork
        
        mLoader = aLoader
    }
    
    func read(aFh :FileHandle, aError :inout String) throws /*IOException*/ {
        
        //        ByteBuffer NetworkFileBuffer = StUtils.readBuffer(aFc, (int)aFc.size())
        
        let NetworkFileBuffer = aFh.readDataToEndOfFile() as NSData
        dout("NetworkFileBuffer.count (should be equal to filesize minus the name+version) \(NetworkFileBuffer.count)")
        mPos = 0 // reading position in buffer
        
        // Read and validate header
        try readGuid(from: NetworkFileBuffer, at: &mPos)
        //        NetworkFileBuffer.getInt() // network version
        _ = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        let NodeCount = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        let SectionCount = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        let FairwayCount = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        let LocationCount = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        let AdminCount = try getInt32FromNSDataBuffer(from: NetworkFileBuffer, at: &mPos)
        
        if (NodeCount < 0 || SectionCount < 0 || FairwayCount < 0 || LocationCount < 0  || AdminCount < 0) {
            aError = StError.ER_FILEFORMAT
            return
        }
        if mLoader.isCancelled { return }
        
        dout("Read Nodes")
        try readNodes(NetworkFileBuffer, Int(NodeCount))
        if mLoader.isCancelled { return }
        
        
        dout("Read sections.")
        if try !readSections(aBuffer: NetworkFileBuffer, aSectionCount: SectionCount, aError: &aError) {
            dout("StWWNetworkReader: ReadSections failed...")
            return
        }
        if mLoader.isCancelled{
            return
        }
        
        dout("Read fairways.")
        if try !readFairways(aBuffer: NetworkFileBuffer, aFairwayCount: Int(FairwayCount), aError: &aError) {
            dout("StWWNetworkReader", "ReadFairways failed...")
            return
        }
        if mLoader.isCancelled{
            return
        }
        
        dout("Read Administrations.")
        if try !readAdministrations(aBuffer: NetworkFileBuffer, aAdminCount: Int(AdminCount), aError: &aError) {
            dout("StWWNetworkReader", "ReadAdministrations failed...")
            return
        }
        if mLoader.isCancelled {
            return
        }
        
        dout("Read locations.")
        if try !readLocations(aBuffer: NetworkFileBuffer, aLocationCount: Int(LocationCount), aError: &aError) {
            dout("StWWNetworkReader", "ReadLocations failed...")
            return
        }
        if mLoader.isCancelled {
            return
        }
        
        dout("Fix references.")
        if (!fixSectionPartReferences(aError: &aError)) {
            dout("StWWNetworkReader", "Fix references failed...")
            return
        }
        
        dout("Network Reader done. Remove references.")
        mFixupConstraint.removeAll()
        mFixupStructure.removeAll()
            
        
//        mWWNetwork.removeAllNetworkData() // TODO Debug
        
    }
    
    func readAdministrations(aBuffer :NSData, aAdminCount :Int, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        
        var Department, FaxNr, MailAddress, MailPlace, MailPostalCode, Name, PhoneNr, VisitAddress,
            VisitPlace, Website :String
        var ManType :WWManagerType
        
        var ManTypeByte :UInt8
        
        // Read administrations data.
        for i in 0 ..< aAdminCount {
            
            ManTypeByte = try aBuffer.get(&mPos)
            Department = try aBuffer.readStrW(&mPos)
            FaxNr = try aBuffer.readStrW(&mPos)
            MailAddress = try aBuffer.readStrW(&mPos)
            MailPlace = try aBuffer.readStrW(&mPos)
            MailPostalCode = try aBuffer.readStrW(&mPos)
            Name = try aBuffer.readStrW(&mPos)
            PhoneNr = try aBuffer.readStrW(&mPos)
            VisitAddress = try aBuffer.readStrW(&mPos)
            VisitPlace = try aBuffer.readStrW(&mPos)
            Website = try aBuffer.readStrW(&mPos)
            
            //            if (!BYTEToManType(ManType, ManTypeBYTE)) {
            //                aError = erFileFormat
            //                return false
            //            }
            do  {
                ManType = try BYTEToManType(aManTypeByte: ManTypeByte)
            } catch  {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            mWWNetwork.createAdministration(aDepartment: Department, aFaxNr: FaxNr, aMailAddress: MailAddress, aMailPlace: MailPlace, aMailPostalCode: MailPostalCode,
                                            aName: Name, aPhoneNr: PhoneNr, aManType: ManType, aVisitAddress: VisitAddress, aVisitPlace: VisitPlace, aWebsite: Website)
        }
        
        return true
    }
    
    private func BYTEToBarrierType(aBarrierByte :UInt8) throws -> WWBarrierType {
        switch (aBarrierByte) {
        case WWNetworkReader1_0_0_3.WW_BT_WEIR             : return WWBarrierType.WEIR
        case WWNetworkReader1_0_0_3.WW_BT_FLOODGATE        : return WWBarrierType.FLOODGATE
        case WWNetworkReader1_0_0_3.WW_BT_STORMSURGEBARRIER: return WWBarrierType.STORMSURGEBARRIER
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToBridgePassageType(aTypeByte :UInt8) throws -> WWBridgePassageType {
        switch (aTypeByte) {
        case WWNetworkReader1_0_0_3.WW_BPT_UNDEFINED                 : return WWBridgePassageType.UNDEFINED
        case WWNetworkReader1_0_0_3.WW_BPT_BASCULEBRIDGE             : return WWBridgePassageType.BASCULEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_DOUBLEBASCULEBRIDGE       : return WWBridgePassageType.DOUBLEBASCULEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_DOUBLESWINGBRIDGE         : return WWBridgePassageType.DOUBLESWINGBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_DRAWBRIDGE                : return WWBridgePassageType.DRAWBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_DOUBLEDRAWBRIDGE          : return WWBridgePassageType.DOUBLEDRAWBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_SWINGBRIDGE               : return WWBridgePassageType.SWINGBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_ROLLINGBASCULEBRIDGE      : return WWBridgePassageType.ROLLINGBASCULEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_DOUBLEROLLINGBASCULEBRIDGE: return WWBridgePassageType.DOUBLEROLLINGBASCULEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_LIFTBRIDGE                : return WWBridgePassageType.LIFTBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_TRAPDOORBRIDGE            : return WWBridgePassageType.TRAPDOORBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_PONTOONSWINGBRIDGE        : return WWBridgePassageType.PONTOONSWINGBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_PONTOONBRIDGE             : return WWBridgePassageType.PONTOONBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_RETRACTABLEBRIDGE         : return WWBridgePassageType.RETRACTABLEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_FIXEDSPAN                 : return WWBridgePassageType.FIXEDSPAN
        case WWNetworkReader1_0_0_3.WW_BPT_STRAUSSBASCULEBRIDGE      : return WWBridgePassageType.STRAUSSBASCULEBRIDGE
        case WWNetworkReader1_0_0_3.WW_BPT_TRANSPORTERBRIDGE         : return WWBridgePassageType.TRANSPORTERBRIDGE
            
        default: throw ThrowableError.generalError
        }
    }
    
    func BYTEToConstraintClass(aConstraintByte :UInt8) throws -> WWConstraintClass {
        switch (aConstraintByte) {
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_DIRECTION     : return WWConstraintClass.DIRECTION
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_SHIPPINGCLASS : return WWConstraintClass.SHIPPINGCLASS
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_VESSELSIZE    : return WWConstraintClass.VESSELSIZE
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_CURRENTS      : return WWConstraintClass.CURRENT
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_PERMIT        : return WWConstraintClass.PERMIT
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_VHFREPORT     : return WWConstraintClass.VHFREPORT
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_VHFSECTOR     : return WWConstraintClass.VHFSECTOR
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_BANK          : return WWConstraintClass.BANK
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_COMMERCEEXCEPT: return WWConstraintClass.COMMERCEEXCEPT
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_TYPE_INFORMATION   : return WWConstraintClass.INFORMATION
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToConstraintDirection(aDirectionByte :UInt8) throws -> WWConstraintDirection {
        switch (aDirectionByte) {
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_DIRECTION_NEGATIVE:
            return WWConstraintDirection.NEGATIVE
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_DIRECTION_BOTH     : return WWConstraintDirection.BOTH
        case WWNetworkReader1_0_0_3.WW_CONSTRAINT_DIRECTION_POSITIVE : return WWConstraintDirection.POSITIVE
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToDay(aDayByte :UInt8) throws -> StWWDay.WWDay {
        switch (aDayByte) {
        case WWNetworkReader1_0_0_3.WW_DAY_FIXEDDATE       : return StWWDay.WWDay.FIXEDDATE
        case WWNetworkReader1_0_0_3.WW_DAY_GOODFRIDAY      : return StWWDay.WWDay.GOODFRIDAY
        case WWNetworkReader1_0_0_3.WW_DAY_EASTER          : return StWWDay.WWDay.EASTER
        case WWNetworkReader1_0_0_3.WW_DAY_EASTERMONDAY    : return StWWDay.WWDay.EASTERMONDAY
        case WWNetworkReader1_0_0_3.WW_DAY_ASCENSIONDAY    : return StWWDay.WWDay.ASCENSIONDAY
        case WWNetworkReader1_0_0_3.WW_DAY_PENTECOST       : return StWWDay.WWDay.PENTECOST
        case WWNetworkReader1_0_0_3.WW_DAY_PENTECOSTMONDAY : return StWWDay.WWDay.PENTECOSTMONDAY
        case WWNetworkReader1_0_0_3.WW_DAY_CORPUSCHRISTI   : return StWWDay.WWDay.CORPUSCHRISTI
        case WWNetworkReader1_0_0_3.WW_DAY_KONINGINNEDAG   : return StWWDay.WWDay.NLKONINGINNEDAG
        case WWNetworkReader1_0_0_3.WW_DAY_BEVRIJDINGSDAG5 : return StWWDay.WWDay.NLBEVRIJDINGSDAG5
        case WWNetworkReader1_0_0_3.WW_DAY_BUSSUNDBETTAG   : return StWWDay.WWDay.DEBUSSUNDBETTAG
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToDirection(aDirectionBYTE :UInt8) throws -> WWDefs.WWDirection {
        switch (aDirectionBYTE) {
        case WWNetworkReader1_0_0_3.WW_WWDIRECTION_ALL: return WWDefs.WWDirection.wwdAll
        case WWNetworkReader1_0_0_3.WW_WWDIRECTION_POSITIVE: return WWDefs.WWDirection.wwdPositive
        case WWNetworkReader1_0_0_3.WW_WWDIRECTION_NEGATIVE: return WWDefs.WWDirection.wwdNegative
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToFixedStructureType(aTypeByte :UInt8) throws -> WWFixedStructureType {
        switch (aTypeByte) {
        case WWNetworkReader1_0_0_3.WW_FST_UNDEFINED      : return WWFixedStructureType.UNDEFINED
        case WWNetworkReader1_0_0_3.WW_FST_AQUEDUCT       : return WWFixedStructureType.AQUEDUCT
        case WWNetworkReader1_0_0_3.WW_FST_TUNNEL         : return WWFixedStructureType.TUNNEL
        case WWNetworkReader1_0_0_3.WW_FST_SIPHON         : return WWFixedStructureType.SIPHON
        case WWNetworkReader1_0_0_3.WW_FST_CABLE          : return WWFixedStructureType.CABLE
        case WWNetworkReader1_0_0_3.WW_FST_PIPELINE       : return WWFixedStructureType.PIPELINE
        case WWNetworkReader1_0_0_3.WW_FST_CONVEYERBELT   : return WWFixedStructureType.CONVEYERBELT
        case WWNetworkReader1_0_0_3.WW_FST_FORMERSTRUCTURE: return WWFixedStructureType.FORMERSTRUCTURE
        case WWNetworkReader1_0_0_3.WW_FST_POWERLINE      : return WWFixedStructureType.POWERLINE
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToLocationClass(aClassByte :UInt8) throws -> WWLocationClass {
        switch (aClassByte) {
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_BARRIER        : return WWLocationClass.BARRIER
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_BRIDGE         : return WWLocationClass.BRIDGE
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_FIXEDSTRUCTURE : return WWLocationClass.FIXEDSTRUCTURE
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_LOCK           : return WWLocationClass.LOCK
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_NONPHYSBARRIER : return WWLocationClass.NONPHYSBARRIER
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_SHIPTUNNEL     : return WWLocationClass.SHIPTUNNEL
        case WWNetworkReader1_0_0_3.WW_LOCATIONCLS_BOATLIFT       : return WWLocationClass.BOATLIFT
            
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToManType(aManTypeByte :UInt8) throws -> WWManagerType {
        switch (aManTypeByte) {
        case WWNetworkReader1_0_0_3.WW_MT_UNKNOWN                    : return WWManagerType.UNKNOWN
        case WWNetworkReader1_0_0_3.WW_MT_NL_MINISTRYOFDEFENCE       : return WWManagerType.NL_MINISTRYOFDEFENCE
        case WWNetworkReader1_0_0_3.WW_MT_NL_DGSM                    : return WWManagerType.NL_DGSM
        case WWNetworkReader1_0_0_3.WW_MT_NL_RVOB                    : return WWManagerType.NL_RVOB
        case WWNetworkReader1_0_0_3.WW_MT_ENERGYTRANSPORT            : return WWManagerType.ENERGYTRANSPORT
        case WWNetworkReader1_0_0_3.WW_MT_NL_MUNICIPALITY            : return WWManagerType.NL_MUNICIPALITY
        case WWNetworkReader1_0_0_3.WW_MT_FOREIGNMUNICIPALITY        : return WWManagerType.FOREIGNMUNICIPALITY
        case WWNetworkReader1_0_0_3.WW_MT_MUNICIPALPORTADMINISTRATION: return WWManagerType.MUNICIPALPORTADMINISTRATION
        case WWNetworkReader1_0_0_3.WW_MT_PORTADMINISTRATION         : return WWManagerType.PORTADMINISTRATION
        case WWNetworkReader1_0_0_3.WW_MT_HOOGHEEMRAADSCHAP          : return WWManagerType.HOOGHEEMRAADSCHAP
        case WWNetworkReader1_0_0_3.WW_MT_HEEMRAADSCHAP              : return WWManagerType.HEEMRAADSCHAP
        case WWNetworkReader1_0_0_3.WW_MT_PORTAUTHORITY              : return WWManagerType.PORTAUTHORITY
        case WWNetworkReader1_0_0_3.WW_MT_NL_MINISTRYOFAGRICULTURE   : return WWManagerType.NL_MINISTRYOFAGRICULTURE
        case WWNetworkReader1_0_0_3.WW_MT_NL_NS                      : return WWManagerType.NL_NS
        case WWNetworkReader1_0_0_3.WW_MT_PRIVATE                    : return WWManagerType.PRIVATE
        case WWNetworkReader1_0_0_3.WW_MT_PROVINCE                   : return WWManagerType.PROVINCE
        case WWNetworkReader1_0_0_3.WW_MT_BE_HETRIJK                 : return WWManagerType.BE_HETRIJK
        case WWNetworkReader1_0_0_3.WW_MT_RECREATION                 : return WWManagerType.RECREATION
        case WWNetworkReader1_0_0_3.WW_MT_NL_RIJKSWATERSTAAT         : return WWManagerType.NL_RIJKSWATERSTAAT
        case WWNetworkReader1_0_0_3.WW_MT_DE_WASSERUNDSCHIFFFAHRTSAMT: return WWManagerType.DE_WASSERUNDSCHIFFFAHRTSAMT
        case WWNetworkReader1_0_0_3.WW_MT_WATERSCHAP                 : return WWManagerType.WATERSCHAP
        case WWNetworkReader1_0_0_3.WW_MT_FR_VNFDT                   : return WWManagerType.FR_VNFDT
            
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToNAPValueType(aNAPValueByte :UInt8) throws -> WWNAPValueType {
        switch (aNAPValueByte) {
        case WWNetworkReader1_0_0_3.WW_NVT_UNKNOWN       : return WWNAPValueType.UNKNOWN
        case WWNetworkReader1_0_0_3.WW_NVT_NAPDEFAULT    : return WWNAPValueType.NAPDEFAULT
        case WWNetworkReader1_0_0_3.WW_NVT_NAPWATERLEVEL : return WWNAPValueType.NAPWATERLEVEL
        case WWNetworkReader1_0_0_3.WW_NVT_CLEARANCE     : return WWNAPValueType.CLEARANCE
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToNonPhysBarrierType(aTypeBYTE :UInt8) throws -> WWNonPhysBarrierType {
        switch (aTypeBYTE) {
        case WWNetworkReader1_0_0_3.WW_NPBT_UNDEFINED      : return WWNonPhysBarrierType.wnbtUndefined
        case WWNetworkReader1_0_0_3.WW_NPBT_ONEWAYTRAFFIC   : return WWNonPhysBarrierType.wnbtOnewayTraffic
        default: throw ThrowableError.generalError
        }
    }
    
    
    
    
    private func BYTEToOperationRule(aRuleByte :UInt8) throws -> StWWOperationRule.WWOperationRule {
        switch (aRuleByte) {
        case WWNetworkReader1_0_0_3.WW_OPERATIONRULE_NOOPERATION : return StWWOperationRule.WWOperationRule.NOOPERATION
        case WWNetworkReader1_0_0_3.WW_OPERATIONRULE_ASNORMAL    : return StWWOperationRule.WWOperationRule.ASNORMAL
        case WWNetworkReader1_0_0_3.WW_OPERATIONRULE_ASWEEKDAY   : return StWWOperationRule.WWOperationRule.ASWEEKDAY
        case WWNetworkReader1_0_0_3.WW_OPERATIONRULE_ASSPECIFIED : return StWWOperationRule.WWOperationRule.ASSPECIFIED
        default: throw ThrowableError.generalError
        }
    }
    
    static func BYTEToOperationType(aTypeBYTE :UInt8) -> StWWOperation.WWOperationType
    {
        ////////////// case constantes niet aanwezig in codes, maar onder andere naam
        switch (aTypeBYTE) {
        //            case WWDSGN_OPTYPE_UNKNOWN       : aType = wotUnknown; break;
        //            case WWDSGN_OPTYPE_SELFSERVICE   : aType = wotSelfService; break;
        //            case WWDSGN_OPTYPE_REMOTECONTROL : aType = wotRemoteControl; break;
        //            case WWDSGN_OPTYPE_SELFREMOTE    : aType = wotSelfRemote; break;
        //            case WWDSGN_OPTYPE_DETECTION     : aType = wotDetection; break;
        //            case WWDSGN_OPTYPE_BUTTON        : aType = wotButton; break;
        //            case WWDSGN_OPTYPE_MOBILECREW    : aType = wotMobileCrew; break;
        //            case WWDSGN_OPTYPE_KEEPER        : aType = wotKeeper; break;
        //            case WWDSGN_OPTYPE_KEY           : aType = wotKey; break;
        
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_UNKNOWN       : return StWWOperation.WWOperationType.wotUnknown
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_SELFSERVICE   : return StWWOperation.WWOperationType.wotSelfService
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_REMOTECONTROL : return StWWOperation.WWOperationType.wotRemoteControl
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_SELFREMOTE    : return StWWOperation.WWOperationType.wotSelfRemote
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_DETECTION     : return StWWOperation.WWOperationType.wotDetection
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_BUTTON        : return StWWOperation.WWOperationType.wotButton
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_MOBILECREW    : return StWWOperation.WWOperationType.wotMobileCrew
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_KEEPER        : return StWWOperation.WWOperationType.wotKeeper
        case WWNetworkReader1_0_0_3.WW_OPERATIONTYPE_KEY           : return StWWOperation.WWOperationType.wotKey
        default: return StWWOperation.WWOperationType.wotReadError
            
        }
        return StWWOperation.WWOperationType.wotReadError
    }
    
    private func BYTEToShippingFlags(aShippingFlagsByte :UInt8) throws -> UInt8 {
        var ShippingFlags :UInt8 = 0
        
        if ((aShippingFlagsByte & ~(WWNetworkReader1_0_0_3.WW_SF_RECREATION | WWNetworkReader1_0_0_3.WW_SF_COMMERCIAL)) != 0){
            throw ThrowableError.generalError
        }
        
        if ((aShippingFlagsByte & WWNetworkReader1_0_0_3.WW_SF_RECREATION) == WWNetworkReader1_0_0_3.WW_SF_RECREATION) {
            ShippingFlags |= WWDefs.WSF_RECREATION
        }
        if ((aShippingFlagsByte & WWNetworkReader1_0_0_3.WW_SF_COMMERCIAL) == WWNetworkReader1_0_0_3.WW_SF_COMMERCIAL) {
            ShippingFlags |= WWDefs.WSF_COMMERCIAL
        }
        
        return ShippingFlags
    }
    
    private func BYTEToVertRefDatum(aDatumByte :UInt8) throws -> WWVertRefDatum {
        switch (aDatumByte) {
        case WWNetworkReader1_0_0_3.WW_VRD_UNKNOWN       : return WWVertRefDatum.UNKNOWN
        case WWNetworkReader1_0_0_3.WW_VRD_BOEZEMLEVEL   : return WWVertRefDatum.BOEZEMLEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_MEANWATERLEVEL: return WWVertRefDatum.MEANWATERLEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_CANALLEVEL    : return WWVertRefDatum.CANALLEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_LAKELEVEL     : return WWVertRefDatum.LAKELEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_NAP           : return WWVertRefDatum.NAP
        case WWNetworkReader1_0_0_3.WW_VRD_POLDERLEVEL   : return WWVertRefDatum.POLDERLEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_WEIRLEVEL     : return WWVertRefDatum.WEIRLEVEL
        case WWNetworkReader1_0_0_3.WW_VRD_HSWLEVEL      : return WWVertRefDatum.HSWLEVEL
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToWaterBodyID(aWaterBodyByte :UInt8) throws -> WWWaterbodyID {
        switch (aWaterBodyByte) {
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_NORTHSEA                  : return WWWaterbodyID(WWWaterbodyID.NORTHSEA)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_WADDENSEA                 : return WWWaterbodyID(WWWaterbodyID.WADDENSEA)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_IJSSELMEERANDMARKERMEER   : return WWWaterbodyID(WWWaterbodyID.IJSSELMEERANDMARKERMEER)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDNETHERLANDS         : return WWWaterbodyID(WWWaterbodyID.INLANDNETHERLANDS)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDBELGUIMANDLUXEMBURG : return WWWaterbodyID(WWWaterbodyID.INLANDBELGIUMLUXEMBURG)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDGERMANY             : return WWWaterbodyID(WWWaterbodyID.INLANDGERMANY)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDFRANCE              : return WWWaterbodyID(WWWaterbodyID.INLANDFRANCE)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDGREATBRITAIN        : return WWWaterbodyID(WWWaterbodyID.INLANDGREATBRITAIN)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_BALTICSEA                 : return WWWaterbodyID(WWWaterbodyID.BALTICSEA)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDDANMARK             : return WWWaterbodyID(WWWaterbodyID.INLANDDANMARK)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_INLANDSWEDEN              : return WWWaterbodyID(WWWaterbodyID.INLANDSWEDEN)
        case WWNetworkReader1_0_0_3.WW_WATERBODYID_DONAU                     : return WWWaterbodyID(WWWaterbodyID.DONAU)
        //        default: throw Exception()
        default: throw ThrowableError.generalError
        }
    }
    
    private func BYTEToWeekDay(aWeekDayByte :UInt8) throws -> StDateTime.WeekDay {
    switch (aWeekDayByte) {
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_SUNDAY    : return StDateTime.WeekDay.SUNDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_MONDAY    : return StDateTime.WeekDay.MONDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_TUESDAY   : return StDateTime.WeekDay.TUESDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_WEDNESDAY : return StDateTime.WeekDay.WEDNESDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_THURSDAY  : return StDateTime.WeekDay.THURSDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_FRIDAY    : return StDateTime.WeekDay.FRIDAY
    case WWNetworkReader1_0_0_3.WW_WEEKDAY_SATURDAY  : return StDateTime.WeekDay.SATURDAY
    default: throw ThrowableError.generalError
    }
    }
    
    private func fixSectionPartReferences(aError :inout String) -> Bool {
        aError = StError.ER_OK
        
        // Fix structure references in section parts.
        for i in (0 ..< mFixupStructure.count).reversed() {
            var Fixup = mFixupStructure[i]
            if let index = Fixup.Index,
               index < 0 || index >= mWWNetwork.getStructureCount() {
                aError = StError.ER_FILEFORMAT
                return false
            }
            (Fixup.Obj as? StWWSectionPart)?.mStructure = mWWNetwork.getStructure(aIndex: Fixup.Index!)
        }
        
        // Fix constraint references in section parts.
        for i in (0 ..< mFixupConstraint.count).reversed() {
            var Fixup = mFixupConstraint[i]
            var Fairway = (Fixup.Obj as? StWWSectionPart)?.mSection.mFairway
            if let index = Fixup.Index,
               index < 0 || index >= Fairway!.getConstraintCount() {
                aError = StError.ER_FILEFORMAT
                return false
            }
            (Fixup.Obj as? StWWSectionPart)?.addConstraint(aConstraint: Fairway!.getConstraint(aIndex: Fixup.Index!))
        }
        
        return true
    }
    
    func readConstraints(aBuffer :NSData, aFairway :StWWFairway, aConstraintCount :Int, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        
        var ConstraintClassByte, Flags, Direction :UInt8
        var ConstraintClass :WWConstraintClass
        var ConstraintDirection :WWConstraintDirection
        var PeriodEnd, PeriodStart :Int64
        var Positive :Bool
        var Channel :Int
        var Callsign, InfoURL, Remark :String
        var Constraint :StWWConstraint
        var DepthMax, HeightMax, LengthMax, WidthMax :Float
        var End, Start :Double
        
        for i in 0 ..< aConstraintCount {
            ConstraintClassByte = try aBuffer.get(&mPos)
            
            do {
                try ConstraintClass = BYTEToConstraintClass(aConstraintByte: ConstraintClassByte)
            } catch {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            switch ConstraintClass {
            
            case WWConstraintClass.BANK:
                
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1 ///////////// TODO: test time conversion function
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                
                var LeftBank :Bool = try aBuffer.get(&mPos) != 0
                
                Constraint = StWWConstraintBank(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aLeftBank: LeftBank)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break
            
            case WWConstraintClass.COMMERCEEXCEPT:
                
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                
                var DepthMin :Float = try aBuffer.getFloat(&mPos)
                var HeightMin :Float = try aBuffer.getFloat(&mPos)
                var LengthMin :Float = try aBuffer.getFloat(&mPos)
                var WidthMin :Float = try aBuffer.getFloat(&mPos)
                
                Constraint = StWWConstraintCommerceExcept(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd,
                                                          aLengthMin: LengthMin, aWidthMin: WidthMin, aHeightMin: HeightMin, aDepthMin: DepthMin);
                aFairway.addConstraint(aConstraint: Constraint)
            //                    break;
            
            case WWConstraintClass.CURRENT:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                var Current = try aBuffer.getFloat(&mPos)
                Positive = try aBuffer.get(&mPos) != 0
                Constraint = StWWConstraintCurrent(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aCurrent: Current, aPositive: Positive)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.DIRECTION:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                Positive = try aBuffer.get(&mPos) != 0;
                Constraint = StWWConstraintDirection(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aPositive: Positive)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.INFORMATION:
                
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                
                var Information :String = try aBuffer.readStrW(&mPos)
                
                Constraint = StWWConstraintInformation(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aInformation: Information)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.PERMIT:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                InfoURL = try aBuffer.readStrW(&mPos)
                Remark = try aBuffer.readStrW(&mPos)
                Constraint = StWWConstraintPermit(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aInfoURL: InfoURL, aRemark: Remark)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.SHIPPINGCLASS:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                Flags = try aBuffer.get(&mPos)
                do {
                    Flags = try BYTEToShippingFlags(aShippingFlagsByte: Flags)
                } catch {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                Constraint = StWWConstraintShippingClass(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aShippingFlags: Flags)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.VESSELSIZE:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                DepthMax = try aBuffer.getFloat(&mPos)
                HeightMax = try aBuffer.getFloat(&mPos)
                LengthMax = try aBuffer.getFloat(&mPos)
                WidthMax = try aBuffer.getFloat(&mPos)
                Constraint = StWWConstraintVesselSize(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aLengthMax: LengthMax, aWidthMax: WidthMax, aHeightMax: HeightMax, aDepthMax: DepthMax)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.VHFREPORT:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                Direction = try aBuffer.get(&mPos)
                Channel = Int(try aBuffer.getInt(&mPos))
                Callsign = try aBuffer.readStrW(&mPos);
                do {
                    ConstraintDirection = try BYTEToConstraintDirection(aDirectionByte: Direction)
                } catch {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                Constraint = StWWConstraintVHFReport(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aDirection: ConstraintDirection, aCallsign: Callsign, aChannel: Channel)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            case WWConstraintClass.VHFSECTOR:
                End = try aBuffer.getDouble(&mPos)
                Start = try aBuffer.getDouble(&mPos)
                PeriodEnd = (End == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: End))?.getMilliseconds() ?? -1
                PeriodStart = (Start == 0) ? Int64(0) : (StUtils.TDateTimeToDate(TDateTime: Start))?.getMilliseconds() ?? -1
                Channel = Int(try aBuffer.getInt(&mPos))
                Callsign = try aBuffer.readStrW(&mPos)
                Constraint = StWWConstraintVHFSector(aPeriodStart: PeriodStart, aPeriodEnd: PeriodEnd, aCallsign: Callsign, aChannel: Channel)
                aFairway.addConstraint(aConstraint: Constraint)
            //                break;
            
            }
        }
        return true;
    }
    
    func readFairways(aBuffer :NSData, aFairwayCount :Int, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        
        var FairwayGUID :StUUID
        var SectionCount, ConstraintCount, SectionIndex :Int
        var MarksInFDir :Bool
        var Name :String
        
        for i in 0 ..< aFairwayCount {
            FairwayGUID = try readGuid(from: aBuffer, at: &mPos)
            SectionCount = Int(try aBuffer.getInt(&mPos))
            ConstraintCount = Int(try aBuffer.getInt(&mPos))
            MarksInFDir = try aBuffer.get(&mPos) != 0
            Name = try aBuffer.readStrW(&mPos)
            
            var Fairway :StWWFairway = mWWNetwork.createFairway(aGUID: FairwayGUID, aName: Name, aMarksInFDir: MarksInFDir)
            
            if SectionCount > 0 {
                for j in 0 ..< SectionCount {
                    SectionIndex = Int(try aBuffer.getInt(&mPos))
                    if /*SectionIndex < 0 || */SectionIndex >= mWWNetwork.getSectionCount() {
                        aError = StError.ER_FILEFORMAT
                        return false
                    }
                    if SectionIndex > -1 {
                        Fairway.addSection(aSection: mWWNetwork.getSection(aIndex: SectionIndex))
                    }
                }
            }
            
            if try !readConstraints(aBuffer: aBuffer, aFairway: Fairway, aConstraintCount: ConstraintCount, aError: &aError) {
                return false
            }
            if mLoader.isCancelled {
                return true
            }
            
            Fairway.generateSectionGroups()
        }
        
        return true
    }
    
    func readLocations(aBuffer :NSData, aLocationCount :Int, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        var PosGlb :Double3 = Double3()
        var Class, Type :UInt8 //ManTypeByte,
        var LocationGUID :StUUID
        var SectionIndex, PassageCount, AdminIndex, DescPassageCount, Number :Int
        var SectionPos :Double
        var Section :StWWSection
        var LocationClass :WWLocationClass
        var Name, ISRS, Address, PostalCode, Place, PhoneNr, Remark :String
        var VHFRadioChannel, ComplexName, ManDepartment :String
        //        String ManFaxNr, ManMailAddress, ManMailPlace, ManMailPostalCode;
        //        String ManName, ManPhoneNr, ManVisitAddress, ManVisitPlace;
        //        WWManagerType ManType;
        var BarrierType :WWBarrierType
        var PassageType :WWBridgePassageType
        var FixedStructureType :WWFixedStructureType
        var NonPhysBarrierType :WWNonPhysBarrierType
        var NormallyOpened, HasTidalDoors :Bool
        var HeightOpened, ThresholdDepth, Height :StWWVertValue?
        var DepthInside, DepthMiddle, DepthOutside :StWWVertValue?
        var Width, Fall, ChamberLength1, ChamberLength2, ChamberLengthTotal, ChamberWidth :Float
        var LockingLength, LockingLengthLowTide, MinGateWidth :Float
        var os :OperatedStructure = OperatedStructure()
        var daySchedule :OperationDaySchedule = OperationDaySchedule()
        
        for i in 0 ..< aLocationCount {
            //            BYTE    Class;
            //            bool    GenerateWpt;
            //            GUID    GUID;
            //            double3 PosGlb;
            //            int     SectionIndex;
            //            double  SectionPos;
            //
            Class = try aBuffer.get(&mPos)
            var GenerateWpt :Bool = try aBuffer.get(&mPos) != 0
            LocationGUID = try readGuid(from: aBuffer, at: &mPos)
            PosGlb.x = try aBuffer.getDouble(&mPos)
            PosGlb.y = try aBuffer.getDouble(&mPos)
            PosGlb.z = try aBuffer.getDouble(&mPos)
            SectionIndex = Int(try aBuffer.getInt(&mPos))
            SectionPos = try aBuffer.getDouble(&mPos)
            if (SectionIndex < 0 || SectionIndex >= mWWNetwork.getSectionCount()) {
                aError = StError.ER_FILEFORMAT
                return false
            }
            Section = mWWNetwork.getSection(aIndex: SectionIndex)
            
            do {
                LocationClass = try BYTEToLocationClass(aClassByte: Class)
            } catch {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            Name = try aBuffer.readStrW(&mPos)
            ISRS = try aBuffer.readStrW(&mPos)
            
            AdminIndex = Int(try aBuffer.getInt(&mPos))
            Address = try aBuffer.readStrW(&mPos)
            PostalCode = try aBuffer.readStrW(&mPos)
            Place = try aBuffer.readStrW(&mPos)
            PhoneNr = try aBuffer.readStrW(&mPos)
            VHFRadioChannel = try aBuffer.readStrW(&mPos)
            ComplexName = try aBuffer.readStrW(&mPos)
            PassageCount = Int(try aBuffer.getInt(&mPos))
            
            if AdminIndex < 0 || AdminIndex > mWWNetwork.getAdministrationCount() {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            var Admin :StWWAdministration = mWWNetwork.getAdministration(aIndex: AdminIndex)
            
            if mLoader.isCancelled {
                return true
            }
            
            switch (LocationClass) {
            case WWLocationClass.BARRIER:
                Type = try aBuffer.get(&mPos)
                NormallyOpened = try aBuffer.get(&mPos) != 0
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                do {
                    BarrierType = try BYTEToBarrierType(aBarrierByte: Type)
                } catch {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                var Barrier :StWWBarrier = mWWNetwork.createBarrier(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb, aSection: Section, aSectionPos: SectionPos, aGenerateWpt: GenerateWpt,
                                                                    aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount, aPhoneNr: PhoneNr, aPlace: Place, aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel, aNormallyOpened: NormallyOpened, aType: BarrierType)
                
                for j in 0 ..< DescPassageCount {
                    Number = Int(try aBuffer.getInt(&mPos))
                    
                    HeightOpened = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    ThresholdDepth = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    Width = try aBuffer.getFloat(&mPos)
                    Remark = try aBuffer.readStrW(&mPos)
                    
                    Barrier.createPassage(aHeightOpened: HeightOpened!, aNumber: Number, aRemark: Remark, aThresholdDepth: ThresholdDepth!, aWidth: Width)
                    
                    if mLoader.isCancelled {
                        return true
                    }
                }
                break
            case WWLocationClass.BRIDGE:
                os.clear()
                if (try !readOperatedStructure(aBuffer: aBuffer, aOperatedStructure: os, aSchedule: daySchedule)) {
                    return false
                }
                
                if (mLoader.isCancelled) {
                    return true
                }
                
                NormallyOpened = try aBuffer.get(&mPos) != 0
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                var Bridge = mWWNetwork.createBridge(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb, aSection: Section, aSectionPos: SectionPos, aGenerateWpt: GenerateWpt,
                                                     aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount, aPhoneNr: PhoneNr, aPlace: Place, aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel, aOperationExplanation: os.OperationExplanation,
                                                     aOperationType: os.OperationType, /* timezone? */ aOperationPeriods: os.OperationPeriods, aOperationRules: os.OperationRules, aNormallyOpened: NormallyOpened)
                
                for j in 0 ..< DescPassageCount {
                    
                    // Read the passage data.
                    Type = try aBuffer.get(&mPos)
                    Number = Int(try aBuffer.getInt(&mPos))
                    
                    var HeightClosed = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    HeightOpened = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    ThresholdDepth = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    Width = try aBuffer.getFloat(&mPos)
                    
                    Remark = try aBuffer.readStrW(&mPos)
                    ISRS = try aBuffer.readStrW(&mPos)
                    
                    
                    // Validate the passage data.
                    do {
                        PassageType = try BYTEToBridgePassageType(aTypeByte: Type)
                    } catch {
                        aError = StError.ER_FILEFORMAT
                        return false
                    }
                    
                    // Create a passage.
                    Bridge.createPassage(aHeightClosed: HeightClosed!, aHeightOpened: HeightOpened!, aISRS: ISRS, aNumber: Number, aRemark: Remark, aThresholdDepth: ThresholdDepth!, aType: PassageType, aWidth: Width)
                    
                    if (mLoader.isCancelled) {
                        return true
                    }
                }
                
                break
            case WWLocationClass.FIXEDSTRUCTURE:
                Type = try aBuffer.get(&mPos)
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                do {
                    FixedStructureType = try BYTEToFixedStructureType(aTypeByte: Type)
                } catch {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                var FixedStructure :StWWFixedStructure = mWWNetwork.createFixedStructure(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb,
                                                                                         aSection: Section, aSectionPos: SectionPos, aGenerateWpt: GenerateWpt, aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount,
                                                                                         aPhoneNr: PhoneNr, aPlace: Place, aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel, aType: FixedStructureType)
                
                // Read described passages.
                for j in 0 ..< DescPassageCount {
                    
                    // Read the passage data.
                    Number = Int(try aBuffer.getInt(&mPos))
                    
                    Height = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    ThresholdDepth = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    Width = try aBuffer.getFloat(&mPos)
                    Remark = try aBuffer.readStrW(&mPos)
                    
                    // Create a passage.
                    FixedStructure.createPassage(aHeight: Height!, aNumber: Number, aRemark: Remark, aThresholdDepth: ThresholdDepth!, aWidth: Width)
                    
                    if (mLoader.isCancelled) {
                        return true
                    }
                }
                
                break
                
            case WWLocationClass.NONPHYSBARRIER:
                os.clear()
                if try !readOperatedStructure(aBuffer: aBuffer, aOperatedStructure: os, aSchedule: daySchedule) {
                    return false
                }
                
                //// CWWNetworkReader1_0_0_3::ReadNonPhysBarrier(CStream &aStream, EWWNonPhysBarrierType &aType, int &aDescPassageCount, EError &aError) {
                // Read fixed structure properties.
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                var TypeByte :UInt8 = try aBuffer.get(&mPos)
                
                do {
                    NonPhysBarrierType = try BYTEToNonPhysBarrierType(aTypeBYTE: TypeByte)
                } catch {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                var NonPhysBarrier = mWWNetwork.createNonPhysBarrier(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb,
                                                                     aSection: Section, aSectionPos: SectionPos, aGenerateWpt: GenerateWpt, aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount, aPhoneNr: PhoneNr, aPlace: Place,
                                                                     aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel, aOperationExplanation: os.OperationExplanation, aOperationType: os.OperationType, /*TimeZone,*/
                                                                     aOperationPeriods: os.OperationPeriods, aOperationRules: os.OperationRules, aType: NonPhysBarrierType)
                
                for j in 0 ..< DescPassageCount {
                    
                    // Read the passage data.
                    Number = Int(try aBuffer.getInt(&mPos))
                    Remark = try aBuffer.readStrW(&mPos)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    NonPhysBarrier.createPassage(aNumber: Number, aRemark: Remark)
                    
                    if (mLoader.isCancelled) {
                        return true
                    }
                }
                
                break
            case WWLocationClass.LOCK:
                os.clear()
                if try !readOperatedStructure(aBuffer: aBuffer, aOperatedStructure: os, aSchedule: daySchedule) {
                    return false
                }
                
                if (mLoader.isCancelled) {
                    return true
                }
                
                Fall = try aBuffer.getFloat(&mPos)
                NormallyOpened = try aBuffer.get(&mPos) != 0
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                var Lock = mWWNetwork.createLock(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb, aSection: Section, aSectionPos: SectionPos,
                                                 aGenerateWpt: GenerateWpt, aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount,
                                                 aPhoneNr: PhoneNr, aPlace: Place, aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel,
                                                 aOperationExplanation: os.OperationExplanation, aOperationType: os.OperationType, aOperationPeriods: os.OperationPeriods, aOperationRules: os.OperationRules,
                                                 aFall: Fall, aNormallyOpened: NormallyOpened)
                
                // Read the described passages.
                for j in 0 ..< DescPassageCount {
                    
                    // Read the lock chamber data.
                    ChamberLength1 = try aBuffer.getFloat(&mPos)
                    ChamberLength2 = try aBuffer.getFloat(&mPos)
                    ChamberLengthTotal = try aBuffer.getFloat(&mPos)
                    ChamberWidth = try aBuffer.getFloat(&mPos)
                    
                    DepthInside = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    DepthMiddle = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    DepthOutside = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    HasTidalDoors = try aBuffer.get(&mPos) != 0
                    
                    Height = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    LockingLength = try aBuffer.getFloat(&mPos)
                    LockingLengthLowTide = try aBuffer.getFloat(&mPos)
                    MinGateWidth = try aBuffer.getFloat(&mPos)
                    Number = Int(try aBuffer.getInt(&mPos))
                    Name = try aBuffer.readStrW(&mPos)
                    Remark = try aBuffer.readStrW(&mPos)
                    ISRS = try aBuffer.readStrW(&mPos)
                    
                    // Create a lock chamber.
                    Lock.createLockChamber(aChamberLength1: ChamberLength1, aChamberLength2: ChamberLength2, aChamberLengthTotal: ChamberLengthTotal, aChamberWidth: ChamberWidth,
                                           aDepthInside: DepthInside!, aDepthMiddle: DepthMiddle!, aDepthOutside: DepthOutside!, aHeight: Height!, aHasTidalDoors: HasTidalDoors, aISRS: ISRS, aLockingLength: LockingLength, aLockingLengthLowTide: LockingLengthLowTide, aMinGateWidth: MinGateWidth,
                                           aName: Name, aNumber: Number, aRemark: Remark)
                    
                    if (mLoader.isCancelled) {
                        return true
                    }
                }
                break
            case WWLocationClass.SHIPTUNNEL:
                
                os.clear()
                if (try !readOperatedStructure(aBuffer: aBuffer, aOperatedStructure: os, aSchedule: daySchedule)) {
                    return false
                }
                
                DescPassageCount = Int(try aBuffer.getInt(&mPos))
                if (DescPassageCount < 0) {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
                
                // Support and functionality for ship-tunnels in WinGPS Marine has yet to be implemented!
                var Tunnel = mWWNetwork.createTunnel(aGUID: LocationGUID, aDataIndex: i, aISRS: ISRS, aName: Name, aPosGlb: PosGlb, aSection: Section, aSectionPos: SectionPos,
                                                     aGenerateWpt: GenerateWpt, aAdmin: Admin, aAddress: Address, aComplexName: ComplexName, aPassageCount: PassageCount, aPhoneNr: PhoneNr, aPlace: Place, aPostalCode: PostalCode, aVHFRadioChannel: VHFRadioChannel,
                                                     aOperationExplanation: os.OperationExplanation, aOperationType: os.OperationType, /*TimeZone,*/ aOperationPeriods: os.OperationPeriods, aOperationRules: os.OperationRules)
                
                //                    // contents of: CWWNetworkReader1_0_0_3::ReadTunnelTube:
                //                    // Read the described passages.
                for j in 0 ..< DescPassageCount {
                    //
                    //                        SWWTunnelTube1_0_0_3 Data
                    
                    //                        struct SWWTunnelTube1_0_0_3 {
                    //                            int                     Number
                    //                            float                   Length
                    //                            float                   Width
                    //                            SWWVertValue1_0_0_0     Depth
                    //                            SWWVertValue1_0_0_0     Height
                    //                        }
                    
                    //                        CStrW Remark
                    //                        if (!aStream.ReadBuf(&Data, sizeof(Data), &aError) ||
                    //                        !aStream.ReadStrW(Remark, &aError))
                    //                        return false
                    //
                    //                        EWWVertRefDatum DatumDepth, DatumHeight
                    //                        EWWNAPValueType NAPValueTypeDepth, NAPValueTypeHeight
                    //
                    //                        // Validate the tunnel tube data.
                    //                        if (!BYTEToVertRefDatum(DatumHeight, Data.Height.Datum) ||
                    //                                !BYTEToNAPValueType(NAPValueTypeHeight, Data.Height.NAPType) ||
                    //                                !BYTEToVertRefDatum(DatumDepth, Data.Depth.Datum) ||
                    //                                !BYTEToNAPValueType(NAPValueTypeDepth, Data.Depth.NAPType)) {
                    //                            aError = erFileFormat
                    //                            return false
                    //                        }
                    
                    Number = Int(try aBuffer.getInt(&mPos))
                    var Length = try aBuffer.getFloat(&mPos)
                    Width = try aBuffer.getFloat(&mPos)
                    
                    var Depth = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    Height = try readWWVertValue(aBuffer: aBuffer, aError: &aError)
                    if aError != StError.ER_OK {
                        return false
                    }
                    
                    Remark = try aBuffer.readStrW(&mPos)
                    
                    // Create a lock chamber.
                    //                        Tunnel.createTunnelTube(new StWWVertValue(Depth.Value, Depth.Datum, Depth.NAPValue, Depth.NAPType),
                    //                                new StWWVertValue(Height.Value, Height.Datum, Height.NAPValue, Height.NAPType), Length,
                    //                                Number, Remark, Width)
                    Tunnel.createTunnelTube(aDepth: Depth!, aHeight: Height!, aLength: Length, aNumber: Number, aRemark: Remark, aWidth: Width)
                    
                    if (mLoader.isCancelled){
                        return true
                    }
                }
                
            //                break
            
            case WWLocationClass.BOATLIFT:
                dout("BOATLIFT not applicable here")
            }
        }
        return true
    }
    
    func readNodes(_ aBuffer :NSData, _ aNodeCount :Int) throws {
        var PosGlb = Double3()
        var NodeGUID :StUUID
        
        for i in 0 ..< aNodeCount {
            NodeGUID = try readGuid(from: aBuffer, at: &mPos)
            PosGlb.x = try aBuffer.getDouble(&mPos)
            PosGlb.y = try aBuffer.getDouble(&mPos)
            PosGlb.z = try aBuffer.getDouble(&mPos)
            
            mWWNetwork.createNode(aGUID: NodeGUID, aDataIndex: i, aPosGlb: PosGlb)
            if (mLoader.isCancelled){
                return
            }
        }
    }
    
    private func readOperatedStructure(aBuffer :NSData, aOperatedStructure :OperatedStructure, aSchedule :OperationDaySchedule) throws -> Bool {
        try aBuffer.readStrW(&mPos) // String TimeZoneId =  // not used in Android
        aOperatedStructure.OperationExplanation = try aBuffer.readStrW(&mPos)
        
        //        aOperatedStructure.RemoteControlled = try aBuffer.get(&mPos) != 0
        var OperationTypeBYTE = try aBuffer.get(&mPos)
        
        var OperationPeriodCount = Int(try aBuffer.getInt(&mPos))
        var OperationRuleCount = Int(try aBuffer.getInt(&mPos))
        
        aOperatedStructure.OperationType = WWNetworkReader1_0_0_3.BYTEToOperationType(aTypeBYTE: OperationTypeBYTE)
        if (aOperatedStructure.OperationType == StWWOperation.WWOperationType.wotReadError) {
            //            aError = StError.ER_FILEFORMAT
            return false
        }
        
        var DayScheduleCount, DayCount :Int
        var Range :Int64
        var Description :String
        var DayOfWeek, Rule, Flags, Day, Offset :UInt8
        var DirectionByte :UInt8
        var NotAfter, NotBefore :Bool
        var NotAfterTime, NotBeforeTime, FixedDate :Int16
        var WeekDay :StDateTime.WeekDay
        var Direction :WWDefs.WWDirection
        var OperationRule :StWWOperationRule.WWOperationRule
        var WWNotAfterTime, WWNotBeforeTime :StWWTime
        var WWDay :StWWDay.WWDay
         
        // Read operation periods
        for i in 0 ..< OperationPeriodCount {
            Range = try readUIntBuff(fb: aBuffer, pos: &mPos)
            Description = try aBuffer.readStrW(&mPos)
            DayScheduleCount = Int(try aBuffer.getInt(&mPos))
            
            var DateRange = StWWDateRange(aStartAndEnd: Range)
            var Period = StWWOperationPeriod(aRange: DateRange, aDescription: Description)
            
            for j in 0 ..< DayScheduleCount {
                aSchedule.clear()
                if (try !readOperationDaySchedule(aBuffer: aBuffer, aSchedule: aSchedule)) {
                    return false
                }
                
                Period.addDaySchedule(aDayMask: aSchedule.DayMask, aTimes: aSchedule.Times)
            }
            
            aOperatedStructure.OperationPeriods.append(Period)
        }
        
        // Read the operation rules.
        for i in 0 ..< OperationRuleCount {
            
            // Read operation rule.
            DayOfWeek = try aBuffer.get(&mPos)
            DirectionByte = try aBuffer.get(&mPos)
            NotAfter = try aBuffer.get(&mPos) != 0
            NotAfterTime = try aBuffer.getShort(&mPos)
            NotBefore = try aBuffer.get(&mPos) != 0
            NotBeforeTime = try aBuffer.getShort(&mPos)
            Rule = try aBuffer.get(&mPos)
            Flags = try aBuffer.get(&mPos)
            DayCount = Int(try aBuffer.getInt(&mPos))
            
            do {
                WeekDay = try BYTEToWeekDay(aWeekDayByte: DayOfWeek)
                Direction = try BYTEToDirection(aDirectionBYTE: DirectionByte)
                OperationRule = try BYTEToOperationRule(aRuleByte: Rule)
                Flags = try BYTEToShippingFlags(aShippingFlagsByte: Flags)
            } catch {
                return false
            }
            
            WWNotAfterTime = try StWWTime(aTime: Int(NotAfterTime))
            WWNotBeforeTime = try StWWTime(aTime: Int(NotBeforeTime))
            
            // Read operation day schedule of operation rule.
            if (OperationRule == StWWOperationRule.WWOperationRule.ASSPECIFIED) {
                aSchedule.clear()
                if (try !readOperationDaySchedule(aBuffer: aBuffer, aSchedule: aSchedule)) {
                    return false
                }
            }
            
            mDays.removeAll()
            for d in 0 ..< DayCount {
                Day = try aBuffer.get(&mPos)
                FixedDate = try aBuffer.getShort(&mPos)
                Offset = try aBuffer.get(&mPos)
                
                do {
                    WWDay = try BYTEToDay(aDayByte: Day)
                } catch {
                    return false
                }
                
                var StDay = StWWDay(aDay: WWDay, aFixedDate: StWWDate(aDate: Int(FixedDate)), aOffset: Offset)
                mDays.append(StDay)
            }
            
            var WWRule = StWWOperationRule(aDays: mDays, aRule: OperationRule, aDayOfWeek: WeekDay, aDirection: Direction, aTimes: aSchedule.Times, aNotAfter: NotAfter, aNotAfterTime: WWNotAfterTime, aNotBefore: NotBefore, aNotBeforeTime: WWNotBeforeTime, aShippingFlags: Flags)
            
            aOperatedStructure.OperationRules.append(WWRule)
        }
        
        return true
    }
    
    func readOperationDaySchedule(aBuffer :NSData,  aSchedule :OperationDaySchedule) throws -> Bool {
        
        aSchedule.DayMask = try aBuffer.get(&mPos)
        var TimesCount = Int(try aBuffer.getInt(&mPos))
        
        var Flags :UInt8
        var Direction :WWDefs.WWDirection
        var Range :Int64
        
        if ((aSchedule.DayMask & 0x80) == 0x80){
            return false
        }
        
        for k in 0 ..< TimesCount {
            var Indication = try aBuffer.readStrW(&mPos)
            Flags = try aBuffer.get(&mPos)
            var DirectionByte = try aBuffer.get(&mPos)
            Range = try readUIntBuff(fb: aBuffer, pos: &mPos)
            
            do {
                Flags = try BYTEToShippingFlags(aShippingFlagsByte: Flags)
                Direction = try BYTEToDirection(aDirectionBYTE: DirectionByte)
            } catch {
                return false
            }
            
            var TimeRange = StWWTimeRange(aStartAndEnd: Range)
            var OperationTime = StWWOperationTime(aDirection: Direction, aIndication: Indication, aShippingFlags: Flags, aTimeRange: TimeRange)
            aSchedule.Times.append(OperationTime)
        }
        return true
    }
    
    func readSectionParts(aBuffer :NSData, aSection :StWWSection, aSectionPartCount :Int, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        
        var PosStart, PosEnd :Double
        var SpeedMax :Float
        var ConstraintCount, StructureIndex, ConstraintIndex :Int
        
        for i in 0 ..< aSectionPartCount {
            PosStart = try aBuffer.getDouble(&mPos)
            PosEnd = try aBuffer.getDouble(&mPos)
            SpeedMax = try aBuffer.getFloat(&mPos)
            ConstraintCount = Int(try aBuffer.getInt(&mPos))
            StructureIndex = Int(try aBuffer.getInt(&mPos))
            
            if PosEnd < PosStart {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            if PosStart == PosEnd {
                if StructureIndex == -1 || ConstraintCount > 0 {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
            } else {
                if StructureIndex != -1 {
                    aError = StError.ER_FILEFORMAT
                    return false
                }
            }
            
            var SectionPart :StWWSectionPart = aSection.addSectionPart(aPartStartPos: PosStart, aPartEndPos: PosEnd, aSpeedMax: SpeedMax)
            
            if ConstraintCount > 0 {
                for j in 0..<ConstraintCount {
                    ConstraintIndex = Int(try aBuffer.getInt(&mPos))
                    var Fixup :WWFixup = WWFixup()
                    Fixup.Obj = SectionPart
                    Fixup.Index = ConstraintIndex
                    mFixupConstraint.append(Fixup)
                }
            }
            
            if StructureIndex > -1 {
                var Fixup :WWFixup = WWFixup()
                Fixup.Obj = SectionPart
                Fixup.Index = StructureIndex
                mFixupStructure.append(Fixup)
            }
        }
        return true
    }
    
    func readSectionPoints(aBuffer :NSData, aSection :StWWSection, aSectionPointCount :Int) throws {
        var PosGlb = Double3()
        var Distance :Double
        var DistanceMark :Int
        
        for i in 0 ..< aSectionPointCount {
            PosGlb.x = try aBuffer.getDouble(&mPos)
            PosGlb.y = try aBuffer.getDouble(&mPos)
            PosGlb.z = try aBuffer.getDouble(&mPos)
            Distance = try aBuffer.getDouble(&mPos)
            DistanceMark = Int(try aBuffer.getInt(&mPos))
            
            aSection.addSectionPoint(aPosGlb: PosGlb, aDistance: Double(Distance), aDistanceMark: Int(DistanceMark))
        }
        
    }
    
    private func readSections(aBuffer :NSData, aSectionCount :Int32, aError :inout String) throws -> Bool {
        aError = StError.ER_OK
        
        var PosGlb = Double3()
        var SectionGUID :StUUID
        var HeadNodeIndex, TailNodeIndex, SectionPointCount, SectionPartCount :Int
        
        var BoundsRadius, Len :Double
        var WaterBodyIDByte :UInt8
        var WaterBodyID :WWWaterbodyID
        var Name :String
        
        for i in 0 ..< aSectionCount {
            SectionGUID = try readGuid(from: aBuffer, at: &mPos)
            HeadNodeIndex = Int(try aBuffer.getInt(&mPos))
            TailNodeIndex = Int(try aBuffer.getInt(&mPos))
            SectionPointCount = Int(try aBuffer.getInt(&mPos))
            PosGlb.x = try aBuffer.getDouble(&mPos)
            PosGlb.y = try aBuffer.getDouble(&mPos)
            PosGlb.z = try aBuffer.getDouble(&mPos)
            BoundsRadius = try aBuffer.getDouble(&mPos)
            Len = try aBuffer.getDouble(&mPos)
            WaterBodyIDByte = try aBuffer.get(&mPos)
            SectionPartCount = Int(try aBuffer.getInt(&mPos))
            
            do {
                try WaterBodyID = BYTEToWaterBodyID(aWaterBodyByte: WaterBodyIDByte)
            } catch ThrowableError.generalError {
                aError = StError.ER_FILEFORMAT
                return false
            }
            
            if HeadNodeIndex < 0 || HeadNodeIndex >= mWWNetwork.getNodeCount() ||
                TailNodeIndex < 0 || TailNodeIndex >= mWWNetwork.getNodeCount() || SectionPointCount < 0 {
                aError = StError.ER_FILEFORMAT
                return false
            }
            Name = try aBuffer.readStrW(&mPos) // TODO: Test string reading function //////////////
            
            let Section :StWWSection = mWWNetwork.createSection(aGUID: SectionGUID, aDataIndex: Int(i), aHeadNode: mWWNetwork.getNode(Int(HeadNodeIndex)), aTailNode: mWWNetwork.getNode(Int(TailNodeIndex)), aBoundsCenterGlb: PosGlb, aBoundsRadius: BoundsRadius, aLen: Len, aName: Name, aWaterBodyID: WaterBodyID)
            
            try readSectionPoints(aBuffer: aBuffer, aSection: Section, aSectionPointCount: Int(SectionPointCount))
            if mLoader.isCancelled {
                return true
            }
            
            if try !readSectionParts(aBuffer: aBuffer, aSection: Section, aSectionPartCount: Int(SectionPartCount), aError: &aError) {
                return false
            }
            if mLoader.isCancelled {
                return true
            }
        }
        
        return true
    }
    
    func readWWVertValue(aBuffer :NSData, aError :inout String) throws -> StWWVertValue? {
        
        do {
            aError = StError.ER_OK
            
            var TVertValue :Float = try aBuffer.getFloat(&mPos)
            var TVertDatum :UInt8 = try aBuffer.get(&mPos)
            var TNAPValue :Float = try aBuffer.getFloat(&mPos)
            var TNAPType :UInt8 = try aBuffer.get(&mPos)
            
            var TDatum :WWVertRefDatum
            var TNAPValueType :WWNAPValueType
            do {
                TDatum = try BYTEToVertRefDatum(aDatumByte: TVertDatum)
                TNAPValueType = try BYTEToNAPValueType(aNAPValueByte: TNAPType)
            } catch {
                aError = StError.ER_FILEFORMAT
                return nil
            }
            return StWWVertValue(aValue: TVertValue, aDatum: TDatum, aNAPValue: TNAPValue, aNAPType: TNAPValueType)
            
        } catch {
            dout("StWWNetworkRdr - popke - BufferUnderflowException gecatched")
            aError = StError.ER_BUFFERUNDERFLOW
            return nil
        }
    }
    
    private class WWFixup {
        var Obj :Any? = nil
        var Index :Int? = nil
    }
    
    class OperatedStructure {
        var OperationExplanation :String = ""
        var RemoteControlled :Bool = false // 1.0.0.1
        var OperationType :StWWOperation.WWOperationType = StWWOperation.WWOperationType.wotUnknown // 1.0.0.3
        var OperationPeriods = [StWWOperationPeriod]()
        var OperationRules = [StWWOperationRule]()
        
        func clear() {
            OperationExplanation = ""
            OperationPeriods.removeAll()
            OperationRules.removeAll()
        }
    }
    
    class OperationDaySchedule {
        var DayMask :UInt8 = 0
        var Times = [StWWOperationTime]()
        func clear() {
            DayMask = 0
            Times.removeAll()
        }
    }
}
