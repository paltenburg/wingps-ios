//
//  StWWFairway.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StWWFairway {

    var mConstraints = [StWWConstraint]()
    let mGUID :StUUID
    var mLen :Double
    let mMarksInFDir :Bool
    let mName :String
    var mSections = [StWWSection]()
    
    init(aGUID :StUUID, aName :String, aMarksInFDir :Bool) {
        mGUID = aGUID
        mLen = 0
        mMarksInFDir = aMarksInFDir
        mName = aName
//        mConstraints = new ArrayList<>();
//        mSections = new ArrayList<>();
    }
    
    func addConstraint(aConstraint :StWWConstraint) {
        mConstraints.append(aConstraint)
    }
    
    func addSection(aSection :StWWSection) {
        mSections.append(aSection)
        if let Index :Int = mSections.firstIndex(where: {$0 === aSection}){
            aSection.setFairway(aFairway: self, aFairwayIndex: Index, aFairwayPos: mLen)
            mLen += aSection.mLen
        }
    }

    func generateSectionGroups() {
        var SectionCount :Int = mSections.count
    }

    func getConstraint(aIndex :Int) -> StWWConstraint {
        return mConstraints[aIndex]
    }

    func getConstraintCount() -> Int {
        return mConstraints.count
    }
    
//    public final UUID getGUID() {
//        return mGUID;
//    }
//    
//    public double getLen() {
//        return mLen;
//    }
//    
//    public boolean getMarksInFDir() {
//        return mMarksInFDir;
//    }
//    
//    public final String getName() {
//        return mName;
//    }
//    
//    public StWWSection getSection(int aIndex) {
//        return mSections.get(aIndex);
//    }
//    
//    public int getSectionCount() {
//        return mSections.size();
//    }
//    
}
