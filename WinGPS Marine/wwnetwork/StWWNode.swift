//
//  StWWNode.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 05/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StWWNode {
    
    private var mDataIndex :Int
    private var mGUID :StUUID
    private var mPosGlb :Double3
    private var mSections :[StWWSection]
    
    init(_ aGUID :StUUID, _ aDataIndex :Int, _ aPosGlb :Double3) {
        mDataIndex = aDataIndex
        mGUID = aGUID
        mPosGlb = Double3(aPosGlb)
        mSections = [StWWSection]()
    }
    
    func addSection(_ aSection :StWWSection) {
        mSections.append(aSection)
    }
    
}
