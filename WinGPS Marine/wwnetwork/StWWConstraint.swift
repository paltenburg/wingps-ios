//
//  StWWConstraint.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 11/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

enum WWConstraintClass {
    case DIRECTION
    case SHIPPINGCLASS
    case VESSELSIZE
    case VHFREPORT
    case VHFSECTOR
    case PERMIT
    case CURRENT
    case BANK
    case COMMERCEEXCEPT
    case INFORMATION
}

enum WWConstraintDirection {
    case NEGATIVE
    case BOTH
    case POSITIVE
}

class StWWConstraint {

    static let WPRNOPERIOD :Int = 0; // No period defined.
    static let WPRBEFORE :Int = 1; // Current time is before the period.
    static let WPRDURING :Int = 2; // Current time is during the period.
    static let WPRAFTER :Int = 3; // The current time is after the period.

    let mConstraintClass :WWConstraintClass
    let mPeriodEnd :Int64
    let mPeriodStart :Int64

    init(aClass :WWConstraintClass, aPeriodStart :Int64, aPeriodEnd :Int64) {
        mConstraintClass = aClass
        mPeriodEnd = aPeriodEnd
        mPeriodStart = aPeriodStart
    }

    func evaluatePeriod(aEntryDateTime :Int64) -> Int {
        return StWWConstraint.WPRNOPERIOD
/*
        if (mPeriodStart == 0)
            return WPRNOPERIOD;
        else if (aEntryDateTime < mPeriodStart)
            return WPRBEFORE;
        else if (aEntryDateTime >= mPeriodEnd)
            return WPRAFTER;
        else
            return WPRDURING;
*/
    }
}

class StWWConstraintCurrent :StWWConstraint {
    
    let mCurrent :Float
    let mPositive :Bool
    
    init(aPeriodStart :Int64, aPeriodEnd :Int64, aCurrent :Float, aPositive :Bool) {
        mCurrent = aCurrent
        mPositive = aPositive
        
        super.init(aClass: WWConstraintClass.CURRENT, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintDirection :StWWConstraint {

    let mPositive :Bool
    
    init(aPeriodStart :Int64, aPeriodEnd :Int64, aPositive :Bool) {
        mPositive = aPositive
        
        super.init(aClass: WWConstraintClass.DIRECTION, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintBank :StWWConstraint {

    let mLeftBank :Bool

    init(aPeriodStart :Int64, aPeriodEnd :Int64, aLeftBank :Bool){
        mLeftBank = aLeftBank
        
        super.init(aClass: WWConstraintClass.BANK, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }

}

class StWWConstraintCommerceExcept :StWWConstraint {
    
    let mLengthMin :Float
    let mWidthMin :Float
    let mHeightMin :Float
    let mDepthMin :Float
    
    init(aPeriodStart :Int64, aPeriodEnd :Int64, aLengthMin :Float, aWidthMin :Float, aHeightMin :Float, aDepthMin :Float) {
        
        mLengthMin = aLengthMin
        mWidthMin = aWidthMin
        mHeightMin = aHeightMin
        mDepthMin = aDepthMin
        
        super.init(aClass: WWConstraintClass.COMMERCEEXCEPT, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintInformation :StWWConstraint {
    
    let mInformation :String

    init(aPeriodStart :Int64, aPeriodEnd :Int64, aInformation :String) {
        mInformation = aInformation
        
        super.init(aClass: WWConstraintClass.INFORMATION, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintPermit :StWWConstraint {
    
    let mInfoURL :String
    let mRemark :String
    
    init(aPeriodStart :Int64, aPeriodEnd :Int64, aInfoURL :String, aRemark :String) {
        mInfoURL = aInfoURL
        mRemark = aRemark
        
        super.init(aClass: WWConstraintClass.PERMIT, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintShippingClass :StWWConstraint {

    let mShippingFlags :UInt8
    
    public init(aPeriodStart :Int64, aPeriodEnd :Int64, aShippingFlags :UInt8) {
        mShippingFlags = aShippingFlags
        
        super.init(aClass: WWConstraintClass.SHIPPINGCLASS, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintVesselSize  :StWWConstraint {

    let mDepthMax :Float
    let mHeightMax :Float
    let mLengthMax :Float
    let mWidthMax :Float
    
    init(aPeriodStart :Int64, aPeriodEnd :Int64, aLengthMax :Float, aWidthMax :Float,
          aHeightMax :Float, aDepthMax :Float) {

        mDepthMax = aDepthMax
        mHeightMax = aHeightMax
        mLengthMax = aLengthMax
        mWidthMax = aWidthMax
        
        super.init(aClass: WWConstraintClass.VESSELSIZE, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintVHFReport :StWWConstraint {

    let mCallsign :String
    let mChannel :Int
    let mDirection :WWConstraintDirection

    init(aPeriodStart :Int64, aPeriodEnd :Int64, aDirection :WWConstraintDirection, aCallsign :String, aChannel :Int) {

        mCallsign = aCallsign
        mChannel = aChannel
        mDirection = aDirection
        
        super.init(aClass: WWConstraintClass.VHFREPORT, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}

class StWWConstraintVHFSector :StWWConstraint {
    
    let mCallsign :String
    let mChannel :Int
    
    init(aPeriodStart :Int64,aPeriodEnd :Int64, aCallsign :String, aChannel :Int) {
        mCallsign = aCallsign
        mChannel = aChannel
        
        super.init(aClass: WWConstraintClass.VHFSECTOR, aPeriodStart: aPeriodStart, aPeriodEnd: aPeriodEnd)

    }
}
