//
//  UnitSettingsController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 20/03/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class UnitSettingsController: UIViewController {
	
	@IBOutlet var backgroundView: UIView!
	@IBOutlet weak var unitModeSegmentedControl: UISegmentedControl!
	@IBOutlet weak var unitSettingsContainer: UIView!
	@IBOutlet weak var speedLabel: UILabel!
	@IBOutlet weak var distanceLargeLabel: UILabel!
	@IBOutlet weak var distanceSmallLabel: UILabel!
	
	let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
	
	weak var delegate: SettingsDelegate? = ViewController.instance

	var speed : String = ""
	var distanceLarge : String = ""
	var distanceSmall : String = ""
	
	enum UnitMode {
		case nautical
		case metric
		case US
		case custom
	}
	
	var unitMode = UnitMode.nautical
	
	override func viewDidLoad() {
		super.viewDidLoad()
		unitSettingsContainer.layer.cornerRadius = 8
		unitSettingsContainer.clipsToBounds = true
		
		speed = defaultSettings?.string(forKey: Resources.SETTINGS_UNIT_SPEED) ?? Resources.SETTINGS_UNIT_SPEED_DEFAULT
		
		distanceLarge = defaultSettings?.string(forKey: Resources.SETTINGS_UNIT_DISTANCE_LARGE) ?? Resources.SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT
		
		distanceSmall = defaultSettings?.string(forKey: Resources.SETTINGS_UNIT_DISTANCE_SMALL) ?? Resources.SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT
		
		checkUnitMode()
		
	}
	
	func checkUnitMode(){
		if(speed == Resources.SETTINGS_UNIT_SPEED_KN && distanceLarge == Resources.SETTINGS_UNIT_DISTANCE_LARGE_NMILE && distanceSmall == Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER){
			unitMode = .nautical
			/*unitModeSegmentedControl.setEnabled(true, forSegmentAt: 0)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 1)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 2)*/
			unitModeSegmentedControl.selectedSegmentIndex = 0
		} else if(speed == Resources.SETTINGS_UNIT_SPEED_KMH && distanceLarge == Resources.SETTINGS_UNIT_DISTANCE_LARGE_KM && distanceSmall == Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER){
			unitMode = .metric
			/*unitModeSegmentedControl.setEnabled(true, forSegmentAt: 1)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 0)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 2)*/
			unitModeSegmentedControl.selectedSegmentIndex = 1
		} else if(speed == Resources.SETTINGS_UNIT_SPEED_MPH && distanceLarge == Resources.SETTINGS_UNIT_DISTANCE_LARGE_MILE && distanceSmall == Resources.SETTINGS_UNIT_DISTANCE_SMALL_YARD){
			unitMode = .US
			/*unitModeSegmentedControl.setEnabled(true, forSegmentAt: 2)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 0)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 1)*/
			unitModeSegmentedControl.selectedSegmentIndex = 2
		} else {
			unitMode = .custom
			/* unitModeSegmentedControl.setEnabled(false, forSegmentAt: 0)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 1)
			unitModeSegmentedControl.setEnabled(false, forSegmentAt: 2)
			*/
			unitModeSegmentedControl.selectedSegmentIndex = 3
		}
	}
	
	func setUnits(){
		
		defaultSettings?.set(speed, forKey: Resources.SETTINGS_UNIT_SPEED)
		defaultSettings?.set(distanceLarge, forKey: Resources.SETTINGS_UNIT_DISTANCE_LARGE)
		defaultSettings?.set(distanceSmall, forKey: Resources.SETTINGS_UNIT_DISTANCE_SMALL)
		
		speedLabel.text = speed
		distanceLargeLabel.text = distanceLarge
		distanceSmallLabel.text = distanceSmall
		
		delegate?.updateSettings()
		
	}
	
	@IBAction func doneButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
	
	@IBAction func onUnitModeChanged(_ sender: UISegmentedControl) {
		if(sender.selectedSegmentIndex == 0){
			speed = Resources.SETTINGS_UNIT_SPEED_KN
			distanceLarge = Resources.SETTINGS_UNIT_DISTANCE_LARGE_NMILE
			distanceSmall = Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER
			unitMode = .nautical
		} else if(sender.selectedSegmentIndex == 1){
			speed = Resources.SETTINGS_UNIT_SPEED_KMH
			distanceLarge = Resources.SETTINGS_UNIT_DISTANCE_LARGE_KM
			distanceSmall = Resources.SETTINGS_UNIT_DISTANCE_SMALL_METER
			unitMode = .metric
		} else if(sender.selectedSegmentIndex == 2){
			speed = Resources.SETTINGS_UNIT_SPEED_MPH
			distanceLarge = Resources.SETTINGS_UNIT_DISTANCE_LARGE_MILE
			distanceSmall = Resources.SETTINGS_UNIT_DISTANCE_SMALL_YARD
			unitMode = .US
		}
		setUnits()
	}
	
	@IBAction func backButton(_ sender: Any) {
		self.dismiss(animated: false, completion: nil)
		performSegue(withIdentifier: "unwindSegueToSettings", sender: self)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		let touch : UITouch? = touches.first
		if (touch?.view == backgroundView){
			performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
			self.dismiss(animated: false, completion: nil)
		}
	}
}
