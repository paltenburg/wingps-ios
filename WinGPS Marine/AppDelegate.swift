//
//  AppDelegate.swift
//  MapDemo
//
//  Created by Standaard on 05/05/17.
//  Copyright © 2017 Stentec. All rights reserved.
//

import UIKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
    var window: UIWindow?
    
    var backgroundSessionCompletionHandler: (() -> Void)?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        debug.doutAppStartTime = StUtils.systemUptimeMillis()
        
        if Resources.appType == .dkw1800 {
            //            AppStorePurchases().makeSureThereIsAReceipt()
            //            AppStorePurchases().requestNewReceipt()
        }
        
//        if Resources.appType == .lite ||
//            Resources.appType == .standard
//        {
//            AppStorePurchases().makeSureThereIsAReceipt()
//
//            AppStorePurchases.getValidatedReceipt()
//        }
        
        
        /////////// Or move to viewcontroller?
//        // start loading waterway network
//        if Resources.useWWNetwork {
//            dout("AppDelegate: start loading network")
//            var allowedWaterbodyMask :Int = 0
//            let actDB = StActManager.getActManager();
//            if actDB.getProductCodeCount() > 0 {
//
//                var CP = StActManager.ChartProduct()
//                for i in 0 ..< actDB.getProductCodeCount() {
//                    actDB.getActCode(aIndex: i, aCP: CP)
//                    allowedWaterbodyMask = allowedWaterbodyMask | WWDefs.chartToWaterbody(CP.Pid, CP.Mod)
//                }
//
//                if (allowedWaterbodyMask > 0) {
//
//                    //SharedPreferences Prefs = getSharedPreferences("Stentec.DKW1800.Settings", MODE_PRIVATE);
//                    //                let LoadNetwork :Bool = ViewController.instance?.mLoadNetwork ?? false
//                    let loadNetwork = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)?.object(forKey: Resources.SETTINGS_LOAD_WWNETWORK) as? Bool ?? true
//
//
//                    //                ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
//                    //                int MemClass = am.getLargeMemoryClass();
//                    //                if (MemClass < 300) {
//                    //
//                    //                    if (LoadNetwork) {
//                    //                        boolean NetworkLoadedOk = StSettingsInterface.settings().getLoadWWNetworkOk();// Prefs.getBoolean(StSettingsInterface.LOAD_WWNETWORK_OK, false);
//                    //                        if (!NetworkLoadedOk) {
//                    //                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_wwnetwork_memory), Toast.LENGTH_LONG).show();
//                    //                            LoadNetwork = false;
//                    //                            StSettingsInterface.settings().setLoadNetwork(LoadNetwork);// Prefs.edit().putBoolean(StSettingsInterface.LOAD_WWNETWORK, false).commit();
//                    //                        }
//                    //                    }
//                    //                }
//
//                    //                StSettingsInterface.settings().setLoadWWNetworkOk(false);
//
//                    if loadNetwork {
//                        dout("AppDelegate: WWNetwork.getNetwork")
//
//                        let WWFilename :String = StActManager.getActManager().getWWFilePath()
//                        WWNetwork.getNetwork(aFilename: WWFilename, mainHandler: nil)
//
//                        dout("AppDelegate: done loading network, generate overlays")
//
//                        NetworkOverlay.instance.generateNetworkOverlays()
//                        dout("AppDelegate: done generating")
//
//                    }
//                }
//            }
//        }
        
        
        return true
    }
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	
		ViewController.instance?.didEnterBackground()
		
		TrackingService.service.saveRunningTrack()
		
		Routes.service.saveRoutes()
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
		ViewController.instance?.willEnterForeground()
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

		TrackingService.service.saveRunningTrack()
		
		Routes.service.saveRoutes()
        
        ViewController.instance?.willTerminate()
	}
	
	func application(_ application: UIApplication, handleEventsForBackgroundURLSession
		identifier: String, completionHandler: @escaping () -> Void) {
		backgroundSessionCompletionHandler = completionHandler
	}
}

