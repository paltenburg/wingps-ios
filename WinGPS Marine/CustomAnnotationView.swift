//
//  CustomAnnotationView.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 13/05/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class CustomAnnotationView : MKAnnotationView {
	
	var textLabel :UILabel?
	var imageView :UIImageView?
	
    var textLabelIsHidden :Bool = true
    
//	let minimalTappingSize = CGSize(width: 100, height: 100)
//
//	override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//		let hitView = super.hitTest(point, with: event)
////		let hitView = self.subviews[0]
//		if (hitView != nil)
//		{
//			self.superview?.bringSubviewToFront(self)
//		}
//		return hitView
//	}
//
//	override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
////		var rect = self.bounds
//
//		var rect = CGRect(origin: CGPoint(x: -50, y: -50), size: CGSize(width: 100, height: 100))
//
////		// ensure minimal tapping size, if width and height are set at all
////		if self.subviews.count >= 2 {
////			rect = self.subviews[0].bounds
////			let diff = CGSize(width: minimalTappingSize.width - rect.size.width, height: minimalTappingSize.height - rect.size.height)
////			if diff.width > 0 {
////				rect = rect.insetBy(dx: -(diff.width / 2), dy: 0)
////			}
////			if diff.height > 0 {
////				rect = rect.insetBy(dx: 0, dy: -(diff.height / 2))
////			}
////		}
//
//		var isInside: Bool = rect.contains(point)
//		if(!isInside) {
//			for view in self.subviews {
//				isInside = view.frame.contains(point)
//				if isInside {
//					break
//				}
//			}
//		}
//		return isInside
//	}
	
}
