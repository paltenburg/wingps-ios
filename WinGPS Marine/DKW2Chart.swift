//
//  StDKW2Chart.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2Chart {
	
	enum EDKW2Codec {
		case DKW2CodecBlock
		case DKW2CodecJPG
		case DKW2CodecPNG
	}
	
	enum EDKW2Version {
		case DKW2VERUNKNOWN
		case DKW2VER1_0_0_0
		case DKW2VER1_0_0_1
		case DKW2VER1_0_0_2
		case DKW2VER2_0_0_0
		case DKW2VER2_1_0_0
		case DKW2VER2_1_0_1
	}
	
	var orgTileOffset: Int64 = 0
	var orgTileSize: Int64 = 0
	var orgTileTableOffset: Int64 = 0
	var orgTileTableSize: Int64 = 0
	var orgTileCount: Int64 = 0
	var updMipTileOffset: Int64 = 0
	var updMipTileSize: Int64 = 0
	var updTileTableOffset: Int64 = 0
	var updTileTableSize: Int64 = 0
	var updTileCount: Int64 = 0
	var mipTileTableOffset: Int64 = 0
	var mipTileTableSize: Int64 = 0
	var mipTileCount: Int64 = 0
	var updateTagOffset: Int64 = 0
	var updateTagSize: Int64 = 0
	var boundsTagOffset: Int64 = 0
	var boundsTagSize: Int64 = 0
	var tagOffset: Int64 = 0
	var tagSize: Int64 = 0
	var width: Int = 0
	var height: Int = 0
	var mipmapCount: Int = 0
	var codec :EDKW2Codec?
	
	let mipmapTable = DKW2MipmapTable()
	let updateTag = DKW2UpdateTag()
	let boundsTag = DKW2BoundsTag()
	var fileName :URL?
	var fileHandle :DKWFileHandle?
	
	
	//    // other fields
	//    private Context            mContext;             // Context of app
	var fileSize :Int64 = 0         // File size in bytes.
	//    private long               mFileStartPos;        // Start position of file (for assets this is != 0)
	//    var fileTime :Int64            // Last write-time of the file.
	var fileTime :Date?            // Last write-time of the file.
	//    //    SEvent<FDKW2ChartChanged>   mOnChanged;       // Event that is called when the chart file was changed by Update() or Write...().
	let tag = DKW2Tag()
	var tileTableC = DKW2TileTableC()
	var validated :Bool?           // True if the chart info was validated between loading and saving.
	//    private StVersion          mVersion;             // DKW2 format version.
	var versionDKW2 :EDKW2Version?         // DKW2 format version.
	
	init(fileName: URL) {
		self.fileName = fileName
		loadFromDKW2File(from: fileName)
		
	}
	
	func headerInit() {
		orgTileOffset = 0
		orgTileSize = 0
		orgTileTableOffset = 0
		orgTileTableSize = 0
		orgTileCount = 0
		updMipTileOffset = 0
		updMipTileSize = 0
		updTileTableOffset = 0
		updTileTableSize = 0
		updTileCount = 0
		mipTileTableOffset = 0
		mipTileTableSize =  0
		mipTileCount = 0
		updateTagOffset = 0
		updateTagSize = 0
		boundsTagOffset = 0
		boundsTagSize = 0
		tagOffset = 0
		tagSize = 0
		width = 0
		height = 0
		mipmapCount = 0
		codec = EDKW2Codec.DKW2CodecBlock
	}
	
	
	func loadFromDKW2File(from fileName: URL) {
		
		headerInit()
		self.fileName = fileName
		
		do {
			try self.fileHandle = DKWFileHandle(FileHandle(forReadingFrom: fileName))
		} catch {
			//			errorCode = .ER_FILE_ERROR
			return
		}
	

		//        aWarnings = 0;
		//        aError.setErrorCode(StError.ER_OK);
		
		// Reset all fields before initializing them. This way, the class returns
		// valid data if an error occurs during reading.
		boundsTag.clear()
		//        file = aFile
		fileSize = 0
		fileTime = nil
		
		validated = false
		versionDKW2 = EDKW2Version.DKW2VERUNKNOWN
		
		// Read the file
		print("loadFromDKW2File:", fileName.lastPathComponent)

		//        try {
		fileSize = Int64(getFileSize(fileName.path)!)
		fileTime = getLastModified(fileName.path)
		//        DKW2FileRead(aWarnings, aError)
		try! DKW2FileRead() // TODO: Debug pass on error throwing
		//        } catch (IOException e) {
		//        aError.setErrorCode(StError.ER_FILEFORMAT)
		//        }
		//        if (aError.getErrorCode() != StError.ER_OK)
		//        return
		
		// Ensure that the bounds tag is filled.
		if (boundsTag.getCount() == 0) {
			boundsTag.add(Double2(x: -0.5, y: -0.5))
			boundsTag.add(Double2(x: Double(width) - 0.5, y: -0.5))
			boundsTag.add(Double2(x: Double(width) - 0.5, y: Double(height) - 0.5))
			boundsTag.add(Double2(x: -0.5, y: Double(height) - 0.5))
		}
		
	}
	
	func DKW2FileRead() throws {
		
		var errorCode = ErrorCode.ER_OK
		
		guard let fileHandle = self.fileHandle else {
			return
		}
		
		// Read header
		var Buffer = fileHandle.handle.readData(ofLength: 8) as NSData
		//let Header = String(data: Buffer, encoding: NSASCIIStringEncoding)
		
		// todo: check header and version
		
		// Read version
		Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
		
		// Read dkw2 header
		Buffer = fileHandle.handle.readData(ofLength: 164) as NSData
		
		orgTileOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 0)
		orgTileSize = try getInt64FromNSDataBuffer(from: Buffer, start: 8)
		orgTileTableOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 16)
		orgTileTableSize = try getInt64FromNSDataBuffer(from: Buffer, start: 24)
		orgTileCount = try getInt64FromNSDataBuffer(from: Buffer, start: 32)
		updMipTileOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 40)
		updMipTileSize = try getInt64FromNSDataBuffer(from: Buffer, start: 48)
		updTileTableOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 56)
		updTileTableSize = try getInt64FromNSDataBuffer(from: Buffer, start: 64)
		updTileCount = try getInt64FromNSDataBuffer(from: Buffer, start: 72)
		mipTileTableOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 80)
		mipTileTableSize = try getInt64FromNSDataBuffer(from: Buffer, start: 88)
		mipTileCount = try getInt64FromNSDataBuffer(from: Buffer, start: 96)
		updateTagOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 104)
		updateTagSize = try getInt64FromNSDataBuffer(from: Buffer, start: 112)
		boundsTagOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 120)
		boundsTagSize = try getInt64FromNSDataBuffer(from: Buffer, start: 128)
		tagOffset = try getInt64FromNSDataBuffer(from: Buffer, start: 136)
		tagSize = try getInt64FromNSDataBuffer(from: Buffer, start: 144)
		width = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 152))
		height = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 156))
		mipmapCount = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 160))
		
		let TileCount = mipmapTable.initialize(count: Int(mipmapCount), width0: Int(width), height0: Int(height))
		
		let TileTable = DKW2TileTable()
		TileTable.initialize(count: TileCount, mipmapTable: mipmapTable)
		
		fileHandle.handle.seek(toFileOffset: UInt64(orgTileTableOffset))
		try readOrgTileTable(fileHandle: fileHandle.handle, tileTable: TileTable)
		
		fileHandle.handle.seek(toFileOffset: UInt64(updTileTableOffset))
		try readUpdTileTable(fileHandle: fileHandle.handle, tileTable: TileTable)
		
		fileHandle.handle.seek(toFileOffset: UInt64(mipTileTableOffset))
		try readMipTileTable(fileHandle: fileHandle.handle, tileTable: TileTable)
		
		fileHandle.handle.seek(toFileOffset: UInt64(updateTagOffset))
		try readUpdateTag(fileHandle: fileHandle.handle, updateTag: updateTag)
		
		// Read bounds tag
		fileHandle.handle.seek(toFileOffset: UInt64(boundsTagOffset))
		try readBoundsTag(from: fileHandle.handle, into: boundsTag)
		
		// Read tag
		fileHandle.handle.seek(toFileOffset: UInt64(tagOffset))
		try readDKW2Tag(from: fileHandle.handle, into: tag, errorCode: &errorCode)
		
		if errorCode != .ER_OK {
			return
		}
		
		// Initialize compressed tile table from the tile table
		tileTableC = DKW2TileTableC(with: TileTable)
		
		// catch file not found
		// catch other exceptions
		
		
	}
	
	func readMipTileTable(fileHandle: FileHandle, tileTable: DKW2TileTable) throws {
		let Buffer = fileHandle.readData(ofLength: Int(mipTileCount) * 16) as NSData
		
		var TileTableMip = [DKW2FileTileTableEntry]()
		
		var BufferOffset = 0
		for _ in 0..<mipTileCount {
			var Entry: DKW2FileTileTableEntry = DKW2FileTileTableEntry()
			Entry.offset = try getInt64FromNSDataBuffer(from:  Buffer, start: BufferOffset)
			BufferOffset += 8
			Entry.sizeRGB = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			Entry.sizeAlpha = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			TileTableMip.append(Entry)
		}
		
		for i in 0..<mipTileCount {
			tileTable.initTileOrg(index: Int(orgTileCount + i), offset: TileTableMip[Int(i)].offset, sizeRGB: TileTableMip[Int(i)].sizeRGB, sizeAlpha: TileTableMip[Int(i)].sizeAlpha)
			
			tileTable.initTileUpd(index: Int(orgTileCount + i), offset: TileTableMip[Int(i)].offset, sizeRGB: TileTableMip[Int(i)].sizeRGB, sizeAlpha: TileTableMip[Int(i)].sizeAlpha)
		}
	}
	
	func readOrgTileTable(fileHandle: FileHandle, tileTable: DKW2TileTable) throws {
		let Buffer = fileHandle.readData(ofLength: Int(orgTileCount) * 16) as NSData
		
		var TileTableOrg = [DKW2FileTileTableEntry]()
		
		var BufferOffset = 0
		for _ in 0..<orgTileCount {
			var Entry: DKW2FileTileTableEntry = DKW2FileTileTableEntry()
			Entry.offset = try getInt64FromNSDataBuffer(from: Buffer, start: BufferOffset)
			BufferOffset += 8
			Entry.sizeRGB = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			Entry.sizeAlpha = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			TileTableOrg.append(Entry)
		}
		
		for i in 0..<orgTileCount {
			tileTable.initTileOrg(index: Int(i), offset: TileTableOrg[Int(i)].offset, sizeRGB: TileTableOrg[Int(i)].sizeRGB, sizeAlpha: TileTableOrg[Int(i)].sizeAlpha)
			tileTable.initTileUpd(index: Int(i), offset: TileTableOrg[Int(i)].offset, sizeRGB: TileTableOrg[Int(i)].sizeRGB, sizeAlpha: TileTableOrg[Int(i)].sizeAlpha)
		}
	}
	
	func readTag(fileHandle: FileHandle, tagOffset: Int64, tagSize: Int, dstBuffer: NSMutableData) throws {
		
		fileHandle.seek(toFileOffset: UInt64(tagOffset))
		
		let TagSize = tagSize - 4
		let Buffer = fileHandle.readData(ofLength: TagSize)
		dstBuffer.setData(Buffer)
		let ChecksumBuffer = fileHandle.readData(ofLength: 4) as NSData
		let ChecksumFromFile: UInt32 = try getUInt32FromNSDataBuffer(buffer: ChecksumBuffer, start: 0)
		let Checksum: UInt32 = try checksum32(buffer: dstBuffer, count: TagSize)
		
		// Error handling
		if ChecksumFromFile != Checksum {
			return
		}
		
		// Decode tag
		var Mask : UInt8 = 0xA6
		for i in 0 ..< TagSize {
			
			var P: UInt8 = try getUByteFromNSDataBuffer(buffer: dstBuffer, start: i)
			P ^= Mask
//			Mask = UInt8.addWithOverflow(Mask, 1).0
			Mask = Mask &+ 1
			dstBuffer.replaceBytes(in: NSRange(location: i, length: 1), withBytes: &P, length: 1)
			
		}
	}
	
	func readUpdateTag(fileHandle: FileHandle, updateTag: DKW2UpdateTag) throws {
		let buffer = NSMutableData(length: Int(updateTagSize))
		if let TagBuffer = buffer {
			try readTag(fileHandle: fileHandle, tagOffset: updateTagOffset, tagSize: Int(updateTagSize), dstBuffer: TagBuffer)
			
			var position = 0
			let s :String = try readStrA(buffer: TagBuffer, at: &position)
			
			// fill updatetag
			updateTag.infoText = s

			updateTag.timeUTCLastApplied = try readDateTime(from: TagBuffer, at: &position)
			
			updateTag.timeUTCLastChecked = try readDateTime(from: TagBuffer, at: &position)
			
			updateTag.timeUTCUpdate = try readDateTime(from: TagBuffer, at: &position)
		}
	}
	
	func readBoundsTag(from fileHandle: FileHandle, into boundsTag: DKW2BoundsTag) throws {
		let buffer = NSMutableData(length: Int(boundsTagSize))
		if let tagBuffer = buffer {
			try readTag(fileHandle: fileHandle, tagOffset: boundsTagOffset, tagSize: Int(boundsTagSize), dstBuffer: tagBuffer)
			
			// Read point count
			var position = 0
			let count = try getInt32FromNSDataBuffer(buffer: tagBuffer, start: position)
			position += 4
			
			if count < 0 || count == 1 || count == 2 {
				return
			}
			
			var warningAdded: Bool = false
			var p1 :[Double] = [-1e12, -1e12]
			for _ in 0 ..< count {
				let x = try getInt32FromNSDataBuffer(buffer: tagBuffer, start: position)
				position += 4
				let y = try getInt32FromNSDataBuffer(buffer: tagBuffer, start: position)
				position += 4
				
				let p2 :[Double] = [Double(x) - 0.5, Double(y) - 0.5]
				if p1 == p2 && !warningAdded {
					warningAdded = true
				}
				boundsTag.add(Double2(p2))
				p1 = p2
			}
		}
	}
	
	func readDateTime(from buffer :NSData, at position :inout Int) throws -> Date{
		
		let year = try getShortFromNSDataBuffer(buffer: buffer, start: position)
		position += 2
		let month = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
		position += 1
		let day = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
		position += 1
		let hour = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
		position += 1
		let min = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
		position += 1
		let sec = try getUByteFromNSDataBuffer(buffer: buffer, start: position)
		position += 1
		let mSec = try getShortFromNSDataBuffer(buffer: buffer, start: position)
		position += 2
		
		var dateComponents = DateComponents()
		dateComponents.timeZone = TimeZone(abbreviation: "GMT")
		dateComponents.year = Int(year)
		dateComponents.month = Int(month)
		dateComponents.day = Int(day)
		dateComponents.hour = Int(hour)
		dateComponents.minute = Int(min)
		dateComponents.second = Int(sec)
		dateComponents.nanosecond = Int(mSec) * 1000000
		
		// Create date from components
		let userCalendar = Calendar.current // user calendar
		let date = userCalendar.date(from: dateComponents)!
		return date
		
	}
	
	
	func readDate(from buffer :NSData, at position :inout Int) throws -> Date{
		
		let year = try getShortFromNSDataBuffer(buffer: buffer, at: &position)
		let month = try getUByteFromNSDataBuffer(buffer: buffer, at: &position)
		let day = try getUByteFromNSDataBuffer(buffer: buffer, at: &position)
		
		if year == 1899 {
			return Date()
		}
		
		var dateComponents = DateComponents()
		dateComponents.timeZone = TimeZone(abbreviation: "GMT")
		dateComponents.year = Int(year)
		dateComponents.month = Int(month)
		dateComponents.day = Int(day)
		
		// Create date from components
		let userCalendar = Calendar.current // user calendar
		let date = userCalendar.date(from: dateComponents)!
		return date
		
	}
	
	func readDKW2Tag(from fileHandle: FileHandle, into tag: DKW2Tag, errorCode :inout ErrorCode) throws {
		
		errorCode = .ER_OK
		
		let buffer = NSMutableData(length: Int(tagSize))
		if let tagBuffer = buffer {
			try readTag(fileHandle: fileHandle, tagOffset: tagOffset, tagSize: Int(tagSize), dstBuffer: tagBuffer)
			
			var pos = 0
			tag.guid = try readGuid(from: tagBuffer, at: &pos)
			
			let productID = try getUInt32FromNSDataBuffer(buffer: tagBuffer, at: &pos)
			
			if productID > 0xfffff {
				errorCode = .ER_FILEFORMAT
				return
			}
			tag.productID = productID
			
			let moduleBitIndex = try getInt32FromNSDataBuffer(from: tagBuffer, at: &pos)
			if moduleBitIndex < -1 || Int(moduleBitIndex) >= DKW2_MAX_LINCENSE_MODULE_COUNT {
				errorCode = .ER_FILEFORMAT
				return
			}
			tag.moduleBitIndex = moduleBitIndex
			
			tag.productStr = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.productName = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.moduleName = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.moduleNameShort = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.chartName = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.chartNumber = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.editionYear = try getShortFromNSDataBuffer(buffer: tagBuffer, at: &pos)
			
			tag.revisionDate = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.expDate = try readDate(from: tagBuffer, at: &pos)
			
			tag.digitalPublisher = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.license = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.licenseDisclaimer = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.sourceChartName = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.sourcePublisher = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.sourceCopyright = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.scale = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.unitsDepth = try readStrA(buffer: tagBuffer, at: &pos)
			
			tag.unitsHeight = try readStrA(buffer: tagBuffer, at: &pos)
			
			let boolVal = try getUByteFromNSDataBuffer(buffer: tagBuffer, at: &pos) != 0
			
			if boolVal {
				let magVar = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				let magVarMeasureDate = try readDate(from: tagBuffer, at: &pos)
				let magVarDrift = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				tag.setMagVar(magVarDefinedP: true, magVarP: magVar, magVarDriftP: magVarDrift, magVarMeasureDateP: magVarMeasureDate)
				
			} else {
				tag.setMagVar(magVarDefinedP: false, magVarP: 0, magVarDriftP: 0, magVarMeasureDateP: nil)
			}
			
			tag.legendFileName = try readStrA(buffer: tagBuffer, at: &pos)
			
			// read links
			
			let count = try getInt32FromNSDataBuffer(from: tagBuffer, at: &pos)
			for _ in 0 ..< count {
				var linkType :ELinkType? = nil
				let b = try getUByteFromNSDataBuffer(buffer: tagBuffer, at: &pos)
				switch b {
				case 0: linkType = ELinkType.FILE
				case 1: linkType = ELinkType.URL
				default:
					break
				}
				if (linkType != nil) {
					tag.addLink(of: linkType!, link: try readStrA(buffer: tagBuffer, at: &pos), description: try readStrA(buffer: tagBuffer, at: &pos))
					
				}
			}
			
			// calibration information
			
			let calPointCount = try getUByteFromNSDataBuffer(buffer: tagBuffer, at: &pos)
			if calPointCount < 2 || calPointCount > 3 {
				errorCode = .ER_GEOCAL_INVALID_POINT_COUNT
				return
			}
			
			let pixX = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let pixY = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let pix1 :[Double] = [pixX, pixY]
			
			let pixX2 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let pixY2 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let pix2 :[Double] = [pixX2, pixY2]
			
			var pix3 = [Double]()
			if calPointCount == 3 {
				let pixX3 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				let pixY3 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				pix3 = [pixX3, pixY3]
			} else {
				pix3 = [0, 0]
			}
			
			let projX = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let projY = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let proj1 = [projX, projY]
			
			let projX2 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let projY2 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
			let proj2 = [projX2, projY2]
			
			var proj3 = [Double]()
			if calPointCount == 3   {
				let projX3 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				let projY3 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				proj3 = [projX3, projY3]
			} else {
				proj3 = [0, 0]
			}
			
			tag.setCalibration(pointCount: Int(calPointCount), pix1: pix1, pix2: pix2, pix3: pix3, proj1: proj1, proj2: proj2, proj3: proj3)
			
			// Projection information
			
			let ref = GeodeticRef()
			let projection = try getUByteFromNSDataBuffer(buffer: tagBuffer, at: &pos)
			
			switch projection {
			case 0:
				ref.projection = GeodeticProjection.MERCATOR
				ref.long0 = 0
			case 2:
				ref.projection = GeodeticProjection.TRANSVERSEMERCATOR
				ref.k0 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				ref.long0 = try getDoubleFromNSDataBuffer(from: tagBuffer, at: &pos)
				ref.M0 = 0
			default:
				errorCode = .ER_GEO_UNKNOWN_PROJECTION
				return
			}
			tag.ref = ref
			
		}
	}
	
	
	func readUpdTileTable(fileHandle: FileHandle, tileTable: DKW2TileTable) throws {
		let Buffer = fileHandle.readData(ofLength: Int(updTileCount) * 20) as NSData
		
		var TileTableUpd = [DKW2FileTileTableUpdEntry]()
		
		var BufferOffset = 0
		for _ in 0 ..< updTileCount {
			var Entry: DKW2FileTileTableUpdEntry = DKW2FileTileTableUpdEntry()
			Entry.tileIndex = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			Entry.offset = try getInt64FromNSDataBuffer(from: Buffer, start: BufferOffset)
			BufferOffset += 8
			Entry.sizeRGB = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			Entry.sizeAlpha = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: BufferOffset))
			BufferOffset += 4
			TileTableUpd.append(Entry)
		}
		
		for i in 0..<updTileCount {
			if (Int64(TileTableUpd[Int(i)].tileIndex) >= orgTileCount) {
				return
			}
			tileTable.initTileUpd(index: TileTableUpd[Int(i)].tileIndex, offset: TileTableUpd[Int(i)].offset, sizeRGB: TileTableUpd[Int(i)].sizeRGB, sizeAlpha: TileTableUpd[Int(i)].sizeAlpha)
		}
	}
	
	class DKW2FileTileTableEntry {
		var offset: Int64 = 0
		var sizeRGB: Int = 0
		var sizeAlpha: Int = 0
	}
	
	class DKW2FileTileTableUpdEntry {
		var tileIndex: Int = 0
		var offset: Int64 = 0
		var sizeRGB: Int = 0
		var sizeAlpha: Int = 0
	}
	
}
