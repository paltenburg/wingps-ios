//
//  Tracks.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 22/10/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import CoreGPX

// Static class to handle routes. Equivalent to the route-functions in Android of StDBManager

class Tracks :NSObject{
    
    // constants:
    static let switchOverThresholdDistance :Double = 20 // in meters
    
    static let service = Tracks()
    
    var tracks = [Track]()
    
    var overlay :TrackOverlay?
        
    private override init(){
        super.init()
        dout("Tracks init")
             
        var idx :Int = 0
        while true {
//            let fileURL = Tracks.getTrackFileUrl(idx: idx)
//            if let gpxRoot = GPXParser(withURL: fileURL)?.parsedData(),
//                let gpxTrack = gpxRoot.tracks.first {
//
//                var track = Track()
//                track.name = gpxTrack.name ?? ""
//
//                for segment in gpxTrack.tracksegments {
//                    for gpxPoint in segment.trackpoints {
//                        track.add(location: CLLocationCoordinate2D(latitude: gpxPoint.latitude!, longitude: gpxPoint.longitude!), name: gpxPoint.name ?? "")
//                    }
//                }
///                track.needsSaving = false
                
            let fileURL = Tracks.getTrackFileUrlByIndex(idx)
            
            if let track = Track(fromFile: fileURL) {
            
                dout("load track \(idx)")
                overlay?.setOverlays(for: track)
                
                tracks.append(track)
                
                idx += 1
            } else {
                break
            }
        }
        
        // update notations
    }
    
//    // when user creates new route and first route point
//    func createNewRoute(location: CLLocationCoordinate2D) -> Route {
//        // check if it is allowed to create a new route at this point
//
//        // find network section
//
//        // init route
//        let route = Route()
//        route.name = "Route " + String(routes.count + 1)
//
//        route.add(location: location, name: "") // section
//        addRoute(route)
//        route.saveRoute()
//
//        return route
//    }
//
//    func getEditRoute() -> Route? {
//        for route in routes {
//            if route.getEdit() {
//                return route
//            }
//        }
//        return nil
//    }
//
//    func stopRouteEdit(){
//        if let route = getEditRoute() {
//            route.setEdit(false)
//            if route.getRoutePointCount() == 0 {
//                deleteRte(route.GUID)
//            } else {
//                route.saveRoute()
//            }
//        }
//    }
//
//    func deleteRte(_ guid: UUID) {
//        if let route = findRouteByGUID(guid) {
//            for i in 0 ..< routes.count {
//                if routes[i] === route {
//                    routes.remove(at: i)
//                }
//            }
//        }
//    }
//
//    func findRouteByGUID(_ guid :UUID) -> Route? {
//        for route in routes {
//            if route.GUID == guid {
//                return route
//            }
//        }
//        return nil
//    }

    func addTrack(_ track :Track){
        tracks.append(track)

        // add drawable elements to main track overlay
        if let overlay = self.overlay {
            overlay.setOverlays(for: track)
        }
    }

//    func getAllOverlays() -> [MKOverlay]{
//        var overlays = [MKOverlay]()
//        for route in routes{
//            overlays.append(contentsOf: route.getAllOverlays())
//        }
//        return overlays
//    }

    func getTrackIndex(_ track :Track) -> Int? {
        for (i, t) in tracks.enumerated() {
            if t === track {
                return i
            }
        }
        return nil
    }

    func deleteTrack(_ track :Track){
        if track === TrackingService.service.runningTrack {
            TrackingService.service.runningTrack = nil
        }
        
        if let i = getTrackIndex(track) {
            tracks.remove(at: i)

            // Remove file and rename others
            var url = Tracks.getTrackFileUrlByIndex(i)
            do {
                try FileManager.default.removeItem(atPath: url.path)

                if i < tracks.count - 1 {
                    for j in i + 1 ..< tracks.count {
                        let nextUrl = Tracks.getTrackFileUrlByIndex(j)
                        try FileManager.default.moveItem(at: nextUrl, to: url)
                        url = nextUrl
                    }
                }
            } catch {
                // if failed, it might have not existed yet.
            }
        }

//        if route === activeRoutePoint?.route {
//            Routes.service.stopActiveRoute()
//        }
    }

//    func startRouteFromPoint(_ routePoint :RoutePoint) {
//        // reset distances
//        if let rpts = activeRoutePoint?.route.routePoints {
//            for rp in rpts { rp.setDistance(-1) }
//        }
//
//        activeRoutePoint = routePoint
//        updateRouteNavpointLocation(routePoint)
//
//        startFollowing()
//    }
//
//    func startFollowing() {
//        locationManager.allowsBackgroundLocationUpdates = true
//        locationManager.startUpdatingLocation()
//    }
//
//    func stopFollowing() {
//        locationManager.stopUpdatingLocation()
//        locationManager.allowsBackgroundLocationUpdates = false
//        for rp in activeRoutePoint?.route.routePoints ?? [RoutePoint]() {
//            rp.setDistance(-1)
//        }
//        activeRoutePoint = nil
//
//    }
//
//    func updateRouteNavpointLocation(_ routePoint : RoutePoint){
//        overlay?.vc.setNavigationWaypoint(routePoint.position)
//
//    }
//
//    func stopActiveRoute(){
//        stopFollowing()
//        overlay?.vc.removeNavwaypoint()
//    }
//
//    func saveRoutes(){
//
//        for route in routes {
//            if route.needsSaving {
//                route.saveToGPX()
//                route.needsSaving = false
//            }
//        }
//    }

    static func getTrackFileUrlByIndex(_ idx :Int) -> URL {
        let trackFilename :String = "track-\(idx).gpx"
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let URL = documentsDirectory.appendingPathComponent(trackFilename)
        return URL
    }
}



//extension Routes: CLLocationManagerDelegate {
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//
//        /// Update route tracking
//        if let location = locations.last {
//            dout("RouteService didUpdateLocations \(location.speed) \(location.course) \(location.coordinate.longitude) \(location.coordinate.latitude)")
//        }
//
//        guard let location = locations.last,
//            location.horizontalAccuracy < 20 else {
//                return
//        }
//
//        if let activeRoutePoint = self.activeRoutePoint {
//
//            dout("distance: ", location.distance(from: CLLocation(latitude: activeRoutePoint.position.latitude, longitude: activeRoutePoint.position.longitude)))
//
//            let activeRoute = activeRoutePoint.route
//            var currentRoutePoint = activeRoutePoint
//            while true {
//                if location.distance(from: CLLocation(latitude: currentRoutePoint.position.latitude, longitude: currentRoutePoint.position.longitude)) < Routes.switchOverThresholdDistance {
//                    if let nextRP = currentRoutePoint.next{
//                        startRouteFromPoint(nextRP)
//                    } else {
//                        stopActiveRoute()
//                        // TODO: Give a notification that the route is finished?
//                    }
//                    break
//                }
//                // try routepoints further up the route
//                if let nextRP = currentRoutePoint.next {
//                    currentRoutePoint = nextRP
//                } else {
//                    break
//                }
//            }
//        }
//    }
//}
