//
//  File.swift
//  WinGPS Marine
//
//  Created by Standaard on 15/04/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import MapKit
import CoreGPX

class Track{
    
    var tailTrackURL :URL? = nil
    
    let maxLength :Double = 10000 // max track length in meters.
    //	let maxLength :Double = 9999999999 // for testing
    
    //	var locationCoordinates = [CLLocationCoordinate2D]()
    var locationsBySegment = [[CLLocation]]()
    
    var name :String = ""
    var createTime :Date
    
    //    var stateVisible :Bool = true
    //    var needsSaving :Bool = false
    
    var length :Double = 0 // length (m)
    var durationSeconds :Double = 0
    var mapView: StMapView?
    
    private var visible = false
    
    var trackAnnotations = [TrackLabelAnnotation]()
    //    var lineOverlays :[MKOverlay] = [MKOverlay]()
    private var trackSegmentPolylines = [StTrackPolyline]()
    
    
    init(){
        createTime = Date()
    }
    
    //    convenience init(mapView :StMapView){
    //        self.init()
    //        self.mapView = mapView
    //    }
    
    convenience init?(fromFile fileURL :URL){
        self.init()
        
        //		if
        //			let tailTrackURL = getTailTrackUrl(),
        //			let gpxRoot = GPXParser(withURL: tailTrackURL)?.parsedData() {
        //
        //			let gpxTrack = gpxRoot.tracks.first
        //			for gpxSegment in gpxTrack?.tracksegments ?? [GPXTrackSegment]() {
        //				var segment = [CLLocation]()
        //				for trackPoint in gpxSegment.trackpoints {
        //					var loc = CLLocation(coordinate: CLLocationCoordinate2D(latitude: trackPoint.latitude!, longitude: trackPoint.longitude!), altitude: Double.nan, horizontalAccuracy: Double.nan, verticalAccuracy: Double.nan, timestamp: trackPoint.time!)
        //
        //					if let lastLoc = segment.last {
        //						length += lastLoc.distance(from: loc)
        //					}
        //					segment.append(loc)
        //				}
        //				locationsBySegment.append(segment)
        //			}
        //		}
        //
        //		// start new segment
        //		locationsBySegment.append( [CLLocation]() )
        
        if FileManager.default.fileExists(atPath: fileURL.path),
           let gpxRoot = GPXParser(withURL: fileURL)?.parsedData() {
            let gpxTrack = gpxRoot.tracks.first
            
            name = gpxTrack?.name ?? ""
            
            for gpxSegment in gpxTrack?.tracksegments ?? [GPXTrackSegment]() {
                var segment = [CLLocation]()
                for trackPoint in gpxSegment.trackpoints {
                    var loc = CLLocation(coordinate: CLLocationCoordinate2D(latitude: trackPoint.latitude!, longitude: trackPoint.longitude!), altitude: Double.nan, horizontalAccuracy: Double.nan, verticalAccuracy: Double.nan, timestamp: trackPoint.time!)
                    
                    if let lastLoc = segment.last {
                        length += lastLoc.distance(from: loc)
                        durationSeconds += loc.timestamp.timeIntervalSince(lastLoc.timestamp)

                    }
                    segment.append(loc)
                }
                locationsBySegment.append(segment)
            }
            
            // start new segment
            locationsBySegment.append( [CLLocation]() )
            //             return track
            dout("loaded track file from: \(fileURL.absoluteString)")

            return
        }
        
        return nil
    }
    
    func setVisible(_ visible :Bool){
        self.visible = visible
        Tracks.service.overlay?.reloadTrackVisibility()
    }
    
    func getVisible() -> Bool {
        return visible
    }
    
    func addLocation(location :CLLocation){
        
        switch Resources.appType {
        case .lite, .dkw1800, .friesemeren:
            
            // In the lite versions, the running track is limited by length
            // First: trim track from the start if length too high.
            while length > maxLength,
                  var firstSegment = locationsBySegment.first,
                  firstSegment.count >= 1 {
                
                if firstSegment.count >= 2 {
                    length -= firstSegment[0].distance(from: firstSegment[1])
                    
                    durationSeconds -= firstSegment[1].timestamp.timeIntervalSince(firstSegment[0].timestamp)
                    
                }
                locationsBySegment[0].remove(at: 0)
                
                if locationsBySegment.count > 1,
                   locationsBySegment[0].count <= 1 {
                    locationsBySegment.remove(at: 0)
                }
            }
            
        case .plus:
            
            dout("Don't remove tail")
            
        }
 
        
        // make sure there is at least one segment there
        if locationsBySegment.count == 0 {
            locationsBySegment.append( [CLLocation]() )
        }
        
        if var lastSegment = locationsBySegment.last {
            
            if let lastLoc = lastSegment.last {
                length += lastLoc.distance(from: location)
                durationSeconds += location.timestamp.timeIntervalSince(lastLoc.timestamp)
            }
            
            locationsBySegment[locationsBySegment.count - 1].append(location)
        }
    }
    
    func addLocations(locations :[CLLocation]){
        for l in locations {
            addLocation(location: l)
        }
    }
    
    func endCurrentSegment(){
        locationsBySegment.append([CLLocation]())
    }
    
    //	func createPolyline(){
    //
    //		mTrackLine = StTrackPolyline(coordinates: locations.map{$0.coordinate}, count: locations.count)
    //
    //	}
    
    //	func getNewTrackPolyline() -> MKPolyline{
    ////		if mTrackLine == nil {
    ////			createPolyline()
    ////		}
    ////		return mTrackLine!
    //
    //		return StTrackPolyline(coordinates: locations.map{$0.coordinate}, count: locations.count)
    //	}
    
    //	func drawLine(){
    //
    //		if let trackLine = mTrackLine {
    //			//
    //			//			if(mNavWaypointLine != nil){
    //			//				mapView.removeOverlay(mNavWaypointLine!)
    //			//			}
    //
    //
    //			mapView?.addOverlay(trackLine)
    //			//mapView.addOverlay(mNavWaypointLine!, level: MKOverlayLevel.aboveRoads)
    //
    //			mapView?.setNeedsDisplay()
    //
    //		}
    //	}
    
    //    // deprecated
    //	func addOverLaysTo(_ mapView :StMapView){
    //
    //		// clear segment array
    //		trackSegmentPolylines = [StTrackPolyline]()
    //
    //		for (i, segment) in locationsBySegment.enumerated() {
    //
    //			trackSegmentPolylines.append(StTrackPolyline(coordinates: segment.map{$0.coordinate}, count: segment.count))
    //
    //			// if current segment is not the last, draw dotted line between this one and next
    //			if i < locationsBySegment.count - 1,
    //				let firstLocOfNextSegment = locationsBySegment[i + 1].first?.coordinate,
    //				let lastLocCoordinate = segment.last?.coordinate
    //			{
    //				let dottedLine = StTrackPolyline(coordinates: [lastLocCoordinate, firstLocOfNextSegment], count: 2)
    //				dottedLine.isDashed = true
    //
    //				trackSegmentPolylines.append(dottedLine)
    //			}
    //		}
    //
    //		mapView.addOverlays(trackSegmentPolylines)
    //	}
    //
    //    // deprecated
    //	func removeOverlaysFrom(_ mapView :StMapView){
    //		mapView.removeOverlays(trackSegmentPolylines)
    //	}
    
    func generateOverlays() -> [MKOverlay] {
        // return MKOverlays for this route
        
        //        lineOverlays.removeAll()
        
        // generate MKOverlays
        //        for (i, routePoint) in routePoints.enumerated() {
        //
        //            var overlaysForThisPoint = [MKOverlay]()
        //
        //            let circle = StRoutePointCircle(center: routePoint.position, radius: 1)
        //            overlaysForThisPoint.append(circle)
        //
        //            routePointOverlays.append(overlaysForThisPoint)
        //
        //            if i < routePoints.count - 1 {
        //
        //                let line = StRoutePolyline( coordinates: [routePoints[i].position, routePoints[i+1].position], count: 2)
        //                lineOverlays.append(line)
        //
        //            }
        //        }
        
        //        mTrackSegmentPolylines = [StTrackPolyline]()
        trackSegmentPolylines.removeAll()
        
        for (i, segment) in locationsBySegment.enumerated() {
            
            trackSegmentPolylines.append(StTrackPolyline(coordinates: segment.map{$0.coordinate}, count: segment.count))
            
            // if current segment is not the last, draw dotted line between this one and next
            if i < locationsBySegment.count - 1,
               let firstLocOfNextSegment = locationsBySegment[i + 1].first?.coordinate,
               let lastLocCoordinate = segment.last?.coordinate
            {
                let dottedLine = StTrackPolyline(coordinates: [lastLocCoordinate, firstLocOfNextSegment], count: 2)
                dottedLine.isDashed = true
                
                trackSegmentPolylines.append(dottedLine)
            }
        }
        
        // TODO: Generate other annotations like trackAnnotations
        trackAnnotations.removeAll()
        // Add rest
        
        return getAllOverlays()
    }
    
    func updateAnnotations() -> [TrackLabelAnnotation] {
        
        var newAnnotations = [TrackLabelAnnotation]()
        
        //        for (i, routePoint) in routePoints.enumerated() {
        //
        //            if i == routePointAnnotations.count {
        //                let newAnnotation = RoutePointLabelAnnotation(routePoint)
        //                routePointAnnotations.append(newAnnotation)
        //                newAnnotations.append(newAnnotation)
        //            } else {
        //                routePointAnnotations[i].coordinate = routePoint.position
        //            }
        //
        //            if i == 0 {
        //                routePointAnnotations[i].title = routePoint.route.name + "\n1"
        //            } else {
        //                routePointAnnotations[i].title = "\(String(i + 1))"
        //            }
        //        }
        
        //TODO: Implement
        
        return newAnnotations
    }
    
    func getAllOverlays() -> [MKOverlay] {
        //        var allOverlays = lineOverlays
        var allOverlays :[MKOverlay] = trackSegmentPolylines
        //        allOverlays.append(contentsOf: trackAnnotations)
        
        for overlays in trackAnnotations {
            //            allOverlays.append(contentsOf: overlayss) // for fix: check routes
        }
        
        //        for overlays in routePointOverlays {
        //            allOverlays.append(contentsOf: overlays)
        //        }
        return allOverlays
    }
    
    //    func updateOverlays(forRoutePoint routePoint :RoutePoint, inRoute route :Route) -> [MKOverlay] {
    //        var overlaysToReturn = [MKOverlay]()
    //
    //        for (i, rp) in routePoints.enumerated() {
    //            if routePoint === rp {
    //                var rpOverlays = [MKOverlay]()
    //                // rebuild overlays for this routepoint
    //                rpOverlays.append(StRoutePointCircle(center: routePoint.position, radius: 0.1))
    //
    //                routePointOverlays[i] = rpOverlays
    //
    //                overlaysToReturn.append(contentsOf: rpOverlays)
    //
    //                // recreate line overlays
    //                if i >= 1 {
    //                    let lineOverlay = StRoutePolyline( coordinates: [routePoints[i - 1].position, routePoints[i].position], count: 2)
    //                    lineOverlays[i - 1] = lineOverlay
    //                    overlaysToReturn.append(lineOverlay)
    //                }
    //
    //                if i < routePoints.count - 1 {
    //                    let lineOverlay = StRoutePolyline( coordinates: [routePoints[i].position, routePoints[i + 1].position], count: 2)
    //                    lineOverlays[i] = lineOverlay
    //                    overlaysToReturn.append(lineOverlay)
    //                }
    //
    //            }
    //        }
    //        return overlaysToReturn
    //    }
    
    
    
    func saveToGpxTrack(){
        //        let root = GPXRoot(creator: "Stentec WinGPS Marine for iOS")
        //
        //        var thisTrack :GPXTrack = GPXTrack()
        //        for segment in locationsBySegment {
        //            var gpxSegment = GPXTrackSegment()
        //            for loc in segment {
        //                var gpxTrackPoint = GPXTrackPoint(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
        //                gpxTrackPoint.time = loc.timestamp
        //
        //                gpxSegment.add(trackpoint: gpxTrackPoint)
        //            }
        //            thisTrack.add(trackSegment: gpxSegment)
        //        }
        //        root.add(track: thisTrack)
        //
        //        do {
        //            try root.gpx().write(to: getTailTrackUrl()!, atomically: true, encoding: .utf8)
        //        } catch {
        //            print(error)
        //        }
        
        let root = GPXRoot(creator: "Stentec WinGPS Marine for iOS")
        
        var thisTrack :GPXTrack = GPXTrack()
//        thisTrack.name = "track-\(Tracks.service.getTrackIndex(self) ?? -1)"
        thisTrack.name = self.name
        for segment in locationsBySegment {
            var gpxSegment = GPXTrackSegment()
            for loc in segment {
                var gpxTrackPoint = GPXTrackPoint(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
                gpxTrackPoint.time = loc.timestamp
                
                gpxSegment.add(trackpoint: gpxTrackPoint)
            }
            thisTrack.add(trackSegment: gpxSegment)
        }
        root.add(track: thisTrack)
        
        do {
            if let trackIndex = Tracks.service.getTrackIndex(self) {
                var url = Tracks.getTrackFileUrlByIndex(trackIndex)
                try root.gpx().write(to: url, atomically: true, encoding: .utf8)
                dout("written track file to: \(url.absoluteString)")
            }
        } catch {
            print(error)
        }
    }
    
}
