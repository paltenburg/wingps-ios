//
//  TrackingService.swift
//  WinGPS Marine
//
//  Created by Standaard on 05/06/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import CoreLocation

class TrackingService :NSObject {
	
//    static let RUNNING_TRACK = "running-track"
    static let RUNNING_TRACK_INDEX = "running-track-index"
	static let service = TrackingService()
	
	let trackingServiceLocationManager = CLLocationManager()

	var runningTrack :Track? = nil
	
	let recordingEnabled :Bool = true
	
	let distanceFilterValue :CLLocationDistance = 5


	
    var paused = false
    
	private override init(){
		super.init()
		
		trackingServiceLocationManager.delegate = self
		trackingServiceLocationManager.distanceFilter = distanceFilterValue
		trackingServiceLocationManager.desiredAccuracy = 5
		trackingServiceLocationManager.showsBackgroundLocationIndicator = true
		
//        self.runningTrack = Track(fromTailTrackFile: true)
        
//        if let runningTrackURLString :String = UserDefaults.standard.string(forKey: TrackingService.RUNNING_TRACK) {
//            self.runningTrack = Track(fromFile: URL(fileURLWithPath: runningTrackURLString))
//        }
        
        let runningTrackIndex = UserDefaults.standard.integer(forKey: TrackingService.RUNNING_TRACK_INDEX)
        if runningTrackIndex > -1 {
            if Tracks.service.tracks.count > runningTrackIndex {
                self.runningTrack = Tracks.service.tracks[runningTrackIndex]
                self.runningTrack?.setVisible(true)
                pause()
            } else {
                UserDefaults.standard.set(-1, forKey: TrackingService.RUNNING_TRACK_INDEX)
            }
        }
	}
	
	func start() {
        paused = false

        makeSureARunningTrackExists()
        
		trackingServiceLocationManager.allowsBackgroundLocationUpdates = true
		trackingServiceLocationManager.startUpdatingLocation()
        
	}
	
	func stop() {
        
		trackingServiceLocationManager.stopUpdatingLocation()
		trackingServiceLocationManager.allowsBackgroundLocationUpdates = false
        
        if Resources.appType == .plus {  // for any other app than the plus: The stop button acts as a pauze button

            paused = false

            saveRunningTrack()
            
            self.runningTrack = nil
            
            UserDefaults.standard.set(-1, forKey: TrackingService.RUNNING_TRACK_INDEX)
            
        } else {   // for any other app than the plus: The stop button acts as a pauze button

            paused = true
            
            self.runningTrack?.endCurrentSegment()
        }
        
    }
	
	func pause() {
        paused = true

		trackingServiceLocationManager.stopUpdatingLocation()
		trackingServiceLocationManager.allowsBackgroundLocationUpdates = false
		self.runningTrack?.endCurrentSegment()
	}
	
	func saveRunningTrack(){
        
        if let runningTrack = self.runningTrack {
            runningTrack.saveToGpxTrack()
            
            if let trackIndex = Tracks.service.getTrackIndex(runningTrack) {
//                let url = Tracks.getTrackFileUrlByIndex(trackIndex)
                UserDefaults.standard.set(trackIndex, forKey: TrackingService.RUNNING_TRACK_INDEX)
            }
        }
	}
    
    func makeSureARunningTrackExists(){
        if Resources.appType == .plus {
            if self.runningTrack == nil {
                // Create new running track
                
                let persistentTrackIndex :Int = UserDefaults.standard.integer(forKey: Resources.PERSISTENT_TRACK_INDEX)
                UserDefaults.standard.set(persistentTrackIndex + 1, forKey: Resources.PERSISTENT_TRACK_INDEX)
                
                let newTrack = Track()
                newTrack.name = "Track " + String(persistentTrackIndex + 1)
                
                Tracks.service.addTrack(newTrack)
                self.runningTrack = newTrack
                self.runningTrack?.setVisible(true)
            }
        } else {
            // Use single tail track for versions other than Plus.
            if self.runningTrack == nil {
                // Create new running track
                
//                let persistentTrackIndex :Int = UserDefaults.standard.integer(forKey: Resources.PERSISTENT_TRACK_INDEX)
                //                UserDefaults.standard.set(persistentTrackIndex + 1, forKey: Resources.PERSISTENT_TRACK_INDEX)
                
                if Tracks.service.tracks.count >= 1 {
                    self.runningTrack = Tracks.service.tracks.first
                } else {
                    let newTrack = Track()
                    newTrack.name = "Track 1"
                    
                    Tracks.service.addTrack(newTrack)
                    self.runningTrack = newTrack
                }
                self.runningTrack?.setVisible(true)
            }
        }
    }
}

extension TrackingService: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		
		/// Update tracking
		if let location = locations.last {
			dout("TrackingService didUpdateLocations \(location.speed) \(location.course) \(location.coordinate.longitude) \(location.coordinate.latitude)")
		}
		
		guard let location = locations.last,
			location.horizontalAccuracy < 20 else {
				return
		}
		
		if recordingEnabled {
			
            makeSureARunningTrackExists()
			
			if let runningTrack = self.runningTrack {
				runningTrack.addLocations(locations: locations)
				
				ViewController.instance?.updateRunningTrack()
			}
		}
	}
}
