//
//  NavigationWaypointAnnotation.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 25/03/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class NavigationWaypointAnnotation: MKPointAnnotation {
    
    private static let TITLE = "Navigation Waypoint"
    
    override init(){
        super.init()
    }
    
    init(_ location: CLLocationCoordinate2D){
        super.init()
        
        coordinate = location
        title = NavigationWaypointAnnotation.TITLE
        
        //subtitle =
        
    }
    
    func getCLLocation() -> CLLocation {
        return CLLocation(latitude: self.coordinate.latitude, longitude: self.coordinate.longitude)
    }
}

