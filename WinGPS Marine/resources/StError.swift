//
//  StError.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 02/02/2021.
//  Copyright © 2021 Stentec. All rights reserved.
//

import Foundation

class StError {
    
    static let ER_OK = "ER_OK"
    
    // Network errors
    static let ER_FILEVERSIONNEWER = "ER_FILEVERSIONNEWER"
    static let ER_FILEVERSIONUNSUPPORTED = "ER_FILEVERSIONUNSUPPORTED"
    static let ER_FILEREADINGERROR = "ER_FILEREADINGERROR"
    static let ER_FILEFORMAT = "ER_FILEFORMAT"
    
    static let ER_BUFFERUNDERFLOW = "ER_BUFFERUNDERFLOW"
}

// Throwable errors:
enum ThrowableError :Error {
    case generalError
}
