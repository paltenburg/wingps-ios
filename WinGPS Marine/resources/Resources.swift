//
//  File.swift
//  WinGPS Marine
//
//  Created by Standaard on 03/09/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class Resources {
    // stentecColor 004A87
    static let stentecColor :UIColor = UIColor(red:0.00, green:0.29, blue:0.53, alpha:1.0)
    static let appid :Int = 69633 // WinGPS Marine Plus
    
    //	static let DKW2ChartsDir :String = getDKW2Directory().path
    
    //    static let appVersionNumber :Int = 130
    static let appVersionString :String = StUtils.getVersionString()
    
    
    #if MARINE
    static var appType :AppType = .lite
    static let packageName :String = "com.stentec.wingps_marine_lite"
    
    static let useWWNetwork = true // app-wide setting to enable and disable use of the network.
    
    static let dkw1800_pid :Int64 = 0 // placeholder
    
    #elseif PLUS
    static var appType :AppType = .plus
    static let packageName :String = "com.stentec.wingps-marine-plus"
    
    static let useWWNetwork = true // app-wide setting to enable and disable use of the network.
    
    static let dkw1800_pid :Int64 = 0 // placeholder
    
    #elseif DKW1800
    static var appType :AppType = .dkw1800
    //    static let packageName :String = "com.stentec.wingps-marine-1800"
    static let packageName :String = "com.stentec.wingps-marine-1800-2021"
    static let dkw1800_pid = 196713 // product id of dkw1800 edition 2021
    
    static let useWWNetwork = false // app-wide setting to enable and disable use of the network.
    
    #elseif FRIESEMEREN
    static var appType :AppType = .friesemeren
    static let packageName :String = "com.stentec.vaarkaart-friese-meren"
    
    static let useWWNetwork = true // app-wide setting to enable and disable use of the network.
    
    static let dkw1800_pid :Int64 = 0 // placeholder
    
    #endif
    
    static let module = 999
    static let pid = -1
    
    
    
    // for testing
    static let alwaysUseDeveloperDeviceCheck = false
    static let alwaysUseProductionDeviceCheck = true
    
    // If running on Mac than bypass device check
    static let runningOnMac = UIDevice.current.systemName == "Mac OS X"
    static let passDeviceCheck = runningOnMac
    //    static let passDeviceCheck = true // DEBUG
    static let PASS_DEVICE_CHECK = "PASS_DEVICE_CHECK"
    
    
    // App settings
    
    static let USERDEFAULT_SETTINGS = "defaultSettings"
    static let DEFAULT_SHOW_WAYPOINTS = "defaultShowWaypoints"
    static let SETTINGS_SHOW_WAYPOINT_NAMES = "defaultShowWaypointNames"
    //	static let SETTINGS_SHOW_APPLEMAPS = "defaultShowAppleMaps"
    static let SETTINGS_SHOW_ROUTES = "settingsShowRoutes"
    static let SETTINGS_SHOW_AIS = "settingsShowAIS"
    static let SETTINGS_SHOW_TRACKS = "settingsShowTracks"
    static let SETTINGS_SHOW_NETWORK = "settingsShowNetwork"
    
    static let SETTINGS_SHOW_APPLEMAPS = "module-\(ChartManagerController.appleMapsTitle)"
    static let SETTINGS_SHOW_NOAACHARTS = "module-\(ChartManagerController.NoaaChartsTitle)"
    static let SETTINGS_SHOW_OVERVIEWCHARTS = "SETTINGS_SHOW_OVERVIEWCHARTS"
    
    static let SETTINGS_LOAD_WWNETWORK = "SETTINGS_LOAD_WWNETWORK"
    
    static let SETTINGS_KEEP_SCREEN_ON = "defaultKeerScreenOn"
    static let SETTINGS_TAILTRACK_VISIBLE = "settingsTailtrackVisible"
    static let SETTINGS_TAILTRACK_VISIBLE_DEFAULT = true
    
    static let PERSISTENT_TRACK_INDEX = "PERSISTENT_TRACK_INDEX"
    
    static let DEFAULT_UNITS_MODE = "defaultUnitsMode"
    static let DEFAULT_UNITS_MODE_NAUTICAL = 0
    static let DEFAULT_UNITS_MODE_METRIC = 1
    static let DEFAULT_UNITS_MODE_US = 2
    static let DEFAULT_UNITS_MODE_CUSTOM = 3
    
    static let SETTINGS_UNIT_SPEED = "settingsUnitSpeed"
    static let SETTINGS_UNIT_SPEED_KN = "kn (knots)"
    static let SETTINGS_UNIT_SPEED_KMH = "km/h (kilometer per hour)"
    static let SETTINGS_UNIT_SPEED_MPH = "mph (miles per hour)"
    static let SETTINGS_UNIT_SPEED_DEFAULT = SETTINGS_UNIT_SPEED_KN
    
    static let SETTINGS_UNIT_DISTANCE_LARGE = "settingsUnitDistanceLarge"
    static let SETTINGS_UNIT_DISTANCE_LARGE_NMILE = "nm (nautical miles)"
    static let SETTINGS_UNIT_DISTANCE_LARGE_KM = "km (kilometer)"
    static let SETTINGS_UNIT_DISTANCE_LARGE_MILE = "mile (statute miles)"
    static let SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT = SETTINGS_UNIT_DISTANCE_LARGE_NMILE
    
    static let SETTINGS_UNIT_DISTANCE_SMALL = "settingsUnitDistanceSmall"
    static let SETTINGS_UNIT_DISTANCE_SMALL_METER = "m (meter)"
    static let SETTINGS_UNIT_DISTANCE_SMALL_YARD = "yd (yard)"
    static let SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT = SETTINGS_UNIT_DISTANCE_SMALL_METER
    
    static let SETTINGS_CHART_VISIBILLITY = "chartVisibilitySettings"
    
    static let SETTING_DISTANCE_CIRCLES_ENABLED = "SETTING_DISTANCE_CIRCLES_ENABLED"
    static let SETTING_DISTANCE_CIRCLES_COUNT = "SETTING_DISTANCE_CIRCLES_COUNT"
    static let SETTING_DISTANCE_CIRCLES_DISTANCE = "SETTING_DISTANCE_CIRCLES_DISTANCE"
    
    static let SETTING_CHART_ORIENTATION = "SETTING_CHART_ORIENTATION"
    static let SETTING_CHART_ORIENTATION_NORTH_UP = 0
    static let SETTING_CHART_ORIENTATION_COURSE_UP = 1
    static let SETTING_CHART_ORIENTATION_DEFAULT_SETTING = SETTING_CHART_ORIENTATION_NORTH_UP
    
    // Stored mapposition rectangle:
    static let STORED_MAP_POSITION_RECT_X = "STORED_MAP_POSITION_RECT_X"
    static let STORED_MAP_POSITION_RECT_Y = "STORED_MAP_POSITION_RECT_Y"
    static let STORED_MAP_POSITION_RECT_WIDTH = "STORED_MAP_POSITION_RECT_WIDTH"
    static let STORED_MAP_POSITION_RECT_HEIGHT = "STORED_MAP_POSITION_RECT_HEIGHT"
    
    // Download settings
    static let SETTINGS_USE_MOBILE_DATA = "settingsUseMobileData"
    
    static let SETTINGS_TCP_CONNECTION_ENABLED = "SETTING_TCP_CONNECTION_ENABLED"
    static let SETTINGS_TCP_HOST = "SETTINGS_TCP_HOST"
    static let SETTINGS_TCP_PORT = "SETTINGS_TCP_PORT"
    
    static let SETTINGS_AIS_ENABLED = "SETTINGS_AIS_ENABLED"
    static let SETTINGS_AIS_MAX_TARGETS = "SETTINGS_AIS_MAX_TARGETS"
    static let SETTINGS_AIS_MAX_TARGET_DEFAULT = 200
    
    // IAP
    static let iapTestingConsumableID = "com.stentec.wingps_marine.testing_consumable"
    static let iapMarineCompleteID = "com.stentec.wingps_marine.complete"
    
    
}

