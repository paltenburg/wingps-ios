//
//  StDKW2BoundsTag.swift
//  MapDemo
//
//  Created by Standaard on 18/09/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation

class DKW2BoundsTag {
	var bounds = Rectangle()
	var boundsValid = false
	var points = [PolyLineD]()
	var polygonValid = false
	
	init(){
	}
	
	//    RectangleD mBounds;           // Bounding rectangle of points
	//    boolean mBoundsValid;         // If true, the bounds are up to date
	//    ArrayList<PolyLineD> mPoints; // Array containing the points
	//    boolean mPolygonValid;        // If true, the polygon's additional fields are valid
	//
	//    public StDKW2BoundsTag() {
	//    mBounds = new RectangleD();
	//    mBounds.XMin = 0;
	//    mBounds.YMin = 0;
	//    mBounds.XMax = 0;
	//    mBounds.YMax = 0;
	//    mBoundsValid = true;
	//    mPolygonValid = true;
	//    mPoints = new ArrayList<PolyLineD>();
	//    }
	
	func add(_ point :Double2) {
		boundsValid = false
		polygonValid = false
		var poly = PolyLineD()
		poly.coord = point
		points.append(poly)
	}
	
	func clear() {
		boundsValid = false
		points.removeAll()
		polygonValid = true
	}
	
	func get(_ index :Int) -> Double2 {
		return points[index].coord
	}
	
	//    public RectangleD getBounds() {
	//    if (!mBoundsValid) {
	//    int Count = mPoints.size();
	//    if (Count > 0) {
	//    mBounds.setMin(mPoints.get(Count - 1).Coord);
	//    mBounds.setMax(mPoints.get(Count - 1).Coord);
	//    for (int i = Count - 2; i >= 0; i--) {
	//    Double2 P = mPoints.get(i).Coord;
	//    if (P.x > mBounds.XMax)
	//    mBounds.XMax = P.x;
	//    else if (P.x < mBounds.XMin)
	//    mBounds.XMin = P.x;
	//    if (P.y > mBounds.YMax)
	//    mBounds.YMax = P.y;
	//    else if (P.y < mBounds.YMin)
	//    mBounds.YMin = P.y;
	//    }
	//    } else {
	//    mBounds.XMin = 0.0;
	//    mBounds.YMin = 0.0;
	//    mBounds.XMax = 0.0;
	//    mBounds.YMax = 0.0;
	//    }
	//    mBoundsValid = true;
	//    }
	//    return mBounds;
	//    }
	
	func getCount() -> Int {
		return points.count
	}
	
	func pointInPolygon(Point :Double2) -> Bool {
		var Poly :[PolyLineD] = []
		for point in points {
			Poly.append(point)
		}
		if !polygonValid {
			Math.polygonInitialize(Poly: &Poly, Count: points.count)
			polygonValid = true
		}
		return Math.pointInPolygon(P: Point, Poly: Poly, Count: points.count)
	}
	
}
