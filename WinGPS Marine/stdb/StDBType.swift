//
//  StDBType.swift
//  MapDemo
//
//  Created by Standaard on 13/04/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit

class StDBType {
	
	static let WDBF_TYPE_FONTCOLOR = 0x0200
	static let WDBF_TYPE_LIGHTCHARFONTCOLOR = 0x0400
	static let WDBF_TYPE_SUBFONTCOLOR = 0x0800
	static let WDBF_TYPE_DRAWICONOVERLAY = 0x1000
	
	var guid :StUUID?
	var timeCreatedUTC :Date?
	var timeModifiedUTC :Date?
	var iconOriginX :Float = 0
	var iconOriginY :Float = 0
	var name :String = ""
	var enabled :Bool = true
	var fixedToChart :Bool = true
	var fontColor :Int = 0
	var fontColorLightChar :Int = 0
	var fontColorSub :Int = 0
	var fontSize :UInt8 = 0
	var fontSizeLightChar :UInt8 = 0
	var fontSizeSub :UInt8 = 0
	var height :Int = 0
	var width :Int = 0
	var iconImage :UIImage? = nil
	var screenScaling :Bool = true
	var showText = true
	
	var overlayWidth :Int = 0
	var overlayHeight :Int = 0
	
	var flags :Int = 0
	
//	func createIconBitmap(iconBitmap :NSData, overlayIconBitmap :NSData, drawOverlay :Bool) throws {
	func createIconBitmap(iconBitmap :NSData, overlayIconBitmap :NSData,
								 wIcon :Int, hIcon :Int,
								 wOverlay :Int, hOverlay :Int,
								 originX :Float, originY :Float, drawOverlay :Bool) throws {

		var Width = self.width
		var Height = self.height

		// create it, and later store the UIImage in cache
		//guard let colorSpace = CGColorSpace(name: CGColorSpace.genericRGBLinear) else {return nil}
		
//		var bitmapContext :CGContext?
		let bitmapInfo = CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue)
		let bitmapContext = CGContext(data: nil, width: Width, height: Height, bitsPerComponent: 8, bytesPerRow: 4 * Width, space: CGColorSpaceCreateDeviceRGB(),
												bitmapInfo: bitmapInfo.rawValue)
		bitmapContext!.setAllowsAntialiasing(true)
		bitmapContext!.setShouldAntialias(true)
		//bitmapContextTmp.interpolationQuality
		var pixelBuffer = bitmapContext!.data?.bindMemory(to: UInt32.self, capacity: Width * Height)
		var pixelCount :Int = 0
		
		while pixelCount < (Width * Height * 4) {
			let color = try readIconPixel(buffer: iconBitmap, at: &pixelCount)
			putInt(color, in: &pixelBuffer!, at: pixelCount - 4)
		}
		
		iconImage = imageFromContext(bitmapContext!)
		
		if false && drawOverlay {
			//		let bitmapInfo = CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue)
			let overlayContext = CGContext(data: nil, width: wOverlay, height: hOverlay, bitsPerComponent: 8, bytesPerRow: 4 * wOverlay,
													 space: CGColorSpaceCreateDeviceRGB(), bitmapInfo: bitmapInfo.rawValue)
			overlayContext!.setAllowsAntialiasing(true)
			overlayContext!.setShouldAntialias(true)
			var overlayPixelBuffer = overlayContext!.data?.bindMemory(to: UInt32.self, capacity: wOverlay * hOverlay)
			var overlayByteCount :Int = 0
			while overlayByteCount < (wOverlay * hOverlay * 4) {
				let color = try readIconPixel(buffer: overlayIconBitmap, at: &overlayByteCount)
				putInt(color, in: &overlayPixelBuffer!, at: overlayByteCount - 4)
			}
			var overlayImage :UIImage = imageFromContext(overlayContext!)!

			var extraTopHeight :Int = 0
			var extraBottomHeight :Int = 0
			var extraLeftWidth :Int = 0
			var extraRightWidth :Int = 0
			var oX :Float = originX - (Float(wOverlay) / 2)
			var oY :Float = originY - (Float(wOverlay) / 2)
			if originY <= Float(hOverlay) / 2{
				extraTopHeight = hOverlay }
			if originY >= Float((hIcon - (hOverlay / 2))) {
				extraBottomHeight = (hOverlay / 2) }
			if originX <= Float(wOverlay / 2) {
				extraLeftWidth = (wOverlay / 2) }
			if originX >= Float(wIcon - (wOverlay / 2)) {
				extraRightWidth = (wOverlay / 2) }
			
			//Aanmaken van een bitmap op een canvas met uitgebreide hoogte/breedte in het geval de overlayIcon erbuiten valt.
			var newWidth :Int = wIcon + extraLeftWidth + extraRightWidth
			var newHeight :Int = hIcon + extraTopHeight + extraBottomHeight
			
			let newBitmapContext = CGContext(data: nil,
													width: newWidth, height: newHeight,
													bitsPerComponent: 8, bytesPerRow: 4 * newWidth, space: CGColorSpaceCreateDeviceRGB(),
													bitmapInfo: CGBitmapInfo(rawValue: CGBitmapInfo.byteOrder32Big.rawValue | CGImageAlphaInfo.premultipliedLast.rawValue).rawValue)
			newBitmapContext!.setAllowsAntialiasing(true)
			newBitmapContext!.setShouldAntialias(true)

//			var pixelBuffer = bitmapContext!.data?.bindMemory(to: UInt32.self, capacity: newWidth * newHeight)
//			var byteCount :Int = 0
//			while byteCount < newWidth * newHeight * 4 {
//				let color = try readIconPixel(buffer: iconBitmap, at: &byteCount)
//				putInt(color, in: &pixelBuffer!, at: pixelCount - 4)
//			}
//			iconImage = imageFromContext(bitmapContext!)

			UIGraphicsPushContext(newBitmapContext!)
			
//			mkTileBitmapContext.interpolationQuality = CGInterpolationQuality.low
			
//			iconImage.draw(in: CGRect(x: v.verts[0], y: v.verts[1],
//											  width: v.verts[2] - v.verts[0], height: v.verts[5] - v.verts[1]))
			
			iconImage!.draw(in: CGRect(x: extraLeftWidth, y: extraTopHeight,
											  width: wIcon, height: hIcon))
			
			//					let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
			
			overlayImage.draw(in: CGRect(x: Int(oX + Float(extraLeftWidth)), y: Int(oY + Float(extraTopHeight)),
												  width: wOverlay, height: hOverlay))
			
			UIGraphicsPopContext()
			
			iconImage = imageFromContext(newBitmapContext)
			
//			iconImage = overlayImage
			
//			Canvas c = new Canvas(bitmap);
//			
//			c.drawBitmap(bitmapIcon, extraLeftWidth, extraTopHeight, null);
//			
//			colorarray = toIntArray(aOverlay);
//			c.drawBitmap(colorarray, 0, aWOverlay, oX, oY, aWOverlay, aHOverlay, true, null);
//			return new BitmapDrawable(mContext.getResources(), bitmap);
		}
	}
	
	func putInt(_ what :UInt32, in pixelArray :inout UnsafeMutablePointer<UInt32>, at pos :Int){
		pixelArray[pos/4] = what
	}
	
	func readIconPixel(buffer: NSData, at position :inout Int) throws -> UInt32 {
		
		if position > (buffer.length - 4) {	throw NSError() }
		
		var byte1 :UInt8 = 0
		var byte2 :UInt8 = 0
		var byte3 :UInt8 = 0
		var byte4 :UInt8 = 0
		buffer.getBytes(&byte1, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte2, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte3, range: NSRange(location: position, length: 1)); position += 1
		buffer.getBytes(&byte4, range: NSRange(location: position, length: 1)); position += 1
		if(byte4 == 0){
			//print(name, "Bytes: ",byte1,byte2,byte3,byte4)
		}
		let alpha = Double(byte4) / 255.0
		
		let B = Double(byte1) * (alpha)
		let G = Double(byte2) * (alpha)
		let R = Double(byte3) * (alpha)
		//byte4 = 255
		return UInt32(R) | UInt32(G) << 8 | UInt32(B) << 16 | UInt32(byte4) << 24
	}
}
