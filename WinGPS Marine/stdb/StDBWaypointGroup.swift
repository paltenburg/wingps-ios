//
//  StDBWaypointGroup.swift
//  WinGPS Marine
//
//  Created by Standaard on 28/06/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation

class StDBWaypointGroup {
	
	var GUID :StUUID?
	var timeUTCModified :Date?
	var timeUTCValidated :Date?
	var name :String = ""
	var enabled :Bool = true
	var isUserGroup :Bool = false
	var waypointIndexCount :Int32 = 0
	var waypointRefCount :Int32 = 0
	var waypointList :Array<StDBWaypoint> = Array<StDBWaypoint>()
	
	func addWaypoint(waypoint :StDBWaypoint) {
		waypointList.append(waypoint)
	}
	
	func addWaypoints(waypoints :Array<StDBWaypoint>){
		waypointList.append(contentsOf: waypoints)
	}
	
	func deleteWaypoint(waypoint :StDBWaypoint){
		for i in 0..<waypointList.count {
			if(waypointList[i] === waypoint){
				waypointList.remove(at: i)
				break
			}
		}
	}
}
