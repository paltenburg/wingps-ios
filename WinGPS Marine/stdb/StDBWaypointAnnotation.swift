//
//  StDBWaypointAnnotation.swift
//  MapDemo
//
//  Created by Standaard on 25/04/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class StDBWaypointAnnotation: MKPointAnnotation {
	
	var waypoint :StDBWaypoint!
	var textTooSmall :Bool = false
	
	var inPreviousView :Bool = false
	var inNewView :Bool = false

	override init(){
		super.init()
	}
	
	init(_ copyFrom :StDBWaypointAnnotation){
		super.init()

		waypoint = copyFrom.waypoint
		
		coordinate = copyFrom.coordinate
		title = copyFrom.title
		//print("Waypoint :",waypoint.name)
		subtitle = copyFrom.subtitle
	}
    
}

extension String {
	func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
		
		return ceil(boundingBox.height)
	}
	
	func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
		let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
		let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: convertToOptionalNSAttributedStringKeyDictionary([convertFromNSAttributedStringKey(NSAttributedString.Key.font): font]), context: nil)
		
		return ceil(boundingBox.width)
	}
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}
