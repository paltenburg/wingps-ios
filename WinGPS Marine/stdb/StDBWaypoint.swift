//
//  StDBWaypoint.swift
//  MapDemo
//
//  Created by Standaard on 19/04/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation

class StDBWaypoint {
	
	static let WDBF_WAYPOINT_HEIGHT =           0x0001;  // Height != 0.
	static let WDBF_WAYPOINT_SUBNAME =          0x0002;  // Sub name is not empty.
	static let WDBF_WAYPOINT_LINKRADIUS =       0x0004;  // Link radius > 0.
	static let WDBF_WAYPOINT_PROXIMITY =        0x0008;  // Proximity > 0.
	static let WDBF_WAYPOINT_TEXT =             0x0010;  // Text is not empty.
	static let WDBF_WAYPOINT_TYPEINDEX =        0x0020;  // Type index is set.
	static let WDBF_WAYPOINT_TYPEREF =          0x0040;  // Type reference is set.
	static let WDBF_WAYPOINT_LINKCOUNT =        0x0080;  // LinkCount > 0.
	static let WDBF_WAYPOINT_LINKAUDIBLE =      0x0100;  // AudibleLink != NULL.
	static let WDBF_WAYPOINT_LINKVISIBLE =      0x0200;  // VisibleLink != NULL.
	static let WDBF_WAYPOINT_NAMEORIGIN =       0x0400;  // NameOrigin != WDBF_NAMEORG_CENTERBOTTOM
	static let WDBF_WAYPOINT_PRECISION =        0x0800;  // Precision != PRECISION_UNDEFINED
	static let WDBF_WAYPOINT_SCALE =            0x1000;  // Scale > 0
	static let WDBF_WAYPOINT_LIGHTCHAR =        0x2000;  // LightChar is not empty
	static let WDBF_WAYPOINT_ANGLE =            0x4000;  // Angle != 0.
	
	var GUID :StUUID?
	var name :String = ""
	var nameOrigin :Int = 0
	var position :Double2?
	var subName :String = ""
	var timeCreatedUTC :Date?
	var timeModifiedUTC :Date?
	var type :StDBType?
	var scale :Float = 0
	var drawScale :Float = 1
	var flags :Int = 0
	
	var angle :Float = 0
	var lightChar :String = ""
	
	var height :Double = 0
	
	var links :Array<WpLink> = Array<WpLink>()
	
	func getLatitude() -> Double {
		return position!.x * 180 / .pi
	}
	
	func getLongitude() -> Double {
		return position!.y * 180 / .pi
	}

	func getScale(zoomLevel: Float) -> Float {
		let metersPerPoint = 156412 / pow(2, zoomLevel)
		let zoomResize =  log10(metersPerPoint / scale) / log10(sqrt(2))
		if zoomResize > 0 {
			drawScale = pow(sqrt(2), (-1 * zoomResize))
		} else {
			drawScale = 1
		}
		return drawScale
	}
	
	class WpLink {
		var description :String = ""
		var url :String = ""
	}
}
