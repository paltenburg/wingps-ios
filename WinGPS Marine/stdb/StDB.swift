//
//  StDB.swift
//  MapDemo
//
//  Created by Standaard on 04/04/2018.
//  Copyright © 2018 Stentec. All rights reserved.
//

import Foundation

class StDB{
	
	let DBID = "StentecWptDB"
	
	var fileName :URL?
	var fileHandle :StDBFileHandle?
	
	var fileSize :Int64 = 0         // File size in bytes.
	var fileTime :Date?            // Last write-time of the file.
	
	// Header
	var formatVersion :StVersion = StVersion(0)
	var fileSizeFromHeader :Int64 = 0
	var guid :StUUID?
	var databaseVersion :StVersion?
	var timeUTCModified :Date?
	var timeUTCValidated :Date?
	var waypointCount :Int = 0
	var typeCount :Int16 = 0
	var groupCount :Int16 = 0
	var routeCount :Int16 = 0
	var polygonCount :Int16 = 0
	var nameCount :UInt8 = 0
	var databaseType :UInt8 = 0
	
	// Database fields
	var dbIdentifier :String = ""
	var dbNames :Array<String> = Array<String>()
	var dbTypes :Array<StDBType> = Array<StDBType>()
	var dbWaypoints :Array<StDBWaypoint> = Array<StDBWaypoint>()
	var dbWaypointGroups :Array<StDBWaypointGroup> = Array<StDBWaypointGroup>()
	
	init(fileName: URL) {
		loadFromStDBFile(from: fileName)
		
	}
	
	func checkID() {
		
	}
	
	func loadFromStDBFile(from fileName: URL) {
		
		
		self.fileName = fileName
		
		do {
			try self.fileHandle = StDBFileHandle(FileHandle(forReadingFrom: fileName))
		} catch {
			//			errorCode = .ER_FILE_ERROR
			return
		}
		
		fileTime = nil
		
		// Read the file
		print("loadFromStDBFile:", fileName.lastPathComponent)
		
		//        try {
		fileSize = Int64(getFileSize(fileName.path)!)
		fileTime = getLastModified(fileName.path)
		try! StDBFileRead() // TODO: Debug pass on error throwing
		//        } catch (IOException e) {
		//        aError.setErrorCode(StError.ER_FILEFORMAT)
		//        }
		//        if (aError.getErrorCode() != StError.ER_OK)
		//        return
		
		
	}
	
	func StDBFileRead() throws {
		
		var errorCode = ErrorCode.ER_OK
		
		guard let fileHandle = self.fileHandle else {
			return
		}
		
		
		// Read ID
		var Buffer = fileHandle.handle.readData(ofLength: 12) as NSData
        
		var dbID = try getStringFromNSDataBuffer(buffer: Buffer, position: 0, length: 12)
		if(dbID != DBID) {
			return
		}
		

		// Read first header data
		Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
		let v :UInt32 = try getUInt32FromNSDataBuffer(buffer: Buffer, start: 0)
		print("Version Int", String(v))
		formatVersion = try getStVersionFromNSDataBuffer(buffer: Buffer, start: 0)
		print("format: ", formatVersion.getVersionString())
		if formatVersion.compareTo(StVersion(extra: 0, build: 4, minor: 1, major: 2)) >= 0 {
			Buffer = fileHandle.handle.readData(ofLength: 50) as NSData
			polygonCount = try getShortFromNSDataBuffer(buffer: Buffer, start: 46)
			nameCount = try getUByteFromNSDataBuffer(buffer: Buffer, start: 48)
			databaseType = try getUByteFromNSDataBuffer(buffer: Buffer, start: 49)

		} else {
			Buffer = fileHandle.handle.readData(ofLength: 48) as NSData
			nameCount = try getUByteFromNSDataBuffer(buffer: Buffer, start: 46)
			databaseType = try getUByteFromNSDataBuffer(buffer: Buffer, start: 47)

		}
		
		
		fileSizeFromHeader = try getInt64FromNSDataBuffer(from: Buffer, start: 0)
		guid = try readGuid(from: Buffer, start: 8)
		databaseVersion = try getStVersionFromNSDataBuffer(buffer: Buffer, start: 24)
		timeUTCModified = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 28)
		//let timeString = String(timeUTCModified!.description)
		timeUTCValidated = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 32)
		waypointCount = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 36))
		typeCount = try getShortFromNSDataBuffer(buffer: Buffer, start: 40)
		groupCount = try getShortFromNSDataBuffer(buffer: Buffer, start: 42)
		routeCount = try getShortFromNSDataBuffer(buffer: Buffer, start: 43)
		
		var strL :Int = 0
		var string :String = ""
		// Identifier
		Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
		strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
		if(strL > 0){
			Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
			dbIdentifier = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
		}
		
		// Names
		for i in 0..<nameCount {
			
			Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
			strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
			string = ""
			if(strL > 0){
				Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
				string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
			}
			

			dbNames.append(string)
			
		}

		// Types
		
		for i in 0..<typeCount {
			var type :StDBType = StDBType()
			Buffer = fileHandle.handle.readData(ofLength: 39) as NSData // First bunch of bytes
			type.guid = try readGuid(from: Buffer, start: 0)
			type.timeCreatedUTC = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 16)
			type.timeModifiedUTC = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 20)
			type.iconOriginX = try getFloatFromNSDataBuffer(buffer: Buffer, start: 24)
			type.iconOriginY = try getFloatFromNSDataBuffer(buffer: Buffer, start: 28)
			type.width = Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 32))
			type.height = Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 33))
			type.overlayWidth = Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 34))
			type.overlayHeight = Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 35))
			type.fontSize = try getUByteFromNSDataBuffer(buffer: Buffer, start: 36)
			type.fontSizeLightChar = try getUByteFromNSDataBuffer(buffer: Buffer, start: 37)
			type.fontSizeSub = try getUByteFromNSDataBuffer(buffer: Buffer, start: 38)
			
			// Read fixed-to-chart
			if formatVersion.compareTo(StVersion(extra: 0, build: 2, minor: 1, major: 2)) >= 0 {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				type.fixedToChart = (try getUByteFromNSDataBuffer(buffer: Buffer, start: 0) == 1)
			}

			// Read flags
			Buffer = fileHandle.handle.readData(ofLength: 2) as NSData
			type.flags = Int(try getUShortFromNSDataBuffer(buffer: Buffer, start: 0)) // Reverse? Signed/Unsigned?
			
			// Read IconBitmap
			let iconBitmapSize = 4 * type.width * type.height
			Buffer = fileHandle.handle.readData(ofLength: iconBitmapSize) as NSData
			let BitmapBuffer = Buffer
			
			// Read OverlayIconBitmap
			let overlayIconBitmapSize = 4 * type.overlayWidth * type.overlayHeight
			Buffer = fileHandle.handle.readData(ofLength: overlayIconBitmapSize) as NSData
			
			try type.createIconBitmap(iconBitmap: BitmapBuffer,
											  overlayIconBitmap: Buffer,
											  wIcon: type.width,
											  hIcon: type.height,
											  wOverlay: type.overlayWidth,
											  hOverlay: type.overlayHeight,
											  originX: type.iconOriginX,
											  originY: type.iconOriginY,
											  drawOverlay: (type.flags & StDBType.WDBF_TYPE_DRAWICONOVERLAY) == StDBType.WDBF_TYPE_DRAWICONOVERLAY)
			
			// Read Name
			Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
			strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
			string = ""
			if(strL > 0){
				Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
				string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
			}
			//print("Name (", i, ") :", string)
			type.name = string
			
			// Read FontColor
			if (type.flags & StDBType.WDBF_TYPE_FONTCOLOR) == StDBType.WDBF_TYPE_FONTCOLOR {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				type.fontColor = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 0))
			}
			
			// Read LightCharFontColor
			if (type.flags & StDBType.WDBF_TYPE_LIGHTCHARFONTCOLOR) == StDBType.WDBF_TYPE_LIGHTCHARFONTCOLOR {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				type.fontColorLightChar = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 0))
			}
			
			// Read SubFontColor
			if (type.flags & StDBType.WDBF_TYPE_SUBFONTCOLOR) == StDBType.WDBF_TYPE_SUBFONTCOLOR {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				type.fontColorSub = Int(try getInt32FromNSDataBuffer(buffer: Buffer, start: 0))
			}
			
			
			dbTypes.append(type)
		}
		
		for t in dbTypes {
			print(t.name)
		}
		// Read Waypoints
		
		for i in 0..<waypointCount {
			
			var waypoint :StDBWaypoint = StDBWaypoint()
			//First 42 Bytes
			Buffer = fileHandle.handle.readData(ofLength: 42) as NSData
			waypoint.GUID = try readGuid(from: Buffer, start: 0)
			waypoint.timeCreatedUTC = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 16)
			waypoint.timeModifiedUTC = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 20)
			let latitude = try getDoubleFromNSDataBuffer(buffer: Buffer, start: 24)
			let longitude = try getDoubleFromNSDataBuffer(buffer: Buffer, start: 32)
			waypoint.position = Double2(x: latitude, y: longitude)
			waypoint.flags = Int(try getUShortFromNSDataBuffer(buffer: Buffer, start: 40))
			
			// Read Name
			Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
			strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
			string = ""
			if(strL > 0){
				Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
				string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
			}
			waypoint.name = string
			//print("Waypoint (", i, ")"," NAME: ",waypoint.name, "Position: ",waypoint.getLatitude()," - ",waypoint.getLongitude())
			// Read Height
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_HEIGHT) == StDBWaypoint.WDBF_WAYPOINT_HEIGHT {
				Buffer = fileHandle.handle.readData(ofLength: 8) as NSData
				waypoint.height = try getDoubleFromNSDataBuffer(buffer: Buffer, start: 0)
			}
			
			// Read SubName
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_SUBNAME) == StDBWaypoint.WDBF_WAYPOINT_SUBNAME {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
				string = ""
				if(strL > 0){
					Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
					string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
				}
				waypoint.subName = string
			}
			
			// Read TextOrigin
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_NAMEORIGIN) == StDBWaypoint.WDBF_WAYPOINT_NAMEORIGIN {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				waypoint.nameOrigin = Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
			}
			
			// Read Waypoint Link Radius
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_LINKRADIUS) == StDBWaypoint.WDBF_WAYPOINT_LINKRADIUS {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				
			}
			
			// Read Precision Undefined
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_PRECISION) == StDBWaypoint.WDBF_WAYPOINT_PRECISION {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				
			}
			
			// Read Angle
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_ANGLE) == StDBWaypoint.WDBF_WAYPOINT_ANGLE {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				waypoint.angle = try getFloatFromNSDataBuffer(buffer: Buffer, start: 0)
			}
			
			// Read Proximity
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_PROXIMITY) == StDBWaypoint.WDBF_WAYPOINT_PROXIMITY {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				
			}
			
			// Read Waypoint Scale
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_SCALE) == StDBWaypoint.WDBF_WAYPOINT_SCALE {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				waypoint.scale = try getFloatFromNSDataBuffer(buffer: Buffer, start: 0)
			}
			
			// Read Light Char
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_LIGHTCHAR) == StDBWaypoint.WDBF_WAYPOINT_LIGHTCHAR {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
				string = ""
				if(strL > 0){
					Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
					string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
				}
				waypoint.lightChar = string
			}
			
			// Read Waypoint Text
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_TEXT) == StDBWaypoint.WDBF_WAYPOINT_TEXT {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				strL = 2 * Int(try getUInt32FromNSDataBuffer(buffer: Buffer, start: 0))
				string = ""
				if(strL > 0){
					Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
					string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
				}
			}
			
			// Read Type Ref
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_TYPEINDEX) == StDBWaypoint.WDBF_WAYPOINT_TYPEINDEX {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				let typeIndex = Int(try getUInt32FromNSDataBuffer(buffer: Buffer, start: 0))
				waypoint.type = dbTypes[typeIndex]
			}
			
			// Read Type Ref
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_TYPEREF) == StDBWaypoint.WDBF_WAYPOINT_TYPEREF {
				Buffer = fileHandle.handle.readData(ofLength: 16) as NSData
				let typeGuid = try readGuid(from: Buffer, start: 0)
				for t in dbTypes {
					if(try t.guid!.equals(guid: typeGuid)){
						waypoint.type = t
					}
				}
			}

			// Read Links
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_LINKCOUNT) == StDBWaypoint.WDBF_WAYPOINT_LINKCOUNT {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				let linkCount = try getUByteFromNSDataBuffer(buffer: Buffer, start: 0)
				for _ in 0..<linkCount {
					
					// Read Description
					var link = StDBWaypoint.WpLink()
					Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
					strL = 2 * Int(try getUInt32FromNSDataBuffer(buffer: Buffer, start: 0))
					string = ""
					if(strL > 0){
						Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
						string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
					}
					link.description = string
					
					// Read Link
					Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
					strL = 2 * Int(try getUInt32FromNSDataBuffer(buffer: Buffer, start: 0))
					string = ""
					if(strL > 0){
						Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
						string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
					}
					link.url = string
					waypoint.links.append(link)
				}
			}
			
			// Read Audible Index
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_LINKAUDIBLE) == StDBWaypoint.WDBF_WAYPOINT_LINKAUDIBLE {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
				
				
			}
			
			// Read Visible Index
			if (waypoint.flags & StDBWaypoint.WDBF_WAYPOINT_LINKVISIBLE) == StDBWaypoint.WDBF_WAYPOINT_LINKVISIBLE {
				Buffer = fileHandle.handle.readData(ofLength: 1) as NSData

			}
			
			
			dbWaypoints.append(waypoint)
		}
		
		// Waypoint Groups
		
		for i in 0..<groupCount {
			var waypointGroup = StDBWaypointGroup()
			
			//First 32 Bytes
			Buffer = fileHandle.handle.readData(ofLength: 32) as NSData
			waypointGroup.GUID = try readGuid(from: Buffer, start: 0)
			waypointGroup.timeUTCModified = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 16)
			waypointGroup.timeUTCValidated = try getDate2000FromNSDataBuffer(buffer: Buffer, start: 20)
			waypointGroup.waypointIndexCount = try getInt32FromNSDataBuffer(buffer: Buffer, start: 24)
			waypointGroup.waypointRefCount = try getInt32FromNSDataBuffer(buffer: Buffer, start: 28)
			
			//Read Name
			Buffer = fileHandle.handle.readData(ofLength: 1) as NSData
			strL = 2 * Int(try getUByteFromNSDataBuffer(buffer: Buffer, start: 0))
			string = ""
			if(strL > 0){
				Buffer = fileHandle.handle.readData(ofLength: strL) as NSData
				string = try getLStringFromNSDataBuffer(buffer: Buffer, position: 0, length: strL)
			}
			waypointGroup.name = string
			
			// Read waypoints in group by Index
			for _ in 0..<waypointGroup.waypointIndexCount {
				Buffer = fileHandle.handle.readData(ofLength: 4) as NSData
				let wpIndex = (Int)(try getInt32FromNSDataBuffer(buffer: Buffer, start: 0))
				let wp :StDBWaypoint = dbWaypoints[wpIndex]
				waypointGroup.addWaypoint(waypoint: wp)
			}
			
			//Read waypoints in group by Ref
			for _ in 0..<waypointGroup.waypointRefCount {
				Buffer = fileHandle.handle.readData(ofLength: 16) as NSData
				let guid :StUUID = try readGuid(from: Buffer, start: 0)
				for wp in dbWaypoints {
					if(wp.GUID === guid){
						waypointGroup.addWaypoint(waypoint: wp)
						break
					}
				}
			}
			print("Waypoint group: ",waypointGroup.name, waypointGroup.waypointList.count)
			dbWaypointGroups.append(waypointGroup)
		}
		
		
		if errorCode != .ER_OK {
			print(String(describing: type(of: errorCode)))
			return
		} else {
			//print("Header complete!")
		}
		
		// catch file not found
		// catch other exceptions
		
	}
	
	class StDBFileHandle {
		var handle :FileHandle
		var lock :Bool = false
		
		init(_ handle :FileHandle){
			self.handle = handle
		}
		
		static func getHandle(_ handle :FileHandle?) -> StDBFileHandle? {
			if let handle = handle {
				return StDBFileHandle(handle)
			} else {
				return nil
			}
		}
	}
}
