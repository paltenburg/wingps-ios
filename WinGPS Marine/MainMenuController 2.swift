//
//  MainMenuController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 19/01/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class MainMenuController: UIViewController {
    
    static let UNWIND_TO_MAPVIEW =  "unwindSegueToMapView"
    static let UNWIND_TO_SETTINGS = "unwindSegueToSettings"
    static let UNWIND_TO_ACCOUNT = "unwindSegueToAccount"
    var unwindIdentifier = UNWIND_TO_MAPVIEW
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var mainMenuContainer: UIView!
    
    @IBOutlet weak var accountMenuItemView: UIView!
    @IBOutlet weak var routeManagerMenuItemView: UIView!
    @IBOutlet weak var aisTargetListMenuItemView: UIView!
    
    @IBOutlet weak var stopSessionMenuItemView: UIView!
    @IBOutlet weak var stopTrackingMenuItemView: UIView!
    
    static var instance :MainMenuController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MainMenuController.instance = self
        
        mainMenuContainer.layer.cornerRadius = 8
        mainMenuContainer.clipsToBounds = true
        
        switch Resources.appType {
            
        case .plus:
            accountMenuItemView.isHidden = false
            routeManagerMenuItemView.isHidden = false
            aisTargetListMenuItemView.isHidden = false
            
            //            stopSessionMenuItemView.isHidden = false
        //            stopTrackingMenuItemView.isHidden = true
            
        case .lite, .dkw1800, .friesemeren:
            accountMenuItemView.isHidden = false
            routeManagerMenuItemView.isHidden = true
            aisTargetListMenuItemView.isHidden = true
            
            //            stopSessionMenuItemView.isHidden = false
        //            stopTrackingMenuItemView.isHidden = true
//
//        case .friesemeren:
//            accountMenuItemView.isHidden = false
//            routeManagerMenuItemView.isHidden = true
//            aisTargetListMenuItemView.isHidden = true
        }
        
        stopSessionMenuItemView.isHidden = true
        if ViewController.instance?.mSettingsShowTailTrack ?? false {
            stopTrackingMenuItemView.isHidden = false
            setTrackingButtonLabels()
        } else {
            stopTrackingMenuItemView.isHidden = true
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //		// add fade animation for when closing
        //		let transition: CATransition = CATransition()
        //		transition.duration = 0.3
        //		transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        //		transition.type = CATransitionType.fade
        //		self.view.window!.layer.add(transition, forKey: nil)
        
    }
    
    @IBAction func settingsButton(_ sender: Any) {
        unwindIdentifier = MainMenuController.UNWIND_TO_SETTINGS
        
        // add fade animation for when closing
        let transition: CATransition = CATransition()
        transition.duration = 0.1
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: nil)
        
        self.dismiss(animated: false, completion: nil)
        
        performSegue(withIdentifier: unwindIdentifier, sender: self)
        
    }
    
    @IBAction func accountButton(_ sender: Any) {
        unwindIdentifier = MainMenuController.UNWIND_TO_ACCOUNT
        
        // add fade animation for when closing
        let transition: CATransition = CATransition()
        transition.duration = 0.1
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: nil)
        
        self.dismiss(animated: false, completion: nil)
        
        performSegue(withIdentifier: unwindIdentifier, sender: self)
    }
    
    @IBAction func aboutHelpButton(_ sender: Any) {
        //        UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/wingps-marine-ios/wingps-marine")! as URL)
        UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com/wingps-marine-ios/dkw-1800-serie-voor-ios")! as URL)
    }
    
    @IBAction func exitButton(_ sender: UIButton) {
        
        self.dismiss(animated: false, completion: nil)
        
        ViewController.instance?.exitButtonPressed()
        
    }
    
    
    @IBOutlet weak var stopTrackingIcon: UIButton!
    @IBOutlet weak var continueTrackingIcon: UIButton!
    @IBOutlet weak var trackingButtonTextOutlet: UIButton!
    @IBAction func toggleTracking(_ sender: UIButton) {
        
        if let vc = ViewController.instance {
            if vc.mSettingsShowTailTrack {
                if TrackingService.service.paused || TrackingService.service.runningTrack == nil {
                    TrackingService.service.start()
                } else {
//                    TrackingService.service.pause()
                    TrackingService.service.stop()
                }
                setTrackingButtonLabels()
            }
        }
    }
    
    func setTrackingButtonLabels(){
        if TrackingService.service.paused || TrackingService.service.runningTrack == nil {
            stopTrackingIcon.isHidden = true
            continueTrackingIcon.isHidden = false
            trackingButtonTextOutlet.setTitle("continue_tracking".localized, for: [])
        } else {
            stopTrackingIcon.isHidden = false
            continueTrackingIcon.isHidden = true
            trackingButtonTextOutlet.setTitle("stop_tracking".localized, for: [])
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        closeMenu()
    }
    
    func closeMenu(){
        
        // add fade animation for when closing
        let transition: CATransition = CATransition()
        transition.duration = 0.1
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: nil)
        
        
        performSegue(withIdentifier: unwindIdentifier, sender: self)
        self.dismiss(animated: false, completion: nil)
    }
    
    /*override func viewDidDisappear(_ animated: Bool){
     super.viewDidDisappear(animated)
     
     performSegue(withIdentifier: unwindIdentifier, sender: self)
     
     }*/
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if (touch?.view == backgroundView){
            closeMenu()
        }
    }
    
    @IBAction func stentecLinkAction(_ sender: UIButton) {
        UIApplication.shared.openURL(NSURL(string: "https://www.stentec.com")! as URL)
    }
}
