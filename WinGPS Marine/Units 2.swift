//
//  Units.swift
//  WinGPS Marine
//
//  Created by Standaard on 24/10/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation

let Units = UnitsClass.instance

class UnitsClass {

	static let instance = UnitsClass()
	
	var mSogUnit = ""
	var mUnitDistSmall :String = Resources.SETTINGS_UNIT_DISTANCE_SMALL_DEFAULT
	var mUnitDistLarge :String = Resources.SETTINGS_UNIT_DISTANCE_LARGE_DEFAULT
	var mSpeedConversion : Float = 1

	private init(){
	}
}
