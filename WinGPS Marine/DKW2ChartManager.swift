//
//  DKW2ChartManager.swift
//  MapDemo
//
//  Created by Standaard on 06/12/2017.
//  Copyright © 2017 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DKW2ChartManager {
	
	class ChartLoadRecord {
		//		var filename = String()
		var filename :URL
		var replace = Bool()
		var user = Bool()
		var loadError = ErrorCode.ER_OK
		
		init(filename :URL, replace :Bool, user :Bool) {
			self.filename = filename
			self.replace = replace
			self.user = user
		}
	}
	
	// region defined by multiple overlapping rectangular shapes
	class Region {
		private var rects = [CGRect]()
		
		func add(_ rect :CGRect){
			var foundOverlapping = false
			var i = 0
			while i < rects.count {
				let oldRect = rects[i]
				
				// check if rect is not overlapped by one of the existing rects
				if oldRect.contains(rect) {
					foundOverlapping = true
					break
				}
				
				// check if rect is overlapping one of the existing rects
				if rect.contains(oldRect) {
					rects.remove(at: i)
				} else {
					i += 1
				}
			}
			if !foundOverlapping {
				rects.append(rect)
			}
		}
		
		func contains(pointDeg :CGPoint) -> Bool{
			for rect in rects {
				if rect.contains(pointDeg){
					return true
				}
			}
			return false
		}
		
		func setEmpty(){
			rects.removeAll()
		}
	}
	
	//	// singleton to return the same chart manager everytime
	static var chartManager :DKW2ChartManager?
	
	//	private static final int LIDLICENSECHECK = 0x10000;
	//
	//	public static final int DKW2CHARTMANAGER_SAVEVISIBLE = 9999;
	//	public static final double DKW2CHART_FILLEDAREA100 = 0.9999999;
	//
	static var CHARTLOADING_CHARTS :Int = 1
	//	public static final int CHARTLOADING_VISIBILITY = 2;
	//	public static final int CHARTLOADING_ORDER = 3;
	
	static let CHARTMANAGER_PIDTOSHOW :String = "CHARTMANAGER_PIDTOSHOW"
	static let CHARTMANAGER_MODTOSHOW :String = "CHARTMANAGER_MODTOSHOW"
	//	public static final String CHARTMANAGER_SHAREDPREFS = "Stentec.Navigation.ChartManager";
	
	var mActDB :StActManager
	//	private int mAllowedWaterbodyMask;
	//	var mChartCollectionManager :DKW2ChartCollectionManager
	
	var mCharts :[DKW2ChartDisplayInfo]
	
	var mChartSets :[DKW2ChartSetInfo]
	var mChartsLoader :ChartsLoader?
	var mChartsRegion = Region()
	
	var mChartWithBorder :DKW2ChartDisplayInfo? = nil
	var viewerCenterScale = Double(0)
	
	var mChartVisibilityChanged :Bool
	var mLoading :Bool
	var mLoadingState :Int
	
	init() {
		
		mCharts = [DKW2ChartDisplayInfo]()
		mChartsRegion = Region()
		mChartSets = [DKW2ChartSetInfo]()
		mActDB = StActManager.getActManager()
		mChartWithBorder = nil
		//		mChartCollectionManager = StDKW2ChartCollectionManager()
		mChartsLoader = nil
		mChartVisibilityChanged = false
		mLoading = false
		mLoadingState = DKW2ChartManager.CHARTLOADING_CHARTS
		//mSharedPreferences = null;
		//		updateChartsWaterbodyMask()
		
		//		var state :String = Environment.getExternalStorageState()
		//		if (!Environment.MEDIA_MOUNTED.equals(state)) {
		//			return;
		//		}
	}
	
	//	/**
	//	* Removes all charts from the chart manager, leaving it empty.
	//	*/
	//	public void clearCharts() {
	//	while (mCharts.size() > 0)
	//	mCharts.remove(0);
	//	}
	
	/**
	* Checks if the chart manager contains a chart with the specified file name.
	*
	* @param aChartFile The file of the chart to find.
	* @param aIndex If the chart is found, this will hold the index of the chart in the manager.
	* @return True, if the chart is found.
	*/
	//	func containsChart(StDocumentFile aChartFile, MutableInteger aIndex) -> Bool {
	func containsChart(aChartFile :URL, aIndex :inout Int) -> Bool {
		for i in 0 ..< mCharts.count {
			var Chart :DKW2ChartDisplayInfo = mCharts[i]
			// if getLastSegmentPath is sufficient
			if Chart.calibratedChart.dKW2Chart!.fileName!.path == aChartFile.path {
				aIndex = i
				return true
			}
		}
		return false
	}
	
	func updateChartSets() {
		mChartSets.removeAll()
		var found : Bool
		var defaultTransparency : Bool
		
		for chartObject in mCharts {
			var tag : DKW2Tag = (chartObject.calibratedChart.dKW2Chart?.tag)!
			found = false
			for chartSetObject in mChartSets {
				if (chartSetObject.productID) == Int64(tag.productID) &&
					(chartSetObject.moduleBitIndex)! == Int64(tag.moduleBitIndex) &&
					(chartSetObject.mName == tag.moduleName){
					
					found = true
					chartSetObject.chartCount! += 1
					if chartObject.getChartInViewer() {
						chartSetObject.chartVisCount! += 1
					}
					
					chartObject.transparentColor = UInt32(chartSetObject.transparentColor!)
					chartObject.useTransparentColor = chartSetObject.useTransparentColor!
					chartSetObject.charts.append(chartObject)
                    
					break
				}
			}
			if !found {
				var csi : DKW2ChartSetInfo = DKW2ChartSetInfo()
				csi.chartCount = 1
				csi.chartVisCount = (chartObject.getChartInViewer()) ? 1 : 0
				csi.productID = Int64(tag.productID)
				csi.moduleBitIndex = Int(tag.moduleBitIndex)
				csi.pName = tag.productName
				csi.mName = tag.moduleName
				csi.visible = chartObject.getModuleInViewer()
				
				defaultTransparency = false
				if(csi.productID == 196660 ||
					csi.productID == 196673 ||
					csi.productID == 196681 ||
					csi.productID == 196690 ||
					csi.productID == 196696 ||
					csi.productID == 196702) {
					defaultTransparency = (tag.moduleNameShort == "BE") || (tag.moduleNameShort == "NF") || (tag.moduleNameShort == "NWD") || (tag.moduleNameShort == "ZWD")
				} else if(
					csi.productID == 196651 ||
						csi.productID == 196657 ||
						csi.productID == 196659 ||
						csi.productID == 196675 ||
						csi.productID == 196678 ||
						csi.productID == 196684 ||
						csi.productID == 196693 ||
						csi.productID == 196699 ||
						csi.productID == 196705) {
					defaultTransparency = true
				}
				
				csi.transparentColor = 0xFFFFFF
				csi.useTransparentColor = defaultTransparency
				chartObject.transparentColor = 0xFFFFFFFF
				chartObject.useTransparentColor = defaultTransparency
				csi.charts.append(chartObject)
				mChartSets.append(csi)
			}
		}
        
        // sort sets
        mChartSets = mChartSets.sorted(by: {
            ($0.mName ?? "") < ($1.mName ?? "")
        })
        for csi in mChartSets {
            csi.charts = csi.charts.sorted(by: {
                $0.calibratedChart.dKW2Chart!.tag.chartNumber <
                $1.calibratedChart.dKW2Chart!.tag.chartNumber
            })
        }
	}
	
	//	/**
	//	* Deletes chart files that could not be read.
	//	*/
	//	public void deleteChartsWithLoadError() {
	//	if (mChartsLoader != null && mChartsLoader.mErrorsFound) {
	//	for (int i = 0; i < mChartsLoader.mLoadList.size(); i++) {
	//	if (mChartsLoader.mLoadList.get(i).LoadError.getErrorCode() != StError.ER_OK && mChartsLoader.mLoadList.get(i).LoadError.getErrorCode() != StError.ER_FILENOTFOUND)
	//	mChartsLoader.mLoadList.get(i).Filename.delete();
	//	}
	//	}
	//	}
	
	/**
	* Finds the most detailed and enabled chart at the specified position.
	*
	* @param aPos The position. X = longitude, Y = latitude in radians.
	* @return A StDKW2ChartDisplayInfo object of the most detailed chart or null if no chart could be found.
	*/
	func findDetailChartAtPos(aPos :CLLocationCoordinate2D) -> DKW2ChartDisplayInfo? {
		var bestScale = 3.4E38
		var result :DKW2ChartDisplayInfo? = nil
		for chart in mCharts {
//			let chart :DKW2ChartDisplayInfo = mCharts[i]
			if chart.getInViewer() && chart.calibratedChart.posInChart(aPos: aPos, aIncludeBorder: !chart.hideBorder) {
				//          Double2 Pix = Chart.getChart().getCalibration().geoWGS84ToPix(aPos);
				let scale = chart.calibratedChart.centerScale
				if result == nil || scale! < bestScale {
					result = chart
					bestScale = scale!
				}
			}
		}
		return result
	}
	
//		public StDKW2ChartDisplayInfo findTopChartInViewerAtPos(final Geo2 aPos) {
//		for (int I = mCharts.size() - 1; I >= 1; I--) {
//		StDKW2ChartDisplayInfo Chart = mCharts.get(I);
//		if (Chart.getInViewer() && Chart.getChart().posInChart(aPos, !Chart.getHideBorder()))
//		return Chart;
//		}
//		return null;
//		}
	
//		public int getAllowedWaterbodyMask() {
//		return mAllowedWaterbodyMask;
//		}
    
    func getChartByGUID(_ GUID :StUUID?) -> DKW2ChartDisplayInfo? {
        for chart in mCharts {
            if chart.calibratedChart.dKW2Chart!.tag.guid === GUID {
                return chart
            }
        }
        return nil
    }
	
	/**
	* Gets (and creates if it does not exist) the chart manager.
	*
	* @return The StDKW2ChartManager object.
	*/
	// TODO: should be synchronized
	static func getChartManager() -> DKW2ChartManager {
		if let chartManager = chartManager {
			return chartManager
		} else {
			self.chartManager = DKW2ChartManager()
			return chartManager!
		}
	}
	
	/**
	* Gets and refreshes (including loading any new charts) the chart manager.
	*
	* @param aDBHelper The database helper object to which chart settings are saved.
	* @param aMainHandler The handler which receives a message whenever the chart load state changes.
	* @param aPid The productid of the charts the chart manager will load. Use -1 for all productids.
	* @param aMod The modulemask of the charts the chart manager will load. Use 999 for all modules.
	* @return The StDKW2ChartManager object.
	*/
	//	public static synchronized StDKW2ChartManager getChartManagerRefresh(StDKW2ChartDatabaseHelper aDBHelper, Handler aMainHandler, int aPid, int aMod) {
	//		return getChartManagerRefresh(aDBHelper, aMainHandler, aPid, aMod, 0, 0, false);
	//		}
	
	/**
	* Gets and refreshes (including loading any new charts) the chart manager. This function can also be used to enable
	* a specific chartset after loading.
	*
	* @param aDBHelper The database helper object to which charts settings are saved.
	* @param aMainHandler The handler which receives a message whenever the chart load state changes.
	* @param aPid The productid of the charts the chart manager will load. Use -1 for all productids.
	* @param aMod The modulemask of the charts the chart manager will load. Use 999 for all modules.
	* @param aPidShow The productid of the chart set that will be enabled after loading.
	* @param aModShow The modulemask of the chart set that will be enabled after loading.
	* @param aSaveVisibility If true, visibility of still present charts after a refresh is saved. This will remove the visibility setting of removed charts.
	* @return The StDKW2ChartManager object.
	*/
	static func getChartManagerRefresh(
		//		aDBHelper :StDKW2ChartDatabaseHelper,
		//		aMainHandler :Handler,
		aPid :Int,
		aMod :Int,
		aPidShow :Int,
		aModShow :Int,
		aSaveVisibility :Bool
		) -> DKW2ChartManager {
		//	static func getChartManagerRefresh() -> DKW2ChartManager {
		let chartManager = getChartManager()
		
		chartManager.refresh(/*aDBHelper, aMainHandler,*/ aPid: aPid, aMod: aMod, aPidShow: aPidShow, aModShow: aModShow, aSaveVisibility: aSaveVisibility)
		
		return chartManager
	}
	
	
	//	public StDKW2CalibratedChart getChart(int Index) {
	//	return mCharts.get(Index).getChart();
	//	}
	
	func getChartBorderVisible() -> DKW2ChartDisplayInfo? {
		return mChartWithBorder
    }
	
	//	public int getChartCount() {
	//	return mCharts.size();
	//	}
	//
	//	public long getChartFileDate(int Index) {
	//	StDKW2Chart chart = mCharts.get(Index).getChart().getDKW2Chart();
	//	StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
	//	StDocumentFile path = DKWSettings.getDKW2ChartsDir();
	//	StDocumentFile chartFile = new StDocumentFile(path, chart.getFileName().toString());
	//	return chartFile.lastModified();
	//	}
	//
	//	public ArrayList<StDKW2ChartDisplayInfo> getCharts() {
	//	return mCharts;
	//	}
	//
	//	public int getChartsEnabledCount() {
	//	int Result = 0;
	//	for (StDKW2ChartDisplayInfo cInfo : mCharts) {
	//	if (cInfo.getInViewer())
	//	Result++;
	//	}
	//	return Result;
	//	}
	//
	//	public DKW2ChartSetInfo getChartSet(long aProductID, int aModule) {
	//	for (DKW2ChartSetInfo sInfo : mChartSets) {
	//	int Module = 1 << sInfo.moduleBitIndex;
	//	if (sInfo.productID == aProductID && ((Module & aModule) == Module))
	//	return sInfo;
	//	}
	//	return null;
	//	}
	//
	//	public ArrayList<DKW2ChartSetInfo> getChartSets() {
	//	return mChartSets;
	//	}
	//
	//	public Region getChartsRegion() {
	//	return mChartsRegion;
	//	}
	
	func getChartVisibilityChanged() -> Bool {
		return mChartVisibilityChanged
	}
	
	//	public StDKW2ChartDisplayInfo getFirstVisibleChart() {
	//	for (StDKW2ChartDisplayInfo cInfo : mCharts) {
	//	if (cInfo.getModuleInViewer() && cInfo.getInViewer())
	//	return cInfo;
	//	}
	//	return null;
	//	}
	//
	//	public StDKW2ChartDisplayInfo getFirstVisibleChart(long aProductID, int aModule) {
	//	for (StDKW2ChartDisplayInfo cInfo : mCharts) {
	//	StDKW2Tag tag = cInfo.getChart().getDKW2Chart().getTag();
	//	int Module = 1 << tag.getModuleBitIndex();
	//	if (tag.getProductID() == aProductID && (Module & aModule) == Module && cInfo.getInViewer())
	//	return cInfo;
	//	}
	//	return null;
	//	}
	//
	//	public boolean getIsLoading() {
	//	return mLoading;
	//	}
	//
	//	public Date getLastUpdateDate() {
	//	long ChartDate;
	//	long Result = 0;
	//	for (int i = 0; i < mCharts.size(); i++) {
	//	StDKW2Chart Chart = mCharts.get(i).getChart().getDKW2Chart();
	//	ChartDate = Chart.getUpdateTag().getTimeUTCUpdate().getTime();
	//	if (ChartDate > Result)
	//	Result = ChartDate;
	//	}
	//	return new Date(Result);
	//	}
	//
	//	public int getLoadingState() {
	//	return mLoadingState;
	//	}
	//
	//	public boolean getTileHiddenByChart(RectangleD TileBnds, StGeodeticCalibration TileCal, StDKW2CalibratedChart Chart) {
	//	StDKW2BoundsTag Bounds = Chart.getDKW2Chart().getBoundsTag();
	//	StGeodeticCalibration Cal = Chart.getCalibration();
	//
	//	// Check upper left
	//	Double2 P = TileCal.pixTransform(Cal, TileBnds.getMin());
	//	if (!Bounds.pointInPolygon(P))
	//	return false;
	//
	//	// Check bottom right
	//	P = TileCal.pixTransform(Cal, TileBnds.getMax());
	//	if (!Bounds.pointInPolygon(P))
	//	return false;
	//
	//	// Check upper right
	//	P = TileCal.pixTransform(Cal, Double2.makeDouble2(TileBnds.XMax, TileBnds.YMin));
	//	if (!Bounds.pointInPolygon(P))
	//	return false;
	//
	//	// Check bottom left
	//	P = TileCal.pixTransform(Cal, Double2.makeDouble2(TileBnds.XMin, TileBnds.YMax));
	//	return Bounds.pointInPolygon(P);
	//
	//	}
	
	
	//	func getVisibleCharts(final GreatCirclePoint ViewerCenter, double ViewerRadius, double CenterScale, final StGeodeticCalibration Calibration, final StGeodeticCalibration SecondCalibration, ArrayList<StDKW2ChartDisplayInfo> VisibleCharts) {
	func getVisibleCharts(centerScale :Double, calibration :GeodeticCalibration, visibleCharts :inout [DKW2ChartDisplayInfo]) {
		visibleCharts.removeAll()
		var zoom = Double()
		
		// if there is a chart with a visible chart border, only add that chart and return
		if let chartWithBorder = mChartWithBorder {
			if chartWithBorder.calibratedChart.isVisible(viewerCalibration: calibration, zoom: &zoom) {
				chartWithBorder.zoom = zoom
				chartWithBorder.enabled = true
				visibleCharts.append(chartWithBorder)
			}
			return
		}
		
		for i in 0 ..< mCharts.count {
			var CDI :DKW2ChartDisplayInfo = mCharts[i]
			if CDI.getInViewer() && CDI.calibratedChart.isVisible(viewerCalibration: calibration, zoom: &zoom) {
				CDI.zoom = zoom
				visibleCharts.append(CDI)
			}
		}
		
		viewerCenterScale = centerScale
		visibleCharts.sort(by: chartListCompare)
		
		//    for (int i = VisibleCharts.size() - 1; i >= 0; i--) {
		for i in 0 ..< visibleCharts.count {
			var CDI :DKW2ChartDisplayInfo = visibleCharts[i]
			CDI.enabled = true
			for ii in (0 ..< CDI.chartsHidden.count).reversed() {
				CDI.chartsHidden[ii]!.enabled = false
			}
		}
		
		/*
		int L = VisibleCharts.size();
		if (L > 1) {
		int I = 0;
		while (I < L && (VisibleCharts.get(I).getFilledArea() < DKW2CHART_FILLEDAREA100 ||
		(VisibleCharts.get(I).getFilledArea() >= DKW2CHART_FILLEDAREA100 && VisibleCharts.get(I).getHasHole())))
		I++;
		
		if (L - 1 >= I + 1)
		for (int J = L - 1; J >= I + 1; J--)
		VisibleCharts.get(J).setEnabled(false);
		}*/
	}
	
	//	public boolean hasChartPStr(final String aPStr) {
	//	for (StDKW2ChartDisplayInfo Chart : mCharts) {
	//	StDKW2Chart c = Chart.getChart().getDKW2Chart();
	//	if (c.getTag().getProductStr().equals(aPStr))
	//	return true;
	//	}
	//	return false;
	//	}
	//
	//	public void importChartCollections(int aPid, int aMod) {
	//
	//	StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
	//	StDocumentFile path = DKWSettings.getDKW2ChartsDir();
	//
	//	ArrayList<String> mImpCols = new ArrayList<>();
	//	DKWSettings.getChartColsImported(mImpCols);
	//
	//	ArrayList<ChartProduct> CPs = new ArrayList<>();
	//	if (aPid == -1 && aMod == 999) {
	//	int PrCount = mActDB.getActCodeCount();
	//
	//	for (int i = 0; i < PrCount; i++) {
	//	ChartProduct CP = new ChartProduct();
	//	mActDB.getActCode(i, CP);
	//	CPs.add(CP);
	//	}
	//	} else {
	//	ChartProduct CP = new ChartProduct();
	//	CP.Mod = aMod;
	//	CP.Pid = aPid;
	//	CPs.add(CP);
	//	}
	//
	//	String filename, extension;
	//	StDocumentFile ccol;
	//	for (int i = 0; i < CPs.size(); i++) {
	//	ChartProduct CP = CPs.get(i);
	//
	//	int ChartCount = mActDB.getFileNameCount(CP.Pid, CP.Mod);
	//	for (int j = 0; j < ChartCount; j++) {
	//	filename = mActDB.getFileName(CP.Pid, CP.Mod, j);
	//	extension = filename.substring(filename.lastIndexOf(".") + 1);
	//	if (!extension.equalsIgnoreCase("collection"))
	//	continue;
	//
	//	ccol = new StDocumentFile(path, filename);
	//	if (!ccol.exists())
	//	continue;
	//
	//	mImpCols.remove(filename);
	//
	//	if (!DKWSettings.getChartColImported(filename)) {
	//	StError Error = new StError();
	//	if (mChartCollectionManager.importCollection(ccol, CP.Pid, CP.Mod, Error)) {
	//	DKWSettings.addChartColImported(filename);
	//	ArrayList<UUID> GUIDs = mChartCollectionManager.getCollection(CP.Pid, CP.Mod);
	//	if (GUIDs != null)
	//	updateChartsVisible(CP.Pid, CP.Mod, GUIDs);
	//	}
	//	}
	//	}
	//	}
	//	for (int i = 0; i < mImpCols.size(); i++)
	//	DKWSettings.removeChartColImported(mImpCols.get(i));
	//
	//	}
	
	func inChartsRegion(aGPDeg :Geo2) -> Bool {
		return mChartsRegion.contains(pointDeg: CGPoint(x: aGPDeg.lon, y: aGPDeg.lat))
	}
	
	//	public void getChartsInChartsDir() {
	//	StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
	//	StDocumentFile path = DKWSettings.getDKW2ChartsDir();
	//
	//	if (!path.exists())
	//	return;
	//
	//	StDocumentFile[] filesList = path.listFiles();
	//	String extension;
	//	MutableInteger CIndex = new MutableInteger();
	//	StDocumentFile chart;
	//	StError Error = new StError();
	//
	//	ArrayList<ChartProduct> CPs = new ArrayList<ChartProduct>();
	//
	//	for (StDocumentFile df : filesList) {
	//	String fn = df.getName();
	//	extension = fn.substring(fn.lastIndexOf(".") + 1);
	//	if (!extension.equalsIgnoreCase("dkw2"))
	//	continue;
	//
	//	chart = new StDocumentFile(path, fn);
	//
	//	StDKW2Chart DKW2Chart = new StDKW2Chart(chart, null, 0, Error);
	//	if (Error.getErrorCode() != StError.ER_OK)
	//	continue;
	//
	//	boolean found = false;
	//	for (int i = 0; i < CPs.size(); i++) {
	//	ChartProduct CP = CPs.get(i);
	//	if (CP.Pid == DKW2Chart.getTag().getProductID() && CP.Mod == (1 << DKW2Chart.getTag().getModuleBitIndex())) {
	//	found = true;
	//	break;
	//	}
	//	}
	//	if (!found) {
	//	ChartProduct CP = new ChartProduct();
	//	CP.Pid = (int)DKW2Chart.getTag().getProductID();
	//	CP.Mod = (1 << DKW2Chart.getTag().getModuleBitIndex());
	//	CPs.add(CP);
	//	}
	//	}
	//	}
	
	
	//		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	func refresh(
		//		StDKW2ChartDatabaseHelper aDBHelper,
		//		Handler aMainHandler,
		aPid :Int,
		aMod :Int,
		aPidShow :Int,
		aModShow :Int,
		aSaveVisibility :Bool
		) {
		
		// if charts are loading at the moment do not start again.
		if mLoading {
			return
		}
		
		//    long Start = System.nanoTime();
		mLoading = true
		mLoadingState = DKW2ChartManager.CHARTLOADING_CHARTS
		
		//		StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
		//		StDocumentFile path = DKWSettings.getDKW2ChartsDir();
		//
		//		if (!path.exists()) {
		//		if (mCharts.size() > 0) {
		//		mCharts.clear();
		//		//          return true;
		//		} else {
		//		//          return false;
		//		}
		//		}
		
		for CDI in mCharts {
			CDI.setValidated(aValidated: false)
		}
		
		var updated :Bool = false
		
		var CPs = [StActManager.ChartProduct]()
		if (aPid == -1 && aMod == 999) {
			
			var PrCount :Int = mActDB.getProductCodeCount()
			
			for i in 0 ..< PrCount {
				var CP = StActManager.ChartProduct()
				mActDB.getActCode(aIndex: i, aCP: CP)
				CPs.append(CP)
			}
		} else {
			var CP = StActManager.ChartProduct()
			CP.Mod = aMod
			CP.Pid = aPid
			CPs.append(CP)
		}
		
		// check the sets for completeness
		var i :Int = 0
		while i < CPs.count {
			var CP = CPs[i]
			if !mActDB.getProductComplete(aPid: CP.Pid, aMod: CP.Mod) {
				CPs.remove(at: i)
			} else {
				i += 1
			}
		}
		
		var loadList = [ChartLoadRecord]()
		
		let loadAvailableFilesFromDir = false // Used for earlier versions
		if loadAvailableFilesFromDir {
			loadList = getDKW2FilesFromDirectory(updated: &updated)
		} else {
			
			var clr :ChartLoadRecord
			//    StError Error = new StError();
			var CDI :DKW2ChartDisplayInfo
			for i in 0 ..< CPs.count {
				
				var CP :StActManager.ChartProduct = CPs[i]
				
				var ChartCount :Int = mActDB.getFileNameCount(aPid: CP.Pid, aMod: CP.Mod)
				
				//				MutableInteger Index = new MutableInteger()
				var Index :Int = 0
				var filename :String, fileExtension :String
				//				StDocumentFile chart
				var chart: URL
				
				for j in 0 ..< ChartCount {
					filename = mActDB.getFileName(aPid: CP.Pid, aMod: CP.Mod, aIndex: j)
					
					if filename.count == 0 {
						continue
					}
					
					fileExtension = filename.substring(from: filename.index(after: filename.lastIndex(of: ".")!))
					
					if !(fileExtension.caseInsensitiveCompare("dkw2") == .orderedSame) {
						continue
					}
					
					//					chart = new StDocumentFile(path, filename);
					//					chart = URL(fileURLWithPath: Resources.DKW2ChartsDir + filename)
					chart = getDKW2Directory().appendingPathComponent(filename)
					
					if !FileManager.default.fileExists(atPath: chart.path) {
						continue
					}
					
					if containsChart(aChartFile: chart, aIndex: &Index) {
						CDI = mCharts[Index]
						// check if the chart has changed
						if (getFileSize(chart.path)! != UInt64(CDI.calibratedChart.dKW2Chart!.fileSize)) ||
							(getLastModified(chart.path) != CDI.calibratedChart.dKW2Chart!.fileTime!) {
							//						chart is different, so reload it and replace the original
							loadList.append(ChartLoadRecord(filename: chart, replace: true, user: false));
							updated = true;
							continue;
						} else {
							// chart hasn't changed so continue
							CDI.validated = true
							continue
						}
					}
					
					loadList.append(ChartLoadRecord(filename: chart, replace: false, user: false))
					updated = true;
				}
			}
		}
		
		/*
		
		if (aPid == -1 && aMod == 999) {
		
		// TODO: check the user folder
		StDocumentFile userpath = new StDocumentFile(DKWSettings.getDKW2ChartsDir(), "User");
		if (userpath.exists()) {
		StDocumentFile[] files = userpath.listFiles();
		String extension;
		MutableInteger CIndex = new MutableInteger();
		StDocumentFile chart;
		if (files != null) {
		for (StDocumentFile file : files) {
		String filename = file.getName();
		extension = filename.substring(filename.lastIndexOf(".") + 1);
		if (!extension.equalsIgnoreCase("dkw2"))
		continue;
		
		chart = file;
		
		if (containsChart(chart, CIndex)) {
		CDI = mCharts.get(CIndex.getValue());
		// check if the chart has changed
		if (chart.length() != CDI.getChart().getDKW2Chart().getFileSize() ||
		chart.lastModified() != CDI.getChart().getDKW2Chart().getFileLastModified()) {
		// chart is different, so reload it and replace the original
		clr = new ChartLoadRecord(chart, true, true);
		LoadList.add(clr);
		updated = true;
		continue;
		} else {
		// chart hasn't changed so continue
		CDI.setValidated(true);
		continue;
		}
		}
		
		clr = new ChartLoadRecord(chart, false, true);
		LoadList.add(clr);
		updated = true;
		}
		}
		}
		}
		*/
		
		var index = 0
		while index < mCharts.count {
			if !mCharts[index].getValidated() {
				mCharts.remove(at: index)
				updated = true
			} else {
				index += 1
			}
		}
		
		if updated {
			//		mChartsLoader = new ChartsLoader(aDBHelper, aMainHandler, LoadList, aPid, aMod, aSaveVisibility);
			mChartsLoader = ChartsLoader(chartManager: self, loadList: loadList)
			//		if (aPidShow != 0) {
			mChartsLoader!.execute()
			//		mChartsLoader.setChartsetToShow(aPidShow, aModShow);
			//		}
			//    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
			//		mChartsLoader.execute();
			//		else
			//		mChartsLoader.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
		} else {
			mLoading = false;
		}
		
		//    double Duration = (System.nanoTime() - Start) / 1000000000f;
		//    Log.d("ChartManager", "Chart refreshing took: " + String.valueOf(Duration) + " seconds");
		
		//    return updated;
	}
	
    func setChartBorderVisible(aChart :StUUID?) {
        if aChart == nil {
            for chart in mCharts{ //(StDKW2ChartDisplayInfo
                chart.setShowBorder(false)
            }
            mChartWithBorder = nil
        } else {
            if let chart = getChartByGUID(aChart) {
                chart.setShowBorder(true)
                mChartWithBorder = chart
            }
        }
        TileAndQueueCache.clearCache() // to force re-decoding tiles with right includeAlpha-setting
    }
	
	func setChartVisibilityChanged(aChanged :Bool) {
		mChartVisibilityChanged = aChanged
	}
	
	//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	//	public void updateChartsHiddenInTask(Handler aMainHandler) {
	//	mLoading = true;
	//	mLoadingState = CHARTLOADING_ORDER;
	//
	//	ChartsHiddenUpdater chu = new ChartsHiddenUpdater(aMainHandler);
	//	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
	//	chu.execute();
	//	else
	//	chu.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	//	}
	//	/*
	//	public void setSharedPreferences(SharedPreferences aSharedPreferences) {
	//	mSharedPreferences = aSharedPreferences;
	//	}
	//	*/
	//	public void updateChartsHidden() {
	//	/*
	//	for (int I = mCharts.size() - 1; I >= 1; I--) {
	//	StDKW2ChartDisplayInfo CDI1 = mCharts.get(I);
	//	CDI1.setHiddenCalculated(false);
	//	CDI1.setOverlapCalculated(false);
	//	CDI1.clearChartsHidden();
	//	CDI1.clearChartsOverlap();
	//	}
	//	*/
	//	//    MutableBoolean Hidden = new MutableBoolean(false);
	//	//    MutableBoolean Overlap = new MutableBoolean(false);
	//
	//	for (int I = mCharts.size() - 1; I >= 1; I--) {
	//	StDKW2ChartDisplayInfo CDI1 = mCharts.get(I);
	//	CDI1.clearChartsHidden();
	//	CDI1.clearChartsOverlap();
	//	}
	//	for (int I = mCharts.size() - 1; I >= 1; I--) {
	//	StDKW2ChartDisplayInfo CDI1 = mCharts.get(I);
	//	CDI1.setHiddenCalculated(false);
	//	CDI1.setOverlapCalculated(false);
	//	//            CDI1.clearChartsHidden();
	//	//            CDI1.clearChartsOverlap();
	//
	//	if (!CDI1.getInViewer())
	//	continue;
	//
	//	for (int II = I - 1; II >= 0; II--) {
	//	StDKW2ChartDisplayInfo CDI2 = mCharts.get(II);
	//
	//	if (!CDI2.getInViewer())
	//	continue;
	//
	//	if (!CDI1.getHiddenCalculated() || !CDI2.getHiddenCalculated()) {
	//	if (chartHiddenByChart(CDI1.getChart(), CDI2.getChart()))
	//	CDI2.addChartHidden(CDI1);
	//	else if (chartHiddenByChart(CDI2.getChart(), CDI1.getChart()))
	//	CDI1.addChartHidden(CDI2);
	//	}
	//	if (!CDI1.getOverlapCalculated() || !CDI2.getOverlapCalculated()) {
	//	if (chartOverlapsChart(CDI1.getChart(), CDI2.getChart()) ||
	//	chartOverlapsChart(CDI2.getChart(), CDI1.getChart())) {
	//	CDI1.addChartOverlap(CDI2);
	//	CDI2.addChartOverlap(CDI1);
	//	}
	//	}
	//
	//	/*
	//	if (!CDI1.getHiddenCalculated() || !CDI2.getHiddenCalculated()
	//	|| !CDI1.getOverlapCalculated() || !CDI2.getOverlapCalculated()) {
	//
	//	chartHiddenByChart2(CDI1.getChart(), CDI2.getChart(), Hidden, Overlap, true);
	//	if (Hidden.getValue())
	//	CDI2.addChartHidden(CDI1);
	//	if (Overlap.getValue())
	//	CDI1.addChartOverlap(CDI2);
	//	chartHiddenByChart2(CDI2.getChart(), CDI1.getChart(), Hidden, Overlap, true);
	//	if (Hidden.getValue())
	//	CDI1.addChartHidden(CDI2);
	//	if (Overlap.getValue())
	//	CDI2.addChartOverlap(CDI1);
	//
	//	}*/
	//	}
	//	CDI1.setHiddenCalculated(true);
	//	CDI1.setOverlapCalculated(true);
	//	}
	//	}
	
	
	func updateChartsRegion() {
		mChartsRegion.setEmpty()
		for chart in mCharts {
			if chart.getInViewer() {
				
				var p_deg :Geo2 = radToDeg(chart.calibratedChart.getCorner(0))
				var R :RectD = RectD(left: p_deg.lon, top: p_deg.lat, right: p_deg.lon, bottom: p_deg.lat)
				for j in 1 ..< 4 {
					p_deg = radToDeg(chart.calibratedChart.getCorner(j))
					if p_deg.lon < R.left {
						R.left = p_deg.lon
					}
					if p_deg.lon > R.right {
						R.right = p_deg.lon
					}
					if p_deg.lat < R.top {
						R.top = p_deg.lat
					}
					if p_deg.lat > R.bottom {
						R.bottom = p_deg.lat
					}
				}
				mChartsRegion.add(CGRect(x: R.left, y: R.bottom, width: R.right - R.left, height: R.top - R.bottom))
			}
		}
	}
	
	//	public void updateChartsWaterbodyMask() {
	//	mAllowedWaterbodyMask = 0;
	//	ChartProduct CP = new ChartProduct();
	//	for (int i = 0; i < mActDB.getActCodeCount(); i++) {
	//	mActDB.getActCode(i, CP);
	//	mAllowedWaterbodyMask = mAllowedWaterbodyMask | StWWDefs.chartToWaterbody(CP.Pid, CP.Mod);
	//	}
	//	}
	//
	//	/*
	//	public void updateChartsVisible(SQLiteDatabase Database) {
	//	String[] Columns = { "chartguid", "inviewer" };
	//	Cursor cursor = Database.query(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, Columns, null, null, null, null, null);
	//
	//	cursor.moveToFirst();
	//	while (!cursor.isAfterLast()) {
	//	// get guid
	//	UUID guid = UUID.fromString(cursor.getString(0));
	//	StDKW2ChartDisplayInfo Chart = getChartByGUID(guid);
	//	if (Chart != null)
	//	Chart.setInViewer(cursor.getInt(1) == 1);
	//	cursor.moveToNext();
	//	}
	//	cursor.close();
	//
	//	updateChartsHidden();
	//	}
	//	*/
	//
	//	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	//	public void writeChartsVisible(StDKW2ChartDatabaseHelper Database) {
	//	VisibleChartsWriter vcw = new VisibleChartsWriter(Database);
	//	if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
	//	vcw.execute();
	//	else
	//	vcw.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	//
	//	/*
	//	// delete all current entries in the database
	//	Database.delete(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, null);
	//
	//	// add all existing data
	//	for (int i = 0; i < mCharts.size(); i++) {
	//	StDKW2ChartDisplayInfo Chart = mCharts.get(i);
	//	ContentValues values = new ContentValues();
	//	values.put("chartguid", Chart.getChart().getDKW2Chart().getTag().getGUID().toString());
	//	values.put("inviewer", Chart.getInViewer() ? 1 : 0);
	//	Database.insert(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, values);
	//	}
	//	*/
	//	}
	//
	//	private boolean chartHiddenByChart(StDKW2CalibratedChart Chart1, StDKW2CalibratedChart Chart2) {
	//	if (Chart1.getRadius() > Chart2.getRadius())
	//	return false;
	//	if (GeodeticsGreatCircle.distAlongGreatCircle(Chart1.getCenterGC(), Chart2.getCenterGC()) > Chart1.getRadius() + Chart2.getRadius())
	//	return false;
	//
	//	StDKW2BoundsTag Bounds1 = Chart1.getDKW2Chart().getBoundsTag();
	//	StDKW2BoundsTag Bounds2 = Chart2.getDKW2Chart().getBoundsTag();
	//	StGeodeticCalibration Cal1 = Chart1.getCalibration();
	//	StGeodeticCalibration Cal2 = Chart2.getCalibration();
	//	for (int I = Bounds1.getCount() - 1; I >= 0; I--) {
	//	Double2 P = Cal1.pixTransform(Cal2, Bounds1.get(I));
	//	if (!Bounds2.pointInPolygon(P))
	//	return false;
	//	}
	//	return true;
	//	}
	//
	//	private boolean chartOverlapsChart(StDKW2CalibratedChart Chart1, StDKW2CalibratedChart Chart2) {
	//	StDKW2BoundsTag Bounds1 = Chart1.getDKW2Chart().getBoundsTag();
	//	StDKW2BoundsTag Bounds2 = Chart2.getDKW2Chart().getBoundsTag();
	//	StGeodeticCalibration Cal1 = Chart1.getCalibration();
	//	StGeodeticCalibration Cal2 = Chart2.getCalibration();
	//	for (int I = Bounds1.getCount() - 1; I >= 0; I--) {
	//	Double2 P = Cal1.pixTransform(Cal2, Bounds1.get(I));
	//	if (Bounds2.pointInPolygon(P))
	//	return true;
	//	}
	//	return false;
	//	}
	//
	//	private void chartHiddenByChart2(StDKW2CalibratedChart aC1, StDKW2CalibratedChart aC2,
	//	MutableBoolean aHidden, MutableBoolean aOverlaps, boolean aCheckOverlap) {
	//
	//	aHidden.setValue(true);
	//	aOverlaps.setValue(false);
	//
	//	boolean HiddenFound = false;
	//	boolean OverlapFound = (!aCheckOverlap);
	//	boolean PointInPolygon;
	//
	//	if (aC1.getRadius() > aC2.getRadius()) {
	//	aHidden.setValue(false);
	//	HiddenFound = true;
	//	} else if (GeodeticsGreatCircle.distAlongGreatCircle(aC1.getCenterGC(), aC2.getCenterGC()) > aC1.getRadius() + aC2.getRadius()) {
	//	aHidden.setValue(false);
	//	HiddenFound = true;
	//	}
	//
	//	StDKW2BoundsTag Bounds1 = aC1.getDKW2Chart().getBoundsTag();
	//	StDKW2BoundsTag Bounds2 = aC2.getDKW2Chart().getBoundsTag();
	//	StGeodeticCalibration Cal1 = aC1.getCalibration();
	//	StGeodeticCalibration Cal2 = aC2.getCalibration();
	//	for (int I = Bounds1.getCount() - 1; I >= 0; I--) {
	//	Double2 P = Cal1.pixTransform(Cal2, Bounds1.get(I));
	//	PointInPolygon = Bounds2.pointInPolygon(P);
	//
	//	if (!HiddenFound) {
	//	// check if p lies within polygon of chart2
	//	if (!PointInPolygon) {
	//	aHidden.setValue(false);
	//	HiddenFound = true;
	//	}
	//	}
	//	if (!OverlapFound) {
	//	if (PointInPolygon) {
	//	aOverlaps.setValue(true);
	//	OverlapFound = true;
	//	}
	//	}
	//	if (HiddenFound && OverlapFound)
	//	return;
	//	}
	//	}
	
	//	private void updateChartSets() {
	//	mChartSets.clear();
	//	boolean found;
	//	boolean Default;
	//	for (StDKW2ChartDisplayInfo cInfo : mCharts) {
	//	StDKW2Tag tag = cInfo.getChart().getDKW2Chart().getTag();
	//	found = false;
	//	for (DKW2ChartSetInfo item : mChartSets) {
	//	if (item.productID == tag.getProductID() && item.moduleBitIndex == tag.getModuleBitIndex()
	//	&& item.mName.equals(tag.getModuleName())) {
	//	found = true;
	//	item.chartCount++;
	//	if (cInfo.getChartInViewer())
	//	item.chartVisCount++;
	//
	//	cInfo.setTransparentColor(item.transparentColor);
	//	cInfo.setUseTransparentColor(item.useTransparentColor);
	//
	//	break;
	//	}
	//	}
	//	if (!found) {
	//	DKW2ChartSetInfo csi = new DKW2ChartSetInfo();
	//	csi.chartCount = 1;
	//	csi.chartVisCount = (cInfo.getChartInViewer()) ? 1 : 0;
	//	csi.productID = tag.getProductID();
	//	csi.moduleBitIndex = tag.getModuleBitIndex();
	//	csi.pName = tag.getProductName();
	//	csi.mName = tag.getModuleName();
	//	csi.visible = cInfo.getModuleInViewer();
	//
	//	// Set default transparency based on chartset
	//	Default = false;
	//	if (csi.productID == 196660 || csi.productID == 196673 || csi.productID == 196681 || csi.productID == 196690 || csi.productID == 196696) {
	//	Default = (tag.getModuleNameShort().equals("BE") || tag.getModuleNameShort().equals("NF") ||
	//	tag.getModuleNameShort().equals("NWD") || tag.getModuleNameShort().equals("ZWD"));
	//	} else if (csi.productID == 196651 || csi.productID == 196657 || csi.productID == 196659 || csi.productID == 196675 ||
	//	csi.productID == 196678 || csi.productID == 196684 || csi.productID == 196693 || csi.productID == 196699)
	//	Default = true;
	//
	//	csi.transparentColor = 0xFFFFFFFF;
	//	csi.useTransparentColor = Default;
	//	cInfo.setTransparentColor(0xFFFFFFFF);
	//	cInfo.setUseTransparentColor(Default);
	//
	//	mChartSets.add(csi);
	//	}
	//	}
	//	}
	
	
	//
	//	private void updateChartsVisible(int aPid, int aMod, ArrayList<UUID> aCharts) {
	//	for (int i = 0; i < mCharts.size(); i++) {
	//	StDKW2ChartDisplayInfo Chart = mCharts.get(i);
	//	StDKW2Tag Tag = Chart.getChart().getDKW2Chart().getTag();
	//	int Module = 1 << Tag.getModuleBitIndex();
	//	if (Tag.getProductID() == aPid && (Module & aMod) == Module)
	//	Chart.setInViewer(aCharts.contains(Tag.getGUID()));
	//	}
	//	}
	
	func chartListCompare(this :DKW2ChartDisplayInfo, that :DKW2ChartDisplayInfo) -> Bool {
		
		let c1 = this.calibratedChart
		let c2 = that.calibratedChart
		
		if c1 === c2 {
			return false
		}
		
		let s1 = c1.centerScale!
		let s2 = c2.centerScale!
		
		var z1 :Double = s1 / viewerCenterScale
		//       String logz = "c1: " + c1.getDKW2Chart().getFileName() + ", z1: " + z1;
		if (z1 < 1) {
			z1 = 1 / z1 * 0.5;
			//          logz += " < 1: " + z1;
		}
		//            Log.d("ChartCompare", logz);
		
		var z2 :Double = s2 / viewerCenterScale
		//       logz = "c2: " + c2.getDKW2Chart().getFileName() + ", z2: " + z2;
		if (z2 < 1) {
			z2 = 1 / z2 * 0.5;
			//          logz += " < 1: " + z2;
		}
		//       Log.d("ChartCompare", logz);
		
		if z1 < z2 {
			return true
		} else if (z1 > z2) {
			return false
		} else {
			if (s1 < s2) {
				return true
			} else if (s1 > s2) {
				return false
			} else {
				// disabled comparing by chart name
				//					String n1 = c1.getDKW2Chart().getTag().getChartName()
				//					int Result = n1.compareTo(c2.getDKW2Chart().getTag().getChartName())
				//					if (Result == 0) {
				//						if (c1.hashCode() > c2.hashCode())
				//						return -1;
				//						else if (c1.hashCode() < c2.hashCode())
				//						return 1;
				//						else
				//						return 0;
				//					}
				//					return Result;
				return false
			}
		}
	}
	
	public class DKW2ChartSetInfo {
		var chartCount :Int?
		var chartVisCount :Int?
		var mName :String?
		var moduleBitIndex :Int?
		var pName :String?
		var productID :Int64?
		var transparentColor :Int?
		var useTransparentColor :Bool?
		var visible :Bool?
		var charts : [DKW2ChartDisplayInfo] = [DKW2ChartDisplayInfo]()
	}
	
	//	private class VisibleChartsWriter extends AsyncTask<Void, Void, Boolean> {
	//
	//	StDKW2ChartDatabaseHelper mDBHelper;
	//
	//	public VisibleChartsWriter(StDKW2ChartDatabaseHelper aDBHelper) {
	//	mDBHelper = aDBHelper;
	//	}
	//
	//	@Override
	//	protected Boolean doInBackground(Void... params) {
	//	SQLiteDatabase DB = mDBHelper.getWritableDatabase();
	//
	//	// delete all current entries in the database
	//	DB.delete(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, null);
	//
	//	// add all existing data
	//	for (int i = 0; i < mCharts.size(); i++) {
	//
	//	try {
	//	if (DB.isOpen()) {
	//	StDKW2ChartDisplayInfo Chart = mCharts.get(i);
	//	ContentValues values = new ContentValues();
	//	values.put("chartguid", Chart.getChart().getDKW2Chart().getTag().getGUID().toString());
	//	values.put("inviewer", Chart.getChartInViewer() ? 1 : 0);
	//	values.put("moduleinviewer", Chart.getModuleInViewer() ? 1 : 0);
	//	DB.insert(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, values);
	//	// ^ java.lang.IllegalStateException: attempt to re-open an already-closed object: SQLiteDatabase: /data/data/com.stentec.wingps_marine_lite/databases/StChartsDB
	//	//                at android.database.sqlite.SQLiteClosable.acquireReference(SQLiteClosable.java:55)
	//	//                at android.database.sqlite.SQLiteDatabase.insertWithOnConflict(SQLiteDatabase.java:1437)
	//	//                at android.database.sqlite.SQLiteDatabase.insert(SQLiteDatabase.java:1339)
	//	//                at com.stentec.c.w.a(WinGPSMarine:985)
	//	}
	//	} catch (IllegalStateException e){
	//	Log.e("popke", "Still IllegalStateException in StDKW2ChartManager.java, despite \"DB.isOpen()\" check");
	//	e.printStackTrace();
	//	}
	//	}
	//
	//	mDBHelper.close();
	//	return true;
	//	}
	//	}
	//
	//	private class ChartsHiddenUpdater extends AsyncTask<Void, Void, Boolean> {
	//
	//	private Handler mMainHandler;
	//
	//	public ChartsHiddenUpdater(Handler aMainHandler) {
	//	mMainHandler = aMainHandler;
	//	}
	//
	//	@Override
	//	protected Boolean doInBackground(Void... params) {
	//	updateChartsHidden();
	//	return true;
	//	}
	//
	//	@Override
	//	protected void onPostExecute(Boolean Result) {
	//	mLoading = false;
	//	mLoadingState = CHARTLOADING_CHARTS;
	//	if (mMainHandler != null) {
	//	Message msg = Message.obtain();
	//	msg.what = AppConstants.CHARTMANAGER_CHARTSLOADED;
	//	msg.arg1 = 0;
	//	mMainHandler.sendMessage(msg);
	//	}
	//	}
	//	}
	
	//	private class ChartsLoader extends AsyncTask<Void, Void, Boolean> {
	class ChartsLoader{
		
		let chartManager :DKW2ChartManager
		//			private StDKW2ChartDatabaseHelper mChartDB;
		//			private boolean mErrorsFound;
		//			private Handler mMainHandler;
		//			private int mPid;
		//			private int mMod;
		//			private int mPidShow; // pid of set to show
		//			private int mModShow; // mod of set to show
		var loadList :[ChartLoadRecord]
		//			private boolean mSaveVisible;
		//			//private SharedPreferences mSharedPreferences;
		
		//		init(StDKW2ChartDatabaseHelper aChartDB, Handler aMainHandler, ArrayList<ChartLoadRecord> aLoadList, int aPid, int aMod, boolean aSaveVisible) {
		init(chartManager :DKW2ChartManager, loadList :[ChartLoadRecord]) {
			
			self.chartManager = chartManager
			//			mChartDB = aChartDB;
			//			mErrorsFound = false;
			//			mMainHandler = aMainHandler;
			//			mPid = aPid;
			//			mMod = aMod;
			//			mPidShow = 0;
			//			mModShow = 0;
			self.loadList = loadList
			//			mSaveVisible = aSaveVisible;
			//            mSharedPreferences = null;
		}
		
		//		/**
		//		* Set a chart set to show in any case.
		//		* <p>Set this before running the task!
		//		*
		//		* @param aPidShow The productid of the chart set to show.
		//		* @param aModShow The modulemask of the chart set to show.
		//		*/
		//		void setChartsetToShow(int aPidShow, int aModShow) {
		//		mPidShow = aPidShow;
		//		mModShow = aModShow;
		//		}
		//
		//		@Override
		func execute(){
			//
			//		SQLiteDatabase DB = mChartDB.getWritableDatabase();
			//
			//		// save visible first
			//		if (mSaveVisible) {
			//
			//		// delete all current entries in the database
			//		DB.delete(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, null);
			//
			//		// add all existing data
			//		for (int i = 0; i < mCharts.size(); i++) {
			//		StDKW2ChartDisplayInfo Chart = mCharts.get(i);
			//		ContentValues values = new ContentValues();
			//		values.put("chartguid", Chart.getChart().getDKW2Chart().getTag().getGUID().toString());
			//		values.put("inviewer", Chart.getChartInViewer() ? 1 : 0);
			//		values.put("moduleinviewer", Chart.getModuleInViewer() ? 1 : 0);
			//		DB.insert(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, null, values);
			//		}
			//		}
			//
			//			var error = ErrorCode.ER_OK
			var CDI :DKW2ChartDisplayInfo?
			//		MutableInteger Index = new MutableInteger();
			
			
			for i in 0 ..< loadList.count {
				var clr = loadList[i]
				//				var dKW2Chart = DKW2Chart(clr.Filename, null, 0, Error)
				var dkw2Chart = DKW2Chart(fileName: clr.filename)
				//				if (Error != ErrorCode.ER_OK) {
				//					clr.LoadError.setErrorCode(Error.getErrorCode());
				//					mErrorsFound = true;
				//					continue;
				//				}
				//				if (clr.User && (DKW2Chart.getTag().getProductID() & LIDLICENSECHECK) == LIDLICENSECHECK)
				//				continue;
				var cChart = DKW2CalibratedChart(dkw2Chart)
				CDI = DKW2ChartDisplayInfo(cChart)
				CDI?.validated = true
				
				//				if (clr.Replace && containsChart(clr.Filename, Index))
				//				mCharts.set(Index.getValue(), CDI);
				//				else
				chartManager.mCharts.append(CDI!)
			}
			
			//		if (mMainHandler != null) {
			//		Message msg = Message.obtain();
			//		mLoadingState = CHARTLOADING_VISIBILITY;
			//		msg.what = AppConstants.CHARTMANAGER_LOADSTATECHANGED;
			//		mMainHandler.sendMessage(msg);
			//		}
			//
			//		//           boolean DBCreated = mChartDB.getCreated();
			//
			//		String[] Columns = { "inviewer", "moduleinviewer" };
			//
			//		long pid = -1, mbi = -1;
			//		String mns = "";
			
			
			// Set
			let chartVisibilitySettings = UserDefaults(suiteName: Resources.SETTINGS_CHART_VISIBILLITY)
			//			for (int i = 0; i < mCharts.size(); i++) {
			for CDI in chartManager.mCharts {
				
				//			CDI = mCharts.get(i);
				//
				//			String chartGUID = CDI.getChart().getDKW2Chart().getTag().getGUID().toString();
				//
				//			// Android: Update visibility of the charts from the chart settings database
				//			// iOS: Store visibillity in defaults
				//
				//			boolean inDatabase = false;
				//			Cursor cursor = null;
				//			try { // workaround after: java.lang.IllegalStateException: attempt to re-open an already-closed object: SQLiteDatabase: /data/user/0/com.stentec.wingps_marine_lite/databases/StChartsDB
				//			cursor = DB.query(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, Columns, "chartguid = '" + chartGUID + "'", null, null, null, null);
				//			// If chart found in database set it's visibility based on the database data (or enable it anyway if it is a chart from the given pidshow and modshow).
				//			if (cursor.getCount() == 1) {
				//			inDatabase = true;
				//			if (mPidShow == CDI.getChart().getDKW2Chart().getTag().getProductID() && mModShow == (1 << CDI.getChart().getDKW2Chart().getTag().getModuleBitIndex())) {
				//			CDI.setInViewer(true);
				//			CDI.setModuleInViewer(true);
				//			} else {
				//			cursor.moveToFirst();
				//			CDI.setInViewer(cursor.getInt(0) == 1);
				//			CDI.setModuleInViewer(cursor.getInt(1) == 1);
				//			}
				//			}
				//			} catch (Exception e){
				//			} finally {
				//			if (cursor != null) cursor.close();
				//			}
				//			if (!inDatabase) {
				//			// If not in database check if this is the first chart. If so make it visible and all other charts in the set as well.
				//			CDI.setInViewer(true);
				//			if (i == 0) {
				//			pid = CDI.getChart().getDKW2Chart().getTag().getProductID();
				//			mbi = CDI.getChart().getDKW2Chart().getTag().getModuleBitIndex();
				//			mns = CDI.getChart().getDKW2Chart().getTag().getModuleName();
				//			}
				//			if (pid == CDI.getChart().getDKW2Chart().getTag().getProductID() &&
				//			mbi == CDI.getChart().getDKW2Chart().getTag().getModuleBitIndex() &&
				//			mns.equals(CDI.getChart().getDKW2Chart().getTag().getModuleName()))
				//			CDI.setModuleInViewer(true);
				//			else { // if the set is not visible, but it is the set given in mpidshow enable it anyway
				//			if (mPidShow == CDI.getChart().getDKW2Chart().getTag().getProductID() &&
				//			mModShow == (1 << CDI.getChart().getDKW2Chart().getTag().getModuleBitIndex()))
				//			CDI.setModuleInViewer(true);
				//			else
				//			CDI.setModuleInViewer(false);
				//			}
				//			}
				
				// Replacement of the Android code above: set chart visibility by chart Guid in userdefaults
				
				let pid = CDI.calibratedChart.dKW2Chart?.tag.productID ?? 0
				let mod = CDI.calibratedChart.dKW2Chart?.tag.moduleBitIndex ?? -1
				
				let chartSetString = "module-\(pid)-\(mod)"
				if chartVisibilitySettings?.object(forKey: chartSetString) != nil {
					CDI.setModuleInViewer(chartVisibilitySettings?.object(forKey: chartSetString) as? Bool ?? true)
				} else {
					CDI.setModuleInViewer(true)
				}
				
				
				if let chartGUID = CDI.calibratedChart.dKW2Chart?.tag.guid.toString() {
					if chartVisibilitySettings?.object(forKey: chartGUID) != nil,
						let visibility = chartVisibilitySettings?.bool(forKey: chartGUID)
					{
						CDI.setInViewer(visibility)
						//						CDI.setModuleInViewer(visibility)
					} else {
						// default settings
						CDI.setInViewer(true)
						//						CDI.setModuleInViewer(true)
					}
				}
			}
			//
			//			mChartDB.close();
			//
			//			if (mMainHandler != null) {
			//			Message msg = Message.obtain();
			//			mLoadingState = CHARTLOADING_ORDER;
			//			msg.what = AppConstants.CHARTMANAGER_LOADSTATECHANGED;
			//			mMainHandler.sendMessage(msg);
			//			}
			
			
			//			updateChartsHidden();
			
			
			chartManager.updateChartSets()
			chartManager.updateChartsRegion()
			
			
			/*updateChartsWaterbodyMask();
			
			if (mSaveVisible) {
			Import new chart collections
			importChartCollections(mPid, mMod);
			}
			
			return true;
			*/
			
			onPostExecute()
		}
		
		//		@Override
		//		protected void onPostExecute(Boolean Result) {
		func onPostExecute(){
			chartManager.mLoading = false
			//		mLoadingState = CHARTLOADING_CHARTS;
			//		if (mMainHandler != null) { // handler to pass through chart loading state to mapview
			//		Message msg = Message.obtain();
			//		msg.what = AppConstants.CHARTMANAGER_CHARTSLOADED;
			//		if (mPidShow != 0)
			//		msg.obj = String.valueOf(mPidShow) + "_" + String.valueOf(mModShow);
			//		msg.arg1 = mSaveVisible ? DKW2CHARTMANAGER_SAVEVISIBLE : 0;
			//		msg.arg2 = mErrorsFound ? 1 : 0;
			//		mMainHandler.sendMessage(msg);
			//		}
			
			
		}
		
	}
	
	
	
	//		private class ChartsLoader extends AsyncTask<Void, Void, Boolean> implements AllowedChartsConstants {
	//
	//		private ArrayList<StDKW2ChartDisplayInfo> mCList;
	//		private ArrayList<DKW2ChartSetInfo> mSList;
	//		private StDKW2ChartDatabaseHelper mChartDB;
	//
	//		public ChartsLoader(StDKW2ChartDatabaseHelper aChartDB) {
	//		mCList = new ArrayList<StDKW2ChartDisplayInfo>();
	//		mSList = new ArrayList<DKW2ChartSetInfo>();
	//		mChartDB = aChartDB;
	//		}
	//
	//		@Override
	//		protected Boolean doInBackground(Void... params) {
	//
	//		SQLiteDatabase DB = mChartDB.getReadableDatabase();
	//
	//		StDKW2Settings DKWSettings = StDKW2Settings.getSettings();
	//		File path = DKWSettings.getDKW2ChartsDir();
	//
	//		if (!path.exists())
	//		return true;
	//
	//		//           boolean updated = false;
	//
	//		//           DKW2FileFilter filefilter = new DKW2FileFilter();
	//		//           String DKW2Files[] = path.list(filefilter);
	//
	//		int ChartCount = mActDB.getFileNameCount(PID, MODULE);
	//
	//		EDKW2Error Error = EDKW2Error.DKW2AllOk;
	//		//           MutableInteger Index = new MutableInteger();
	//		String[] Columns = { "inviewer" };
	//		String filename, extension;
	//		File chart;
	//
	//		for (int i = 0; i < ChartCount; i++) {
	//		filename = mActDB.getFileName(PID, MODULE, i);
	//		extension = filename.substring(filename.lastIndexOf(".") + 1);
	//		if (!extension.equalsIgnoreCase("dkw2"))
	//		continue;
	//
	//		chart = new File(path, filename);
	//		if (!chart.exists())
	//		continue;
	//
	//		StDKW2Chart DKW2Chart = new StDKW2Chart(chart, null, 0, Error);
	//		StDKW2CalibratedChart CChart = new StDKW2CalibratedChart(DKW2Chart);
	//		StDKW2ChartDisplayInfo CDI = new StDKW2ChartDisplayInfo(CChart);
	//		mCList.add(CDI);
	//
	//		// Update visibility of the charts from the chart settings database
	//		Cursor cursor = DB.query(StDKW2ChartDatabaseHelper.DKW2CHARTDBTABLENAME, Columns, "chartguid = '" + DKW2Chart.getTag().getGUID().toString() + "'", null, null, null, null);
	//		if (cursor.getCount() == 1) {
	//		cursor.moveToFirst();
	//		CDI.setInViewer(cursor.getInt(0) == 1);
	//		} else {
	//		CDI.setInViewer(true);
	//		}
	//		cursor.close();
	//		}
	//
	//		mChartDB.close();
	//
	//		updateChartsHiddenTask();
	//		updateChartSetsTask();
	//
	//		//           double Duration = (System.nanoTime() - Start) / 1000000000f;
	//		//           Log.d("ChartManager", "Chart refreshing took: " + String.valueOf(Duration) + " seconds");
	//
	//		return true;
	//		}
	//
	//		@Override
	//		protected void onPostExecute(Boolean Result) {
	//		if (Result) {
	//		mCharts = mCList;
	//		mChartSets = mSList;
	//		}
	//		mLoading = false;
	//		}
	//
	//	private void updateChartsHiddenTask() {
	//	MutableBoolean Hidden = new MutableBoolean(false);
	//	MutableBoolean Overlap = new MutableBoolean(false);
	//
	//	for (int I = mCList.size() - 1; I >= 1; I--) {
	//	StDKW2ChartDisplayInfo CDI1 = mCList.get(I);
	//	CDI1.setHiddenCalculated(false);
	//	CDI1.setOverlapCalculated(false);
	//	CDI1.clearChartsHidden();
	//	CDI1.clearChartsOverlap();
	//
	//	if (!CDI1.getInViewer())
	//	continue;
	//
	//	for (int II = I - 1; II >= 0; II--) {
	//	StDKW2ChartDisplayInfo CDI2 = mCList.get(II);
	//
	//	if (!CDI2.getInViewer())
	//	continue;
	//
	//	if (!CDI1.getHiddenCalculated() || !CDI2.getHiddenCalculated()
	//	|| !CDI1.getOverlapCalculated() || !CDI2.getOverlapCalculated()) {
	//
	//	chartHiddenByChart2(CDI1.getChart(), CDI2.getChart(), Hidden, Overlap, true);
	//	if (Hidden.getValue())
	//	CDI2.addChartHidden(CDI1);
	//	if (Overlap.getValue()) {
	//	CDI1.addChartOverlap(CDI2);
	//	CDI2.addChartOverlap(CDI1);
	//	}
	//	chartHiddenByChart2(CDI2.getChart(), CDI1.getChart(), Hidden, Overlap, false);
	//	if (Hidden.getValue())
	//	CDI1.addChartHidden(CDI2);
	//	}
	//	}
	//	CDI1.setHiddenCalculated(true);
	//	CDI1.setOverlapCalculated(true);
	//	}
	//	}
	//
	//	private void updateChartSetsTask() {
	//	boolean found;
	//	for (StDKW2ChartDisplayInfo cInfo : mCList) {
	//	StDKW2Tag tag = cInfo.getChart().getDKW2Chart().getTag();
	//	found = false;
	//	for (DKW2ChartSetInfo item : mSList) {
	//	if (item.productID == tag.getProductID() && item.moduleBitIndex == tag.getModuleBitIndex()) {
	//	found = true;
	//	item.chartCount++;
	//	break;
	//	}
	//	}
	//	if (!found) {
	//	DKW2ChartSetInfo csi = new DKW2ChartSetInfo();
	//	csi.chartCount = 1;
	//	csi.productID = tag.getProductID();
	//	csi.moduleBitIndex = tag.getModuleBitIndex();
	//	csi.pName = tag.getProductName();
	//	csi.mName = tag.getModuleName();
	//	csi.visible = (cInfo.getInViewer()) ? true : false;
	//	mSList.add(csi);
	//	}
	//	}
	//	}
	//	}
	//	*/
	
	func getDKW2FilesFromDirectory(updated :inout Bool) -> [ChartLoadRecord] {
		
		var loadList = [ChartLoadRecord]()
		
		// Fill loadlist with available DKW2 files
		var DKW2Files = [URL]()
		do{ // Get the directory contents urls (including subfolders urls)
			
			let useBundle :Bool = true
			
			if useBundle {
				let directoryContents = try FileManager.default.contentsOfDirectory(at: getAppBundleDKW2Directory(), includingPropertiesForKeys: nil, options: [])
				// if you want to filter the directory contents you can do like this:
				DKW2Files = directoryContents.filter{ $0.pathExtension == "dkw2" }
				print("DKW2 urls:", DKW2Files)
				//			let fileNames = files.map{ $0.deletingPathExtension().lastPathComponent }
				//			print("list:", fileNames)
			} else {
				
				let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
				
				let directoryContents = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: [])
				// if you want to filter the directory contents you can do like this:
				DKW2Files = directoryContents.filter{ $0.pathExtension == "dkw2" }
				print("DKW2 urls in .documentDirectory:", DKW2Files)
				
			}
			
		} catch {
			print(error.localizedDescription)
		}
		
		// TODO: Loadlist here is filled with a list of files that's from the account, and checked if it exists.
		
		//			var clr :ChartLoadRecord
		//			//    StError Error = new StError();
		//			var CDI :DKW2ChartDisplayInfo
		//			for i in 0 ..< CPs.size() {
		//
		//				ChartProduct CP = CPs.get(i)
		//
		//				int ChartCount = mActDB.getFileNameCount(CP.Pid, CP.Mod);
		//
		//				MutableInteger Index = new MutableInteger();
		//				String filename, extension;
		//				StDocumentFile chart;
		//
		//				for (int j = 0; j < ChartCount; j++) {
		for j in 0 ..< DKW2Files.count {
			//					filename = mActDB.getFileName(CP.Pid, CP.Mod, j);
			//					extension = filename.substring(filename.lastIndexOf(".") + 1);
			//					if (!extension.equalsIgnoreCase("dkw2"))
			//					continue;
			//
			//							chart = new StDocumentFile(path, filename);
			let chart = DKW2Files[j]
			//					if (!chart.exists())
			//					continue;
			//
			//					if (containsChart(chart, Index)) {
			//						CDI = mCharts.get(Index.getValue());
			//						// check if the chart has changed
			//						if (chart.length() != CDI.getChart().getDKW2Chart().getFileSize() ||
			//							chart.lastModified() != CDI.getChart().getDKW2Chart().getFileLastModified()) {
			// chart is different, so reload it and replace the original
			//									loadList.append(ChartLoadRecord(filename: chart, replace: true, user: false));
			//							updated = true;
			//							continue;
			//						} else {
			//							// chart hasn't changed so continue
			//							CDI.setValidated(true);
			//							continue;
			//						}
			//					}
			//
			loadList.append(ChartLoadRecord(filename: chart, replace: false, user: false))
			updated = true
		}
		//			}
		
		return loadList
	}
	
	static func refreshCharts(){
		//		boolean WriteCharts = true; // mean writing to chart settings to update visibility
		//
		//		// Returning from the chart manager
		//		if (RequestCode == AppConstants.ACTIVITY_CHARTMANAGER) {
		//			mSelectedChart = null;
		//			if (ResultCode == RESULT_OK) {
		//				int Result = data.getIntExtra(ChartManagerActivity.CHARTMANAGERRESULT, 0);
		//				if (Result == ChartManagerActivity.CHARTMANAGERRESULT_CENTERCHART) {
		//					disableFollow(mImageButtonFollow);
		//					UUID cGUID = UUID.fromString(data.getStringExtra(ChartManagerActivity.CHARTMANAGERCHART));
		//					StDKW2ChartDisplayInfo chart = StDKW2ChartManager.getChartManager().getChartByGUID(cGUID);
		//					if (chart != null) {
		//						mSelectedChart = cGUID;
		//						Geo2 center = chart.getChart().getCenterGeo();
		//						mZoomChart = chart;
		//						mZoomCenter = new GeoPoint(Math.toDegrees(center.Lat), Math.toDegrees(center.Long));
		//						mZoomComplete = chart.getShowBorder();
		//						//zoomToChart(chart, new GeoPoint(Math.toDegrees(center.y), Math.toDegrees(center.x)), chart.getShowBorder());
		//					}
		//				} else if (Result == ChartManagerActivity.CHARTMANAGERRESULT_RELOADCHARTS) {
		//					// After newly downloaded chart
		//
		////					int pid = getResources().getInteger(R.integer.pid);
		////					int mod = getResources().getInteger(R.integer.module);
		////
		////					int pidshow = 0, modshow = 0;
		////					String cdata = data.getStringExtra(ChartManagerActivity.CHARTMANAGERDATA);
		////					if (cdata != null) {
		////						String list[] = cdata.split(";");
		////						String pr[] = list[0].split("_");
		////						pidshow = Integer.valueOf(pr[0]);
		////						modshow = Integer.valueOf(pr[1]);
		////					}
		////
		////					WriteCharts = false;
		////					StDKW2ChartManager.getChartManagerRefresh(mChartDB, mUIHandler, pid, mod, pidshow, modshow, true);
		//
		//				}
		//			}
		
		//		if mDKW2TilesOverlay != null {
		// writeCharts is to store chart visibillity settings (which is already done in chart manager)
		if DKW2ChartManager.getChartManager().getChartVisibilityChanged() {
			DKW2ChartManager.getChartManager().updateChartsRegion()
			//			DKW2ChartManager.getChartManager().updateChartsHiddenInTask(mUIHandler)
			DKW2ChartManager.getChartManager().setChartVisibilityChanged(aChanged: false)
		}
		
		//			mDKW2TilesOverlay.setUseOSM(StDKW2Settings.getSettings().getUseESRI());
		//			mDKW2TilesOverlay.setUseNOAA(StDKW2Settings.getSettings().getUseNOAA());
		//			mDKW2TilesOverlay.invalidateCal() // -> triggers DKW2TileOverlay.updateCalibration -> CalibrationUpdater
		
		DKW2ChartManager.chartManagerRefresh()
		
		
		//		}
		//					if (WriteCharts) {
		//						StDBManager.getManager().updateEnabledChartDatabases();
		//						StDKW2ChartManager.getChartManager().writeChartsVisible(mChartDB);
		//					}
		
		//			mMapView.invalidate();
		//		dKW2TileRenderer.reloadData()
        
//        NetworkOverlay.instance.addVisibleNetworkLinesToMapview() // too heavy to do after each refresh
        NetworkOverlay.instance.onChartManagerRefresh() 

	}
	
	static func chartManagerRefresh(){
		dout(.def, "chartManagerRefresh()")
		// reload waypoint files
        
		ViewController.instance?.loadWaypointFiles()
		
		let pid = Resources.pid
		let mod = Resources.module
		
		//		SharedPreferences chartPrefs = getSharedPreferences(StDKW2ChartManager.CHARTMANAGER_SHAREDPREFS, MODE_PRIVATE);
		let prefs = UserDefaults.standard
		let PidShow = prefs.integer(forKey: DKW2ChartManager.CHARTMANAGER_PIDTOSHOW)
		let ModShow = prefs.integer(forKey: DKW2ChartManager.CHARTMANAGER_MODTOSHOW)
		
		//		StDKW2ChartManager ChartManager = StDKW2ChartManager.getChartManagerRefresh(mChartDB, mUIHandler, pid, mod, PidShow, ModShow, false);
		var chartManager = DKW2ChartManager.getChartManagerRefresh(aPid: pid, aMod: mod, aPidShow: PidShow, aModShow: ModShow, aSaveVisibility: false)
		
		ViewController.instance?.dKW2TileRenderer?.reloadData()
		
	}
}
