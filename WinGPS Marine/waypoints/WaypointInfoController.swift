//
//  WaypointInfoController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 07/05/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit


extension FloatingPoint {
    var minutes:  Self {
        return (self*3600)
            .truncatingRemainder(dividingBy: 3600)/60
    }
    var seconds:  Self {
        return (self*3600)
            .truncatingRemainder(dividingBy: 3600)
            .truncatingRemainder(dividingBy: 60)
    }
}



class WaypointInfoController : UIViewController{
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subNameLabel: UILabel!
    @IBOutlet weak var subNameView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var urlLabel: UILabel!
    @IBOutlet weak var urlView: UIView!
    
    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var lonLabel: UILabel!
    
    var boxCenterX: NSLayoutConstraint?
    var boxCenterY: NSLayoutConstraint?
    
    var panGesture = UIPanGestureRecognizer()
    
    var annotationView : MKAnnotationView?
    var waypoint : StDBWaypoint?
    
    var wpURL : [String] = [String]()
    
    var borderView : UIImageView?
    
    override func viewDidLoad() {
        containerView.layer.cornerRadius = 8
        containerView.clipsToBounds = true
        nameLabel.text = waypoint?.name
        subNameLabel.text = waypoint?.subName
        if(subNameLabel.text == ""){
            subNameView.isHidden = true
        } else {
            subNameView.isHidden = false
        }
        typeLabel.text = waypoint?.type?.name
        
        if let lightChar = waypoint?.lightChar {
            
            

            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: lightChar, options: [], range: NSRange(location: 0, length: lightChar.utf16.count))

            for match in matches {
                guard let range = Range(match.range, in: lightChar) else { continue }
                let url = lightChar[range]
                wpURL.append(String(url))
            }
            if wpURL.count > 0 {
                urlLabel.attributedText = NSAttributedString(string:
                    lightChar, attributes: [NSAttributedString.Key.link: wpURL[0]])
                let tap = UITapGestureRecognizer(target: self, action: Selector("urlTap:"))
                urlLabel.addGestureRecognizer(tap)
            } else {
                urlLabel.text = lightChar
            }
            
            
        } else {
            urlLabel.text = ""
            
        }
        if(urlLabel.text == ""){
            urlView.isHidden = true
        } else {
            urlView.isHidden = false
        }
        
        let coord = CLLocationCoordinate2D(latitude: waypoint?.getLatitude() ?? 0, longitude: waypoint?.getLongitude() ?? 0)

        latLabel.text = coord.dms.latitude
        lonLabel.text = coord.dms.longitude
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(WaypointInfoController.draggedView(_:)))
        containerView.isUserInteractionEnabled = true
        containerView.addGestureRecognizer(panGesture)
        
        if (annotationView?.subviews.count)! > 1 {
            borderView = UIImageView(frame: (annotationView?.subviews[0].frame.insetBy(dx: -4, dy: -4))!)//CGRect(origin: view.center, size: CGSize(width: view.frame.width + 2, height: view.frame.height + 2)))
            borderView?.layer.borderWidth = 2
            borderView?.layer.borderColor = UIColor(red:0.00, green:0.29, blue:0.53, alpha:1.0).cgColor //UIColor.blue.cgColor
            annotationView?.addSubview(borderView!)
            
            annotationView?.layoutIfNeeded()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let waypointHeight = ((annotationView?.frame.height)! / 2)
        var distanceFromAnnotationCenter = (containerView.frame.height / 2) + waypointHeight + 30
        if((annotationView?.center.y)! < containerView.frame.height) {
            distanceFromAnnotationCenter = -distanceFromAnnotationCenter
        }
        
        
        containerView.center = CGPoint(x: (annotationView?.center.x)!, y: (annotationView?.center.y)! - distanceFromAnnotationCenter)
        
//        if (!containerView.superview!.bounds.intersection(containerView.frame).equalTo(containerView.frame))
//        {
//            //view is partially out of bounds
//        }
        
    }
    
    
    
    @objc func urlTap(_ sender:UITapGestureRecognizer){
        var urlString = wpURL[0]
        if !(urlString.hasPrefix("http")){
            urlString = "http://" + urlString
        }
        guard let url = URL(string: urlString) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if (touch?.view == backgroundView){
            performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
            self.dismiss(animated: false, completion: nil)
            if(borderView != nil){
                borderView?.removeFromSuperview()
            }
        }
        
    }
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        //self.view.bringSubviewToFront(containerView)
        let translation = sender.translation(in: self.view)
        containerView.center = CGPoint(x: containerView.center.x + translation.x, y: containerView.center.y + translation.y)
        sender.setTranslation(CGPoint.zero, in: self.view)
    }
    
}
