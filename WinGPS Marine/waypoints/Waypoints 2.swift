//
//  Waypoints.swift
//  WinGPS Marine
//
//  Created by Standaard on 26/04/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class Waypoints {
	
	static func getCustomWaypointIconView() -> UIImageView {

		//		let color = UIColor.blue
		let height :CGFloat = 100.0;
		let width :CGFloat = 100.0;
		let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		
		//		color.setFill()
		
		let context = UIGraphicsGetCurrentContext()!
		context.translateBy(x: width/2, y: height/2)
		context.setLineWidth(15.0)
		context.setStrokeColor(UIColor.white.cgColor)
		context.setLineCap(CGLineCap.round)
		context.setLineJoin(CGLineJoin.round)
//		context.setFillColor(UIColor.blue.cgColor)
		//context.setFillColor(UIColor(red: 0.4, green: 0.4, blue: 1.0, alpha: 1.0).cgColor)
		let a = 15
		let b = 40
		context.move(to: CGPoint(x: -a, y: 0))
		context.addLine(to: CGPoint(x: a, y: 0))
		context.move(to: CGPoint(x: 0, y: -a))
		context.addLine(to: CGPoint(x: 0, y: a))

		context.move(to: CGPoint(x: -b, y: 0))
		context.addLine(to: CGPoint(x: 0, y: b))
		context.addLine(to: CGPoint(x: b, y: 0))
		context.addLine(to: CGPoint(x: 0, y: -b))
		context.addLine(to: CGPoint(x: -b, y: 0))
		context.drawPath(using: .stroke)
		
		context.setLineWidth(6.0)
		context.setStrokeColor(UIColor.blue.cgColor)
		context.move(to: CGPoint(x: -a, y: 0))
		context.addLine(to: CGPoint(x: a, y: 0))
		context.move(to: CGPoint(x: 0, y: -a))
		context.addLine(to: CGPoint(x: 0, y: a))
		
		context.move(to: CGPoint(x: -b, y: 0))
		context.addLine(to: CGPoint(x: 0, y: b))
		context.addLine(to: CGPoint(x: b, y: 0))
		context.addLine(to: CGPoint(x: 0, y: -b))
		context.addLine(to: CGPoint(x: -b, y: 0))
		context.drawPath(using: .stroke)
		
//		context.translateBy(x: 0, y: -10)
		
		//		color.setFill()
		//UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		//		guard let cgImage = image?.cgImage else { return nil }
//		return UIImage(cgImage: (image?.cgImage)!)

		
		let iconImage = UIImage(cgImage: (image?.cgImage)!)
		let iconImageView = UIImageView(image: iconImage)
		
		iconImageView.transform = CGAffineTransform(translationX: -iconImageView.image!.size.width / 2, y: -iconImageView.image!.size.height / 2)
		iconImageView.transform = iconImageView.transform.scaledBy(x: 0.15, y: 0.15)
		
//		iconImageView.setNeedsDisplay()
		return iconImageView
	}
}
