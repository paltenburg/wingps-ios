//
//  RouteManagerController.swift
//  WinGPS Marine
//
//  Created by Standaard on 04/10/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

//class RouteManagerController :UIViewController, UITableViewDataSource, UITableViewDelegate {
@objc class RouteManagerController :UIViewController {
	
	@IBOutlet weak var tableView: UITableView!

	@IBOutlet var backgroundView: UIView!

	@IBOutlet weak var viewContainer: UIView!

	@IBOutlet weak var bottomView: UIView!

	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	var routeTableCells = [RouteTableCell]()

	var selectedIndexPath :IndexPath = IndexPath(row: -1, section: 0)

	@IBAction func closeButton(_ sender: Any) {
		closeDialog()
	}

	fileprivate func closeDialog() {
//		self.dismiss(animated: true, completion: {()-> Void in
			self.performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
//		})

	}
	
	override func viewWillAppear(_ animated: Bool) {
		tableView.delegate = self
		tableView.dataSource = self
	}
	
	override func viewDidLoad() {
		viewContainer.layer.cornerRadius = 8
		viewContainer.clipsToBounds = true
	}
	
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if touch?.view == backgroundView {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
	@IBAction func newRouteButtonAction(_ sender: UIButton) {
		ViewController.instance?.createNewRouteInCenter()
		performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
	}
}

extension RouteManagerController :UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		let nrOfRoutes :Int = Routes.service.routes.count
		routeTableCells = [RouteTableCell](repeating: RouteTableCell(), count: nrOfRoutes)
		return nrOfRoutes
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

		dout("cellForRowAt indexPath: ", indexPath.section, " ", indexPath.row)

		var tableCell :RouteTableCell
		var row = indexPath.row
		var route = Routes.service.routes[row]

//		if let oldTableCell = tableView.dequeueReusableCell(withIdentifier: "routeTableCell-\(indexPath.section)-\(indexPath.row)", for: indexPath) {
		if let oldTableCell = tableView.dequeueReusableCell(withIdentifier: "routeTableCell", for: indexPath) as? RouteTableCell {
			tableCell = oldTableCell
		} else {
			tableCell = RouteTableCell()
		}
		
		tableCell.controller = self
		tableCell.route = route
		
		tableCell.routeNameLabel.text = route.name
		//		tableCell.routeDateLabel.text = "-date-"
		tableCell.routeDateLabel.text = ""
		tableCell.routeLengthLabel.text =
			StUtils.formatDistance(distInM: Double(route.length), unitLarge: Units.mUnitDistLarge, unitSmall: Units.mUnitDistSmall)
		tableCell.routeNrPointsLabel.text = String(route.routePoints.count)
		
		tableCell.routeNameLabelDetailView.text = tableCell.routeNameLabel.text
		tableCell.dateLabelDetailView.text = tableCell.routeDateLabel.text
		tableCell.lengthLabelDetailView.text = tableCell.routeLengthLabel.text
		tableCell.nPointsLabelDetailView.text = tableCell.routeNrPointsLabel.text
		
		routeTableCells[row] = tableCell
		
//		tableCell.accessoryType = .checkmark
//		tableCell.accessoryType = tableCell.isSelected ? .checkmark : .none

		return tableCell
	}
	
//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
////		// Unselect row
////		tableView.deselectRow(at: indexPath, animated: false)
////
////		// Toggle selection
////		let item = self.items[indexPath.section]
////		item.selected = !item.selected
//		tableView.reloadData()
//	}
}

@objc extension RouteManagerController :UITableViewDelegate{
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		dout("didDeselectRowAt")
		
		tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)

//		if let cell = tableView.cellForRow(at: indexPath) as? RouteTableCell{
//			cell.accessoryType = .none
//			cell.isExpanded = false
//		}
		
	}

//	public func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) {
//	
////	func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) {
//		if tableView.cellForRow(at: indexPath)?.isSelected ?? false {
//			tableView.deselectRow(at: indexPath, animated: true)
//		}
//	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		dout("didSelectRowAt selected: \(selectedIndexPath.row == indexPath.row)")
		
//		tableView.deselectRow(at: indexPath, animated: true)

		var previouslySelected = selectedIndexPath
		if selectedIndexPath.row == indexPath.row {
			selectedIndexPath = IndexPath(row: -1, section: 0)
		} else {
			selectedIndexPath = indexPath
		}
		
		//		var expanded = false
//		let cell = routeTableCells[indexPath.row]
//		if cell.isExpanded {
//			expanded = false
//		} else {
//			expanded = true
//		}
//		cell.isExpanded = expanded
		
        if previouslySelected.row >= 0 {
            tableView.reloadRows(at: [previouslySelected], with: UITableView.RowAnimation.automatic)
        }
		if selectedIndexPath.row >= 0 {
			tableView.reloadRows(at: [selectedIndexPath], with: UITableView.RowAnimation.automatic)
		}
        
//		tableView.reloadRows(at: [], with: UITableView.RowAnimation.automatic)
//		(tableView.cellForRow(at: indexPath) as? RouteTableCell)?.isExpanded = expanded
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		//		dout("heightForRowAt \(indexPath.section) \(indexPath.row) selected: \(routeTableCells[indexPath.row].isSelected)")
		
		let rtCell = routeTableCells[indexPath.row]
		
		dout("heightForRowAt \(indexPath.section) \(indexPath.row) selected: \(selectedIndexPath.row == indexPath.row) listItemView: \(rtCell.listItemView != nil)")
		
		//		if let rtCell = (tableView.cellForRow(at: indexPath) as? RouteTableCell){
		if selectedIndexPath.row == indexPath.row {
			rtCell.listItemView?.isHidden = true
			rtCell.expandedView?.isHidden = false
			
			return 155
		} else {
			rtCell.listItemView?.isHidden = false
			rtCell.expandedView?.isHidden = true
			
			return 70
		}
	}
}
