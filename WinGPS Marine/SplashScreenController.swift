//
//  SplashScreenController.swift
//  WinGPS Marine
//
//  Created by Floris Kuik on 01/02/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit

class SplashScreenController: UIViewController {
    
    @IBOutlet weak var WinGPSMarineSplashView: UIStackView!
    
    @IBOutlet var backgroundView: UIView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var splashView: UIView!
    @IBOutlet weak var shadowImageView: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
    
    
    @IBOutlet weak var DKW1800SplashView: UIView!
    @IBOutlet weak var versionLabel1800: UILabel!
    @IBOutlet weak var startButton1800: UIButton!
    
    @IBOutlet weak var frieseMerenSplashView: UIView!
    @IBOutlet weak var versionLabelFrieseMeren: UILabel!
    @IBOutlet weak var startButtonFrieseMeren: UIButton!
    
    @IBOutlet weak var marinePlusSplashView: UIView!
    @IBOutlet weak var versionLabelMarinePlus: UILabel!
    @IBOutlet weak var startButtonMarinePlus: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        switch Resources.appType {
        case .lite:
            dout("SplashScreenController: Lite version")
            
            startButton.layer.cornerRadius = 8
            startButton.clipsToBounds = true
            
            versionLabel.text = "\("software_version".localized) \(StUtils.getVersionString()) (\(StUtils.compileDate.asString(style: .short)))"
            
            splashView.isHidden = false
            DKW1800SplashView.isHidden = true
            frieseMerenSplashView.isHidden = true
            marinePlusSplashView.isHidden = true


        case .plus:
            dout("SplashScreenController: Paid version")
            
            startButtonMarinePlus.layer.cornerRadius = 8
            startButtonMarinePlus.clipsToBounds = true
            
            versionLabelMarinePlus.text = "\("software_version".localized) \(StUtils.getVersionString()) (\(StUtils.compileDate.asString(style: .short)))"
            
            splashView.isHidden = true
            DKW1800SplashView.isHidden = true
            frieseMerenSplashView.isHidden = true
            marinePlusSplashView.isHidden = false

        case .dkw1800:
            dout("SplashScreenController: DKW 1800")
            
            splashView.isHidden = true
            DKW1800SplashView.isHidden = false
            frieseMerenSplashView.isHidden = true
            marinePlusSplashView.isHidden = true

            
            startButton1800.layer.cornerRadius = 8
            startButton1800.clipsToBounds = true
            
            versionLabel1800.text = "\("software_version".localized) \(StUtils.getVersionString()) (\(StUtils.compileDate.asString(style: .short)))"
            
        case .friesemeren:
            dout("SplashScreenController: Friese Meren")
            
            splashView.isHidden = true
            DKW1800SplashView.isHidden = true
            frieseMerenSplashView.isHidden = false
            marinePlusSplashView.isHidden = true

            
            startButtonFrieseMeren.layer.cornerRadius = 8
            startButtonFrieseMeren.clipsToBounds = true
            
            versionLabelFrieseMeren.text = "\("software_version".localized) \(StUtils.getVersionString()) (\(StUtils.compileDate.asString(style: .short)))"
            
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func startButton(_ sender: Any) {
        
        // DEBUG //
        dout("Versie info: \n \(UIDevice.current.name)\n            \(UIDevice.current.systemName)\n            \(UIDevice.current.systemVersion)\n            \(UIDevice.current.model)            \(UIDevice.current.localizedModel)            \(UIDevice.current.userInterfaceIdiom)            \(UIDevice.current.identifierForVendor)")

        // If we are coming from an "exited" status, restart processes
        ViewController.instance?.continueIfFromExit()
        
        
        let transition: CATransition = CATransition()
        transition.duration = 0.2
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.fade
        self.view.window!.layer.add(transition, forKey: nil)
        
        self.dismiss(animated: false, completion: {() in
            
            switch Resources.appType {
                
            case .lite:
                
                // after tapping "Ok", check if login screen needs to be displayed
                let actDB = StActManager.getActManager()
                actDB.initialize()
                
                if actDB.getUserMail().count == 0 || actDB.getUserPass().count == 0 {
                    StatusCheck.showLoginController(handler: nil)
                }
                
            case .plus:
                
                // after tapping "Ok", check if login screen needs to be displayed
                let actDB = StActManager.getActManager()
                actDB.initialize()
                
                if actDB.getUserMail().count == 0 || actDB.getUserPass().count == 0 {
                    StatusCheck.showLoginController(handler: nil)
                }
         
            case .dkw1800:
                dout("DKW 1800")
                
                AppStorePurchases().makeSureThereIsAReceipt()
                
                // Force the use of a stentec account, which then downloads the 1800 charts
                // after tapping "Ok", check if login screen needs to be displayed
                let actDB = StActManager.getActManager()
                actDB.initialize()
                
                if actDB.getUserMail().count == 0 || actDB.getUserPass().count == 0 {
                    // First show welcome screen:
                    if StUtils.notLoggedInYetAfterInstall() {
                        
                        let welcomeDialog = UIAlertController(title: "dialog_dkw1800_welcome_title".localized, message: "dialog_dkw1800_welcome".localized, preferredStyle: .alert)
                        welcomeDialog.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                            StatusCheck.showLoginController(handler: nil, withoutEscapeButtons: true)
                        }))
                        getTopViewController()?.presentFromMain(welcomeDialog, animated: true)
                    } else {
                        StatusCheck.showLoginController(handler: nil, withoutEscapeButtons: true)
                    }
                }
                
            case .friesemeren:
                dout("Friese Meren")
                
                // same as .lite for now
                
                // after tapping "Ok", check if login screen needs to be displayed
                let actDB = StActManager.getActManager()
                actDB.initialize()
                
                if actDB.getUserMail().count == 0 || actDB.getUserPass().count == 0 {
                    
                    // First show welcome screen:
                    if StUtils.notLoggedInYetAfterInstall() {
                        
                        let welcomeDialog = UIAlertController(title: "dialog_fm_welcome_title".localized, message: "dialog_chartapp_welcome".localized, preferredStyle: .alert)
                        welcomeDialog.addAction(UIAlertAction(title: "ok".localized, style: .default, handler: {(alert) -> Void in
                            StatusCheck.showLoginController(handler: nil, withoutEscapeButtons: true)
                        }))
                        getTopViewController()?.presentFromMain(welcomeDialog, animated: true)
                    } else {
                        StatusCheck.showLoginController(handler: nil, withoutEscapeButtons: true)
                    }
                
                }
            }
            ViewController.instance?.removeMapviewBlur()
        })
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch : UITouch? = touches.first
        if (touch?.view == backgroundView){
            //performSegue(withIdentifier: "unwindSegueToMapView", sender: self)
            //self.dismiss(animated: true, completion: nil)
        }
    }
}
