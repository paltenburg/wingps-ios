//
//  AllSettingsManager.swift
//  WinGPS Marine
//
//  Created by Popke Altenburg on 02/10/2020.
//  Copyright © 2020 Stentec. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsDelegate: class{
    func updateSettings()
}

class AllSettingsViewController : UIViewController{
    
//    @IBOutlet var backgroundView: UIView!
//    @IBOutlet weak var doneButton: UIButton!
    
    //MARK: Main settings
    @IBOutlet weak var showWaypointsSwitch: UISwitch?
    @IBOutlet weak var showWaypointNamesSwitch: UISwitch?
    @IBOutlet weak var showAppleMapsSwitch: UISwitch?
    @IBOutlet weak var useNetworkSwitch: UISwitch?
    //    @IBOutlet weak var keepScreenOnSwitch: UISwitch!
    @IBOutlet weak var keepScreenOnSwitch: UISwitch?
    @IBOutlet weak var wifiOnlySwitch: UISwitch?
    @IBOutlet weak var showTrackSwitch: UISwitch?
    
    @IBOutlet weak var unitSettingsButton: UIButton?
    @IBOutlet weak var distanceCirclesButton: UIButton?
    @IBOutlet weak var tcpConnectionButton: UIButton?

    // Don't use the arrow buttons, but the whole row view.
    @IBOutlet weak var unitSettingsRowView: UIView?
    @IBOutlet weak var distanceCirclesRowView: UIView?
    @IBOutlet weak var tcpConnectionRowView: UIView?
    @IBOutlet weak var aisSettingsRowView: UIView?
    
    @IBOutlet weak var chartOrientationControl: UISegmentedControl?
    
    //MARK: Ais settings
    static var aisEnabled :Bool = true
    static var maxTargets :Int = Resources.SETTINGS_AIS_MAX_TARGET_DEFAULT
    
    @IBOutlet weak var aisEnabledSwitchOutlet: UISwitch?
    @IBOutlet weak var maxTargetsOutlet: UITextField?
    
    static let UNWIND_TO_UNIT_SETTINGS = "unwindSegueToUnitSettings"
    static let UNWIND_TO_DISTANCE_CIRCLES_SETTINGS = "unwindSegueToDistanceCirclesSettings"
    static let UNWIND_TO_TCP_CONNECTION_SETTINGS = "unwindSegueToTcpConnectionSettings"
    // segue to ais settings is directly referenced where it's used
    
    weak var delegate: SettingsDelegate? = ViewController.instance
    
    let defaultSettings = UserDefaults(suiteName: Resources.USERDEFAULT_SETTINGS)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        unitSettingsButton.layer.cornerRadius = 8
//        unitSettingsButton.clipsToBounds = true
//        distanceCirclesButton.layer.cornerRadius = 8
//        distanceCirclesButton.clipsToBounds = true
//        tcpConnectionButton?.layer.cornerRadius = 8
//        tcpConnectionButton?.clipsToBounds = true
        
        //MARK: Main settings
        showWaypointsSwitch?.isOn = defaultSettings?.object(forKey: Resources.DEFAULT_SHOW_WAYPOINTS) as? Bool ?? ViewController.instance?.mSettingsShowWaypoints ?? true
        showWaypointNamesSwitch?.isOn = defaultSettings?.object(forKey: Resources.SETTINGS_SHOW_WAYPOINT_NAMES) as? Bool ?? ViewController.instance?.mSettingsShowWaypointNames ?? false
        useNetworkSwitch?.isOn = defaultSettings?.object(forKey: Resources.SETTINGS_SHOW_NETWORK) as? Bool ?? ViewController.instance?.mNetworkOverlayEnabled ?? false
        showAppleMapsSwitch?.isOn = defaultSettings?.object(forKey: Resources.SETTINGS_SHOW_APPLEMAPS) as? Bool ?? ViewController.instance?.mSettingsShowAppleMaps ?? true
        keepScreenOnSwitch?.isOn = defaultSettings?.object(forKey: Resources.SETTINGS_KEEP_SCREEN_ON) as? Bool ?? true
        wifiOnlySwitch?.isOn = !(defaultSettings?.object(forKey: Resources.SETTINGS_USE_MOBILE_DATA) as? Bool ?? true)
        showTrackSwitch?.isOn = defaultSettings?.object(forKey: Resources.SETTINGS_TAILTRACK_VISIBLE) as? Bool ?? Resources.SETTINGS_TAILTRACK_VISIBLE_DEFAULT
        
        // Add submenu button actions to row views
        unitSettingsRowView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(unitSettingsTapped)))
        distanceCirclesRowView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(distanceCirclesSettingsTapped)))
        tcpConnectionRowView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tcpConnectionTapped)))
        aisSettingsRowView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(aisSettingsTapped)))
        
        // chart orientation
        chartOrientationControl?.selectedSegmentIndex = defaultSettings?.integer(forKey: Resources.SETTING_CHART_ORIENTATION) ?? Resources.SETTING_CHART_ORIENTATION_DEFAULT_SETTING
        
        //MARK: Ais settings
        AllSettingsViewController.aisEnabled = defaultSettings?.object(forKey: Resources.SETTINGS_AIS_ENABLED) as? Bool ?? AllSettingsViewController.aisEnabled
        aisEnabledSwitchOutlet?.isOn = AisSettingsController.aisEnabled
        AllSettingsViewController.maxTargets = defaultSettings?.object(forKey: Resources.SETTINGS_AIS_MAX_TARGETS) as? Int ?? AllSettingsViewController.maxTargets
        maxTargetsOutlet?.text = String(AisSettingsController.maxTargets)
    }
    
    //MARK: Main settings
    @IBAction func showWaypointsSwitch(_ showWaypointsSwitch: UISwitch) {
        defaultSettings?.set(showWaypointsSwitch.isOn, forKey: Resources.DEFAULT_SHOW_WAYPOINTS)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }
    
    @IBAction func showWaypointNamesSwitch(_ showWaypointNamesSwitch: UISwitch){
        defaultSettings?.set(showWaypointNamesSwitch.isOn, forKey: Resources.SETTINGS_SHOW_WAYPOINT_NAMES)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }
    
    @IBAction func useNetworkSwitch(_ sender: UISwitch) {
        defaultSettings?.set(sender.isOn, forKey: Resources.SETTINGS_SHOW_NETWORK)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }
    
    @IBAction func showAppleMaps(_ showAppleMapsSwitch: UISwitch) {
        defaultSettings?.set(showAppleMapsSwitch.isOn, forKey: Resources.SETTINGS_SHOW_APPLEMAPS)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }
    
    @IBAction func keepScreenOnSwitch(_ keepScreenOnSwitch: UISwitch) {
        defaultSettings?.set(keepScreenOnSwitch.isOn, forKey: Resources.SETTINGS_KEEP_SCREEN_ON)
        defaultSettings?.synchronize()
        UIApplication.shared.isIdleTimerDisabled = keepScreenOnSwitch.isOn
    }
    
    @IBAction func wifiOnlySwitch(_ wifiOnlySwitch: UISwitch) {
        defaultSettings?.set(!wifiOnlySwitch.isOn, forKey: Resources.SETTINGS_USE_MOBILE_DATA)
        defaultSettings?.synchronize()
        DownloadService.service.updateDownloadSettings()
    }
    
    @IBAction func ShowTrackSwitch(_ ShowTrackSwitch: UISwitch) {
        defaultSettings?.set(ShowTrackSwitch.isOn, forKey: Resources.SETTINGS_TAILTRACK_VISIBLE)
        defaultSettings?.set(ShowTrackSwitch.isOn, forKey: Resources.SETTINGS_SHOW_TRACKS)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }

    
//    @IBAction func unitSettingsButton(_ sender: UIButton) {
//
//        self.dismiss(animated: false, completion: nil)
//        performSegue(withIdentifier: SettingsController.UNWIND_TO_UNIT_SETTINGS, sender: self)
//    }
//
//    @IBAction func distanceCirclesButton(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
//        performSegue(withIdentifier: SettingsController.UNWIND_TO_DISTANCE_CIRCLES_SETTINGS, sender: self)
//    }
//
//    @IBAction func tcpConnectionButton(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
//        performSegue(withIdentifier: SettingsController.UNWIND_TO_TCP_CONNECTION_SETTINGS, sender: self)
//    }
    
    
    // Enter submenu's
    
    @IBAction func unitSettingsTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: AllSettingsViewController.UNWIND_TO_UNIT_SETTINGS, sender: self)
    }
        
    @IBAction func distanceCirclesSettingsTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: AllSettingsViewController.UNWIND_TO_DISTANCE_CIRCLES_SETTINGS, sender: self)
    }

    @IBAction func tcpConnectionTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: AllSettingsViewController.UNWIND_TO_TCP_CONNECTION_SETTINGS, sender: self)
    }
    
    @IBAction func aisSettingsTapped(_ sender: Any) {
//        self.dismiss(animated: false, completion: nil)
        performSegue(withIdentifier: "segueToAisSettings", sender: self)
    }
    
    
    // chart orientation
    @IBAction func chartOrientationControlChanged(_ sender: UISegmentedControl) {
//        if (sender.selectedSegmentIndex == Resources.SETTING_CHART_ORIENTATION_NORTH_UP){
//        } else if (sender.selectedSegmentIndex == Resources.SETTING_CHART_ORIENTATION_COURSE_UP){
//        }
        defaultSettings?.set(sender.selectedSegmentIndex, forKey: Resources.SETTING_CHART_ORIENTATION)
        defaultSettings?.synchronize()
        updateViewControllerSettings()
    }
    
    
    func updateViewControllerSettings() {
        delegate?.updateSettings()
    }
    
    //MARK: Ais settings
    
    @IBAction func aisEnabledSwitchAction(_ sender: UISwitch) {
        AisSettingsController.aisEnabled = sender.isOn
        
        defaultSettings?.set(AllSettingsViewController.aisEnabled, forKey: Resources.SETTINGS_AIS_ENABLED)

        ViewController.instance?.updateSettings()
    }
    
    @IBAction func maxTargetsAction(_ sender: UITextField) {
        AllSettingsViewController.maxTargets = Int(sender.text ?? "") ?? AllSettingsViewController.maxTargets
        defaultSettings?.set(AllSettingsViewController.maxTargets, forKey: Resources.SETTINGS_AIS_MAX_TARGETS)

        ViewController.instance?.updateSettings()
    }

    
}
