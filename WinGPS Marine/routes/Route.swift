//
//  Route.swift
//  WinGPS Marine
//
//  Created by Standaard on 21/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import MapKit
import CoreGPX

class Route{

	var GUID :UUID
	var name :String = ""
	var routePoints = [RoutePoint]()
	var createTime :Date
	var startTime :Date?
	var stateEdit :Bool = false
	var stateVisible :Bool = true
	var needsSaving :Bool = false
	
//	var overlays :[MKOverlay] = [MKOverlay]()
	var routePointOverlays :[[MKOverlay]] = [[MKOverlay]]()
	var routePointAnnotations :[RoutePointLabelAnnotation] = [RoutePointLabelAnnotation]()
	var lineOverlays :[MKOverlay] = [MKOverlay]()

	var length :Double = 0
	
	init(){
		GUID = UUID.init() // init randomly
		createTime = Date()
	}
	
	func add(location :CLLocationCoordinate2D, name :String) -> RoutePoint?{
		return add(indexParam: -1, location: location, name: name, section: nil, sectionPos: -1)
	}

	func add(location :CLLocationCoordinate2D, name :String, section :StWWSection?, sectionPos: Double) -> RoutePoint?{
		return add(indexParam: -1, location: location, name: name, section: section, sectionPos: sectionPos)
	}
	
	func add(indexParam: Int, location :CLLocationCoordinate2D, name :String, section :StWWSection?, sectionPos: Double) -> RoutePoint?{
		var routePoint = RoutePoint(route: self, location :location, name: name)
		
		var index = indexParam
		
		if index == -1 {
			routePoints.append(routePoint)
			index = routePoints.count - 1
		} else {
			routePoints.insert(routePoint, at: index)
		}
		
		if index > 0 {
			var prev = routePoints[index - 1]
			prev.next = routePoint
			routePoint.prev = prev
		}
		
		// todo: next part is about updating of rhumb lines
//		routePoint.updateSubPoints()
//		if let next = routePoint.next {
//			next.updateSubPoints()
//		}
		// todo: return the new point
		
		updateLength()
		needsSaving = true
		
		return routePoint
	}
	
	func findRoutePoint(_ rp :RoutePoint) -> Int {
		for (i, rp2) in routePoints.enumerated() {
			if rp2 === rp {
				return i
			}
		}
		return -1
	}
	
	func getEdit() -> Bool {
		return stateEdit
	}
	
	func setEdit(_ edit :Bool){
		if edit {
			Routes.service.stopRouteEdit()
			// set state to "edit" and "visible"
			stateEdit = true
		} else {
			// set state to "not-edit"
			stateEdit = false
		}
		// save state to app-settings
	}
	
	func getRoutePointCount() -> Int {
		return routePoints.count
	}
	

	
	func generateOverlays() -> [MKOverlay] {
		// return MKOverlays for this route
		
//		self.overlays = [MKOverlay]()
		
//		var routeLine = StRoutePolyline(coordinates: routePoints.map{$0.position}, count: routePoints.count)
//		overlays.append(routeLine)
		
		routePointOverlays.removeAll()
		lineOverlays.removeAll()
		for (i, routePoint) in routePoints.enumerated() {
		
			var overlaysForThisPoint = [MKOverlay]()

			let circle = StRoutePointCircle(center: routePoint.position, radius: 1)
			overlaysForThisPoint.append(circle)
			
			routePointOverlays.append(overlaysForThisPoint)
			
			if i < routePoints.count - 1 {
				
				let line = StRoutePolyline( coordinates: [routePoints[i].position, routePoints[i+1].position], count: 2)
				lineOverlays.append(line)
				
			}
		}
		
		return getAllOverlays()
	}
	
	func updateAnnotations() -> [RoutePointLabelAnnotation] {
		
		var newAnnotations = [RoutePointLabelAnnotation]()
		
		for (i, routePoint) in routePoints.enumerated() {
			
			if i == routePointAnnotations.count {
				let newAnnotation = RoutePointLabelAnnotation(routePoint)
				routePointAnnotations.append(newAnnotation)
				newAnnotations.append(newAnnotation)
			} else {
				routePointAnnotations[i].coordinate = routePoint.position
			}
			
			if i == 0 {
				routePointAnnotations[i].title = routePoint.route.name + "\n1"
			} else {
				routePointAnnotations[i].title = "\(String(i + 1))"
			}
		}
		
		return newAnnotations
	}
	
	func getAllOverlays() -> [MKOverlay] {
		var allOverlays = lineOverlays
		for overlays in routePointOverlays {
			allOverlays.append(contentsOf: overlays)
		}
		return allOverlays
	}
	
	func getOverlays(forRoutePoint routePoint :RoutePoint) -> [MKOverlay] {
		var overlays = [MKOverlay]()
		for (i, rp) in routePoints.enumerated() {
			if routePoint === rp {
				// route point overlays:
				overlays.append(contentsOf: routePointOverlays[i])
				// line overlays:
				if i >= 1 {
					overlays.append(lineOverlays[i - 1])
				}
				if i < routePoints.count - 1 {
					overlays.append(lineOverlays[i])
				}
			}
		}
		return overlays
	}
	
	func getAnnotation(forRoutePoint rp :RoutePoint) -> RoutePointLabelAnnotation {
		return routePointAnnotations[findRoutePoint(rp)]
	}
	
	func updateOverlays(forRoutePoint routePoint :RoutePoint, inRoute route :Route) -> [MKOverlay] {
		var overlaysToReturn = [MKOverlay]()

		for (i, rp) in routePoints.enumerated() {
			if routePoint === rp {
				var rpOverlays = [MKOverlay]()
				// rebuild overlays for this routepoint
				rpOverlays.append(StRoutePointCircle(center: routePoint.position, radius: 0.1))
				
				routePointOverlays[i] = rpOverlays
				
				overlaysToReturn.append(contentsOf: rpOverlays)
				
				// recreate line overlays
				if i >= 1 {
					let lineOverlay = StRoutePolyline( coordinates: [routePoints[i - 1].position, routePoints[i].position], count: 2)
					lineOverlays[i - 1] = lineOverlay
					overlaysToReturn.append(lineOverlay)
				}
				
				if i < routePoints.count - 1 {
					let lineOverlay = StRoutePolyline( coordinates: [routePoints[i].position, routePoints[i + 1].position], count: 2)
					lineOverlays[i] = lineOverlay
					overlaysToReturn.append(lineOverlay)
				}
				
			}
		}
		return overlaysToReturn
	}

	func removePointAt(pos :Int){
		
		if routePoints[pos] === Routes.service.activeRoutePoint {
			Routes.service.stopActiveRoute()
		}
		
		routePoints.remove(at: pos)
		
		if pos == 0 {
			if pos != routePoints.count {
				routePoints[pos].prev = nil
			} else {
				// route is empty
				//// TODO: remove?
			}
		} else {
			
			if pos < routePoints.count {
				routePoints[pos - 1].next = routePoints[pos]
				routePoints[pos].prev = routePoints[pos - 1]
			}
		}
		
		updateLength()
		needsSaving = true

	}
	
	func removeSelf(){
		Routes.service.deleteRoute(self)
	}
	
	func saveToGPX() {

		if routePoints.count == 0 { return }
		
		let root = GPXRoot(creator: "Stentec WinGPS Marine for iOS")
		
		var gpxRoute :GPXRoute = GPXRoute()
		gpxRoute.name = self.name
		
		for routePoint in routePoints {

//			var gpxTrackPoint = GPXTrackPoint(latitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude)
//			gpxTrackPoint.time = loc.timestamp
//			gpxSegment.add(trackpoint: gpxTrackPoint)
//			thisTrack.add(trackSegment: gpxSegment)
			
			var gpxRoutePoint = GPXRoutePoint(latitude: routePoint.position.latitude, longitude: routePoint.position.longitude)
			gpxRoutePoint.name = routePoint.name
//			gpxRoutePoint.time = routePoint.time
			
			gpxRoute.routepoints.append(gpxRoutePoint)
			
		}
		
		root.add(route: gpxRoute)
		
		if let routeId = Routes.service.getRouteIndex(self){
			let fileURL = Routes.getRouteFileUrl(idx: routeId)
			
			do {
				try root.gpx().write(to: fileURL, atomically: true, encoding: .utf8)
				needsSaving = false
			} catch {
				print(error)
			}
		}
	}
	
	func updateLength(){
		var length :Double = 0
		
		for rp in routePoints {
			if let nextRp = rp.next {
				length += CLLocation(latitude: rp.position.latitude, longitude: rp.position.longitude)
					.distance(from: CLLocation(latitude: nextRp.position.latitude, longitude: nextRp.position.longitude))
			}
		}
		self.length = length
	}
}

class RoutePoint {
	
	var name :String
	var next :RoutePoint?
	var position :CLLocationCoordinate2D
	var prev :RoutePoint?
	var route :Route
	
	private var distance :Double = -1 // When following route: distance to user
	
	var annotation :RoutePointLabelAnnotation?
	
	init(route :Route, location :CLLocationCoordinate2D, name :String){
		self.name = name
		self.position = location
		self.route = route
		
		// todo: add wwnetwork section data
	}
	
	func removeSelf(){
		var pos = route.findRoutePoint(self)
		route.removePointAt(pos: pos)
	}
	
	func updatePosition(_ newPos :CLLocationCoordinate2D) {
		position = newPos
		
		route.updateLength()
	}
	
	func getDistance() -> Double {
		return distance
	}
	
	func setDistance(_ d :Double){
		distance = d
		
		// Update annotation
		annotation?.updateAnnotationView()
	}
}
