//
//  Routes.swift
//  WinGPS Marine
//
//  Created by Standaard on 21/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation
import CoreGPX

// Static class to handle routes. Equivalent to the route-functions in Android of StDBManager

class Routes :NSObject{
	
	// constants:
	static let switchOverThresholdDistance :Double = 20 // in meters
	
	static let service = Routes()
	
	var routes = [Route]()
	
	var overlay :RouteOverlay?
	
	var activeRoutePoint :RoutePoint? = nil
	
	let routesLocationManager = CLLocationManager()

	let distanceFilterValue :CLLocationDistance = 1

	private override init(){
		super.init()
		
		routesLocationManager.delegate = self
		routesLocationManager.distanceFilter = kCLDistanceFilterNone
		routesLocationManager.desiredAccuracy = 20
		routesLocationManager.showsBackgroundLocationIndicator = true
		
		var idx :Int = 0
		while true {
			let fileURL = Routes.getRouteFileUrl(idx: idx)
			if let gpxRoot = GPXParser(withURL: fileURL)?.parsedData(),
				let gpxRoute = gpxRoot.routes.first {
				
				var route = Route()
				route.name = gpxRoute.name ?? ""
				
				for gpxPoint in gpxRoute.routepoints {
					route.add(location: CLLocationCoordinate2D(latitude: gpxPoint.latitude!, longitude: gpxPoint.longitude!), name: gpxPoint.name ?? "")
				}
				route.needsSaving = false
				
				overlay?.setOverlays(for: route)
				
				if route.routePoints.count > 0 {
					routes.append(route)
				}
				
				idx += 1
			} else {
				break
			}
		}
		// update notations
	}
	
	// when user creates new route and first route point
	func createNewRoute(location: CLLocationCoordinate2D) -> Route {
		// check if it is allowed to create a new route at this point
		
		// find network section
		
		// init route
		let route = Route()
		route.name = "Route " + String(routes.count + 1)

		route.add(location: location, name: "") // section
		addRoute(route)
		route.saveToGPX()
	
		return route
	}
	
	func getEditRoute() -> Route? {
		for route in routes {
			if route.getEdit() {
				return route
			}
		}
		return nil
	}
	
	func stopRouteEdit(){
		if let route = getEditRoute() {
			route.setEdit(false)
			if route.getRoutePointCount() == 0 {
				deleteRte(route.GUID)
			} else {
				route.saveToGPX()
			}
		}
	}
	
	func deleteRte(_ guid: UUID) {
		if let route = findRouteByGUID(guid) {
			for i in 0 ..< routes.count {
				if routes[i] === route {
					routes.remove(at: i)
				}
			}
		}
	}
	
	func findRouteByGUID(_ guid :UUID) -> Route? {
		for route in routes {
			if route.GUID == guid {
				return route
			}
		}
		return nil
	}
	
	func addRoute(_ route :Route){
		routes.append(route)
		
		// add drawable elements to main route overlay
		if let overlay = self.overlay {
			overlay.setOverlays(for: route)
		}
	}
	
	func getAllOverlays() -> [MKOverlay]{
		var overlays = [MKOverlay]()
		for route in routes{
			overlays.append(contentsOf: route.getAllOverlays())
		}
		return overlays
	}
	
	func getRouteIndex(_ route :Route) -> Int? {
		for (i, r) in routes.enumerated() {
			if r === route {
				return i
			}
		}
		return nil
	}
	
	func deleteRoute(_ route :Route){
		if let i = getRouteIndex(route) {
			routes.remove(at: i)
			
			// Remove file and rename others
			var url = Routes.getRouteFileUrl(idx: i)
			do {
				try FileManager.default.removeItem(atPath: url.path)
				
				if i < routes.count - 1 {
					for j in i + 1 ..< routes.count {
						let nextUrl = Routes.getRouteFileUrl(idx: j)
						try FileManager.default.moveItem(at: nextUrl, to: url)
						url = nextUrl
					}
				}
			} catch {
				// if failed, it might have not existed yet.
			}
		}
		
		if route === activeRoutePoint?.route {
			Routes.service.stopActiveRoute()
		}
	}
	
	func startRouteFromPoint(_ routePoint :RoutePoint) {
		// reset distances
		if let rpts = activeRoutePoint?.route.routePoints {
			for rp in rpts { rp.setDistance(-1) }
		}
		
		activeRoutePoint = routePoint
		updateRouteNavpointLocation(routePoint)
		
		startFollowing()
	}
	
	func startFollowing() {
		routesLocationManager.allowsBackgroundLocationUpdates = true
		routesLocationManager.startUpdatingLocation()
	}
	
	func stopFollowing() {
		routesLocationManager.stopUpdatingLocation()
		routesLocationManager.allowsBackgroundLocationUpdates = false
		for rp in activeRoutePoint?.route.routePoints ?? [RoutePoint]() {
			rp.setDistance(-1)
		}
		activeRoutePoint = nil

	}
	
	func updateRouteNavpointLocation(_ routePoint : RoutePoint){
		overlay?.vc.setNavigationWaypoint(routePoint.position)
		
	}
	
	func stopActiveRoute(){
		stopFollowing()
		overlay?.vc.removeNavwaypoint()
	}
	
	func saveRoutes(){
		
		for route in routes {
			if route.needsSaving {
				route.saveToGPX()
				route.needsSaving = false
			}
		}
	}
	
	static func getRouteFileUrl(idx :Int) -> URL {
		let filename :String = "route-\(idx).gpx"
		let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
		let URL = documentsDirectory.appendingPathComponent(filename)
		return URL
	}
}

extension Routes: CLLocationManagerDelegate {
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

		/// Update route tracking
		if let location = locations.last {
			dout("RouteService didUpdateLocations \(location.speed) \(location.course) \(location.coordinate.longitude) \(location.coordinate.latitude)")
		}
		
		guard let location = locations.last,
			location.horizontalAccuracy < 20 else {
				return
		}
		
		if let activeRoutePoint = self.activeRoutePoint {
			
			dout("distance: ", location.distance(from: CLLocation(latitude: activeRoutePoint.position.latitude, longitude: activeRoutePoint.position.longitude)))
			
			let activeRoute = activeRoutePoint.route
			var currentRoutePoint = activeRoutePoint
			while true {
				if location.distance(from: CLLocation(latitude: currentRoutePoint.position.latitude, longitude: currentRoutePoint.position.longitude)) < Routes.switchOverThresholdDistance {
					if let nextRP = currentRoutePoint.next{
						startRouteFromPoint(nextRP)
					} else {
						stopActiveRoute()
						// TODO: Give a notification that the route is finished?
					}
					break
				}
				// try routepoints further up the route
				if let nextRP = currentRoutePoint.next {
					currentRoutePoint = nextRP
				} else {
					break
				}
			}
		}
	}
}
