//
//  RouteOverlay.swift
//  WinGPS Marine
//
//  Created by Standaard on 27/08/2019.
//  Copyright © 2019 Stentec. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class RouteOverlay {
	
	var vc :ViewController
	
	var editRoute :Route?
	
	var RPDrag :RoutePoint?
	var RPMovable :Bool = false
	var longPressed :Bool = false
	
	var longPressedRoutePoint :RoutePoint?
	
	var dragAnnotation :DraggingAnnotation?
	
	init(_ viewController :ViewController) {
		self.vc = viewController
		Routes.service.overlay = self
		
		// init route overlays for already loaded routes
		for route in Routes.service.routes {
			setOverlays(for: route)
		}
	}
	
	func setEditRoute(route :Route?, RPEdited :RoutePoint?) {
		if !(route === editRoute) {
			editRoute = route
			
			if RPEdited != nil {
				
				RPDrag = RPEdited
				RPMovable = true
				
				self.dragAnnotation = DraggingAnnotation()
				if let da = self.dragAnnotation {
					da.coordinate = RPEdited!.position
//					da.title = "Routepoint"
					vc.mapView.addAnnotation(da)
				}
				
				// enable pangestures // -> in annotationview
				
				
				longPressed = false

				// Remove dragging mode for this waypoint after 3 seconds:
				//mTouchLong.setTask(RPTouchLong.TOUCH_DRAGPOSSIBLE, 0, 0)
				//mTouchHandler.postDelayed(mTouchLong, 3000)
				//mMapView.invalidate()
			}
		}
		
		if editRoute == nil {
			clearRPDrag()

		} else {
			
//			// disable longpressmenu on mapview
//			vc.longPressGesture?.isEnabled = false
			
			setOverlays(for: editRoute!)
			
		}
	}
	
	func clearRPDrag(){
		RPDrag = nil
		
		if let da = dragAnnotation {
			vc.mapView.removeAnnotation(da)
			dragAnnotation = nil
		}
	}

	func onSingleTap(_ sender :UIGestureRecognizer, tappedPoint :CGPoint, tappedLocation :CLLocationCoordinate2D){
	
		guard let editRoute = self.editRoute else { return }
		
		// check if tapped on route point -> make draggable
		if let routePoint = touchingRoutePoint(point: tappedPoint, inRoute: editRoute) {
			// touched a routepoint
			RPDrag = routePoint
		} else { // not tapped on existing point, so create new
		
			// check if tapped on route leg -> insert new point
			
			var RPAfter :RoutePoint? = nil
			
			// check wwnetwork
			var section :WWSection? = nil
			var sectionPos :Double = 0
			
			if let rpAfter = RPAfter  {
				var index = editRoute.findRoutePoint(rpAfter)
				RPDrag = editRoute.add(indexParam: index, location: tappedLocation, name: "", section: section, sectionPos: sectionPos)
			} else {
				// else: append new route point to end
				RPDrag = editRoute.add(location: tappedLocation, name: "", section: section, sectionPos: sectionPos)
			}
		}
		
		if let da = self.dragAnnotation {
			vc.mapView.removeAnnotation(da)
		} else {
			self.dragAnnotation = DraggingAnnotation()
		}
		
		if let da = self.dragAnnotation {
			da.coordinate = RPDrag!.position
//			da.title = "Routepoint"
			vc.mapView.addAnnotation(da)
		}
		
		editRoute.needsSaving = true
		
		// Refresh overlay
		setOverlays(for: editRoute)
	}

	@objc func draggedPinAnnotationView(_ sender :UIGestureRecognizer){
		//self.view.bringSubviewToFront(containerView)
//		let translation = sender.translation(in: vc.mapView)
		//		waypointInfoView.center = CGPoint(x: waypointInfoView.center.x + translation.x, y: waypointInfoView.center.y + translation.y)
		
//		if let pinView = sender.view as? MKPinAnnotationView {

			self.dragAnnotation!.coordinate = vc.mapView.convert(sender.location(in: vc.mapView), toCoordinateFrom: vc.mapView)

			vc.mapView.view(for: self.dragAnnotation!)?.setNeedsDisplay()
	
			// Refresh overlay
//			setOverlays(for: self.editRoute!)
			
			if let RPDrag = self.RPDrag {
//				RPDrag.position = self.dragAnnotation!.coordinate
				RPDrag.updatePosition(self.dragAnnotation!.coordinate)
				updateOverlays(forPoint: RPDrag, inRoute: self.editRoute!)

				if RPDrag === Routes.service.activeRoutePoint {
					Routes.service.updateRouteNavpointLocation(RPDrag)
				}
			}
//		}
	}
	
	func setOverlays(for route: Route){
		// create or update overlays
		var oldOverlays = route.getAllOverlays()
		vc.mapView.addOverlays(route.generateOverlays())
		vc.mapView.removeOverlays(oldOverlays)
		
		// create or update annotations
		var newAnnotations = route.updateAnnotations()
		vc.mapView.addAnnotations(newAnnotations)

		if route.routePointAnnotations.count == route.routePoints.count + 1 {
			let annotationToRemove = route.routePointAnnotations.remove(at: route.routePointAnnotations.count - 1 )
			// remove last annotation
			vc.mapView.removeAnnotation(annotationToRemove)
		}
	}
	
	func updateOverlays(forPoint routePoint :RoutePoint, inRoute route :Route){
		vc.mapView.removeOverlays(route.getOverlays(forRoutePoint: routePoint))
		vc.mapView.addOverlays(route.updateOverlays(forRoutePoint: routePoint, inRoute: route))
		
		// update annotations
		route.getAnnotation(forRoutePoint: routePoint).coordinate = routePoint.position
	}
	
	func touchingRoutePoint(point :CGPoint) -> RoutePoint? {
		for route in Routes.service.routes {
			if let touchedRP = touchingRoutePoint(point: point, inRoute: route) {
				return touchedRP
			}
		}
		return nil
	}
		
	func touchingRoutePoint(point :CGPoint, inRoute editRoute :Route) -> RoutePoint? {
		
		let hitRadius = 50
		
		for routePoint in editRoute.routePoints {
			var screenPos = vc.mapView.convert(routePoint.position, toPointTo: vc.mapView)
			
			if screenPos.distanceTo(point) < Double(hitRadius) {
				return routePoint
			}
		}
		return nil
	}

	//	func refreshOverlays(){
	//		var oldOverlays = vc.mapView.overlays
	//		var newOverlays = Routes.service.getAllOverlays()
	//		for ol in oldOverlays {
	//			if !(ol is StRoutePolyline),
	//			!(ol is StRoutePointCircle) {
	//				newOverlays.append(ol)
	//			}
	//		}
	//		let serialQueue = DispatchQueue(label: "com.test.mySerialQueue")
	//
	//		vc.mapView.removeOverlays(vc.mapView.overlays)
	//		vc.mapView.addOverlays(newOverlays)
	//
	//	}

	
	func makeRoutePointLongpressMenu(stackView :inout UIStackView, routePoint :RoutePoint) {
		
		longPressedRoutePoint = routePoint
		
		//// show route options: start route from here, edit route, remove route point, remove route ////
		
		for sv in stackView.arrangedSubviews {
			sv.removeFromSuperview()
		}
		
		//		var startRouteButton = UIButton(frame: CGRect(x:0, y: 0, width: 100, height: 50))
		
		if Routes.service.activeRoutePoint?.route === routePoint.route {
			var stopRouteButton = UIButton(type: .system)
			stopRouteButton.setTitle("Stop Route", for: .normal)
			stopRouteButton.addTarget(self, action: #selector(stopRouteButtonTapped), for: .touchUpInside)
			stackView.addArrangedSubview(stopRouteButton)
		}
		
//		if Routes.service.activeRoutePoint?.route !== routePoint.route {

		if Routes.service.activeRoutePoint !== routePoint {
			var startRouteButton = UIButton(type: .system)
			startRouteButton.setTitle("Start Route From Point", for: .normal)
			startRouteButton.addTarget(self, action: #selector(startRouteButtonTapped), for: .touchUpInside)
			stackView.addArrangedSubview(startRouteButton)
		}
		
		//		}
		
		var editRouteButton = UIButton(type: .system)
		editRouteButton.setTitle("Edit Route", for: .normal)
		editRouteButton.addTarget(self, action: #selector(editRouteButtonTapped), for: .touchUpInside)
		stackView.addArrangedSubview(editRouteButton)
		
		var removeRoutePointButton = UIButton(type: .system)
		removeRoutePointButton.setTitle("Remove Route Point", for: .normal)
		removeRoutePointButton.addTarget(self, action: #selector(removeRoutePoint), for: .touchUpInside)
		stackView.addArrangedSubview(removeRoutePointButton)
		
		var removeRouteButton = UIButton(type: .system)
		removeRouteButton.setTitle("Remove Route", for: .normal)
		removeRouteButton.addTarget(self, action: #selector(removeRoute), for: .touchUpInside)
		stackView.addArrangedSubview(removeRouteButton)
		
		return
		
		//		let stackView = UIStackView(frame: CGRect(x:0, y: 0, width: 100, height: 50))
		//		stackView.axis = .vertical
		//		stackView.alignment = .fill // .Leading .FirstBaseline .Center .Trailing .LastBaseline
		//		stackView.distribution = .fill // .FillEqually .FillProportionally .EqualSpacing .EqualCentering
		////		stackView.distribution  = UIStackViewDistribution.equalSpacing
		//		stackView.alignment = UIStackView.Alignment.center
		//		stackView.spacing   = 16.0
		//
		//		let label = UIButton(type: .custom)
		//		label.setTitle("Label", for: .normal)
		//		stackView.addArrangedSubview(label)
		//		let label2 = UILabel()
		//		label2.text = "Label 2"
		//		stackView.addArrangedSubview(label2)
		//
		//		// for horizontal stack view, you might want to add width constraint to label or whatever view you're adding.
		//		return stackView
		
	}
	
	func makeRoutePointLongpressMenuEditMode(stackView :inout UIStackView, routePoint :RoutePoint) {
		
		longPressedRoutePoint = routePoint
		
		// show option to remove routepoint
		
		for sv in stackView.arrangedSubviews {
			sv.removeFromSuperview()
		}
		
		var removeRoutePointButton = UIButton(type: .system)
		removeRoutePointButton.setTitle("Remove Route Point", for: .normal)
		removeRoutePointButton.addTarget(self, action: #selector(removeRoutePoint), for: .touchUpInside)
		stackView.addArrangedSubview(removeRoutePointButton)

	}
	
	@objc func startRouteButtonTapped(sender: UIButton!) {
		dout("startRoute function")
		vc.generalUsageMenuContainer.isHidden = true
		
		Routes.service.startRouteFromPoint(longPressedRoutePoint!)
		
	}
	
	@objc func stopRouteButtonTapped(sender: UIButton!) {
		dout("startRoute function")
		vc.generalUsageMenuContainer.isHidden = true
		
		Routes.service.stopActiveRoute()
	}
	
	@objc func editRouteButtonTapped(sender: UIButton!) {
		dout("editRoute function")
		vc.generalUsageMenuContainer.isHidden = true
		
		if let routePoint = longPressedRoutePoint {
			startToEditRoute(route: routePoint.route)
		}
	}
	
	@objc func removeRoutePoint(sender: UIButton!) {
		dout("removeRoutePoint function")
		vc.generalUsageMenuContainer.isHidden = true

		longPressedRoutePoint?.removeSelf()
		
		if longPressedRoutePoint === RPDrag {
			clearRPDrag()
		}

		// refresh mapview overlays
		setOverlays(for: (longPressedRoutePoint?.route)!)

	}
	
	@objc func removeRoute(sender: UIButton!) {
		dout("removeRoute function")
		vc.generalUsageMenuContainer.isHidden = true
		
		if let route = longPressedRoutePoint?.route {
			remove(route)
		}
	}
	
	func remove(_ route :Route){
		
		vc.mapView.removeOverlays(route.getAllOverlays())
		vc.mapView.removeAnnotations(route.routePointAnnotations)
		
		route.removeSelf()
	}
	
	func startToEditRoute(route :Route){
		route.setEdit(true)
		
		if !vc.mRouteOverlayEnabled {
			// todo: if routes are not enabled: set all routes to visible
		}
		
		// disable touch of waypointoverlay
		vc.waypointTouchEnabled = false
		
		// set editmode of routeoverlay to this route
		setEditRoute(route: route, RPEdited: route.routePoints[0])
		
		// disable follow/centermode
		vc.stopEditingRouteButtonView.isHidden = false
	}
	
}

class StRoutePolyline :MKPolyline { }

class StRoutePointCircle :MKCircle { }

class DraggingAnnotation :MKPointAnnotation {
	func setCoordinate(newCoord :CLLocationCoordinate2D){
		
	}
}

class RoutePointLabelAnnotation :MKPointAnnotation {
	
	var routePoint :RoutePoint
	var annotationView :MKAnnotationView?
//	var textLabel :UILabel?
	
	init(_ routePoint :RoutePoint){
		self.routePoint = routePoint
		super.init()
		coordinate = routePoint.position
		routePoint.annotation = self
	}

	func updateAnnotationView(){
		if let annotationView = self.annotationView {
			for sv in annotationView.subviews {
				sv.removeFromSuperview()
			}
			annotationView.addSubview(getTextLabelView())
		}
	}
	
	func getTextLabelView() -> UILabel {
		let textLabel = UILabel()
		textLabel.textColor = UIColor.black
		
		textLabel.textAlignment = .center
		textLabel.numberOfLines = 0
		
		//		let title = annotation.title ?? "failed"
		//			textLabel.text = title ?? "failed"

		textLabel.textAlignment = .left
		if routePoint.getDistance() > 0 {
			textLabel.text = "\(title ?? "")\n\(StUtils.formatDistance(routePoint.getDistance()))"
		} else {
			textLabel.text = "\(title ?? "")"
		}
			
		textLabel.invalidateIntrinsicContentSize()
		
		//			textLabel.frame = CGRect(x: 0, y: 0 , width: 400, height: 20)
		textLabel.sizeToFit()
		textLabel.frame.origin = CGPoint(x: 12, y: -(textLabel.frame.height / 2))
		return textLabel
	}
	
}
